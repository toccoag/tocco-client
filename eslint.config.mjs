import path from 'node:path'
import {fileURLToPath} from 'node:url'

import babelParser from '@babel/eslint-parser'
import babel from '@babel/eslint-plugin'
import {fixupPluginRules} from '@eslint/compat'
import {FlatCompat} from '@eslint/eslintrc'
import chaiFriendly from 'eslint-plugin-chai-friendly'
import pluginCypress from 'eslint-plugin-cypress/flat'
import _import from 'eslint-plugin-import'
import jest from 'eslint-plugin-jest'
import promise from 'eslint-plugin-promise'
import react from 'eslint-plugin-react'
import reactHooks from 'eslint-plugin-react-hooks'
import unusedImports from 'eslint-plugin-unused-imports'
import globals from 'globals'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({baseDirectory: __dirname})

export default [
  {
    ignores: [
      '**/test-bundler.js',
      '**/node_modules',
      '**/dist',
      '**/coverage',
      '**/.idea',
      '**/templates',
      'server/static/node_modules',
      'server/wp',
      'server/legacy',
      'packages/**/*/dist',
      'packages/**/*/node_modules',
      'packages/**/*/coverage',
      'packages/core/tocco-ui/src/HtmlEditor/extraPlugins',
      '**/*.lock',
      '**/*.json'
    ]
  },
  ...compat.extends('standard', 'prettier'),
  react.configs.flat.recommended,
  react.configs.flat['jsx-runtime'],
  pluginCypress.configs.globals,
  {
    plugins: {
      '@babel': babel,
      react,
      promise,
      import: _import,
      'chai-friendly': chaiFriendly,
      jest,
      // TODO: TOCDEV-9952
      // not yet released
      // https://github.com/facebook/react/issues/28313
      'react-hooks': fixupPluginRules(reactHooks),
      'unused-imports': unusedImports
    },

    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node,
        ...jest.environments.globals.globals,
        __CI__: false,
        __DEV__: false,
        __DEV_PACKAGE_INPUT__: false,
        __PROD__: false,
        __BACKEND_URL__: false,
        __PACKAGE__: false,
        __PACKAGE_NAME__: false,
        __webpack_public_path__: true,
        jestExpect: false,
        sinon: false
      },
      parser: babelParser
    },

    settings: {
      react: {
        version: 'detect'
      }
    },

    rules: {
      curly: [2, 'all'],
      'brace-style': [2, '1tbs'],

      'key-spacing': [
        2,
        {
          beforeColon: false,
          afterColon: true,
          mode: 'strict'
        }
      ],

      semi: [2, 'never'],

      'max-len': [
        'error',
        {
          code: 120,
          ignoreUrls: true
        }
      ],

      'arrow-parens': [2, 'as-needed'],
      'object-curly-spacing': [2, 'never'],
      'no-var': 2,
      'prefer-const': 2,

      'no-trailing-spaces': [
        2,
        {
          skipBlankLines: true
        }
      ],

      'generator-star-spacing': [
        2,
        {
          before: false,
          after: true
        }
      ],

      'no-console': 2,

      'react/jsx-no-bind': [
        2,
        {
          allowArrowFunctions: true,
          allowBind: true
        }
      ],

      'react/display-name': 0,
      'jsx-quotes': [2, 'prefer-double'],
      'react-hooks/rules-of-hooks': 2,
      'react-hooks/exhaustive-deps': 2,
      'no-nested-ternary': 2,
      'no-unused-expressions': 0,

      'chai-friendly/no-unused-expressions': [
        2,
        {
          allowTaggedTemplates: true
        }
      ],

      'import/default': 2,
      'import/no-duplicates': 2,

      'import/order': [
        2,
        {
          groups: ['builtin', 'external', 'parent', ['sibling', 'index']],
          'newlines-between': 'always',

          alphabetize: {
            order: 'asc',
            caseInsensitive: true
          }
        }
      ],

      'import/first': 2,
      'import/no-webpack-loader-syntax': 0,
      'prefer-regex-literals': 0,
      'eol-last': 2,
      'react/jsx-uses-react': 'off',
      'react/react-in-jsx-scope': 'off',
      'unused-imports/no-unused-imports': 'error',
      'no-shadow': 'error',
      'default-param-last': 'error'
    }
  },
  {
    files: ['**/reducer.js'],
    rules: {
      'default-param-last': 'off'
    }
  }
]
