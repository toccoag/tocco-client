/* eslint-disable no-console */
import {ReadableStream} from 'node:stream/web'

import {configure} from '@testing-library/dom'
import crypto from '@trust/webcrypto'
import chai from 'chai'
import chaiAsPromised from 'chai-as-promised'
import fetch from 'node-fetch'
import sinon from 'sinon'
import sinonChai from 'sinon-chai'
import '@testing-library/jest-dom'

configure({asyncUtilTimeout: 5000})

global.__PACKAGE_NAME__ = 'jest'

global.chai = chai
global.sinon = sinon
const jestExpect = global.expect
global.expect = chai.expect
global.jestExpect = jestExpect
global.should = chai.should()

global.crypto = crypto

global.fetch = fetch
global.Response = fetch.Response
global.Headers = fetch.Headers
global.Request = fetch.Request
global.ReadableStream = ReadableStream

chai.use(sinonChai)
chai.use(chaiAsPromised)
chai.config.truncateThreshold = 0

const error = console.error
console.error = function (warning, ...args) {
  // Throw error on prop type warnings
  if (/(Invalid prop|Failed prop type)/.test(warning)) {
    throw new Error(warning)
  }

  // Do not print Rect Intl warnings
  if (/^\[React Intl\]/.test(warning)) {
    return
  }

  error.apply(console, [warning, ...args])
}
