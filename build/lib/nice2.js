import path from 'path'

import dotenv from 'dotenv'

import {getNiceVersionWithoutPatch} from '../../scripts/helpers'

import {getAllDirectories, readFile} from './packages'

const {parsed: envs} = dotenv.config({path: './.env'})
const nice2PropertiesPath = '/boot/src/main/resources/ch/tocco/nice2/boot/impl/default.properties'

export const getNice2Folder = () => {
  const basePath = envs.NICE2_REPO_BASE_PATH
  if (!basePath) {
    throw new Error("Environment variable 'NICE2_REPO_BASE_PATH' not defined. Cannot find correct nice2 folder")
  }
  const propertiesNiceVersion = getNiceVersionWithoutPatch()

  const niceDirs = getAllDirectories(basePath)
  const niceDir = niceDirs.find(dir => {
    const properties = readFile(path.join(basePath, dir, nice2PropertiesPath), false)
    return properties.includes(`nice.version=${propertiesNiceVersion}`)
  })

  return path.join(basePath, niceDir)
}
