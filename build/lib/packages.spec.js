import {
  getAllPackagesInDirectory,
  getCustomerOfPackage,
  getPackageDirectory,
  isPackageReleased,
  packageExists,
  readFile
} from './packages'

/**
 * test are executed on real file structure
 */
describe('build', () => {
  describe('lib', () => {
    describe('packages', () => {
      describe('readFile', () => {
        test('read file content', () => {
          expect(readFile('package.json')).to.not.equal('')
        })

        test('throw error if file not exists', () => {
          try {
            readFile('invalid-file-name')
            expect.fail('should throw an error')
          } catch (err) {
            expect(err.message).to.equal("File 'invalid-file-name' does not exist")
          }
        })

        test('should return empty string if file not exists and errorIfFileNotExists is false', () => {
          expect(readFile('invalid-file-name', false)).to.equal('')
        })
      })

      describe('getAllPackagesInDirectory', () => {
        test('get all packages in apps folder', () => {
          expect(getAllPackagesInDirectory('apps')).to.eql(['admin', 'devcon', 'widget-showcase'])
        })

        test('only get direct child folders and not recursive', () => {
          expect(getAllPackagesInDirectory('customers/ihkschwaben')).to.eql(['actions', 'bundles', 'widgets'])
        })
      })

      describe('getPackageDirectory', () => {
        test('get package directory of admin', () => {
          expect(getPackageDirectory('admin')).to.equal('packages/apps/admin')
        })
      })

      describe('getCustomerOfPackage', () => {
        test('get customer name of package', () => {
          expect(getCustomerOfPackage('ihkschwaben-disadvantage-compensation')).to.equal('ihkschwaben')
        })
      })

      describe('packageExists', () => {
        test('package admin exists', () => {
          expect(packageExists('admin')).to.equal(true)
        })

        test('package admin exists', () => {
          expect(packageExists('invalid-package-name')).to.equal(false)
        })
      })

      describe('isPackageReleased', () => {
        test('package admin is not released', () => {
          expect(isPackageReleased('entity-list')).to.equal(false)
        })

        test('package admin exists', () => {
          expect(isPackageReleased('widget-bundle')).to.equal(true)
        })
      })
    })
  })
})
