/* eslint-disable no-console */

// ANSI Color Chart: https://en.m.wikipedia.org/wiki/ANSI_escape_code#Colors

const log = console.log.bind(console)

const error = (...messages) => {
  console.error(`\x1b[31m✖ ${messages}\x1b[0m`)
}

const info = (...messages) => {
  console.info(`\x1b[36mℹ︎ ${messages}\x1b[0m `)
}

const success = (...messages) => {
  console.log(`\x1b[32m✔ ${messages}\x1b[0m`)
}

const warn = (...messages) => {
  console.warn(`\x1b[33m⚠ ${messages}\x1b[0m`)
}

export default {log, error, info, success, warn}
