import fs from 'fs'

import config from '../../config/index'

const paths = config.utils_paths

const customersDir = 'customers'

/**
 * gets conent of file
 */
export const readFile = (path, errorIfFileNotExists = true) => {
  if (fs.existsSync(path)) {
    const buffer = fs.readFileSync(path)
    return buffer.toString()
  }
  if (errorIfFileNotExists) {
    throw new Error(`File '${path}' does not exist`)
  }
  return ''
}

/**
 * gets content of json file
 */
export const readJsonFile = path => JSON.parse(readFile(path))

export const getAllDirectories = path =>
  fs.existsSync(path) ? fs.readdirSync(path).filter(file => fs.statSync(path + '/' + file).isDirectory()) : []

export const getAllFiles = path => fs.readdirSync(path).filter(file => fs.statSync(path + '/' + file).isFile())

/**
 * returns all files recursively in path
 */
export const getAllFilesRecursive = path => {
  return fs
    .readdirSync(path)
    .map(file => `${path}/${file}`)
    .map(relativePath => {
      if (fs.statSync(relativePath).isFile()) {
        return relativePath
      } else {
        return getAllFilesRecursive(relativePath)
      }
    })
    .flat()
}

export const getAllCustomers = () => {
  const path = paths.client('packages')
  return getAllDirectories(`${path}/${customersDir}`)
}

const getCutomersRootDirs = () => {
  const path = paths.client('packages')
  const customers = getAllCustomers()
  const customerRootDirs = customers.reduce((acc, customerDir) => {
    const customerPath = `${path}/${customersDir}/${customerDir}`
    return [...acc, ...getAllDirectories(customerPath).map(dir => `${customersDir}/${customerDir}/${dir}`)]
  }, [])
  return customerRootDirs
}

const getAllPackagesRootDirs = () => {
  const path = paths.client('packages')

  const rootDirs = getAllDirectories(path).filter(d => d !== customersDir)
  const customerRootDirs = getCutomersRootDirs()

  return [...rootDirs, ...customerRootDirs]
}

/**
 * returns all packages expect if it is exluded with the optional exclude function
 */
export const getAllPackages = excludeFn => {
  const path = paths.client('packages')

  const rootDirs = getAllPackagesRootDirs()

  return rootDirs.reduce((acc, directory) => {
    const packages = getAllDirectories(`${path}/${directory}`)
    const includedPackages = excludeFn ? packages.filter(p => excludeFn(directory, p)) : packages
    return [...acc, ...includedPackages]
  }, [])
}

/**
 * returns all packages which are directly located in this directory (no recursive search)
 */
export const getAllPackagesInDirectory = directory => {
  const path = paths.client('packages')
  return getAllDirectories(`${path}/${directory}`)
}

/**
 * returns the path where the package is defined
 */
export const getPackageDirectory = packageName => {
  const path = paths.client('packages')

  const rootDirs = getAllPackagesRootDirs()

  const rootDir = rootDirs.find(dir => getAllDirectories(`${path}/${dir}`).includes(packageName))
  return rootDir ? `packages/${rootDir}/${packageName}` : null
}

/**
 * gets customer name to which a package belongs
 */
export const getCustomerOfPackage = packageName => {
  const packageDir = getPackageDirectory(packageName)
  const match = packageDir.match(/^packages\/customers\/(?<customer>[^/]*)/)
  return match?.groups?.customer
}

/**
 * returns true if a package name exists in the repo
 */
export const packageExists = packageName => {
  const availablePackages = getAllPackages()

  return availablePackages.indexOf(packageName) > -1
}

/**
 * returns package json
 */
export const getPackageJson = packageName => readJsonFile(`${getPackageDirectory(packageName)}/package.json`)

/**
 * returns true if a package is released (not set to private in the package json)
 */
export const isPackageReleased = packageName => getPackageJson(packageName).private !== true
