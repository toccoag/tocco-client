import {resolve, join} from 'path'

import CopyWebpackPlugin from 'copy-webpack-plugin' // eslint-disable-line import/default
import {argv} from 'yargs'

import {getAllFiles, getPackageDirectory} from './packages'

const getAllApps = path => getAllFiles(path).map(file => file.replace('.js', ''))
const getSelectedApps = defaultApps => {
  const app = argv.app
  if (!app) {
    return defaultApps
  }
  if (Array.isArray(app)) {
    return app
  }

  return [app]
}

export const adjustConfigForBundles = (webpackConfig, config, paths) => {
  const {__PACKAGE__} = config.globals
  const packageDir = getPackageDirectory(__PACKAGE__)

  const path = paths.client(`${packageDir}/src/apps`)
  const apps = getAllApps(path)

  const selectedApps = getSelectedApps(apps)

  const entry = apps
    .filter(app => selectedApps.includes(app))
    .reduce((acc, app) => ({...acc, [app]: `${path}/${app}.js`}), {})

  webpackConfig.entry = entry
  webpackConfig.output.filename = '[name].js'
  webpackConfig.output.library = 'tocco-[name]'

  return webpackConfig
}

const CKEDITOR_GLOBAL_VAR_REGEX = /CKEDITOR/g
const NEW_CKEDITOR_GLOBAL_VAR = 'NEW_CKEDITOR'

const CKEDITOR_CLASS_PREFIX_REGEX = /\bcke(?:(?![:])(\b|_))/g
// has to be a 3-character text since the editor has some substring logic implemented
const NEW_CKEDITOR_CLASS_PREFIX = `edi$1`

/**
 * Workaround: co-existence with ckeditor from legacy actions
 *  - rename classnames/ids from cke* to edi*
 *  - rename global instance window.CKEDITOR to window.NEW_CKEDITOR
 */
const tranformCKEditor = (content, filename) => {
  if (filename.endsWith('.js') || filename.endsWith('.css')) {
    return content
      .toString()
      .replace(CKEDITOR_GLOBAL_VAR_REGEX, NEW_CKEDITOR_GLOBAL_VAR)
      .replace(CKEDITOR_CLASS_PREFIX_REGEX, NEW_CKEDITOR_CLASS_PREFIX)
  }
  return content
}

export const getCKEditorPlugin = outputDir => {
  const extraPluginPath = join('packages', 'core', 'tocco-ui', 'src', 'HtmlEditor', 'extraPlugins')
  const lang = ['lang/de.js', 'lang/de-ch.js', 'lang/fr.js', 'lang/it.js', 'lang/en.js']

  const includes = [
    'ckeditor.js',
    'config.js',
    'contents.css',
    'styles.js',
    'adapters/**/*',
    'skins/**/*',
    'vendor/**/*',
    ...lang,
    ...lang.map(l => `plugins/*/${l}`),
    'plugins/*/!(lang)/**/*',
    'plugins/*/*'
  ]

  // do not reuse CopyWebpackPlugin for ckeditor4 for other stuff
  // it will potentially be removed by `removeCKEditor` function
  return new CopyWebpackPlugin({
    patterns: [
      {
        from: `{${includes.join(',')}}`,
        to({absoluteFilename}) {
          // rename entry file ckeditor.js to ckeditor-new.js to guarantee co-existence with legacy ckeditor
          return absoluteFilename.endsWith('ckeditor.js')
            ? resolve(outputDir, 'ckeditor4', 'ckeditor-new.js')
            : resolve(outputDir, 'ckeditor4')
        },
        context: join('node_modules', 'ckeditor4'),
        transform: tranformCKEditor
      },
      // codemirror
      {
        from: `{plugin.js,css/**/*,js/**/*,images/**/*,dialogs/**/*,${lang.join(',')}}`,
        to: resolve(outputDir, 'ckeditor4', 'plugins', 'codemirror'),
        context: join(extraPluginPath, 'codemirror'),
        transform: tranformCKEditor
      }
    ]
  })
}

/**
 * Workaround: co-existence with ckeditor from legacy actions
 *  - rename classnames/ids from cke* to edi*
 *  - rename global instance window.CKEDITOR to window.NEW_CKEDITOR
 */
export const getCKEditorRules = () => [
  {
    test: /\.(js|css)$/,
    include: /node_modules\/(?=(ckeditor4|ckeditor4-react|ckeditor4-integrations-common)\/).*/,
    loader: 'string-replace-loader',
    options: {
      multiple: [
        {search: CKEDITOR_GLOBAL_VAR_REGEX, replace: NEW_CKEDITOR_GLOBAL_VAR, flags: 'g'},
        {search: CKEDITOR_CLASS_PREFIX_REGEX, replace: NEW_CKEDITOR_CLASS_PREFIX, flags: 'g'}
      ]
    }
  }
]

export const removeCKEditor = webpackConfig => {
  webpackConfig.plugins = webpackConfig.plugins.filter(
    plugin =>
      !(plugin instanceof CopyWebpackPlugin && plugin.patterns.some(pattern => pattern.context.includes('ckeditor4')))
  )
  return webpackConfig
}
