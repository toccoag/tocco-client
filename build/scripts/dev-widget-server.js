import fetch from 'isomorphic-fetch'
import {argv} from 'yargs'

import config from '../../config/index'
import logger from '../lib/logger'

const port = config.server_port
const host = config.server_host

const key = argv.key

const appPackageMap = {
  'password-update': 'login'
}

if (!key) {
  logger.error('Please select an existing widget config key with --key={WIDGET_CONFIG_KEY} parameter.')
  process.exit(1)
}

// `window.location.hostname` might be used in __BACKEND_URL__ variable
// eslint-disable-next-line
const window = {location: {hostname: 'localhost'}}
// eslint-disable-next-line
const backendUrl = eval(config.globals.__BACKEND_URL__)

const getWidgetConfig = async () => {
  const widgetConfig = await fetch(`${backendUrl}/nice2/rest/widget/configs/${key}`).then(response => response.json())
  return widgetConfig
}

/**
 * Sets widget input props via __DEV_PACKAGE_INPUT__ variable.
 * Variable is replaced by build time.
 * Use IIFE to be able to set `customTheme` on runtime via `window.toccoTheme` var on page.
 */
const setInput = widgetConfig => {
  const {locale, config: inputConfig} = widgetConfig
  config.globals.__DEV_PACKAGE_INPUT__ = `(() => {
    return {
      backendUrl: '${backendUrl}',
      locale: '${locale}',
      customTheme: window.toccoTheme || {},
      ...${JSON.stringify(inputConfig)},
      appContext: {
        embedType: 'widget',
        widgetConfigKey: '${key}'
      }
    }
})()`
}

const setPackage = widgetConfig => {
  const {appName} = widgetConfig
  const packageName = appPackageMap[appName] || appName

  config.globals.__PACKAGE__ = packageName
  config.globals.__PACKAGE_NAME__ = JSON.stringify(packageName)
  config.globals.__APP__ = appName
}

const logConfigLink = widgetConfig => {
  const {key: configPk, appName} = widgetConfig
  const link = `${backendUrl}/tocco/e/Widget_config/${configPk}/detail`
  logger.info(`Config loaded from ${link} and running '${appName}' widget.`)
}

getWidgetConfig().then(widgetConfig => {
  setInput(widgetConfig)
  setPackage(widgetConfig)

  const server = require('../../server/main')
  server({simulateCms: true}).listen(port)
  logConfigLink(widgetConfig)
  logger.success(`Server is now running at http://${host}:${port}.`)
})
