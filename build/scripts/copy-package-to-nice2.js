import {exec} from 'child_process'

import {argv} from 'yargs'

import logger from '../lib/logger'
import {getNice2Folder} from '../lib/nice2'
import {getCustomerOfPackage, getPackageDirectory, packageExists} from '../lib/packages'

const defaultModule = 'core/web-core'
const packageToNice2ModuleMap = {
  'input-edit': 'optional/qualification',
  'resource-scheduler': 'optional/resourcescheduler',
  'docs-browser': 'core/dms',
  devcon: 'core/devcon',
  'widget-bundle': 'core/widget',
  'widget-showcase': 'core/widget'
}

const getNice2Module = packageName => {
  const customer = getCustomerOfPackage(packageName)
  if (customer) {
    return `customer/${customer}`
  }
  return packageToNice2ModuleMap[packageName] || defaultModule
}

const copyPackage = () => {
  const packageName = argv.package
  if (!packageName || !packageExists(packageName)) {
    logger.error('Please select a valid package with --package={PACKAGE_NAME} parameter.')
    process.exit(1)
  }

  const packageDir = getPackageDirectory(packageName)
  const nice2Module = getNice2Module(packageName)
  const nice2Folder = getNice2Folder()
  logger.info(`Copy package '${packageName}' to nice2 into '${nice2Module}' inside '${nice2Folder}'`)

  const sourceDir = `${packageDir}/dist/*`
  const targetDir = `${nice2Folder}/${nice2Module}/resources/resources/webapp/node_modules/tocco-${packageName}/dist`

  exec(`mkdir -p ${targetDir} && cp -r ${sourceDir} ${targetDir}`, (error, stdout, stderr) => {
    if (error) {
      logger.error(error)
    }

    if (stderr) {
      logger.error(stderr)
    }
    if (stdout) {
      logger.info(stdout)
    }

    logger.success(
      // eslint-disable-next-line max-len
      `Copied '${packageDir}/dist/*' to '${nice2Folder}/${nice2Module}/resources/resources/webapp/node_modules/tocco-${packageName}/dist'`
    )
  })
}

copyPackage()
