const path = require('path')

function createJestConfig(packageDirname) {
  const packageName = path.basename(packageDirname)

  const rootDir = path.join(__dirname, '..')
  const relativePackageDir = path.relative(rootDir, packageDirname)

  const srcPath = relativePackageDir ? `/${relativePackageDir}/src/` : '/packages/'

  return {
    rootDir,
    setupFilesAfterEnv: ['<rootDir>/build/jest-setup.js'],
    transformIgnorePatterns: ['node_modules/(?!redux-form|lodash-es|redux-saga|uuid)'],
    moduleNameMapper: {
      '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
        '<rootDir>/__mocks__/fileMock.js',
      '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js'
    },
    globals: {
      __DEV__: false,
      __BACKEND_URL__: ''
    },
    verbose: true,
    testTimeout: 10000,
    cacheDirectory: '.jestcache',
    testEnvironment: 'jsdom',
    testEnvironmentOptions: {
      // don't load "browser" field => able to run @fullcalendar tests
      // https://github.com/fullcalendar/fullcalendar/issues/7113#issuecomment-1384696198
      customExportConditions: []
    },
    displayName: packageName,
    testMatch: [
      `<rootDir>${srcPath}**/*.spec.{js,jsx}`,
      `<rootDir>build/**/*.spec.{js,jsx}`,
      `<rootDir>scripts/**/*.spec.{js,jsx}`
    ],
    modulePathIgnorePatterns: ['<rootDir>/plop/'],
    collectCoverage: false,
    coverageReporters: ['json', 'lcov'],
    collectCoverageFrom: [`<rootDir>${srcPath}**/*.js`, `!<rootDir>${srcPath}**/main.js`],
    coverageDirectory: `<rootDir>${srcPath}coverage/`
  }
}

module.exports = createJestConfig
