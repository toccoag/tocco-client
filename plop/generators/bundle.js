import {getRootDir} from '../utils'

export default {
  description: 'Create new bundle (e.g. customer bundle such as customer-bundle)',
  prompts: [
    {
      type: 'input',
      name: 'package',
      message: 'Package Name (e.g. customer-bundle)',
      validate: value => (value ? true : 'package name is required')
    },
    {
      type: 'input',
      name: 'folder',
      message: 'Folder (bundles, customers/<customer>/bundles)',
      validate: value => {
        if (!value) {
          return 'folder name is required'
        }
        if (value !== 'bundles' && !value.match(/^customers\/([^/]*)\/bundles$/)) {
          return 'folder name has to be either `bundles` or `customers/<customer>/bundles`'
        }
        return true
      }
    }
  ],
  actions: data => {
    const actions = []

    const baseTemplateFolder = './plop/templates/bundle'
    const targetFolder = 'packages/{{folder}}/{{kebabCase package}}'

    const rootDir = getRootDir(data.folder)

    const files = ['changelog.md', 'package.json', '.npmignore', 'build/webpack.js']

    files.forEach(file => {
      actions.push({
        type: 'add',
        path: `${targetFolder}/${file}`,
        templateFile: `${baseTemplateFolder}/${file}`,
        data: {rootDir}
      })
    })

    return actions
  }
}
