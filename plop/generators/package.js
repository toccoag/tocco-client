import {getRootDir} from '../utils'

export default {
  description: 'Create a package in the mono-repo',
  prompts: [
    {
      type: 'input',
      name: 'package',
      message: 'Package Name (e.g. address-update or <customer>-address-update)',
      validate: value => (value ? true : 'package name is required')
    },
    {
      type: 'input',
      name: 'folder',
      message: 'Folder (widgets, actions, apps, core, customers/<customer>/actions, customers/<customer>/widgets)',
      validate: value => {
        if (!value) {
          return 'folder name is required'
        }
        if (
          !['widgets', 'actions', 'apps', 'core'].includes(value) &&
          !value.match(/^customers\/([^/]*)\/actions$/) &&
          !value.match(/^customers\/([^/]*)\/widgets$/)
        ) {
          // eslint-disable-next-line max-len
          return 'folder name has to be either `widgets`, `actions`, `apps`, `core` or `customers/<customer>/actions` or `customers/<customer>/widgets`'
        }
        return true
      }
    },
    {
      type: 'rawlist',
      name: 'released',
      message: 'Is package released? (default: false)',
      choices: ['false', 'true']
    }
  ],
  actions: data => {
    const actions = []

    const baseTemplateFolder = './plop/templates/package'
    const targetFolder = 'packages/{{folder}}/{{kebabCase package}}'

    const rootDir = getRootDir(data.folder)

    const files = ['package.json', 'jest.config.js', '/src/main.js', '/src/modules/reducers.js', '/src/dev/input.json']

    if (data.released === 'true') {
      files.push('.npmignore')
      files.push('changelog.md')
      // if package is released add license to package.json which is showed on the npm website
      data.licenseLine = '\n  "license": "AGPL-3.0+",'
    } else {
      // if package is not released set "private": true in package.json
      data.privateLine = '\n  "private": true,'
    }

    files.forEach(file => {
      actions.push({
        type: 'add',
        path: `${targetFolder}/${file}`,
        templateFile: `${baseTemplateFolder}/${file}`,
        data: {rootDir}
      })
    })

    return actions
  }
}
