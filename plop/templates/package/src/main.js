import {appFactory, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import reducers, {sagas} from './modules/reducers'

const packageName = '{{kebabCase package}}'

const initApp = (id, input, events, publicPath) => {
  const content = <div>{{titleCase package}}</div>

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const {{properCase package}}App = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

{{properCase package}}App.propTypes = {
  appContext: appContext.propTypes.isRequired,
  selection: selection.propType.isRequired
}

export default {{properCase package}}App
