import {getAllCustomers, getAllPackages, getAllPackagesInDirectory} from '../build/lib/packages'

const isNotBundle = directory => !directory.endsWith('bundles')

export const prompts = {
  appChoices: getAllPackages(isNotBundle).sort(),
  bundleChoices: [
    ...getAllPackagesInDirectory('bundles'),
    ...getAllCustomers()
      .map(customer => getAllPackagesInDirectory(`customers/${customer}/bundles`))
      .reduce((acc, arr) => [...acc, ...arr], [])
  ]
}

export const getRootDir = folder => {
  const pathDepth = folder.match(/\//g)?.length
  const numberOfSubfolders = (pathDepth || 0) + 3
  const rootDir = [...Array(numberOfSubfolders).keys()].map(() => '../').join('')
  return rootDir
}
