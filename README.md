# Tocco Client
[![Coverage](https://sonar.tocco.ch/api/project_badges/measure?project=ch.tocco.nice2%3Aclient&metric=coverage&token=sqb_a84bcea56cba601970c1706c7667d2c2a0f710a0)](https://sonar.tocco.ch/dashboard?id=ch.tocco.nice2%3Aclient)
[![Documentation Status](https://img.shields.io/static/v1?label=docs&message=Sphinx&color=blue&logoColor=white&logo=readthedocs)](https://docs.tocco.ch/framework/client/)
[![Storybook](https://raw.githubusercontent.com/storybooks/brand/master/badge/badge-storybook.svg?sanitize=true)](https://toccoag.gitlab.io/tocco-storybook/)

This repository contains the web client for the [Tocco Business Framework](https://www.tocco.ch).

This project is based on following technologies, tools and libraries:
* [React](https://facebook.github.io/react/)
* [Redux](https://github.com/reactjs/redux)
* [redux-saga](https://github.com/yelouafi/redux-saga)
* [npm](https://www.npmjs.com/)
* [webpack](https://webpack.github.io/)
* [Storybook](https://storybook.js.org/)

Initial project structure is based on:
https://github.com/davezuko/react-redux-starter-kit.

## Documentation
How to setup the project, to contribute and much more is documented in the [Read the docs Client Documentation](https://docs.tocco.ch/framework/client/index.html)

Since this project heavily uses Redux and Sagas, you should be aware of it's concepts and also ES6.
A good starting point can be found in these docs:
* https://redux.js.org/
* https://medium.com/sons-of-javascript/javascript-an-introduction-to-es6-1819d0d89a0f
* https://davidwalsh.name/es6-generators
