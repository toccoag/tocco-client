/* eslint-disable no-console */
const fetch = require('isomorphic-fetch')

const {activateWidget} = require('./database')

const getPrimaryKeyFromLocation = location => location.substring(location.lastIndexOf('/') + 1)
const getPrimaryKeyFromResponse = response => {
  const location = response.headers.get('location')
  return getPrimaryKeyFromLocation(location)
}

const getHeaders = config => {
  const headers = new Headers()
  headers.set('Content-Type', 'application/json')
  headers.set('Authorization', 'Basic ' + Buffer.from(config.env.USER + ':' + config.env.API_KEY).toString('base64'))

  return headers
}

const addSearchFilter = async (config, data) => {
  const body = JSON.stringify(data)
  const options = {
    method: 'POST',
    body,
    headers: getHeaders(config)
  }

  console.log(`add serach filter`, body)

  const response = await fetch(`${config.baseUrl}/nice2/rest/client/searchfilters`, options)
  await throwIfResponseFailed(config, response, `cannot add search filter`)
  const responseData = await response.json()
  console.log(`added search filter`, response.status)

  return [responseData, response]
}

const getEntity = async (config, entity, key, paths) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({paths}),
    headers: getHeaders(config)
  }

  console.log(`get entity '${entity}'`, paths)
  const response = await fetch(
    `${config.baseUrl}/nice2/rest/entities/2.0/${entity}/${key}?_omitLinks=true&_useSelectors=true`,
    options
  )
  const data = await response.json()
  return data
}

const getEntityVersion = async (config, entity, key) => {
  const options = {
    method: 'GET',
    headers: getHeaders(config)
  }

  console.log(`get entity-version '${entity}':${key}`)
  const response = await fetch(
    `${config.baseUrl}/nice2/rest/entities/2.0/${entity}/${key}?_omitLinks=true&_useSelectors=true`,
    options
  )
  const json = await response.json()
  return json.version
}

const getEntityKeys = async (config, entity, query) => {
  const options = {
    method: 'GET',
    headers: getHeaders(config)
  }

  console.log(`get entity '${entity}' keys`)
  const response = await fetch(
    `${config.baseUrl}/nice2/rest/entities/2.0/${entity}?paths=pk&_omitLinks=true&_useSelectors=true&_where=${query}`,
    options
  )
  const json = await response.json()
  return json.data.map(({key}) => key)
}

const getEntityKey = async (config, entity, query) => {
  const keys = await getEntityKeys(config, entity, query)
  return keys[0]
}

const patchEntity = async (config, entity, key, data) => {
  const version = await getEntityVersion(config, entity, key)
  data.version = version

  const body = JSON.stringify(data)
  const options = {
    method: 'PATCH',
    body,
    headers: getHeaders(config)
  }

  console.log(`patch entity '${entity}'`, body)

  const response = await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/${entity}/${key}`, options)
  await throwIfResponseFailed(config, response, `cannot patch entity '${entity}'`)
  console.log(`patched entity '${entity}'`, response.status)
  return {response}
}

const setPreferences = async (config, preferences) => {
  const body = JSON.stringify(preferences)
  const options = {
    method: 'PATCH',
    body,
    headers: getHeaders(config)
  }

  console.log(`set preferences`, body)

  const response = await fetch(`${config.baseUrl}/nice2/rest/client/preferences`, options)
  return {response}
}

const addEntity = async (config, entity, data) => {
  const body = JSON.stringify(data)
  const options = {
    method: 'POST',
    body,
    headers: getHeaders(config)
  }

  console.log(`add entity '${entity}'`, body)

  const response = await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/${entity}`, options)
  const location = response.headers.get('location')
  await throwIfResponseFailed(config, response, `cannot add entity '${entity}'`)
  const pk = getPrimaryKeyFromLocation(location)
  console.log(`added entity '${entity}' (pk=${pk})`, response.status)

  return [{pk, ...data.paths}, response]
}

const setPassword = async (config, principalPk, oldPassword, newPassword) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({oldPassword, newPassword}),
    headers: getHeaders(config)
  }

  console.log(`set password for principal '${principalPk}'`)

  const response = await fetch(`${config.baseUrl}/nice2/rest/principals/${principalPk}/password-update`, options)
  await throwIfResponseFailed(config, response, `cannot set password for principal '${principalPk}'`)
  console.log(`set password for principal '${principalPk}'`, response.status)
}

const setPasswordForUser = async (config, userPk, password) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({
      limit: 1,
      sort: 'update_timestamp desc',
      offset: 0,
      paths: ['pk', 'username', 'relUser'],
      where: `relUser.pk == ${userPk}`
    }),
    headers: getHeaders(config)
  }

  const response = await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Principal/search?_omitLinks=true`, options)
  const responseData = await response.json()
  const principal = responseData.data[0]
  await setPassword(config, principal.key, '', password)
  return [{username: principal.paths.username.value.username, pk: principal.key, password}]
}

const addWidgetDomain = async (config, domain, businessUnit) => {
  const options = {
    method: 'POST',
    body: JSON.stringify({
      limit: 1,
      sort: 'update_timestamp desc',
      offset: 0,
      paths: ['domain'],
      where: `domain == "${domain}"`
    }),
    headers: getHeaders(config)
  }

  const response = await fetch(
    `${config.baseUrl}/nice2/rest/entities/2.0/Widget_domain/search?_omitLinks=true`,
    options
  )
  const responseData = await response.json()
  if (responseData.data.length === 1) {
    return [{pk: responseData.data[0].key, ...responseData.data[0].paths}, responseData]
  }

  return await addEntity(config, 'Widget_domain', {
    model: 'Widget_domain',
    paths: {
      relBusiness_unit: businessUnit,
      domain,
      subdomain: 'tocco'
    }
  })
}
/**
 * Adds domain via REST API.
 * @param {object} config Cypress config
 * @param {string} label domain label
 * @param {string} domainType unique id of domain type
 * @param {string} correspondenceLanguage unique id of correspondence language
 * @returns created domain
 */
const addDomain = (config, label, domainType, correspondenceLanguage) =>
  addEntity(config, 'Domain', {
    model: 'Domain',
    paths: {
      label,
      relDomain_type: {uniqueId: domainType},
      relCorrespondence_language: {uniqueId: correspondenceLanguage}
    }
  })

/**
 * Adds folder to domain via REST API.
 * @param {object} config Cypress config
 * @param {string} label folder label
 * @param {string} domain primary key of domain to which folder is added
 * @returns created folder
 */
const addFolderToDomain = (config, label, domain) =>
  addEntity(config, 'Folder', {
    model: 'Folder',
    paths: {
      label,
      relDomain: {key: domain}
    }
  })

/**
 * Adds folder to parent folder via REST API.
 * @param {object} config Cypress config
 * @param {string} label folder label
 * @param {string} parentFolder primary key of parent folder
 * @returns created folder
 */
const addFolderToFolder = (config, label, parentFolder) =>
  addEntity(config, 'Folder', {
    model: 'Folder',
    paths: {
      label,
      relFolder: {key: parentFolder}
    }
  })

/**
 * Adds new widget-configuration via REST API.
 * @param {object} config Cypress config
 * @param {string} uniqueId Widget unique id - used as widget key
 * @param {string} label Widget label
 * @param {string} businessUnit `relBusiness_unit` for Widget_domain
 * @param {object} widget `relWidget` data (see: http://localhost:8080/tocco/e/Widget/list)
 * @param {object} data Data of the specific widget config
 * @returns widgetConfig
 */
const addWidget = async (config, uniqueId, label, businessUnit, widget, data) => {
  const [widgetDomain] = await addWidgetDomain(config, 'localhost', businessUnit)
  const [widgetConfig] = await addEntity(config, 'Widget_config', {
    model: 'Widget_config',
    paths: {
      unique_id: uniqueId,
      label,
      relWidget: widget,
      relWidget_domain: {key: widgetDomain.pk},
      relCorrespondence_language: {uniqueId: 'de'}
    }
  })

  const [{pk: specificWidgetConfigPK}] = await addEntity(config, data.model, data)

  const options = {
    method: 'PUT',
    body: JSON.stringify({entityName: data.model, key: specificWidgetConfigPK}),
    headers: getHeaders(config)
  }

  await fetch(`${config.baseUrl}/nice2/rest/widget/configs/${widgetConfig.pk}/specific-config`, options)

  return widgetConfig
}

const addLoginWidget = (
  config,
  {uniqueId = 'login', label = 'Login', redirectUrl = 'http://localhost:3000/', redirectWidgetConfigKey} = {}
) =>
  addWidget(
    config,
    uniqueId,
    label,
    {uniqueId: 'test1'},
    {uniqueId: 'login'},
    {
      model: 'Login_widget_config',
      paths: {
        redirect_url: redirectWidgetConfigKey ? `/?key=${redirectWidgetConfigKey}` : redirectUrl
      }
    }
  )

const addUserMenuWidget = async (config, {uniqueId = 'user_menu', label = 'User Menu'} = {}) => {
  await activateWidget(config, 'user_menu')
  return addWidget(
    config,
    uniqueId,
    label,
    {uniqueId: 'test1'},
    {uniqueId: 'user_menu'},
    {
      model: 'User_menu_widget_config',
      paths: {}
    }
  )
}

const throwIfResponseFailed = async (config, response, message) => {
  if (response.status >= 400) {
    if (config.env.DEV) {
      console.error(await response.text())
    }
    console.error(message, response.status)
    throw new Error(`${message}, ${response.status}`)
  }
}

module.exports = {
  getHeaders,
  addSearchFilter,
  patchEntity,
  addEntity,
  getEntity,
  getEntityKeys,
  getEntityKey,
  setPreferences,
  addLoginWidget,
  addUserMenuWidget,
  addWidget,
  addDomain,
  addFolderToDomain,
  addFolderToFolder,
  setPasswordForUser,
  getPrimaryKeyFromResponse
}
