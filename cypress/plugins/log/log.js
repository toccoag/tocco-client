/* eslint-disable no-console */
const fs = require('fs')
const path = require('path')

const {format} = require('date-fns')
const _flatten = require('lodash/flatten')

const EXPORT_PATH = 'cypress/exports'
const EXPORT_FILE_NAME = 'testcases.md'

const EXPORT_FILE = path.join(EXPORT_PATH, EXPORT_FILE_NAME)

const getResultPlaceholder = title => `$$result_${title.join('').toLowerCase()}$$`

let data = ''

const createFile = () => {
  try {
    if (!fs.existsSync(EXPORT_PATH)) {
      fs.mkdirSync(EXPORT_PATH)
    } else if (fs.existsSync(EXPORT_FILE)) {
      fs.unlinkSync(EXPORT_FILE)
    }

    fs.writeFileSync(EXPORT_FILE, data)
  } catch (error) {
    console.log(error)
  }
}

const prepare = () => {
  data = ''
}
const appendToData = text => {
  data += text
}
const replaceData = text => {
  data = text
}

const logBeforeRun = specs => {
  prepare()

  const header = `# Cypress Testfälle

Generiert am: ${format(new Date(), 'dd.MM.yyyy HH:mm')}

Anzahl Specs in diesem Run: ${specs.length}\n`
  appendToData(header)
}

const logSpec = ({cypressTest, spec}) => {
  const testTitle = cypressTest.titlePath[0]
  const specHeader = `\n## ${testTitle}\n\nTestdatei: ${spec.relative}\n\n`
  appendToData(specHeader)

  return true
}
const logTest = ({cypressTest}) => {
  const testHeader = `\n### 🧪 ${cypressTest.title}\n\nResultat: ${getResultPlaceholder(cypressTest.titlePath)}\n\n`
  appendToData(testHeader)

  return true
}
const logStep = message => {
  const step = `  1. ${message}\n`
  appendToData(step)

  return true
}

const generateResult = (runs, totals) => {
  /* eslint-disable max-len */
  let result = `\n# Cypress Resultat

| Spec | Test | Passing | Failing | Pending | Skipped |
|---|:---:|:---:|:---:|:---:|:---:|
`
  result += runs
    .map(
      r =>
        `| ${r.spec.name} | ${r.stats.tests} | ${r.stats.passes} | ${r.stats.failures} | ${r.stats.pending} | ${r.stats.skipped} |`
    )
    .join('\n')

  result += `\n| Total | ${totals.totalTests} | ${totals.totalPassed} | ${totals.totalFailed} | ${totals.totalPending} | ${totals.totalSkipped} |`

  return result
  /* eslint-enable max-len */
}

const replaceResultPlaceholders = runs => {
  const testRuns = _flatten(runs.map(run => run.tests))
  replaceData(
    testRuns.reduce(
      (acc, testRun) => acc.replace(getResultPlaceholder(testRun.title), testRun.state === 'passed' ? '✅' : '❌'),
      data
    )
  )
}

const logAfterRun = (runs, totals) => {
  replaceResultPlaceholders(runs)

  const results = generateResult(runs, totals)
  appendToData(results)

  createFile(data)
}

module.exports = {logBeforeRun, logSpec, logTest, logStep, logAfterRun}
