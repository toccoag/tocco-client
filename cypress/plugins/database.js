/* eslint-disable no-console */
const {spawn} = require('child_process')

const {Client} = require('pg')

const setEnv = (config, key) => (config.env[key] ? {[key]: config.env[key]} : {})

const resetDatabase = config => {
  const promise = new Promise((resolve, reject) => {
    const env = {
      ...setEnv(config, 'BACKEND_URL'),
      ...setEnv(config, 'HIBERNATE_MAIN_SERVERNAME'),
      ...setEnv(config, 'HIBERNATE_MAIN_DATABASENAME'),
      ...setEnv(config, 'HIBERNATE_MAIN_USER'),
      ...setEnv(config, 'HIBERNATE_MAIN_PASSWORD'),
      ...setEnv(config, 'POSTGRES_USER'),
      ...setEnv(config, 'POSTGRES_PASSWORD')
    }

    const child = spawn('yarn db:force-restore', {shell: true, env: {...process.env, ...env}})

    child.stdout.pipe(process.stdout)
    child.stderr.pipe(process.stderr)

    child.on('error', error => {
      console.error(`error: ${error.message}`)
      console.error(error)
      reject(error)
    })

    child.on('close', code => {
      console.log(`yarn db:force-restore process exited with code ${code}`)
      resolve(code)
    })
  })
  return promise
}

const setupDashboard = async config => {
  const client = new Client({
    user: config.env.HIBERNATE_MAIN_USER || 'nice',
    host: config.env.HIBERNATE_MAIN_SERVERNAME || 'localhost',
    database: config.env.HIBERNATE_MAIN_DATABASENAME || 'test_cypress',
    password: config.env.HIBERNATE_MAIN_PASSWORD || 'nice',
    port: 5432
  })
  await client.connect()

  const text = `UPDATE nice_infobox SET active = 'f'
  WHERE unique_id IN 
  ('newapplications', 'next_birthdays', 'open_todos', 
  'newmemberships', 'newregistrations', 'prospective_reservations', 'newlicences')`

  try {
    await client.query(text)
    console.log('Tocco dashboard successfully set up.')
  } catch (err) {
    console.error(`Couldn't setup Tocco dashboard.`, err, err.stack)
  }

  await client.end()
}

const activateWidget = async (config, uniqueId) => {
  const client = new Client({
    user: config.env.HIBERNATE_MAIN_USER || 'nice',
    host: config.env.HIBERNATE_MAIN_SERVERNAME || 'localhost',
    database: config.env.HIBERNATE_MAIN_DATABASENAME || 'test_cypress',
    password: config.env.HIBERNATE_MAIN_PASSWORD || 'nice',
    port: 5432
  })
  await client.connect()

  const text = `UPDATE nice_widget SET active = 't'
  WHERE unique_id = '${uniqueId}' OR unique_id = 'login'`

  try {
    await client.query(text)
    console.log(`Widget '${uniqueId}' successfully activated.`)
  } catch (err) {
    console.error(`Couldn't activate widget '${uniqueId}'.`, err, err.stack)
  }

  await client.end()
}

module.exports = {
  resetDatabase,
  activateWidget,
  setupDashboard
}
