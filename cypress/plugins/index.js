const {resetDatabase, setupDashboard} = require('./database')
const helpers = require('./helpers')
const {consoleLog, consoleError} = require('./log')
const {logStep, logTest, logBeforeRun, logAfterRun, logSpec} = require('./log/log')
const seeds = require('./seeds')
const {sendResultToSlack} = require('./slack')

const renameKeys = o =>
  Object.keys(o || {}).reduce((acc, key) => Object.assign({}, acc, {[key.replace(/^CYPRESS_/, '')]: o[key]}), {})

const getEnvs = () => {
  const envs = {
    HIBERNATE_MAIN_SERVERNAME: process.env.HIBERNATE_MAIN_SERVERNAME,
    HIBERNATE_MAIN_DATABASENAME: process.env.HIBERNATE_MAIN_DATABASENAME,
    HIBERNATE_MAIN_USER: process.env.HIBERNATE_MAIN_USER,
    HIBERNATE_MAIN_PASSWORD: process.env.HIBERNATE_MAIN_PASSWORD,
    POSTGRES_USER: process.env.POSTGRES_USER
  }
  return Object.keys(envs).reduce(
    (acc, key) => ({...acc, ...(typeof envs[key] !== 'undefined' ? {[key]: envs[key]} : {})}),
    {}
  )
}

module.exports = (on, config) => {
  const envs = require('dotenv').config()
  config.env = Object.assign({}, renameKeys(envs.parsed), config.env, getEnvs())

  on('before:run', details => {
    logBeforeRun(details.specs)
  })
  on('after:run', async details => {
    logAfterRun(details.runs, {
      totalDuration: details.totalDuration,
      totalFailed: details.totalFailed,
      totalPassed: details.totalPassed,
      totalPending: details.totalPending,
      totalSkipped: details.totalSkipped,
      totalSuites: details.totalSuites,
      totalTests: details.totalTests
    })

    await sendResultToSlack(details)
  })

  on('task', {
    'log:spec': data => logSpec(data),
    'log:test': data => logTest(data),
    'log:step': data => logStep(data),
    log: consoleLog,
    'log:error': consoleError,
    'db:empty': async () => {
      await resetDatabase(config)
      // hide all (except one) entity-list infoboxes to not wait too long for dashboard
      await setupDashboard(config)
      return true
    },
    'db:seed:admin': () => seeds.admin.seed(config),
    'db:seed:user': () => seeds.user.seed(config),
    'db:seed:address-update': ({editable = true}) => seeds.addressUpdate.seed(config, editable),
    'db:assert:entity': ({model, pk, data}) => helpers.assertEntity(config, model, pk, data),
    'db:seed:login': () => seeds.login.seed(config),
    'db:seed:dms': () => seeds.dms.seed(config),
    'db:seed:duplicate': () => seeds.duplicate.seed(config),
    'db:seed:finance': () => seeds.finance.seed(config),
    'db:seed:login-role:seed': () => seeds.loginRole.seed(config),
    'db:seed:login-role:automatic-login-role': ({principalKey, roleId}) =>
      seeds.loginRole.setLoginRoleAutomatic(config, principalKey, roleId),
    'db:seed:event-registration': () => seeds.eventRegistration.seed(config),
    'db:seed:form-components': () => seeds.formComponents.seed(config),
    'db:seed:terms-conditions': () => seeds.termsConditions.seed(config),
    'db:seed:terms-conditions:add-checkbox': pk => seeds.termsConditions.addCheckboxForTermsCondition(config, pk),
    'db:seed:resource-calendar:tocrt76': () => seeds.resourceCalendar.tocrt76(config),
    'db:seed:input-nodes': () => seeds.inputNodes.seed(config),
    'db:seed:input': ({inputType, userCount = 1, responsible}) =>
      seeds.input.seed(config, inputType, userCount, responsible),
    'db:seed:input-edit-widget': () => seeds.inputEditWidget.seed(config),
    'db:seed:event-logic-copy': () => seeds.eventLogicCopy.seed(config),
    'db:seed:mailing-list-widget': () => seeds.mailingList.seed(config)
  })

  return config
}
