/* eslint-disable no-console, max-len */

const slackAuthToken = process.env.SLACK_AUTH_TOKEN
const pipelineUrl = process.env.CI_PIPELINE_URL
const sourceBranch = process.env.CI_COMMIT_REF_NAME
const gitlabProjectNamespace = process.env.CI_PROJECT_NAMESPACE
const gitlabProjectPath = process.env.CI_PROJECT_PATH
const gitlabProjectId = process.env.CI_PROJECT_NAME

const sendSlackMessage = async body => {
  const options = {
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${slackAuthToken}`
    },
    method: 'POST',
    body: JSON.stringify(body)
  }

  await fetch('https://slack.com/api/chat.postMessage', options)
}

const sendResultToSlack = async result => {
  if (!slackAuthToken) {
    console.log('Only report to slack when slack url is configured.')
    return
  }

  if (result.totalFailed === 0) {
    console.log('Succesful runs are not reported.')
    return
  }

  try {
    const failedTests = result.runs
      .map(run => run.tests.filter(t => t.state === 'failed').map(test => ({...test, spec: run.spec.fileName})))
      .reduce((acc, tests) => [...acc, ...tests], [])

    const pagesGitlabUrl = `https://${gitlabProjectNamespace}.gitlab.io`
    const baseGitlabUrl = `https://gitlab.com/${gitlabProjectPath}`
    const timestamp = Math.floor(Date.now() / 1000)
    const body = {
      channel: 'C034NTD15PU', // channel id of #ci
      attachments: [
        {
          color: '#951e13',
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `*Failed Cypress Run*\n<${pipelineUrl}|Cypress run> has failed.`
              },
              fields: [
                {
                  type: 'mrkdwn',
                  text: `*Branch*\n<${baseGitlabUrl}/-/tree/${sourceBranch}|${sourceBranch}>`
                },
                {
                  type: 'mrkdwn',
                  text: `*Report*\n<${pagesGitlabUrl}/${gitlabProjectId}|Cypress Report>`
                }
              ]
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `*Summary*\nTotal Tests: ${result.totalTests} (${result.totalPassed} passed :white_check_mark: / *${result.totalFailed} failed* :x:)`
              }
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `*Failed Tests*\n${failedTests.map(test => `\t•\t_${test.spec}_: ${test.title.at(-1)}`).join('\n')}`
              }
            },
            {
              type: 'context',
              elements: [
                {
                  type: 'mrkdwn',
                  text: `${gitlabProjectId} | <!date^${timestamp}^{date_short_pretty} at {time}|${new Date().toLocaleString()}>`
                }
              ]
            }
          ]
        }
      ]
    }

    await sendSlackMessage(body)
    console.log('Sent message to slack')
  } catch (error) {
    console.warn('Could not send failure message to slack', error, error.stack)
  }
}

module.exports = {
  sendResultToSlack
}
