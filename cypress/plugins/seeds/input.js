/* This seed is generated by the Tocco Browser Extension */

const fetch = require('isomorphic-fetch')

const {getHeaders, getEntityKey, getPrimaryKeyFromResponse, patchEntity} = require('../rest')

const createEvent = async config => {
  const eventResponse = await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Event`, {
    method: 'POST',
    body: JSON.stringify({
      model: 'Event',
      paths: {
        relEvent_status: {unique_id: 'open'},
        label: 'input edit event',
        participation_max: 10
      }
    }),
    headers: getHeaders(config)
  })
  return getPrimaryKeyFromResponse(eventResponse)
}

const createUser = async (config, index) => {
  const userResponse = await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/User`, {
    method: 'POST',
    body: JSON.stringify({
      model: 'User',
      paths: {
        firstname: `Vorname ${index}`,
        lastname: `Nachname ${index}`,
        relGender: {version: 1, unique_id: 'female'}
      }
    }),
    headers: getHeaders(config)
  })
  return getPrimaryKeyFromResponse(userResponse)
}

const createRegistration = async (config, eventKey, count) => {
  for (let i = 0; i < count; i++) {
    const userKey = await createUser(config, i)
    await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Registration`, {
      method: 'POST',
      body: JSON.stringify({
        model: 'Registration',
        paths: {
          relRegistration_status: {unique_id: 'booked'},
          relEvent: {key: eventKey},
          relUser: {key: userKey}
        }
      }),
      headers: getHeaders(config)
    })
  }
}

const createInput = async (config, eventKey, inputType, responsible) => {
  const inputNodeKey = await getEntityKey(config, 'Input_node', `short=="${inputType}"`)
  await fetch(`${config.baseUrl}/nice2/rest/qualification/action/connectInputNode`, {
    method: 'POST',
    body: JSON.stringify({
      entity: 'Event',
      selection: {entityName: 'Event', type: 'ID', ids: [eventKey], count: 1},
      formData: {
        model: 'Connect_input_node_action_settings',
        paths: {
          relInput_node: {key: inputNodeKey, model: 'Input_node'},
          date: '2024-06-15'
        }
      },
      formProperties: {}
    }),
    headers: getHeaders(config)
  })
  const inputKey = await getEntityKey(config, 'Input', `relEvent.pk==${eventKey}`)

  if (responsible) {
    await patchEntity(config, 'Input', inputKey, {
      model: 'Input',
      version: 0,
      paths: {
        relResponsible_user: [{key: responsible, model: 'User'}],
        relInput_status: {uniqueId: 'open', model: 'Input_status'}
      }
    })
  }

  if (inputType.endsWith('threshold_value')) {
    await Promise.all([
      await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Threshold_value`, {
        method: 'POST',
        body: JSON.stringify({model: 'Threshold_value', paths: {relInput: {key: inputKey}, value: 20, grade: 2}}),
        headers: getHeaders(config)
      }),
      await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Threshold_value`, {
        method: 'POST',
        body: JSON.stringify({model: 'Threshold_value', paths: {relInput: {key: inputKey}, value: 60, grade: 4}}),
        headers: getHeaders(config)
      }),
      await fetch(`${config.baseUrl}/nice2/rest/entities/2.0/Threshold_value`, {
        method: 'POST',
        body: JSON.stringify({model: 'Threshold_value', paths: {relInput: {key: inputKey}, value: 100, grade: 6}}),
        headers: getHeaders(config)
      })
    ])
  }
  return inputKey
}

const seed = async (config, inputType, userCount, responsible) => {
  const eventKey = await createEvent(config)
  await createRegistration(config, eventKey, userCount)
  return await createInput(config, eventKey, inputType, responsible)
}

module.exports = {seed}
