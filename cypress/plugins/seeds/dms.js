const {addDomain, addFolderToDomain, addFolderToFolder} = require('../rest')

const seed = async config => {
  /*
        DMS structure which is created:

        - Internal DMS
          - Main Folder
            - Sub Folder
      */
  const [domain] = await addDomain(config, 'Internal DMS', 'internal_file_repository', 'de')
  const [mainFolder] = await addFolderToDomain(config, 'Main Folder', domain.pk)
  await addFolderToFolder(config, 'Sub Folder', mainFolder.pk)

  return null
}

module.exports = {seed}
