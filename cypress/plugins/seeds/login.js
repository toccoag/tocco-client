const {activateWidget} = require('../database')
const {addLoginWidget} = require('../rest')

const seed = async config => {
  await activateWidget(config, 'login')
  const {pk} = await addLoginWidget(config)

  return {pk, uniqueId: 'login', label: 'Login'}
}

module.exports = {seed}
