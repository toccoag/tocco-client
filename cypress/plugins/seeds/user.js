const {addEntity} = require('../rest')

const seed = async config => {
  const [user] = await addEntity(config, 'User', {
    model: 'User',
    key: null,
    paths: {
      firstname: 'Hans',
      lastname: 'Müller',
      relGender: {key: '2'}
    }
  })

  return user
}

module.exports = {seed}
