const {addEntity, setPreferences} = require('../rest')

const seed = async config => {
  const [event] = await addEntity(config, 'Event', {
    model: 'Event',
    key: null,
    paths: {
      relEvent_language: {uniqueId: 'de'},
      change_appointment: true,
      minimal_presence: 80,
      relEvent_overbooking: {uniqueId: 'waiting_list'},
      approval_compulsory: false,
      relCost_delimitation_type: {uniqueId: 'none'},
      relStint_auction_status: {uniqueId: 'auction_disabled'},
      relEvent_status: {uniqueId: 'open'},
      relRegistration_propagation_status: {uniqueId: 'automatic'},
      relLecturer_booking_propagation_status: {uniqueId: 'manually'},
      presence_total_relevance: true,
      label: 'test',
      participation_max: 1
    }
  })

  await setPreferences(config, {path: '/nice2/ui/settings', values: {'admin.detail.relationViewCollapsed': true}})

  return event
}

module.exports = {seed}
