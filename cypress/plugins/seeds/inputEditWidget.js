const {activateWidget} = require('../database')
const {addWidget, addEntity, setPasswordForUser, addUserMenuWidget} = require('../rest')

const addInputEditWidget = config =>
  addWidget(
    config,
    'input-edit',
    'Input edit',
    {uniqueId: 'test1'},
    {uniqueId: 'input_edit'},
    {
      model: 'Input_edit_widget_config',
      paths: {}
    }
  )

const seed = async config => {
  await activateWidget(config, 'input_edit')
  const widgetConfig = await addInputEditWidget(config)
  const userMenuWidgetConfig = await addUserMenuWidget(config)

  const [responsibleUser] = await addEntity(config, 'User', {
    model: 'User',
    key: null,
    paths: {
      firstname: 'Responsible',
      lastname: 'User',
      relGender: {uniqueId: 'female'}
    }
  })
  const [responsibleLogin] = await setPasswordForUser(config, responsibleUser.pk, 'R3sp0nsibl3')

  const [notResponsibleUser] = await addEntity(config, 'User', {
    model: 'User',
    key: null,
    paths: {
      firstname: 'Not Responsible',
      lastname: 'User',
      relGender: {uniqueId: 'female'}
    }
  })
  const [notResponsibleLogin] = await setPasswordForUser(config, notResponsibleUser.pk, 'N0t_r3sp0nsibl3')

  return {widgetConfig, userMenuWidgetConfig, responsibleLogin, notResponsibleLogin, responsibleUser}
}

module.exports = {seed}
