const {activateWidget} = require('../database')
const {addWidget, addEntity, setPasswordForUser} = require('../rest')

const addAddressUpdateWidget = (config, uniqueId, label) =>
  addWidget(
    config,
    uniqueId,
    label,
    {uniqueId: 'test1'},
    {uniqueId: 'address_update'},
    {
      model: 'Address_update_widget_config',
      paths: {
        email_notification: true,
        relBusiness_unit: {uniqueId: 'test1'},
        relEmail_template: {uniqueId: 'address_update'},
        relUser_status: {uniqueId: 'check'},
        disable_edit: false
      }
    }
  )

const seed = async (config, editable) => {
  const [user] = await addEntity(config, 'User', {
    model: 'User',
    key: null,
    paths: {
      firstname: 'Susanne',
      lastname: 'Peters',
      relGender: {uniqueId: 'female'}
    }
  })

  const [address] = await addEntity(config, 'Address', {
    model: 'Address',
    paths: {
      relCorrespondence_language: {uniqueId: 'de'},
      relAddress_type: {uniqueId: 'neutral'},
      relCollective_salutation: {uniqueId: 'neutral'},
      relAddress_status: {uniqueId: 'active'},
      relCountry_c: {key: '43'},
      zip_c: '8006',
      city_c: 'Zürich',
      canton: 'ZH',
      admincode2: 'Bezirk Zürich',
      community_identification_nr: '261',
      address_c: 'Schaffhauserstrasse'
    }
  })

  const [addressUser] = await addEntity(config, 'Address_user', {
    model: 'Address_user',
    paths: {relUser: {key: user.pk}, relAddress: {key: address.pk}, publication: true, editable}
  })

  await activateWidget(config, 'address_update')
  const widgetConfig = await addAddressUpdateWidget(config, 'address-update', 'Address Update')

  const [login] = await setPasswordForUser(config, user.pk, 'Susi!1234')

  return {widgetConfig, user, address, addressUser, login}
}

module.exports = {seed}
