const fs = require('fs')
const path = require('path')

// dynamically import/export all seed files inside this folder
module.exports = fs
  .readdirSync(__dirname)
  .filter(f => f !== 'index.js')
  .reduce((acc, file) => ({...acc, [path.parse(file).name]: require('./' + file)}), {})
