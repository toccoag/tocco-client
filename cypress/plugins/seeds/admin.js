const {addEntity, addSearchFilter} = require('../rest')

const addEvent = (config, eventData) =>
  addEntity(config, 'Event', {
    model: 'Event',
    key: null,
    paths: {
      relEvent_language: {uniqueId: 'de'},
      change_appointment: true,
      minimal_presence: 80,
      relEvent_overbooking: {uniqueId: 'waiting_list'},
      approval_compulsory: false,
      relCost_delimitation_type: {uniqueId: 'none'},
      relStint_auction_status: {uniqueId: 'auction_disabled'},
      relEvent_status: {uniqueId: 'open'},
      relRegistration_propagation_status: {uniqueId: 'automatic'},
      relLecturer_booking_propagation_status: {uniqueId: 'manually'},
      presence_total_relevance: true,
      label: 'test',
      abbreviation: 'test',
      participation_max: 1,
      ...eventData
    }
  })

const seed = async config => {
  const [event1] = await addEvent(config, {
    label: 'Event1',
    abbreviation: 'evt1'
  })
  const [event2] = await addEvent(config, {
    label: 'Event2',
    abbreviation: 'evt2',
    relEvent_status: {uniqueId: 'request'}
  })
  const [event3] = await addEvent(config, {
    label: 'Event3',
    abbreviation: 'evt3',
    relEvent_status: {uniqueId: 'cancelled'}
  })

  const [filter] = await addSearchFilter(config, {
    name: 'Angefragte Veranstaltungen',
    query: 'relEvent_status.unique_id == "request"',
    entityName: 'Event',
    order: ''
  })

  return {events: [event1, event2, event3], filter}
}

module.exports = {seed}
