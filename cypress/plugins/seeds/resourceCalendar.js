const {startOfMonth, addMonths, setHours, setMinutes} = require('date-fns')

const {addEntity} = require('../rest')

const tocrt76 = async config => {
  const [user] = await addEntity(config, 'User', {
    model: 'User',
    key: null,
    paths: {
      firstname: 'Hans',
      lastname: 'Müller',
      relGender: {uniqueId: 'male'}
    }
  })

  const [building] = await addEntity(config, 'Building', {
    model: 'Building',
    key: null,
    paths: {
      label: 'Schulhaus'
    }
  })

  const [room] = await addEntity(config, 'Room', {
    model: 'Room',
    key: null,
    paths: {
      active: true,
      abbreviation: 'raum',
      relBuilding: {key: building.pk, version: 0}
    }
  })

  const [event] = await addEntity(config, 'Event', {
    model: 'Event',
    key: null,
    paths: {
      relEvent_language: {uniqueId: 'de'},
      change_appointment: true,
      minimal_presence: 80,
      relEvent_overbooking: {uniqueId: 'waiting_list'},
      approval_compulsory: false,
      relCost_delimitation_type: {uniqueId: 'none'},
      relStint_auction_status: {uniqueId: 'auction_disabled'},
      relEvent_status: {uniqueId: 'open'},
      relRegistration_propagation_status: {uniqueId: 'automatic'},
      relLecturer_booking_propagation_status: {uniqueId: 'automatic'},
      presence_total_relevance: true,
      label: 'Deutsch Kurs',
      participation_max: 10,
      first_course_date: null,
      last_course_date: null,
      duration_days: 10,
      education_amount: 7200000
    }
  })

  const [lecturerBooking] = await addEntity(config, 'Lecturer_booking', {
    model: 'Lecturer_booking',
    key: null,
    paths: {
      relInvoice_status: {uniqueId: 'open'},
      evaluate_wage_scale: true,
      relSalary_type: {uniqueId: 'overtime'},
      relEvent: {key: event.pk},
      relUser: {key: user.pk},
      relLecturer_status: {uniqueId: 'registered'}
    }
  })

  const setTime = (date, hours, minutes) => setMinutes(setHours(date, hours), minutes)
  const firstOfNextMonth = addMonths(startOfMonth(new Date()), 1)
  const [reservation] = await addEntity(config, 'Reservation', {
    model: 'Reservation',
    key: null,
    paths: {
      relUser: {key: user.pk},
      relEvent: {key: event.pk},
      date_from: setTime(firstOfNextMonth, 15, 0).toISOString(),
      date_till: setTime(firstOfNextMonth, 16, 0).toISOString(),
      facility_date_from: setTime(firstOfNextMonth, 14, 30).toISOString(),
      facility_date_till: setTime(firstOfNextMonth, 16, 30).toISOString(),
      relRoom: [{key: room.pk}],
      relResponsible_user: {key: user.pk}
    }
  })

  return {
    user,
    building,
    room,
    event,
    lecturerBooking,
    reservation
  }
}

module.exports = {tocrt76}
