const _flatten = require('lodash/flatten')
const _isArray = require('lodash/isArray')
const _isEqual = require('lodash/isEqual')

const {getEntity} = require('./rest')

const getValue = (entity, [pathPart, ...pathParts]) => {
  const value = entity[pathPart].value
  if (pathParts.length === 0) {
    return value
  } else {
    if (value === null) {
      return null
    } else if (_isArray(value)) {
      return _flatten(value.map(({paths}) => getValue(paths, pathParts)))
        .filter(v => !!v)
        .toSorted()
    } else {
      return getValue(value.paths, pathParts)
    }
  }
}

const assertEntity = async (config, model, pk, expectedData) => {
  const paths = Object.keys(expectedData)
  const actualEntity = await getEntity(config, model, pk, paths)
  paths.forEach(path => {
    const pathParts = path.split('.')
    const actualValue = getValue(actualEntity.paths, pathParts)
    const expectedValue = expectedData[path]
    if (!_isEqual(actualValue, expectedValue)) {
      throw new Error(`Value '${model}.${path}' not equal: expected '${expectedValue}', actual: '${actualValue}'`)
    }
  })
  return true
}

module.exports = {assertEntity}
