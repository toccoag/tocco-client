describe('DMS', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.task('db:seed:dms')
    cy.login()
  })

  it(`DMS: Navigation und Suche`, () => {
    cy.visit('/tocco/docs')
    cy.waitForListLoaded()

    // list domains
    cy.logStep('DMS öffnen und Liste der Domains anzeigen lassen')
    cy.listRowContains('Internal DMS')
    cy.breadcrumbsIs(['Home', 'Dokument'])
    cy.getByAttr('btn-action-createcopy').should('exist')
    cy.getByAttr('btn-action-dms-delete').should('exist')
    cy.getByAttr('btn-action-dms-move').should('not.exist')
    cy.getByAttr('btn-action-zip_export').should('exist')

    cy.logStep('In ein Domain navigieren')
    cy.contains('Internal DMS').click()

    // list items of domain 'Internal DMS'
    cy.logStep('Domaininhalte sollten dargestellt werden')
    cy.listRowContains('Main Folder')
    cy.url().should('include', 'docs/domain/')
    cy.breadcrumbsIs(['Home', 'Dokument', 'Internal DMS'])
    cy.getByAttr('btn-action-createcopy').should('exist')
    cy.getByAttr('btn-action-dms-delete').should('exist')
    cy.getByAttr('btn-action-dms-move').should('exist')
    cy.getByAttr('btn-action-zip_export').should('exist')

    cy.logStep('In ein Ordner navigieren')
    cy.contains('Main Folder').click()

    // list items of folder 'Main Folder'
    cy.logStep('Ordnerinhalte sollten dargestellt werden')
    cy.listRowContains('Sub Folder')
    cy.url().should('include', 'docs/folder/')
    cy.breadcrumbsIs(['Home', 'Dokument', 'Internal DMS', 'Main Folder'])

    cy.logStep('In einen leeren Unterordner navigieren')
    cy.contains('Sub Folder').click()

    // list items of folder 'Sub Folder'
    cy.logStep('Leerer Unterordner sollte dargestellt werden')
    cy.contains('Es wurden keine passenden Einträge gefunden.')
    cy.breadcrumbsIs(['Home', 'Dokument', 'Internal DMS', 'Main Folder', 'Sub Folder'])

    // click on first item in breadcrumb
    cy.logStep('Über die Breadcrumbs auf den ausgewählten Domain navigieren')
    cy.getByAttr('breadcrumb-item').eq(1).click()
    cy.breadcrumbsIs(['Home', 'Dokument'])
    cy.listRowContains('Internal DMS')

    // use back button in browser
    cy.logStep('Über den Browser-Zurück-Button wieder in den leeren Unterordner navigieren')
    cy.go('back')
    cy.contains('Es wurden keine passenden Einträge gefunden.')
    cy.breadcrumbsIs(['Home', 'Dokument', 'Internal DMS', 'Main Folder', 'Sub Folder'])
  })
})
