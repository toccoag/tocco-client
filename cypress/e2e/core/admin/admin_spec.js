describe('Admin', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  describe('Navigation, Suche und Erstellen', () => {
    it('Suchfilter und TQL Abfrage', () => {
      cy.task('db:seed:admin').then(() => {
        cy.login()
        cy.visit('/tocco')
        cy.waitForDashboardLoaded()

        cy.logStep('Menu mit Shortcut öffnen')
        cy.get('body').type('{ctrl}{alt}m')

        cy.logStep('Menu-Eintrag "Veranstaltung" über Menü-Suche und Pfeiltasten auswählen')
        cy.getByAttr('admin-nav', 'input-search-box').should('be.focused').clear().type('Veranstaltung')
        cy.getByAttr('admin-menuitem-address').should('not.exist')
        cy.getByAttr('admin-menuitem-event').should('exist')
        cy.get('body').type('{downarrow}')
        cy.get('[data-cy=admin-menuitem-event] a[data-quick-navigation]').should('be.focused')

        cy.logStep('Auf "Veranstaltung" navigieren')
        cy.focused().click()

        cy.url().should('include', 'e/Event/list')

        // be able to use search filters
        cy.logStep('Suchfilter hinzufügen und entfernen')
        cy.getByAttr('tbl-list-row').should('have.length', 2)
        cy.getByAttr('btn-horizontal').contains('Aktive Veranstaltungen').click()
        cy.getByAttr('tbl-list-row').should('have.length', 3)
        cy.getByAttr('btn-horizontal').contains('Aktive Veranstaltungen').click()
        cy.get('[data-cy=btn-horizontal]:nth-child(4) [data-cy=btn-add-remove]').click()
        cy.getByAttr('tbl-list-row').should('have.length', 1)
        cy.getByAttr('tbl-cell-event_nr').should('have.text', '2')
        cy.getByAttr('tbl-cell-relEvent_status').should('have.text', 'Anfrage')
        cy.get('[data-cy=btn-horizontal]:nth-child(4) [data-cy=btn-add-remove]').click()
        cy.getByAttr('tbl-list-row').should('have.length', 2)

        // open search as query
        cy.logStep('Aktuelle Suche als Abfrage öffnen')
        cy.getByAttr('btn-search-menu').click()
        cy.getByAttr('menuitem-open-search-as-tql-query').click()
        cy.get('#input-tql-query').hasCodeValue(
          // eslint-disable-next-line max-len
          'relEvent_status.unique_id != "archived" and relEvent_status.unique_id != "cancelled" order by update_timestamp desc'
        )

        // change tql query
        cy.logStep('TQL anpassen - Fehler sollten gehandlet werden')
        cy.get('#input-tql-query').clearCodeValue()
        cy.get('#input-tql-query').typeCodeValue('relEvent_status.unique_id == "cancelled";')

        cy.getByAttr('query-error-message').should('exist')
        cy.get('#input-tql-query').clearCodeValue()
        cy.get('#input-tql-query').typeCodeValue('relEvent_status.unique_id == "cancelled"')

        cy.getByAttr('query-error-message').should('not.exist')
        cy.getByAttr('btn-search').click()
        cy.getByAttr('tbl-list-row').should('have.length', 1)
        cy.getByAttr('tbl-cell-relEvent_status').should('have.text', 'Abgesagt')

        // save tql query as filter
        cy.logStep('TQL Abfrage als Filter speichern')
        cy.getByAttr('btn-query-menu').click()
        cy.getByAttr('menuitem-save-tql-query-as-search-filter').click()
        cy.get('#input-simpleForm-label').type('Abgesagte Veranstaltungen')
        cy.getByAttr('btn-simpleForm-submit').click()
        cy.get('#input-tql-query').should('not.exist')
        cy.getByAttr('btn-horizontal').contains('Abgesagte Veranstaltungen').should('exist')
        cy.get('[data-cy=btn-horizontal]:nth-child(5) [data-cy=btn-add-remove]').click()
        cy.contains('Es wurden keine passenden Einträge gefunden.').should('exist')

        cy.get('[data-cy=btn-horizontal]:nth-child(4) [data-cy=btn-add-remove]').click()
        cy.get('[data-cy=btn-horizontal]:nth-child(5) [data-cy=btn-add-remove]').click()

        cy.logStep('Navigation ins Detail')
        cy.getByAttr('tbl-navigation-arrow').first().click()
        cy.url().should('include', '/detail')
        cy.breadcrumbsIs(['Home', 'Veranstaltung', '3 / evt3 / Event3'])
      })
    })

    it('Erstellen, Editieren und Löschen einer Entität', () => {
      cy.login()
      cy.visit('/tocco/e/User/list')
      cy.waitForListLoaded()

      cy.logStep('Neuen Benutzer anlegen')
      cy.getByAttr('btn-action-new').click()
      cy.get('#input-detailForm-firstname').type('Hans')
      cy.get('#input-detailForm-lastname').type('Muster')
      cy.get('#input-detailForm-relGender').selectValue('Männlich')
      cy.getByAttr('btn-detail-form_submit').click()

      cy.get('#input-detailForm-user_nr').should('exist')
      cy.breadcrumbsIs(['Home', 'Person', 'Muster, Hans'])
      cy.get('#input-detailForm-firstname').should('have.value', 'Hans')
      cy.get('#input-detailForm-lastname').should('have.value', 'Muster')

      cy.logStep('Benutzer editieren')
      cy.get('#input-detailForm-email').type('hans.muster')
      cy.getByAttr('btn-detail-form_submit', 'icon-floppy-disk-circle-xmark').should('exist')
      cy.getByAttr('btn-detail-form_submit').click()
      cy.getByAttr('errorlist-input-detailForm-email', 'erroritem-0').should('exist')
      cy.getByAttr('errorlist-input-detailForm-email', 'erroritem-0').should('has.text', 'Ungültige E-Mail-Adresse')

      cy.get('#input-detailForm-email').type('@tocco.ch')
      cy.getByAttr('btn-detail-form_submit', 'icon-save').should('exist')
      cy.getByAttr('btn-detail-form_submit').click()

      cy.getByAttr('toaster-title').should('has.text', 'Gespeichert')

      cy.logStep('Benutzer löschen')
      cy.getByAttr('btn-action-delete').click()
      cy.getByAttr('btn-delete-ok').click()
      cy.getByAttr('toaster-title').should('has.text', 'Die Aktion wurde ausgeführt.')

      cy.breadcrumbsIs(['Home', 'Person'])
      cy.url().should('include', 'e/User/list')
    })
  })

  describe('Basic Tests on Home', () => {
    beforeEach(() => {
      cy.login()
      cy.visit('/tocco')
      cy.waitForDashboardLoaded()
    })

    it('Menü', () => {
      cy.logStep('Menü via Shortcut öffnen und schliessen')
      cy.get('body').type('{ctrl}{alt}m')
      cy.getByAttr('admin-nav').isInViewport()
      cy.get('body').type('{ctrl}{alt}m')
      cy.getByAttr('admin-nav').isNotInViewport()

      cy.logStep('Menü mit Maus öffnen und schliessen')
      cy.get('#burger-button').click()
      cy.getByAttr('admin-nav').isInViewport()
      cy.get('#burger-button').click()
      cy.getByAttr('admin-nav').isNotInViewport()

      cy.logStep('Alle Menüeinträge aus- und einklappen')
      cy.get('body').type('{ctrl}{alt}m')
      cy.getByAttr('btn-expand-all-menu-entries').click()
      cy.get('[data-cy=menu-entry-wrapper] > [data-cy^=admin-menuitem]').each($el => {
        cy.wrap($el).get('[data-cy=menu-children-wrapper]').should('be.visible')
      })
      cy.getByAttr('btn-collapse-all-menu-entries').click()
      cy.get('[data-cy=menu-entry-wrapper] > [data-cy^=admin-menuitem]').each($el => {
        cy.wrap($el).get('[data-cy=menu-children-wrapper]').should('not.be.visible')
      })
      cy.get('body').type('{ctrl}{alt}m')

      cy.logStep('Weitere Ergebnisse sollten im Module-Tab angezeigt werden')
      cy.get('body').type('{ctrl}{alt}m')
      cy.getByAttr('admin-nav', 'input-search-box').should('be.focused').clear().type('bus')
      cy.getByAttr('extended-search-wrapper').should('be.visible')
      cy.getByAttr('admin-nav', 'input-search-box').should('be.focused').clear()
      cy.get('body').type('{ctrl}{alt}m')

      cy.logStep('System Menü via Shortcut öffnen und schliessen')
      cy.getByAttr('menu-tab-system')
      cy.get('body').type('{ctrl}{alt}k')
      cy.getByAttr('admin-menuitem-system_activity').isInViewport()
      cy.get('body').type('{ctrl}{alt}k')
      cy.getByAttr('admin-menuitem-system_activity').isNotInViewport()

      cy.logStep('Fullscreen-Aktion (Log-Ansicht) über das Menü öffnen')
      cy.get('body').type('{ctrl}{alt}m')
      cy.getByAttr('menu-tab-system').click()
      cy.getByAttr('admin-nav', 'input-search-box').should('be.focused').clear().type('Log-Ansicht')
      cy.getByAttr('admin-menuitem-log').should('exist')
      cy.get('[data-cy=menu-entry-wrapper] > [data-cy^=admin-menuitem]').should('have.length', 1)
      cy.get('[data-cy=admin-menuitem-log] a[data-quick-navigation]').click()
      cy.url().should('include', 'e/action/log')
    })

    it('Geschäftsbereich-Menü', () => {
      cy.logStep('Geschäftsbereich-Menü öffnen und alle Geschäftsbereiche ansehen')
      cy.contains('test1').click()
      cy.contains('test2', {timeout: 15000})
      cy.logStep('Geschäftsbereich-Menü schliessen')
      cy.contains('test1').click()

      cy.logStep('Geschäftsbereich-Menü öffnen und zweiten Geschäftsbereich auswählen')
      cy.contains('test1').click()
      cy.contains('test2').click()
      cy.waitForDashboardLoaded()
      cy.logStep('Geschäftsbereich wieder auf ersten Geschäftsbereich wechseln')
      cy.contains('test2')
      cy.contains('test2').click()
      cy.contains('test1').click()
      cy.contains('test1')
      cy.waitForDashboardLoaded()
    })

    it('Dashboard anpassen', () => {
      cy.logStep('Home anpassen')
      cy.getByAttr('btn-infobox-settings').click()
      cy.getByAttr('menuitem-edit-home').click()
      cy.logStep('Altes Admin abwählen')
      cy.get('#old_admin_link').click()
      cy.logStep('Speichern')
      cy.contains('OK').click()
      cy.logStep('Infobox "Altes Admin" wird ausgeblendet')
      cy.getByAttr('infobox-old_admin_link').should('not.exist')

      cy.logStep('Home zurücksetzen')
      cy.getByAttr('btn-infobox-settings').click()
      cy.getByAttr('menuitem-reset-home').click()
      cy.getByAttr('infobox-old_admin_link').should('be.visible')
      cy.logStep('"Altes Admin" Infobox ist wieder sichtbar')
    })

    it('Hilfe-Menü', () => {
      cy.logStep('Hilfe-Menü öffnen')
      cy.getByAttr('btn-helpcenter').click()

      cy.logStep('Handbuch, REST-Dokumentation und Über Tocco sind sichtbar')
      cy.getByAttr('menuitem-documentation').should('be.visible')
      cy.getByAttr('menuitem-rest-documentation').should('be.visible')
      cy.getByAttr('menuitem-about').should('be.visible')

      cy.logStep('Hilfe-Menü schliessen')
      cy.getByAttr('btn-helpcenter').click()

      cy.getByAttr('menuitem-documentation').should('not.exist')
      cy.getByAttr('menuitem-rest-documentation').should('not.exist')
      cy.getByAttr('menuitem-about').should('not.exist')
    })

    it('Über Tocco - Dialog', () => {
      cy.logStep('"Über Tocco" über das Hilfe-Menü öffnen')
      cy.getByAttr('btn-helpcenter').click()
      cy.getByAttr('menuitem-about').click()
      cy.getByAttr('modal-content').should('be.visible')

      cy.logStep('Tocco Version ist richtig angegeben')
      // Check Tocco Version and if the modal is open
      cy.request({
        method: 'GET',
        url: Cypress.config().baseUrl + '/nice2/rest/client/settings'
      }).then(resp => {
        cy.contains(resp.body.niceVersion)
      })

      cy.logStep('Tocco Infos sind korrekt')
      cy.contains('Tocco AG').contains('2')
      cy.contains('Riedtlistrasse 27')
      cy.contains('CH-8006 Zürich')
      cy.contains('https://www.tocco.ch')
      cy.contains('info@tocco.ch')
      cy.get('[href="tel:(+41) 044 388 60 00"]').should('exist')

      cy.logStep('Dialog wieder schliessen')
      cy.getByAttr('btn-modal-close').click()
      cy.getByAttr('modal-content').should('not.exist')
    })

    it('Benutzer-Menü', () => {
      cy.logStep('Benutzer-Menü öffnen')
      cy.getByAttr('btn-user').click()
      cy.logStep('Passwort, Zwei-Faktor-Authentisierung und Abmelden sind sichtbar')
      cy.getByAttr('menuitem-password-update').should('be.visible')
      cy.getByAttr('menuitem-two-factor-connector').should('be.visible')
      cy.getByAttr('menuitem-logout').should('be.visible')
      cy.getByAttr('btn-user').click()

      cy.logStep('Benutzer-Menü schliessen')
      cy.getByAttr('menuitem-password-update').should('not.exist')
      cy.getByAttr('menuitem-two-factor-connector').should('not.exist')
      cy.getByAttr('menuitem-logout').should('not.exist')
    })

    it('Passwort ändern - Aktion', () => {
      cy.logStep('Password ändern über Benutzer-Menü öffnen')
      cy.getByAttr('btn-user').click()
      cy.getByAttr('menuitem-password-update').click()

      cy.logStep('Richtiges altes Passwort muss eingeben werden')
      cy.get('#newPassword-input').should('be.disabled')
      cy.get('#oldPassword-input').type('Test')
      cy.getByAttr('btn-submit').should('be.disabled')
      cy.get('#oldPassword-input').type(Cypress.env('PASSWORD'))
      cy.get('#newPasswordRepeat-input').should('be.disabled')

      cy.logStep('Neues Passwort muss den Richtlinien entsprechen')
      cy.get('#newPassword-input').type('Test')
      cy.getByAttr('btn-submit').should('be.disabled')
      cy.get('#newPassword-input').clear()
      cy.get('#newPassword-input').type('ThisIsANewSecret123$')

      cy.logStep('Passwort-Wiederholung muss identisch mit dem neuen Passwort sein')
      cy.get('#newPasswordRepeat-input').type('test')
      cy.getByAttr('btn-submit').should('be.disabled')
      cy.get('#newPasswordRepeat-input').clear()
      cy.get('#newPasswordRepeat-input').type('ThisIsANewSecret123$')
      cy.logStep('Wenn alles stimmt, kann gespeichert werden')
      cy.getByAttr('btn-submit').should('not.be.disabled')
      cy.getByAttr('btn-modal-close').click()
    })
  })

  it(`Listenansicht: Spalten anpassen, umsortieren, etc.`, () => {
    cy.login()
    cy.logStep('Personen-Liste öffnen')
    cy.visit('/tocco/e/User/list')
    cy.waitForListLoaded()

    cy.logStep('Spalten sollten in der Standard-Reihenfolge dargestellt werden')
    cy.get('th[data-cy="tbl-header-cell-numbering-column"]:nth-child(1)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-multi-selection"]:nth-child(2)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-navigation-column"]:nth-child(3)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-mark-column"]:nth-child(4)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-user_nr"]:nth-child(5)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-firstname"]:nth-child(6)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-lastname"]:nth-child(7)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-email"]:nth-child(8)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-relUser_status"]:nth-child(9)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-phone_company"]:nth-child(10)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-phone_private"]:nth-child(11)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-phone_mobile"]:nth-child(12)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-relAddress_c"]:nth-child(13)').should('exist')
    cy.get('th[data-cy="tbl-header-cell-candidate_number"]:nth-child(14)').should('exist')

    // hide / show columns
    cy.logStep('"Spalten anpassen" öffnen')
    cy.getByAttr('btn-preferences').click()
    cy.getByAttr('menuitem-columns').click()
    cy.logStep('Vorname abwählen, Akad. Titlel anwählen')
    cy.get('#firstname').click()
    cy.get('label[for="firstname"]').should('have.text', '10.Vorname')
    cy.get('#relAcademic_title').click()
    cy.get('label[for="relAcademic_title"]').should('have.text', '9.Akad. Titel')
    cy.logStep('Speichern')
    cy.getByAttr('btn-ok').click()

    cy.logStep('Spalte "Vorname" sollte ausgeblendet sein')
    cy.getByAttr('tbl-header-cell-firstname').should('not.exist')
    cy.logStep('Spalte "Akad. Titel" sollte als letzte Spalte eingeblendet sein')
    cy.get('th[data-cy="tbl-header-cell-relAcademic_title"]:nth-child(13)').should('exist')

    // reset settings
    cy.logStep('"Alles zurücksetzen" ausführen')
    cy.getByAttr('btn-preferences').click()
    cy.getByAttr('menuitem-preferencesReset').click()

    cy.logStep('Spalte "Vorname" ist wieder sichtbar, "Akad. Titel" ist nicht mehr sichtbar')
    cy.get('th[data-cy="tbl-header-cell-firstname"]:nth-child(6)').should('exist')
    cy.getByAttr('tbl-header-cell-relAcademic_title').should('not.exist')

    // re-arrange column by drag and drop
    cy.logStep('Spalte "Telefon G" nach "Nachname" mittels Drag and Drop verschieben')
    cy.get('#header-cell-drop-phone_company').trigger('dragstart')
    cy.get('#header-cell-drop-lastname').trigger('dragenter', 'right')
    cy.get('#header-cell-drop-lastname').trigger('drop')
    cy.get('th[data-cy="tbl-header-cell-phone_company"]:nth-child(8)').should('exist')

    cy.logStep('"Spalten anpassen" öffnen')
    cy.getByAttr('btn-preferences').click()
    cy.getByAttr('menuitem-columns').click()
    cy.logStep('Telefon G sollte auf 4. Position sichtbar sein')
    cy.get('label[for="phone_company"]').should('have.text', '4.Telefon G')
    cy.get('#relAcademic_title').click()
    cy.logStep('Akad. Titel wieder anwählen')
    cy.get('label[for="relAcademic_title"]').should('have.text', '11.Akad. Titel')
    cy.logStep('Speichern')
    cy.getByAttr('btn-ok').click()

    cy.logStep('Akad. Titel per Drag and Drop links von der Nr. platzieren')
    cy.get('#header-cell-drop-relAcademic_title').trigger('dragstart')
    cy.get('#header-cell-drop-user_nr').trigger('dragenter', 'left')
    cy.get('#header-cell-drop-user_nr').trigger('drop')

    cy.logStep('Akad. Titel wurde richtig verschoben')
    cy.get('th[data-cy="tbl-header-cell-relAcademic_title"]:nth-child(5)').should('exist')

    cy.logStep('"Alles zurücksetzen" ausführen')
    cy.getByAttr('btn-preferences').click()
    cy.getByAttr('menuitem-columnsReset').click()

    cy.logStep('Telefon G sollte wieder an ursprünglicher Position sein, Akad Titel ist nicht mehr sichtbar')
    cy.get('th[data-cy="tbl-header-cell-phone_company"]:nth-child(10)').should('exist')
    cy.getByAttr('tbl-header-cell-relAcademic_title').should('not.exist')
  })

  it('Formular-Komponenten', () => {
    cy.login()
    cy.task('db:seed:form-components').then(response => {
      cy.visit(`/tocco/e/Event/${response.pk}/detail`)

      // datepicker
      cy.logStep('Datepicker')
      cy.get('#input-detailForm-first_course_date .react-datepicker__input').focus()
      cy.get('.react-datepicker').should('not.exist')

      cy.get('#input-detailForm-first_course_date').click().typeDate('12.10.2022')
      cy.get('#input-detailForm-first_course_date .react-datepicker__input').type('{downArrow}{downArrow}{enter}')
      cy.get('#input-detailForm-first_course_date').hasDate('19.10.2022')
      cy.get('#input-detailForm-first_course_date').clearDate()
      cy.get('#input-detailForm-first_course_date').hasDate('')

      cy.get('#input-detailForm-first_course_date').click().typeDate('12102022')
      cy.get('#input-detailForm-first_course_date').typeDate('{enter}')
      cy.get('#input-detailForm-first_course_date').hasDate('12.10.2022')

      cy.get('#input-detailForm-first_course_date').clearDate()
      cy.get('#input-detailForm-first_course_date').click().typeDate('121022')
      cy.get('#input-detailForm-first_course_date').typeDate('{enter}')
      cy.get('#input-detailForm-first_course_date').hasDate('12.10.2022')

      cy.get('.react-datepicker').should('not.exist')
      cy.get('#input-detailForm-first_course_date [data-cy=btn-calendar]').click()
      cy.get('.react-datepicker').should('exist')
      cy.get('.react-datepicker__day--selected').should('have.text', '12')
      cy.get('#input-detailForm-first_course_date .react-datepicker__input').type('{downArrow}{rightArrow}')
      cy.get('.react-datepicker__day--keyboard-selected').should('have.text', '13')
      cy.focused().type('{downArrow}{downArrow}{downArrow}{downArrow}{enter}')
      cy.get('#input-detailForm-first_course_date').hasDate('10.11.2022')

      // textarea
      cy.logStep('Text')
      const longText =
        // eslint-disable-next-line max-len
        'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.'
      cy.get('#input-detailForm-targets').focus().type(longText)
      cy.get('#input-detailForm-targets').should('have.value', longText)

      cy.get('#input-detailForm-targets').clear()
      cy.get('#input-detailForm-targets').should('have.value', '')

      const multilineText = `Lorem ipsum dolor sit amet, consetetur sadipscing elitr.

Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.`
      cy.get('#input-detailForm-targets').type(multilineText)
      cy.get('#input-detailForm-targets').should('have.value', multilineText)

      // duration
      cy.logStep('Duration')
      cy.get('#input-detailForm-education_amount').typeDuration({hours: '2', minutes: '10'})
      cy.get('#input-detailForm-education_amount').should('have.text', 'Std.Min.') // labels should be visible
      cy.get('#input-detailForm-education_amount').hasDuration({hours: '2', minutes: '10'})
      cy.get('#input-detailForm-education_amount').clearDuration()

      cy.get('#input-detailForm-education_amount').typeDuration({minutes: '80'})
      cy.get('#input-detailForm-education_amount').hasDuration({hours: '1', minutes: '20'})
      cy.get('#input-detailForm-education_amount').clearDuration()

      cy.get('#input-detailForm-education_amount').typeDuration({hours: '26'})
      cy.get('#input-detailForm-education_amount').hasDuration({hours: '26', minutes: '0'})

      // single select
      cy.logStep('Single Select')
      cy.get('#input-detailForm-relEvent_teaching_method').selectValue('Präsenzunterricht')
      cy.get('#input-detailForm-relEvent_teaching_method').hasValueSelected('Präsenzunterricht')
      cy.get('#input-detailForm-relEvent_teaching_method').clearSelectedValue()

      // multi select
      cy.logStep('Multi Select')
      cy.get('#input-detailForm-relTarget_group').selectValue('Einsteiger')
      cy.get('#input-detailForm-relTarget_group').selectValue('Fortgeschrittene')
      cy.get('#input-detailForm-relTarget_group').hasValueSelected('Einsteiger')
      cy.get('#input-detailForm-relTarget_group').hasValueSelected('Fortgeschrittene')
      cy.get('#input-detailForm-relTarget_group').removeSelectedValue('Fortgeschrittene')
      cy.get('#input-detailForm-relTarget_group').hasValueSelected('Einsteiger')

      // single remote select
      cy.logStep('Single Remote Select mit erweiteren Suche')
      cy.get('#input-detailForm-relUser').selectRemoteValue('Tocco AG')
      cy.get('#input-detailForm-relUser').hasValueSelected('1, Tocco AG, Support, support@tocco.ch')
      cy.get('#input-detailForm-relUser').clearSelectedValue()

      cy.get('#input-detailForm-relUser').openAdvancedSearch()
      cy.getByAttr('modal-content').toggleSingleSelection(0)
      cy.getByAttr('modal-content').should('not.exist')
      cy.get('#input-detailForm-relUser').hasValueSelected('Tocco AG, Support')
      cy.get('#input-detailForm-relUser').clearSelectedValue()

      cy.get('#input-detailForm-relUser').openRemoteCreate()
      cy.get('#input-detailForm-firstname').type('Hans')
      cy.get('#input-detailForm-lastname').type('Muster')
      cy.get('#input-detailForm-relGender').selectValue('Männlich')
      cy.getByAttr('modal-content').within(() => {
        cy.getByAttr('btn-detail-form_submit').click()
      })
      cy.getByAttr('modal-content').should('not.exist')
      cy.get('#input-detailForm-relUser').hasValueSelected('Muster, Hans')

      // email-template
      cy.logStep('E-Mail Template (mit Freemarker Inhalten)')
      cy.visit(`/tocco/e/Email_template/create`)
      cy.get('#input-detailForm-mail_text_de').typeHtmlValue('Test')
      cy.get('#input-detailForm-mail_text_de').hasHtmlValue('Test')
      cy.get('#input-detailForm-mail_text_de').clearHtmlValue()
      cy.get('#input-detailForm-mail_text_de').hasHtmlValue('')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(true)
      cy.get('#input-detailForm-mail_text_de').typeHtmlSourceValue('<a href="https://tocco.ch">Webseite')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(false)
      cy.get('#input-detailForm-mail_text_de').hasHtmlValue('Webseite')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(true)
      cy.get('#input-detailForm-mail_text_de').hasHtmlSourceValue('<p><a href="https://tocco.ch">Webseite</a></p>')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(false)
      cy.get('#input-detailForm-mail_text_de').clearHtmlValue()

      // keep freemarker in place
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(true)
      cy.get('#input-detailForm-mail_text_de').typeHtmlSourceValue(
        `<!--[#assign myList = ["one", "two", "three"]]-->
<table><tbody>
  <!--[#list myList as item]-->
  <tr>
   <td>\${item}`,
        {parseSpecialCharSequences: false}
      )

      cy.get('#input-detailForm-mail_text_de').typeHtmlSourceValue(
        '{rightArrow}{rightArrow}{rightArrow}{rightArrow}{rightArrow}'
      )
      cy.get('#input-detailForm-mail_text_de').typeHtmlSourceValue(
        '{rightArrow}{rightArrow}{rightArrow}{rightArrow}{rightArrow}'
      )
      cy.get('#input-detailForm-mail_text_de').typeHtmlSourceValue('<!-- [/#list] -->')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(false)
      // eslint-disable-next-line no-template-curly-in-string
      cy.get('#input-detailForm-mail_text_de').hasHtmlValue('${item}')
      cy.get('#input-detailForm-mail_text_de').toggleSourceEditing()
      cy.get('#input-detailForm-mail_text_de').assertSourceEditing(true)
      cy.get('#input-detailForm-mail_text_de').hasHtmlSourceValue(
        // eslint-disable-next-line max-len, no-template-curly-in-string
        '<!--[#assign myList = ["one", "two", "three"]]--><table><tbody><!--[#list myList as item]--><tr><td>${item}</td></tr><!-- [/#list] --></tbody></table>​'
      )
    })
  })
})
