describe('Doubletten', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  it(`TOCRT-32 - Konfigurieren und finden von Doubletten`, () => {
    cy.task('db:seed:duplicate').then(([pk1, pk2]) => {
      cy.login()

      cy.visit('/tocco/e/Duplicate_config/list')
      cy.waitForListLoaded()

      // activate duplicate config for person
      cy.logStep('Doubletten-Konfiguration für Personen aktivieren')
      cy.get('table').toggleMultiSelection(0)

      cy.getByAttr('btn-action-actions').click()
      cy.getByAttr('menuitem-activate-duplicate-config').click()
      cy.getByAttr('btn-confirm-ok').click()

      cy.get('[data-cy="tbl-list-row"]:nth-child(1)').getByAttr('tbl-cell-active')

      cy.get('table').toggleMultiSelection(1)
      cy.getTableRow(0).getByAttr('tbl-cell-active', 'icon-check').should('exist')

      // should have found duplicate
      cy.logStep('Gefundene Doubletten auf Doubletten-Liste ansehen')
      cy.visit('/tocco/e/Duplicate/list')
      cy.getTableRow(0).getByAttr('tbl-cell-relDuplicate_entity_model').should('have.text', 'Person')
      cy.getTableRow(0).getByAttr('tbl-cell-description').should('have.text', 'Meier, Hans')
      cy.getTableRow(0).getByAttr('tbl-cell-relDuplicate_status').should('have.text', 'Offen')
      cy.getTableRow(0).getByAttr('tbl-cell-duplicate_count').should('contain.text', '2')

      // duplication warning on each duplicate
      cy.logStep('Doubletten Warnung auf Personen-Detail ansehen')
      cy.getTableRow(0).navigateToDetail()
      cy.getByAttr('box-relation-preview').getTableRow(0).navigateToDetail()

      cy.breadcrumbsIs(['Home', 'Doublette', 'Meier, Hans / Offen', 'Person', 'Meier, Hans'])
      cy.get('#input-detailForm-duplicate_warning').should('exist')
      cy.get('#input-detailForm-duplicate_warning a').should('have.text', 'Offene Doublette prüfen')
      cy.get('#input-detailForm-relDuplicate').should('have.text', 'Meier, Hans / Offen')

      cy.go('back')
      cy.breadcrumbsIs(['Home', 'Doublette', 'Meier, Hans / Offen'])
      cy.getByAttr('box-relation-preview').getTableRow(0).navigateToDetail()

      cy.breadcrumbsIs(['Home', 'Doublette', 'Meier, Hans / Offen', 'Person', 'Meier, Hans'])
      cy.get('#input-detailForm-duplicate_warning').should('exist')
      cy.get('#input-detailForm-duplicate_warning a').should('have.text', 'Offene Doublette prüfen')
      cy.get('#input-detailForm-relDuplicate').should('have.text', 'Meier, Hans / Offen')

      cy.go('back')
      cy.breadcrumbsIs(['Home', 'Doublette', 'Meier, Hans / Offen'])

      // start merge
      cy.logStep('Offene Doublette zusammenführen')
      cy.getByAttr('btn-action-nice2.netui.actions.standard.entityMergeAction').click()

      // should show correct merge table
      cy.logStep('Anzeige der Zusammenführen-Aktion überprüfen')
      cy.getByAttr(`tbl-header-cell-${pk1}`).should('contain.text', 'Meier, Hans')
      cy.getTableRow(0).getByAttr('tbl-cell-column-label').should('contain.text', 'Person-Nr.')
      cy.getTableRow(0).getByAttr(`tbl-cell-${pk1}`).should('contain.text', '2')
      cy.getTableRow(0).getByAttr(`tbl-cell-${pk2}`).should('contain.text', '3')

      cy.getTableRow(1).getByAttr('tbl-cell-column-label').should('contain.text', 'Login')
      cy.getTableRow(1).getByAttr(`tbl-cell-${pk1}`).should('contain.text', 'hans.meier@tocco.ch')
      cy.getTableRow(1).getByAttr(`tbl-cell-${pk2}`).should('contain.text', 'hans.meier@tocco.ch_add1')

      // should select other entity for data
      cy.logStep('Zweite Person anwählen / behalten und erste Person archivieren')
      cy.getByAttr(`tbl-header-cell-${pk1}`).should('contain.text', '(Behalten)')
      cy.getByAttr(`tbl-header-cell-${pk2}`).should('contain.text', '(Archivieren)')

      cy.getByAttr(`tbl-header-cell-${pk2}`).find(`#targetEntity${pk2}`).click()

      cy.getByAttr(`tbl-header-cell-${pk1}`).should('contain.text', '(Archivieren)')
      cy.getByAttr(`tbl-header-cell-${pk2}`).should('contain.text', '(Behalten)')

      cy.logStep('Zusammenführen ausführen')
      cy.getByAttr('btn-merge-save').click()

      cy.getByAttr('box-merge-summary-success').should('contain.text', 'Das Zusammenführen war erfolgreich.')
      cy.getByAttr('btn-merge-close').click()

      // should navigate to list
      cy.logStep('Keine offenen Doubletten vorhanden')
      cy.breadcrumbsIs(['Home', 'Doublette'])
      cy.getByAttr('tbl').hasNoData()

      // should merge users
      cy.logStep('Person ist zusammengeführt und korrekte Person wurde behalten/archiviert')
      cy.visit(`/tocco/e/User/${pk2}/detail`)
      cy.getByAttr('box-relation-box-Principal').click()
      cy.getByAttr('box-relation-preview')
        .getTableRow(0)
        .getByAttr('tbl-cell-username')
        .should('have.text', 'hans.meier@tocco.ch')
      cy.getByAttr('box-relation-preview')
        .getTableRow(1)
        .getByAttr('tbl-cell-username')
        .should('have.text', 'hans.meier@tocco.ch_add1')

      cy.visit(`/tocco/e/User/${pk1}/detail`)
      cy.get('#input-detailForm-relUser_status').hasValueSelected('Archiv')
      cy.getByAttr('box-relation-box-Principal').click()
      cy.getByAttr('box-relation-preview').hasNoData()
    })
  })
})
