describe('Vorlagen', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.login()
  })

  it('Vorlage erstellen, aktualisieren und löschen', () => {
    cy.visit('/tocco/e/User/list')
    cy.waitForListLoaded()

    cy.logStep('Neue Person erstellen')
    cy.getByAttr('btn-action-new').click()

    cy.getByAttr('btn-detail-form_submit').should('exist')

    cy.getByAttr('entity-template-empty').contains('Keine Vorlagen verfügbar').should('exist')

    cy.logStep('Verschiedene Formularfelder ausfüllen')
    cy.get('#input-detailForm-firstname').type('Vorname')
    cy.get('#input-detailForm-lastname').type('Nachname')
    cy.get('#input-detailForm-relGender').selectValue('Weiblich')
    cy.get('#input-detailForm-birthdate').typeDate('20.03.1990')
    cy.get('#input-detailForm-relUser_code1').selectValue('Präsident')
    cy.get('#input-detailForm-relUser_code1').selectValue('Geschäftsleitung')
    cy.get('#input-detailForm-relNationality').selectRemoteValue('Schweiz')

    cy.logStep('Vorlage erstellen')
    cy.getByAttr('btn-action-entity-template-create').click()
    cy.get('#input-simpleForm-label').type('Vorlage 1')
    cy.getByAttr('btn-simpleForm-submit').click()
    cy.contains('Vorlage wurde erstellt und ausgewählt.').should('exist')

    // template should be selected
    cy.logStep('Vorlage ist ausgewählt')
    cy.getByAttr('entity-template-list').contains('Vorlage 1').should('exist')
    cy.get('#input-detailForm-firstname').should('have.value', 'Vorname')
    cy.get('#input-detailForm-lastname').should('have.value', 'Nachname')
    cy.get('#input-detailForm-birthdate').hasDate('20.03.1990')
    cy.get('#input-detailForm-relGender').hasValueSelected('Weiblich')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Präsident')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Geschäftsleitung')
    cy.get('#input-detailForm-relNationality').hasValueSelected('Schweiz')
    cy.getByAttr('btn-action-entity-template-update').should('exist')

    // update template
    cy.logStep('Formular anpassen und Vorlage aktualisieren')
    cy.get('#input-detailForm-firstname').clear().type('Hans')
    cy.wait(200) // wait until Debouncer has saved input to formValues
    cy.getByAttr('btn-action-entity-template-update').click()
    cy.getByAttr('btn-confirm-ok').click()
    cy.contains('Die Vorlage wurde aktualisiert.').should('exist')

    // deselect template
    cy.logStep('Formular abwählen')
    cy.getByAttr('entity-template-list').contains('Vorlage 1').click()
    cy.get('#input-detailForm-firstname').should('have.value', '')
    cy.get('#input-detailForm-lastname').should('have.value', '')
    cy.getByAttr('btn-action-entity-template-update').should('not.exist')

    // select template again
    cy.logStep('Formular anwählen')
    cy.getByAttr('entity-template-list').contains('Vorlage 1').click()
    cy.get('#input-detailForm-firstname').should('have.value', 'Hans')
    cy.get('#input-detailForm-lastname').should('have.value', 'Nachname')
    cy.get('#input-detailForm-birthdate').hasDate('20.03.1990')
    cy.get('#input-detailForm-relGender').hasValueSelected('Weiblich')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Präsident')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Geschäftsleitung')
    cy.get('#input-detailForm-relNationality').hasValueSelected('Schweiz')

    cy.get('#input-detailForm-firstname').clear().type('Firstname')
    cy.get('#input-detailForm-lastname').clear().type('Lastname')

    // save user
    cy.logStep('Person speichern / erstellen')
    cy.getByAttr('btn-detail-form_submit').click()

    cy.contains('Person-Nr.').should('exist') // wait for navigate to detail
    cy.breadcrumbsIs(['Home', 'Person', 'Lastname, Firstname'])
    cy.get('#input-detailForm-firstname').should('have.value', 'Firstname')
    cy.get('#input-detailForm-lastname').should('have.value', 'Lastname')
    cy.get('#input-detailForm-birthdate').hasDate('20.03.1990')
    cy.get('#input-detailForm-relGender').hasValueSelected('Weiblich')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Präsident')
    cy.get('#input-detailForm-relUser_code1').hasValueSelected('Geschäftsleitung')
    cy.get('#input-detailForm-relNationality').hasValueSelected('Schweiz')

    // copy user and select template
    cy.logStep('Person kopieren')
    cy.getByAttr('btn-buttonmenu-open').click()
    cy.getByAttr('menuitem-copy').click()
    cy.get('#input-detailForm-firstname').should('have.value', 'Firstname')
    cy.get('#input-detailForm-lastname').should('have.value', 'Lastname')

    cy.logStep('Vorlage auswählen')
    cy.getByAttr('entity-template-list').contains('Vorlage 1').click()
    cy.get('#input-detailForm-firstname').should('have.value', 'Hans')
    cy.get('#input-detailForm-lastname').should('have.value', 'Nachname')

    cy.logStep('Kopierte Person erstellen')
    cy.getByAttr('btn-detail-form_submit').click()

    cy.logStep('Vorlage löschen')
    cy.getByAttr('btn-action-new').click()

    // delete entity template again
    cy.getByAttr('entity-template-loading').should('exist')
    cy.getByAttr('entity-template-loading').should('not.exist')
    cy.getByAttr('entity-template-list').contains('Vorlage 1').should('exist')
    cy.getByAttr('entity-template-list', 'btn-entity-template-ballmenu').should('exist')
    cy.getByAttr('entity-template-list').within(() => {
      cy.getByAttr('btn-entity-template-ballmenu').click({force: true})
    })
    cy.getByAttr('menuitem-entity-template-edit').should('exist')
    cy.getByAttr('menuitem-entity-template-delete').click()
    cy.getByAttr('modal-content').within(() => {
      cy.contains('Vorlage löschen').should('exist')
      cy.contains('Datenobjekt-Vorlage (1)').should('exist')
      cy.getByAttr('btn-delete-ok').click()
    })
    cy.getByAttr('entity-template-empty').contains('Keine Vorlagen verfügbar').should('exist')
  })
})
