describe('Noteneingabe - Aktion', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.task('db:seed:input-nodes')
  })

  beforeEach(() => {
    cy.login()
  })

  const openInputEdit = inputKey => {
    cy.visit(`/tocco/e/Input/${inputKey}`)
    cy.waitForLoaded()
    cy.clickActionMenuItem('new_input_edit')
    cy.awaitInputEditData()

    cy.get('[data-cy*="tbl-cell"]')
      .then(thing => {
        const ratingNodes = []
        Array.from(thing, t => t.dataset.cy.substring('tbl-cell-'.length))
          .filter(id => !isNaN(parseInt(id)) || id === 'rating')
          .forEach(id => {
            if (!ratingNodes.includes(id)) {
              ratingNodes.push(id)
            }
          })
        return ratingNodes
      })
      .as('ratingNodes')
  }

  describe('General', () => {
    beforeEach(() => {
      cy.task('db:seed:input', {inputType: 'grades', userCount: 3}).then(openInputEdit)
    })

    it('Filterung der Daten', () => {
      cy.getByAttr('tbl-list-row').should('have.length', 3)
      const searchField = cy.get('#input-simpleForm-relUser\\.lastname')
      searchField.type('Nachname 1')
      cy.getByAttr('tbl-list-row').should('have.length', 1)
      searchField.clear()
      cy.getByAttr('tbl-list-row').should('have.length', 3)
    })

    it('Navigation zwischen Personen- und Noten-Felder', () => {
      cy.get('@ratingNodes').then(ratingNodes => {
        cy.getByAttr('tbl-list-row').first().focusInputEditCell(ratingNodes[1])

        cy.hasInputEditCellFocused(`1:${ratingNodes[1]}`)
        cy.focused().type('{rightarrow}')
        cy.hasInputEditCellFocused(`1:${ratingNodes[2]}`)
        cy.focused().type('{rightarrow}{leftarrow}{leftarrow}{leftarrow}')
        cy.hasInputEditCellFocused(`1:${ratingNodes[0]}`)
        cy.focused().type('{uparrow}{downarrow}{downarrow}')
        cy.hasInputEditCellFocused(`3:${ratingNodes[0]}`)
        cy.focused().type('{downarrow}{uparrow}')
        cy.hasInputEditCellFocused(`2:${ratingNodes[0]}`)
      })
    })

    it('Stammdaten - Aktion', () => {
      cy.getByAttr('input-edit-table').clickActionButton('new_input_edit_info')
      cy.getByAttr('modal-content').getValueByLabel('Veranstaltung').should('have.text', 'input edit event')
      cy.getByAttr('modal-content').getValueByLabel('Fach').should('have.text', 'Grades')
      cy.getByAttr('modal-content').getValueByLabel('Noteneingabe-Typ').should('have.text', 'Noten')
      cy.getByAttr('modal-content').getValueByLabel('Status Noteneingabe').should('have.text', 'Vorbereitet')

      // these two values can change easily based on backend configuration, just check that they are here
      cy.getByAttr('modal-content').getValueByLabel('Notenskala')
      cy.getByAttr('modal-content').getValueByLabel('Rundung')
    })
  })

  describe('Types', () => {
    // exams are weighted
    it(`Noten: Durchschnittsberechnung mit gewichteten Prüfungen`, () => {
      cy.logStep('Noteneingabe öffnen')
      cy.task('db:seed:input', {inputType: 'grades'}).then(openInputEdit)

      cy.logStep('Verschiedene Noten eingeben')
      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 3)
        cy.fillInputEditCell(ratingNodes[1], 3)
        cy.fillInputEditCell(ratingNodes[2], 5)
      })
      cy.awaitInputEditCalculation()
      cy.logStep('Durchschnittsberechnung wird ausgeführt')
      cy.hasInputEditCellValue('grade', '4.00')
      cy.hasInputEditCellValue('definate_grade', '4.00')

      cy.logStep('Notenkorrektur vornehmen')
      cy.fillInputEditCell('pre_grade', 6)
      cy.logStep('Notenkorrektur wird übernommen')
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('grade', '4.00')
      cy.hasInputEditCellValue('definate_grade', '6.00')

      cy.logStep('Dispensation anwählen')
      cy.toggleInputEditDispense()
      cy.awaitInputEditCalculation()
      cy.logStep('Korrektur wird entfernt')
      cy.hasInputEditCellValue('grade', '4.00')
      cy.hasEmptyInputEditCell('definate_grade')
    })

    // duplicate logic from regular grades is not tested again
    it(`Pflichtnoten: Durchschnitt wird nicht berechnet falls Noten nicht vollständig sind`, () => {
      cy.logStep('Noteneingabe öffnen')
      cy.task('db:seed:input', {inputType: 'grades_mandatory'}).then(openInputEdit)

      cy.logStep('Noten unvollständig eingeben')
      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 3)
        cy.fillInputEditCell(ratingNodes[1], 3)
      })
      cy.awaitInputEditCalculation()
      cy.logStep('Durchschnitt bleibt leer')
      cy.hasEmptyInputEditCell('grade')
      cy.hasEmptyInputEditCell('definate_grade')
    })

    // duplicate logic from regular grades is not tested again
    it(`Noten mit Streichnoten: Ignoriert schlechteste Note`, () => {
      cy.task('db:seed:input', {inputType: 'grades_drop'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 6)
        cy.fillInputEditCell(ratingNodes[1], 1)
        cy.fillInputEditCell(ratingNodes[2], 5)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('grade', '6.00')
      cy.hasInputEditCellValue('definate_grade', '6.00')
    })

    // duplicate logic from regular grades is not tested again
    it(`Wiederholungsprüfung: Nimmt nur beste Noten`, () => {
      cy.task('db:seed:input', {inputType: 'grades_max'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 6)
        cy.fillInputEditCell(ratingNodes[1], 1)
        cy.fillInputEditCell(ratingNodes[2], 5)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('grade', '6.00')
      cy.hasInputEditCellValue('definate_grade', '6.00')
    })

    it(`Punkte summiert: Berechnet Summe`, () => {
      cy.task('db:seed:input', {inputType: 'points_sum'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 10)
        cy.fillInputEditCell(ratingNodes[1], 20)
        cy.fillInputEditCell(ratingNodes[2], 30)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('value', '60.00')

      cy.fillInputEditCell('points_correction', 80)
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('value', '60.00')

      cy.toggleInputEditDispense()
      cy.awaitInputEditCalculation()
      cy.hasEmptyInputEditCell('value')
    })

    it(`Punkte mit Notenumrechnung (Formel): Berechne Summe und Note`, () => {
      cy.task('db:seed:input', {inputType: 'points_sum_calculation_formula'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 10)
        cy.fillInputEditCell(ratingNodes[1], 20)
        cy.fillInputEditCell(ratingNodes[2], 30)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('value', '60.00')
      cy.hasInputEditCellValue('definate_grade', '4.00')

      cy.fillInputEditCell('points_correction', 80)
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('value', '60.00')
      cy.hasInputEditCellValue('definate_grade', '5.00')

      cy.toggleInputEditDispense()
      cy.awaitInputEditCalculation()
      cy.hasEmptyInputEditCell('value')
      cy.hasEmptyInputEditCell('definate_grade')
    })

    // dispense and correction handling is the same as formula collection, so not tested again
    // threshold values are:
    // - > 20 -> 2
    // - > 60 -> 4
    // - > 100 -> 6
    it(`Punkte mit Notenumrechnung (Schwellwert): Berechne Summe und Note`, () => {
      cy.task('db:seed:input', {inputType: 'points_sum_calculation_threshold_value'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 5)
        cy.fillInputEditCell(ratingNodes[1], 4)
        cy.fillInputEditCell(ratingNodes[2], 10)
        cy.awaitInputEditCalculation()
        cy.hasEmptyInputEditCell('definate_grade')

        cy.fillInputEditCell(ratingNodes[0], 5)
        cy.fillInputEditCell(ratingNodes[1], 5)
        cy.fillInputEditCell(ratingNodes[2], 10)
        cy.awaitInputEditCalculation()
        cy.hasInputEditCellValue('definate_grade', '2.00')

        cy.fillInputEditCell(ratingNodes[0], 30)
        cy.fillInputEditCell(ratingNodes[1], 30)
        cy.fillInputEditCell(ratingNodes[2], 39)
        cy.awaitInputEditCalculation()
        cy.hasInputEditCellValue('definate_grade', '4.00')
      })
    })

    // exams are weighted
    it(`Punkte mit Durchschnitt: Berechnet Durchschnitt mit gewichteten Prüfungen`, () => {
      cy.task('db:seed:input', {inputType: 'points_average'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 10)
        cy.fillInputEditCell(ratingNodes[1], 10)
        cy.fillInputEditCell(ratingNodes[2], 30)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('points_average', '20.00')

      cy.fillInputEditCell('points_correction', 80)
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('points_average', '20.00')

      cy.toggleInputEditDispense()
      cy.awaitInputEditCalculation()
      cy.hasEmptyInputEditCell('points_average')
    })

    // exams are weighted
    it(`Punkte mit Durchschnitt und Notenumrechnung (Formel): Berechne Durchschitt und Note`, () => {
      cy.task('db:seed:input', {inputType: 'points_average_calculation_formula'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 10)
        cy.fillInputEditCell(ratingNodes[1], 10)
        cy.fillInputEditCell(ratingNodes[2], 30)
      })
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('points_average', '20.00')
      cy.hasInputEditCellValue('definate_grade', '2.00')

      cy.fillInputEditCell('points_correction', 80)
      cy.awaitInputEditCalculation()
      cy.hasInputEditCellValue('points_average', '20.00')
      cy.hasInputEditCellValue('definate_grade', '5.00')

      cy.toggleInputEditDispense()
      cy.awaitInputEditCalculation()
      cy.hasEmptyInputEditCell('points_average')
      cy.hasEmptyInputEditCell('definate_grade')
    })

    // exams are weighted
    // dispense and correction handling is the same as formula collection, so not tested again
    // threshold values are:
    // - > 20 -> 2
    // - > 60 -> 4
    // - > 100 -> 6
    it(`Punkte mit Durchschnitt und Notenumrechnung (Schwellwert): Berechne Durchschitt und Note`, () => {
      cy.task('db:seed:input', {inputType: 'points_average_calculation_threshold_value'}).then(openInputEdit)

      cy.get('@ratingNodes').then(ratingNodes => {
        cy.fillInputEditCell(ratingNodes[0], 19)
        cy.fillInputEditCell(ratingNodes[1], 19)
        cy.fillInputEditCell(ratingNodes[2], 19)
        cy.awaitInputEditCalculation()
        cy.hasEmptyInputEditCell('definate_grade')

        cy.fillInputEditCell(ratingNodes[0], 20)
        cy.fillInputEditCell(ratingNodes[1], 20)
        cy.fillInputEditCell(ratingNodes[2], 20)
        cy.awaitInputEditCalculation()
        cy.hasInputEditCellValue('definate_grade', '2.00')

        cy.fillInputEditCell(ratingNodes[0], 99)
        cy.fillInputEditCell(ratingNodes[1], 99)
        cy.fillInputEditCell(ratingNodes[2], 99)
        cy.awaitInputEditCalculation()
        cy.hasInputEditCellValue('definate_grade', '4.00')
      })
    })

    it(`Freitext: Eingabe von Freitext und Dispensationen`, () => {
      // free text does not have any logic, so just check we can interact
      cy.task('db:seed:input', {inputType: 'free_text'}).then(openInputEdit)

      cy.fillInputEditCell('text', 'whatever free text')
      cy.hasFilledInputEditCell('text')
      cy.toggleInputEditDispense()
    })

    it(`Textwahl: Auswahl von definierten Texten und Dispensationen`, () => {
      // choice rating does not have any logic, so just check we can interact
      cy.task('db:seed:input', {inputType: 'choice'}).then(openInputEdit)

      cy.focusInputEditCell('rating').type('{downarrow}{downarrow}{downarrow}{enter}')
      cy.hasFilledInputEditCell('rating')
      cy.toggleInputEditDispense()
    })
  })
})
