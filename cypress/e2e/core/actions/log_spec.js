describe('Log-Ansicht - Aktion', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.login()
  })

  it(`Log-Ansicht`, () => {
    cy.logStep('Log-Ansicht anzeigen')
    cy.visit('/tocco/e/action/log')
    cy.waitForLoaded()

    cy.getByAttr('form-field').should('exist')
    cy.getByAttr('form-field').should('have.length', 3)

    cy.logStep('Zeilenanzahl verändern')
    cy.get('#fileCountField').click()
    cy.focused().clear().type(123)
    cy.focused().type('Test')
    cy.get('#fileCountField').should('have.value', '123')

    cy.logStep('Host kann nicht verändert werden')
    cy.get('#hostnameField').click({force: true})
    cy.get('#hostnameField').should('be.disabled')
    cy.get('#hostnameField').should('have.not.focus')

    cy.logStep('Log neu laden')
    cy.contains('Log-Datei')
    cy.contains('Zeilenanzahl')
    cy.contains('Host')
    cy.contains('Neu laden').click()
  })
})
