describe('Konfiguration neu laden - Aktion', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.login()
  })
  it('Konfiguration neu laden', () => {
    cy.logStep('Aktion über Menü öffnen')
    cy.visit('/tocco')
    cy.waitForDashboardLoaded()
    cy.get('body').type('{ctrl}{alt}m')
    cy.getByAttr('menu-tab-system').click()
    cy.get('body').get('[data-cy="admin-menuitem-reload-configuration"] [data-quick-navigation]').click()
    cy.logStep('ACL anwählen')
    cy.get('input[id="editable-value-acl"]').click()
    cy.logStep('Ausgwählte neu laden')
    cy.get('button[id="reload-selected-button"]').click()
  })
})
