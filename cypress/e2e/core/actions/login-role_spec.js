describe('Login-Rollen - Aktion', () => {
  before(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.login()
  })

  const areCheckboxesChecked = (roleKeys, buKeys) => {
    for (const roleKey of roleKeys) {
      for (const buKey of buKeys) {
        cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKey}-${buKey}]`).should('be.checked')
      }
    }
  }

  const areCheckboxesUnchecked = (roleKeys, buKeys) => {
    for (const roleKey of roleKeys) {
      for (const buKey of buKeys) {
        cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKey}-${buKey}]`).should('not.be.checked')
      }
    }
  }

  it('Login-Rollen', () => {
    cy.task('db:seed:login-role:seed').then(({principalKey, roleKeys, buKeys}) => {
      cy.logStep('Ein Login öffnen')
      cy.visit(`/tocco/e/Principal/${principalKey}`)
      cy.waitForLoaded()

      cy.logStep('Aktion "Login-Rollen" ausführen')
      cy.clickActionMenuItem('login-role')

      cy.logStep('Suche nach Bezeichnung ausführen')
      cy.getByAttr('modal-content').getValueByLabel('Bezeichnung').type('Veranstaltung')
      cy.getByAttr('modal-content').getByAttr('role-row').should('have.length', 2)

      cy.logStep('Event Manager an-/abwählen selektiert für alle Geschäftsbereiche')
      cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKeys.eventmanager}]`).click()
      areCheckboxesChecked([roleKeys.eventmanager], [buKeys.test1, buKeys.test2])
      cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKeys.eventmanager}]`).click()
      areCheckboxesUnchecked([roleKeys.eventmanager], [buKeys.test1, buKeys.test2])

      cy.logStep('Alle an-/abwählen selektiert alle Rollen für alle Geschäftsbereiche')
      cy.getByAttr('modal-content').find(`input[id=checkbox-all-${buKeys.test2}]`).click()
      areCheckboxesChecked([roleKeys.eventmanager, roleKeys.eventguest], [buKeys.test2])
      cy.getByAttr('modal-content').find(`input[id=checkbox-all-${buKeys.test2}]`).click()
      areCheckboxesUnchecked([roleKeys.eventmanager, roleKeys.eventguest], [buKeys.test2])

      cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKeys.eventmanager}]`).click()
      cy.getByAttr('modal-content').find(`input[id=checkbox-all-${buKeys.test1}]`).click()

      cy.logStep('Suche nach Bezeichnung welche keine Resultate liefert (z.B. xyz)')
      cy.getByAttr('modal-content').getValueByLabel('Bezeichnung').type('xyz')

      cy.logStep('"Keine Rollen gefunden" Hinweis wird angezeigt')
      cy.getByAttr('modal-content').contains('Keine Rollen gefunden').should('exist')

      cy.logStep('Hinweis für ausgeblendete Rollen wird angezeigt, solange Filter gesetzt ist')
      cy.getByAttr('modal-content').contains('Achtung: 3 gesetzte Berechtigungen ausgeblendet').should('exist')
      cy.getByAttr('modal-content').contains('Filter zurücksetzen').click()
      cy.getByAttr('modal-content').contains('Achtung: 3 gesetzte Berechtigungen ausgeblendet').should('not.exist')

      cy.logStep('Speichern')
      cy.getByAttr('modal-content').contains('Speichern').click()

      cy.logStep('Aktion "Login-Rollen" nochmals ausführen und Inhalte auf Richtigkeit überprüfen')
      cy.clickActionMenuItem('login-role')

      areCheckboxesChecked([roleKeys.eventmanager], [buKeys.test1, buKeys.test2])
      areCheckboxesChecked([roleKeys.eventguest], [buKeys.test1])

      // check login roles written to db (not perfect because we can not check which one test1 is related to)
      cy.logStep('Auf DB überprüfen, ob Rollen für richtige Beschäftsbereiche gesetzt sind')
      cy.task('db:assert:entity', {
        model: 'Principal',
        pk: principalKey,
        data: {
          'relLogin_role.relRole.unique_id': ['eventguest', 'eventmanager'],
          'relLogin_role.relBusiness_unit.unique_id': ['test1']
        }
      })

      cy.logStep('Erweiterte Suche öffnen')
      cy.getByAttr('modal-content').getByAttr('icon-chevron-down').click()

      cy.logStep('Nach Rollentyp suchen')
      cy.getByAttr('modal-content').getValueByLabel('Rollentyp').type('Inhalt{enter}')
      cy.getByAttr('modal-content').getValueByLabel('Rollentyp').getByAttr('icon-chevron-up').click()
      cy.getByAttr('modal-content').getByAttr('role-row').contains('Inhalt').should('exist')
      cy.getByAttr('modal-content').getByAttr('role-row').contains('System').should('not.exist')
      cy.getByAttr('modal-content').getByAttr('role-row').contains('Gast').should('not.exist')
      cy.getByAttr('modal-content').getByAttr('role-row').contains('Manager').should('not.exist')
      cy.getByAttr('modal-content').getValueByLabel('Rollentyp').getByAttr('icon-times').click()

      cy.logStep('Nach Geschäftsbereich suchen')
      cy.getByAttr('modal-content').getValueByLabel('Geschäftsbereich').type('test1{enter}')
      cy.getByAttr('modal-content').getValueByLabel('Geschäftsbereich').getByAttr('icon-chevron-up').click()
      cy.getByAttr('modal-content').find(`input[id=checkbox-all-${buKeys.test2}]`).should('not.exist')
      cy.getByAttr('modal-content').getValueByLabel('Geschäftsbereich').getByAttr('icon-times').click()

      cy.logStep('Nach Selektierten suchen')
      cy.getByAttr('modal-content').getValueByLabel('Nur Selektierte anzeigen').type('Ja{enter}')
      cy.getByAttr('modal-content').getByAttr('role-row').should('have.length', 2)
      cy.getByAttr('modal-content').getValueByLabel('Nur Selektierte anzeigen').getByAttr('icon-times').click()

      cy.logStep('Aktion über "Abbrechen" schliessen')
      cy.contains('Abbrechen').click()
      cy.getByAttr('modal-content').should('not.exist')

      // check automatic role blocker
      cy.task('db:seed:login-role:automatic-login-role', {principalKey, roleId: 'eventguest'})
      cy.clickActionMenuItem('login-role')

      // none of the multi checkboxes should influence automatic role
      cy.getByAttr('modal-content').find(`input[id=checkbox-all]`).click().click()
      cy.getByAttr('modal-content').find(`input[id=checkbox-all-${buKeys.test1}]`).click().click()
      cy.getByAttr('modal-content').find(`input[id=checkbox-${roleKeys.eventguest}]`).click().click()
      cy.getByAttr('modal-content')
        .find(`input[id=checkbox-${roleKeys.eventguest}-${buKeys.test1}]`)
        .should('be.checked')
        .should('be.disabled')
    })
  })
})
