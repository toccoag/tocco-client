describe('Resourcenkalender - Aktion', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  it(`TOCRT-76 - Aktionen "Dozentenbuchungskalender" und "Belegungskalender`, () => {
    cy.task('db:seed:resource-calendar:tocrt76').then(data => {
      const {user, room} = data
      cy.login()

      cy.logStep('Dozentenbuchungskalender öffnen')
      cy.visit('/tocco/e/User/list')
      cy.waitForListLoaded()

      cy.get('table').toggleMultiSelection(0)

      cy.getByAttr('btn-action-actions').click()
      cy.getByAttr('menuitem-lecturer-resource-scheduler').click()

      cy.logStep('Selektiere Resourcen sind schon ausgewählt')
      cy.hasResource('User', user.pk, 'Müller, Hans')
      cy.showMonthView()
      cy.calendarNavigateNext()
      cy.hasEvent('User', user.pk, 1, '15 Uhr - 16 Uhr', 'Deutsch Kurs')

      cy.logStep('Dozenten sind ausgeklappt')
      cy.getByAttr('panel-wrapper-lecturer', 'panel-body-lecturer').should('be.visible')
      cy.getByAttr('panel-wrapper-participant', 'panel-body-participant').should('not.be.visible')
      cy.getByAttr('panel-wrapper-event', 'panel-body-event').should('not.be.visible')
      cy.getByAttr('panel-wrapper-room', 'panel-body-room').should('not.be.visible')
      cy.getByAttr('panel-wrapper-appliance', 'panel-body-appliance').should('not.be.visible')

      cy.logStep('Belegungskalender öffnen')
      cy.visit('/tocco/e/Room/list')
      cy.waitForListLoaded()

      cy.get('table').toggleMultiSelection(0)

      cy.getByAttr('btn-action-actions').click()
      cy.getByAttr('menuitem-room-resource-scheduler').click()

      cy.logStep('Selektiere Resourcen sind schon ausgewählt')
      cy.hasResource('Room', room.pk, 'raum')
      cy.showMonthView()
      cy.calendarNavigateNext()
      cy.hasEvent('Room', room.pk, 1, '14:30 - 16:30', 'Deutsch Kurs')

      cy.logStep('Räume sind ausgeklappt')
      cy.getByAttr('panel-wrapper-room', 'panel-body-room').should('be.visible')
      cy.getByAttr('panel-wrapper-lecturer', 'panel-body-lecturer').should('not.be.visible')
      cy.getByAttr('panel-wrapper-participant', 'panel-body-participant').should('not.be.visible')
      cy.getByAttr('panel-wrapper-event', 'panel-body-event').should('not.be.visible')
      cy.getByAttr('panel-wrapper-appliance', 'panel-body-appliance').should('not.be.visible')
    })
  })
})
