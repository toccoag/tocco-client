describe('Veranstaltungsplattform - Widget', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  it('Anmeldung eines existierten Benutzers an einer Veranstaltung', () => {
    cy.task('db:seed:event-registration').then(({extranetWidgetConfig, login, invoicePaymentMethodKey}) => {
      cy.logStep('Anmelden')
      cy.login({
        username: login.username,
        password: login.password
      })

      const widgetConfigKey = extranetWidgetConfig.unique_id
      const invoicePaymentMethod = invoicePaymentMethodKey
      const registerForEvent = label => {
        // open detail for event
        cy.contains(label).click()

        // in detail open registration action
        cy.getByAttr('btn-action-event-registration-action').first().click()

        // in action submit as for the existing user all mandatory fields are already filled
        cy.getByAttr('sumbit-event-registration').click()
      }

      cy.logStep('Veranstaltungsplattform öffnen')
      cy.logStep('An einer Veranstaltung anmelden')
      cy.visitWidget(widgetConfigKey)
      registerForEvent('Simple Extranet Veranstaltung')

      // go back to list
      cy.logStep('Veranstaltungsplattform nochmals öffnen (od. Zurück zur Listenansicht)')
      cy.visitWidget(widgetConfigKey)

      // same user cannot be register a second time for the same event
      cy.logStep('An der gleichen Veranstaltung nochmals anmelden')
      registerForEvent('Simple Extranet Veranstaltung')
      cy.logStep('Anmeldung schlägt fehlt, da schon angemeldet')
      cy.getByAttr('toaster-title').should('has.text', 'Validation fehlgeschlagen')
      cy.getByAttr('toaster-content').should('has.text', 'Sie sind bereits an dieser Veranstaltung angemeldet.')

      // go back to list
      cy.logStep('Listansicht nochmals öffnen')
      cy.visitWidget(widgetConfigKey)

      // open detail for event
      cy.logStep('In eine ausgebuchte Veranstaltung navigieren')
      cy.contains('volle Veranstaltung').click()

      // in detail open registration action
      cy.logStep('Anmelden ist nicht möglich')
      cy.getByAttr('btn-action-event-registration-action').should('not.exist')

      // go back to list
      cy.logStep('Listenansicht öffnen')
      cy.visitWidget(widgetConfigKey)

      cy.logStep('An einer Veranstaltung mit E-Payment anmelden')
      registerForEvent('Epayment Veranstaltung')

      // check content of payment page
      cy.logStep('Zahlungsseite mit Artikelübersicht wird korrekt dargestellt')
      cy.contains('Artikel')
      cy.contains('Buch')
      cy.contains('1x')
      cy.contains('32.50')
      cy.contains('Zahlungsmethode')
      cy.contains('Zahlung auf Rechnung')

      // select payment method
      cy.logStep('Zahlungsmethode "Rechnung" auswählen')
      cy.log(invoicePaymentMethod)
      cy.getByAttr(`choice-answer-option-${invoicePaymentMethod}`).click()

      // finish payment
      cy.logStep('Abschliessen')
      cy.getByAttr('payment-provider-next').click()
    })
  })
})
