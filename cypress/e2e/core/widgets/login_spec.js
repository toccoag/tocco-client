describe('Login - Widget', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  it('Login', () => {
    cy.task('db:seed:login').then(response => {
      cy.logStep('Widget öffnen')
      cy.visitWidget(response.uniqueId)

      cy.get('[data-cy="input-login-form_user"]', {timeout: 15000}).should('be.visible')
      cy.getByAttr('btn-login-form_login').should('exist')
      cy.getByAttr('btn-login-form_request').should('exist')

      // should not be possible to click login button
      cy.logStep('"Anmelde" Button erst klickbar, wenn Benutzername und Passwort ausgefüllt ist')
      cy.getByAttr('input-login-form_user').type('{selectall}{del}some_user')
      cy.getByAttr('btn-login-form_login').should('be.disabled')
      cy.getByAttr('input-login-form_user').type('{selectall}{del}')
      cy.getByAttr('input-login-form_password').type('{selectall}{del}12345')
      cy.getByAttr('btn-login-form_login').should('be.disabled')
      cy.getByAttr('btn-login-form_request')

      // should send login request
      cy.logStep('Anmeldung schlägt fehl für einen nicht existierenden Benutzer')
      cy.getByAttr('input-login-form_user').type('{selectall}{del}some_user')
      cy.getByAttr('input-login-form_password').type('{selectall}{del}Test_pw1')
      cy.getByAttr('btn-login-form_login').click()
      cy.contains('Login fehlgeschlagen.')

      // should request password from request page
      cy.logStep('"Password vergessen?" anwählen')
      cy.contains('Passwort vergessen?')
      cy.logStep('E-Mail Adresse eingeben und Passwort anfordern')
      cy.getByAttr('btn-login-form_request').click()
      cy.getByAttr('input-password-request').type('{selectall}{del}test@testmail.com')
      cy.getByAttr('btn-password-request_submit').click()
      cy.contains('Passwort wurde per E-Mail versendet')

      // should change to login page on abort of password request
      cy.logStep('Zurück zum Login und Passwort anfragen abbrechen')
      cy.getByAttr('btn-login-form_request').click()
      cy.getByAttr('btn-password-request_abort').click()
      cy.getByAttr('input-login-form_user').should('be.visible')
      cy.getByAttr('input-login-form_password').should('be.visible')
    })
  })
})
