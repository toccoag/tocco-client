import {format, startOfToday} from 'date-fns'

describe('Adress-Aktualisierung - Widget', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })

  it('Adresse aktualisieren', () => {
    cy.task('db:seed:address-update', {editable: true}).then(({widgetConfig, user, address, login}) => {
      cy.logStep('Anmelden')
      cy.login({
        username: login.username,
        password: login.password
      })
      cy.logStep('Widget öffnen und Adresse überprüfen')
      cy.visitWidget(widgetConfig.unique_id)
      cy.get('#input-addressForm-firstname').should('have.value', 'Susanne')
      cy.get('#input-addressForm-lastname').should('have.value', 'Peters')
      cy.get('#input-addressForm-relGender').hasValueSelected('Weiblich')

      cy.logStep('Daten ändern')
      cy.get('#input-addressForm-birthdate').typeDate('12.04.1988')
      cy.get('#input-addressForm-addressfield_c').should('have.value', 'Schaffhauserstrasse')
      cy.get('#input-addressForm-locationfield_c').should('have.value', '8006')
      cy.get(
        `#${CSS.escape('input-addressForm-relAddress_user[publication].relAddress.relCountry_c')}`
      ).hasValueSelected('Schweiz')

      cy.get('#input-addressForm-addressfield_c').clear().type('Greutensberg')
      cy.get('.react-autosuggest__suggestion--first').click()
      cy.get('#input-addressForm-locationfield_c').should('have.value', '9514')

      cy.get('#input-addressForm-email').type('susanne.peters@tocco.ch')
      cy.get('#input-addressForm-phone_private').type('0761234567')
      cy.get('#input-addressForm-phone_private').should('have.value', '+41 76 123 45 67')

      cy.logStep('Speichern')
      cy.getByAttr('btn-detail-form_submit').first().click()
      cy.getByAttr('toaster-title').should('has.text', 'Gespeichert')

      cy.logStep('Daten wurden angepasst')
      cy.task('db:assert:entity', {
        model: 'User',
        pk: user.pk,
        data: {
          birthdate: '1988-04-12',
          phone_private: '+41761234567',
          email: 'susanne.peters@tocco.ch'
        }
      })

      cy.task('db:assert:entity', {
        model: 'Address',
        pk: address.pk,
        data: {
          zip_c: '9514',
          city_c: 'Wuppenau',
          canton: 'TG',
          address_c: 'Greutensberg'
        }
      })
    })
  })

  it('Adresse ansehen mit Bearbeitung deaktiviert', () => {
    cy.task('db:seed:address-update', {editable: false}).then(({widgetConfig, login}) => {
      cy.logStep('Anmelden')
      cy.login({
        username: login.username,
        password: login.password
      })
      cy.logStep('Widget öffnen und Adresse überprüfen')
      cy.visitWidget(widgetConfig.unique_id)
      cy.get('#input-addressForm-firstname').should('have.text', 'Susanne')
      cy.get('#input-addressForm-lastname').should('have.text', 'Peters')
      cy.get('#input-addressForm-relGender').should('have.text', 'Weiblich')

      cy.get('#input-addressForm-addressfield_c').should('have.value', 'Schaffhauserstrasse')
      cy.get('#input-addressForm-locationfield_c').should('have.value', '8006')
      cy.get(`#${CSS.escape('input-addressForm-relAddress_user[publication].relAddress.relCountry_c')}`).should(
        'have.text',
        'Schweiz'
      )

      cy.logStep('Daten können nicht geändert und gespeichert werden')
      cy.getByAttr('btn-detail-form_submit').should('not.exist')
    })
  })

  it('Bestimmungen (AGB & Datenschutz)', () => {
    cy.task('db:seed:terms-conditions').then(({widgetConfig, privacy, conditions}) => {
      cy.logStep('Bestimmungen ohne Checkboxen zum Adress-Aktualisierungs-Widget hinzufügen')
      cy.logStep('Anmelden')
      cy.login()
      cy.logStep('Widget öffnen und Bestimmungen kontrollieren')
      cy.visitWidget(widgetConfig.unique_id)
      // privacy protections and terms condition should be visible without any validation
      cy.contains('Bitte lesen Sie die AGBs genau durch.')
      cy.contains('Bitte lesen Sie die Datenschutzbestimmungen genau durch.')

      cy.logStep('Speichern ohne Validierung ist möglich')
      cy.getByAttr('btn-detail-form_submit').last().click()
      cy.getByAttr('toaster-title').should('has.text', 'Gespeichert')

      // no confirmation should have been created
      cy.logStep('Ins Admin zur Person wechseln')
      cy.login()
      cy.visit('/tocco/e/User/1/detail')
      cy.logStep('Es wurden keine Einwilligungen erfasst')
      cy.getByAttr('box-relation-box-Terms_condition_confirmation').click()
      cy.getByAttr('box-relation-preview', 'tbl').hasNoData()

      cy.logStep('Checkboxen zu den Bestimmungen hinzufügen')
      cy.task('db:seed:terms-conditions:add-checkbox', privacy)
      cy.task('db:seed:terms-conditions:add-checkbox', conditions)
      cy.logStep('Widget öffnen')
      cy.visitWidget(widgetConfig.unique_id)

      // should show checkboxes and enable mandatory validation
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[conditions]"]').should('exist')
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[privacy_protection]"]').should('exist')

      cy.logStep('Speichern ohne Anwählen der Bestimmungen geht nicht')
      cy.getByAttr('btn-detail-form_submit').last().click()

      cy.get(
        '[data-cy="errorlist-input-addressForm-relTerms_condition_confirmation[conditions]"] [data-cy=erroritem-0]'
      ).should('exist')
      cy.get(
        // eslint-disable-next-line max-len
        '[data-cy="errorlist-input-addressForm-relTerms_condition_confirmation[privacy_protection]"] [data-cy=erroritem-0]'
      ).should('exist')

      cy.logStep('Bestimmungen sind richtig verlinkt')
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[conditions]"] + span a').should(
        'have.attr',
        'href',
        'https://tocco.ch/conditions'
      )
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[conditions]"] + span a').should(
        'have.text',
        'AGBs'
      )

      cy.get('[id="input-addressForm-relTerms_condition_confirmation[privacy_protection]"] + a').should(
        'have.attr',
        'href',
        'https://tocco.ch/privacy-protection'
      )
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[privacy_protection]"] + a').should(
        'have.text',
        'Bitte lesen Sie die Datenschutzbestimmungen genau durch.'
      )

      cy.logStep('Bestimmungen anwählen')
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[conditions]').click()
      cy.get('[id="input-addressForm-relTerms_condition_confirmation[privacy_protection]"]').click()

      cy.logStep('Speichern')
      cy.getByAttr('btn-detail-form_submit').last().click()
      cy.getByAttr('toaster-title').should('has.text', 'Gespeichert')

      // should have created confirmation
      const today = format(startOfToday(), 'dd.MM.yyyy')

      cy.logStep('Ins Admin zur Person wechseln')
      cy.visit('/tocco/e/User/1/detail')
      cy.logStep('Einwilligungen für beide Bestimmungen wurden erstellt.')
      cy.getByAttr('box-relation-box-Terms_condition_confirmation').click()
      cy.getByAttr('box-relation-preview')
        .getTableRow(0)
        .getByAttr('tbl-cell-relTerms_condition')
        .should('have.text', 'Datenschutz')
      cy.getByAttr('box-relation-preview')
        .getTableRow(0)
        .getByAttr('tbl-cell-relWidget_config')
        .should('have.text', 'address-update / Adress-Aktualisierung')

      cy.getByAttr('box-relation-preview')
        .getTableRow(0)
        .getByAttr('tbl-cell-accepted_date')
        .should('contain.text', today)
      cy.getByAttr('box-relation-preview')
        .getTableRow(1)
        .getByAttr('tbl-cell-relTerms_condition')
        .should('have.text', 'AGB')
      cy.getByAttr('box-relation-preview')
        .getTableRow(1)
        .getByAttr('tbl-cell-relWidget_config')
        .should('have.text', 'address-update / Adress-Aktualisierung')
      cy.getByAttr('box-relation-preview')
        .getTableRow(1)
        .getByAttr('tbl-cell-accepted_date')
        .should('contain.text', today)
    })
  })
})
