describe('Teilnehmer- und Mailingliste - Widget', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
  })
  it('Teilnehmer- und Mailingliste', () => {
    cy.task('db:seed:mailing-list-widget').then(({widgetConfig, login}) => {
      cy.logStep('Anmelden')
      cy.login({
        username: login.username,
        password: login.password
      })
      cy.logStep('Nur angemeldete Veranstaltungen sind sichtbar')
      cy.visitWidget(widgetConfig.unique_id)
      // only events with a registration for the user appear
      cy.getByAttr('tbl-list-row').should('have.length', 1)
      cy.getByAttr('checkbox-list-selection').click()
      cy.logStep('"Teilnehmerliste" generieren')
      cy.getByAttr('btn-action-Teilnehmerliste').click()
      cy.getByAttr('btn-simpleForm-submit').click()
      cy.getByAttr('toaster-title').should('include.text', 'Der Report wurde generiert')
      cy.logStep('In eine Veranstlatung navigieren')
      cy.contains('Cypress Test Event').click()
      cy.logStep('Nur publizierte Anmeldungen sind sichtbar')
      // only published registrations appear
      cy.getByAttr('tbl-list-row').should('have.length', 2)
      cy.logStep('Alle Anmeldungen anwählen')
      cy.getByAttr('checkbox-list-selection').click({multiple: true})
      cy.logStep('"E-Mail senden" Aktion ausführen')
      cy.getByAttr('btn-action-mailing-list-mail-action').click()
      cy.logStep('CC ausfüllen')
      cy.getByAttr('form-field').find(`input[id="input-mailing-list-action-cc"]`).type('testWidget@cc.ch')
      cy.getByAttr('btn-send-mail').click()
      cy.getByAttr('toaster-title').should('include.text', 'Aktion wurde ausgeführt')

      cy.logStep('Ins Admin wechseln')
      cy.logout()
      cy.login()
      cy.visit('/tocco/e/Email_archive/list')
      cy.logStep('E-Mail öffnen und überprüfen, dass E-Mail an alle Teilnehmer & CC versendet wurde')
      cy.getByAttr('tbl-list-row').should('have.length', 4)
      cy.getByAttr('tbl-list-row').contains('testUser@tocco.ch').should('be.visible')
      cy.getByAttr('tbl-list-row').contains('testadditional@tocco.ch').should('be.visible')
      cy.getByAttr('tbl-list-row').contains('test0@tocco.ch').should('be.visible')
      cy.visit('/tocco/e/Output_job/list')
      cy.logStep('Ausgabe öffnen und überprüfen, dass Teilnehmerliste generiert wurde')
      cy.getByAttr('tbl-list-row').should('have.length', 1)
    })
  })
})
