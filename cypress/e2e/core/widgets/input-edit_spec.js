describe('Noteneingabe - Widget', () => {
  beforeEach(() => {
    cy.task('db:empty', undefined, {timeout: 180000})
    cy.task('db:seed:input-nodes')
  })

  it('Noteneingabe', () => {
    cy.task('db:seed:input-edit-widget').then(
      ({widgetConfig, responsibleLogin, responsibleUser, notResponsibleLogin}) => {
        cy.task('db:seed:input', {inputType: 'grades', userCount: 3, responsible: responsibleUser.pk})
        cy.logStep('Anmelden')
        cy.login({
          username: responsibleLogin.username,
          password: responsibleLogin.password
        })
        cy.logStep('"Prüfung bearbeiten" kann geöffnet werden')
        cy.visitWidget(widgetConfig.unique_id)
        cy.clickActionButton('exam-edit')
        cy.getByAttr('modal-content').contains('Prüfungen bearbeiten')
        cy.getByAttr('btn-modal-close').click()

        cy.logStep('Nach offen und abgeschlossenen Noteneingaben suchen')
        cy.getValueByLabel('Fach')
        cy.getValueByLabel('Veranstaltung')
        cy.getSelectByLabel('Status Noteneingabe').hasValueSelected('Offen')
        cy.getSelectByLabel('Status Noteneingabe').selectValue('Abgeschlossen')

        cy.listRowContains('grades - Grades')
        cy.listRowContains('input edit event')
        cy.listRowContains('Noten')
        cy.listRowContains('Offen')
        cy.listRowContains('15.06.2024')

        cy.logStep('Ins Detail einer offenen Noteneingabe navigieren')
        cy.getTableRow(0).navigateToDetail()

        cy.logStep('Inhalte überprüfen: noch keine Eingabe vorhanden')
        cy.getValueByLabel('Teilnehmer').contains('0')
        cy.getValueByLabel('Dispensiert').contains('0')
        cy.contains('Max. Noten-Ø def.').should('not.exist')
        cy.contains('Min. Noten-Ø def.').should('not.exist')
        cy.contains('Noten-Ø def.').should('not.exist')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('1: 0')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('2: 0')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('3: 0')

        cy.logStep('Noten eingeben starten')
        cy.clickActionButton('new_input_edit')
        cy.awaitInputEditData()
        cy.logStep('Jemanden dispensieren')
        cy.getByAttr('tbl-list-row').first().toggleInputEditDispense()
        cy.awaitInputEditCalculation()
        cy.logStep('Restliche Noten eingeben')

        cy.get('[data-cy*="tbl-cell"]').then(thing => {
          const ratingNodes = []
          Array.from(thing, t => t.dataset.cy.substring('tbl-cell-'.length))
            .filter(id => !isNaN(parseInt(id)) || id === 'rating')
            .forEach(id => {
              if (!ratingNodes.includes(id)) {
                ratingNodes.push(id)
              }
            })

          cy.getByAttr('tbl-list-row').eq(1).fillInputEditCell(ratingNodes[0], 4)
          cy.getByAttr('tbl-list-row').eq(1).fillInputEditCell(ratingNodes[1], 4)
          cy.getByAttr('tbl-list-row').eq(1).fillInputEditCell(ratingNodes[2], 4)
          cy.getByAttr('tbl-list-row').eq(1).hasInputEditCellValue('grade', '4.0')

          // fill last field second, so the average is not set to 5 early (6 + 4 / 2)
          cy.getByAttr('tbl-list-row').last().fillInputEditCell(ratingNodes[0], 6)
          cy.getByAttr('tbl-list-row').last().fillInputEditCell(ratingNodes[2], 5)
          cy.getByAttr('tbl-list-row').last().fillInputEditCell(ratingNodes[1], 4)
          cy.getByAttr('tbl-list-row').last().hasInputEditCellValue('grade', '5.0')
        })

        cy.logStep('Stammdaten prüfen')
        cy.clickActionButton('new_input_edit_info')
        cy.getByAttr('modal-content').getValueByLabel('Veranstaltung').should('have.text', 'input edit event')
        cy.getByAttr('modal-content').getValueByLabel('Fach').should('have.text', 'Grades')
        cy.getByAttr('modal-content').getValueByLabel('Noteneingabe-Typ').should('have.text', 'Noten')
        cy.getByAttr('modal-content').getValueByLabel('Status Noteneingabe').should('have.text', 'Offen')
        cy.getByAttr('btn-modal-close').click()

        cy.logStep('Zurück zum Detail navigieren')
        cy.getByAttr('btn-back').click()

        cy.logStep('Inhalte überprüfen: Eingabe ist erfasst und aktualisiert, die Durchschnitte sind berechnet')
        cy.getValueByLabel('Teilnehmer').contains('3')
        cy.getValueByLabel('Dispensiert').contains('1')
        cy.getValueByLabel('Max. Noten-Ø def.').contains('5')
        cy.getValueByLabel('Min. Noten-Ø def.').contains('4')
        cy.getValueByLabel('Noten-Ø def.').contains('4.5')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('1: 5')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('2: 4')
        cy.getValueByLabel('Prüfungen (Durchschnitt)').contains('3: 4.5')

        cy.logStep('Mit einem Benutzer einloggen, welcher keine Rechte auf die Noteneingabe hat')

        cy.logout()

        cy.login({
          username: notResponsibleLogin.username,
          password: notResponsibleLogin.password
        })

        cy.logStep('Der Benutzer sollte keine Daten sehen')
        cy.visitWidget(widgetConfig.unique_id)
        cy.hasNoData()
      }
    )
  })
})
