const getLabel = (subject, label) => {
  const selector = `label [title="${label}"]`
  if (subject) {
    return cy.wrap(subject).find(selector)
  } else {
    return cy.get(selector)
  }
}

Cypress.Commands.add('getValueByLabel', {prevSubject: 'optional'}, (subject, label) => {
  return getLabel(subject, label).next()
})

Cypress.Commands.add('getSelectByLabel', {prevSubject: 'optional'}, (subject, label) => {
  return getLabel(subject, label).next().getByAttr('form-field').find('span > div')
})
