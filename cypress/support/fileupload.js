const getUploadButton = (subject, fieldName) => {
  if (subject) {
    return cy.wrap(subject).getByAttr(`btn-upload-${fieldName}`)
  } else {
    return cy.getByAttr(`btn-upload-${fieldName}`)
  }
}

const uploadFile = (subject, fieldName, file) => {
  getUploadButton(subject, fieldName).selectFile(file, {
    action: 'drag-drop'
  })
}

Cypress.Commands.add('uploadFile', {prevSubject: 'optional'}, (subject, fieldName, file) => {
  uploadFile(subject, fieldName, file)
})

Cypress.Commands.add('uploadModifiedFile', {prevSubject: 'optional'}, (subject, fieldName, filePath, modifierFn) => {
  const pathParts = filePath.split('/')
  const fileName = pathParts[pathParts.length - 1]

  cy.readFile(filePath).then(fileContent => {
    const modifiedFileContent = modifierFn(fileContent)
    uploadFile(subject, fieldName, {
      contents: Cypress.Buffer.from(modifiedFileContent),
      fileName,
      lastModified: Date.now()
    })
  })
})
