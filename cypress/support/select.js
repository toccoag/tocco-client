Cypress.Commands.add('selectValue', {prevSubject: true}, (subject, value) => {
  cy.wrap(subject).click().focus().type('{downArrow}')
  cy.get('.MenuList').contains(value).click()
})

Cypress.Commands.add('selectRemoteValue', {prevSubject: true}, (subject, value) => {
  cy.wrap(subject).click().focus().type(value)
  cy.get('.MenuList').contains(value).click()
})

Cypress.Commands.add('hasValueSelected', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).should('contain', value)
})

Cypress.Commands.add('clearSelectedValue', {prevSubject: true}, subject => {
  cy.wrap(subject).within(() => {
    cy.get('[data-cy=btn-clear]').click()
  })
})

Cypress.Commands.add('openAdvancedSearch', {prevSubject: true}, subject => {
  cy.wrap(subject).within(() => {
    cy.get('[data-cy=btn-advancedSearch]').click()
  })
})

Cypress.Commands.add('openRemoteCreate', {prevSubject: true}, subject => {
  cy.wrap(subject).within(() => {
    cy.get('[data-cy=btn-remoteCreate]').click()
  })
})

Cypress.Commands.add('removeSelectedValue', {prevSubject: true}, (subject, value) => {
  cy.wrap(subject).within(() => {
    cy.get(`[aria-label="Remove ${value}"]`).click()
  })
})
