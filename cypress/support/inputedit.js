const getCell = (subject, cell) => {
  if (subject) {
    return cy.wrap(subject).getByAttr(`tbl-cell-${cell}`)
  } else {
    return cy.getByAttr(`tbl-cell-${cell}`)
  }
}

Cypress.Commands.add('awaitInputEditCalculation', () => {
  // calculation should be started and then finished
  // just checking if icon does not exist may miss the calculation being started
  cy.getByAttr('icon-circle-notch').should('exist')
  cy.get('[data-cy="icon-circle-notch"]', {timeout: 10000}).should('not.exist')
})

Cypress.Commands.add('awaitInputEditData', () => {
  cy.getByAttr('input-edit-table').should('exist')
  cy.getByAttr('tbl-list-row').should('exist')
})

Cypress.Commands.add('fillInputEditCell', {prevSubject: 'optional'}, (subject, cell, value) => {
  return getCell(subject, cell).type(value)
})

Cypress.Commands.add('focusInputEditCell', {prevSubject: 'optional'}, (subject, cell) => {
  return getCell(subject, cell).find('input').focus()
})

Cypress.Commands.add('toggleInputEditDispense', {prevSubject: 'optional'}, subject => {
  return getCell(subject, 'dispense').find('input').click()
})

Cypress.Commands.add('hasInputEditCellValue', {prevSubject: 'optional'}, (subject, cell, value) => {
  getCell(subject, cell).contains(value)
})

Cypress.Commands.add('hasEmptyInputEditCell', {prevSubject: 'optional'}, (subject, cell) => {
  getCell(subject, cell).should('have.text', '')
})

Cypress.Commands.add('hasFilledInputEditCell', {prevSubject: 'optional'}, (subject, cell) => {
  getCell(subject, cell).should('not.have.text', '')
})

Cypress.Commands.add('hasInputEditCellFocused', id => {
  cy.focused().should('have.attr', 'id', id)
})
