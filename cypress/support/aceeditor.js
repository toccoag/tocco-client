Cypress.Commands.add('hasCodeValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.ace_content').should('have.text', value)
  })
})

Cypress.Commands.add('clearCodeValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.ace_text-input').clear()
  })
})

Cypress.Commands.add('typeCodeValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.ace_text-input').type(value)
  })
})
