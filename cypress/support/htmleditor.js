// see: https://www.cypress.io/blog/2020/02/12/working-with-iframes-in-cypress/
const getIframeDocument = () => {
  return (
    cy
      .get('iframe')
      // Cypress yields jQuery element, which has the real
      // DOM element under property "0".
      // From the real DOM iframe element we can get
      // the "document" element, it is stored in "contentDocument" property
      // Cypress "its" command can access deep properties using dot notation
      // https://on.cypress.io/its
      .its('0.contentDocument')
      .should('exist')
  )
}

const getIframeBody = () => {
  // get the document
  return (
    getIframeDocument()
      // automatically retries until body is loaded
      .its('body')
      .should('not.be.undefined')
      // wraps "body" DOM element to allow
      // chaining more Cypress commands, like ".find(...)"
      .then(cy.wrap)
  )
}

const waitForEditorToBeReady = () => {
  cy.get('.edi').should('exist')
  cy.get('.edi_button.edi_button__bold').should('not.have.class', 'edi_button_disabled')
  getIframeBody().should('have.class', 'edi_editable')
}

Cypress.Commands.add('typeHtmlValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    waitForEditorToBeReady()
    getIframeBody().type(value)
  })
})

Cypress.Commands.add('hasHtmlValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    waitForEditorToBeReady()
    getIframeBody().should('have.text', value)
  })
})

Cypress.Commands.add('clearHtmlValue', {prevSubject: true}, subject => {
  return cy.wrap(subject).within(() => {
    waitForEditorToBeReady()
    getIframeBody().clear()
  })
})

Cypress.Commands.add('toggleSourceEditing', {prevSubject: true}, subject => {
  return cy.wrap(subject).within(() => {
    cy.get('.edi_button__source').click()
  })
})

Cypress.Commands.add('assertSourceEditing', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    if (value) {
      cy.get('.edi_button__source.edi_button_on').should('exist')
      cy.get('.edi_button__source.edi_button_off').should('not.exist')
    } else {
      cy.get('.edi_button__source.edi_button_on').should('not.exist')
      cy.get('.edi_button__source.edi_button_off').should('exist')
    }
  })
})

Cypress.Commands.add('typeHtmlSourceValue', {prevSubject: true}, (subject, value, options = {}) => {
  return cy.wrap(subject).within(() => {
    cy.get('.CodeMirror textarea').type(value, {force: true, ...options})
  })
})

Cypress.Commands.add('hasHtmlSourceValue', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.CodeMirror .CodeMirror-code .CodeMirror-line').should($div => {
      const cleanValue = value.replace(/\s/g, '')
      const cleanSourceValue = Cypress.$($div).text().replace(/\s/g, '')
      expect(cleanSourceValue).to.have.string(cleanValue)
    })
  })
})
