Cypress.Commands.add('typeDate', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.react-datepicker__input').focus().type(value, {force: true})
  })
})

Cypress.Commands.add('hasDate', {prevSubject: true}, (subject, value) => {
  return cy.wrap(subject).within(() => {
    cy.get('.react-datepicker__input').should('have.value', value)
  })
})

Cypress.Commands.add('clearDate', {prevSubject: true}, subject => {
  return cy.wrap(subject).within(() => {
    cy.get('[data-cy=btn-clear]').click()
  })
})
