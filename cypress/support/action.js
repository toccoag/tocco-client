Cypress.Commands.add('clickActionButton', action => {
  cy.getByAttr(`btn-action-${action}`).click()
})

Cypress.Commands.add('clickActionMenuItem', action => {
  cy.clickActionButton('actions')
  cy.getByAttr(`menuitem-${action}`).click()
})

Cypress.Commands.add('clickOutputMenuItem', action => {
  cy.clickActionButton('output')
  cy.getByAttr(`menuitem-${action}`).click()
})
