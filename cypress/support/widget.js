const WidgetBaseUrl = '/widget-server'

Cypress.Commands.add('visitWidget', (widgetConfigKey, suffix = '') => {
  cy.visit(`${WidgetBaseUrl}/?key=${widgetConfigKey}#${suffix}`)
})
