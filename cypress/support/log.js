Cypress.Commands.add('logSpec', () => {
  cy.task('log:spec', {cypressTest: Cypress.currentTest, spec: Cypress.spec})
})

Cypress.Commands.add('logTest', () => {
  cy.task('log:test', {cypressTest: Cypress.currentTest, spec: Cypress.spec})
})

Cypress.Commands.add('logStep', message => {
  Cypress.log({
    name: 'logStep',
    displayName: 'Step',
    message: `${message}`
  })
  cy.task('log:step', message)
})

before(() => {
  cy.logSpec()
})

beforeEach(() => {
  cy.logTest()
})
