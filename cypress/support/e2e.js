// type definitions for Cypress object "cy"
/// <reference types="cypress" />

// type definitions for custom commands
/// <reference types="." />

import './aceeditor'
import './action'
import './bootstrap'
import './commands'
import './datepicker'
import './duration'
import './fileupload'
import './form'
import './htmleditor'
import './inputedit'
import './log'
import './resourcecalendar'
import './select'
import './table'
import './widget'
