Cypress.Commands.add('login', (options = {}) => {
  const {backendUrl = '', username = Cypress.env('USER'), password = Cypress.env('PASSWORD')} = options
  cy.log('Login into Tocco')
  cy.request({
    url: `${backendUrl}/nice2/session`,
    method: 'POST'
  }).then(request => {
    if (request.body?.success !== true) {
      cy.log(`Logging in... username: ${username}`)
      cy.request({
        url: `${backendUrl}/nice2/login`,
        body: {username, password},
        method: 'POST',
        form: true
      }).then(req => {
        if (req.status === 200) {
          cy.log('Logged in')
        } else {
          cy.log('Could not log in')
        }
      })
    } else {
      cy.log('Already logged in')
    }
  })
})

Cypress.Commands.add('logout', (options = {}) => {
  const {backendUrl = ''} = options
  cy.log('Logout from Tocco')
  cy.request({
    url: `${backendUrl}/nice2/logout`,
    method: 'POST'
  })
})

Cypress.Commands.add('waitForLoaded', () => {
  cy.contains('[data-cy="btn-user"]', 'tocco-test', {timeout: 30000}) // wait till username is shown in top right corner
})

Cypress.Commands.add('waitForDashboardLoaded', () => {
  cy.waitForLoaded()
  cy.getByAttr('infobox-old_admin_link')
})

Cypress.Commands.add('waitForListLoaded', () => {
  cy.waitForLoaded()
})

before(() => {
  cy.intercept({resourceType: /xhr|fetch/}, {log: false})
})
