Cypress.Commands.add('getByAttr', {prevSubject: 'optional'}, (subject, ...attrs) => {
  const selector = attrs.map(attr => `[data-cy="${attr}"]`).join(' ')
  if (subject) {
    return cy.wrap(subject).find(selector)
  }

  return cy.get(selector)
})

Cypress.Commands.add('breadcrumbsIs', expectedItems => {
  return cy
    .getByAttr('breadcrumb-item')
    .should('have.length', expectedItems.length)
    .each((item, index) => {
      cy.wrap(item).should('contain.text', expectedItems[index])
    })
})

Cypress.Commands.add('isInViewport', {prevSubject: true}, subject => {
  return cy.get(subject).should($el => {
    const bottom = Cypress.$(cy.state('window')).height()
    const right = Cypress.$(cy.state('window')).width()
    const rect = $el[0].getBoundingClientRect()

    expect(rect).to.satisfy(r => r.top >= 0 && r.top <= bottom && r.left >= 0 && r.left <= right)
  })
})

Cypress.Commands.add('isNotInViewport', {prevSubject: true}, subject => {
  return cy.get(subject).should($el => {
    const bottom = Cypress.$(cy.state('window')).height()
    const right = Cypress.$(cy.state('window')).width()
    const rect = $el[0].getBoundingClientRect()

    expect(rect).to.satisfy(r => r.top < 0 || r.top > bottom || r.left < 0 || r.left > right)
  })
})
