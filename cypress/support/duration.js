Cypress.Commands.add('typeDuration', {prevSubject: true}, (subject, {hours, minutes} = {}) => {
  return cy.wrap(subject).within(() => {
    if (hours) {
      cy.get('[data-cy=input-hours]').type(hours)
    }

    if (minutes) {
      cy.get('[data-cy=input-minutes]').type(minutes)
    }
  })
})

Cypress.Commands.add('hasDuration', {prevSubject: true}, (subject, {hours, minutes} = {}) => {
  return cy.wrap(subject).within(() => {
    if (hours) {
      cy.get('[data-cy=input-hours]').should('have.value', hours)
    }

    if (minutes) {
      cy.get('[data-cy=input-minutes]').should('have.value', minutes)
    }
  })
})

Cypress.Commands.add('clearDuration', {prevSubject: true}, subject => {
  return cy.wrap(subject).within(() => {
    cy.get('[data-cy=input-hours]').clear()
    cy.get('[data-cy=input-minutes]').clear()
  })
})
