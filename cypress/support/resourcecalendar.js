const getResourceDataId = (type, id) => `[data-resource-id="${id}${type}"]`

/**
 * Types: User, Room, Appliance, Event
 */

Cypress.Commands.add('hasResource', (type, id, value) => {
  cy.get(`.fc-datagrid-cell${getResourceDataId(type, id)}`).should('has.text', value)
})

Cypress.Commands.add('hasEvent', (type, id, index, time, title) => {
  const selector = `.fc-timeline-lane${getResourceDataId(type, id)} .fc-timeline-event-harness:nth-child(${index})`
  cy.get(`${selector} .fc-event-time`).should('has.text', time)
  cy.get(`${selector} .fc-event-title`).should('has.text', title)
})

Cypress.Commands.add('showMonthView', () => {
  return cy.getByAttr('calendar-navigation', 'btn-calendar-month').click()
})

Cypress.Commands.add('calendarNavigateNext', () => {
  return cy.getByAttr('calendar-navigation', 'btn-calendar-next').click()
})

Cypress.Commands.add('calendarNavigatePrev', () => {
  return cy.getByAttr('calendar-navigation', 'btn-calendar-prev').click()
})
