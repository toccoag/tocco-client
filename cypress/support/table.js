Cypress.Commands.add('getTableRow', {prevSubject: 'optional'}, (subject, rowIndex) => {
  const selector = `[data-cy=tbl-list-row]:nth-child(${rowIndex + 1})`
  if (subject) {
    return cy.wrap(subject).find(selector)
  }

  return cy.get(selector)
})

Cypress.Commands.add('listRowContains', value => {
  cy.getByAttr('tbl-list-row').within(() => {
    cy.contains(value)
  })
})

Cypress.Commands.add('getRowsContaining', value => {
  cy.getByAttr('tbl-list-row').filter(`:contains("${value}")`)
})

Cypress.Commands.add('selectRows', {prevSubject: true}, subject => {
  cy.wrap(subject).getByAttr('checkbox-list-selection').click({multiple: true})
})

Cypress.Commands.add('navigateToDetail', {prevSubject: true}, subject => {
  cy.wrap(subject).getByAttr('tbl-navigation-arrow', 'icon-arrow-right').click()
})

Cypress.Commands.add('hasNoData', {prevSubject: 'optional'}, subject => {
  cy.wrap(subject).getByAttr('tbl-list-no-data-row').should('exist')
})

Cypress.Commands.add('toggleSingleSelection', {prevSubject: true}, (subject, rowIndex) => {
  cy.wrap(subject).within(() => {
    cy.get(`[data-cy=tbl-list-row]:nth-child(${rowIndex + 1}) [data-cy=tbl-cell-single-selection] input`).click()
  })
})

Cypress.Commands.add('toggleMultiSelection', {prevSubject: true}, (subject, rowIndex) => {
  cy.wrap(subject).within(() => {
    cy.get(`[data-cy=tbl-list-row]:nth-child(${rowIndex + 1})  [data-cy=tbl-cell-multi-selection] input`).click()
  })
})
