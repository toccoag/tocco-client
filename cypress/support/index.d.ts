// type definitions for Cypress object "cy"
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable<Subject = any> {
    ////// bootstrap //////
    login(options?: {username?: string; password?: string}): void
    logout(): void
    waitForLoaded(): void
    waitForDashboardLoaded(): void
    waitForListLoaded(): void

    ////// log //////

    /**
     * Logs title of spec for cypress export
     * @example
     * cy.logSpec()
     */
    logSpec(): void

    /**
     * Logs title of test for cypress export
     * @example
     * cy.logTest()
     */
    logTest(): void

    /**
     * Logs test step for cypress export
     * @example
     * cy.logStep('Aktion Rechnung ausführen')
     */
    logStep(message: string): void

    ////// widget //////
    /**
     * cy.visit for Widgets
     * @example
     * cy.visitWidget(widgetConfigKey)
     */
    visitWidget(widgetConfigKey: string, suffix: string = ''): void

    ////// commands //////
    /**
     * Get element cy `data-cy` attribute
     * @example
     * cy.getByAttr('btn-submit')
     */
    getByAttr(attribute: string): Chainable<Subject>

    /**
     * Asserts the breadcrumb is equal to the passed ordered list
     * @example
     * cy.breadcrumbsIs(['Home', 'Person', 'Muster, Hans'])
     */
    breadcrumbsIs(expectedIteams: string[]): Chainable<Subject>

    /**
     * Asserts whether the element is visible within the viewport.
     * @example
     * cy.getByAttr('btn-submit').isInViewport()
     */
    isInViewport(): Chainable<Subject>

    /**
     * Asserts whether the element is not visible within the viewport.
     * @example
     * cy.getByAttr('btn-submit').isNotInViewport()
     */
    isNotInViewport(): Chainable<Subject>

    ////// aceeditor //////
    /**
     * Asserts a code editor (ace) component has defined value
     * @example
     * cy.get('#input-code')
     *  .hasCodeValue('event_status.unique_id != "archived"')
     */
    hasCodeValue(value: string): Chainable<Subject>

    /**
     * Clears a value into a code editor (ace) component.
     * @example
     * cy.get('#input-code')
     *  .clearCodeValue()
     */
    clearCodeValue(): Chainable<Subject>

    /**
     * Types a value into a code editor (ace) component.
     * @example
     * cy.get('#input-code')
     *  .typeCodeValue('event_status.unique_id != "archived"')
     */
    typeCodeValue(value: string): Chainable<Subject>

    ////// datepicker //////
    /**
     * Types a date into a react datepicker component
     * @example
     * cy.get('#input-detailForm-birthdate').typeDate('20.03.1990')
     */
    typeDate(value: string): Chainable<Subject>

    /**
     * Asserts a react datepicker component has defined date as value
     * @example
     * cy.get('#input-detailForm-birthdate').hasDate('20.03.1990')
     */
    hasDate(value: string): Chainable<Subject>

    /**
     * Clears the value from the react datepicker component
     * @example
     * cy.get('#input-detailForm-birthdate').clearDate()
     */
    clearDate(): Chainable<Subject>

    ////// duration //////
    /**
     * Types a duration into a duration edit.
     * @example
     * cy.get('#input-detailForm-education_amount').typeDuration({hours: '2', minutes: '20'})
     */
    typeDuration(duration: {hours: string; minutes: string}): Chainable<Subject>

    /**
     * Asserts a duration edit as a defined duration as value
     * @example
     * cy.get('#input-detailForm-education_amount').hasDuration({hours: '2', minutes: '20'})
     */
    hasDuration(duration: {hours: string; minutes: string}): Chainable<Subject>

    /**
     * Clears the value from the duration edit
     * @example
     * cy.get('#input-detailForm-education_amount').clearDuration()
     */
    clearDuration(): Chainable<Subject>

    ////// htmleditor //////
    /**
     * Types a value into a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').typeHtmlValue('Hallo')
     */
    typeHtmlValue(value: string): Chainable<Subject>

    /**
     * Asserts a value from a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').hasHtmlValue('Hallo')
     */
    hasHtmlValue(value: string): Chainable<Subject>

    /**
     * Clear a value from a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').clearHtmlValue()
     */
    clearHtmlValue(): Chainable<Subject>

    /**
     * Toggles the source editing mode from a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').toggleSourceEditing()
     */
    toggleSourceEditing(): Chainable<Subject>

    /**
     * Asserts if the source editing mode from a html editor (ckeditor4) component is active/deactive.
     * @example
     * cy.get('#input-html').assertSourceEditing(true)
     */
    assertSourceEditing(sourceEditingEnabled: boolean): Chainable<Subject>

    /**
     * Toggles the source editing mode from a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').typeHtmlSourceValue('<br />')
     */
    typeHtmlSourceValue(value: string): Chainable<Subject>

    /**
     * Asserts a value from the source editing mode from a html editor (ckeditor4) component.
     * @example
     * cy.get('#input-html').hasHtmlSourceValue('<br />')
     */
    hasHtmlSourceValue(value: string): Chainable<Subject>

    ////// resourcecalendar //////
    /**
     * Asserts the resource calendar has a resource
     * @example
     * cy.hasResource('User', 4, 'Müller, Hans')
     */
    hasResource(type: 'User' | 'Room' | 'Appliance' | 'Event', id: number, value: string): void

    /**
     * Asserts the resource calendar has an event
     * @example
     * cy.hasEvent('User', 4, 1, '17.00 Uhr - 18.00 Uhr', 'Deutsch Kurs')
     */
    hasEvent(
      type: 'User' | 'Room' | 'Appliance' | 'Event',
      id: number,
      index: number,
      time: string,
      title: string
    ): void

    /**
     * Switches to the month view of the calendar
     * @example
     * cy.showMonthView()
     */
    showMonthView(): Chainable<Subject>

    /**
     * Navigate next
     * @example
     * cy.calendarNavigateNext()
     */
    calendarNavigateNext(): Chainable<Subject>

    /**
     * Navigate previous
     * @example
     * cy.calendarNavigatePrev()
     */
    calendarNavigatePrev(): Chainable<Subject>

    ////// select //////
    /**
     * Selects a value from a simple drop down
     * @example:
     * cy.get('#input-detailForm-relGender').selectValue('Weiblich')
     */
    selectValue(value: string): void

    /**
     * Selects a value from a remote drop down
     * @example
     * cy.get('#input-detailForm-relNationality').selectRemoteValue('Schweiz')
     */
    selectRemoteValue(value: string): void

    /**
     * Asserts if select has a value selected from the drop down
     * @example
     * cy.get('#input-detailForm-relGender').hasValueSelected('Weiblich')
     */
    hasValueSelected(value: string): Chainable<Subject>

    /**
     * Clears a value from a simple drop down
     * @example
     * cy.get('#input-detailForm-relGender').clearSelectedValue()
     */
    clearSelectedValue(value: string): Chainable<Subject>

    /**
     * Opens the advanced search from a remote drop down
     * @example
     * cy.get('#input-detailForm-relGender').openAdvancedSearch()
     */
    openAdvancedSearch(): Chainable<Subject>

    /**
     * Opens the remote create from a remote drop down
     * @example
     * cy.get('#input-detailForm-relGender').openRemoteCreate()
     */
    openRemoteCreate(): Chainable<Subject>

    /**
     * Remove a single value from a simple drop down
     * @example
     * cy.get('#input-detailForm-relTarget_group').removeSelectedValue('Einsteiger')
     */
    removeSelectedValue(value: string): Chainable<Subject>

    ////// table //////
    /**
     * Gets row of a table - index starts at 0
     * @example
     * cy.getTableRow(0)
     */
    getTableRow(index: number): Chainable<Subject>

    /**
     * Asserts a row exists with a specific value
     * @example
     * cy.listRowContains('Sub Folder')
     */
    listRowContains(value: string): Chainable<Subject>

    /**
     * Gets rows of a table by content
     * @example
     * cy.getRowsContaining('Bernasconi')
     */
    getRowsContaining(value: string): Chainable<Subject>

    /**
     * Clicks the row selection checkbox. Should be chained to a row selection.
     * @example
     * cy.getRowsContaining('Bernasconi').selectRows()
     */
    selectRows(): void

    /**
     * Navigates to detail on the given row
     * @example
     * cy.getByAttr('tbl').getTableRow(0).navigateToDetail()
     */
    navigateToDetail(): Chainable<Subject>

    /**
     * Asserts if there is the "no data" info visible inside the table
     * @example
     * cy.getByAttr('tbl').hasNoData()
     */
    hasNoData(): Chainable<Subject>

    /**
     * Toggles the radio button for the single selection inside a table.
     * @example
     * cy.get('table').toggleSingleSelection(0)
     */
    toggleSingleSelection(index: number): Chainable<Subject>

    /**
     * Toggles the checkbox button for the multi selection inside a table.
     * @example
     * cy.get('table').toggleMultiSelection(0)
     */
    toggleMultiSelection(index: number): Chainable<Subject>

    ////// action //////
    /**
     * Clicks on action button
     * @example
     * cy.getByAttr('input-edit-table').clickActionButton('new_input_edit_info')
     */
    clickActionButton(action: string): void

    /**
     * Clicks on a menu item in the 'actions' action group
     * @example
     * cy.clickActionMenuItem('new_input_edit')
     */
    clickActionMenuItem(action: string): void

    /**
     * Clicks on a menu item in the 'output' action group
     * @example
     * cy.clickOutputMenuItem('bill_with_creditslip')
     */
    clickOutputMenuItem(action: string): void

    ////// form //////
    /**
     * Gets the value field belonging to a given label
     * @example
     * cy.getByAttr('modal-content').getValueByLabel('Fach').should('have.text', 'Grades')
     */
    getValueByLabel(label: string): Chainable<Subject>

    /**
     * Gets the select field belonging to a given label
     * @example
     * cy.getSelectByLabel('Status Noteneingabe').hasValueSelected('Offen')
     */
    getSelectByLabel(label: string): Chainable<Subject>

    ////// inputedit //////
    /**
     * Waits for the input edit data calculation to start and end
     * @example
     * cy.awaitInputEditCalculation()
     */
    awaitInputEditCalculation(): void

    /**
     * Waits for the input edit table to be filled with any data
     * @example
     * cy.awaitInputEditData()
     */
    awaitInputEditData(): void

    /**
     * Fills a given input edit cell with a given value
     * Can be used with a subject or without
     * @example
     * cy.fillInputEditCell('2', 3)
     */
    fillInputEditCell(cell: string, value: number | string): Chainable<Subject>

    /**
     * Focuses a given input edit cell
     * Can be used with a subject or without
     * @example
     * cy.getByAttr('tbl-list-row').first().focusInputEditCell('2')
     */
    focusInputEditCell(cell: string): Chainable<Subject>

    /**
     * Toggles the dispense checkbox
     * Can be used with a subject or without
     * @example
     * cy.toggleInputEditDispense()
     */
    toggleInputEditDispense(): Chainable<Subject>

    /**
     * Asserts that a given input edit cell has a given value
     * Can be used with a subject or without
     * @example
     * cy.hasInputEditCellValue('grade', '4.00')
     */
    hasInputEditCellValue(cell: string, value: number | string): void

    /**
     * Asserts that a given input edit cell has no value
     * Can be used with a subject or without
     * @example
     * cy.hasEmptyInputEditCell('definate_grade')
     */
    hasEmptyInputEditCell(cell: string): void

    /**
     * Asserts that a given input edit cell has some value
     * Can be used with a subject or without
     * @example
     * cy.hasFilledInputEditCell('text')
     */
    hasFilledInputEditCell(cell: string): void

    /**
     * Asserts if a given cell is focused
     * @example
     * cy.hasInputEditCellFocused('1:2')
     */
    hasInputEditCellFocused(id: string): void

    /**
     * uploads a file to a tocco fileupload field
     *
     * @see https://docs.cypress.io/api/commands/selectfile#Arguments
     *
     * @example
     * cy.uploadFile('file', 'cypress/e2e/core/admin/resources/incoming_payment.xml')
     */
    uploadFile(fieldName: string, file: FileContents): void

    /**
     * reads a textbased file (e.g. txt / xml json), modifies its content using the passed function and uploads the result to an upload field
     *
     * @param file name of the upload field
     * @param filePath this is a relative path starting at project root
     * @param modifierFn the function to modify the file content
     *
     * @example
     * cy.uploadModifiedFile('input-simpleForm-file', 'cypress/e2e/core/admin/resources/incoming_payment.xml', content => content.replaceAll('{REFERENCE}', '1234566'))
     */
    uploadModifiedFile(fieldName: string, filePath: string, modifierFn: function): void
  }
}
