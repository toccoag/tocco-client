var toccoTheme = {
    colors: {
        primary: '#9E2124'
    },
    fontFamily: {
        regular: '"Helvetica Neue", Helvetica, Arial, sans-serif'
    },
    fontSize: {
        base: 1.4
    },
    space: {
        base: 1.4
    }
};
