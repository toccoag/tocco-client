const fs = require('fs')
const path = require('path')

const compress = require('compression')
const express = require('express')
const {createProxyMiddleware} = require('http-proxy-middleware')
const webpack = require('webpack')

const logger = require('../build/lib/logger').default
const webpackConfig = require('../build/webpack.config').default
const config = require('../config').default

const getFileFromUrl = url => {
  if (url.startsWith('/wp')) {
    return 'wp.html'
  }
  if (url.startsWith('/legacy')) {
    return 'legacy.html'
  }
  return 'index.html'
}

const server = serverConfig => {
  const {simulateCms} = serverConfig || {}

  const app = express()
  app.use(compress()) // Apply gzip compression

  // ------------------------------------
  // Apply Webpack HMR Middleware
  // ------------------------------------
  const publicPath = webpackConfig.output.path

  if (config.env === 'development') {
    const adminCss = fs.readFileSync('./server/admin.css', 'utf8')
    const cmsCss = fs.readFileSync('./server/cms.css', 'utf8')

    const compiler = webpack(webpackConfig)

    const wdm = require('webpack-dev-middleware')(compiler, {
      publicPath: webpackConfig.output.publicPath,
      index: !simulateCms // do not serve index.html automatically for cms simulation
    })

    logger.info('Enabling webpack dev and HMR middleware')
    app.use(wdm)
    wdm.waitUntilValid(() => {
      logger.success('Compilation finished! Hot reload is watching for changes...')
    })

    app.use(require('webpack-hot-middleware')(compiler))

    // Serve static assets from ~/public since Webpack is unaware of
    // these files. This middleware doesn't need to be enabled outside
    // of development since this directory will be copied into ~/dist
    // when the application is compiled.

    app.use(express.static(publicPath))
    app.use('/static', express.static('server/static'))
    app.use('/wp', express.static('server/wp'))
    app.use('/legacy', express.static('server/legacy'))

    // `window.location.hostname` might be used in __BACKEND_URL__ variable
    // eslint-disable-next-line
    const window = {location: {hostname: 'localhost'}}
    // eslint-disable-next-line
    const backendUrl = eval(config.globals.__BACKEND_URL__)

    for (const urlPath of ['/nice2', '/js', '/img', '/css']) {
      app.use(
        urlPath,
        createProxyMiddleware({
          target: backendUrl + urlPath,
          changeOrigin: true
        })
      )
    }

    app.use('/dynamic/styles.css', function (req, res, next) {
      res.set('content-type', 'text/css')

      res.send(simulateCms ? cmsCss : adminCss)
      res.end()
    })

    // This rewrites all routes requests to the root /index.html file
    // (ignoring file requests). If you want to implement universal
    // rendering, you'll want to remove this middleware.
    app.use('*', function (req, res, next) {
      const file = simulateCms ? getFileFromUrl(req.originalUrl) : 'index.html'
      const filename = path.join(compiler.outputPath, file)
      compiler.outputFileSystem.readFile(filename, (err, result) => {
        if (err) {
          return next(err)
        }
        res.set('content-type', 'text/html')
        res.send(result)
        res.end()
      })
    })
  } else {
    logger.warn(
      'Server is being run outside of live development mode, meaning it will ' +
        'only serve the compiled application bundle in ~/dist. Generally you ' +
        'do not need an application server for this and can instead use a web ' +
        'server such as nginx to serve your static files. See the "deployment" ' +
        'section in the README for more information on deployment strategies.'
    )
  }

  return app
}

module.exports = server
