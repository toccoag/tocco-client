import logger from '../build/lib/logger'

import {execute, getMandatoryEnvVariable} from './helpers'
import {checkHasSshKey, addGitlabAsKnownHost, setupGitUser} from './helpers/git'

export const PREFIX = 'https://toccoag.gitlab.io/tocco-storybook/'
const STORYBOOK_REPO_NAME = 'tocco-storybook'
const STORYBOOK_BRANCH = 'master'

/**
 * Returns the storybook link for a branch
 */
export const getStorybookLink = branchName => {
  const branchNameAdjusted = branchName.replaceAll('/', '_')
  return `${PREFIX}${branchNameAdjusted}`
}

/**
 * Build storybook with backend url with is used for stories with backend interaction (e.g. admin story)
 */
export const buildStorybook = async backendUrl => {
  logger.info(`Build storybook with backend url ${backendUrl}`)
  await execute(`BACKEND=${backendUrl} yarn build-storybook`)
}

/**
 * Deploy storybook of a branch to the storybook repository
 */
export const deployStorybook = async branchName => {
  const publicKey = getMandatoryEnvVariable('GITLAB_PUBLIC_KEY')
  await checkHasSshKey()
  await addGitlabAsKnownHost(publicKey)

  const repoUrl = `git@gitlab.com:toccoag/${STORYBOOK_REPO_NAME}.git`
  logger.log(`Clone repository ${STORYBOOK_REPO_NAME}`)
  await execute(`git clone ${repoUrl} --single-branch --branch ${STORYBOOK_BRANCH} --depth 1`, 'dist/')

  const folderName = getStorybookFolderName(branchName)
  logger.log(`Copy storybook pages of branch ${branchName} to folder ${folderName}`)
  const folderPath = `dist/${STORYBOOK_REPO_NAME}/${folderName}`
  await execute(`rm -rf ${folderPath}`)
  await execute(`mkdir ${folderPath}`)
  await execute(`mv dist/storybook/* ${folderPath}`)

  logger.log('Commit and push')
  const repoPath = `dist/${STORYBOOK_REPO_NAME}`
  await setupGitUser(repoPath)
  await execute('git add -A', repoPath)
  const revision = await execute('git rev-parse --short HEAD', repoPath)
  await execute(`git diff-index --quiet HEAD || git commit -m "rebuild storybook at ${revision}"`, repoPath)
  await execute(`git pull --rebase origin ${STORYBOOK_BRANCH}`, repoPath)
  await execute(`git push -q ${repoUrl} HEAD:${STORYBOOK_BRANCH}`, repoPath)
}

// folder name cannot contain "/" so replace it with "_"
export const getStorybookFolderName = branchName => branchName.replaceAll('/', '_')
