import * as packages from '../build/lib/packages'

import {mockDependencies, mockExistsGitTag, mockGetGitLogSinceTag, mockGetVariables} from './helpers'
import {
  getNextVersion,
  getPackageNamesFromCommit,
  parseGitLog,
  getNextChangelog,
  getReleaseCommitMessage
} from './releasePackagesHelpers'

describe('scripts', () => {
  describe('releasePackagesHelpers', () => {
    mockDependencies('3.6.1')

    describe('getNextVersion', () => {
      test('increment patch version', () => {
        mockGetVariables({niceVersion: '3.6.0'})
        expect(getNextVersion('admin')).to.equal('3.6.2')
      })

      test('not matching prefix', () => {
        mockGetVariables({niceVersion: '3.7.0'})

        expect(getNextVersion('admin')).to.equal('3.7.0')
      })
    })

    describe('getPackageNamesFromCommit', () => {
      test('invalid commit message subject format', () => {
        const commitMessageSubject = 'invalid commit message subject'
        expect(getPackageNamesFromCommit(commitMessageSubject)).to.eql([])
      })

      test('no package defined', () => {
        const commitMessageSubject = 'chore: release packages'
        expect(getPackageNamesFromCommit(commitMessageSubject)).to.eql([])
      })

      test('single package defined', () => {
        const commitMessageSubject = 'test(admin): add test'
        expect(getPackageNamesFromCommit(commitMessageSubject)).to.eql(['admin'])
      })

      test('multiple packages defined', () => {
        const commitMessageSubject = 'test( admin, tocco-ui ): add test'
        expect(getPackageNamesFromCommit(commitMessageSubject)).to.eql(['admin', 'tocco-ui'])
      })

      test('all packages are affected', () => {
        const allPackages = ['admin', 'entity-list', 'tocco-ui']
        jest.spyOn(packages, 'getAllPackages').mockImplementation(() => allPackages)

        const commitMessageSubject = 'test(tocco-client): add test'
        expect(getPackageNamesFromCommit(commitMessageSubject)).to.eql(allPackages)
      })
    })

    describe('parseGitLog', () => {
      test('commit message has no changelog', async () => {
        const commits = [
          `test(admin): add test
        
          Refs: TOCDEV-1
          Cherry-Pick: Up`
        ]
        mockGetGitLogSinceTag(commits)

        const expected = [
          {
            packages: ['admin'],
            changelog: undefined
          }
        ]
        expect(await parseGitLog('tocco-admin@nice36')).to.eql(expected)
      })

      test('commit message has changelog', async () => {
        const commits = [
          `test(admin): add test
        
          Refs: TOCDEV-1
          Cherry-Pick: Up
          Changelog: add test`
        ]
        mockGetGitLogSinceTag(commits)

        const expected = [
          {
            packages: ['admin'],
            changelog: 'add test'
          }
        ]
        expect(await parseGitLog('tocco-admin@nice36')).to.eql(expected)
      })

      test('commit message has no body', async () => {
        const commits = ['test(admin): add test']
        mockGetGitLogSinceTag(commits)

        const expected = [
          {
            packages: ['admin'],
            changelog: undefined
          }
        ]
        expect(await parseGitLog('tocco-admin@nice36')).to.eql(expected)
      })

      test('multiple commits with changelog in commit message', async () => {
        const commits = [
          `test(admin): add test
        
          Refs: TOCDEV-1
          Cherry-Pick: Up
          Changelog: add test`,
          `feat(entity-list): add feat
          
          Refs: TOCDEV-1
          Cherry-Pick: Up
          Changelog: add feat to entity-list`
        ]
        mockGetGitLogSinceTag(commits)

        const expected = [
          {
            packages: ['admin'],
            changelog: 'add test'
          },
          {
            packages: ['entity-list'],
            changelog: 'add feat to entity-list'
          }
        ]
        expect(await parseGitLog('tocco-admin@nice36')).to.eql(expected)
      })

      test('changelog without whitespace and lowercase letter', async () => {
        const commits = [
          `test(admin): add test
        
          Refs: TOCDEV-1
          Cherry-Pick: Up
          changelog:add test`
        ]
        mockGetGitLogSinceTag(commits)

        const expected = [
          {
            packages: ['admin'],
            changelog: 'add test'
          }
        ]
        expect(await parseGitLog('tocco-admin@nice36')).to.eql(expected)
      })
    })

    describe('getNextChangelog', () => {
      test('single commit with transitive dependency', async () => {
        const commits = [
          `test(tocco-util): add test

          Changelog: add test`
        ]
        mockGetGitLogSinceTag(commits)
        mockExistsGitTag(true)

        expect(await getNextChangelog('devcon')).to.equal('- add test')
      })

      test('no changelog', async () => {
        const commits = ['test(tocco-util): add test']
        mockGetGitLogSinceTag(commits)
        mockExistsGitTag(true)

        expect(await getNextChangelog('devcon')).to.equal('')
      })

      test('package of commit message is not releated', async () => {
        const commits = [
          `
          test(tocco-util): add test

          Changelog: add test
          `
        ]
        mockGetGitLogSinceTag(commits)
        mockExistsGitTag(true)

        expect(await getNextChangelog('tocco-theme')).to.equal('')
      })

      test('multiple commits with changelog in commit message', async () => {
        const commits = [
          `test(devcon): add test

          Changelog: add test`,
          `feat(tocco-util): add feat
          
          Changelog: add feat to tocco-util`,
          `feat(admin): adjust admin routing
          
          Changelog: adjust admin routing`
        ]
        mockGetGitLogSinceTag(commits)
        mockExistsGitTag(true)

        expect(await getNextChangelog('devcon')).to.equal('- add test\n- add feat to tocco-util')
      })

      test('remove duplicates in changelog', async () => {
        const commits = [
          `test(tocco-ui): add test

          Changelog: add test`,
          `test(tocco-util): add test
          
          Changelog: add test`,
          `feat(app-extensions): adjust form handling
          
          Changelog: adjust form handling`
        ]
        mockGetGitLogSinceTag(commits)
        mockExistsGitTag(true)

        expect(await getNextChangelog('devcon')).to.equal('- add test\n- adjust form handling')
      })

      test('initial release for version', async () => {
        mockExistsGitTag(false)
        mockGetVariables({niceVersion: '3.6.0'})

        expect(await getNextChangelog('devcon')).to.equal('- initial release for version 3.6')
      })
    })

    describe('getReleaseCommitMessage', () => {
      test('create a commit messages with multiple packages', () => {
        const releasedPackages = [
          {package: 'tocco-admin', version: '3.8.10'},
          {package: 'tocco-devcon', version: '3.8.1'}
        ]
        const expected = `chore: release packages

- tocco-admin@3.8.10
- tocco-devcon@3.8.1`
        expect(getReleaseCommitMessage(releasedPackages)).to.equal(expected)
      })
    })
  })
})
