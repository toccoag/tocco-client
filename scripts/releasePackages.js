/**
 * Script releases packages to npm. Should only be executed by the Gitlab CI
 *
 * Workflow:
 * - Setup git for pushing commits and setup npm for releasing
 * - Get candidate packages which may be released
 * - For each candidate package:
 *    - Check if changelog is not empty (or force releasing is active)
 *    - Define new package version
 *    - Write changelog to changelog.md file
 *    - Publish package to npm
 *    - Set npm dist tag to released package
 *    - Send slack message
 * - Make commit with changed package.json and changelog.md
 * - Push commit
 * - For each released package:
 *    - Set git release tag of package to new commit
 *    - Push tag
 *
 * Mandatory environment variables:
 * - FULL_CALENDAR_LICENCE: used during releasing a package having a depedendency to the scheduler package
 * - GITLAB_DEPLOY_PRIVATE_KEY: used for the git interaction
 * - GITLAB_PUBLIC_KEY: used for the git interaction
 * - NPMJS_AUTH_TOKEN: used for the authentification with npm
 * - SLACK_AUTH_TOKEN: used for sending the slack release message
 * - BACK_OFFICE_USERNAME: username to access Tocco Back Office via REST
 * - BACK_OFFICE_API_KEY: api key to access Tocco Back Office via REST
 *
 * Optional environment variables:
 * - CI_RELEASING_PACKAGES: per default all packages are candidate for releasing. A comma separated list
 * (e.g. `admin,devcon`) can be passed to reduce the number of candidate packages.
 * - CI_RELEASING_FORCE: per default a package is only released if the changelog is not empty.
 * Set this variable to `true` that the packages are always released.
 */
import logger from '../build/lib/logger'
import {getPackageDirectory} from '../build/lib/packages'

import {
  execute,
  getMandatoryEnvVariable,
  getNpmTag,
  prependFile,
  setupGitlab,
  setupGitUser,
  transformPackageName
} from './helpers'
import {
  checkIfRunningInCI,
  getNextChangelog,
  getNextVersion,
  getPackagesToRelease,
  getReleaseCommitMessage,
  sendReleaseMessage,
  writeYarnrc
} from './releasePackagesHelpers'

const main = async () => {
  checkIfRunningInCI()

  const ciReleasingPackages = process.env.CI_RELEASING_PACKAGES
  const ciReleasingForce = process.env.CI_RELEASING_FORCE === 'true'
  getMandatoryEnvVariable('SLACK_AUTH_TOKEN')
  getMandatoryEnvVariable('BACK_OFFICE_USERNAME')
  getMandatoryEnvVariable('BACK_OFFICE_API_KEY')
  getMandatoryEnvVariable('FULL_CALENDAR_LICENCE')
  const npmAuthToken = getMandatoryEnvVariable('NPMJS_AUTH_TOKEN')

  await writeYarnrc(npmAuthToken)

  await setupGitlab()
  await setupGitUser()

  const npmTag = getNpmTag()
  const packagesToRelease = await getPackagesToRelease(ciReleasingPackages)
  const packagesPublished = []

  logger.log(`Start releasing process for the following packages: ${packagesToRelease}`)

  for (const packageName of packagesToRelease) {
    logger.info(`Start releasing package "${packageName}"`)

    const changelog = await getNextChangelog(packageName)
    if (!changelog && ciReleasingForce !== true) {
      logger.warn(`Skip releasing package ${packageName} because changelog is empty.`)
      continue
    }

    const nextVersion = getNextVersion(packageName)
    logger.log(`New version: ${nextVersion}`)
    logger.log(`Changelog:\n${changelog}`)

    const packageDirectory = getPackageDirectory(packageName)

    const changelogFile = `${packageDirectory}/changelog.md`
    const newChangelog = `${nextVersion}\n${changelog}\n`
    prependFile(changelogFile, newChangelog)

    const packageNameAndVersion = `${transformPackageName(packageName)}@${nextVersion}`
    logger.log(`Start publishing to npm of package "${packageNameAndVersion}"`)

    /**
     * - update version in package.json to nextVersion
     */
    await execute(`yarn version ${nextVersion}`, packageDirectory)

    /**
     * - run yarn pack (=> runs yarn prepack)
     * - uploads package archive to npm
     * - uses version from package.json
     * - uses auth token from .yarnrc.yml
     *
     * sources:
     *  - https://docs.npmjs.com/requiring-2fa-for-package-publishing-and-settings-modification
     *  - https://yarnpkg.com/configuration/yarnrc#npmPublishRegistry
     *  - https://yarnpkg.com/configuration/yarnrc#npmRegistries
     */
    await execute(`yarn npm publish`, packageDirectory)
    logger.log('Package published to npm')

    await execute(`yarn npm tag add ${packageNameAndVersion} ${npmTag}`)
    logger.log(`Set dist-tag ${npmTag} to ${packageNameAndVersion}`)

    await sendReleaseMessage(packageNameAndVersion, changelog)
    logger.log('Sent release message to Slack')

    packagesPublished.push({package: transformPackageName(packageName), version: nextVersion})
  }

  logger.info('Commit changes')

  if (packagesPublished.length === 0) {
    logger.info('No package released. Nothing to push')
  } else {
    const commitMessage = getReleaseCommitMessage(packagesPublished)
    await execute('git checkout -- .yarnrc.yml')
    await execute("git add '*changelog.md' '*package.json'")
    await execute(`git commit --no-verify -a -m "${commitMessage}"`)
    logger.log('Changes committed')
    // push current branch. Don't use force as script should fail
    // if a new commit was added to the branch during the releasing
    await execute(`git push origin $CI_COMMIT_REF_NAME`)
    logger.log('Changes pushed')

    for (const p of packagesPublished) {
      const tagName = `${p.package}@${npmTag}`
      // use force as tag may already exist and should be overridden
      await execute(`git tag ${tagName} -f`)
      logger.log(`Add tag "${tagName}" to commit`)
      await execute(`git push origin ${tagName} -f`)
      logger.log(`Push tag "${tagName}"`)
    }
  }
}

main()
