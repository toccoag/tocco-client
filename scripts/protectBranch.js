/**
 * Script protected/unprotected a branch on Gitlab
 *
 * Execution:
 * node ./protectBranch protect|unprotect
 *
 * Workflow:
 * - load current protection setting for branch
 * - do (un)protection of branch and throw an error if new settings cannot be applied
 *
 * Mandatory environment variables:
 * - CI_COMMIT_REF_NAME: target branch for which the job is executed (always passed by Gitlab)
 * - CI_PROJECT_ID: project id for which the job is executed (always passed by Gitlab)
 * - GITLAB_ACCESS_TOKEN: Gitlab api key to interact with the rest api
 */

import fetch from 'isomorphic-fetch'

import logger from '../build/lib/logger'

// https://docs.gitlab.com/ee/api/protected_branches.html
//  0  => No access
//  30 => Developer access
//  40 => Maintainer access
//  60 => Admin access

const NoOneAccessLevel = 0
const DeveloperAccessLevel = 30

const TargetBranch = process.env.CI_COMMIT_REF_NAME
const CIProjectId = process.env.CI_PROJECT_ID
const GitlabAccessToken = process.env.GITLAB_ACCESS_TOKEN
const Url = `https://gitlab.com/api/v4/projects/${CIProjectId}/protected_branches/${encodeURIComponent(TargetBranch)}`

const RequestOptions = {
  headers: {
    'Content-Type': 'application/json',
    'PRIVATE-TOKEN': GitlabAccessToken
  }
}

const fetchSettings = async () => {
  const options = {
    ...RequestOptions,
    method: 'GET'
  }

  const response = await fetch(Url, options)
  const statusCode = response.status
  const data = response.json()
  if (statusCode === 404) {
    throw new Error('Protected branch not found')
  } else if (statusCode === 401) {
    throw new Error('Unauthorized')
  } else if (statusCode >= 400) {
    throw new Error(`Could not fetch protected branch. Status code: ${statusCode}`)
  }

  return data
}

const setAccessLevels = async levels => {
  const body = {
    name: TargetBranch,
    allowed_to_merge: levels
  }

  const options = {
    ...RequestOptions,
    method: 'PATCH',
    body: JSON.stringify(body)
  }

  logger.log('set access levels', body)
  const response = await fetch(Url, options)
  const statusCode = response.status
  const data = response.json()
  if (statusCode >= 400) {
    throw new Error(`Could not set access level for '${TargetBranch}'. Status code: ${statusCode}`)
  }
  return data
}

const findAccessLevel = (settings, accessLevel) => {
  const mergeAccessLevels = settings.merge_access_levels || []
  return mergeAccessLevels.find(a => a.access_level === accessLevel)
}

// remove access to developers
const protectBranch = async () => {
  try {
    const settings = await fetchSettings()
    const developerAccess = findAccessLevel(settings, DeveloperAccessLevel)
    const noOneAccess = findAccessLevel(settings, NoOneAccessLevel)
    if (developerAccess) {
      await setAccessLevels([
        {id: developerAccess.id, _destroy: true},
        ...(noOneAccess ? [] : [{access_level: NoOneAccessLevel}])
      ])
    } else {
      logger.error(`Could not protect branch '${TargetBranch}'!\nNo developer access found.`)
    }
  } catch (error) {
    logger.error(`Could not protect branch '${TargetBranch}'!`)
    logger.error(error)
  }
}

// add access to developers again
const unprotectBranch = async () => {
  try {
    const settings = await fetchSettings()
    const developerAccess = findAccessLevel(settings, DeveloperAccessLevel)
    const noOneAccess = findAccessLevel(settings, NoOneAccessLevel)
    if (!developerAccess) {
      await setAccessLevels([
        {access_level: DeveloperAccessLevel},
        ...(noOneAccess ? [{id: noOneAccess.id, _destroy: true}] : [])
      ])
    }
  } catch (error) {
    logger.error(`Could not unprotect branch '${TargetBranch}'!`)
    logger.error(error)
  }
}

const args = process.argv
const type = args[2]

switch (type) {
  case 'protect':
    protectBranch()
    break
  case 'unprotect':
    unprotectBranch()
    break

  default:
    logger.error(`no routine found for '${type}'`)
    process.exit(9) // invalid argument
}
