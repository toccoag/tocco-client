/**
 * Build storybook for a branch
 *
 * Execution:
 * node ./storybookBranch
 *
 * Workflow:
 * - build storybook for branch
 * - deploy builded storybook to storybook repo
 *
 * Mandatory environment variables:
 * - CI_COMMIT_REF_NAME: target branch for which the job is executed (always passed by Gitlab)
 * - GITLAB_DEPLOY_PRIVATE_KEY: used for the git interaction
 * - GITLAB_PUBLIC_KEY: used for the git interaction
 */
import {readJsonFile} from '../build/lib/packages'

import {getMandatoryEnvVariable} from './helpers'
import {buildStorybook, deployStorybook} from './storybookHelpers'

const main = async () => {
  const backendUrl = readJsonFile('./scripts/variables.json').testSystemUrl
  const branchName = getMandatoryEnvVariable('CI_COMMIT_REF_NAME')
  await buildStorybook(backendUrl)
  await deployStorybook(branchName)
}

main()
