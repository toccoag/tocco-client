export const insertPrincipal = (cypressUser, cypressUserPasswordHash, cypressUserApiKeyHash) => {
  const sql = `INSERT INTO nice_principal
  (
      username,
      password,
      new_password,
      unconfirmed_password_dispatch,
      fail_login_attempts,
      fk_user,
      fk_principal_type,
      fk_principal_status,
      fk_interface_language,
      fk_two_step_login_status,
      _nice_version,
      _nice_create_timestamp,
      _nice_update_timestamp,
      _nice_create_user,
      _nice_update_user,
      sso_subject,
      _system_entity,
      secret,
      wrong_password_count,
      support_user,
      force_two_factor_auth
  ) VALUES (
      $1,
      $2,
      false,
      0,
      0,
      (SELECT min(pk) FROM nice_user),
      (SELECT pk FROM nice_principal_type WHERE unique_id = 'databased'),
      (SELECT pk FROM nice_principal_status WHERE unique_id = 'active'),
      (SELECT pk FROM nice_interface_language WHERE unique_id = 'de'),
      (SELECT pk FROM nice_two_step_login_status WHERE unique_id = 'inactive'),
      1,
      now(),
      now(),
      'tocco',
      'tocco',
      '',
      false,
      '',
      0,
      false,
      false
  ) ON CONFLICT (username) DO UPDATE
      SET password = '${cypressUserPasswordHash}',
      fail_login_attempts = 0,
      fk_principal_status = (SELECT pk FROM nice_principal_status WHERE unique_id = 'active'),
      fk_interface_language = (SELECT pk FROM nice_interface_language WHERE unique_id = 'de')`
  const variables = [cypressUser, cypressUserPasswordHash]
  return [sql, variables]
}

export const insertLoginRole = cypressUser => {
  const sql = `INSERT INTO nice_login_role (
    _nice_version,
    _nice_create_timestamp,
    _nice_update_timestamp,
    _nice_create_user,
    _nice_update_user,
    fk_principal,
    fk_role,
    fk_business_unit,
    manual,
    initial_identifier,
    _system_entity
) SELECT
    1,
    now(),
    now(),
    'tocco',
    'tocco',
    p.pk,
    r.pk,
    null,
    true,
    '',
    false
  FROM
    nice_role AS r LEFT OUTER JOIN nice_role_type AS rt ON r.fk_role_type = rt.pk,
    nice_principal as p
    WHERE p.username = '${cypressUser}' AND rt.unique_id <> 'guest'
ON CONFLICT DO NOTHING;`
  return [sql]
}

export const insertApiKey = (cypressUser, cypressUserApiKeyHash) => {
  const sql = `INSERT INTO nice_api_key (
    label,
    key,
    session_key,
    _nice_version,
    _nice_create_timestamp,
    _nice_update_timestamp,
    _nice_create_user,
    _nice_update_user,
    _system_entity,
    fk_principal,
    fk_api_key_usage
) VALUES (
    'Cypress API Key',
    $1,
    uuid_generate_v4(),
    1,
    now(),
    now(),
    'tocco',
    'tocco',
    false,
    (SELECT pk FROM nice_principal WHERE username = '${cypressUser}'),
    (SELECT pk FROM nice_api_key_usage WHERE unique_id = 'general')
) ON CONFLICT DO NOTHING;`
  const variables = [cypressUserApiKeyHash]
  return [sql, variables]
}
