import pg from 'pg'

import logger from '../../build/lib/logger'

import {insertApiKey, insertLoginRole, insertPrincipal} from './sql'

const {Client} = pg

const POSTGRES_PORT = 5432

/**
 * Create pg.Client with given connection options.
 * Use `getPostgresConnectionOptions` if a connection to the `postgres` database is needed.
 * @param {object} options - {host, database, user, password}
 * @returns pg.Client
 *
 * Example:
 * ```
 * const client = createClient(options)
 * const postgresClient = createClient(getPostgresConnectionOptions(options))
 * ```
 */
const createClient = ({host, database, user, password}) =>
  new Client({
    user,
    host,
    database,
    password,
    port: POSTGRES_PORT
  })

/**
 * Maps postgres connection options to be able to create pg.Client.
 * @param {object} options - {host, postgresUser, postgresPassword}
 * @returns object - {host, database, user, password}
 */
const getPostgresConnectionOptions = ({host, postgresUser, postgresPassword}) => ({
  user: postgresUser,
  host,
  database: postgresUser ? 'postgres' : undefined,
  password: postgresPassword
})

/**
 * Opens pg connection with given connection options and invokes function `fn` with
 * open client.
 * Logs error message and stack trace and rethrows error.
 * @param {object} options - {host, database, user, password}
 * @param {function} fn - (client:Pg.Client) => Promise
 */
const handleQuery = async (options, fn) => {
  try {
    const client = createClient(options)
    await client.connect()

    try {
      await fn(client)
    } catch (err) {
      await client.end()
      throw err
    }

    await client.end()
  } catch (error) {
    logger.error(`Couldn't run query.`, error, error.stack)
    throw error
  }
}

/**
 * Returns true if postgres connection can be established, otherwise false.
 * @param {object} options - {host, database, user, password}
 * @returns boolean
 */
export const isPostgresReady = async options => {
  try {
    const client = createClient(getPostgresConnectionOptions(options))
    await client.connect()
    await client.end()
    return true
  } catch (error) {
    return false
  }
}

/**
 * Deny new connection for the database.
 * @param {object} options - {host, database user, password}
 */
export const denyDatabaseConnections = async options => {
  const {database} = options

  logger.log(`Deny connections for database '${database}'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    const query = `UPDATE pg_database SET datallowconn = false where datname = '${database}';`
    await client.query(query)
  })
}

/**
 * Allow new connection for the database.
 * @param {object} options - {host, database user, password}
 */
export const allowDatabaseConnections = async options => {
  const {database} = options

  logger.log(`Allow connections for database '${database}'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    const query = `UPDATE pg_database SET datallowconn = true where datname = '${database}';`
    await client.query(query)
  })
}

/**
 * Forcefully kills all open database connections.
 * @param {object} options - {host, database, user, password}
 */
export const killDatabaseConnections = async options => {
  const {database} = options

  logger.log(`Kill connections for database '${database}'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    const query = `SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity
      WHERE pg_stat_activity.datname = '${database}' AND pid <> pg_backend_pid();`
    await client.query(query)
  })
}

const createDatabaseUser = async (client, user, password) => {
  const result = await client.query(`SELECT rolname FROM pg_catalog.pg_roles WHERE  rolname = '${user}';`)
  if (result.rows.length !== 1) {
    await client.query(`CREATE ROLE ${user} WITH SUPERUSER LOGIN PASSWORD '${password}';`)
  }
}

const createDatabase = async (client, database, user) => {
  const query = `CREATE DATABASE ${database} WITH OWNER ${user};`
  await client.query(query)
}

const setupDatabaseWithExtesions = async options => {
  logger.log(`Create extensions for database '${options.database}'.`)
  await handleQuery(options, async client => {
    const query = `CREATE EXTENSION IF NOT EXISTS pg_trgm;
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
      CREATE EXTENSION IF NOT EXISTS unaccent;`
    await client.query(query)
  })
}

/**
 * Creates empty database for nice2 - with needed extensions and owner.
 * @param {object} options - {host, database, user, password postgresUser, postgresPassword}
 */
const createEmptyDatabase = async options => {
  await killDatabaseConnections(options)

  const {database, user, password} = options

  logger.log(`Create user '${user}' and database '${database}'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    await createDatabaseUser(client, user, password)
    await createDatabase(client, database, user)
  })

  await setupDatabaseWithExtesions(options)
}

/**
 * Returns true if template `${database}_template` exists, otherwise false.
 * @param {object} options - {host, database, postgresUser, postgresPassword}
 * @returns boolean
 */
const doesTemplateExist = async options => {
  const {database} = options

  try {
    const client = createClient(getPostgresConnectionOptions(options))
    await client.connect()

    let exists = false

    try {
      const result = await client.query(`SELECT 1 FROM pg_database WHERE datname='${database}_template'`)
      exists = result.rows.length === 1
      await client.end()
      return exists
    } catch (err) {
      await client.end()
      throw err
    }
  } catch (error) {
    logger.error(`Couldn't retrieve information about database template '${database}_template'.`, error, error.stack)
    return false
  }
}

const restoreDatabaseFromTemplate = async options => {
  const {database, user} = options

  logger.log(`Restore database '${database}'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    await client.query(`DROP DATABASE IF EXISTS ${database}`)
    await client.query(`CREATE DATABASE ${database} WITH OWNER ${user} TEMPLATE ${database}_template`)
  })
}

/**
 * Drops database template if exists.
 * @param {object} options - {host, database, user, password}
 */
export const dropDatabaseTemplate = async options => {
  const {database} = options

  logger.log(`Drop database template '${database}_template'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    await client.query(`DROP DATABASE IF EXISTS ${database}_template`)
  })
}

/**
 * Creates database template from current database.
 * @param {object} options - {host, database, user, password}
 */
export const createDatabaseTemplate = async options => {
  const {database, user} = options

  logger.log(`Create database template '${database}_template'.`)
  await handleQuery(getPostgresConnectionOptions(options), async client => {
    await client.query(`CREATE DATABASE ${database}_template WITH OWNER ${user} TEMPLATE ${database}`)
  })
}

/**
 * Drops database template and re-creates template from current database.
 * @param {object} options - {host, database, user, password}
 */
export const updateDatabaseTemplate = async options => {
  logger.log(`Update database template '${options.database}_template'.`)
  await dropDatabaseTemplate(options)
  await killDatabaseConnections(options)
  await createDatabaseTemplate(options)
}

/**
 * Restores database from template or creates a new empty database.
 * @param {object} options - {host, database, user, password, postgresUser, postgresPassword}
 */
export const restoreDatabase = async options => {
  const hasTemplate = await doesTemplateExist(options)
  if (hasTemplate) {
    logger.log(`Restore database '${options.database}' from template.`)
    await restoreDatabaseFromTemplate(options)
  } else {
    logger.log(`Create empty database '${options.database}'.`)
    await createEmptyDatabase(options)
  }
  logger.log(`Database '${options.database}' successfully restored.`)
}

/**
 * Inserts and sets up tocco user for cypress tests.
 * @param {object} options {host, database, user, password}
 * @param {string} cypressUser
 * @param {string} cypressUserPasswordHash
 * @param {string} cypressUserApiKeyHash
 */
export const setupCypressUser = async (options, cypressUser, cypressUserPasswordHash, cypressUserApiKeyHash) => {
  logger.log(`Setup Cypress user '${cypressUser}'.`)

  await handleQuery(options, async client => {
    await client.query(...insertPrincipal(cypressUser, cypressUserPasswordHash, cypressUserApiKeyHash))
    await client.query(...insertLoginRole(cypressUser))
    await client.query(...insertApiKey(cypressUser, cypressUserApiKeyHash))
  })
}

/**
 * Create `options` object from environment variables.
 * @returns object - {host, database, user, password}
 */
export const getDatabaseOptionsFromEnvs = () => ({
  host: process.env.HIBERNATE_MAIN_SERVERNAME || 'localhost',
  user: process.env.HIBERNATE_MAIN_USER || 'nice',
  password: process.env.HIBERNATE_MAIN_PASSWORD || 'nice',
  database: process.env.HIBERNATE_MAIN_DATABASENAME || 'test_cypress',
  sslMode: process.env.HIBERNATE_MAIN_SSLMODE || 'disable',
  postgresUser: process.env.POSTGRES_USER,
  postgresPassword: process.env.POSTGRES_PASSWORD
})
