import logger from '../../build/lib/logger'

import {
  restoreDatabase,
  setupCypressUser,
  updateDatabaseTemplate,
  killDatabaseConnections,
  denyDatabaseConnections,
  allowDatabaseConnections
} from './database'
import {runDatabaseRefactoring, setupEnvironmentVariablesForNice2, startNice2} from './nice2'
import {waitForNice2, waitForPostgresReady} from './waitFor'

const setupDatabaseForCypress = async options => {
  const cypressUser = process.env.CYPRESS_USER
  const cypressUserPasswordHash = process.env.CYPRESS_USER_PASSWORD_HASH
  const cypressUserApiKeyHash = process.env.CYPRESS_USER_API_KEY_HASH

  await setupCypressUser(options, cypressUser, cypressUserPasswordHash, cypressUserApiKeyHash)
}

/**
 * Gets nice2 ready for cypress testing - restores database, runs database refactoring and starts nice2.
 * @param {object} options - {host, database, user, password}
 * @param {boolean} skipDatabaseRefatoring
 */
export const run = async (options, skipDatabaseRefatoring = false) => {
  await waitForPostgresReady(options)

  await restoreDatabase(options)

  setupEnvironmentVariablesForNice2(options)

  if (!skipDatabaseRefatoring) {
    await runDatabaseRefactoring()
    await setupDatabaseForCypress(options)
    await updateDatabaseTemplate(options)
  } else {
    logger.log('Skips database refactoring')
  }

  startNice2()
}

/**
 * Restores database forcefully while nice2 is running.
 * @param {object} options - {host, database, user, password}
 * @param {string} backendUrl
 */
export const restoreDatabaseForcefully = async (options, backendUrl) => {
  await waitForNice2(backendUrl)

  await denyDatabaseConnections(options)
  await killDatabaseConnections(options)
  await restoreDatabase(options)
  await allowDatabaseConnections(options)

  await waitForNice2(backendUrl)
}
