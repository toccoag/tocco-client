import dotenv from 'dotenv'
import yargs from 'yargs/yargs'

import {dropDatabaseTemplate, getDatabaseOptionsFromEnvs, setupCypressUser, updateDatabaseTemplate} from './database'
import {publishCypressExport} from './publish'
import {restoreDatabaseForcefully, run} from './run'
import {waitForNice2} from './waitFor'

dotenv.config({path: './.env'})

yargs(process.argv.slice(2))
  .scriptName('cli')
  .usage('$0 <cmd> [args]')
  .command(
    'db:drop-template',
    'drop database template',
    () => {},
    async () => {
      await dropDatabaseTemplate(getDatabaseOptionsFromEnvs())
    }
  )
  .command(
    'db:update-template',
    'updated database template from current database',
    () => {},
    async () => {
      await updateDatabaseTemplate(getDatabaseOptionsFromEnvs())
    }
  )
  .command(
    'db:prepare',
    'insert cypress user for testing',
    () => {},
    async () => {
      const cypressUser = process.env.CYPRESS_USER
      const cypressUserPasswordHash = process.env.CYPRESS_USER_PASSWORD_HASH
      const cypressUserApiKeyHash = process.env.CYPRESS_USER_API_KEY_HASH

      await setupCypressUser(getDatabaseOptionsFromEnvs(), cypressUser, cypressUserPasswordHash, cypressUserApiKeyHash)
    }
  )
  .command(
    'waitFor:nice2',
    'pings nice2 and waits to be available',
    () => {},
    async () => {
      const backendUrl = process.env.BACKEND_URL || 'http://localhost:8080'
      await waitForNice2(backendUrl)
    }
  )
  .command(
    'run:nice2',
    'prepares and start nice2 for cypress testing',
    y => {
      return y.option('s', {
        alias: 'skip',
        describe: 'skips db refactoring during start'
      })
    },
    async argv => {
      await run(getDatabaseOptionsFromEnvs(), Boolean(argv.s))
    }
  )
  .command(
    'run:force-restore',
    'restores database forcefully',
    () => {},
    async () => {
      const backendUrl = process.env.BACKEND_URL || 'http://localhost:8080'
      await restoreDatabaseForcefully(getDatabaseOptionsFromEnvs(), backendUrl)
    }
  )
  .command(
    'cypress:publish',
    'publishes the cypress exports to gitlab pages',
    () => {},
    () => {
      publishCypressExport()
    }
  )
  .parse()
