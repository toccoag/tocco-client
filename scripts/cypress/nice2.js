import logger from '../../build/lib/logger'
import {getNice2Folder} from '../../build/lib/nice2'
import {spawn} from '../helpers'

/**
 * Sets environment variables for nice2 gradle task.
 * @param {object} options - {host, user, password, database, sslMode}
 */
export const setupEnvironmentVariablesForNice2 = ({host, user, password, database, sslMode}) => {
  process.env.HIBERNATE_MAIN_SERVERNAME = host
  process.env.HIBERNATE_MAIN_USER = user
  process.env.HIBERNATE_MAIN_PASSWORD = password
  process.env.HIBERNATE_MAIN_DATABASENAME = database
  process.env.HIBERNATE_MAIN_SSLMODE = sslMode
  process.env.NICE2_WIDGET_ENABLEWIDGETSERVER = true
}

/**
 * Start nice2 via shell
 */
export const startNice2 = () => {
  logger.log('Start nice2')

  const folder = getNice2Folder()
  spawn(
    // eslint-disable-next-line max-len
    `sh ./gradlew customer:test:bootRun -PmaxHeapSize='4G' --args='--spring.profiles.active=development --spring.output.ansi.enabled=ALWAYS'`,
    folder
  )
}

/**
 * Run database refactoring via shell
 */
export const runDatabaseRefactoring = async () => {
  logger.log('Run database refactoring')

  const folder = getNice2Folder()
  await spawn(
    // eslint-disable-next-line max-len
    `sh ./gradlew customer:test:bootRun -PjvmArgs='-Dch.tocco.nice2.disableSchemaModelStartupCheck=true' --args='--spring.profiles.active=development --spring.output.ansi.enabled=ALWAYS --ch.tocco.nice2.enableUpgradeMode=true --ch.tocco.nice2.enterprisesearch.disableStartup=true --ch.tocco.nice2.disableRoleSync=true --ch.tocco.nice2.disableLanguageSync=true --ch.tocco.nice2.disableBusinessUnitSync=true --nice2.reporting.runReportSynchronizerFragment=true'`,
    folder
  )
}
