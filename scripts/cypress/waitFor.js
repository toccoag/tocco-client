import fetch from 'isomorphic-fetch'

import logger from '../../build/lib/logger'

import {isPostgresReady} from './database'

const isHostAvailable = async url => {
  try {
    const response = await fetch(url, {method: 'GET'})

    const statusCode = response.status
    if (statusCode !== 200) {
      throw new Error('Server not available')
    }
    return true
  } catch (error) {
    return false
  }
}

/**
 * Calls `fn` and returns true if `fn` return true.
 * Aborts after 10 tries and returns false.
 *
 * `tries` parameter should not be used when invoked.
 * @param {function} fn - () => boolean
 * @param {number} tries - optional
 * @returns boolean
 *
 * Example:
 * ```
 * const isAvailable = await waitFor(() => isHostAvailable(url))
 * ```
 */
export const waitFor = (fn, tries = 1) =>
  new Promise((resolve, reject) => {
    fn().then(isReady => {
      if (isReady) {
        resolve(true)
      } else if (tries === 10) {
        reject(new Error('not ready'))
      } else {
        logger.warn(`${tries}/10 tries.`)
        setTimeout(() => {
          waitFor(fn, tries + 1)
            .then(resolve)
            .catch(reject)
        }, 1000)
      }
    })
  })

/**
 * Pings server and returns true if server is available.
 * Aborts after 10 tries and returns false.
 *
 * @param {string} url
 * @returns boolean
 *
 * Example:
 * ```
 * const isAvailable = await waitForReadyness('https://www.google.ch')
 * ```
 */
export const waitForHostAvailable = url => {
  logger.log(`Wait for ${url}`)
  return waitFor(() => isHostAvailable(url))
}

/**
 * Connects to database and return true if database is available.
 * Aborts after 10 tries and returns false.
 *
 * @param {object} options - {host, database, user, password}
 * @returns boolean
 *
 *  * Example:
 * ```
 * const options = {host: 'localhost', database: 'test_cypress', user: 'nice', password: 'nice'}
 * const isReady = await waitForPostgresReady(options)
 * ```
 */
export const waitForPostgresReady = options => {
  logger.log(`Wait for postgres`)
  return waitFor(() => isPostgresReady(options))
}

export const waitForNice2 = async url => {
  await waitForHostAvailable(`${url}/status-tocco`)
  await waitForHostAvailable(`${url}/tocco`)
}
