import {readFileSync, mkdirSync, existsSync, writeFileSync} from 'fs'

import showdown from 'showdown'

import logger from '../../build/lib/logger'

export const publishCypressExport = () => {
  try {
    const converter = new showdown.Converter({tables: true})
    const testcases = readFileSync('cypress/exports/testcases.md', 'utf8')
    const html = converter.makeHtml(testcases)

    if (!existsSync('public')) {
      mkdirSync('public')
    }

    const file = `<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<style>
  *, html {
    font-family: Avenir, Arial, sans-serif;
  }

  body {
    background-color: rgb(222, 230, 237);
  }

  #container {
    max-width: 1024px;
    background-color: white;
    padding: 20px;
    border-radius: 10px;
    margin: 20px auto;
  }

  table {
    border-collapse: collapse;
    border: 1px solid black;
  }

  th, td {
    border: 1px solid black;
    padding: 8px;
  }

  tr:hover {
    background-color: #ddd;
  }

  tr:hover td {
    background-color: transparent;
  }

  h1 {margin: 0 0 0.5em;}
  h2 {margin: 1.5em 0 0.5em;}
  h3 {margin: 1em 0 0.5em;}
  p {margin: 0;}
</style>
</head>
<body>
<div id="container">
${html}
</div>
</body>
</html>`

    writeFileSync('public/index.html', file)
    logger.log('Published the cypress test cases.')
  } catch (error) {
    logger.error('Could not publish cypress test cases.', error, error.stack)
  }
}
