import _intersection from 'lodash/intersection'
import _uniq from 'lodash/uniq'

import logger from '../build/lib/logger'
import {getAllPackages, getPackageJson, isPackageReleased, packageExists} from '../build/lib/packages'

import {
  execute,
  existsGitTag,
  getAllToccoDependencies,
  getCustomersOnVersion,
  getGitLogSinceTag,
  getGitReleaseTag,
  getMandatoryEnvVariable,
  getNiceVersionWithoutPatch,
  sendSlackMessage
} from './helpers'

/**
 * Gets next version for a package. If the version has the wrong major and/or minor, the correct one is used
 */
export const getNextVersion = packageName => {
  const currentVersion = getPackageJson(packageName).version
  const niceVersionWithoutPatch = getNiceVersionWithoutPatch()

  if (currentVersion.startsWith(`${niceVersionWithoutPatch}.`)) {
    const [major, manor, patch] = currentVersion.split('.', 3)
    return `${major}.${manor}.${parseInt(patch) + 1}`
  } else {
    return `${niceVersionWithoutPatch}.0`
  }
}

/**
 * Returns a list of packages defined in the commit message subject
 */
export const getPackageNamesFromCommit = commitMessageSubject => {
  const allowedPattern = /^(\w+)(\(.+\))?: (.+)$/
  const match = allowedPattern.exec(commitMessageSubject)
  if (match && match[2]) {
    const packagesString = match[2].slice(1, -1)
    if (packagesString === 'tocco-client') {
      return getAllPackages()
    } else {
      return packagesString.split(',').map(line => line.trim())
    }
  }

  return []
}

/**
 * Parses the git log between git tag and head to a list of objects with packages and changelog
 */
export const parseGitLog = async gitTag => {
  const commits = await getGitLogSinceTag(gitTag)
  return commits.map(commit => {
    const subject = commit.split('\n', 1)[0]
    const packages = getPackageNamesFromCommit(subject)

    const changelog = commit
      .split('\n')
      .map(line => line.trim())
      .find(line => line.toLowerCase().startsWith('changelog:'))
      ?.replace(/Changelog:/i, '')
      ?.trim()

    return {
      packages,
      changelog
    }
  })
}

/**
 * Gets changelog (unreleased (related) commits) of a package.
 */
export const getNextChangelog = async packageName => {
  const gitReleaseTag = getGitReleaseTag(packageName)

  // return hard coded message if package was not already released for this version
  // else reading the git history fails and the package is not released
  if (!(await existsGitTag(gitReleaseTag))) {
    return `- initial release for version ${getNiceVersionWithoutPatch()}`
  }

  const allDependencies = getAllToccoDependencies(packageName)
  const commits = await parseGitLog(gitReleaseTag)
  return _uniq(
    commits
      // only get commits related to this package
      .filter(commit => _intersection(commit.packages, allDependencies).length > 0)
      .filter(commit => commit.changelog)
      .map(commit => `- ${commit.changelog}`)
  ).join('\n')
}

/**
 * Gets candidate packages which should be released.
 * Either via method argument a list of packages to release is passed or all releasable packages are returned
 */
export const getPackagesToRelease = async releasingPackages => {
  let packages
  if (releasingPackages) {
    packages = releasingPackages.split(',').map(packageName => packageName.trim())

    packages.forEach(packageName => {
      if (!packageExists(packageName)) {
        logger.error(`Package "${packageName}" does not exist!`)
        process.exit(1)
      }

      if (!isPackageReleased(packageName)) {
        logger.error(`Package "${packageName}" cannot be released because it is private!`)
        process.exit(1)
      }
    })
  } else {
    packages = await getAllCandidateReleasePackages()
  }

  return packages
}

/**
 * Gets all candidate release packages.
 * Only customer bundles with an active back office installation are returned.
 */
const getAllCandidateReleasePackages = async () => {
  const customers = await getCustomersOnVersion(
    getMandatoryEnvVariable('BACK_OFFICE_USERNAME'),
    getMandatoryEnvVariable('BACK_OFFICE_API_KEY')
  )

  logger.log(`The following customers have an installation on this version: ${customers}`)

  const excludeFn = directory => {
    if (!directory.startsWith('customers/')) {
      return true
    }

    const customerName = directory.replace('customers/', '').split('/')[0]
    return customers.includes(customerName)
  }

  return getAllPackages(excludeFn).filter(p => isPackageReleased(p))
}

/**
 * Returns the commit message for the released packages. Packages is a list of objects with the package and version
 */
export const getReleaseCommitMessage = packages => {
  const commitMessageBody = packages.map(p => `- ${p.package}@${p.version}`).join('\n')
  return `chore: release packages\n\n${commitMessageBody}`
}

/**
 * Sends release message to a slack webhook
 */
export const sendReleaseMessage = async (packageNameAndVersion, changelog) => {
  try {
    const body = {
      channel: 'C02J80NPY9Y', // channel id of #frontend-releases
      blocks: [
        {
          type: 'section',
          text: {
            type: 'mrkdwn',
            text: `:rocket: *${packageNameAndVersion} released*`
          }
        },
        ...(changelog
          ? [
              {
                type: 'divider'
              },
              {
                type: 'section',
                text: {
                  type: 'mrkdwn',
                  text: `:page_facing_up: Changelog:\n${changelog}`
                }
              }
            ]
          : [])
      ]
    }
    await sendSlackMessage(body)
  } catch (error) {
    logger.warn('Could not send release message to Slack')
  }
}

/**
 * Checks if the script is executed in the CI (CI environment variable is set)
 */
export const checkIfRunningInCI = () => {
  if (!process.env.CI) {
    const message = `The script should only be executed on the Gitlab CI!
      This is a security mechanism to avoid running the script locally without configuring anything correctly.
      The script is normally only executed on release branches and master (defined in .gitlab-ci.yml).
      If the script is executed locally, you must ensure that you don't release a package 
      without committing the changed package version. Else the CI will fail the next time releasing this package
      as an existing package version cannot be overridden. Additionally the NPM tag and git tag are moved
      or the git and ssh config is changed which leads to issues.`
    logger.error(message)
    process.exit(1)
  }
}

/**
 * Write the npm auth token to .yarnrc.yml
 */
export const writeYarnrc = async npmAuthToken => {
  await execute(`yarn config set 'npmRegistries["//registry.npmjs.org"].npmAuthToken' ${npmAuthToken}`)
}
