/**
 * Script opens a new merge request on Gitlab
 *
 * Execution:
 * node ./openMergeRequest
 *
 * Workflow:
 * - load open merge requests
 * - create new merge request if not already one exist for this branch
 *
 * Mandatory environment variables:
 * - CI_COMMIT_REF_NAME: target branch for which the job is executed (always passed by Gitlab)
 * - CI_PROJECT_ID: project id for which the job is executed (always passed by Gitlab)
 * - GITLAB_ACCESS_TOKEN: Gitlab api key to interact with the rest api
 */

import fetch from 'isomorphic-fetch'

import logger from '../build/lib/logger'

import {getMandatoryEnvVariable} from './helpers'
import {getTargetBranch} from './openMergeRequestHelpers'

/**
 * Get list of open merge request source branches
 */
const getOpenedMergeRequestSourceBranches = async (ciProjectId, gitlabAccessToken) => {
  const options = {
    headers: {
      'Content-Type': 'application/json',
      'PRIVATE-TOKEN': gitlabAccessToken
    }
  }

  const response = await fetch(`https://gitlab.com/api/v4/projects/${ciProjectId}/merge_requests?state=opened`, options)
  const statusCode = response.status
  const data = await response.json()
  if (statusCode === 401) {
    throw new Error('Unauthorized')
  } else if (statusCode >= 400) {
    throw new Error(`Could not fetch merge requests. Status code: ${statusCode}`)
  }

  return data.map(r => r.source_branch)
}

/**
 * open a new merge request for a source branch
 */
const openMergeRequest = async (ciProjectId, gitlabAccessToken, sourceBranch, targetBranch) => {
  const body = {
    id: ciProjectId,
    source_branch: sourceBranch,
    target_branch: targetBranch,
    remove_source_branch: true,
    title: sourceBranch
  }

  const options = {
    headers: {
      'Content-Type': 'application/json',
      'PRIVATE-TOKEN': gitlabAccessToken
    },
    method: 'POST',
    body: JSON.stringify(body)
  }

  const response = await fetch(`https://gitlab.com/api/v4/projects/${ciProjectId}/merge_requests`, options)
  const statusCode = response.status
  if (statusCode === 401) {
    throw new Error('Unauthorized')
  } else if (statusCode >= 400) {
    throw new Error(`Could not create merge requests. Status code: ${statusCode}`)
  }
}

const main = async () => {
  const ciProjectId = getMandatoryEnvVariable('CI_PROJECT_ID')
  const gitlabAccessToken = getMandatoryEnvVariable('GITLAB_ACCESS_TOKEN')
  const sourceBranch = getMandatoryEnvVariable('CI_COMMIT_REF_NAME')

  logger.log(`Check if merge request for branch '${sourceBranch}' should be opened`)

  const openedMergeRequestBranches = await getOpenedMergeRequestSourceBranches(ciProjectId, gitlabAccessToken)
  if (openedMergeRequestBranches.includes(sourceBranch)) {
    logger.info('No new merge request opened')
  } else {
    const targetBranch = getTargetBranch(sourceBranch)
    openMergeRequest(ciProjectId, gitlabAccessToken, sourceBranch, targetBranch)
    logger.info(`Opened a new merge request with target branch ${targetBranch}`)
  }
}

main()
