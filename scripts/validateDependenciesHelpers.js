import {readFile} from '../build/lib/packages'

/**
 * Extract the module name from a source code line. If nothing found undefined is returned
 */
export const extractModuleName = line => {
  const match = line.match(/^.* from '(?<module>[^']*)'/)
  const moduleString = match?.groups?.module
  return moduleString?.split('/')[0]
}

/**
 * Returns list of Tocco module names from a single file
 */
export const getToccoModuleNames = filePath => {
  return readFile(filePath)
    .split('\n')
    .map(line => extractModuleName(line))
    .filter(moduleName => moduleName?.startsWith('tocco-'))
}
