import {mockReadFile} from './helpers'
import {extractModuleName, getToccoModuleNames} from './validateDependenciesHelpers'

describe('scripts', () => {
  describe('validateDependenciesHelpers', () => {
    describe('extractModuleName', () => {
      test('single line import', () => {
        const line = "import {scrollBehaviourPropType} from 'tocco-ui'"
        expect(extractModuleName(line)).to.eql('tocco-ui')
      })

      test('single line import with path', () => {
        const line = "import SimpleFormApp from 'tocco-simple-form/src/main'"
        expect(extractModuleName(line)).to.eql('tocco-simple-form')
      })

      test('import over multiple lines', () => {
        /*
        import {
          ...
        } from 'tocco-app-extensions'
         */
        const line = "} from 'tocco-app-extensions'"
        expect(extractModuleName(line)).to.eql('tocco-app-extensions')
      })

      test('no import line', () => {
        const line = "const packageName = 'entity-detail'"
        expect(extractModuleName(line)).to.eql(undefined)
      })
    })

    describe('getToccoModuleNames', () => {
      test('filter out non tocco modules', () => {
        const content = `import PropTypes from 'prop-types'
import {fork} from 'redux-saga/effects'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  formData,
  notification,
  selection
} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {appContext, reducer as reducerUtil, saga as sagaUtil} from 'tocco-util'

import ParticipationView from './components/ParticipationView'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'evaluation-execution-participation-action'
const EXTERNAL_EVENTS = ['onSuccess', 'onCancel', 'emitAction']`

        mockReadFile(content)

        expect(getToccoModuleNames('ignoredPath')).to.eql(['tocco-app-extensions', 'tocco-entity-list', 'tocco-util'])
      })
    })
  })
})
