import * as logger from '../build/lib/logger'

import {mockGetVariables} from './helpers'
import {isValidCommitMessage} from './validateCommitMessageHelpers'

describe('scripts', () => {
  describe('validateCommitMessageHelpers', () => {
    describe('isValidCommitMessage', () => {
      const expectInvalidCommitMessage = (message, expectedError) => {
        const errorFn = jest.fn()
        jest.spyOn(logger.default, 'error').mockImplementation(errorFn)

        expect(isValidCommitMessage(message)).to.be.false

        expect(errorFn.mock.calls.length).to.eql(1)
        expect(errorFn.mock.calls[0][0]).to.eql(expectedError)
      }

      const expectValidCommitMessage = message => {
        const errorFn = jest.fn()
        jest.spyOn(logger.default, 'error').mockImplementation(errorFn)

        expect(isValidCommitMessage(message)).to.be.true

        expect(errorFn.mock.calls.length).to.eql(0)
      }

      test('first line is too long', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectInvalidCommitMessage('x'.repeat(71), 'The first line of the commit message is longer than 70 characters!')
      })

      test('first line has max length', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        const prefix = 'fix: '
        expectValidCommitMessage(prefix + 'x'.repeat(70 - prefix.length))
      })

      test('no type is invalid', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectInvalidCommitMessage(
          'fix test',
          'The commit message does not match pattern "<type>(<scope>): <subject>" but it was "fix test"!'
        )
      })

      test('invalid type', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectInvalidCommitMessage('invalid: fix test', 'In the commit message "invalid" is not an allowed type!')
      })

      test('invalid scope', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectInvalidCommitMessage('fix(invalid): fix test', 'In the commit message "invalid" is not an allowed scope!')
      })

      test('tocco-client is a valid scope', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectValidCommitMessage('fix(tocco-client): fix test')
      })

      test('oneliner is valid', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        expectValidCommitMessage('test: fix test')
      })

      test('second line is not empty', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        const message = `test: fix test
        invalid on this line`
        expectInvalidCommitMessage(message, 'Second line of the commit message has to be an empty line!')
      })

      test('complex', () => {
        mockGetVariables({requireCherryPickInCommitMessage: false})

        const message = `refactor(entity-list): refactor

more detailed describtion

Refs: TOCDEV-1
Cherry-pick: Up
Changelog: refactor entity-list`
        expectValidCommitMessage(message)
      })

      describe('cherry pick', () => {
        test('body must contain cherry pick', () => {
          mockGetVariables({requireCherryPickInCommitMessage: true})

          const message = `test: fix test

Refs: TOCDEV-1`
          expectInvalidCommitMessage(message, 'The body of the commit message must contain Cherry-pick: Up/No!')
        })

        test('body contains cherry pick up', () => {
          mockGetVariables({requireCherryPickInCommitMessage: true})

          const message = `test: fix test

Cherry-pick: Up`
          expectValidCommitMessage(message)
        })

        test('body contains cherry pick no', () => {
          mockGetVariables({requireCherryPickInCommitMessage: true})

          const message = `test: fix test

Cherry-pick: No`
          expectValidCommitMessage(message)
        })

        test('body contains cherry pick case insensitive', () => {
          mockGetVariables({requireCherryPickInCommitMessage: true})

          const message = `test: fix test

CherRy-PiCk: uP`
          expectValidCommitMessage(message)
        })

        test('body contains cherry pick but it is not required', () => {
          mockGetVariables({requireCherryPickInCommitMessage: false})

          const message = `test: fix test

Cherry-pick: No`
          expectValidCommitMessage(message)
        })
      })
    })
  })
})
