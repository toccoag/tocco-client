/**
 * Script checks for each package if all tocco dependencies used in the code are defined in the package.json
 *
 * Execution:
 * node ./validateDependencies
 */

import _difference from 'lodash/difference'
import _uniq from 'lodash/uniq'

import logger from '../build/lib/logger'
import {getAllPackages, getAllFilesRecursive, getPackageDirectory} from '../build/lib/packages'

import {getAllToccoDependencies, transformPackageName} from './helpers'
import {getToccoModuleNames} from './validateDependenciesHelpers'

const getMissingDependencies = packageName => {
  const packageDirectory = getPackageDirectory(packageName)
  const allJsFiles = getAllFilesRecursive(`${packageDirectory}/src`).filter(file => file.endsWith('.js'))

  const expectedDependencies = _uniq(allJsFiles.map(file => getToccoModuleNames(file)).flat())
  const actualDependencies = getAllToccoDependencies(packageName).map(name => transformPackageName(name))

  return _difference(expectedDependencies, actualDependencies).map(dependency => ({packageName, dependency}))
}

const main = async () => {
  const missingDependencies = getAllPackages()
    .map(packageName => getMissingDependencies(packageName))
    .flat()

  if (missingDependencies.length > 0) {
    const list = missingDependencies
      .map(m => `- add dependency '${m.dependency}' to package '${m.packageName}'`)
      .join('\n')
    logger.error(`Not all dependencies used in the code are correctly defined in the package.json. \n${list}`)
    process.exit(1)
  }

  logger.success('All dependencies used in the code are correctly defined in the package.json')
}

main()
