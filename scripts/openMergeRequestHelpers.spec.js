import {getTargetBranch} from './openMergeRequestHelpers'

describe('scripts', () => {
  describe('openMergeRequestHelpers', () => {
    describe('getTargetBranch', () => {
      test('master', () => {
        expect(getTargetBranch('cherry-picking/_master_auto-1704989687')).to.eql('master')
      })

      test('release branch', () => {
        expect(getTargetBranch('cherry-picking/_nice-releases/37_auto-1705313747')).to.eql('nice-releases/37')
      })
    })
  })
})
