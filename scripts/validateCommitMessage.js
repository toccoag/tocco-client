/**
 * Script validate commit message (saved in .git/COMMIT_EDITMSG)
 */

import {existsSync, lstatSync, readFile, readFileSync} from 'fs'

import logger from '../build/lib/logger'

import {isValidCommitMessage} from './validateCommitMessageHelpers'

function getGitFolder() {
  let gitDirLocation = './.git'
  if (!existsSync(gitDirLocation)) {
    logger.error(`Cannot find file ${gitDirLocation} during commit message validation`)
  }

  if (!lstatSync(gitDirLocation).isDirectory()) {
    const unparsedText = '' + readFileSync(gitDirLocation)
    gitDirLocation = unparsedText.substring('gitdir: '.length).trim()
  }

  if (!existsSync(gitDirLocation)) {
    logger.error(`Cannot find file ${gitDirLocation} during commit message validation`)
  }

  return gitDirLocation
}

const commitMsgFile = process.argv[2] || getGitFolder() + '/COMMIT_EDITMSG'

readFile(commitMsgFile, function (err, buffer) {
  const message = buffer.toString()

  if (err) {
    logger.error(`Error reading file during commit message validation: ${err}`)
  }

  if (isValidCommitMessage(message)) {
    process.exit(0)
  } else {
    process.exit(1)
  }
})
