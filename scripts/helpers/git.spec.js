import {getGitReleaseTag} from './git'
import {mockGetVariables} from './mocking'

describe('scripts', () => {
  describe('helpers', () => {
    describe('git', () => {
      describe('getGitReleaseTag', () => {
        test('return git release tag name', () => {
          mockGetVariables({niceVersion: '3.6.0'})

          expect(getGitReleaseTag('admin')).to.equal('tocco-admin@nice36')
        })
      })
    })
  })
})
