import {execute, prependFile, spawn} from './fileSystem'
import {existsGitTag, getGitLogSinceTag, getGitReleaseTag, setupGitlab, setupGitUser} from './git'
import {
  getAllToccoDependencies,
  getCustomersOnVersion,
  getMandatoryEnvVariable,
  getNpmTag,
  getNiceVersionWithoutPatch,
  getNiceVersionWithoutPatchAndDots,
  sendSlackMessage,
  transformPackageName,
  hasCherrypickUp,
  hasCherrypickNo
} from './helpers'
import {mockDependencies, mockExistsGitTag, mockGetGitLogSinceTag, mockGetVariables, mockReadFile} from './mocking'

export {
  execute,
  spawn,
  existsGitTag,
  getAllToccoDependencies,
  getCustomersOnVersion,
  getGitLogSinceTag,
  getGitReleaseTag,
  getMandatoryEnvVariable,
  getNpmTag,
  getNiceVersionWithoutPatch,
  getNiceVersionWithoutPatchAndDots,
  hasCherrypickUp,
  hasCherrypickNo,
  mockDependencies,
  mockExistsGitTag,
  mockGetGitLogSinceTag,
  mockGetVariables,
  mockReadFile,
  prependFile,
  sendSlackMessage,
  setupGitlab,
  setupGitUser,
  transformPackageName
}
