import fetch from 'isomorphic-fetch'
import _flatten from 'lodash/flatten'
import _uniq from 'lodash/uniq'

import logger from '../../build/lib/logger'
import {getPackageJson, packageExists, readJsonFile} from '../../build/lib/packages'

/**
 * Gets value of mandatory env variable. Exits program if variable is not set
 */
export const getMandatoryEnvVariable = name => {
  const variable = process.env[name]
  if (!variable) {
    logger.error(`Mandatory env variable "${name}" is not set`)
    process.exit(1)
  }
  return variable
}

/**
 * Gets npm tag of current version (e.g. nice39)
 */
export const getNpmTag = () => `nice${getNiceVersionWithoutPatchAndDots()}`

/**
 * Gets current nice version without patch number and dots (e.g. 39)
 */
export const getNiceVersionWithoutPatchAndDots = () => getNiceVersionWithoutPatch().replace('.', '')

/**
 * Gets current nice version without patch number (e.g. 3.9)
 */
export const getNiceVersionWithoutPatch = () =>
  readJsonFile('./scripts/variables.json').niceVersion.split('.').slice(0, 2).join('.')

/**
 * Transforms package name to dependency name which are always prefixed with 'tocco-'
 */
export const transformPackageName = packageName => {
  if (packageName.startsWith('tocco-')) {
    return packageName
  } else {
    return `tocco-${packageName}`
  }
}

/**
 * Transforms dependency name back to package name and remove tocco prefix if necessary
 */
export const transformPackageNameBack = packageName =>
  packageExists(packageName) ? packageName : packageName.replace('tocco-', '')

/**
 * Returns list of dev dependencies of a package
 */
const getDevDependenciesOfPackage = packageName => Object.keys(getPackageJson(packageName).devDependencies || {})

/**
 * Gets all (also transitive) tocco dependencies of a package
 *
 * `alreadyChecked` is an internal parameter used to cache which packages are already checked
 */
export const getAllToccoDependencies = (packageName, alreadyChecked = []) => {
  if (alreadyChecked.includes(packageName)) {
    return []
  }
  alreadyChecked.push(packageName)

  return _uniq(
    _flatten([
      transformPackageName(packageName),
      ...getDevDependenciesOfPackage(packageName)
        .filter(p => p.startsWith('tocco-'))
        .map(p => getAllToccoDependencies(transformPackageNameBack(p), alreadyChecked))
    ])
  )
    .sort()
    .map(name => transformPackageNameBack(name))
}

/**
 * Sends slack message with app
 * See https://api.slack.com/methods/chat.postMessage for body options (channel name and attachments/blocks/text are required)
 */
export const sendSlackMessage = async body => {
  const options = {
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${getMandatoryEnvVariable('SLACK_AUTH_TOKEN')}`
    },
    method: 'POST',
    body: JSON.stringify(body)
  }

  await fetch('https://slack.com/api/chat.postMessage', options)
}

/**
 * Gets customers on version of current branch. The following conditions must be fullfilled:
 * - user has role itinfrastructureguest
 * - there is a module with type 'customer_module'
 * - the module has not the status 'deleted'
 * - where exists a non obsolete installation linked to the version
 */
export const getCustomersOnVersion = async (username, apiKey) => {
  const where = `relModule_type.unique_id == "customer_module" 
  and relModule_status.unique_id != "deleted" 
  and exists(
    relInstallation where relInstallation_status.unique_id != "obsolete" 
    and relProject_version.label == "${getNiceVersionWithoutPatch()}"
  )`

  const options = {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic ${btoa(`${username}:${apiKey}`)}`
    },
    method: 'GET'
  }

  const response = await fetch(
    `https://tocco.ch/nice2/rest/entities/2.0/Module/?_where=${where}&_sort=unique_id&_limit=1000&_paths=unique_id`,
    options
  )

  if (response.status !== 200) {
    throw new Error('Cannot load customers from back office!')
  }

  const json = await response.json()

  return json.data.map(d => d.paths.unique_id.value)
}

/**
 * Returns true if commit message body contains Cherry-pick: Up
 */
export const hasCherrypickUp = commitMessageBody =>
  commitMessageBody.split('\n').some(l => containsCaseInsensitive(l, 'Cherry-pick: Up'))

/**
 * Returns true if commit message body contains Cherry-pick: No
 */
export const hasCherrypickNo = commitMessageBody =>
  commitMessageBody.split('\n').some(l => containsCaseInsensitive(l, 'Cherry-pick: No'))

const containsCaseInsensitive = (string, searchString) =>
  string.trim().replace(' ', '').toLowerCase().includes(searchString.trim().replace(' ', '').toLowerCase())
