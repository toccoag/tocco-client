import {
  getNiceVersionWithoutPatch,
  getNiceVersionWithoutPatchAndDots,
  getNpmTag,
  transformPackageName,
  transformPackageNameBack,
  getAllToccoDependencies,
  hasCherrypickUp,
  hasCherrypickNo
} from './helpers'
import {mockDependencies, mockGetVariables} from './mocking'

describe('scripts', () => {
  describe('helpers', () => {
    describe('helpers', () => {
      mockDependencies()

      describe('getNiceVersionWithoutPatch', () => {
        test('test minor version less than 10', () => {
          mockGetVariables({niceVersion: '3.6.0'})

          expect(getNiceVersionWithoutPatch()).to.equal('3.6')
        })

        test('test minor version larger than 10', () => {
          mockGetVariables({niceVersion: '3.11.0'})

          expect(getNiceVersionWithoutPatch()).to.equal('3.11')
        })

        test('test ignore patch version', () => {
          mockGetVariables({niceVersion: '3.6.1'})

          expect(getNiceVersionWithoutPatch()).to.equal('3.6')
        })
      })

      describe('getNiceVersionWithoutPatchAndDots', () => {
        test('return version without patch and docs', () => {
          mockGetVariables({niceVersion: '3.6.0'})

          expect(getNiceVersionWithoutPatchAndDots()).to.equal('36')
        })
      })

      describe('getNpmTag', () => {
        test('return npm tag name', () => {
          mockGetVariables({niceVersion: '3.6.0'})

          expect(getNpmTag()).to.equal('nice36')
        })
      })

      describe('transformPackageName', () => {
        test('already prefixed', () => {
          expect(transformPackageName('tocco-ui')).to.equal('tocco-ui')
        })

        test('add prefix', () => {
          expect(transformPackageName('admin')).to.equal('tocco-admin')
        })
      })

      describe('transformPackageNameBack', () => {
        test('package names starts with tocco-', () => {
          expect(transformPackageNameBack('tocco-ui')).to.equal('tocco-ui')
        })

        test('remove prefix', () => {
          expect(transformPackageNameBack('tocco-admin')).to.equal('admin')
        })
      })

      describe('getAllToccoDependencies', () => {
        test('has no dependency', () => {
          expect(getAllToccoDependencies('tocco-util')).to.eql(['tocco-util'])
        })

        test('has dependencies', () => {
          expect(getAllToccoDependencies('tocco-ui')).to.eql(['tocco-theme', 'tocco-ui', 'tocco-util'])
        })

        test('has transitive dependencies', () => {
          expect(getAllToccoDependencies('devcon')).to.eql([
            'app-extensions',
            'devcon',
            'tocco-test-util',
            'tocco-theme',
            'tocco-ui',
            'tocco-util'
          ])
        })
      })

      describe('hasCherrypickUp', () => {
        test('no cherry pick', () => {
          const commitMessageBody = 'Refs: TOCDEV-1'
          expect(hasCherrypickUp(commitMessageBody)).to.be.false
        })

        test('has cherry pick up', () => {
          const commitMessageBody = 'Cherry-pick: Up'
          expect(hasCherrypickUp(commitMessageBody)).to.be.true
        })

        test('has cherry pick no', () => {
          const commitMessageBody = 'Cherry-pick: No'
          expect(hasCherrypickUp(commitMessageBody)).to.be.false
        })

        test('has cherry pick insensitive', () => {
          const commitMessageBody = 'CherRy-PiCk: uP'
          expect(hasCherrypickUp(commitMessageBody)).to.be.true
        })

        test('has cherry pick no whitespace', () => {
          const commitMessageBody = 'Cherry-pick:Up'
          expect(hasCherrypickUp(commitMessageBody)).to.be.true
        })
      })

      describe('hasCherrypickNo', () => {
        test('no cherry pick', () => {
          const commitMessageBody = 'Refs: TOCDEV-1'
          expect(hasCherrypickNo(commitMessageBody)).to.be.false
        })

        test('has cherry pick no', () => {
          const commitMessageBody = 'Cherry-pick: No'
          expect(hasCherrypickNo(commitMessageBody)).to.be.true
        })

        test('has cherry pick uo', () => {
          const commitMessageBody = 'Cherry-pick: Up'
          expect(hasCherrypickNo(commitMessageBody)).to.be.false
        })

        test('has cherry pick insensitive', () => {
          const commitMessageBody = 'CherRy-PiCk: nO'
          expect(hasCherrypickNo(commitMessageBody)).to.be.true
        })

        test('has cherry pick no whitespace', () => {
          const commitMessageBody = 'Cherry-pick:No'
          expect(hasCherrypickNo(commitMessageBody)).to.be.true
        })
      })
    })
  })
})
