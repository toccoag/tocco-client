import {homedir} from 'os'

import logger from '../../build/lib/logger'

import {exec, execute} from './fileSystem'
import {getMandatoryEnvVariable, getNpmTag, transformPackageName} from './helpers'

/**
 * Setup gitlab deploy private key and public ssh key
 */
export const setupGitlab = async () => {
  const publicKey = getMandatoryEnvVariable('GITLAB_PUBLIC_KEY')
  const currentBranch = getMandatoryEnvVariable('CI_COMMIT_REF_NAME')

  logger.log(`Checkout branch '${currentBranch}'`)
  await execute('git checkout $CI_COMMIT_REF_NAME')

  // add ssh origin (and not https which is used by Gitlab itself)
  await execute('git remote remove origin')
  await execute('git remote add origin git@gitlab.com:toccoag/tocco-client.git')

  await checkHasSshKey()
  await addGitlabAsKnownHost(publicKey)

  // override local tags (if they are cached on gitlab) and do not fetch branches
  await execute('git fetch origin "refs/tags/*:refs/tags/*" --force --quiet')
}

/**
 * Configures git user Tocco Admin
 */
export const setupGitUser = async (directory = undefined) => {
  await execute('git config user.email "admin@tocco.ch"', directory)
  await execute('git config user.name "Tocco Admin"', directory)
}

/**
 * Gets git release tag for a package
 */
export const getGitReleaseTag = packageName => `${transformPackageName(packageName)}@${getNpmTag()}`

/**
 * Returns true if a git tag with a specific name exist on remote
 */
export const existsGitTag = async tagName => (await execute(`git ls-remote origin "${tagName}"`)) !== ''

/**
 * Returns list of git commits between git tag and head
 */
export const getGitLogSinceTag = async gitTag => {
  const splitter = '---COMMIT END---'
  try {
    const {stdout, stderr} = await exec(`git log --pretty=tformat:"%s%n%b${splitter}" "${gitTag}"..HEAD --reverse`)

    if (stderr) {
      logger.error(stderr)
      process.exit(1)
    }

    return stdout
      .split(splitter)
      .map(s => s.trim())
      .filter(s => s) // filter out empty elements
  } catch (e) {
    logger.error(e)
    process.exit(1)
  }
}

/**
 * Call hasSshKey and show an error message on how to add the missing ssh key
 */
export const checkHasSshKey = async () => {
  // the ssh agent cannot be started and the ssh key cannot be added inside this script as each "execute"
  // call is done in an own sub process which is similar to open a new terminal for each command.
  // So the "state" is not shared between commands
  if (!(await hasSshKey())) {
    const message = `Add the following shell commands to the before_script of the Gitlab job:
    - eval $(ssh-agent -s)
    - ssh-add <(echo "\${GITLAB_DEPLOY_PRIVATE_KEY}" | base64 -d)`
    logger.error(message)
    process.exit(1)
  }
}

/**
 * Returns true if a git tag with a specific name exist
 */
const hasSshKey = async () => (await execute('ssh-add -l')) !== ''

/**
 * Add gitlab.com to ~/.ssh/known_hosts
 */
export const addGitlabAsKnownHost = async publicKey => {
  await execute(`mkdir -p ${homedir}/.ssh`)
  await execute(`echo "gitlab.com ssh-rsa ${publicKey}" >>${homedir}/.ssh/known_hosts`)
}
