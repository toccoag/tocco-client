import {exec as execChildProcess, spawn as spawnChildProcess} from 'child_process'
import {readFileSync, writeFile} from 'fs'
import * as util from 'util'

import logger from '../../build/lib/logger'

/**
 * Spawns command i a separate process. Optionally the destination where the command should be spawned can be specified.
 * Outputs will be printed to the console.
 */
export const spawn = (command, directory = undefined) =>
  new Promise((resolve, reject) => {
    const process = spawnChildProcess(command, {cwd: directory, stdio: 'inherit', shell: true})
    process.on('error', error => {
      reject(error)
    })

    process.on('close', code => {
      logger.log(`child process exited with code ${code}`)
      resolve(code)
    })
  })

/**
 * "exec" function of child_process as promise
 */
export const exec = util.promisify(execChildProcess)

/**
 * Executes a command. Optionally the destination where the command should be executed can be specified and
 * if an output in stderr should not exit the program
 */
export const execute = async (command, directory = undefined) => {
  try {
    const {stdout, stderr} = await exec(command, {cwd: directory})

    if (stderr) {
      logger.log(stderr)
    }

    return stdout?.trim()
  } catch (e) {
    logger.error(e)
    if (e.stdout) {
      logger.error(e.stdout)
    }
    if (e.stderr) {
      logger.error(e.stderr)
    }
    process.exit(1)
  }
}

/**
 * Prepends content to file
 */
export const prependFile = (filePath, newContent) => {
  const fileBuffer = readFileSync(filePath)
  const text = fileBuffer.toString()

  writeFile(filePath, `${newContent}\n${text}`, error => {
    if (error) {
      logger.error(error)
      process.exit(1)
    }
  })
}
