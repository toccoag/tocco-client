import * as packages from '../../build/lib/packages'

import * as gitHelpers from './git'

/**
 * Mock devDependenciesOfPackage and packageExists for building a dependency tree.
 *
 * Dependency tree:
 *
 * - devcon
 *   - app-extensions
 *     - tocco-test-util
 *       - tocco-theme
 *     - tocco-theme
 *     - tocco-ui
 *       - tocco-theme
 *       - tocco-util
 *         -non-tocco-dev-dependency
 *     - tocco-util
 *       -non-tocco-dev-dependency
 *   - tocco-ui
 *     - tocco-theme
 *     - tocco-util
 *       -non-tocco-dev-dependency
 *   - tocco-util
 *     -non-tocco-dev-dependency
 */
export const mockDependencies = (packageVersion = undefined) => {
  const dependencyMap = {
    'app-extensions': ['tocco-test-util', 'tocco-theme', 'tocco-ui', 'tocco-util'],
    devcon: ['tocco-app-extensions', 'tocco-ui', 'tocco-util'],
    'tocco-ui': ['tocco-theme', 'tocco-util'],
    'tocco-util': [],
    'tocco-test-util': ['tocco-theme'],
    'tocco-theme': []
  }
  const devDependenciesOfPackage = packageName => ({
    version: packageVersion,
    devDependencies: dependencyMap[packageName]?.reduce((acc, name) => ({...acc, [name]: '*'}), {})
  })
  jest.spyOn(packages, 'getPackageJson').mockImplementation(devDependenciesOfPackage)

  const packageExists = packageName =>
    ['tocco-test-util', 'tocco-theme', 'tocco-ui', 'tocco-util'].includes(packageName)
  jest.spyOn(packages, 'packageExists').mockImplementation(packageExists)
}

/**
 * Mock the method readJsonFile of the packages helper for the file variables.json
 */
export const mockGetVariables = variables => {
  jest.spyOn(packages, 'readJsonFile').mockImplementation(() => variables)
}

/**
 * Mock the method readFile of the packages helper
 */
export const mockReadFile = content => {
  jest.spyOn(packages, 'readFile').mockImplementation(() => content)
}

/**
 * Mocks the method getGitLogSinceTag of the git helper
 */
export const mockGetGitLogSinceTag = commits => {
  jest.spyOn(gitHelpers, 'getGitLogSinceTag').mockImplementation(() => commits)
}

/**
 * Mocks the method existsGitTag of the git helper
 */
export const mockExistsGitTag = gitTagExists => {
  jest.spyOn(gitHelpers, 'existsGitTag').mockImplementation(() => gitTagExists)
}
