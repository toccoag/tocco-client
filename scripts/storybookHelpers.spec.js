import {getStorybookLink, getStorybookFolderName, PREFIX} from './storybookHelpers'

describe('scripts', () => {
  describe('storybookHelpers', () => {
    describe('getStorybookLink', () => {
      test('no slash', () => {
        expect(getStorybookLink('new-feature')).to.eql(`${PREFIX}new-feature`)
      })

      test('one slash', () => {
        expect(getStorybookLink('pr/new-feature')).to.eql(`${PREFIX}pr_new-feature`)
      })

      test('two slashes', () => {
        expect(getStorybookLink('pr/38/fix')).to.eql(`${PREFIX}pr_38_fix`)
      })
    })

    describe('getStorybookFolderName', () => {
      test('no slash', () => {
        expect(getStorybookFolderName('new-feature')).to.eql('new-feature')
      })

      test('one slash', () => {
        expect(getStorybookFolderName('pr/new-feature')).to.eql('pr_new-feature')
      })

      test('two slashes', () => {
        expect(getStorybookFolderName('pr/38/fix')).to.eql('pr_38_fix')
      })
    })
  })
})
