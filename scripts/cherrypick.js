/**
 * Script looks for commits with a "Cherry-pick: Up" in the commit message body on the source branch
 * and cherry pick them to the target branch (master or next release branch)
 *
 * Naming:
 * - source branch: branch on which the CI job is running (e.g. nice-releases/227)
 * - target branch: branch to which the commits should be cherry-picked (next release branch or master;
 * e.g. nice-releases/228)
 * - auto merge tag: each release branch has a git tag like auto-merge/${VERSION}. All commits before this tag
 * (including the commit itself) are already cherry-picked (if necessary)
 *
 * Workflow:
 * - Setup git for pushing commits
 * - Checkout source branch
 * - Get commit id which belongs to the auto merge tag of the source branch (e.g. tag auto-merge/227)
 * - Retrieve commits which should be cherry-picked. Such a commit has a Cherry-pick:Up in the commit message body
 * and are after the auto merge tag in the history (=not already cherry-picked)
 * - Stop if nothing should be cherry-picked
 * - Checkout target branch
 * - Cherry pick commit by commit (start by oldest commit)
 * - Create and push new branch cherry-picking/_TARGET-BRANCH_-auto-xxxxxxxx if at least one commit is cherry-picked
 * - Move auto merge tag to head of source branch
 *
 * Mandatory environment variables:
 * - GITLAB_DEPLOY_PRIVATE_KEY: used for the git interaction
 * - GITLAB_PUBLIC_KEY: used for the git interaction
 * - CI_COMMIT_REF_NAME: source branch for which the job is executed (always passed by Gitlab)
 */
import logger from '../build/lib/logger'
import {readJsonFile} from '../build/lib/packages'

import {execute, getNiceVersionWithoutPatchAndDots, hasCherrypickUp, setupGitlab, setupGitUser} from './helpers'

/**
 * Get commit id which belongs to the tag and check if tag is on source branch (else throw an error)
 */
const getTagCommitId = async (tagName, sourceBranch) => {
  const tagCommit = await execute(`git log -1 --format=%H ${tagName}`)
  logger.log(`${tagCommit} is commit id of tag ${tagName}`)

  // format argument is used to remove asterisk before current branch name
  const commitExistsOnBranches = (await execute(`git branch --contains ${tagName} --format='%(refname:short)'`)).split(
    '\n'
  )

  if (!commitExistsOnBranches.includes(sourceBranch)) {
    logger.error(`Commit id ${tagCommit} is not valid because it does not exist in source branch ${sourceBranch}`)
    process.exit(1)
  }

  return tagCommit
}

/**
 * Move tag to head of release branch and push tag
 */
const moveTag = async (tagName, sourceBranch) => {
  await execute(`git checkout ${sourceBranch}`)
  await execute(`git tag -f ${tagName}`)
  await execute(`git push -f origin ${tagName}`)
  logger.info(`Move tag ${tagName} to head of branch ${sourceBranch}`)
}

/**
 * Check commit by commit if it should be cherry-picked. Stops if auto-merge tag is reached
 * (as a previous job already checked these commits). If no commit is found to cherry-pick
 * then stop the script. Returns a list of commit ids which should be cherry-picked
 * where the oldest commit is the first element in the list.
 */
const retrieveCommitsToCherryPick = async (sourceBranch, tagName, tagCommit) => {
  const commitsToCherryPick = []
  // get all commit ids of the source branch
  const commits = (await execute(`git rev-list ${sourceBranch}`)).split('\n')

  for (const commit of commits) {
    // stop by commit of tag because older commits are already checked
    if (commit === tagCommit) {
      break
    }

    const commitMessageBody = await execute(`git log -1 --pretty=%b ${commit}`)
    if (hasCherrypickUp(commitMessageBody)) {
      logger.log(`${commit} has 'Cherry-pick: Up' in commit message`)
      commitsToCherryPick.push(commit)
    } else {
      logger.log(`${commit} has no 'Cherry-pick: Up' in commit message`)
    }
  }

  if (commitsToCherryPick.length === 0) {
    logger.info('Nothing to cherry pick')
    await moveTag(tagName, sourceBranch)
    process.exit(0)
  }

  // reserve order that oldest commit is picked first
  const commitsToCherryPickReversed = commitsToCherryPick.toReversed()
  logger.info(`Order of commits to cherry-pick:\n${commitsToCherryPickReversed.map(c => `- ${c}`).join('\n')}`)

  return commitsToCherryPickReversed
}

/**
 * Check if last commit is empty (same tree hash) then remove it
 */
const removeLastCommitIfEmpty = async () => {
  const currentCommitTreeHeash = await execute('git rev-parse HEAD^{tree}')
  const previousCommitTreeHeash = await execute('git rev-parse HEAD~1^{tree}')
  if (currentCommitTreeHeash === previousCommitTreeHeash) {
    logger.log('Last commit is removed because it an empty commit')
    await execute('git reset --hard HEAD~1')
  }
}

/**
 * Get commit id of current commit
 */
const getCurrentCommitId = async () => await execute('git rev-parse HEAD')

const main = async () => {
  const niceVersion = getNiceVersionWithoutPatchAndDots()

  const tagName = `auto-merge/${niceVersion}`
  const sourceBranch = `nice-releases/${niceVersion}`
  const targetBranch = readJsonFile('./scripts/variables.json').targetBranch

  await setupGitlab()
  await setupGitUser()

  const tagCommit = await getTagCommitId(tagName, sourceBranch)
  const commitsToCherryPick = await retrieveCommitsToCherryPick(sourceBranch, tagName, tagCommit)

  await execute(`git fetch origin ${targetBranch}`)
  await execute(`git checkout ${targetBranch}`)

  const commitIdBefore = await getCurrentCommitId()

  for (const commit of commitsToCherryPick) {
    logger.info(`Cherry pick ${commit}`)
    /*
     * if a commit is cherry picked but nothing is changed on the target branch --keep-redundant-commits is required
     * that the cherry-picking does not fail. afterwards with the method removeLastCommitIfEmpty this empty commit
     * is removed. for example a commit from the master is manually picked to nice-releases/229
     * (and Cherry-pick: Up is added). first the commit is picked to nice-releases/30. next the script tries to pick
     * the commit to master but there the change already exists (and the script should not fail).
     */
    await execute(`git cherry-pick ${commit} --keep-redundant-commits -x`)
    const hasUncommittedChanges = (await execute(`git status -s`)).length > 0
    if (hasUncommittedChanges) {
      logger.error(`Cherry picking '${commit}' failed and must be resolved manually`)
      process.exit(1)
    }
    await removeLastCommitIfEmpty()
  }

  const commitIdAfter = await getCurrentCommitId()

  if (commitIdBefore === commitIdAfter) {
    logger.info('Marked commits already picked')
  } else {
    const newBranch = `cherry-picking/_${targetBranch}_auto-${Date.now()}`
    await execute(`git checkout -b ${newBranch}`)
    await execute(`git push origin ${newBranch}`)
    logger.info(`Cherry-picked commits pushed to remote branch ${newBranch}`)
  }

  await moveTag(tagName, sourceBranch)
}

main()
