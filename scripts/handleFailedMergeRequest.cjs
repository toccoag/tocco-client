/* eslint-disable max-len, no-console */
/**
 * Script sends a slack message when opening a merge request has failed.
 * This script has to run without any pre-installed node_modules.
 * Therefore we cannot use `babel-node`. This file is in `CommonJs` until we migrate our codebase to ESM.
 * Because the other scripts run in ESM with babel-node, these imports are not available.
 */

/**
 * COPIED - Extract target branch from source branch
 */
const getTargetBranch = branch => {
  // the source branch has the pattern `cherry-picking/_${SOURCE_BRANCH}_auto-${TIMESTAMP}`
  // steps:
  // - remove prefix `cherry-picking/_`
  // - split by `_auto-` and target branch is first element
  return branch.replace('cherry-picking/_', '').split('_auto-', 2)[0]
}

/**
 * COPIED - Sends slack message with webhook
 */
const sendSlackMessage = async body => {
  const options = {
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      Authorization: `Bearer ${process.env.SLACK_AUTH_TOKEN}`
    },
    method: 'POST',
    body: JSON.stringify(body)
  }

  await fetch('https://slack.com/api/chat.postMessage', options)
}

const pipelineUrl = process.env.CI_PIPELINE_URL
const sourceBranch = process.env.CI_COMMIT_REF_NAME
const gitlabProjectPath = process.env.CI_PROJECT_PATH
const gitlabProjectId = process.env.CI_PROJECT_NAME

const sendFailedMessage = async () => {
  try {
    const targetBranch = getTargetBranch(sourceBranch)

    const baseGitlabUrl = `https://gitlab.com/${gitlabProjectPath}`
    const timestamp = Math.floor(Date.now() / 1000)
    const body = {
      channel: 'C034NTD15PU', // channel id of #ci
      attachments: [
        {
          color: '#951e13',
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `*Failed cherry-pick merge request*\nCreating a <${pipelineUrl}|cherry-pick merge request> has failed.`
              },
              fields: [
                {
                  type: 'mrkdwn',
                  text: `*Branch*\n<${baseGitlabUrl}/-/tree/${sourceBranch}|${sourceBranch}>`
                },
                {
                  type: 'mrkdwn',
                  text: `*Target branch*\n<${baseGitlabUrl}/-/tree/${targetBranch}|${targetBranch}>`
                }
              ],
              accessory: {
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'Create merge request',
                  emoji: false
                },
                url: `${baseGitlabUrl}/-/merge_requests/new?merge_request%5Bsource_branch%5D=${encodeURIComponent(
                  sourceBranch
                )}&merge_request%5Btarget_branch%5D=${encodeURIComponent(targetBranch)}`,
                action_id: 'create-merge-request'
              }
            },
            {
              type: 'context',
              elements: [
                {
                  type: 'mrkdwn',
                  text: `${gitlabProjectId} | <!date^${timestamp}^{date_short_pretty} at {time}|${new Date().toLocaleString()}>`
                }
              ]
            }
          ]
        }
      ]
    }

    await sendSlackMessage(body)
  } catch (error) {
    console.warn('Could not send failure message to Slack')
  }
}

const main = async () => {
  sendFailedMessage()
}

main()
