/**
 * Build storybook for a merge request
 *
 * Execution:
 * node ./storybookMergeRequest
 *
 * Workflow:
 * - build storybook for merge request branch
 * - deploy builded storybook to storybook repo
 * - load current description for merge request
 * - add storybook link to description if it not already exist
 *
 * Mandatory environment variables:
 * - CI_COMMIT_REF_NAME: target branch for which the job is executed (always passed by Gitlab)
 * - GITLAB_DEPLOY_PRIVATE_KEY: used for the git interaction
 * - GITLAB_PUBLIC_KEY: used for the git interaction
 * - CI_PROJECT_ID: project id for which the job is executed (always passed by Gitlab)
 * - CI_MERGE_REQUEST_IID: merge request id for which the job is executed (always passed by Gitlab)
 * - CI_MERGE_REQUEST_SOURCE_BRANCH_NAME: source branch of the merge request (always passed by Gitlab)
 * - GITLAB_ACCESS_TOKEN: Gitlab api key to interact with the rest api
 */

import fetch from 'isomorphic-fetch'

import logger from '../build/lib/logger'
import {readJsonFile} from '../build/lib/packages'

import {getMandatoryEnvVariable} from './helpers'
import {buildStorybook, deployStorybook, getStorybookLink} from './storybookHelpers'

const PROJECT_ID = process.env.CI_PROJECT_ID
const MERGE_REQUEST_ID = process.env.CI_MERGE_REQUEST_IID
const BRANCH_NAME = process.env.CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
const GITLAB_ACCESS_TOKEN = process.env.GITLAB_ACCESS_TOKEN

const url = `https://gitlab.com/api/v4/projects/${PROJECT_ID}/merge_requests/${MERGE_REQUEST_ID}`
const requestOptions = {
  headers: {
    'Content-Type': 'application/json',
    'PRIVATE-TOKEN': GITLAB_ACCESS_TOKEN
  }
}

const getStorybookText = () => `\n\nStorybook: ${getStorybookLink(BRANCH_NAME)}`

/**
 * Load merge request object via rest api
 */
const getMergeRequest = async () => {
  const options = {
    ...requestOptions,
    method: 'GET'
  }

  const response = await fetch(url, options)
  const statusCode = response.status
  const data = response.json()
  if (statusCode === 401) {
    throw new Error('Unauthorized')
  } else if (statusCode >= 400) {
    throw new Error(`Could not fetch merge request. Status code: ${statusCode}`)
  }

  return data
}

/**
 * Update description of the merge request
 */
const updateDescription = async description => {
  const body = {
    description
  }

  const options = {
    ...requestOptions,
    method: 'PUT',
    body: JSON.stringify(body)
  }

  const response = await fetch(url, options)
  const statusCode = response.status
  if (statusCode >= 400) {
    throw new Error(`Could not set description. Status code: ${statusCode}`)
  }
}

/**
 * Add storybook url to merge request description
 */
const linkStorybook = async () => {
  const mergeRequest = await getMergeRequest()
  const description = mergeRequest.description || ''
  const storybookText = getStorybookText()

  if (description.includes(storybookText)) {
    logger.info('Storybook link already exists in merge request description')
  } else {
    await updateDescription(description + storybookText)
    logger.info('Storybook link added to merge request description')
  }
}

const main = async () => {
  const backendUrl = readJsonFile('./scripts/variables.json').testSystemUrl
  const branchName = getMandatoryEnvVariable('CI_COMMIT_REF_NAME')
  await buildStorybook(backendUrl)
  await deployStorybook(branchName)
  await linkStorybook()
}

main()
