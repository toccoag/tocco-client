import logger from '../build/lib/logger'
import {packageExists, readJsonFile} from '../build/lib/packages'

import {hasCherrypickUp, hasCherrypickNo} from './helpers'

/**
 * Pattern: <type>(<scope>): <subject>
 * Matching groups:
 * type: match[1]
 * (scope): match[2]
 * subject: match[3]
 */
const ALLOWED_PATTERN = /^(\w+)(\(.+\))?: (.+)$/
const ALLOWED_TYPES = ['feat', 'fix', 'docs', 'style', 'refactor', 'test', 'chore']
const ALLOWED_MAX_LENGTH = 70

/**
 * Validate commit message with the following rules:
 * - first line max length
 * - pattern of first line
 * - valid type is used in first line
 * - if a scope is defined a valid package name must be used
 * - second line is empty
 * - if forced via variables.json "Cherry-pick: Up/No" is required
 */
export const isValidCommitMessage = message => {
  const lines = message.split('\n')
  const firstLine = lines[0]
  const secondLine = lines[1] || ''
  const body = lines.slice(2).join('\n')

  return isFirstLineValid(firstLine) && isSecondLineValid(secondLine) && isBodyValid(body)
}

const isFirstLineValid = line => {
  if (line.length > ALLOWED_MAX_LENGTH) {
    logger.error(`The first line of the commit message is longer than ${ALLOWED_MAX_LENGTH} characters!`)
    return false
  }

  const match = ALLOWED_PATTERN.exec(line)

  if (!match) {
    logger.error(`The commit message does not match pattern "<type>(<scope>): <subject>" but it was "${line}"!`)
    return false
  }

  const type = match[1]
  const scope = (match[2] || '').replace('(', '').replace(')', '')

  if (!ALLOWED_TYPES.includes(type)) {
    logger.error(`In the commit message "${type}" is not an allowed type!`)
    return false
  }

  // the tocco-client scope is used to release all packages with a non-empty changelog
  if (scope && !packageExists(scope) && scope !== 'tocco-client') {
    logger.error(`In the commit message "${scope}" is not an allowed scope!`)
    return false
  }

  return true
}

const isSecondLineValid = line => {
  if (line !== '') {
    logger.error('Second line of the commit message has to be an empty line!')
    return false
  }
  return true
}

const isBodyValid = body => {
  const requireCherryPick = readJsonFile('./scripts/variables.json').requireCherryPickInCommitMessage
  const hasCherryPick = hasCherrypickUp(body) || hasCherrypickNo(body)
  if (requireCherryPick && !hasCherryPick) {
    logger.error('The body of the commit message must contain Cherry-pick: Up/No!')
    return false
  }
  return true
}
