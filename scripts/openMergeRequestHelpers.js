/**
 * Extract target branch from source branch
 */
export const getTargetBranch = sourceBranch => {
  // the source branch has the pattern `cherry-picking/_${SOURCE_BRANCH}_auto-${TIMESTAMP}`
  // steps:
  // - remove prefix `cherry-picking/_`
  // - split by `_auto-` and target branch is first element
  return sourceBranch.replace('cherry-picking/_', '').split('_auto-', 2)[0]
}
