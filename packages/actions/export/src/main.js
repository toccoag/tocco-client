import PropTypes from 'prop-types'
import {appFactory, formData, templateValues, selection, externalEvents} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil} from 'tocco-util'

import Export from './components/ExportContainer'
import reducers, {sagas} from './modules/reducers'

const packageName = 'export'

const EXTERNAL_EVENTS = ['onSuccess']

const initApp = (id, input, events, publicPath) => {
  const content = <Export />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({listApp: EntityListApp}))
  templateValues.addToStore(store)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['actions', 'entities', 'entity-list', 'client', 'component', 'common', 'rest', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const ExportApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

ExportApp.propTypes = {
  selection: selection.propType.isRequired,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}

export default ExportApp
