import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {appFactory, rest, form, formData, display, externalEvents, notification, login} from 'tocco-app-extensions'
import {sagasUtils} from 'tocco-sso-login/src/main'
import {api, env, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/RegisterForm'
import {getDispatchActions} from '../../input'
import {Pages} from '../../utils/constants'
import {getOptionalModules, getRequiredModules, mapModulesToBean, mapModulesToPseudoField} from '../../utils/modules'

import * as actions from './actions'

export const eventRegistrationActionSelector = state => state.eventRegistrationAction
export const textResourceSelector = (state, key) => state.intl.messages[key]
export const loginSelector = state => state.login
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([
    takeLatest(actions.INIT, init),
    takeLatest(appFactory.INPUT_CHANGED, inputChanged),
    takeLatest(actions.LOAD_FORM_INFO, loadFormInfo),
    takeLatest(actions.REGISTER, register),
    takeLatest(actions.CHECK_SSO_AVAILABLE, checkSsoAvailable)
  ])
}

export function* init() {
  yield put(login.doSessionCheck())
  yield take(login.SET_LOGGED_IN)

  const {loggedIn} = yield select(loginSelector)
  const widgetConfig = yield call(loadWidgetConfig)

  if (loggedIn === true || !widgetConfig.config.showAuthentication) {
    yield put(actions.setPage(Pages.RegisterView))
  } else {
    yield put(actions.setPage(Pages.Login))
  }
}

export function* loadWidgetConfig() {
  const widgetConfigKey = env.getWidgetConfigKey()
  const resource = `widget/configs/${widgetConfigKey}`
  const options = {
    method: 'GET'
  }

  const response = yield call(rest.requestSaga, resource, options)
  return response.body
}

export function* inputChanged({payload}) {
  const {input} = payload

  const derivedActions = getDispatchActions(input)
  yield all(derivedActions.map(action => put(action)))
}

export function* loadFormInfo() {
  const formInfo = yield call(loadFormDefinition)
  yield call(loadCurrentUser, formInfo)
  yield put(actions.setLoaded(true))
}

export function* loadFormDefinition() {
  const {eventKey} = yield select(eventRegistrationActionSelector)
  const {
    body: {formName, formScope, eventFormName, submitButtonName}
  } = yield call(rest.requestSaga, `widgets/eventRegistration/${eventKey}/registrationForm`, {
    method: 'GET'
  })
  const formDefinition = yield call(fetchForm, formName, formScope, eventKey)

  const modules = yield call(loadModules, eventKey)

  const modulesPseudoFormFields = yield call(getModulesPseudoFormFields, modules)
  const questionsPseudoFormFields = yield call(getQuestionsPseudoFormFields, eventKey)

  let newFormDefinition = form.replaceBoxChildren(
    formDefinition,
    'never_hide_event_question_box',
    questionsPseudoFormFields
  )
  newFormDefinition = form.replaceBoxChildren(newFormDefinition, 'never_hide_modules_box', modulesPseudoFormFields)

  const fieldDefinitions = yield call(form.getFieldDefinitions, newFormDefinition)

  yield all([
    put(actions.setFormDefinition(newFormDefinition)),
    put(formData.setTermsCurrentEntity('Event', eventKey)),
    put(actions.setEventFormName(eventFormName)),
    put(actions.setFormScope(formScope)),
    put(actions.setFieldDefinitions(fieldDefinitions)),
    put(actions.setSubmitButtonName(submitButtonName))
  ])
  return {formDefinition: newFormDefinition, fieldDefinitions, formName, formScope}
}

export function* fetchForm(formName, formScope, id) {
  const formDefinition = yield call(rest.fetchForm, formName, formScope)
  return {
    ...formDefinition,
    createEndpoint: `widgets/eventRegistration/${id}/validate`,
    updateEndpoint: `widgets/eventRegistration/${id}/validate`
  }
}

export function* getQuestionsPseudoFormFields(eventKey) {
  const response = yield call(rest.requestSaga, `/widgets/eventRegistration/${eventKey}/questions`)

  const questions = response.body.questions
  yield put(actions.setQuestionFieldMapping(form.getQuestionFieldMapping(questions)))
  const questionsPseudoFields = questions.map(q => form.mapQuestionToPseudoField(q))
  yield put(actions.addPseudoEntityPaths(form.getMergedEntityPaths(questionsPseudoFields)))
  return questionsPseudoFields.map(f => f.formField)
}

export function* loadModuleValidation() {
  const {eventKey} = yield select(eventRegistrationActionSelector)
  const paths = ['maximal_optional_modules', 'minimal_optional_modules']
  const eventEntity = yield call(rest.fetchEntity, 'Event', eventKey, {paths})

  const minOptionalModules = eventEntity.paths.minimal_optional_modules.value
  const maxOptionalModules = eventEntity.paths.maximal_optional_modules.value

  return {
    minOptionalModules,
    maxOptionalModules
  }
}

export function* loadModules(eventKey) {
  const resource = `/widgets/eventRegistration/${eventKey}/modules`
  const response = yield call(rest.requestSaga, resource)
  return response.body.modules
}

const getModuleLimit = (limit, modules) => {
  if (limit) {
    const reduction = modules.filter(
      ({additionalInformation: {promotion_status_id: promotionId}}) => promotionId === 'passed'
    ).length
    const adjustedLimit = limit - reduction
    return adjustedLimit > 0 ? adjustedLimit : undefined
  } else {
    return limit
  }
}

export function* getModulesPseudoFormFields(modules) {
  const optionalModuleValidation = yield call(loadModuleValidation)

  const requiredModules = getRequiredModules(modules)
  const minRequiredModules = requiredModules.filter(m => !m.editable && !m.checkboxHidden).length
  const optionalModules = getOptionalModules(modules)

  const requiredLabel = yield select(textResourceSelector, 'client.actions.event-registration-action.modules.required')
  const optionalLabel = yield select(textResourceSelector, 'client.actions.event-registration-action.modules.optional')

  const modulesPseudoFields = [
    ...(requiredModules.length > 0
      ? [mapModulesToPseudoField(requiredLabel, requiredModules, true, minRequiredModules, requiredModules.length)]
      : []),
    ...(optionalModules.length > 0
      ? [
          mapModulesToPseudoField(
            optionalLabel,
            optionalModules,
            false,
            getModuleLimit(optionalModuleValidation.minOptionalModules, optionalModules),
            getModuleLimit(optionalModuleValidation.maxOptionalModules, optionalModules)
          )
        ]
      : [])
  ]
  yield put(actions.addPseudoEntityPaths(form.getMergedEntityPaths(modulesPseudoFields)))
  yield put(actions.setRequiredModulesForbidden(requiredModules.some(m => m.forbidden || !(m.editable || m.checked))))
  return modulesPseudoFields.map(f => f.formField)
}

export function* loadCurrentUser(formInfo) {
  const users = yield call(rest.fetchEntitiesPage, 'User', {where: 'pk == :currentUser'})

  const {fieldDefinitions} = formInfo
  if (users.length === 1) {
    const paths = yield call(form.getUsedPaths, fieldDefinitions, field => field.pseudoField !== true)
    const user = yield call(rest.fetchEntity, 'User', users[0].key, {paths}, {useSelectors: true})

    const {pseudoEntityPaths} = yield select(eventRegistrationActionSelector)
    user.paths = {
      ...user.paths,
      ...pseudoEntityPaths
    }

    yield all([put(actions.setCurrentUser(user)), call(setupForm, user, formInfo)])
  } else {
    yield call(setupFormForNewUser, fieldDefinitions)
  }
}

export function* setupForm(user, {fieldDefinitions, formName, formScope}) {
  const flattenEntity = yield call(api.getFlattenEntity, user)

  yield call(display.enhanceEntityWithDisplayExpressions, flattenEntity, formName, fieldDefinitions, formScope)
  yield call(display.enhanceEntityWithDisplays, flattenEntity, fieldDefinitions)

  const formValues = yield call(form.entityToFormValues, flattenEntity, fieldDefinitions)

  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* setupFormForNewUser(fieldDefinitions) {
  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const {pseudoEntityPaths, newUserEmail} = yield select(eventRegistrationActionSelector)
  const entityValues = yield call(api.getFlattenEntity, {model: 'User', paths: {...pseudoEntityPaths}})
  const formValues = yield call(
    form.entityToFormValues,
    {...entityValues, ...defaultValues, ...(newUserEmail ? {email: newUserEmail} : {})},
    fieldDefinitions
  )
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* register() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const bean = yield call(loadRegisterBean)
  const body = {
    ...bean
  }

  const {eventKey} = yield select(eventRegistrationActionSelector)
  const response = yield call(rest.requestSaga, `widgets/eventRegistration/${eventKey}/register`, {
    method: 'POST',
    body,
    acceptedErrorCodes: ['VALIDATION_FAILED']
  })
  if (response.status === 201) {
    const location = response.headers.get('location')
    const userKey = location.substring(location.lastIndexOf('/') + 1)
    yield put(actions.setUserKey(userKey))
    yield call(handleSuccess)
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    if (response.body.errorCode === 'VALIDATION_FAILED') {
      yield put(
        notification.toaster({
          type: 'error',
          title: 'client.component.actions.validationError',
          body: validation.getErrorCompact(response.body.errors)
        })
      )
    } else {
      yield put(externalEvents.fireExternalEvent('onError'))
    }
  }
}

export function* loadRegisterBean() {
  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {fieldDefinitions, questionFieldMapping, eventKey} = yield select(eventRegistrationActionSelector)
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const splittedFlatEntity = form.splitFlattenEntity(flatEntity, fieldDefinitions)

  const user = yield call(api.toEntity, splittedFlatEntity.entity)
  const modules = mapModulesToBean(splittedFlatEntity.pseudoFields)
  const questions = form.mapQuestionsToBean(
    questionFieldMapping,
    [eventKey, ...modules.map(m => m.key)],
    splittedFlatEntity.pseudoFields
  )

  return {
    user,
    questions,
    modules
  }
}

export function* checkSsoAvailable() {
  const ssoAvailable = yield call(sagasUtils.checkSsoAvailable)
  yield put(actions.setSsoAvailable(ssoAvailable))
}

export function* handleSuccess() {
  const isPaymentFlow = yield call(checkPaymentModel)
  if (!isPaymentFlow) {
    yield put(actions.setPage(Pages.RegistrationResult))
    return
  }

  const orderUuid = yield call(loadOrderUuid)
  if (orderUuid) {
    const {navigationStrategy} = yield select(inputSelector)
    yield call(navigationStrategy.navigateToActionRelative, {
      appId: 'payment-provider-action',
      queryParams: {orderUuid}
    })
  } else {
    yield put(actions.setPage(Pages.RegistrationResult))
  }
}

export function* checkPaymentModel() {
  const modules = yield call(rest.fetchModules)
  return modules.includes('nice.optional.eventonlinepayment')
}

export function* loadOrderUuid() {
  const {eventKey, userKey} = yield select(eventRegistrationActionSelector)
  const options = {
    acceptedStatusCodes: [404]
  }
  const response = yield call(rest.requestSaga, `/widgets/eventRegistration/${eventKey}/order/${userKey}/uuid`, options)

  if (response.status === 200) {
    return response.body.uuid
  } else {
    return null
  }
}
