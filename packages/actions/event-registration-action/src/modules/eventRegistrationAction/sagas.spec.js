import {actions as formActions} from 'redux-form'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, form, formData, display, externalEvents, appFactory, notification, login} from 'tocco-app-extensions'
import {api, env} from 'tocco-util'

import {Pages} from '../../utils/constants'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('event-registration-action', () => {
  describe('modules', () => {
    describe('eventRegistrationAction', () => {
      const REDUX_FORM_NAME = 'event-registration-action'
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.INIT, sagas.init),
                takeLatest(appFactory.INPUT_CHANGED, sagas.inputChanged),
                takeLatest(actions.LOAD_FORM_INFO, sagas.loadFormInfo),
                takeLatest(actions.REGISTER, sagas.register),
                takeLatest(actions.CHECK_SSO_AVAILABLE, sagas.checkSsoAvailable)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('init', () => {
          test('should show login page for anonymous user with authentication enabled', () => {
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: false}],
                {
                  take() {}
                },
                [matchers.call(sagas.loadWidgetConfig), {config: {showAuthentication: true}}]
              ])
              .put(login.doSessionCheck())
              .take(login.SET_LOGGED_IN)
              .put(actions.setPage(Pages.Login))
              .run()
          })

          test('should show registration page for logged in user with authentication enabled', () => {
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: true}],
                {
                  take() {}
                },
                [matchers.call(sagas.loadWidgetConfig), {config: {showAuthentication: true}}]
              ])
              .put(login.doSessionCheck())
              .take(login.SET_LOGGED_IN)
              .put(actions.setPage(Pages.RegisterView))
              .run()
          })

          test('should show registration page for anonymous user with authentication disabled', () => {
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: false}],
                {
                  take() {}
                },
                [matchers.call(sagas.loadWidgetConfig), {config: {showAuthentication: false}}]
              ])
              .put(login.doSessionCheck())
              .take(login.SET_LOGGED_IN)
              .put(actions.setPage(Pages.RegisterView))
              .run()
          })

          test('should show registration page for logged in user with authentication disabled', () => {
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: true}],
                {
                  take() {}
                },
                [matchers.call(sagas.loadWidgetConfig), {config: {showAuthentication: false}}]
              ])
              .put(login.doSessionCheck())
              .take(login.SET_LOGGED_IN)
              .put(actions.setPage(Pages.RegisterView))
              .run()
          })
        })

        describe('loadWidgetConfig', () => {
          test('should fetch and return widget config', () => {
            env.setWidgetConfigKey('widget-key')
            const widgetConfig = {config: {showAuthentication: false}}

            return expectSaga(sagas.loadWidgetConfig)
              .provide([
                [matchers.call(rest.requestSaga, 'widget/configs/widget-key', {method: 'GET'}), {body: widgetConfig}]
              ])
              .returns(widgetConfig)
              .run()
          })
        })

        describe('inputChanged', () => {
          test('should set eventKey from selection', () => {
            const eventKey = '123'
            return expectSaga(sagas.inputChanged, {
              payload: {input: {selection: {ids: [eventKey], entityName: 'Event', type: 'ID'}}}
            })
              .put(actions.setEventKey('123'))
              .run()
          })

          test('should set eventKey from eventKey', () => {
            const eventKey = '123'
            return expectSaga(sagas.inputChanged, {
              payload: {input: {eventKey}}
            })
              .put(actions.setEventKey('123'))
              .run()
          })
        })

        describe('loadFormInfo', () => {
          test('should load form and user info', () => {
            const formInfo = {}
            return expectSaga(sagas.loadFormInfo)
              .provide([
                [matchers.call(sagas.loadFormDefinition), formInfo],
                [matchers.call(sagas.loadCurrentUser, formInfo)]
              ])
              .put(actions.setLoaded(true))
              .run()
          })
        })

        describe('loadFormDefinition', () => {
          test('should return form info', () => {
            const eventKey = '123'
            const formName = 'form-name'
            const formScope = 'form-scope'
            const formDefinition = {id: 'form-id', children: []}
            const eventFormName = 'event-form'
            const fieldDefinitions = [{id: 'field-id'}]
            const questionsPseudoFormFields = []
            const modules = []
            const modulesPseudoFormFields = []
            const widgetConfigKey = '456'
            const submitButtonName = 'submitButtonName'

            env.setWidgetConfigKey(widgetConfigKey)

            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {eventKey}],
                [
                  matchers.call(rest.requestSaga, `widgets/eventRegistration/${eventKey}/registrationForm`, {
                    method: 'GET'
                  }),
                  {body: {formName, formScope, eventFormName, submitButtonName}}
                ],
                [matchers.call(sagas.fetchForm, formName, formScope, eventKey), formDefinition],
                [matchers.call(sagas.loadModules, eventKey), modules],
                [matchers.call(sagas.getModulesPseudoFormFields, modules), modulesPseudoFormFields],
                [matchers.call(sagas.getQuestionsPseudoFormFields, eventKey), questionsPseudoFormFields],
                [matchers.call(form.getFieldDefinitions, formDefinition), fieldDefinitions]
              ])
              .put(actions.setFormDefinition(formDefinition))
              .put(formData.setTermsCurrentEntity('Event', eventKey))
              .put(actions.setEventFormName(eventFormName))
              .put(actions.setFormScope(formScope))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .put(actions.setSubmitButtonName(submitButtonName))
              .returns({formDefinition, fieldDefinitions, formName, formScope})
              .run()
          })

          test('should replace placeholder boxes', () => {
            const eventKey = '123'
            const formName = 'form-name'
            const formScope = 'form-scope'
            const formDefinition = {
              id: 'form-id',
              children: [
                {id: 'never_hide_event_question_box', children: []},
                {id: 'never_hide_modules_box', children: []}
              ]
            }
            const questionsPseudoFormFields = [
              {
                id: 'pseudoField_question_1',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    componentType: 'field',
                    dataType: 'string',
                    id: 'pseudoField_question_1',
                    path: 'pseudoField_question_1',
                    pseudoField: true
                  }
                ]
              }
            ]
            const modules = [{key: '380'}]
            const modulesPseudoFormFields = [
              {
                id: 'pseudoField_modules_required',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    componentType: 'field',
                    dataType: 'string',
                    id: 'pseudoField_modules_required',
                    path: 'pseudoField_modules_required',
                    pseudoField: true
                  }
                ]
              }
            ]
            const widgetConfigKey = '456'

            const expectedFieldDefinitions = [
              {
                componentType: 'field',
                dataType: 'string',
                id: 'pseudoField_question_1',
                ignoreCopy: undefined,
                path: 'pseudoField_question_1',
                pseudoField: true,
                readonly: false
              },
              {
                componentType: 'field',
                dataType: 'string',
                id: 'pseudoField_modules_required',
                ignoreCopy: undefined,
                path: 'pseudoField_modules_required',
                pseudoField: true,
                readonly: false
              }
            ]
            const expectedFormDefinition = {
              id: 'form-id',
              children: [
                {id: 'never_hide_event_question_box', children: questionsPseudoFormFields},
                {id: 'never_hide_modules_box', children: modulesPseudoFormFields}
              ]
            }

            const eventFormName = 'Event_registration_event'

            env.setWidgetConfigKey(widgetConfigKey)

            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {eventKey}],
                [matchers.call.fn(rest.requestSaga), {body: {formName, formScope, eventFormName}}],
                [matchers.call(sagas.fetchForm, formName, formScope, eventKey), formDefinition],
                [matchers.call(sagas.loadModules, eventKey), modules],
                [matchers.call(sagas.getModulesPseudoFormFields, modules), modulesPseudoFormFields],
                [matchers.call(sagas.getQuestionsPseudoFormFields, eventKey), questionsPseudoFormFields]
              ])
              .put(actions.setFormDefinition(expectedFormDefinition))
              .put(formData.setTermsCurrentEntity('Event', eventKey))
              .put(actions.setEventFormName(eventFormName))
              .put(actions.setFormScope(formScope))
              .put(actions.setFieldDefinitions(expectedFieldDefinitions))
              .returns({
                formDefinition: expectedFormDefinition,
                fieldDefinitions: expectedFieldDefinitions,
                formName,
                formScope
              })
              .run()
          })
        })

        describe('fetchForm', () => {
          test('should fetch and enhance form', () => {
            const eventKey = '123'
            const formName = 'form-name'
            const formScope = 'form-scope'
            const formDefinition = {id: 'form-id'}
            return expectSaga(sagas.fetchForm, formName, formScope, eventKey)
              .provide([[matchers.call(rest.fetchForm, formName, formScope), formDefinition]])
              .returns({
                id: 'form-id',
                createEndpoint: `widgets/eventRegistration/${eventKey}/validate`,
                updateEndpoint: `widgets/eventRegistration/${eventKey}/validate`
              })
              .run()
          })
        })

        describe('getQuestionsPseudoFormFields', () => {
          test('create pseudo event question fields', () => {
            env.setWidgetConfigKey('456')

            const eventKey = '123'
            const response = {
              body: {
                questions: []
              }
            }
            const questions = []
            const questionFieldMapping = {}
            const questionsPseudoFields = []
            const mergedEntityPaths = {}
            return expectSaga(sagas.getQuestionsPseudoFormFields, eventKey)
              .provide([
                [matchers.call(rest.requestSaga, '/widgets/eventRegistration/123/questions'), response],
                [matchers.call(form.getQuestionFieldMapping, questions), questionFieldMapping],
                [matchers.call(form.getMergedEntityPaths, questionsPseudoFields), mergedEntityPaths]
              ])
              .put(actions.setQuestionFieldMapping(questionFieldMapping))
              .put(actions.addPseudoEntityPaths(mergedEntityPaths))
              .run()
          })
        })

        describe('loadModules', () => {
          test('fetch modules', () => {
            const eventKey = '123'
            const requestUrl = `/widgets/eventRegistration/${eventKey}/modules`
            const response = {
              body: {
                modules: [
                  {
                    key: '345',
                    type: 'required'
                  },
                  {
                    key: '678',
                    type: 'optional'
                  }
                ]
              }
            }

            const expectedModules = [
              {
                key: '345',
                type: 'required'
              },
              {
                key: '678',
                type: 'optional'
              }
            ]

            return expectSaga(sagas.loadModules, eventKey)
              .provide([[matchers.call(rest.requestSaga, requestUrl), response]])
              .returns(expectedModules)
              .run()
          })
        })

        describe('getModulesPseudoFormFields', () => {
          test('create pseudo module fields', () => {
            const modules = [
              {
                key: '345',
                type: 'required',
                display: 'A',
                forbidden: true,
                additionalInformation: {}
              },
              {
                key: '678',
                type: 'optional',
                display: 'B',
                additionalInformation: {}
              },
              {
                key: '123',
                type: 'required',
                display: 'C',
                editable: true,
                additionalInformation: {}
              }
            ]
            const optionalModuleValidation = {minOptionalModules: 1, maxOptionalModules: 3}
            const expectedEntityPaths = {
              pseudoField_modules_required: {
                type: 'choice',
                value: {
                  options: [
                    {
                      id: '345',
                      label: 'A',
                      immutable: true
                    }
                  ]
                },
                writable: true
              },
              pseudoField_modules_optional: {
                type: 'choice',
                value: {
                  options: [
                    {
                      id: '678',
                      label: 'B',
                      immutable: true
                    }
                  ]
                },
                writable: true
              }
            }
            const expectedFormFields = [
              {
                id: 'pseudoField_modules_required',
                label: 'Modules',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_modules_required',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_modules_required',
                    dataType: 'choice',
                    validation: {minSelection: 1, mandatory: true, maxSelection: 2},
                    renderHtmlInOptions: true,
                    selectionType: 'multiple'
                  }
                ]
              },
              {
                id: 'pseudoField_modules_optional',
                label: 'Modules',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_modules_optional',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_modules_optional',
                    dataType: 'choice',
                    validation: {minSelection: 1, mandatory: true, maxSelection: 3},
                    renderHtmlInOptions: true,
                    selectionType: 'multiple'
                  }
                ]
              }
            ]
            return expectSaga(sagas.getModulesPseudoFormFields, modules)
              .provide([
                [matchers.call(sagas.loadModuleValidation), optionalModuleValidation],
                [
                  matchers.select(
                    sagas.textResourceSelector,
                    'client.actions.event-registration-action.modules.required'
                  ),
                  'Modules'
                ],
                [
                  matchers.select(
                    sagas.textResourceSelector,
                    'client.actions.event-registration-action.modules.optional'
                  ),
                  'Modules'
                ]
              ])
              .put.like({
                action: {
                  type: actions.ADD_PSEUDO_ENTITY_PATHS,
                  payload: {pseudoEntityPaths: expectedEntityPaths}
                }
              })
              .put(actions.setRequiredModulesForbidden(true))
              .returns(expectedFormFields)
              .run()
          })

          test('reduce optional module limits for passed modules', () => {
            const modules = [
              {
                key: '678',
                type: 'optional',
                display: 'B',
                additionalInformation: {
                  promotion_status_id: 'passed'
                }
              }
            ]
            const optionalModuleValidation = {minOptionalModules: 1, maxOptionalModules: 3}
            const expectedFormFields = [
              {
                id: 'pseudoField_modules_optional',
                label: 'Modules',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_modules_optional',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_modules_optional',
                    dataType: 'choice',
                    validation: {maxSelection: 2},
                    renderHtmlInOptions: true,
                    selectionType: 'multiple'
                  }
                ]
              }
            ]
            return expectSaga(sagas.getModulesPseudoFormFields, modules)
              .provide([
                [matchers.call(sagas.loadModuleValidation), optionalModuleValidation],
                [
                  matchers.select(
                    sagas.textResourceSelector,
                    'client.actions.event-registration-action.modules.required'
                  ),
                  'Modules'
                ],
                [
                  matchers.select(
                    sagas.textResourceSelector,
                    'client.actions.event-registration-action.modules.optional'
                  ),
                  'Modules'
                ]
              ])
              .returns(expectedFormFields)
              .run()
          })
        })

        describe('loadCurrentUser', () => {
          test('should load logged in user data', () => {
            const fieldDefinitions = [{id: 'field-id'}]
            const paths = ['field-id']
            const formInfo = {fieldDefinitions}
            const pseudoEntityPaths = {
              pseudoField_question_1: {
                value: 'value'
              }
            }
            const user = {
              key: '321'
            }
            const userModified = {
              key: '321',
              paths: {
                pseudoField_question_1: {
                  value: 'value'
                }
              }
            }
            return expectSaga(sagas.loadCurrentUser, formInfo)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {pseudoEntityPaths}],
                [matchers.call(rest.fetchEntitiesPage, 'User', {where: 'pk == :currentUser'}), [user]],
                [matchers.call.fn(form.getUsedPaths), paths],
                [matchers.call(rest.fetchEntity, 'User', user.key, {paths}, {useSelectors: true}), user],
                [matchers.call(sagas.setupForm, user, formInfo)]
              ])
              .put(actions.setCurrentUser(userModified))
              .call(sagas.setupForm, userModified, formInfo)
              .run()
          })

          test('should initialize form for new user', () => {
            const fieldDefinitions = [{id: 'field-id'}]
            const formInfo = {fieldDefinitions}
            return expectSaga(sagas.loadCurrentUser, formInfo)
              .provide([
                [matchers.call(rest.fetchEntitiesPage, 'User', {where: 'pk == :currentUser'}), []],
                [matchers.call(sagas.setupFormForNewUser, fieldDefinitions)]
              ])
              .call(sagas.setupFormForNewUser, fieldDefinitions)
              .run()
          })
        })

        describe('setupForm', () => {
          test('should initialize redux form', () => {
            const fieldDefinitions = [{id: 'field-id'}]
            const formName = 'form-name'
            const formScope = 'form-scope'
            const formInfo = {fieldDefinitions, formName, formScope}
            const user = {
              key: '321'
            }
            const flatUser = {
              key: '321'
            }
            const formValues = {
              field: 'value'
            }
            return expectSaga(sagas.setupForm, user, formInfo)
              .provide([
                [matchers.call(api.getFlattenEntity, user), flatUser],
                [
                  matchers.call(
                    display.enhanceEntityWithDisplayExpressions,
                    flatUser,
                    formName,
                    fieldDefinitions,
                    formScope
                  )
                ],
                [matchers.call(display.enhanceEntityWithDisplays, flatUser, fieldDefinitions)],
                [matchers.call(form.entityToFormValues, flatUser, fieldDefinitions), formValues]
              ])
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .call(display.enhanceEntityWithDisplayExpressions, flatUser, formName, fieldDefinitions, formScope)
              .call(display.enhanceEntityWithDisplays, flatUser, fieldDefinitions)
              .run()
          })
        })

        describe('setupFormForNewUser', () => {
          test('should initialize redux form', () => {
            const fieldDefinitions = [{id: 'field-id'}, {id: 'second', defaultValue: 'value'}]
            const pseudoEntityPaths = {
              pseudoField_question_1: {
                value: 'value'
              }
            }
            const expectedDefaultValues = {
              second: 'value'
            }
            const expectedEntityValues = {
              __key: undefined,
              __model: 'User',
              __version: undefined,
              pseudoField_question_1: 'value'
            }
            const expectedFormValues = {
              ...expectedEntityValues,
              ...expectedDefaultValues,
              email: 'test@tocco.ch'
            }
            return expectSaga(sagas.setupFormForNewUser, fieldDefinitions)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {pseudoEntityPaths, newUserEmail: 'test@tocco.ch'}]
              ])
              .call(form.getDefaultValues, fieldDefinitions)
              .call(api.getFlattenEntity, {
                model: 'User',
                paths: {...pseudoEntityPaths}
              })
              .call(
                form.entityToFormValues,
                {...expectedEntityValues, ...expectedDefaultValues, email: 'test@tocco.ch'},
                fieldDefinitions
              )
              .put(formActions.initialize(REDUX_FORM_NAME, expectedFormValues))
              .run()
          })

          test('should not set email for no existing new user', () => {
            const fieldDefinitions = []
            const expectedEntityValues = {
              __key: undefined,
              __model: 'User',
              __version: undefined
            }
            const expectedFormValues = {
              ...expectedEntityValues
            }
            return expectSaga(sagas.setupFormForNewUser, fieldDefinitions)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {pseudoEntityPaths: {}, newUserEmail: undefined}]
              ])
              .call(form.getDefaultValues, fieldDefinitions)
              .call(api.getFlattenEntity, {model: 'User', paths: {}})
              .call(form.entityToFormValues, {...expectedEntityValues}, fieldDefinitions)
              .put(formActions.initialize(REDUX_FORM_NAME, expectedFormValues))
              .run()
          })
        })

        describe('register', () => {
          const widgetConfigKey = '456'
          env.setWidgetConfigKey(widgetConfigKey)
          const eventKey = '123'
          const userKey = '321'
          const bean = {
            user: {
              key: userKey
            }
          }
          const requestUrl = `widgets/eventRegistration/${eventKey}/register`
          const requestOptions = {
            method: 'POST',
            body: {...bean},
            acceptedErrorCodes: ['VALIDATION_FAILED']
          }
          const headers = new Headers()
          headers.append('Location', `/nice2/rest/entities/2.0/User/${userKey}`)

          test('should send registration request', () => {
            return expectSaga(sagas.register)
              .provide([
                [matchers.call(sagas.loadRegisterBean), bean],
                [matchers.call(sagas.handleSuccess)],
                [select(sagas.eventRegistrationActionSelector), {eventKey}],
                [matchers.call(rest.requestSaga, requestUrl, requestOptions), {status: 201, headers}]
              ])
              .call(rest.requestSaga, requestUrl, requestOptions)
              .call(sagas.handleSuccess)
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(actions.setUserKey(userKey))
              .run()
          })
          test('should show toaster for validation error', () => {
            const body = {
              errorCode: 'VALIDATION_FAILED',
              errors: [
                {
                  model: 'Registration',
                  entityValidatorErrors: {
                    SingleRegistrationFromFlowValidator: ['already registered']
                  }
                }
              ]
            }
            return expectSaga(sagas.register)
              .provide([
                [matchers.call(sagas.loadRegisterBean), bean],
                [select(sagas.eventRegistrationActionSelector), {eventKey}],
                [matchers.call(rest.requestSaga, requestUrl, requestOptions), {status: 400, body}]
              ])
              .call(rest.requestSaga, requestUrl, requestOptions)
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: 'already registered'
                })
              )
              .run()
          })
          test('should call onError on unsuccesful request', () => {
            return expectSaga(sagas.register)
              .provide([
                [matchers.call(sagas.loadRegisterBean), bean],
                [select(sagas.eventRegistrationActionSelector), {eventKey}],
                [matchers.call(rest.requestSaga, requestUrl, requestOptions), {status: 401, body: {}}]
              ])
              .call(rest.requestSaga, requestUrl, requestOptions)
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(externalEvents.fireExternalEvent('onError'))
              .run()
          })
        })

        describe('loadRegisterBean', () => {
          test('should return register bean', () => {
            const eventKey = '222'
            const questionFieldMapping = {
              1: {type: 'text_single_line', relatedEntityKey: '222'},
              2: {type: 'text_single_line', relatedEntityKey: '345'},
              3: {type: 'text_single_line', relatedEntityKey: '123'},
              4: {type: 'text_single_line', relatedEntityKey: '678'}
            }
            const flatEntity = {
              firstname: 'value',
              pseudoField_question_1: 'value1',
              pseudoField_question_2: 'value2',
              pseudoField_question_3: 'value3',
              pseudoField_question_4: 'value4',
              pseudoField_modules_required: {options: [{id: '123', checked: true, attempt: 2}]},
              pseudoField_modules_optional: {
                options: [
                  {id: '345', checked: true, attempt: 3},
                  {id: '678', checked: false, attempt: 1}
                ]
              }
            }
            const flatUser = {
              firstname: 'value'
            }
            const user = {
              key: '321',
              paths: {}
            }
            const returnValue = {
              user,
              questions: [
                {
                  key: '1',
                  type: 'text_single_line',
                  value: 'value1'
                },
                {
                  key: '2',
                  type: 'text_single_line',
                  value: 'value2'
                },
                {
                  key: '3',
                  type: 'text_single_line',
                  value: 'value3'
                }
              ],
              modules: [
                {
                  key: '123',
                  attempt: 2
                },
                {
                  key: '345',
                  attempt: 3
                }
              ]
            }
            const fieldDefinitions = [
              {
                id: 'field-id'
              },
              {
                id: 'pseudoField_question_1',
                pseudoField: true
              },
              {
                id: 'pseudoField_question_2',
                pseudoField: true
              },
              {
                id: 'pseudoField_question_3',
                pseudoField: true
              },
              {
                id: 'pseudoField_question_4',
                pseudoField: true
              },
              {
                id: 'pseudoField_modules_required',
                pseudoField: true
              },
              {
                id: 'pseudoField_modules_optional',
                pseudoField: true
              }
            ]
            return expectSaga(sagas.loadRegisterBean)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {eventKey, fieldDefinitions, questionFieldMapping}],
                // do not pass arguments here because getFormValues doesn't work with redux-saga-test-plan
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatUser), user]
              ])
              .returns(returnValue)
              .run()
          })
        })

        describe('handleSuccess', () => {
          test('should redirect to payment site', () => {
            const orderUuid = 'uuid'
            const navigateToActionRelative = sinon.spy()
            const navigationStrategy = {
              navigateToActionRelative
            }
            return expectSaga(sagas.handleSuccess)
              .provide([
                [select(sagas.inputSelector), {navigationStrategy}],
                [matchers.call(sagas.checkPaymentModel), true],
                [matchers.call(sagas.loadOrderUuid), orderUuid]
              ])
              .run()
              .then(() => {
                expect(navigateToActionRelative).to.have.been.calledWith({
                  appId: 'payment-provider-action',
                  queryParams: {orderUuid}
                })
              })
          })

          test('should set result site if no payment necessary', () => {
            const navigateToActionRelative = sinon.spy()
            const navigationStrategy = {
              navigateToActionRelative
            }
            return expectSaga(sagas.handleSuccess)
              .provide([
                [select(sagas.inputSelector), {navigationStrategy}],
                [matchers.call(sagas.checkPaymentModel), false]
              ])
              .not.call.fn(sagas.loadOrderUuid)
              .put(actions.setPage(Pages.RegistrationResult))
              .run()
              .then(() => {
                expect(navigateToActionRelative).to.not.have.been.called
              })
          })

          test('should set result site if no order is available', () => {
            const navigateToActionRelative = sinon.spy()
            const navigationStrategy = {
              navigateToActionRelative
            }
            return expectSaga(sagas.handleSuccess)
              .provide([
                [select(sagas.inputSelector), {navigationStrategy}],
                [matchers.call(sagas.checkPaymentModel), true],
                [matchers.call(sagas.loadOrderUuid), null]
              ])
              .put(actions.setPage(Pages.RegistrationResult))
              .run()
              .then(() => {
                expect(navigateToActionRelative).to.not.have.been.called
              })
          })
        })

        describe('checkPaymentModel', () => {
          test('should return true if eventonlinepayment is installed', () => {
            const modules = ['nice.optional.eventonlinepayment', 'nice.optional.event', 'nice.optional.onlinepayment']
            return expectSaga(sagas.checkPaymentModel)
              .provide([[matchers.call(rest.fetchModules), modules]])
              .returns(true)
              .run()
          })

          test('should return false if eventonlinepayment is not installed', () => {
            const modules = ['nice.optional.event', 'nice.optional.onlinepayment']
            return expectSaga(sagas.checkPaymentModel)
              .provide([[matchers.call(rest.fetchModules), modules]])
              .returns(false)
              .run()
          })
        })

        describe('loadOrderUuid', () => {
          test('should return order uuid', () => {
            const userKey = '1'
            const eventKey = '2'
            const uuid = 'some uuid'
            const options = {
              acceptedStatusCodes: [404]
            }
            return expectSaga(sagas.loadOrderUuid)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {userKey, eventKey}],
                [
                  matchers.call(
                    rest.requestSaga,
                    `/widgets/eventRegistration/${eventKey}/order/${userKey}/uuid`,
                    options
                  ),
                  {body: {uuid}, status: 200}
                ]
              ])
              .returns(uuid)
              .run()
          })

          test('should return null if order is missing', () => {
            const userKey = '1'
            const eventKey = '2'
            const options = {
              acceptedStatusCodes: [404]
            }
            return expectSaga(sagas.loadOrderUuid)
              .provide([
                [select(sagas.eventRegistrationActionSelector), {userKey, eventKey}],
                [
                  matchers.call(
                    rest.requestSaga,
                    `/widgets/eventRegistration/${eventKey}/order/${userKey}/uuid`,
                    options
                  ),
                  {status: 404}
                ]
              ])
              .returns(null)
              .run()
          })
        })
      })
    })
  })
})
