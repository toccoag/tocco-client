export const INIT = 'eventRegistrationAction/INIT'
export const LOAD_FORM_INFO = 'eventRegistrationAction/LOAD_FORM_INFO'
export const SET_EVENT_KEY = 'eventRegistrationAction/SET_EVENT_KEY'
export const SET_USER_KEY = 'eventRegistrationAction/SET_USER_KEY'
export const SET_FORM_DEFINITION = 'eventRegistrationAction/SET_FORM_DEFINITION'
export const SET_EVENT_FORM_NAME = 'eventRegistrationAction/SET_EVENT_FORM_NAME'
export const SET_FIELD_DEFINITIONS = 'eventRegistrationAction/SET_FIELD_DEFINITIONS'
export const SET_FORM_SCOPE = 'eventRegistrationAction/SET_FORM_SCOPE'
export const SET_CURRENT_USER = 'eventRegistrationAction/SET_CURRENT_USER'
export const SET_LOADED = 'eventRegistrationAction/SET_INITIALIZED'
export const REGISTER = 'eventRegistrationAction/REGISTER'
export const SET_QUESTION_FIELD_MAPPING = 'eventRegistrationAction/SET_QUESTION_FIELD_MAPPING'
export const ADD_PSEUDO_ENTITY_PATHS = 'eventRegistrationAction/ADD_PSEUDO_ENTITY_PATHS'
export const CHECK_SSO_AVAILABLE = 'eventRegistrationAction/CHECK_SSO_AVAILABLE'
export const SET_SSO_AVAILABLE = 'eventRegistrationAction/SET_SSO_AVAILABLE'
export const SET_NEW_USER_EMAIL = 'eventRegistrationAction/SET_NEW_USER_EMAIL'
export const SET_PAGE = 'eventRegistrationAction/SET_PAGE'
export const SET_SUBMIT_BUTTON_NAME = 'eventRegistrationAction/SET_SUBMIT_BUTTON_NAME'
export const SET_REQUIRED_MODULES_FORBIDDEN = 'eventRegistrationAction/SET_REQUIRED_MODULES_FORBIDDEN'

export const init = () => ({
  type: INIT
})

export const loadFormInfo = () => ({
  type: LOAD_FORM_INFO
})

export const setEventKey = eventKey => ({
  type: SET_EVENT_KEY,
  payload: {eventKey}
})

export const setUserKey = userKey => ({
  type: SET_USER_KEY,
  payload: {userKey}
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const setEventFormName = eventFormName => ({
  type: SET_EVENT_FORM_NAME,
  payload: {eventFormName}
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const setFormScope = formScope => ({
  type: SET_FORM_SCOPE,
  payload: {formScope}
})

export const setCurrentUser = currentUser => ({
  type: SET_CURRENT_USER,
  payload: {currentUser}
})

export const setLoaded = loaded => ({
  type: SET_LOADED,
  payload: {loaded}
})

export const register = () => ({
  type: REGISTER
})

export const setQuestionFieldMapping = questionFieldMapping => ({
  type: SET_QUESTION_FIELD_MAPPING,
  payload: {
    questionFieldMapping
  }
})

export const addPseudoEntityPaths = pseudoEntityPaths => ({
  type: ADD_PSEUDO_ENTITY_PATHS,
  payload: {
    pseudoEntityPaths
  }
})

export const checkSsoAvailable = () => ({
  type: CHECK_SSO_AVAILABLE
})

export const setSsoAvailable = ssoAvailable => ({
  type: SET_SSO_AVAILABLE,
  payload: {
    ssoAvailable
  }
})

export const setNewUserEmail = newUserEmail => ({
  type: SET_NEW_USER_EMAIL,
  payload: {
    newUserEmail
  }
})

export const setPage = page => ({
  type: SET_PAGE,
  payload: {
    page
  }
})

export const setSubmitButtonName = submitButtonName => ({
  type: SET_SUBMIT_BUTTON_NAME,
  payload: {submitButtonName}
})

export const setRequiredModulesForbidden = requiredModulesForbidden => ({
  type: SET_REQUIRED_MODULES_FORBIDDEN,
  payload: {requiredModulesForbidden}
})
