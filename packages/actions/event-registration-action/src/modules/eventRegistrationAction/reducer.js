import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  page: null,
  eventKey: null,
  userKey: null,
  formDefinition: null,
  eventFormName: null,
  fieldDefinitions: null,
  formScope: null,
  currentUser: null,
  loaded: false,
  questionFieldMapping: undefined,
  pseudoEntityPaths: {},
  ssoAvailable: false,
  newUserEmail: undefined,
  submitButtonName: null,
  requiredModulesForbidden: false
}

const addPseudoEntityPaths = (state, {payload: {pseudoEntityPaths}}) => ({
  ...state,
  pseudoEntityPaths: {...state.pseudoEntityPaths, ...pseudoEntityPaths}
})

const ACTION_HANDLERS = {
  [actions.SET_EVENT_KEY]: reducerUtil.singleTransferReducer('eventKey'),
  [actions.SET_USER_KEY]: reducerUtil.singleTransferReducer('userKey'),
  [actions.SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [actions.SET_EVENT_FORM_NAME]: reducerUtil.singleTransferReducer('eventFormName'),
  [actions.SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions'),
  [actions.SET_FORM_SCOPE]: reducerUtil.singleTransferReducer('formScope'),
  [actions.SET_CURRENT_USER]: reducerUtil.singleTransferReducer('currentUser'),
  [actions.SET_LOADED]: reducerUtil.singleTransferReducer('loaded'),
  [actions.SET_QUESTION_FIELD_MAPPING]: reducerUtil.singleTransferReducer('questionFieldMapping'),
  [actions.ADD_PSEUDO_ENTITY_PATHS]: addPseudoEntityPaths,
  [actions.SET_SSO_AVAILABLE]: reducerUtil.singleTransferReducer('ssoAvailable'),
  [actions.SET_NEW_USER_EMAIL]: reducerUtil.singleTransferReducer('newUserEmail'),
  [actions.SET_PAGE]: reducerUtil.singleTransferReducer('page'),
  [actions.SET_SUBMIT_BUTTON_NAME]: reducerUtil.singleTransferReducer('submitButtonName'),
  [actions.SET_REQUIRED_MODULES_FORBIDDEN]: reducerUtil.singleTransferReducer('requiredModulesForbidden')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
