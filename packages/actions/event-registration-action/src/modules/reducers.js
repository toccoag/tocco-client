import {reducer as form} from 'redux-form'

import eventRegistrationActionReducer from './eventRegistrationAction/reducer'
import eventRegistrationActionSagas from './eventRegistrationAction/sagas'

export default {
  eventRegistrationAction: eventRegistrationActionReducer,
  form
}

export const sagas = [eventRegistrationActionSagas]
