import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setNewUserEmail, init, setPage} from '../../modules/eventRegistrationAction/actions'

import EventRegistration from './EventRegistration'

const mapActionCreators = {
  setNewUserEmail,
  init,
  setPage
}

const mapStateToProps = state => ({
  page: state.eventRegistrationAction.page
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EventRegistration))
