import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {Pages} from '../../utils/constants'

import EventRegistration from './EventRegistration'

/* eslint-disable react/prop-types */
jest.mock('../Login', () => ({onLoggedIn, onRegister}) => (
  <div data-testid="login">
    <button onClick={onLoggedIn}>Login</button>
    <button onClick={() => onRegister({username: 'test@tocco.ch'})}>Register</button>
  </div>
))
jest.mock('../RegisterView', () => () => <div data-testid="register"></div>)
/* eslint-enable react/prop-types */

describe('event-registration-action', () => {
  describe('EventRegistration', () => {
    test('should run init on mount', () => {
      const props = {
        init: sinon.spy(),
        setNewUserEmail: sinon.spy(),
        setPage: sinon.spy(),
        page: undefined
      }

      testingLibrary.renderWithIntl(<EventRegistration {...props} />)

      expect(props.init).to.have.been.calledOnce
      expect(screen.queryAllByTestId('login')).to.have.length(0)
      expect(screen.queryAllByTestId('register')).to.have.length(0)
    })

    test('should show login view', () => {
      const props = {
        init: sinon.spy(),
        setNewUserEmail: sinon.spy(),
        setPage: sinon.spy(),
        page: Pages.Login
      }

      testingLibrary.renderWithIntl(<EventRegistration {...props} />)

      expect(screen.queryAllByTestId('login')).to.have.length(1)
      expect(screen.queryAllByTestId('register')).to.have.length(0)
    })

    test('should show register view', () => {
      const props = {
        init: sinon.spy(),
        setNewUserEmail: sinon.spy(),
        setPage: sinon.spy(),
        page: Pages.RegisterView
      }

      testingLibrary.renderWithIntl(<EventRegistration {...props} />)

      expect(screen.queryAllByTestId('login')).to.have.length(0)
      expect(screen.queryAllByTestId('register')).to.have.length(1)
    })

    test('should navigate to register view on login', () => {
      const props = {
        init: sinon.spy(),
        setNewUserEmail: sinon.spy(),
        setPage: sinon.spy(),
        page: Pages.Login
      }

      testingLibrary.renderWithIntl(<EventRegistration {...props} />)

      fireEvent.click(screen.getByRole('button', {name: 'Login'}))

      expect(props.setPage).to.have.been.calledWith(Pages.RegisterView)
    })

    test('should navigate to register view on register new user', () => {
      const props = {
        init: sinon.spy(),
        setNewUserEmail: sinon.spy(),
        setPage: sinon.spy(),
        page: Pages.Login
      }

      testingLibrary.renderWithIntl(<EventRegistration {...props} />)

      fireEvent.click(screen.getByRole('button', {name: 'Register'}))

      expect(props.setNewUserEmail).to.have.been.calledWith('test@tocco.ch')
      expect(props.setPage).to.have.been.calledWith(Pages.RegisterView)
    })
  })
})
