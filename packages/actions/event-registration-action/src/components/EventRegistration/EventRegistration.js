import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'

import {Pages} from '../../utils/constants'
import Login from '../Login'
import RegisterView from '../RegisterView'
import RegistrationResult from '../RegistrationResult'

const EventRegistration = ({init, page, setPage, setNewUserEmail}) => {
  useEffect(() => {
    init()
  }, [init])

  return (
    <LoadMask required={[page !== null]}>
      {(() => {
        switch (page) {
          case Pages.RegisterView:
            return <RegisterView />
          case Pages.Login:
            return (
              <Login
                onLoggedIn={() => setPage(Pages.RegisterView)}
                onSsoLogin={({successful}) => {
                  if (successful) {
                    setPage(Pages.RegisterView)
                  }
                }}
                onRegister={({username}) => {
                  setNewUserEmail(username)
                  setPage(Pages.RegisterView)
                }}
              />
            )
          case Pages.RegistrationResult:
            return <RegistrationResult />
          default:
            return null
        }
      })()}
    </LoadMask>
  )
}

EventRegistration.propTypes = {
  setNewUserEmail: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  init: PropTypes.func.isRequired,
  page: PropTypes.string
}

export default EventRegistration
