import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import LoginApp from 'tocco-login/src/main'
import SsoLoginApp from 'tocco-sso-login/src/main'
import {Typography} from 'tocco-ui'

import {StyledChoiceText, StyledContainer} from './StyledComponents'

const getRedirectUrl = eventKey => {
  const url = new URL(window.location.href)
  url.hash = '' // clear complicated hash history /#action/event-registration-action and use eventKey instead
  if (!url.searchParams.has('eventKey')) {
    url.searchParams.append('eventKey', eventKey)
  }
  return url.href
}

const Login = ({
  ssoAvailable,
  eventKey,
  checkSsoAvailable,
  locale,
  onLoggedIn,
  onSsoLogin,
  onRegister,
  fireExternalEvent
}) => {
  useEffect(() => {
    checkSsoAvailable()
  }, [checkSsoAvailable])

  useEffect(() => {
    fireExternalEvent('onAuthenticationPage')
  }, [fireExternalEvent])

  return (
    <div>
      <StyledContainer>
        <Typography.H2>
          <FormattedMessage id="client.actions.event-registration-action.loginTitle" />
        </Typography.H2>
        {ssoAvailable && (
          <div>
            <SsoLoginApp
              ssoLoginEndpoint="/sso"
              loginCompleted={onSsoLogin}
              redirectUrl={getRedirectUrl(eventKey)}
              locale={locale}
            />
            <StyledChoiceText>
              <FormattedMessage id="client.actions.event-registration-action.loginChoice" />
            </StyledChoiceText>
          </div>
        )}
        <LoginApp
          showTitle={false}
          register={onRegister}
          loginSuccess={onLoggedIn}
          locale={locale}
          entryPage="username"
        />
      </StyledContainer>
    </div>
  )
}

Login.propTypes = {
  checkSsoAvailable: PropTypes.func.isRequired,
  eventKey: PropTypes.string.isRequired,
  onLoggedIn: PropTypes.func.isRequired,
  onSsoLogin: PropTypes.func.isRequired,
  onRegister: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired,
  ssoAvailable: PropTypes.bool,
  locale: PropTypes.string
}

export default Login
