import styled from 'styled-components'
import {declareFont, scale} from 'tocco-ui'

export const StyledContainer = styled.div`
  max-width: 420px;
  margin: 0 auto;
`

export const StyledChoiceText = styled.div`
  ${declareFont()}
  text-align: center;
  padding: ${scale.space(0.6)} 0;
`
