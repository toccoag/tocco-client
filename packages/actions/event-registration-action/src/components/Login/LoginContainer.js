import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import {checkSsoAvailable} from '../../modules/eventRegistrationAction/actions'

import Login from './Login'

const mapActionCreators = {
  checkSsoAvailable,
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  ssoAvailable: state.eventRegistrationAction.ssoAvailable,
  locale: state.intl.locale,
  eventKey: state.eventRegistrationAction.eventKey
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Login))
