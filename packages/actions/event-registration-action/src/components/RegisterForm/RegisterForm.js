import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

import {mapFormValuesToModuleKeys} from '../../utils/modules'

export const REDUX_FORM_NAME = 'event-registration-action'

const RegisterForm = ({
  formDefinition,
  formScope,
  formValues,
  submitting,
  valid,
  currentUser,
  register,
  fireExternalEvent,
  eventKey,
  questionFieldMapping,
  intl,
  submitButtonName,
  requiredModulesForbidden
}) => {
  const formEventProps = form.hooks.useFormEvents({submitForm: register})

  useEffect(() => {
    fireExternalEvent('onRegisterPage')
  }, [fireExternalEvent])

  const shouldRenderField = fieldName => {
    if (form.isQuestionField(fieldName)) {
      const eventKeys = mapFormValuesToModuleKeys(formValues)
      return form.shouldRenderQuestionField(fieldName, [eventKey, ...eventKeys], questionFieldMapping)
    }
    return true
  }

  return (
    <form {...formEventProps}>
      <form.FormBuilder
        entity={currentUser}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        mode={formScope}
        beforeRenderField={shouldRenderField}
      />
      <Button
        disabled={submitting || !valid || requiredModulesForbidden}
        label={submitButtonName || intl.formatMessage({id: 'client.actions.event-registration-action.submit'})}
        type="submit"
        look="raised"
        ink="primary"
        data-cy="sumbit-event-registration"
      />
    </form>
  )
}

RegisterForm.propTypes = {
  formDefinition: PropTypes.object,
  formScope: PropTypes.string,
  currentUser: PropTypes.object,
  formValues: PropTypes.object,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  register: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  questionFieldMapping: PropTypes.object,
  eventKey: PropTypes.string.isRequired,
  submitButtonName: PropTypes.string,
  requiredModulesForbidden: PropTypes.bool
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(RegisterForm)
