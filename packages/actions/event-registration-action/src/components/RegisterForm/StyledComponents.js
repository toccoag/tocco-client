import styled from 'styled-components'
import {themeSelector, scale} from 'tocco-ui'

const promotionStatusColorMap = {
  passed: 'signal.successLight',
  not_passed: 'signal.dangerLight'
}

const getAttemptColor = (attempt, maxAttempts) => {
  if (maxAttempts && maxAttempts < attempt) {
    return 'signal.dangerLight'
  } else if (maxAttempts && maxAttempts === attempt) {
    return 'signal.warningLight'
  }

  return 'signal.successLight'
}

export const StyledPromotionStatus = styled.span`
  display: inline-block;
  background-color: ${({promotionStatus, theme}) =>
    themeSelector.color(promotionStatusColorMap[promotionStatus] || 'signal.infoLight')({theme})};
  border-radius: ${themeSelector.radii('statusLabel')};
  padding: ${scale.space(-2.1)} ${scale.space(0)};
`

export const StyledAttempt = styled.span`
  margin-left: ${scale.space(-1)};
  background-color: ${({attempt, maxAttempts, theme}) =>
    themeSelector.color(getAttemptColor(attempt, maxAttempts) || 'signal.infoLight')({theme})};
  border-radius: ${themeSelector.radii('statusLabel')};
  padding: ${scale.space(-2.1)} ${scale.space(0)};
`
