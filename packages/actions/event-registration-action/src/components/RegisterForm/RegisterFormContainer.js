import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form, externalEvents} from 'tocco-app-extensions'

import {register} from '../../modules/eventRegistrationAction/actions'

import RegisterForm, {REDUX_FORM_NAME} from './RegisterForm'

const mapActionCreators = {
  register,
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  formDefinition: state.eventRegistrationAction.formDefinition,
  formScope: state.eventRegistrationAction.formScope,
  currentUser: state.eventRegistrationAction.currentUser,
  loaded: state.eventRegistrationAction.loaded,
  questionFieldMapping: state.eventRegistrationAction.questionFieldMapping,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state),
  submitButtonName: state.eventRegistrationAction.submitButtonName,
  requiredModulesForbidden: state.eventRegistrationAction.requiredModulesForbidden
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(RegisterForm))
