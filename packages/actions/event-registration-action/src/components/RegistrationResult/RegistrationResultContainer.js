import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import RegistrationResult from './RegistrationResult'

const mapActionCreators = {
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({})

export default connect(mapStateToProps, mapActionCreators)(RegistrationResult)
