import {testingLibrary} from 'tocco-test-util'

import RegistrationResult from './RegistrationResult'

describe('event-registration-action', () => {
  describe('RegistrationResult', () => {
    test('should fire external payment event when result page is displayed', () => {
      const props = {
        fireExternalEvent: sinon.spy(),
        paymentStatus: 'success'
      }

      testingLibrary.renderWithIntl(<RegistrationResult {...props} />)

      expect(props.fireExternalEvent).to.have.been.calledWith('onSuccess')
    })
  })
})
