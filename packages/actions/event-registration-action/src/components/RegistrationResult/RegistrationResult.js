import PropTypes from 'prop-types'
import {useEffect} from 'react'

const RegistrationResult = ({fireExternalEvent}) => {
  useEffect(() => {
    fireExternalEvent('onSuccess')
  }, [fireExternalEvent])
  return <></>
}

RegistrationResult.propTypes = {
  fireExternalEvent: PropTypes.func.isRequired
}

export default RegistrationResult
