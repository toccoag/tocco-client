import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'

import {loadFormInfo} from '../../modules/eventRegistrationAction/actions'
import {REDUX_FORM_NAME} from '../RegisterForm'

import EventRegistrationActionView from './RegisterView'

const mapActionCreators = {
  loadFormInfo
}

const mapStateToProps = state => ({
  formDefinition: state.eventRegistrationAction.formDefinition,
  eventFormName: state.eventRegistrationAction.eventFormName,
  fieldDefinitions: state.eventRegistrationAction.fieldDefinitions,
  formScope: state.eventRegistrationAction.formScope,
  loaded: state.eventRegistrationAction.loaded,
  eventKey: state.eventRegistrationAction.eventKey,
  locale: state.intl.locale,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EventRegistrationActionView))
