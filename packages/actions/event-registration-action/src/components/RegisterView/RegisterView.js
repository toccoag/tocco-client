import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {form} from 'tocco-app-extensions'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {LoadMask} from 'tocco-ui'

import RegisterForm from '../RegisterForm'

const EventRegistrationAction = ({
  formDefinition,
  eventFormName,
  fieldDefinitions,
  formScope,
  formInitialValues,
  loaded,
  loadFormInfo,
  eventKey,
  locale
}) => {
  useEffect(() => {
    loadFormInfo()
  }, [loadFormInfo])

  const handleAsyncValidate = form.hooks.useAsyncValidation({
    formInitialValues,
    fieldDefinitions,
    formDefinition,
    mode: formScope
  })
  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  return (
    <LoadMask required={[loaded]}>
      {eventFormName && (
        <EntityDetailApp
          entityName="Event"
          formName={eventFormName}
          entityId={eventKey}
          mode="update"
          locale={locale}
        />
      )}
      <RegisterForm
        validate={handleSyncValidate}
        asyncValidate={handleAsyncValidate}
        asyncBlurFields={fieldDefinitions ? form.getUsedPaths(fieldDefinitions).map(form.transformFieldName) : null}
        eventKey={eventKey}
      />
    </LoadMask>
  )
}

EventRegistrationAction.propTypes = {
  loaded: PropTypes.bool,
  loadFormInfo: PropTypes.func.isRequired,
  formDefinition: PropTypes.object,
  eventFormName: PropTypes.string,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object),
  formScope: PropTypes.string,
  formInitialValues: PropTypes.object,
  intl: PropTypes.object.isRequired,
  eventKey: PropTypes.string.isRequired,
  locale: PropTypes.string
}

export default EventRegistrationAction
