import {selection as selectionUtil} from 'tocco-app-extensions'
import {action} from 'tocco-util'

import {setEventKey} from './modules/eventRegistrationAction/actions'

export const getDispatchActions = input => action.getDispatchActions(input, actionSettings)

const actionSettings = [
  {
    name: 'selection',
    action: setEventKey,
    argsFactory: input => [selectionUtil.getSingleKey(input.selection, 'Event')]
  },
  {
    name: 'eventKey',
    action: setEventKey,
    argsFactory: input => [input.eventKey]
  }
]
