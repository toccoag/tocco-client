import PropTypes from 'prop-types'
import {appFactory, selection, formData, externalEvents, actionEmitter, notification, login} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, appContext} from 'tocco-util'

import EventRegistration from './components/EventRegistration'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'event-registration-action'
const EXTERNAL_EVENTS = [
  'onSuccess',
  'onError',
  /**
   * Is fired when the authentication page is rendered
   */
  'onAuthenticationPage',
  /**
   * Is fired when the register page is rendered
   */
  'onRegisterPage',
  'emitAction'
]

const initApp = (id, input, events, publicPath) => {
  const content = <EventRegistration />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({listApp: EntityListApp}))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  login.addToStore(store)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EventRegistrationAction = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

EventRegistrationAction.propTypes = {
  appContext: appContext.propTypes.isRequired,
  /**
   * Selection of a single `Event` (only type `ID` is supported and exacte one key is required)
   * either `selection` or `eventKey` is mandatory
   */
  selection: selection.propType,
  /**
   * Event key for registration
   * either `selection` or `eventKey` is mandatory
   */
  eventKey: PropTypes.string
}

export default EventRegistrationAction
