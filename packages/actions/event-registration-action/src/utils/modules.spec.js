import {screen} from '@testing-library/react'
import _omit from 'lodash/omit'
import _reduce from 'lodash/reduce'
import {testingLibrary} from 'tocco-test-util'

import {
  getOptionalModules,
  getRequiredModules,
  mapModulesToBean,
  mapModulesToPseudoField,
  getLabelComponent
} from './modules'

const modules = [
  {
    key: '345',
    type: 'required',
    display: 'A',
    checked: true,
    checkboxHidden: false,
    additionalInformation: {
      attempt: 3
    }
  },
  {
    key: '678',
    type: 'optional',
    display: 'B',
    checked: false,
    editable: true,
    checkboxHidden: false,
    additionalInformation: {
      attempt: 1
    }
  }
]

describe('event-registration-action', () => {
  describe('utils', () => {
    describe('modules', () => {
      describe('getRequiredModules', () => {
        test('should filter required modules', () => {
          const requiredModules = getRequiredModules(modules)

          expect(requiredModules).to.eql([modules[0]])
        })
      })

      describe('getOptionalModules', () => {
        test('should filter optionsl modules', () => {
          const optionalModules = getOptionalModules(modules)

          expect(optionalModules).to.eql([modules[1]])
        })
      })

      describe('mapModulesToPseudoField', () => {
        const withoutLabelComponent = pseudoField => ({
          ...pseudoField,
          entityPath: _reduce(
            pseudoField.entityPath,
            (result, obj, key) => ({
              ...result,
              [key]: {
                ...obj,
                value: {
                  ...obj.value,
                  options: obj.value.options.map(o => _omit(o, ['labelComponent']))
                }
              }
            }),
            {}
          )
        })

        test('should map required modules', () => {
          const label = 'A'
          const requiredModules = [modules[0]]
          const minSelection = 1
          const maxSelection = 1
          const pseudoField = mapModulesToPseudoField(label, requiredModules, true, minSelection, maxSelection)

          expect(withoutLabelComponent(pseudoField)).to.eql({
            formField: {
              id: 'pseudoField_modules_required',
              label,
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_modules_required',
                  componentType: 'field',
                  pseudoField: true,
                  renderHtmlInOptions: true,
                  path: 'pseudoField_modules_required',
                  dataType: 'choice',
                  validation: {minSelection, mandatory: true, maxSelection},
                  selectionType: 'multiple'
                }
              ]
            },
            entityPath: {
              pseudoField_modules_required: {
                type: 'choice',
                value: {
                  options: [
                    {
                      id: '345',
                      checked: true,
                      immutable: true,
                      hideCheckbox: false,
                      attempt: 3
                    }
                  ]
                },
                writable: true
              }
            }
          })
        })

        test('should map optional modules', () => {
          const label = 'B'
          const optionalModules = [modules[1]]
          const minSelection = undefined
          const maxSelection = undefined
          const pseudoField = mapModulesToPseudoField(label, optionalModules, false, minSelection, maxSelection)

          expect(withoutLabelComponent(pseudoField)).to.eql({
            formField: {
              id: 'pseudoField_modules_optional',
              label,
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_modules_optional',
                  componentType: 'field',
                  pseudoField: true,
                  renderHtmlInOptions: true,
                  path: 'pseudoField_modules_optional',
                  dataType: 'choice',
                  validation: {},
                  selectionType: 'multiple'
                }
              ]
            },
            entityPath: {
              pseudoField_modules_optional: {
                type: 'choice',
                value: {
                  options: [
                    {
                      id: '678',
                      checked: false,
                      immutable: false,
                      hideCheckbox: false,
                      attempt: 1
                    }
                  ]
                },
                writable: true
              }
            }
          })
        })

        test('should handle hidden checkbox', () => {
          const label = 'label'
          const module = {
            key: '123',
            type: 'optional',
            display: 'display',
            checked: false,
            editable: false,
            checkboxHidden: true,
            additionalInformation: {
              attempt: 2
            }
          }
          const minSelection = undefined
          const maxSelection = undefined
          const pseudoField = mapModulesToPseudoField(label, [module], false, minSelection, maxSelection)

          expect(withoutLabelComponent(pseudoField)).to.eql({
            formField: {
              id: 'pseudoField_modules_optional',
              label,
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_modules_optional',
                  componentType: 'field',
                  pseudoField: true,
                  renderHtmlInOptions: true,
                  path: 'pseudoField_modules_optional',
                  dataType: 'choice',
                  validation: {},
                  selectionType: 'multiple'
                }
              ]
            },
            entityPath: {
              pseudoField_modules_optional: {
                type: 'choice',
                value: {
                  options: [
                    {
                      id: '123',
                      hideCheckbox: true,
                      checked: false,
                      immutable: true,
                      attempt: 2
                    }
                  ]
                },
                writable: true
              }
            }
          })
        })
      })

      describe('getLabelComponent', () => {
        test('should add promotion status', () => {
          const option = {
            key: '123',
            display: 'display',
            additionalInformation: {
              promotion_status_id: 'promotion',
              promotion_status_label: 'promotion_label'
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('promotion_label')).to.exist
        })

        test('should add attempt without max attempt', () => {
          const option = {
            key: '123',
            display: 'display',
            additionalInformation: {
              attempt: 1
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.attempt')).to.exist
        })

        test('should add attempt with max attempt', () => {
          const option = {
            key: '123',
            display: 'display',
            additionalInformation: {
              attempt: 2,
              max_attempts: 2
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.attemptWithMax')).to.exist
        })

        test('should add over max attempt', () => {
          const option = {
            key: '123',
            display: 'display',
            additionalInformation: {
              attempt: 3,
              max_attempts: 2
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.overMaxAttempts')).to.exist
        })

        test('should hide attempt on hidden checkbox', () => {
          const option = {
            key: '123',
            display: 'display',
            checkboxHidden: true,
            additionalInformation: {
              attempt: 3
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.attempt')).to.not.exist
          expect(screen.queryByText('client.actions.event-registration-action.attemptWithMax')).to.not.exist
          expect(screen.queryByText('client.actions.event-registration-action.overMaxAttempts')).to.not.exist
        })

        test('should hide attempt on hidden checkbox with max attempts', () => {
          const option = {
            key: '123',
            display: 'display',
            checkboxHidden: true,
            additionalInformation: {
              attempt: 3,
              max_attempts: 3
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.attempt')).to.not.exist
          expect(screen.queryByText('client.actions.event-registration-action.attemptWithMax')).to.not.exist
          expect(screen.queryByText('client.actions.event-registration-action.overMaxAttempts')).to.not.exist
        })

        test('should show attempt on hidden checkbox and too many attempts', () => {
          const option = {
            key: '123',
            display: 'display',
            checkboxHidden: true,
            additionalInformation: {
              attempt: 3,
              max_attempts: 2
            }
          }
          testingLibrary.renderWithIntl(getLabelComponent(option))
          expect(screen.queryByText('display')).to.exist
          expect(screen.queryByText('client.actions.event-registration-action.overMaxAttempts')).to.exist
        })
      })

      describe('mapModulesToBean', () => {
        test('should map bean', () => {
          const pseudoFields = {
            modules_required: {
              options: [
                {
                  id: '345',
                  label: 'A',
                  checked: true,
                  attempt: 1
                }
              ]
            },
            modules_optional: {
              options: [
                {
                  id: '678',
                  label: 'B',
                  checked: true,
                  attempt: 2
                },
                {
                  id: '901',
                  label: 'C',
                  checked: false,
                  attempt: 3
                }
              ]
            }
          }

          const modulesForBean = mapModulesToBean(pseudoFields)

          expect(modulesForBean).to.eql([
            {key: '345', attempt: 1},
            {key: '678', attempt: 2}
          ])
        })
      })
    })
  })
})
