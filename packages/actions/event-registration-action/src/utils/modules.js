import {FormattedMessage} from 'react-intl'
import {form} from 'tocco-app-extensions'

import {StyledPromotionStatus, StyledAttempt} from '../components/RegisterForm/StyledComponents'

const REQUIRED = 'required'
const OPTIONAL = 'optional'
const getPrefixedPath = key => `modules_${key}`

export const getRequiredModules = modules => modules.filter(m => m.type === REQUIRED)
export const getOptionalModules = modules => modules.filter(m => m.type !== REQUIRED)

export const mapModulesToPseudoField = (label, modules, required, minSelection, maxSelection) => {
  const path = getPrefixedPath(required ? REQUIRED : OPTIONAL)

  const choiceOptions = modules.map(o => {
    const labelComponent = getLabelComponent(o)
    return form.createChoiceOption(o.key, {
      ...(labelComponent ? {labelComponent} : {label: o.display}),
      checked: o.checked,
      immutable: !o.editable,
      hideCheckbox: o.checkboxHidden,
      attempt: o.additionalInformation.attempt
    })
  })

  return form.createMultipleChoiceFieldWithSelection(path, choiceOptions, label, false, minSelection, maxSelection, {
    renderHtmlInOptions: true
  })
}

export const getLabelComponent = option => {
  const {
    promotion_status_id: promotionStatusId,
    promotion_status_label: promotionStatusLabel,
    attempt,
    max_attempts: maxAttempts
  } = option?.additionalInformation || {}

  const showAttempts = attempt && (!option.checkboxHidden || (maxAttempts && maxAttempts < attempt))

  if (promotionStatusId || attempt) {
    return (
      <>
        {option.display}
        {promotionStatusId && showAttempts ? (
          <StyledPromotionStatus promotionStatus={promotionStatusId}>
            {promotionStatusLabel}
            {' / '}
            <FormattedMessage
              id={getAttemptMessageId(attempt, maxAttempts)}
              values={{
                attempt,
                maxAttempts
              }}
            />
          </StyledPromotionStatus>
        ) : (
          <>
            {promotionStatusId && (
              <StyledPromotionStatus promotionStatus={promotionStatusId}>{promotionStatusLabel}</StyledPromotionStatus>
            )}
            {showAttempts && (
              <StyledAttempt attempt={attempt} maxAttempts={maxAttempts}>
                <FormattedMessage
                  id={getAttemptMessageId(attempt, maxAttempts)}
                  values={{
                    attempt,
                    maxAttempts
                  }}
                />
              </StyledAttempt>
            )}
          </>
        )}
      </>
    )
  }

  return null
}

const getAttemptMessageId = (attempt, maxAttempts) => {
  if (maxAttempts) {
    return maxAttempts < attempt
      ? 'client.actions.event-registration-action.overMaxAttempts'
      : 'client.actions.event-registration-action.attemptWithMax'
  }

  return 'client.actions.event-registration-action.attempt'
}

export const mapFormValuesToModuleKeys = formValues => {
  const pseudoFields = {
    [getPrefixedPath(REQUIRED)]: formValues[form.transformPath(getPrefixedPath(REQUIRED))],
    [getPrefixedPath(OPTIONAL)]: formValues[form.transformPath(getPrefixedPath(OPTIONAL))]
  }
  return mapModulesToBean(pseudoFields)
}

export const mapModulesToBean = pseudoFields => [
  ...mapPseudoFieldToModuleKeys(REQUIRED, pseudoFields),
  ...mapPseudoFieldToModuleKeys(OPTIONAL, pseudoFields)
]

const mapPseudoFieldToModuleKeys = (type, pseudoFields) => {
  const rawValue = pseudoFields[getPrefixedPath(type)]
  return rawValue ? rawValue.options.filter(o => o.checked).map(o => ({key: o.id, attempt: o.attempt})) : []
}
