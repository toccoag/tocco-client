import PropTypes from 'prop-types'
import {useEffect} from 'react'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {Button, LoadMask, StyledStickyButtonWrapper} from 'tocco-ui'
import {navigationStrategy as navigationStrategyUtil} from 'tocco-util'

const ConfigureAutomation = ({
  fetchConfigs,
  setValid,
  setCurrentValues,
  submit,
  initialized,
  configs,
  formDefinitions,
  formValues,
  valid,
  submitting,
  navigationStrategy,
  listApp,
  emitAction,
  intl
}) => {
  useEffect(() => {
    fetchConfigs()
  }, [fetchConfigs])

  return (
    <LoadMask required={[initialized]}>
      {Object.keys(configs).map(model => {
        const onChange = e => {
          setValid(model, e.valid)
          setCurrentValues(model, e.values)
        }
        return (
          <SimpleFormApp
            key={model}
            form={formDefinitions[model] || {}}
            mode="create"
            onChange={onChange}
            noButtons={true}
            defaultValues={formValues[model]}
            entityName={model}
            navigationStrategy={navigationStrategy}
            listApp={listApp}
            emitAction={emitAction}
          />
        )
      })}
      <StyledStickyButtonWrapper>
        <Button
          label={intl.formatMessage({id: 'client.actions.configure-automation.save'})}
          ink="primary"
          look="raised"
          disabled={Object.values(valid).some(v => !v) || submitting}
          pending={submitting}
          onClick={submit}
        />
      </StyledStickyButtonWrapper>
    </LoadMask>
  )
}

ConfigureAutomation.propTypes = {
  fetchConfigs: PropTypes.func.isRequired,
  setValid: PropTypes.func.isRequired,
  setCurrentValues: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
  initialized: PropTypes.bool.isRequired,
  configs: PropTypes.object,
  formDefinitions: PropTypes.object,
  formValues: PropTypes.object,
  valid: PropTypes.objectOf(PropTypes.bool),
  submitting: PropTypes.bool,
  navigationStrategy: navigationStrategyUtil.propTypes.isRequired,
  listApp: PropTypes.func,
  emitAction: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default ConfigureAutomation
