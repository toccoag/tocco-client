import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {fetchConfigs, setCurrentValues, setValid, submit} from '../../modules/configureAutomation/actions'

import ConfigureAutomation from './ConfigureAutomation'

const mapActionCreators = {
  fetchConfigs,
  setValid,
  setCurrentValues,
  submit
}

const mapStateToProps = state => ({
  initialized: state.configureAutomation.initialized,
  configs: state.configureAutomation.configs,
  formDefinitions: state.configureAutomation.formDefinitions,
  formValues: state.configureAutomation.formValues,
  valid: state.configureAutomation.valid,
  submitting: state.configureAutomation.submitting,
  navigationStrategy: state.input.navigationStrategy,
  listApp: state.input.listApp,
  emitAction: state.input.emitAction
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ConfigureAutomation))
