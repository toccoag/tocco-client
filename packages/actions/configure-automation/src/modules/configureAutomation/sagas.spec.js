import {all, takeLatest, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {form, display, externalEvents, rest, notification} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('configure-automation', () => {
  describe('sagas', () => {
    describe('rootSaga', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([takeLatest(actions.FETCH_CONFIGS, sagas.fetchConfigs), takeLatest(actions.SUBMIT, sagas.submit)])
        )
        expect(generator.next().done).to.equal(true)
      })
    })

    describe('fetchConfigs', () => {
      test('fetchConfigs', () => {
        const selection = {
          entityName: 'Automation',
          type: 'ID',
          ids: ['5202']
        }
        const configs = [
          {
            entityName: 'Mail_automation',
            key: '2',
            version: 1
          }
        ]
        const body = {
          configs
        }

        return expectSaga(sagas.fetchConfigs)
          .provide([
            [select(sagas.inputSelector), {selection}],
            [matchers.call(rest.requestSaga, 'automation/actions/configureAutomation/5202', {method: 'GET'}), {body}],
            [matchers.call(sagas.loadFormDefinition, 'Mail_automation'), {}],
            [matchers.call(sagas.loadEntity, 'Mail_automation', '2'), {}]
          ])
          .put(actions.setConfigs(configs))
          .put(actions.setInitialized(true))
          .run()
      })
    })

    describe('loadFormDefinition', () => {
      test('loadFormDefinition', () => {
        const formDefinition = {
          id: 'Mail_automation_create',
          children: []
        }

        return expectSaga(sagas.loadFormDefinition, 'Mail_automation')
          .provide([[matchers.call(rest.fetchForm, 'Mail_automation', 'create'), formDefinition]])
          .put(actions.setFormDefinition('Mail_automation', formDefinition))
          .run()
      })
    })

    describe('loadEntity', () => {
      test('loadEntity', () => {
        const formDefinition = {
          id: 'Mail_automation_create',
          children: [
            {
              componentType: 'field-set',
              id: 'relEmail_template'
            }
          ]
        }
        const formDefinitions = {
          Mail_automation: formDefinition
        }
        const fieldDefinitions = [
          {
            id: 'relEmail_template',
            componentType: 'field'
          }
        ]
        const response = {
          paths: {
            relEmail_template: {
              value: {
                key: '152'
              }
            }
          }
        }
        const flattenEntity = {
          relEmail_template: {
            key: '152'
          }
        }
        const formValues = {
          __key: undefined,
          __model: undefined,
          __version: undefined,
          relEmail_template: {
            key: '152'
          }
        }

        return expectSaga(sagas.loadEntity, 'Mail_automation', '2')
          .provide([
            [select(sagas.configureAutomationSelector), {formDefinitions}],
            [matchers.call(form.getFieldDefinitions, formDefinition), fieldDefinitions],
            [matchers.call(rest.fetchEntity, 'Mail_automation', '2', {paths: ['relEmail_template']}), response],
            [matchers.call(display.enhanceEntityWithDisplays, flattenEntity, fieldDefinitions), {}],
            [matchers.call(form.entityToFormValues, flattenEntity, fieldDefinitions), formValues]
          ])
          .put(actions.setFormValues('Mail_automation', formValues))
          .run()
      })
    })

    describe('submit', () => {
      const currentValues = {
        Mail_automation: {
          relEmail_template: {
            key: '122'
          }
        }
      }
      const configs = {
        Mail_automation: {
          key: '2',
          version: 1
        }
      }
      const selection = {
        entityName: 'Automation',
        type: 'ID',
        ids: ['5202']
      }
      const entityBeans = [
        {
          model: 'Mail_automation',
          key: '2',
          version: 1,
          paths: {
            relEmail_template: {
              key: '122'
            }
          }
        }
      ]

      test('successfully saved', () => {
        const response = {
          status: 204
        }

        return expectSaga(sagas.submit)
          .provide([
            [select(sagas.configureAutomationSelector), {currentValues, configs}],
            [select(sagas.inputSelector), {selection}],
            [
              matchers.call(rest.requestSaga, 'automation/actions/configureAutomation/5202', {
                method: 'PUT',
                body: {entityBeans},
                acceptedStatusCodes: [400]
              }),
              response
            ]
          ])
          .put(actions.setSumbitting(true))
          .put(externalEvents.fireExternalEvent('onSuccess'))
          .run()
      })

      test('error during saving saved', () => {
        const response = {
          status: 400,
          body: {
            message: 'An error occurred!'
          }
        }

        return expectSaga(sagas.submit)
          .provide([
            [select(sagas.configureAutomationSelector), {currentValues, configs}],
            [select(sagas.inputSelector), {selection}],
            [
              matchers.call(rest.requestSaga, 'automation/actions/configureAutomation/5202', {
                method: 'PUT',
                body: {entityBeans},
                acceptedStatusCodes: [400]
              }),
              response
            ]
          ])
          .put(actions.setSumbitting(true))
          .put(actions.setSumbitting(false))
          .put(
            notification.toaster({
              type: 'warning',
              title: 'client.actions.configure-automation.validationFailed',
              body: 'An error occurred!'
            })
          )
          .run()
      })
    })
  })
})
