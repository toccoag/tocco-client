import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, display, form, externalEvents, notification, selection as selectionUtil} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import * as actions from './actions'

export const configureAutomationSelector = state => state.configureAutomation
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeLatest(actions.FETCH_CONFIGS, fetchConfigs), takeLatest(actions.SUBMIT, submit)])
}

export function* fetchConfigs() {
  const {selection} = yield select(inputSelector)
  const key = selectionUtil.getSingleKey(selection, 'Automation')

  const {
    body: {configs}
  } = yield call(rest.requestSaga, `automation/actions/configureAutomation/${key}`, {
    method: 'GET'
  })

  yield put(actions.setConfigs(configs))
  yield all(configs.map(config => call(loadFormDefinition, config.entityName)))
  yield all(configs.filter(config => config.key).map(config => call(loadEntity, config.entityName, config.key)))
  yield put(actions.setInitialized(true))
}

export function* loadFormDefinition(model) {
  const formDefinition = yield call(rest.fetchForm, model, 'create')
  yield put(actions.setFormDefinition(model, form.removeActions(formDefinition, ['save'])))
}

export function* loadEntity(model, key) {
  const {formDefinitions} = yield select(configureAutomationSelector)
  const formDefinition = formDefinitions[model]

  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)
  const paths = yield call(form.getUsedPaths, fieldDefinitions)

  const entity = yield call(rest.fetchEntity, model, key, {paths})
  const flattenEntity = yield call(api.getFlattenEntity, entity)

  yield call(display.enhanceEntityWithDisplays, flattenEntity, fieldDefinitions)

  const formValues = yield call(form.entityToFormValues, flattenEntity, fieldDefinitions)
  yield put(actions.setFormValues(model, formValues))
}

export function* submit() {
  const {currentValues, configs} = yield select(configureAutomationSelector)

  const entityBeans = Object.entries(currentValues).map(([model, paths]) => ({
    model,
    key: configs[model].key,
    version: configs[model].version,
    paths
  }))

  const {selection} = yield select(inputSelector)
  const key = selectionUtil.getSingleKey(selection, 'Automation')

  yield put(actions.setSumbitting(true))
  const response = yield call(rest.requestSaga, `automation/actions/configureAutomation/${key}`, {
    method: 'PUT',
    body: {
      entityBeans
    },
    acceptedStatusCodes: [400]
  })

  if (response.status === 204) {
    yield put(externalEvents.fireExternalEvent('onSuccess'))
  } else {
    yield put(actions.setSumbitting(false))
    yield put(
      notification.toaster({
        type: 'warning',
        title: 'client.actions.configure-automation.validationFailed',
        body: response.body.message
      })
    )
  }
}
