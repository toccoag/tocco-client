import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const setConfigs = (state, {payload: {configs}}) => ({
  ...state,
  configs: configs.reduce(
    (acc, curr) => ({
      ...acc,
      [curr.entityName]: {
        key: curr.key,
        version: curr.version
      }
    }),
    {}
  )
})

const ACTION_HANDLERS = {
  [actions.SET_CONFIGS]: setConfigs,
  [actions.SET_FORM_DEFINITION]: reducerUtil.addToObjectReducer('model', 'formDefinition', 'formDefinitions'),
  [actions.SET_FORM_VALUES]: reducerUtil.addToObjectReducer('model', 'formValues', 'formValues'),
  [actions.SET_INITIALIZED]: reducerUtil.singleTransferReducer('initialized'),
  [actions.SET_VALID]: reducerUtil.addToObjectReducer('model', 'valid', 'valid'),
  [actions.SET_CURRENT_VALUES]: reducerUtil.addToObjectReducer('model', 'currentValues', 'currentValues'),
  [actions.SET_SUBMITTING]: reducerUtil.singleTransferReducer('submitting')
}

const initialState = {
  configs: {},
  formDefinitions: {},
  formValues: {},
  initialized: false,
  currentValues: {},
  valid: {},
  submitting: false
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
