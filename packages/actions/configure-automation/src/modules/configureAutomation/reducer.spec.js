import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  configs: {},
  formDefinitions: {},
  formValues: {},
  initialized: false,
  currentValues: {},
  valid: {},
  submitting: false
}

describe('configure-automation', () => {
  describe('modules', () => {
    describe('configureAutomation', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        describe('SET_CONFIGS', () => {
          test('should handle SET_CONFIGS', () => {
            const configs = [
              {
                entityName: 'Automation_listener',
                key: '2',
                version: 0
              },
              {
                entityName: 'Mail_automation',
                key: '1',
                version: 1
              }
            ]

            const stateBefore = {
              configs: {},
              formDefinitions: {}
            }
            const expectedStateAfter = {
              configs: {
                Automation_listener: {
                  key: '2',
                  version: 0
                },
                Mail_automation: {
                  key: '1',
                  version: 1
                }
              },
              formDefinitions: {}
            }

            expect(reducer(stateBefore, actions.setConfigs(configs))).to.be.eql(expectedStateAfter)
          })
        })
      })
    })
  })
})
