export const FETCH_CONFIGS = 'configureAutomation/FETCH_CONFIGS'
export const SET_CONFIGS = 'configureAutomation/SET_CONFIGS'
export const SET_FORM_DEFINITION = 'configureAutomation/SET_FORM_DEFINITION'
export const SET_FORM_VALUES = 'configureAutomation/SET_FORM_VALUES'
export const SET_INITIALIZED = 'configureAutomation/SET_INITIALIZED'
export const SET_VALID = 'configureAutomation/SET_VALID'
export const SET_SUBMITTING = 'configureAutomation/SET_SUBMITTING'
export const SET_CURRENT_VALUES = 'configureAutomation/SET_CURRENT_VALUES'
export const SUBMIT = 'configureAutomation/SUBMIT'

export const fetchConfigs = () => ({
  type: FETCH_CONFIGS
})

export const setConfigs = configs => ({
  type: SET_CONFIGS,
  payload: {
    configs
  }
})

export const setFormDefinition = (model, formDefinition) => ({
  type: SET_FORM_DEFINITION,
  payload: {
    model,
    formDefinition
  }
})

export const setFormValues = (model, formValues) => ({
  type: SET_FORM_VALUES,
  payload: {
    model,
    formValues
  }
})

export const setInitialized = initialized => ({
  type: SET_INITIALIZED,
  payload: {
    initialized
  }
})

export const setValid = (model, valid) => ({
  type: SET_VALID,
  payload: {
    model,
    valid
  }
})

export const setSumbitting = submitting => ({
  type: SET_SUBMITTING,
  payload: {
    submitting
  }
})

export const setCurrentValues = (model, currentValues) => ({
  type: SET_CURRENT_VALUES,
  payload: {
    model,
    currentValues
  }
})

export const submit = () => ({
  type: SUBMIT
})
