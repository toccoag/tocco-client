import configureAutomation, {sagas as configureAutomationSagas} from './configureAutomation'

export default {
  configureAutomation
}

export const sagas = [configureAutomationSagas]
