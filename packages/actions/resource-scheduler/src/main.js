import PropTypes from 'prop-types'
import {
  actionEmitter,
  appFactory,
  errorLogging,
  externalEvents,
  notification,
  selection as selectionPropType
} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import ResourceSchedulerContainer from './containers/ResourceSchedulerContainer'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'resource-scheduler'

const EXTERNAL_EVENTS = [
  /**
   * Fired when an event in the calendar gets clicked
   *
   * Payload:
   * - `id`: id of source entity
   * - `model`: name of the entity
   */
  'onEventClick'
]

const initApp = (id, input, events, publicPath) => {
  const content = <ResourceSchedulerContainer />
  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store, state => state.input.emitAction)

  const handleNotifications = !events.emitAction

  notification.addToStore(store, handleNotifications)
  errorLogging.addToStore(store, handleNotifications, ['console', 'remote', 'notification'])

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input, handleNotifications),
    publicPath,
    textResourceModules: ['component', 'common', packageName, 'entity-list', 'scheduler']
  })
}

;(() => {
  if (__PACKAGE_NAME__ === 'resource-scheduler') {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const {store, component} = initApp('dev', input, {})

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const ResourceSchedulerApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName: props.id, externalEvents: EXTERNAL_EVENTS})
  return component
}

ResourceSchedulerApp.propTypes = {
  id: PropTypes.string,
  /**
   * ISO Language Code
   */
  locale: PropTypes.string,
  onEventClick: PropTypes.func,
  selection: selectionPropType.propType,
  actionProperties: PropTypes.shape({
    calendarType: PropTypes.string
  })
}

export default ResourceSchedulerApp
export const app = appFactory.createBundleableApp(packageName, initApp, ResourceSchedulerApp)
