import PropTypes from 'prop-types'
import {SignalList} from 'tocco-ui'

const ValidationErrors = ({errors}) => {
  const validationErrors = errors.map(({entityValidatorErrors}) =>
    Object.keys(entityValidatorErrors).map(validator => (
      <SignalList.List key={validator}>
        {entityValidatorErrors[validator].map((msg, index) => (
          <SignalList.Item key={`entity-${validator}-${index}`} label={msg} />
        ))}
      </SignalList.List>
    ))
  )

  return <>{validationErrors}</>
}

ValidationErrors.propTypes = {
  errors: PropTypes.array
}

export default ValidationErrors
