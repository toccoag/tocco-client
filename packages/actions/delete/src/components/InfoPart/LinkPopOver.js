import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Popover, Typography} from 'tocco-ui'

import {deleteEntityPropType} from '../../utils/deleteRequestParser'

const LinkPopOver = ({deleteEntityProp, children, maxCountLink}) => {
  if (
    deleteEntityProp.keys.length < maxCountLink &&
    (!deleteEntityProp.keysOtherBu || deleteEntityProp.keysOtherBu.length === 0)
  ) {
    return children
  }

  const content = (
    <>
      {deleteEntityProp.keys.length > maxCountLink && (
        <Typography.P>
          <FormattedMessage id="client.delete.tooManyRecords" />
        </Typography.P>
      )}
      {deleteEntityProp.keysOtherBu?.length > 0 && (
        <Typography.P>
          <FormattedMessage id="client.delete.recordInOtherBU" values={{count: deleteEntityProp.keysOtherBu.length}} />
        </Typography.P>
      )}
    </>
  )

  return (
    <Popover content={content} placement="top">
      {children}
    </Popover>
  )
}

LinkPopOver.propTypes = {
  deleteEntityProp: deleteEntityPropType.isRequired,
  maxCountLink: PropTypes.number.isRequired,
  children: PropTypes.element
}

export default LinkPopOver
