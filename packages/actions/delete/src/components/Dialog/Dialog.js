import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Button, SignalBox, Typography, useOnEnterHandler} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import {deleteInfoPropType} from '../../utils/deleteRequestParser'
import InfoPart from '../InfoPart'

import {StyledButtonsWrapper, StyledSectionWrapper} from './StyledComponents'

const Dialog = ({
  dialogInfo: {
    rootEntitiesDeletable,
    rootEntitiesNotDeletable,
    relatedDeletable,
    relatedNotDeletable,
    hasUnreadableEntities,
    limitExceeded
  },
  doDelete,
  onCancel,
  navigationStrategy
}) => {
  useOnEnterHandler(onCancel)

  if (limitExceeded) {
    return (
      <Typography.P>
        <FormattedMessage id="client.delete.limitExceededText" />
      </Typography.P>
    )
  }

  return (
    <>
      {Object.keys(rootEntitiesDeletable).length > 0 && (
        <StyledSectionWrapper>
          <Typography.P>
            <FormattedMessage id="client.delete.confirmText" />
          </Typography.P>
          <InfoPart
            key="infopart-deletable"
            rootEntities={rootEntitiesDeletable}
            relatedEntities={relatedDeletable}
            maxCountLink={100}
            navigationStrategy={navigationStrategy}
          />
        </StyledSectionWrapper>
      )}
      {Object.keys(rootEntitiesNotDeletable).length > 0 && (
        <StyledSectionWrapper>
          <Typography.P>
            <FormattedMessage id="client.delete.textNotDeletable" />
          </Typography.P>
          <InfoPart
            key="infopart-notdeletable"
            rootEntities={rootEntitiesNotDeletable}
            relatedEntities={relatedNotDeletable}
            maxCountLink={100}
            navigationStrategy={navigationStrategy}
          />
        </StyledSectionWrapper>
      )}
      {Object.keys(rootEntitiesDeletable).length === 0 && Object.keys(rootEntitiesNotDeletable).length === 0 && (
        <StyledSectionWrapper>
          <Typography.P>
            <FormattedMessage id="client.delete.errorMessage" />
          </Typography.P>
        </StyledSectionWrapper>
      )}
      {hasUnreadableEntities && (
        <StyledSectionWrapper>
          <SignalBox condition="warning">
            <Typography.Span>
              <FormattedMessage id="client.delete.unreadableEntities" />
            </Typography.Span>
          </SignalBox>
        </StyledSectionWrapper>
      )}
      <StyledSectionWrapper>
        <StyledButtonsWrapper>
          <Button
            onClick={doDelete}
            disabled={Object.keys(rootEntitiesDeletable).every(
              entity => rootEntitiesDeletable[entity].keys.length === 0
            )}
            data-cy="btn-delete-ok"
          >
            <FormattedMessage id="client.delete.deleteButton" />
          </Button>
          <Button ink="primary" look="raised" onClick={onCancel} data-cy="btn-delete-cancel">
            <FormattedMessage id="client.delete.cancelButton" />
          </Button>
        </StyledButtonsWrapper>
      </StyledSectionWrapper>
    </>
  )
}
Dialog.propTypes = {
  onCancel: PropTypes.func.isRequired,
  doDelete: PropTypes.func.isRequired,
  dialogInfo: deleteInfoPropType.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export default Dialog
