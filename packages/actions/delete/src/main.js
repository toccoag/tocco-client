import PropTypes from 'prop-types'
import {actionEmitter, appFactory, externalEvents, notification, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import Delete from './components/Delete'
import reducers, {sagas} from './modules/reducers'

const packageName = 'delete'

const EXTERNAL_EVENTS = ['onSuccess', 'onCancel', 'onError', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <Delete />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__DEV__ && __PACKAGE_NAME__ === packageName) {
    const input = require('./dev/input.json')

    const {store, component} = initApp(packageName, input)

    if (module.hot) {
      module.hot.accept('./modules/reducers', () => {
        const hotReducers = require('./modules/reducers').default
        reducerUtil.hotReloadReducers(store, hotReducers)
      })
    }

    appFactory.renderApp(component)
  } else {
    appFactory.registerAppInRegistry(packageName, initApp)
  }
})()

const DeleteApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

DeleteApp.propTypes = {
  selection: selection.propType.isRequired,
  /**
   * Allows configuration of a special endpoint e.g. to delete special entities (Default: standard delete endpoint)
   */
  customDeleteEndpoint: PropTypes.string
}

export default DeleteApp
export const app = appFactory.createBundleableApp(packageName, initApp, DeleteApp)
