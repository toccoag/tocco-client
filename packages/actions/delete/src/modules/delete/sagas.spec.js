import {takeLatest} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import {call, select} from 'redux-saga-test-plan/matchers'
import {rest, externalEvents, selection as selectionUtil} from 'tocco-app-extensions'

import {getDialogInfo, getEntitiesToDelete} from '../../utils/deleteRequestParser'

import * as actions from './actions'
import mainSaga, * as sagas from './sagas'

describe('delete', () => {
  describe('modules', () => {
    describe('sagas', () => {
      describe('mainSaga', () => {
        test('should fork sagas', () => {
          const saga = testSaga(mainSaga)
          saga
            .next()
            .all([
              takeLatest(actions.LOAD_DIALOG_INFO, sagas.loadDialogInfo),
              takeLatest(actions.DO_DELETE, sagas.doDelete),
              takeLatest(actions.ON_CANCEL, sagas.onCancel)
            ])
        })
      })

      describe('getDeleteEndpoint', () => {
        test('should return custom endpoint if set', () => {
          const input = {customDeleteEndpoint: 'custom/delete'}
          return expectSaga(sagas.getDeleteEndpoint)
            .provide([[select(sagas.inputSelector), input]])
            .returns('custom/delete')
            .run()
        })

        test('should return default endpoint', () => {
          const input = {}
          return expectSaga(sagas.getDeleteEndpoint)
            .provide([[select(sagas.inputSelector), input]])
            .returns('client/delete')
            .run()
        })
      })

      describe('doDelete', () => {
        const entitiesToDelete = {
          entityName: 'Registration',
          keys: ['179', '88', '89']
        }
        const input = {customDeleteEndpoint: 'custom/delete'}
        const textResources = {
          'client.delete.successfullyMessage': 'OK',
          'client.delete.validationErrorMessage': 'VALIDATION ERROR',
          'client.delete.errorMessage': 'NOT OK'
        }
        const requestOptions = {
          method: 'POST',
          body: {
            entityModel: 'Registration',
            keys: ['179', '88', '89']
          },
          acceptedErrorCodes: ['VALIDATION_FAILED'],
          acceptedStatusCodes: [500, 400, 409]
        }

        test('should delete entites and fire onSuccess event', () => {
          const response = {ok: true, body: {deletedEntities: {Registration: ['179', '88', '89']}}}
          return expectSaga(sagas.doDelete)
            .provide([
              [select(sagas.entitiesToDeleteSelector), entitiesToDelete],
              [select(sagas.inputSelector), input],
              [select(sagas.textResourceSelector), textResources],
              [call(rest.requestSaga, input.customDeleteEndpoint, requestOptions), response]
            ])
            .put(actions.setDeletingInProgress(true))
            .put(
              externalEvents.fireExternalEvent('onSuccess', {
                message: 'OK',
                remoteEvents: [
                  {
                    type: 'entity-delete-event',
                    payload: {
                      entities: [
                        {entityName: 'Registration', key: '179'},
                        {entityName: 'Registration', key: '88'},
                        {entityName: 'Registration', key: '89'}
                      ]
                    }
                  }
                ]
              })
            )
            .run()
        })

        test('should show validation error on error toaster', () => {
          const response = {
            ok: false,
            status: 400,
            body: {
              errorCode: 'VALIDATION_FAILED',
              errors: [{model: 'Event', entityValidationErrors: [{SomeValidator: ['Fehlgeschlagen']}]}]
            }
          }
          return expectSaga(sagas.doDelete)
            .provide([
              [select(sagas.entitiesToDeleteSelector), entitiesToDelete],
              [select(sagas.inputSelector), input],
              [select(sagas.textResourceSelector), textResources],
              [call(rest.requestSaga, input.customDeleteEndpoint, requestOptions), response]
            ])
            .put(actions.setDeletingInProgress(true))
            .put.actionType('externalEvents/FIRE_EXTERNAL_EVENT')
            .run()
        })

        test('should handle conflict error properly', () => {
          const response = {
            ok: false,
            status: 409,
            body: {information: 'conflict'}
          }
          return expectSaga(sagas.doDelete)
            .provide([
              [select(sagas.entitiesToDeleteSelector), entitiesToDelete],
              [select(sagas.inputSelector), input],
              [select(sagas.textResourceSelector), textResources],
              [call(rest.requestSaga, input.customDeleteEndpoint, requestOptions), response]
            ])
            .put(actions.setDeletingInProgress(true))
            .put(externalEvents.fireExternalEvent('onError', {message: 'conflict'}))
            .run()
        })

        test('should handle error properly', () => {
          const response = {
            ok: false,
            status: 500,
            body: {}
          }
          return expectSaga(sagas.doDelete)
            .provide([
              [select(sagas.entitiesToDeleteSelector), entitiesToDelete],
              [select(sagas.inputSelector), input],
              [select(sagas.textResourceSelector), textResources],
              [call(rest.requestSaga, input.customDeleteEndpoint, requestOptions), response]
            ])
            .put(actions.setDeletingInProgress(true))
            .put(externalEvents.fireExternalEvent('onError', {message: 'NOT OK'}))
            .run()
        })
      })

      describe('loadDialogInfo', () => {
        test('should load info', () => {
          const entityModel = 'Model'
          const currentBusinessUnitId = 'bu1'
          const keys = ['1', '2', '3']
          const selection = {keys}
          const entities = {entityName: entityModel, keys}
          const input = {selection}
          const principal = {currentBusinessUnit: {id: currentBusinessUnitId}}
          // objects only used to check if values get passed correctly
          const deleteResponse = {body: {id: 'response'}, ok: true}
          const dialogInfo = {id: 'dialog info'}
          const entitiesToDelete = {id: 'entities to delete'}
          return expectSaga(sagas.loadDialogInfo)
            .provide([
              [call(selectionUtil.getEntities, selection, rest.fetchAllEntities), entities],
              [select(sagas.inputSelector), input],
              [
                call(rest.requestSaga, 'client/delete/dialog', {
                  method: 'POST',
                  body: {entityModel, keys},
                  acceptedStatusCodes: [403]
                }),
                deleteResponse
              ],
              [call(rest.fetchPrincipal), principal],
              [call(getDialogInfo, deleteResponse.body, currentBusinessUnitId), dialogInfo],
              [call(getEntitiesToDelete, deleteResponse.body), entitiesToDelete]
            ])
            .put(actions.setDeleteDialogInfo(dialogInfo))
            .put(actions.setEntitiesToDelete(entitiesToDelete))
            .run()
        })
        test('should set empty dialog on error response', () => {
          const entityModel = 'Model'
          const keys = ['1', '2', '3']
          const selection = {keys}
          const entities = {entityName: entityModel, keys}
          const input = {selection}
          const deleteResponse = {ok: false}
          return expectSaga(sagas.loadDialogInfo)
            .provide([
              [call(selectionUtil.getEntities, selection, rest.fetchAllEntities), entities],
              [select(sagas.inputSelector), input],
              [
                call(rest.requestSaga, 'client/delete/dialog', {
                  method: 'POST',
                  body: {entityModel, keys},
                  acceptedStatusCodes: [403]
                }),
                deleteResponse
              ]
            ])
            .put(
              actions.setDeleteDialogInfo({
                rootEntitiesDeletable: {},
                rootEntitiesNotDeletable: {},
                relatedDeletable: {},
                relatedNotDeletable: {},
                hasUnreadableEntities: false
              })
            )
            .run()
        })
      })
    })
  })
})
