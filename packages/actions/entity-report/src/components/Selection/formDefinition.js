import {form} from 'tocco-app-extensions'

export const createSimpleFormDefinition = (intl, reports) => {
  const msg = id => intl.formatMessage({id})

  const formDefinition = form.createSimpleForm({
    children: [
      form.createHorizontalBox({
        children: [
          form.createVerticalBox({
            children: [
              form.createFieldSet({
                path: 'selectedReport',
                dataType: 'single-select-box',
                label: msg('client.entity-report.label'),
                additionalFieldAttributes: {
                  options: reports
                },
                validation: form.createValidation(form.createMandatoryValidation())
              })
            ]
          })
        ]
      })
    ]
  })

  return formDefinition
}
