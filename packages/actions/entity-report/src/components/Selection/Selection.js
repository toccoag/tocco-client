import PropTypes from 'prop-types'
import {useEffect} from 'react'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {LoadMask, useOnEnterHandler} from 'tocco-ui'

import {createSimpleFormDefinition} from './formDefinition'

const Selection = ({intl, loadReports, openReportAction, reports, selectedReport, setSelectedReport}) => {
  const msg = id => intl.formatMessage({id})

  useEffect(() => {
    loadReports()
  }, [loadReports])

  const handleEnter = () => openReportAction(selectedReport.values.selectedReport.key)
  useOnEnterHandler(handleEnter)

  return (
    <LoadMask required={[reports]}>
      <SimpleFormApp
        form={createSimpleFormDefinition(intl, reports)}
        labelPosition="inside"
        mode="create"
        onChange={setSelectedReport}
        onSubmit={handleEnter}
        submitText={msg('client.entity-report.next')}
        validate
      />
    </LoadMask>
  )
}

Selection.propTypes = {
  intl: PropTypes.object.isRequired,
  loadReports: PropTypes.func.isRequired,
  openReportAction: PropTypes.func.isRequired,
  reports: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string,
      display: PropTypes.string
    })
  ),
  selectedReport: PropTypes.shape({
    key: PropTypes.string,
    display: PropTypes.string,
    values: PropTypes.shape({
      selectedReport: PropTypes.shape({
        key: PropTypes.string
      })
    })
  }),
  setSelectedReport: PropTypes.func.isRequired
}

export default Selection
