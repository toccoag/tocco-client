import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import Result from './Result'

describe('create-api-key', () => {
  describe('components', () => {
    describe('Result', () => {
      const apiKey = '123456789'
      const apiName = 'test_name'

      test('Test render', async () => {
        testingLibrary.renderWithIntl(<Result intl={IntlStub} apiKey={apiKey} apiName={apiName} />)

        expect(screen.getAllByText('client.create-api-key.success-info')).to.have.length(1)
        expect(screen.getAllByText('client.create-api-key.apiName')).to.have.length(1)
        expect(screen.getAllByText('client.create-api-key.apiKey')).to.have.length(1)

        expect(screen.getAllByText(apiKey)).to.exist
        expect(screen.getAllByText(apiName)).to.exist
      })
    })
  })
})
