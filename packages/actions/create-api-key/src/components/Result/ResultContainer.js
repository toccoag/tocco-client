import {connect} from 'react-redux'

import Result from './Result'

const mapActionCreator = {}

const mapStateToProps = state => ({
  apiKey: state.createApiKey.apiKey,
  apiName: state.createApiKey.apiKeyName
})

export default connect(mapStateToProps, mapActionCreator)(Result)
