import PropTypes from 'prop-types'
import {Button} from 'tocco-ui'
import {js} from 'tocco-util'

const CopyIcon = ({content}) => {
  const copy = () => {
    js.copyToClipboard(content)
  }

  return <Button icon="copy" onClick={copy} iconOnly={true} withoutBackground={true} />
}

CopyIcon.propTypes = {
  content: PropTypes.string
}

export default CopyIcon
