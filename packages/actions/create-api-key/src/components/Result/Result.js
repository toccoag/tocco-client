import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Typography, LoadMask} from 'tocco-ui'

import CopyIcon from './CopyIcon'

function Result({apiKey, apiName}) {
  return (
    <LoadMask required={[apiKey, apiName]}>
      <Typography.P>
        <FormattedMessage id={'client.create-api-key.success-info'} />
      </Typography.P>

      <Typography.P>
        <Typography.B>
          <FormattedMessage id={'client.create-api-key.apiName'} />{' '}
        </Typography.B>
        {apiName}
      </Typography.P>

      <Typography.P>
        <Typography.B>
          <FormattedMessage id={'client.create-api-key.apiKey'} />{' '}
        </Typography.B>
        {apiKey}
        <CopyIcon content={apiKey} />
      </Typography.P>
    </LoadMask>
  )
}

Result.propTypes = {
  apiKey: PropTypes.string,
  apiName: PropTypes.string
}

export default Result
