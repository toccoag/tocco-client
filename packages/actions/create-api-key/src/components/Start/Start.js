import PropTypes from 'prop-types'
import {useEffect, useRef, useState} from 'react'
import {EditableValue, StatedValue, Typography, Button, useOnEnterHandler} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import {StyledButtonWrapper} from './StyledComponents'

function Start({fetchApiKeyRequest, setApiKeyName, setShowKey, intl}) {
  const [apiName, setApiName] = useState('')
  const [buttonDisabled, setButtonDisabled] = useState(true)
  const msg = id => intl.formatMessage({id})

  const handleButtonClick = () => {
    setApiKeyName(apiName)
    fetchApiKeyRequest()
    setShowKey(true)
  }
  useOnEnterHandler(handleButtonClick)

  useEffect(() => {
    setButtonDisabled(apiName?.trim().length === 0)
  }, [apiName])

  const divEl = useRef(null)
  customHooks.useAutofocus(divEl)

  return (
    <div ref={divEl}>
      <Typography.P>{msg('client.create-api-key.info')}</Typography.P>

      <StatedValue label={msg('client.create-api-key.apiName')}>
        <EditableValue value={apiName} type="string" id="api-key-name" events={{onChange: setApiName}} />
      </StatedValue>

      <StyledButtonWrapper>
        <Button
          label={msg('client.create-api-key.createButton')}
          ink="primary"
          look="raised"
          onClick={handleButtonClick}
          disabled={buttonDisabled}
        />
      </StyledButtonWrapper>
    </div>
  )
}

Start.propTypes = {
  intl: PropTypes.object.isRequired,
  fetchApiKeyRequest: PropTypes.func.isRequired,
  setApiKeyName: PropTypes.func.isRequired,
  setShowKey: PropTypes.func.isRequired
}

export default Start
