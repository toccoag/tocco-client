import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {fetchApiKeyRequest, setApiKeyName, setShowKey} from '../../modules/createApiKey/actions'

import Start from './Start'

const mapActionCreator = {
  fetchApiKeyRequest,
  setApiKeyName,
  setShowKey
}

const mapStateToProps = state => ({})

export default connect(mapStateToProps, mapActionCreator)(injectIntl(Start))
