import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import Start from './Start'

describe('create-api-key', () => {
  describe('components', () => {
    describe('Start', () => {
      const fetchApiKeyRequest = sinon.spy()
      const setApiKeyName = sinon.spy()
      const toggleShowKey = sinon.spy()

      test('Test render', async () => {
        testingLibrary.renderWithIntl(
          <Start
            intl={IntlStub}
            fetchApiKeyRequest={fetchApiKeyRequest}
            setApiKeyName={setApiKeyName}
            toggleShowKey={toggleShowKey}
          />
        )

        expect(screen.getAllByText('client.create-api-key.info')).to.have.length(1)
        expect(screen.getAllByText('client.create-api-key.apiName')).to.have.length(1)
        expect(screen.getAllByText('client.create-api-key.createButton')).to.have.length(1)
      })
    })
  })
})
