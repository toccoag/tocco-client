import PropTypes from 'prop-types'

import Result from '../Result'
import Start from '../Start'

const CreateApiKey = ({showKey}) => {
  return <>{showKey ? <Result /> : <Start />}</>
}

CreateApiKey.propTypes = {
  showKey: PropTypes.bool.isRequired
}

export default CreateApiKey
