import {connect} from 'react-redux'

import CreateApiKey from './CreateApiKey'

const mapActionCreators = {}

const mapStateToProps = state => ({
  showKey: state.createApiKey.showKey
})

export default connect(mapStateToProps, mapActionCreators)(CreateApiKey)
