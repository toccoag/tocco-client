export const FETCH_API_KEY_REQUEST = 'create-api-key/FETCH_API_KEY_REQUEST'
export const SET_API_KEY = 'create-api-key/SET_API_KEY'
export const SET_SHOW_KEY = 'create-api-key/SET_SHOW_KEY'
export const SET_API_KEY_NAME = 'create-api-key/SET_API_KEY_NAME'

export const fetchApiKeyRequest = () => ({
  type: FETCH_API_KEY_REQUEST
})

export const setApiKey = apiKey => ({
  type: SET_API_KEY,
  payload: {apiKey}
})

export const setShowKey = showKey => ({
  type: SET_SHOW_KEY,
  payload: {showKey}
})

export const setApiKeyName = apiKeyName => ({
  type: SET_API_KEY_NAME,
  payload: {apiKeyName}
})
