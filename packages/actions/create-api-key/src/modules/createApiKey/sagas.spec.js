import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest, notification} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('set-api-key', () => {
  describe('modules', () => {
    describe('sagas', () => {
      test('main saga', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([takeEvery(actions.FETCH_API_KEY_REQUEST, sagas.fetchApiKeyRequest)])
        )
        expect(generator.next().done).to.be.true
      })

      describe('fetchApiKeyRequest', () => {
        const keyName = 'xx'
        const selection = {
          entityName: 'Principal',
          type: 'ID',
          ids: ['13400'],
          count: 1
        }

        const resource = `/usermanager/actions/createApiKey/${keyName}`
        const options = {
          method: 'POST',
          body: selection
        }
        const body = {apiKey: 'test_name'}

        test('setApiKey', () => {
          return expectSaga(sagas.fetchApiKeyRequest)
            .provide([
              [select(sagas.inputSelector), {selection}],
              [select(sagas.createApiKeySelector), keyName],
              [matchers.call(rest.requestSaga, resource, options), {body}]
            ])
            .put(actions.setApiKey(body.apiKey))
            .run()
        })

        test('failure', () => {
          return expectSaga(sagas.fetchApiKeyRequest)
            .provide([
              [select(sagas.inputSelector), {selection}],
              [select(sagas.createApiKeySelector), keyName],
              [matchers.call(rest.requestSaga, resource, options), throwError(new Error('Failed to fetch data'))]
            ])
            .put(
              notification.toaster({
                type: 'error',
                title: 'client.create-api-key.fetchApiKeyFailure.title',
                body: 'client.create-api-key.fetchApiKeyFailure.info'
              })
            )
            .run()
        })
      })
    })
  })
})
