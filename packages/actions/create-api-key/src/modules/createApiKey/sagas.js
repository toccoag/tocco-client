import {call, takeEvery, select, all, put} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input
export const createApiKeySelector = state => state.createApiKey.apiKeyName

export function* fetchApiKeyRequest() {
  const {selection} = yield select(inputSelector)
  const keyName = yield select(createApiKeySelector)

  try {
    const response = yield call(rest.requestSaga, `/usermanager/actions/createApiKey/${keyName}`, {
      method: 'POST',
      body: selection
    })
    yield put(actions.setApiKey(response.body.apiKey))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.create-api-key.fetchApiKeyFailure.title',
        body: 'client.create-api-key.fetchApiKeyFailure.info'
      })
    )
  }
}

export default function* sagas() {
  yield all([takeEvery(actions.FETCH_API_KEY_REQUEST, fetchApiKeyRequest)])
}
