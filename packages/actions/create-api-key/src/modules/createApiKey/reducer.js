import {reducer as reducerUtil} from 'tocco-util'

import {SET_SHOW_KEY, SET_API_KEY_NAME, SET_API_KEY} from './actions'

const initialState = {
  showKey: false,
  apiKeyName: undefined,
  apiKey: undefined
}

const ACTION_HANDLERS = {
  [SET_API_KEY]: reducerUtil.singleTransferReducer('apiKey'),
  [SET_SHOW_KEY]: reducerUtil.singleTransferReducer('showKey'),
  [SET_API_KEY_NAME]: reducerUtil.singleTransferReducer('apiKeyName')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
