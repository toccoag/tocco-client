import createApiKey, {sagas as createApiKeySagas} from './createApiKey'

export default {
  createApiKey
}

export const sagas = [createApiKeySagas]
