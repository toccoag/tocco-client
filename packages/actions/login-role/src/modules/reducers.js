import {reducer as form} from 'redux-form'

import loginRolesReducer from './loginRoles/reducer'
import loginRolesSagas from './loginRoles/sagas'

export default {
  loginRoles: loginRolesReducer,
  form
}

export const sagas = [loginRolesSagas]
