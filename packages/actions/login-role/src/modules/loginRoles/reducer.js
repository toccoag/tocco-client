import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  initialized: false,
  roles: [],
  roleTypes: [],
  businessUnits: [],
  initiallySelectedRoles: [],
  selectedRoles: [],
  saving: false
}

const setInitialized = state => {
  return {
    ...state,
    initialized: true
  }
}

const ACTION_HANDLERS = {
  [actions.SET_INITIALIZED]: setInitialized,
  [actions.SET_ROLES]: reducerUtil.singleTransferReducer('roles'),
  [actions.SET_ROLE_TYPES]: reducerUtil.singleTransferReducer('roleTypes'),
  [actions.SET_BUSINESS_UNITS]: reducerUtil.singleTransferReducer('businessUnits'),
  [actions.SET_INITIALLY_SELECTED_ROLES]: reducerUtil.singleTransferReducer('initiallySelectedRoles'),
  [actions.SET_SELECTED_ROLES]: reducerUtil.singleTransferReducer('selectedRoles'),
  [actions.SET_SAVING]: reducerUtil.singleTransferReducer('saving')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
