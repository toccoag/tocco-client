export const INITIALIZE = 'loginRoles/INITIALIZE'
export const SET_INITIALIZED = 'loginRoles/SET_INITIALIZED'
export const SET_ROLES = 'loginRoles/SET_ROLES'
export const SET_ROLE_TYPES = 'loginRoles/SET_ROLE_TYPES'
export const SET_BUSINESS_UNITS = 'loginRoles/SET_BUSINESS_UNITS'
export const SET_INITIALLY_SELECTED_ROLES = 'loginRoles/SET_INITIALLY_SELECTED_ROLES'
export const SET_SELECTED_ROLES = 'loginRoles/SET_SELECTED_ROLES'
export const RESET_SEARCH_FORM = 'loginRoles/RESET_SEARCH_FORM'
export const CANCEL = 'loginRoles/CANCEL'
export const SAVE = 'loginRoles/SAVE'
export const SET_SAVING = 'loginRoles/SET_SAVING'

export const initialize = () => ({
  type: INITIALIZE
})

export const setInitialized = () => ({
  type: SET_INITIALIZED
})

export const setRoles = roles => ({
  type: SET_ROLES,
  payload: {
    roles
  }
})

export const setRoleTypes = roleTypes => ({
  type: SET_ROLE_TYPES,
  payload: {
    roleTypes
  }
})

export const setBusinessUnits = businessUnits => ({
  type: SET_BUSINESS_UNITS,
  payload: {
    businessUnits
  }
})

export const setInitiallySelectedRoles = initiallySelectedRoles => ({
  type: SET_INITIALLY_SELECTED_ROLES,
  payload: {
    initiallySelectedRoles
  }
})

export const setSelectedRoles = selectedRoles => ({
  type: SET_SELECTED_ROLES,
  payload: {
    selectedRoles
  }
})

export const resetSearchForm = () => ({
  type: RESET_SEARCH_FORM
})

export const cancel = () => ({
  type: CANCEL
})

export const save = () => ({
  type: SAVE
})

export const setSaving = saving => ({
  type: SET_SAVING,
  payload: {
    saving
  }
})
