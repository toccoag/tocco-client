import {actions as formActions} from 'redux-form'
import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import {externalEvents, rest, selection as selectionUtil} from 'tocco-app-extensions'

import {REDUX_SEARCH_FORM_NAME} from '../../components/util/selectors'

import * as actions from './actions'

export const inputSelector = state => state.input
export const loginRolesSelector = state => state.loginRoles

export function* initialize() {
  const principalKey = yield call(getPrincipalKey)
  yield all([
    call(fetchAvailableRoles, principalKey),
    call(fetchSelectedRoles, principalKey),
    call(fetchAvailableBusinessUnits, principalKey),
    call(fetchRoleTypes)
  ])
  yield put(formActions.initialize(REDUX_SEARCH_FORM_NAME, {showSelected: false}))
  yield put(actions.setInitialized())
}

function* getPrincipalKey() {
  const {selection} = yield select(inputSelector)
  return selectionUtil.getSingleKey(selection, 'Principal')
}

function* fetchAvailableRoles(principalKey) {
  const {
    body: {roles}
  } = yield call(rest.requestSaga, `principals/${principalKey}/available-roles`)
  yield put(actions.setRoles(roles))
}

function* fetchSelectedRoles(principalKey) {
  const {
    body: {loginRoles}
  } = yield call(rest.requestSaga, `principals/${principalKey}/selected-roles`)
  yield put(actions.setInitiallySelectedRoles(loginRoles))
  yield put(actions.setSelectedRoles(loginRoles))
}

function* fetchAvailableBusinessUnits(principalKey) {
  const {
    body: {businessUnits}
  } = yield call(rest.requestSaga, `principals/${principalKey}/available-business-units`)
  yield put(actions.setBusinessUnits(businessUnits))
}

function* fetchRoleTypes() {
  const roleTypes = yield call(
    rest.fetchAllEntities,
    'Role_type',
    {
      sorting: [
        {field: 'sorting', order: 'asc'},
        {field: 'label', order: 'asc'}
      ],
      paths: ['label']
    },
    {method: 'GET'},
    response =>
      response.data.map(entity => ({
        key: entity.key,
        label: entity.paths.label.value
      }))
  )
  yield put(actions.setRoleTypes(roleTypes))
}

export function* resetSearchForm() {
  yield put(formActions.reset(REDUX_SEARCH_FORM_NAME))
}

export function* cancel() {
  yield put(externalEvents.fireExternalEvent('onCancel'))
}

export function* save() {
  yield put(actions.setSaving(true))
  const principalKey = yield call(getPrincipalKey)
  const updates = yield call(collectUpdates)
  const options = {
    method: 'PATCH',
    body: updates
  }
  const response = yield call(rest.requestSaga, `principals/${principalKey}/selected-roles`, options)
  if (response.ok) {
    yield put(externalEvents.fireExternalEvent('onSuccess'))
  } else {
    yield put(externalEvents.fireExternalEvent('onError'))
  }
  yield put(actions.setSaving(false))
}

function* collectUpdates() {
  const {initiallySelectedRoles, selectedRoles} = yield select(loginRolesSelector)

  const updates = []

  const initiallySelectedMap = new Map(
    initiallySelectedRoles.map(item => [`${item.roleKey}_${item.businessUnitKey}`, item])
  )
  const selectedMap = new Map(selectedRoles.map(item => [`${item.roleKey}_${item.businessUnitKey}`, item]))

  // Check for roles in initiallySelectedRoles but not in selectedRoles (deselected roles)
  initiallySelectedRoles.forEach(item => {
    const key = `${item.roleKey}_${item.businessUnitKey}`
    if (!selectedMap.has(key)) {
      updates.push({roleKey: item.roleKey, buKey: item.businessUnitKey, selected: false})
    }
  })

  // Check for roles in selectedRoles but not in initiallySelectedRoles (newly selected roles)
  selectedRoles.forEach(item => {
    const key = `${item.roleKey}_${item.businessUnitKey}`
    if (!initiallySelectedMap.has(key)) {
      updates.push({roleKey: item.roleKey, buKey: item.businessUnitKey, selected: true})
    }
  })

  return updates
}

export default function* sagas() {
  yield all([
    takeEvery(actions.INITIALIZE, initialize),
    takeEvery(actions.RESET_SEARCH_FORM, resetSearchForm),
    takeEvery(actions.CANCEL, cancel),
    takeEvery(actions.SAVE, save)
  ])
}
