import {actions as formActions} from 'redux-form'
import {select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, externalEvents} from 'tocco-app-extensions'

import {REDUX_SEARCH_FORM_NAME} from '../../components/util/selectors'

import * as actions from './actions'
import * as sagas from './sagas'

describe('login-role', () => {
  describe('sagas', () => {
    describe('initialize', () => {
      it('should load and put data', () => {
        const expectedRoles = [{key: 'role'}]
        const expectedLoginRoles = [{key: 'login role'}]
        const expectedBusinessUnits = [{key: 'business unit'}]
        const expectedRoleTypes = [{key: 'role type'}]
        const principalKey = '1'

        return expectSaga(sagas.initialize)
          .provide([
            [select(sagas.inputSelector), {selection: {ids: [principalKey], entityName: 'Principal', type: 'ID'}}],
            [
              matchers.call(rest.requestSaga, `principals/${principalKey}/available-roles`),
              {body: {roles: expectedRoles}}
            ],
            [
              matchers.call(rest.requestSaga, `principals/${principalKey}/selected-roles`),
              {body: {loginRoles: expectedLoginRoles}}
            ],
            [
              matchers.call(rest.requestSaga, `principals/${principalKey}/available-business-units`),
              {body: {businessUnits: expectedBusinessUnits}}
            ],
            [matchers.call.fn(rest.fetchAllEntities), expectedRoleTypes]
          ])
          .put(actions.setRoles(expectedRoles))
          .put(actions.setInitiallySelectedRoles(expectedLoginRoles))
          .put(actions.setSelectedRoles(expectedLoginRoles))
          .put(actions.setBusinessUnits(expectedBusinessUnits))
          .put(actions.setRoleTypes(expectedRoleTypes))
          .put(formActions.initialize(REDUX_SEARCH_FORM_NAME, {showSelected: false}))
          .put(actions.setInitialized())
          .run()
      })
    })

    describe('resetSearchForm', () => {
      it('should put form action', () => {
        return expectSaga(sagas.resetSearchForm).put(formActions.reset(REDUX_SEARCH_FORM_NAME)).run()
      })
    })

    describe('cancel', () => {
      it('should fire external event', () => {
        return expectSaga(sagas.cancel).put(externalEvents.fireExternalEvent('onCancel')).run()
      })
    })

    describe('save', () => {
      it('should send updates and fire onSuccess', () => {
        const principalKey = '1'
        const initiallySelectedRoles = [
          {roleKey: 'initial role deselect', businessUnitKey: 'initial business unit deselect'},
          {roleKey: 'initial role keep', businessUnitKey: 'initial business unit keep'}
        ]
        const selectedRoles = [
          {roleKey: 'new role', businessUnitKey: 'new business unit'},
          {roleKey: 'initial role keep', businessUnitKey: 'initial business unit keep'}
        ]
        const updates = [
          {roleKey: 'initial role deselect', buKey: 'initial business unit deselect', selected: false},
          {roleKey: 'new role', buKey: 'new business unit', selected: true}
        ]
        const options = {
          method: 'PATCH',
          body: updates
        }

        return expectSaga(sagas.save)
          .provide([
            [select(sagas.inputSelector), {selection: {ids: [principalKey], entityName: 'Principal', type: 'ID'}}],
            [select(sagas.loginRolesSelector), {initiallySelectedRoles, selectedRoles}],
            [matchers.call.fn(rest.requestSaga), {ok: true}]
          ])
          .call(rest.requestSaga, `principals/${principalKey}/selected-roles`, options)
          .put(actions.setSaving(true))
          .put(externalEvents.fireExternalEvent('onSuccess'))
          .put(actions.setSaving(false))
          .run()
      })

      it('should fire onError if update failed', () => {
        const principalKey = '1'
        const initiallySelectedRoles = []
        const selectedRoles = []
        const updates = []
        const options = {
          method: 'PATCH',
          body: updates
        }

        return expectSaga(sagas.save)
          .provide([
            [select(sagas.inputSelector), {selection: {ids: [principalKey], entityName: 'Principal', type: 'ID'}}],
            [select(sagas.loginRolesSelector), {initiallySelectedRoles, selectedRoles}],
            [matchers.call.fn(rest.requestSaga), {ok: false}]
          ])
          .call(rest.requestSaga, `principals/${principalKey}/selected-roles`, options)
          .put(actions.setSaving(true))
          .put(externalEvents.fireExternalEvent('onError'))
          .put(actions.setSaving(false))
          .run()
      })
    })
  })
})
