import PropTypes from 'prop-types'
import {actionEmitter, appFactory, externalEvents, formData, notification, selection} from 'tocco-app-extensions'
import {consoleLogger, reducer as reducerUtil} from 'tocco-util'

import LoginRoleMatrix from './components/LoginRoleMatrix'
import reducers, {sagas} from './modules/reducers'

const packageName = 'login-role'

const EXTERNAL_EVENTS = ['onSuccess', 'onError', 'onCancel', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <LoginRoleMatrix />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({}))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const LoginRoleApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

LoginRoleApp.propTypes = {
  /**
   * Selection of a single `Principal` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}

export default LoginRoleApp
