import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {cancel, initialize, resetSearchForm, save, setSelectedRoles} from '../../modules/loginRoles/actions'
import {getFilteredBusinessUnits, getFilteredRoles, getHiddenSelectedCount} from '../util/selectors'

import LoginRoleMatrix from './LoginRoleMatrix'

const mapActionCreators = {
  initialize,
  setSelectedRoles,
  resetSearchForm,
  cancel,
  save
}

const mapStateToProps = state => {
  const filteredBusinessUnits = getFilteredBusinessUnits(state)
  const filteredRoles = getFilteredRoles(state)
  return {
    initialized: state.loginRoles.initialized,
    selectedRoles: state.loginRoles.selectedRoles,
    saving: state.loginRoles.saving,
    filteredBusinessUnits,
    filteredRoles,
    hiddenSelectedCount: getHiddenSelectedCount(state, filteredRoles, filteredBusinessUnits)
  }
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(LoginRoleMatrix))
