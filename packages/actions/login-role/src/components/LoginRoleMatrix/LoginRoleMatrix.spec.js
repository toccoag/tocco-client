import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {appFactory, formData} from 'tocco-app-extensions'
import {testingLibrary, IntlStub} from 'tocco-test-util'

import reducers, {sagas} from '../../modules/reducers'

import LoginRoleMatrix from './LoginRoleMatrix'

describe('login-role', () => {
  describe('components', () => {
    describe('LoginRoleMatrix', () => {
      it('should show load mask if not initialized', async () => {
        const initialize = sinon.spy()
        testingLibrary.renderWithIntl(
          <LoginRoleMatrix
            initialized={false}
            filteredBusinessUnits={[]}
            filteredRoles={[]}
            hiddenSelectedCount={0}
            initialize={initialize}
            setSelectedRoles={sinon.spy()}
            resetSearchForm={sinon.spy()}
            cancel={sinon.spy()}
            save={sinon.spy()}
            intl={IntlStub}
          />
        )

        await screen.findAllByTestId('icon-circle-notch')

        expect(initialize).to.have.been.calledOnce
      })

      it('should show matrix', async () => {
        const setSelectedRoles = sinon.spy()
        const save = sinon.spy()
        const cancel = sinon.spy()

        const store = appFactory.createStore(reducers, sagas, {})
        formData.addToStore(store, () => ({}))
        testingLibrary.renderWithStore(
          <LoginRoleMatrix
            initialized={true}
            filteredBusinessUnits={[
              {key: 'bu-1', label: 'business unit 1 label'},
              {key: 'bu-2', label: 'business unit 2 label'}
            ]}
            filteredRoles={[
              {key: 'role-1', label: 'role 1 label', roleTypeKey: '1'},
              {key: 'role-2', label: 'role 2 label', roleTypeKey: '1'},
              {key: 'role-3', label: 'role 3 label', roleTypeKey: '1', businessUnitKey: 'bu-2'}
            ]}
            hiddenSelectedCount={0}
            initialize={sinon.spy()}
            setSelectedRoles={setSelectedRoles}
            resetSearchForm={sinon.spy()}
            cancel={cancel}
            save={save}
            intl={IntlStub}
          />,
          {store}
        )

        await screen.findAllByTestId('icon')

        const user = userEvent.setup()

        expect(screen.getByText('client.actions.login-role.search.label')).to.exist

        const cancelButton = screen.getByText('client.actions.login-role.cancel')
        expect(cancelButton).to.exist
        await user.click(cancelButton)
        expect(cancel).to.have.been.calledOnce

        const saveButton = screen.getByText('client.actions.login-role.save')
        expect(saveButton).to.exist
        await user.click(saveButton)
        expect(save).to.have.been.calledOnce

        expect(screen.getByText('role 1 label')).to.exist
        expect(screen.getByText('role 2 label')).to.exist
        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes).to.have.length(4)
        expect(checkboxes.map(box => box.id)).to.include.members([
          'checkbox-all',
          'checkbox-role-1',
          'checkbox-role-2',
          'checkbox-role-3'
        ])
      })

      it('should show empty matrix', async () => {
        const store = appFactory.createStore(reducers, sagas, {})
        formData.addToStore(store, () => ({}))
        testingLibrary.renderWithStore(
          <LoginRoleMatrix
            initialized={true}
            filteredBusinessUnits={[]}
            filteredRoles={[]}
            hiddenSelectedCount={0}
            initialize={sinon.spy()}
            setSelectedRoles={sinon.spy()}
            resetSearchForm={sinon.spy()}
            cancel={sinon.spy()}
            save={sinon.spy()}
            intl={IntlStub}
          />,
          {store}
        )

        await screen.findAllByTestId('icon')

        expect(screen.getByText('client.actions.login-role.search.label')).to.exist
        expect(screen.getByText('client.actions.login-role.cancel')).to.exist
        expect(screen.getByText('client.actions.login-role.save')).to.exist
        expect(screen.getByText('client.actions.login-role.no-roles')).to.exist
      })
    })
  })
})
