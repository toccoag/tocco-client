import styled from 'styled-components'
import {declareFont, themeSelector, scale} from 'tocco-ui'

export const StyledHeaderSection = styled.div`
  position: sticky;
  top: 0;
  background-color: ${themeSelector.color('paper')};
  border-bottom: 1px solid ${themeSelector.color('border')};
`

export const StyledHiddenSelectionWarning = styled.div`
  ${declareFont()}
  display: flex;
  flex-direction: column;
  margin-top: ${scale.space(1)};
  margin-bottom: ${scale.space(2)};

  button {
    margin-left: auto;
    margin-right: 0;
  }
`

export const StyledNoRolesMessage = styled.div`
  ${declareFont()};
  text-align: center;
  font-style: italic;
  padding: ${scale.space(-0.5)};
  color: ${themeSelector.color('textLight')};
`

export const StyledIconWrapper = styled.span`
  margin-right: ${scale.space(-0.75)};
`
