import PropTypes from 'prop-types'
import {useEffect, useCallback} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, LoadMask, StyledStickyButtonWrapper, SignalBox, Typography, Icon} from 'tocco-ui'

import HeaderRow from '../HeaderRow'
import RoleRow from '../RoleRow'
import SearchForm from '../SearchForm'
import {businessUnitPropType, rolePropType, selectedRolePropType} from '../util/propTypes'
import {addAll, removeAll} from '../util/roleHelper'

import {
  StyledHeaderSection,
  StyledHiddenSelectionWarning,
  StyledNoRolesMessage,
  StyledIconWrapper
} from './StyledComponents'

const getMessage = (intl, id, values = {}) => (
  <SignalBox condition="info">
    <Typography.P>
      <StyledIconWrapper>
        <Icon icon="info-circle" />
      </StyledIconWrapper>
      <FormattedMessage id={`client.actions.login-role.${id}`} values={values} />
    </Typography.P>
  </SignalBox>
)
const getText = (intl, id) => intl.formatMessage({id: `client.actions.login-role.${id}`})

const LoginRoleMatrix = ({
  initialized,
  selectedRoles,
  saving,
  filteredBusinessUnits,
  filteredRoles,
  hiddenSelectedCount,
  initialize,
  setSelectedRoles,
  resetSearchForm,
  cancel,
  save,
  intl
}) => {
  useEffect(() => {
    initialize()
  }, [initialize])

  const memoizedHandleAllCheckedChange = useCallback(
    (relevantRoles, relevantBusinessUnits) => isChecked => {
      let newRoles

      if (isChecked) {
        newRoles = addAll(selectedRoles, relevantRoles, relevantBusinessUnits)
      } else {
        newRoles = removeAll(selectedRoles, relevantRoles, relevantBusinessUnits)
      }

      setSelectedRoles(newRoles)
    },
    [selectedRoles, setSelectedRoles]
  )

  return (
    <LoadMask required={[initialized]}>
      <StyledHeaderSection>
        <SearchForm />
        {hiddenSelectedCount > 0 && (
          <StyledHiddenSelectionWarning>
            {getMessage(intl, 'hidden-selection', {hiddenSelectedCount})}
            <Button onClick={resetSearchForm} label={getText(intl, 'reset')} look="raised" />
          </StyledHiddenSelectionWarning>
        )}
        <HeaderRow onAllCheckedChange={memoizedHandleAllCheckedChange} />
      </StyledHeaderSection>
      {filteredRoles.length > 0 ? (
        <>
          {filteredRoles.map(role => (
            <RoleRow
              key={role.key}
              role={role}
              onAllCheckedChange={memoizedHandleAllCheckedChange}
              getMessage={id => getMessage(intl, id)}
              columns={filteredBusinessUnits.length + 1}
            />
          ))}
        </>
      ) : (
        <StyledNoRolesMessage>{getMessage(intl, 'no-roles')}</StyledNoRolesMessage>
      )}
      <StyledStickyButtonWrapper>
        <Button type="button" onClick={cancel} disabled={saving} label={getText(intl, 'cancel')} />
        <Button
          type="button"
          onClick={save}
          ink="primary"
          look="raised"
          disabled={saving}
          pending={saving}
          label={getText(intl, 'save')}
        />
      </StyledStickyButtonWrapper>
    </LoadMask>
  )
}

LoginRoleMatrix.propTypes = {
  initialized: PropTypes.bool.isRequired,
  saving: PropTypes.bool,
  filteredRoles: PropTypes.arrayOf(rolePropType),
  filteredBusinessUnits: PropTypes.arrayOf(businessUnitPropType),
  hiddenSelectedCount: PropTypes.number,
  selectedRoles: PropTypes.arrayOf(selectedRolePropType),
  initialize: PropTypes.func.isRequired,
  setSelectedRoles: PropTypes.func.isRequired,
  resetSearchForm: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  save: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default LoginRoleMatrix
