import _find from 'lodash/find'
import {formValueSelector} from 'redux-form'

import {allRolesSelected, findSelectedRole} from './roleHelper'

export const REDUX_SEARCH_FORM_NAME = 'loginRoleMatrixSearchForm'
export const searchFormValueSelector = formValueSelector(REDUX_SEARCH_FORM_NAME)

export const idListSelector = (state, formId) => {
  const list = searchFormValueSelector(state, formId)
  return list ? list.map(element => element.id) : []
}

export const getFilteredBusinessUnits = state => {
  const selectedBusinessUnits = idListSelector(state, 'businessUnit')
  return state.loginRoles.businessUnits.filter(bu =>
    selectedBusinessUnits.length > 0 ? selectedBusinessUnits.includes(bu.key) : true
  )
}

export const getFilteredRoles = state => {
  const selectedRoleTypes = idListSelector(state, 'roleType')
  const search = searchFormValueSelector(state, 'label')
  const showSelected = searchFormValueSelector(state, 'showSelected')
  const businessUnits = state.loginRoles.businessUnits
  const selectedRoles = state.loginRoles.selectedRoles

  return state.loginRoles.roles.filter(role => {
    if (selectedRoleTypes.length > 0 && !selectedRoleTypes.includes(role.roleTypeKey)) {
      return false
    }
    if (search && !role.label.toLowerCase().includes(search.toLowerCase())) {
      return false
    }
    if (showSelected) {
      const containsAtLeastOne = businessUnits
        .map(bu => ({roleKey: role.key, businessUnitKey: bu.key}))
        .some(checkbox => Boolean(findSelectedRole(selectedRoles, checkbox.roleKey, checkbox.businessUnitKey)))
      if (!containsAtLeastOne) {
        return false
      }
    }
    return true
  })
}

export const getHiddenSelectedCount = (state, filteredRoles, filteredBusinessUnits) => {
  return state.loginRoles.selectedRoles.filter(selectedRole => {
    const roleVisible = Boolean(_find(filteredRoles, role => role.key === selectedRole.roleKey))
    const buVisible = Boolean(_find(filteredBusinessUnits, bu => bu.key === selectedRole.businessUnitKey))
    return !roleVisible || !buVisible
  }).length
}

export const areAllVisibleSelected = (state, filteredRoles, filteredBusinessUnits) => {
  return allRolesSelected(state.loginRoles.selectedRoles, filteredRoles, filteredBusinessUnits)
}
