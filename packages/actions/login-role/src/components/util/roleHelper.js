export const findSelectedRole = (selectedRoles, roleKey, businessUnitKey) =>
  selectedRoles.find(
    selectedRole => selectedRole.roleKey === roleKey && selectedRole.businessUnitKey === businessUnitKey
  )

export const allRolesSelected = (selectedRoles, roles, businessUnits) => {
  for (const role of roles) {
    for (const businessUnit of businessUnits) {
      if (!role.businessUnitKey || role.businessUnitKey === businessUnit.key) {
        const isSelected = Boolean(findSelectedRole(selectedRoles, role.key, businessUnit.key))
        if (!isSelected) {
          return false
        }
      }
    }
  }
  return true
}

export const addAll = (selectedRoles, roles, businessUnits) => {
  const newSelectedRoles = [...selectedRoles]
  for (const role of roles) {
    for (const businessUnit of businessUnits) {
      if (!role.businessUnitKey || role.businessUnitKey === businessUnit.key) {
        if (!findSelectedRole(newSelectedRoles, role.key, businessUnit.key)) {
          newSelectedRoles.push({roleKey: role.key, businessUnitKey: businessUnit.key, manual: true})
        }
      }
    }
  }
  return newSelectedRoles
}

export const removeAll = (selectedRoles, roles, businessUnits) =>
  selectedRoles.filter(selectedRole => {
    // never remove automatic roles
    if (selectedRole.manual !== true) {
      return true
    }

    const shouldRemoveRole = Boolean(roles.find(role => role.key === selectedRole.roleKey))
    const shouldRemoveBusinessUnit = Boolean(
      businessUnits.find(businessUnit => businessUnit.key === selectedRole.businessUnitKey)
    )
    return !(shouldRemoveRole && shouldRemoveBusinessUnit)
  })
