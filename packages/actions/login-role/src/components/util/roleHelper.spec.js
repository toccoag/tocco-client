import {findSelectedRole, allRolesSelected, addAll, removeAll} from './roleHelper'

describe('login-role', () => {
  describe('components', () => {
    describe('util', () => {
      describe('roleHelper', () => {
        const roles = [{key: 'single bu', businessUnitKey: 'bu 1'}, {key: 'both bu'}]
        const businessUnits = [{key: 'bu 1'}, {key: 'bu 2'}]

        describe('findSelectedRole', () => {
          it('should find role by role key and business unit key', () => {
            const selectedRoles = [
              {id: 'both wrong', roleKey: '2', businessUnitKey: '1'},
              {id: 'business unit wrong', roleKey: '1', businessUnitKey: '1'},
              {id: 'role wrong', roleKey: '2', businessUnitKey: '2'},
              {id: 'correct', roleKey: '1', businessUnitKey: '2'}
            ]
            const result = findSelectedRole(selectedRoles, '1', '2')
            expect(result.id).to.be.eql('correct')
          })
        })

        describe('allRolesSelected', () => {
          it('should return true if all roles are selected in all business units', () => {
            const selectedRoles = [
              {roleKey: 'single bu', businessUnitKey: 'bu 1'},
              {roleKey: 'both bu', businessUnitKey: 'bu 1'},
              {roleKey: 'both bu', businessUnitKey: 'bu 2'}
            ]
            const result = allRolesSelected(selectedRoles, roles, businessUnits)
            expect(result).to.be.true
          })

          it('should return false if a role is missing in a business unit', () => {
            const selectedRoles = [
              {roleKey: 'single bu', businessUnitKey: 'bu 1'},
              {roleKey: 'both bu', businessUnitKey: 'bu 1'}
            ]
            const result = allRolesSelected(selectedRoles, roles, businessUnits)
            expect(result).to.be.false
          })
        })

        describe('addAll', () => {
          it('should add all roles to selected roles', () => {
            const result = addAll([], roles, businessUnits)
            expect(result).to.have.length(3)
            expect(result).to.deep.include({roleKey: 'single bu', businessUnitKey: 'bu 1', manual: true})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 1', manual: true})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 2', manual: true})
          })

          it('should not duplicate already selected roles', () => {
            const result = addAll([{roleKey: 'both bu', businessUnitKey: 'bu 1', manual: false}], roles, businessUnits)
            expect(result).to.have.length(3)
            expect(result).to.deep.include({roleKey: 'single bu', businessUnitKey: 'bu 1', manual: true})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 1', manual: false})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 2', manual: true})
          })

          it('should not remove already selected roles', () => {
            const otherRole = {id: 'some other role'}
            const result = addAll([otherRole], roles, businessUnits)
            expect(result).to.have.length(4)
            expect(result).to.deep.include({roleKey: 'single bu', businessUnitKey: 'bu 1', manual: true})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 1', manual: true})
            expect(result).to.deep.include({roleKey: 'both bu', businessUnitKey: 'bu 2', manual: true})
            expect(result).to.deep.include(otherRole)
          })
        })

        describe('removeAll', () => {
          it('should remove manual roles', () => {
            const selectedRoles = [{roleKey: 'both bu', businessUnitKey: 'bu 2', manual: true}]
            const result = removeAll(selectedRoles, roles, businessUnits)
            expect(result).to.be.empty
          })

          it('should keep automatic roles', () => {
            const selectedRoles = [{roleKey: 'both bu', businessUnitKey: 'bu 2', manual: false}]
            const result = removeAll(selectedRoles, roles, businessUnits)
            expect(result).to.deep.eql(selectedRoles)
          })

          it('should keep different roles', () => {
            const selectedRoles = [
              {roleKey: 'other rule', businessUnitKey: 'bu 2', manual: true},
              {roleKey: 'both bu', businessUnitKey: 'other bu', manual: true}
            ]
            const result = removeAll(selectedRoles, roles, businessUnits)
            expect(result).to.deep.eql(selectedRoles)
          })
        })
      })
    })
  })
})
