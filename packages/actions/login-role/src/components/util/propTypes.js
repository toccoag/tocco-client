import PropTypes from 'prop-types'

export const rolePropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  roleTypeKey: PropTypes.string.isRequired,
  businessUnitKey: PropTypes.string
})

export const selectedRolePropType = PropTypes.shape({
  roleKey: PropTypes.string.isRequired,
  businessUnitKey: PropTypes.string,
  manual: PropTypes.bool
})

export const roleTypePropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
})

export const businessUnitPropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
})
