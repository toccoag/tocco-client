import {
  REDUX_SEARCH_FORM_NAME,
  idListSelector,
  getFilteredBusinessUnits,
  getFilteredRoles,
  getHiddenSelectedCount
} from './selectors'

describe('login-role', () => {
  describe('components', () => {
    describe('util', () => {
      describe('idListSelector', () => {
        it('should return list of ids', () => {
          const state = {
            form: {
              [REDUX_SEARCH_FORM_NAME]: {
                values: {
                  formId: [{id: 'first id'}, {id: 'second id'}]
                }
              }
            }
          }
          const result = idListSelector(state, 'formId')
          expect(result).to.eql(['first id', 'second id'])
        })
      })

      describe('getFilteredBusinessUnits', () => {
        it('should filter business units', () => {
          const state = {
            form: {
              [REDUX_SEARCH_FORM_NAME]: {
                values: {
                  businessUnit: [{id: 'second bu'}]
                }
              }
            },
            loginRoles: {
              businessUnits: [{key: 'first bu'}, {key: 'second bu'}]
            }
          }
          const result = getFilteredBusinessUnits(state)
          expect(result).to.eql([{key: 'second bu'}])
        })

        it('should not filter business units when none selected', () => {
          const state = {
            form: {
              [REDUX_SEARCH_FORM_NAME]: {
                values: {
                  businessUnit: []
                }
              }
            },
            loginRoles: {
              businessUnits: [{key: 'first bu'}, {key: 'second bu'}]
            }
          }
          const result = getFilteredBusinessUnits(state)
          expect(result).to.eql([{key: 'first bu'}, {key: 'second bu'}])
        })
      })

      describe('getFilteredRoles', () => {
        const getState = ({
          roleTypes = [],
          label,
          showSelected = false,
          businessUnits = [],
          selectedRoles = [],
          roles = []
        } = {}) => {
          return {
            form: {
              [REDUX_SEARCH_FORM_NAME]: {
                values: {
                  roleType: roleTypes.map(roleType => ({id: roleType})),
                  label,
                  showSelected
                }
              }
            },
            loginRoles: {
              roles,
              businessUnits: businessUnits.map(businessUnit => ({key: businessUnit})),
              selectedRoles
            }
          }
        }

        it('should filter by role type', () => {
          const state = getState({
            roleTypes: ['correct'],
            roles: [
              {key: 'role1', roleTypeKey: 'correct'},
              {key: 'role2', roleTypeKey: 'wrong'},
              {key: 'role3', roleTypeKey: 'correct'}
            ]
          })
          const result = getFilteredRoles(state)
          expect(result.map(({key}) => key)).to.eql(['role1', 'role3'])
        })

        it('should filter by label', () => {
          const state = getState({
            label: 'correct',
            roles: [
              {key: 'role1', label: 'correct'},
              {key: 'role2', label: 'wrong'},
              {key: 'role3', label: 'correct'}
            ]
          })
          const result = getFilteredRoles(state)
          expect(result.map(({key}) => key)).to.eql(['role1', 'role3'])
        })

        it('should filter by selection', () => {
          const state = getState({
            showSelected: true,
            roles: [{key: 'role1'}, {key: 'role2'}, {key: 'role3'}],
            businessUnits: ['bu1', 'bu2'],
            selectedRoles: [{roleKey: 'role1', businessUnitKey: 'bu1'}]
          })
          const result = getFilteredRoles(state)
          expect(result.map(({key}) => key)).to.eql(['role1'])
        })
      })

      describe('getHiddenSelectedCount', () => {
        it('should count hidden roles', () => {
          const state = {
            loginRoles: {
              selectedRoles: [
                {roleKey: 'correct', businessUnitKey: 'correct'},
                {roleKey: 'wrong', businessUnitKey: 'correct'},
                {roleKey: 'correct', businessUnitKey: 'wrong'},
                {roleKey: 'wrong', businessUnitKey: 'wrong'}
              ]
            }
          }
          const result = getHiddenSelectedCount(state, [{key: 'correct'}], [{key: 'correct'}])
          expect(result).to.eql(3)
        })
      })
    })
  })
})
