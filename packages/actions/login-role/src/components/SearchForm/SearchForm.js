import PropTypes from 'prop-types'
import {useState} from 'react'
import {reduxForm} from 'redux-form'
import {form, searchform} from 'tocco-app-extensions'
import {Ball} from 'tocco-ui'

import {businessUnitPropType, roleTypePropType} from '../util/propTypes'
import {REDUX_SEARCH_FORM_NAME} from '../util/selectors'

import {getFormDefinition} from './formDefinition'

const simpleSearchFields = ['label']

const SearchForm = ({entity, form: formName, formValues, roleTypes, businessUnits, intl}) => {
  const [showExtendedSearchForm, setShowExtendedSearchForm] = useState(false)

  const searchFormDefinition = getFormDefinition(roleTypes, businessUnits, intl)
  const shouldRenderField = name => showExtendedSearchForm || simpleSearchFields.includes(name)

  const handleSubmit = e => {
    e.preventDefault()
    e.stopPropagation()
  }

  const toggleExtendedSearchForm = () => {
    setShowExtendedSearchForm(!showExtendedSearchForm)
  }

  return (
    <div>
      <searchform.StyledExtendSearchButtonWrapper>
        <Ball
          data-cy="btn-extend-search"
          icon={`chevron-${showExtendedSearchForm ? 'up' : 'down'}`}
          onClick={toggleExtendedSearchForm}
          title={intl.formatMessage({id: 'client.common.extendedSearch'})}
        />
      </searchform.StyledExtendSearchButtonWrapper>
      <form onSubmit={handleSubmit}>
        <form.FormBuilder
          entity={entity}
          formName={formName}
          formDefinition={searchFormDefinition}
          formValues={formValues}
          fieldMappingType="search"
          mode="search"
          labelPosition="inside"
          beforeRenderField={shouldRenderField}
        />
      </form>
    </div>
  )
}

SearchForm.propTypes = {
  roleTypes: PropTypes.arrayOf(roleTypePropType),
  businessUnits: PropTypes.arrayOf(businessUnitPropType),
  entity: PropTypes.object,
  form: PropTypes.string,
  formValues: PropTypes.object,
  intl: PropTypes.object.isRequired
}

export default reduxForm({
  form: REDUX_SEARCH_FORM_NAME,
  destroyOnUnmount: false
})(SearchForm)
