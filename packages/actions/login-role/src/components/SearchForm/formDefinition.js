import {form} from 'tocco-app-extensions'

const createOptions = entities =>
  entities.map(entity => form.createChoiceOption(entity.key, {display: entity.label, checked: false}))

export const getFormDefinition = (roleTypes, businessUnits, intl) => {
  const getText = id => intl.formatMessage({id: `client.actions.login-role.search.${id}`})
  return form.createSimpleForm({
    children: [
      form.createFieldSet({label: getText('label'), path: 'label', dataType: 'fulltext-search'}),
      form.createFieldSet({
        label: getText('role-type'),
        path: 'roleType',
        dataType: 'multi-select-box',
        additionalFieldAttributes: {
          options: createOptions(roleTypes)
        }
      }),
      form.createFieldSet({
        label: getText('business-unit'),
        path: 'businessUnit',
        dataType: 'multi-select-box',
        additionalFieldAttributes: {
          options: createOptions(businessUnits)
        }
      }),
      form.createFieldSet({label: getText('show-selected'), path: 'showSelected', dataType: 'boolean'})
    ]
  })
}
