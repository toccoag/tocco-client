import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import SearchForm from './SearchForm'

const mapStateToProps = state => ({
  roleTypes: state.loginRoles.roleTypes,
  businessUnits: state.loginRoles.businessUnits
})

export default connect(mapStateToProps)(injectIntl(SearchForm))
