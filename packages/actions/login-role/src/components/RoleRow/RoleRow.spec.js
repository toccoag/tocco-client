import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import RoleRow from './RoleRow'

describe('login-role', () => {
  describe('components', () => {
    describe('RoleRow', () => {
      const role = {
        key: 'role',
        label: 'role label',
        roleTypeKey: 'role type',
        businessUnitKey: 'bu2'
      }

      it('should show role checkboxes', async () => {
        const onAllCheckedChange = sinon.spy()
        const setSelectedRoles = sinon.spy()
        const firstBu = {
          key: 'bu1',
          label: 'first bu label'
        }
        const secondBu = {
          key: 'bu2',
          label: 'second bu label'
        }
        testingLibrary.renderWithIntl(
          <RoleRow
            role={role}
            filteredBusinessUnits={[firstBu, secondBu]}
            initiallySelectedRoles={[]}
            selectedRoles={[]}
            saving={false}
            onAllCheckedChange={onAllCheckedChange}
            setSelectedRoles={setSelectedRoles}
            getMessage={id => id}
          />
        )

        expect(screen.getByText('role label')).to.exist

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes).to.have.length(2)
        expect(checkboxes.map(box => box.id)).to.include.members(['checkbox-role', 'checkbox-role-bu2'])

        const user = userEvent.setup()
        await user.click(checkboxes.find(({id}) => id === 'checkbox-role'))
        expect(onAllCheckedChange).to.have.been.calledWith([role], [firstBu, secondBu])
        await user.click(checkboxes.find(({id}) => id === 'checkbox-role-bu2'))
        expect(setSelectedRoles).to.have.been.calledWith([{roleKey: 'role', businessUnitKey: 'bu2', manual: true}])
      })

      it('should precheck role checkboxes', () => {
        testingLibrary.renderWithIntl(
          <RoleRow
            role={role}
            filteredBusinessUnits={[
              {
                key: 'bu2',
                label: 'bu label'
              }
            ]}
            initiallySelectedRoles={[]}
            selectedRoles={[{roleKey: role.key, businessUnitKey: role.businessUnitKey}]}
            saving={false}
            onAllCheckedChange={sinon.spy()}
            setSelectedRoles={sinon.spy()}
            getMessage={id => id}
          />
        )

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes.find(({id}) => id === 'checkbox-role-bu2').checked).to.be.true
      })
    })
  })
})
