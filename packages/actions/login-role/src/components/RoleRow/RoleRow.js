import PropTypes from 'prop-types'
import {useCallback} from 'react'
import {EditableValue, Typography} from 'tocco-ui'

import {businessUnitPropType, rolePropType, selectedRolePropType} from '../util/propTypes'
import {allRolesSelected, findSelectedRole} from '../util/roleHelper'

import {StyledCheckboxWrapper, StyledRowLabel, StyledRoledRow} from './StyledComponents'

const RoleRow = ({
  role,
  filteredBusinessUnits,
  initiallySelectedRoles,
  selectedRoles,
  saving,
  onAllCheckedChange,
  setSelectedRoles,
  getMessage,
  columns
}) => {
  const handleRoleCheckedChange = useCallback(
    businessUnit => isChecked => {
      let newRoles
      if (isChecked) {
        newRoles = [...selectedRoles, {roleKey: role.key, businessUnitKey: businessUnit, manual: true}]
      } else {
        newRoles = selectedRoles.filter(
          selectedRole => !(selectedRole.roleKey === role.key && selectedRole.businessUnitKey === businessUnit)
        )
      }
      setSelectedRoles(newRoles)
    },
    [role, selectedRoles, setSelectedRoles]
  )

  return (
    <StyledRoledRow data-cy={`role-row`} columns={columns}>
      <StyledRowLabel>
        <EditableValue
          type="boolean"
          value={allRolesSelected(selectedRoles, [role], filteredBusinessUnits)}
          events={{onChange: onAllCheckedChange([role], filteredBusinessUnits)}}
          id={`checkbox-${role.key}`}
          readOnly={saving}
        />
        <Typography.Span>{role.label}</Typography.Span>
      </StyledRowLabel>

      {filteredBusinessUnits.map(bu => {
        const hasCheckbox = !role.businessUnitKey || role.businessUnitKey === bu.key
        const initiallySelectedRole = findSelectedRole(initiallySelectedRoles, role.key, bu.key)
        const selectedRole = findSelectedRole(selectedRoles, role.key, bu.key)
        const isInitiallySelected = Boolean(initiallySelectedRole)
        const isSelected = Boolean(selectedRole)
        const isRuleRole = selectedRole && selectedRole?.manual !== true
        const isReadOnly = isRuleRole || saving
        const isDirty = isSelected !== isInitiallySelected

        return (
          <StyledCheckboxWrapper key={bu.key} dirty={isDirty}>
            {hasCheckbox && (
              <EditableValue
                type="boolean"
                value={isSelected}
                events={{onChange: handleRoleCheckedChange(bu.key)}}
                id={`checkbox-${role.key}-${bu.key}`}
                readOnly={isReadOnly}
                popoverContent={isRuleRole && getMessage('automatic-role-warning')}
              />
            )}
          </StyledCheckboxWrapper>
        )
      })}
    </StyledRoledRow>
  )
}

RoleRow.propTypes = {
  role: rolePropType.isRequired,
  filteredBusinessUnits: PropTypes.arrayOf(businessUnitPropType).isRequired,
  initiallySelectedRoles: PropTypes.arrayOf(selectedRolePropType).isRequired,
  selectedRoles: PropTypes.arrayOf(selectedRolePropType).isRequired,
  saving: PropTypes.bool,
  onAllCheckedChange: PropTypes.func.isRequired,
  setSelectedRoles: PropTypes.func.isRequired,
  getMessage: PropTypes.func.isRequired,
  columns: PropTypes.number.isRequired
}

export default RoleRow
