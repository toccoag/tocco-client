import {connect} from 'react-redux'

import {setSelectedRoles} from '../../modules/loginRoles/actions'
import {getFilteredBusinessUnits} from '../util/selectors'

import RoleRow from './RoleRow'

const mapActionCreators = {
  setSelectedRoles
}

const mapStateToProps = state => {
  return {
    initiallySelectedRoles: state.loginRoles.initiallySelectedRoles,
    selectedRoles: state.loginRoles.selectedRoles,
    saving: state.loginRoles.saving,
    filteredBusinessUnits: getFilteredBusinessUnits(state)
  }
}

export default connect(mapStateToProps, mapActionCreators)(RoleRow)
