import styled from 'styled-components'
import {themeSelector, scale} from 'tocco-ui'

export const StyledRowLabel = styled.div`
  display: flex;
  align-items: center;
  gap: ${scale.space(-0.5)};

  & > * {
    flex-basis: content;
  }
`
export const StyledRoledRow = styled.div`
  display: grid;
  grid-template-columns: ${({columns}) => `repeat(${columns}, 1fr)`};

  &:hover {
    background-color: ${themeSelector.color('backgroundItemHover')};
  }
`

export const StyledCheckboxWrapper = styled.div`
  text-align: center;
  background-color: ${({dirty}) => dirty && themeSelector.color('backgroundBody')};
`
