import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import HeaderRow from './HeaderRow'

describe('login-role', () => {
  describe('components', () => {
    describe('HeaderRow', () => {
      const roles = [{key: 'role', label: 'role label', roleTypeKey: 'role type'}]

      it('should show business unit checkboxes', async () => {
        const onAllCheckedChange = sinon.spy()
        const firstBu = {
          key: 'bu1',
          label: 'first bu label'
        }
        const secondBu = {
          key: 'bu2',
          label: 'second bu label'
        }

        testingLibrary.renderWithIntl(
          <HeaderRow
            filteredBusinessUnits={[firstBu, secondBu]}
            filteredRoles={roles}
            selectedRoles={[]}
            allVisibleSelected={false}
            saving={false}
            onAllCheckedChange={onAllCheckedChange}
          />
        )

        expect(screen.getByText('first bu label')).to.exist
        expect(screen.getByText('second bu label')).to.exist

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes).to.have.length(3)
        expect(checkboxes.map(box => box.id)).to.include.members([
          'checkbox-all',
          'checkbox-all-bu1',
          'checkbox-all-bu2'
        ])

        const user = userEvent.setup()
        await user.click(checkboxes.find(({id}) => id === 'checkbox-all'))
        expect(onAllCheckedChange).to.have.been.calledWith(roles, [firstBu, secondBu])
        await user.click(checkboxes.find(({id}) => id === 'checkbox-all-bu1'))
        expect(onAllCheckedChange).to.have.been.calledWith(roles, [firstBu])
        await user.click(checkboxes.find(({id}) => id === 'checkbox-all-bu2'))
        expect(onAllCheckedChange).to.have.been.calledWith(roles, [secondBu])
      })

      it('should deactive checkboxes on saving', () => {
        testingLibrary.renderWithIntl(
          <HeaderRow
            filteredBusinessUnits={[
              {
                key: 'bu',
                label: 'bu label'
              }
            ]}
            filteredRoles={roles}
            selectedRoles={[]}
            allVisibleSelected={false}
            saving={true}
            onAllCheckedChange={sinon.spy()}
          />
        )

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes.every(box => box.disabled)).to.be.true
      })

      it('should precheck all checkbox', () => {
        testingLibrary.renderWithIntl(
          <HeaderRow
            filteredBusinessUnits={[
              {
                key: 'bu',
                label: 'bu label'
              }
            ]}
            filteredRoles={roles}
            selectedRoles={[]}
            allVisibleSelected={true}
            saving={false}
            onAllCheckedChange={sinon.spy()}
          />
        )

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes.find(({id}) => id === 'checkbox-all').checked).to.be.true
      })

      it('should precheck business unit checkbox', () => {
        testingLibrary.renderWithIntl(
          <HeaderRow
            filteredBusinessUnits={[
              {
                key: 'bu',
                label: 'bu label'
              }
            ]}
            filteredRoles={roles}
            selectedRoles={roles.map(({key}) => ({roleKey: key, businessUnitKey: 'bu'}))}
            allVisibleSelected={true}
            saving={false}
            onAllCheckedChange={sinon.spy()}
          />
        )

        const checkboxes = screen.getAllByRole('checkbox')
        expect(checkboxes.find(({id}) => id === 'checkbox-all-bu').checked).to.be.true
      })
    })
  })
})
