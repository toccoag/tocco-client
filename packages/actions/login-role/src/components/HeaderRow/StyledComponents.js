import styled from 'styled-components'
import {declareFont} from 'tocco-ui'

export const StyledMatrixHeader = styled.div`
  display: grid;
  grid-template-columns: ${props => `repeat(${props.columns}, 1fr)`};
  margin-top: 1em;
`

export const StyledBusinessUnitHeaderCell = styled.div`
  ${declareFont()};
  text-align: center;
`
