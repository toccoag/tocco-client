import {connect} from 'react-redux'

import {getFilteredBusinessUnits, getFilteredRoles, areAllVisibleSelected} from '../util/selectors'

import HeaderRow from './HeaderRow'

const mapStateToProps = state => {
  const filteredBusinessUnits = getFilteredBusinessUnits(state)
  const filteredRoles = getFilteredRoles(state)
  return {
    selectedRoles: state.loginRoles.selectedRoles,
    saving: state.loginRoles.saving,
    filteredBusinessUnits,
    filteredRoles,
    allVisibleSelected: areAllVisibleSelected(state, filteredRoles, filteredBusinessUnits)
  }
}

export default connect(mapStateToProps)(HeaderRow)
