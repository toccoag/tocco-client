import PropTypes from 'prop-types'
import {EditableValue} from 'tocco-ui'

import {businessUnitPropType, rolePropType, selectedRolePropType} from '../util/propTypes'
import {allRolesSelected} from '../util/roleHelper'

import {StyledBusinessUnitHeaderCell, StyledMatrixHeader} from './StyledComponents'

const HeaderRow = ({
  filteredRoles,
  filteredBusinessUnits,
  selectedRoles,
  allVisibleSelected,
  saving,
  onAllCheckedChange
}) => (
  <StyledMatrixHeader columns={filteredBusinessUnits.length + 1}>
    <div>{/* empty div to fill out grid */}</div>
    {filteredBusinessUnits.map(bu => (
      <StyledBusinessUnitHeaderCell key={bu.key}>{bu.label}</StyledBusinessUnitHeaderCell>
    ))}
    <EditableValue
      type="boolean"
      value={allVisibleSelected}
      events={{onChange: onAllCheckedChange(filteredRoles, filteredBusinessUnits)}}
      id={`checkbox-all`}
      readOnly={filteredRoles.length === 0 || saving}
    />
    {filteredBusinessUnits.map(bu => (
      <StyledBusinessUnitHeaderCell key={bu.key}>
        <EditableValue
          type="boolean"
          value={allRolesSelected(selectedRoles, filteredRoles, [bu])}
          events={{onChange: onAllCheckedChange(filteredRoles, [bu])}}
          id={`checkbox-all-${bu.key}`}
          readOnly={filteredRoles.length === 0 || saving}
        />
      </StyledBusinessUnitHeaderCell>
    ))}
  </StyledMatrixHeader>
)

HeaderRow.propTypes = {
  filteredRoles: PropTypes.arrayOf(rolePropType).isRequired,
  filteredBusinessUnits: PropTypes.arrayOf(businessUnitPropType).isRequired,
  selectedRoles: PropTypes.arrayOf(selectedRolePropType).isRequired,
  allVisibleSelected: PropTypes.bool,
  saving: PropTypes.bool,
  onAllCheckedChange: PropTypes.func.isRequired
}

export default HeaderRow
