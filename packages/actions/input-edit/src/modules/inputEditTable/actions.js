export const INITIALIZE_TABLE = 'inputEditTable/INITIALIZE_TABLE'
export const LOAD_DATA = 'inputEditTable/LOAD_DATA'
export const SET_DATA = 'inputEditTable/SET_DATA'
export const SET_EDIT_FORM = 'inputEditTable/SET_EDIT_FORM'
export const UPDATE_VALUE = 'inputEditTable/UPDATE_VALUE'
export const SET_VALUE = 'inputEditTable/SET_VALUE'
export const SET_SORTING = 'inputEditTable/SET_SORTING'
export const SET_DATA_FORM_COLUMNS = 'inputEditTable/SET_DATA_FORM_COLUMNS'
export const SET_ACTION_DEFINITIONS = 'inputEditTable/SET_ACTION_DEFINITIONS'
export const SET_CALCULATING = 'inputEditTable/SET_CALCULATING'
export const SET_INITIALIZED = 'inputEditTable/SET_INITIALIZED'

export const initializeTable = () => ({
  type: INITIALIZE_TABLE
})

export const loadData = payload => ({
  type: LOAD_DATA,
  payload
})

export const setData = data => ({
  type: SET_DATA,
  payload: {data}
})

export const setEditForm = form => ({
  type: SET_EDIT_FORM,
  payload: form
})

export const updateValue = (inputDataKey, node, value) => ({
  type: UPDATE_VALUE,
  payload: {
    inputDataKey,
    node,
    value
  }
})

export const setValue = (inputDataKey, node, value) => ({
  type: SET_VALUE,
  payload: {
    inputDataKey,
    node,
    value
  }
})

export const setDataFormColumns = dataFormColumns => ({
  type: SET_DATA_FORM_COLUMNS,
  payload: {
    dataFormColumns
  }
})

export const setActionDefinitions = actionDefinitions => ({
  type: SET_ACTION_DEFINITIONS,
  payload: {
    actionDefinitions
  }
})

export const setCalculating = (inputData, loading) => ({
  type: SET_CALCULATING,
  payload: {
    inputData,
    loading
  }
})

export const setInitialized = initialized => ({
  type: SET_INITIALIZED,
  payload: {
    initialized
  }
})
