import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  actionDefinitions: [],
  dataFormColumns: [],
  data: [],
  inputEditForm: [],
  sorting: [],
  initialized: false
}

const setValue = (state, action) => {
  const {inputDataKey, node, value} = action.payload
  return {
    ...state,
    data: state.data.reduce((acc, val) => {
      if (val.pk === inputDataKey) {
        return [
          ...acc,
          {
            ...val,
            [node]: value
          }
        ]
      }
      return [...acc, val]
    }, [])
  }
}

const incrementLoadingCounter = data => (data.loading || 0) + 1
const decrementLoadingCounter = data => Math.max((data.loading || 0) - 1, 0)

const setCalculating = (state, {payload: {inputData, loading}}) => {
  return {
    ...state,
    data: state.data.map(data => {
      if (data.pk === inputData) {
        return {
          ...data,
          loading: loading ? incrementLoadingCounter(data) : decrementLoadingCounter(data)
        }
      } else {
        return data
      }
    })
  }
}

const ACTION_HANDLERS = {
  [actions.SET_DATA_FORM_COLUMNS]: reducerUtil.singleTransferReducer('dataFormColumns'),
  [actions.SET_ACTION_DEFINITIONS]: reducerUtil.singleTransferReducer('actionDefinitions'),
  [actions.SET_EDIT_FORM]: reducerUtil.singleTransferReducer('inputEditForm'),
  [actions.SET_DATA]: reducerUtil.singleTransferReducer('data'),
  [actions.SET_VALUE]: setValue,
  [actions.LOAD_DATA]: reducerUtil.singleTransferReducer('sorting'),
  [actions.SET_CALCULATING]: setCalculating,
  [actions.SET_INITIALIZED]: reducerUtil.singleTransferReducer('initialized')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
