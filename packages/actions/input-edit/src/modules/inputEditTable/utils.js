import {api} from 'tocco-util'

export const isInputField = id => !isNaN(parseInt(id)) || id === 'rating' // textchoice has no exams but is an input

export const transformResponseData = response =>
  response.body.data
    .map(dataRecord => {
      const splitObj = Object.keys(dataRecord).reduce(
        (acc, key) => {
          return {
            ...acc,
            ...(isInputField(key)
              ? {
                  inputFields: {
                    ...acc.inputFields,
                    [key]: dataRecord[key]
                  }
                }
              : {
                  entityFields: {
                    ...acc.entityFields,
                    [key]: dataRecord[key]
                  }
                })
          }
        },
        {
          inputFields: {},
          entityFields: {}
        }
      )

      return {...splitObj.inputFields, ...api.flattenPaths(splitObj.entityFields)}
    })
    .map((dataRecord, index) => ({...dataRecord, __key: dataRecord.pk, index: index + 1}))
