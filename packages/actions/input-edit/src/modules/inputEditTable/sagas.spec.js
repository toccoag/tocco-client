import {all, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {notification, rest} from 'tocco-app-extensions'
import {qualification as qualificationUtil} from 'tocco-util'

import * as inputEditActions from '../inputEdit/actions'
import {setTotalCount} from '../inputEditPagination/actions'
import * as searchActions from '../inputEditSearch/actions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'
import {transformResponseData} from './utils'

describe('input-edit', () => {
  describe('input-edit-table', () => {
    describe('sagas', () => {
      const fakeDataColumns = [
        {
          children: [
            {
              path: 'first field'
            }
          ]
        },
        {
          children: [
            {
              path: 'second field'
            }
          ]
        }
      ]

      const fakeAction = {
        id: 'action',
        componentType: 'action',
        scope: 'detail'
      }

      const fakeDataForm = {
        children: [
          {
            id: 'main-action-bar',
            label: null,
            componentType: 'action-bar',
            children: [
              {
                id: 'output',
                label: 'Ausgabe'
              },
              {
                id: 'nice2.reporting.actions.ChangelogExportAction',
                label: 'Changelog exportieren'
              },
              fakeAction
            ],
            actions: [],
            defaultAction: null
          },
          {
            componentType: 'table',
            children: fakeDataColumns
          }
        ]
      }

      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([
            takeLatest(actions.INITIALIZE_TABLE, sagas.initialize),
            takeLatest(actions.LOAD_DATA, sagas.loadData),
            takeEvery(actions.UPDATE_VALUE, sagas.updateValue)
          ])
        )
        expect(generator.next().done).to.be.true
      })

      describe('initialize', () => {
        test('should load forms', () => {
          const expectedEditForm = [
            {
              editform: 'editform'
            }
          ]
          const expectedDataForm = {
            dataform: 'dataform'
          }
          const readonlyActions = ['action']
          const hiddenActions = ['hidden-action']
          return expectSaga(sagas.initialize)
            .provide([
              [select(sagas.inputSelector), {selection: [12]}],
              [select(sagas.inputEditSearchSelector), {initialized: true}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    editColumns: expectedEditForm,
                    readonly: false,
                    readonlyActions,
                    hiddenActions
                  }
                }
              ],
              [matchers.call.fn(rest.fetchForm), expectedDataForm],
              [matchers.call.fn(sagas.processDataForm)]
            ])
            .call(rest.fetchForm, 'Input_edit_data', 'list')
            .put(actions.setEditForm({inputEditForm: expectedEditForm}))
            .call(sagas.processDataForm, expectedDataForm, false, readonlyActions, hiddenActions)
            .run()
        })

        test('should handle readonly', () => {
          const expectedEditForm = [
            {
              editform: 'editform'
            }
          ]
          const expectedDataForm = {
            dataform: 'dataform'
          }
          const readonlyActions = ['action']
          const hiddenActions = ['hidden-action']

          const sagaRunner = expectSaga(sagas.initialize)
            .provide([
              [select(sagas.inputSelector), {selection: [12]}],
              [select(sagas.inputEditSearchSelector), {initialized: true}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    editColumns: expectedEditForm,
                    readonly: true,
                    readonlyActions,
                    hiddenActions
                  }
                }
              ],
              [matchers.call.fn(rest.fetchForm), expectedDataForm],
              [matchers.call.fn(sagas.processDataForm)]
            ])
            .call(rest.fetchForm, 'Input_edit_data', 'list')
            .put(actions.setEditForm({inputEditForm: expectedEditForm}))
            .put(
              notification.toaster({
                type: 'info',
                title: 'client.actions.InputEdit.input_closed'
              })
            )
            .call(sagas.processDataForm, expectedDataForm, true, readonlyActions, hiddenActions)
            .run()

          expect(expectedEditForm[0].readonly).to.be.true

          return sagaRunner
        })

        test('should load data form from input', () => {
          const expectedEditForm = [
            {
              editform: 'editform'
            }
          ]
          const expectedDataForm = {
            dataform: 'dataform'
          }
          return expectSaga(sagas.initialize)
            .provide([
              [
                select(sagas.inputSelector),
                {
                  selection: [12],
                  actionProperties: {
                    inputEditDataForm: 'PublicInput_edit_data'
                  }
                }
              ],
              [select(sagas.inputEditSearchSelector), {initialized: false}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    editColumns: expectedEditForm,
                    readonly: false,
                    readonlyActions: []
                  }
                }
              ],
              [matchers.call.fn(sagas.processDataForm), {}],
              [matchers.call.fn(rest.fetchForm), expectedDataForm],
              [matchers.call.fn(sagas.loadData), {}]
            ])
            .dispatch(searchActions.initializeSearch(true))
            .call(rest.fetchForm, 'PublicInput_edit_data', 'list')
            .run()
        })
      })

      describe('load-data', () => {
        test('should load data from state', () => {
          const fakeSelection = {ids: ['12']}
          const fakeData = [{pk: '1'}]
          const finishedLoading = sinon.spy()
          return expectSaga(sagas.loadData, {payload: {page: 2, finishedLoading}})
            .provide([
              [select(sagas.inputSelector), {selection: fakeSelection}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                select(sagas.inputEditTableSelector),
                {
                  dataFormColumns: fakeDataColumns,
                  sorting: [{field: 'field', order: 'asc'}],
                  initialized: true
                }
              ],
              [select(sagas.searchQueriesSelector), ["firstname == 'whatever'"]],
              [
                select(sagas.inputEditPaginationSelector),
                {
                  count: 0,
                  recordsPerPage: 25
                }
              ],
              [matchers.call.fn(rest.requestSaga), {body: {data: fakeData, count: 30}}],
              [matchers.call.fn(transformResponseData), fakeData]
            ])
            .call.like({
              fn: rest.requestSaga,
              args: [
                'inputEdit/data/search',
                {
                  method: 'POST',
                  body: {
                    selection: fakeSelection,
                    searchBean: {
                      paths: ['first field', 'second field', 'pk'],
                      sort: 'field asc',
                      where: "firstname == 'whatever'",
                      limit: 25,
                      offset: 25
                    }
                  }
                }
              ]
            })
            .put(actions.setData(fakeData))
            .put(setTotalCount(30))
            .run()
            .then(() => {
              expect(finishedLoading).to.have.been.called
            })
        })

        test('should load data after selection update is done', () => {
          return expectSaga(sagas.loadData, {payload: {}})
            .provide([
              [select(sagas.inputSelector), {selection: {ids: []}}],
              [select(sagas.inputEditSelector), {validation: {valid: true}, updateInProgress: true}],
              [
                select(sagas.inputEditTableSelector),
                {
                  dataFormColumns: fakeDataColumns,
                  sorting: [],
                  initialized: true
                }
              ],
              [select(sagas.searchQueriesSelector), []],
              [
                select(sagas.inputEditPaginationSelector),
                {
                  count: 0,
                  recordsPerPage: 25
                }
              ],
              [matchers.call.fn(rest.requestSaga), {body: {data: []}}]
            ])
            .dispatch(inputEditActions.setSelectionUpdateInProgress(false))
            .call.like({
              fn: rest.requestSaga
            })
            .run()
        })

        test('should load data after initialization', () => {
          return expectSaga(sagas.loadData, {payload: {}})
            .provide([
              [select(sagas.inputSelector), {selection: {ids: []}}],
              [select(sagas.inputEditSelector), {validation: {valid: true}, updateInProgress: false}],
              [
                select(sagas.inputEditTableSelector),
                {
                  dataFormColumns: fakeDataColumns,
                  sorting: [],
                  initialized: false
                }
              ],
              [select(sagas.searchQueriesSelector), []],
              [
                select(sagas.inputEditPaginationSelector),
                {
                  count: 0,
                  recordsPerPage: 25
                }
              ],
              [matchers.call.fn(rest.requestSaga), {body: {data: []}}]
            ])
            .dispatch(actions.setInitialized(true))
            .call.like({
              fn: rest.requestSaga
            })
            .run()
        })
      })

      describe('processDataForm', () => {
        test('should set action and column defintions', () => {
          const actionDefinitions = [{id: 'action'}]
          const dataFormColumns = [{id: 'column'}]
          return expectSaga(sagas.processDataForm, fakeDataForm)
            .provide([[matchers.call.fn(qualificationUtil.splitDataForm), {actionDefinitions, dataFormColumns}]])
            .put(actions.setActionDefinitions(actionDefinitions))
            .put(actions.setDataFormColumns(dataFormColumns))
            .run()
        })
      })

      describe('update-value', () => {
        test('should update value', () => {
          return expectSaga(sagas.updateValue, actions.updateValue(123, 'node', 'value'))
            .provide([
              [select(sagas.inputSelector), {selection: []}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {}
                }
              ]
            ])
            .put(actions.setValue(123, 'node', 'value'))
            .put(actions.setCalculating(123, true))
            .put(actions.setCalculating(123, false))
            .call(rest.requestSaga, 'inputEdit/data', {
              method: 'POST',
              body: {
                inputDataKey: 123,
                node: 'node',
                value: 'value'
              }
            })
            .run()
        })
        test('should set newly calculated values', () => {
          return expectSaga(sagas.updateValue, actions.updateValue(123, 'node', 'value'))
            .provide([
              [select(sagas.inputSelector), {selection: []}],
              [select(sagas.inputEditSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    calculatedValues: [
                      {
                        inputDataKey: 122,
                        node: 'somenode',
                        value: 123
                      },
                      {
                        inputDataKey: 124,
                        node: 'another',
                        value: 'whatever'
                      }
                    ]
                  }
                }
              ]
            ])
            .put(actions.setValue(122, 'somenode', 123))
            .put(actions.setValue(124, 'another', 'whatever'))
            .run()
        })
      })
    })
  })
})
