import {all, call, put, select, take, takeEvery, takeLatest} from 'redux-saga/effects'
import {notification, rest, qualification} from 'tocco-app-extensions'
import {qualification as qualificationUtil} from 'tocco-util'

import * as inputEditActions from '../inputEdit/actions'
import {setTotalCount} from '../inputEditPagination/actions'

import * as actions from './actions'
import {transformResponseData} from './utils'

export const inputSelector = state => state.input
export const inputEditSelector = state => state.inputEdit
export const inputEditTableSelector = state => state.inputEditTable
export const inputEditSearchSelector = state => state.inputEditSearch
export const searchQueriesSelector = state => state.inputEditSearch.searchQueries
export const inputEditPaginationSelector = state => state.inputEditPagination

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_TABLE, initialize),
    takeLatest(actions.LOAD_DATA, loadData),
    takeEvery(actions.UPDATE_VALUE, updateValue)
  ])
}

export function* processDataForm(dataForm, readonly, readonlyActions = [], hiddenActions = []) {
  const {actionDefinitions, dataFormColumns} = yield call(
    qualificationUtil.splitDataForm,
    dataForm,
    readonly,
    readonlyActions,
    hiddenActions
  )
  yield put(actions.setActionDefinitions(actionDefinitions))
  yield put(actions.setDataFormColumns(dataFormColumns))
}

export function* initialize() {
  const {actionProperties, selection} = yield select(inputSelector)
  const inputEditDataForm =
    actionProperties && actionProperties.inputEditDataForm ? actionProperties.inputEditDataForm : 'Input_edit_data'
  const [editFormDefinition, dataForm] = yield all([
    call(rest.requestSaga, 'inputEdit/form', {method: 'POST', body: selection}),
    call(rest.fetchForm, inputEditDataForm, 'list')
  ])
  const {editColumns, readonly, readonlyActions, hiddenActions} = editFormDefinition.body
  if (readonly) {
    editColumns.forEach(c => (c.readonly = true))
    yield put(
      notification.toaster({
        type: 'info',
        title: 'client.actions.InputEdit.input_closed'
      })
    )
  }
  yield put(actions.setEditForm({inputEditForm: editColumns}))
  yield call(processDataForm, dataForm, readonly, readonlyActions, hiddenActions)
  yield put(actions.setInitialized(true))
}

export function* loadData({payload: {page, finishedLoading} = {}} = {}) {
  const {updateInProgress} = yield select(inputEditSelector)
  if (updateInProgress) {
    yield take(({payload, type}) => type === inputEditActions.SET_UPDATE_IN_PROGRESS && !payload.updateInProgress)
  }

  const {initialized} = yield select(inputEditTableSelector)
  if (!initialized) {
    yield take(({type}) => type === actions.SET_INITIALIZED)
  }

  const {selection} = yield select(inputSelector)
  const {dataFormColumns, sorting} = yield select(inputEditTableSelector)
  const searchQueries = yield select(searchQueriesSelector)
  const {recordsPerPage} = yield select(inputEditPaginationSelector)
  const response = yield call(
    qualification.requestData,
    'inputEdit/data/search',
    selection,
    sorting,
    dataFormColumns,
    searchQueries,
    recordsPerPage,
    page || 1
  )

  const transformedData = yield call(transformResponseData, response)
  yield put(actions.setData(transformedData))
  yield put(setTotalCount(response.body.count))
  if (finishedLoading) {
    yield call(finishedLoading)
  }
}

export function* updateValue({payload: {inputDataKey, node, value}}) {
  yield put(actions.setValue(inputDataKey, node, value))
  yield put(actions.setCalculating(inputDataKey, true))

  const response = yield call(rest.requestSaga, 'inputEdit/data', {
    method: 'POST',
    body: {
      inputDataKey,
      node,
      value
    }
  })
  yield handleResponse(response, inputDataKey)
}

function* handleResponse(response, inputDataKey) {
  const values = response.body.calculatedValues
  if (values) {
    const actionsToPut = values.map(value => actions.setValue(value.inputDataKey, value.node, value.value))
    yield all(actionsToPut.map(action => put(action)))
  }
  yield put(actions.setCalculating(inputDataKey, false))
}
