export const SET_TOTAL_COUNT = 'inputEditPagination/SET_TOTAL_COUNT'

export const setTotalCount = totalCount => ({
  type: SET_TOTAL_COUNT,
  payload: {
    totalCount
  }
})
