import {all, takeLatest, call, put, select} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import * as actions from '../inputEditSearch/actions'
import {loadData} from '../inputEditTable/sagas'

export const inputEditSelector = state => state.inputEdit
export const inputEditSearchSelector = state => state.inputEditSearch

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_SEARCH, initialize),
    takeLatest(actions.SET_SEARCH_QUERIES, setSearchQueries)
  ])
}

export function* initialize() {
  const actionProperties = yield select(inputEditSelector)
  const inputEditSearchDataForm =
    actionProperties && actionProperties.inputEditSearchDataForm
      ? actionProperties.inputEditSearchDataForm
      : 'Input_edit_data'
  const searchForm = yield call(rest.fetchForm, inputEditSearchDataForm, 'search')
  yield put(actions.setForm(searchForm))
}

export function* setSearchQueries() {
  const {initialized} = yield select(inputEditSearchSelector)
  if (initialized) {
    yield call(loadData)
  } else {
    yield put(actions.setInitialized(true))
  }
}
