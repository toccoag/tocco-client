import {v4 as uuid} from 'uuid'

import InputEditApp from './main'

export default {
  title: 'Actions/Input Edit',
  component: InputEditApp,
  argTypes: {
    selection: {
      type: 'object',
      defaultValue: {type: 'ID', entityName: 'Input', ids: ['11']}
    }
  }
}

export const Basic = args => <InputEditApp key={uuid()} {...args} />
