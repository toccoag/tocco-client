import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {actions, notification, selection as selectionPropType} from 'tocco-app-extensions'
import {
  SidepanelMainContent,
  Sidepanel,
  SidepanelContainer,
  SidepanelHeader,
  BackButton,
  ActionBarMainContent,
  ActionBar
} from 'tocco-ui'
import {env} from 'tocco-util'

import InputEditSearch from '../InputEditSearch'
import InputEditTable from '../InputEditTable/InputEditTableContainer'

import {StyledPaneWrapper} from './StyledInputEdit'

const InputEdit = ({
  selection,
  onActionClick,
  handleNotifications,
  initializeTable,
  initializeSearch,
  actionDefinitions
}) => {
  useEffect(() => {
    initializeTable()
    initializeSearch()
  }, [selection, initializeTable, initializeSearch])

  const [isCollapsed, setIsCollapsed] = useState(false)

  const customRenderedActions = {
    cancel: definition => (
      <BackButton onClick={() => onActionClick(definition)} icon={definition.icon} label={definition.label} />
    )
  }

  const Actions = actionDefinitions.map(definition => (
    <actions.Action
      key={definition.id}
      definition={definition}
      selection={selection}
      customRenderedActions={customRenderedActions}
    />
  ))

  const isAdmin = env.isInAdminEmbedded()

  return (
    <>
      {handleNotifications && <notification.Notifications />}
      <StyledPaneWrapper>
        <SidepanelContainer
          sidepanelPosition={isAdmin ? 'left' : 'top'}
          sidepanelCollapsed={isCollapsed}
          setSidepanelCollapsed={setIsCollapsed}
          scrollBehaviour={isAdmin ? 'inline' : 'none'}
        >
          <Sidepanel>
            <SidepanelHeader />
            <InputEditSearch />
          </Sidepanel>

          <SidepanelMainContent actionBar>
            <ActionBar>{Actions}</ActionBar>
            <ActionBarMainContent>
              <InputEditTable />
            </ActionBarMainContent>
          </SidepanelMainContent>
        </SidepanelContainer>
      </StyledPaneWrapper>
    </>
  )
}

InputEdit.propTypes = {
  selection: selectionPropType.propType.isRequired,
  handleNotifications: PropTypes.bool,
  initializeTable: PropTypes.func.isRequired,
  initializeSearch: PropTypes.func.isRequired,
  actionDefinitions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  ),
  onActionClick: PropTypes.func.isRequired,
  notify: PropTypes.func.isRequired
}

export default InputEdit
