import PropTypes from 'prop-types'
import {useEffect, useState, useMemo} from 'react'
import {form} from 'tocco-app-extensions'
import SimpleTableFormApp from 'tocco-simple-table-form/src/main'
import {api, env, keyboard} from 'tocco-util'

import {isInputField} from '../../modules/inputEditTable/utils'

import InputCellRenderer from './InputCellRenderer'
import {StyledInputEditTableWrapper} from './StyledComponents'

export const createTableFormDefinition = (dataColumns, inputColumns) => {
  const formDefinition = form.createSimpleForm({
    children: [
      form.createTable({
        children: [
          ...dataColumns.map(dataColumn => {
            const column = dataColumn.children[0]
            return form.createColumn({
              ...dataColumn,
              id: column.id,
              dataType: column.dataType,
              alignment: api.getColumnAlignment(column),
              readonly: true,
              sorting: {sortable: dataColumn.sortable}
            })
          }),
          ...inputColumns.map(column =>
            form.createColumn({
              label: column.label,
              id: column.id,
              dataType: column.dataType,
              readonly: column.readonly,
              validation: {
                ...(column.decimalDigits ? {decimalDigits: column.decimalDigits} : {}),
                ...(column.numberRange ? {numberRange: column.numberRange} : {})
              },
              ...(column.choiceOptions ? {options: column.choiceOptions} : {}),
              clientRenderer: 'input',
              alignment: api.getColumnAlignment(column),
              dynamic: true
            })
          )
        ]
      })
    ]
  })

  return formDefinition
}

const InputEditTable = ({
  data,
  dataFormColumns,
  inputEditForm,
  updateValue,
  loadData,
  dataLoadingInProgress,
  totalCount,
  recordsPerPage
}) => {
  const [formDefinition, setFormDefinition] = useState(null)

  useEffect(() => {
    const newFormDefinition = createTableFormDefinition(dataFormColumns, inputEditForm)
    setFormDefinition(newFormDefinition)
  }, [dataFormColumns, inputEditForm])

  const cellRenderers = useMemo(() => ({input: InputCellRenderer}), [])

  const isAdmin = env.isInAdminEmbedded()

  if (formDefinition) {
    const focusableFields = inputEditForm.filter(({id}) => isInputField(id)).map(({id}) => id)
    return (
      <StyledInputEditTableWrapper onKeyDown={keyboard.navigateTable(focusableFields)} data-cy="input-edit-table">
        <SimpleTableFormApp
          formDefinition={formDefinition}
          data={data}
          cellRenderers={cellRenderers}
          onChange={({key, field, value}) => {
            updateValue(key, field, value)
          }}
          recordsPerPage={recordsPerPage}
          totalCount={totalCount}
          dataLoadingInProgress={dataLoadingInProgress}
          onLoadData={loadData}
          scrollBehaviour={isAdmin ? 'inline' : 'none'}
        />
      </StyledInputEditTableWrapper>
    )
  } else {
    return null
  }
}

InputEditTable.propTypes = {
  data: PropTypes.array.isRequired,
  dataFormColumns: PropTypes.arrayOf(PropTypes.shape({id: PropTypes.string})),
  inputEditForm: PropTypes.array.isRequired,
  updateValue: PropTypes.func.isRequired,
  dataLoadingInProgress: PropTypes.bool,
  totalCount: PropTypes.number,
  recordsPerPage: PropTypes.number.isRequired,
  loadData: PropTypes.func
}

export default InputEditTable
