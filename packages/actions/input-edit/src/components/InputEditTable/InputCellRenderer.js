import PropTypes from 'prop-types'
import {tableForm} from 'tocco-app-extensions'
import {LoadingSpinner} from 'tocco-ui'

const InputCellRenderer = props => {
  const {
    column: {readOnly},
    rowData: {loading}
  } = props
  if (readOnly && loading > 0) {
    return <LoadingSpinner size="1.8em" icon="circle-notch-light" />
  }
  return <tableForm.EditableCellRenderer {...props} />
}

InputCellRenderer.propTypes = {
  column: PropTypes.shape({
    readOnly: PropTypes.bool
  }),
  rowData: PropTypes.shape({
    loading: PropTypes.number
  })
}

export default InputCellRenderer
