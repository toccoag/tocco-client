import styled from 'styled-components'

export const StyledInputEditTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  td {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`
