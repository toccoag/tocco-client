import {connect} from 'react-redux'

import {updateValue, loadData} from '../../modules/inputEditTable/actions'

import InputEditTable from './InputEditTable'

const mapActionCreators = {
  updateValue,
  loadData
}

const mapStateToProps = state => {
  return {
    data: state.inputEditTable.data,
    dataFormColumns: state.inputEditTable.dataFormColumns,
    inputEditForm: state.inputEditTable.inputEditForm,
    dataLoadingInProgress: state.inputEditTable.dataLoadingInProgress,
    totalCount: state.inputEditPagination.totalCount,
    recordsPerPage: state.inputEditPagination.recordsPerPage
  }
}

export default connect(mapStateToProps, mapActionCreators)(InputEditTable)
