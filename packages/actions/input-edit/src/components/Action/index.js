import {lazy} from 'react'
import {actions} from 'tocco-app-extensions'

const actionMap = {
  'input-edit-info': lazy(() => import(/* webpackChunkName: "actions" */ './actions/InputEditInfo')),
  'exam-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ExamEdit'))
}

const LazyAction = actions.actionFactory(actionMap)

export default LazyAction
