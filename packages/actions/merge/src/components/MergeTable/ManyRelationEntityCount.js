import PropTypes from 'prop-types'
import {Typography} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

const ManyRelationEntityCount = ({entityName, entityKey, relationName, totalKeys, navigationStrategy}) => {
  if (totalKeys === 0) {
    return <Typography.Span>{totalKeys}</Typography.Span>
  } else {
    return (
      <navigationStrategy.RelationLink
        entityKey={entityKey}
        entityName={entityName}
        relationName={relationName}
        target="_blank"
      >
        ({totalKeys})
      </navigationStrategy.RelationLink>
    )
  }
}

ManyRelationEntityCount.propTypes = {
  entityName: PropTypes.string.isRequired,
  entityKey: PropTypes.string.isRequired,
  relationName: PropTypes.string.isRequired,
  totalKeys: PropTypes.number.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export default ManyRelationEntityCount
