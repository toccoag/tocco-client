import {action} from 'tocco-util'

import {loadUuid, setUuid} from './modules/evaluationExecutionParticipationAction/actions'

export const getDispatchActions = input => action.getDispatchActions(input, actionSettings)

const actionSettings = [
  {
    name: 'selection',
    action: loadUuid,
    argsFactory: input => [input.selection]
  },
  {
    name: 'uuid',
    action: setUuid,
    argsFactory: input => [input.uuid]
  }
]
