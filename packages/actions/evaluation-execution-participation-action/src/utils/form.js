import _flatten from 'lodash/flatten'
import {form} from 'tocco-app-extensions'
import {remoteLogger} from 'tocco-util'

export const createEmptyForm = (id, readonly, children) => ({
  id,
  componentType: 'form',
  children: [
    {
      id: 'box1',
      label: null,
      componentType: 'layout',
      children: [
        {
          id: 'box1',
          label: null,
          componentType: 'layout',
          children,
          layoutType: 'vertical-box'
        }
      ],
      layoutType: 'horizontal-box'
    }
  ],
  readonly
})

export const createVerticalBox = (id, label, children) => ({
  id,
  label,
  componentType: 'layout',
  children,
  layoutType: 'vertical-box'
})

export const mapQuestion = (question, commentLabel, suggestionLabel) => [
  ...mapQuestionField(question, suggestionLabel),
  ...(question.commentField ? [mapCommentField(question, commentLabel)] : []),
  ..._flatten(question.parallelQuestions.map(q => mapQuestion(q, commentLabel)))
]

const mapQuestionField = (question, suggestionLabel) => {
  switch (question.type) {
    case 'textquestion':
      return [
        form.createTextField(
          `question_${question.key}`,
          mapTextValue(question.textAnswer, !question.writable),
          question.question,
          question.required,
          !question.writable
        )
      ]
    case 'singlechoice':
      return [
        form.createSingleChoiceField(
          `question_${question.key}`,
          mapChoiceOptions(question),
          question.question,
          question.required,
          !question.writable
        )
      ]
    case 'multiplechoice':
      return [
        form.createMultipleChoiceField(
          `question_${question.key}`,
          mapChoiceOptions(question),
          question.question,
          question.required,
          !question.writable
        )
      ]
    case 'singlechoicedropdown':
      return [
        form.createSelectField(
          `question_${question.key}`,
          mapSelectOptions(question),
          question.question,
          false,
          question.required,
          !question.writable
        )
      ]
    case 'multiplechoicedropdown':
      return [
        form.createSelectField(
          `question_${question.key}`,
          mapSelectOptions(question),
          question.question,
          true,
          question.required,
          !question.writable
        )
      ]
    case 'suggestedtextquestion':
      return [
        form.createSelectField(
          `suggestion_${question.key}`,
          mapSelectOptions(question),
          suggestionLabel,
          false,
          question.required,
          !question.writable
        ),
        form.createTextField(
          `question_${question.key}`,
          mapTextValue(question.textAnswer, !question.writable),
          question.question,
          question.required,
          !question.writable
        )
      ]
    default:
      remoteLogger.logError(`Type "${question.type}" is not supported`)
      return []
  }
}

const mapTextValue = (value, readonly) => {
  // display whitespace character else readonly field is hidden by form builder
  if (readonly) {
    return value || ' '
  }

  return value
}

const mapChoiceOptions = question =>
  question.answers.map(answer =>
    form.createChoiceOption(answer.key, {label: answer.label, checked: question.selectedValues.includes(answer.key)})
  )

const mapSelectOptions = question =>
  question.answers.map(answer =>
    form.createSelectOption(answer.key, answer.label, question.selectedValues.includes(answer.key))
  )

const mapCommentField = (question, commentLabel) =>
  form.createTextField(
    `comment_${question.key}`,
    mapTextValue(question.comment, !question.writable),
    commentLabel,
    false,
    !question.writable
  )

export const getShareableQuestions = evaluation =>
  _flatten(evaluation.questionCategories.map(c => c.questions)).filter(q => q.shareable)

export const getQuestionByKey = (evaluation, key) =>
  _flatten(evaluation.questionCategories.map(c => c.questions)).find(q => q.key === key)

export const getAnswerValue = (value, questionType) => {
  switch (questionType) {
    case 'textquestion':
    case 'suggestedtextquestion':
      return value
    case 'singlechoice':
      return value.options.find(o => o.checked)?.id || null
    case 'multiplechoice':
      return value.options.filter(o => o.checked).map(o => o.id)
    case 'singlechoicedropdown':
      return value.options.find(o => o.selected)?.key || null
    case 'multiplechoicedropdown':
      return value.options.filter(o => o.selected).map(o => o.key)
    default:
      remoteLogger.logError(`Type "${questionType}" is not supported`)
      return null
  }
}
