import {remoteLogger} from 'tocco-util'

import {createEmptyForm, createVerticalBox, mapQuestion, getAnswerValue} from './form'

describe('evaluation-execution-participation-action', () => {
  describe('utils', () => {
    describe('forms', () => {
      describe('createEmptyForm', () => {
        test('createEmptyForm', () => {
          const expected = {
            id: 'my-id',
            componentType: 'form',
            children: [
              {
                id: 'box1',
                label: null,
                componentType: 'layout',
                children: [
                  {
                    id: 'box1',
                    label: null,
                    componentType: 'layout',
                    children: [],
                    layoutType: 'vertical-box'
                  }
                ],
                layoutType: 'horizontal-box'
              }
            ],
            readonly: true
          }
          expect(createEmptyForm('my-id', true, [])).to.deep.eql(expected)
        })
      })

      describe('createVerticalBox', () => {
        test('createVerticalBox', () => {
          const expected = {
            id: 'my-id',
            label: 'my-label',
            componentType: 'layout',
            children: [],
            layoutType: 'vertical-box'
          }
          expect(createVerticalBox('my-id', 'my-label', [])).to.deep.eql(expected)
        })
      })

      describe('mapQuestion', () => {
        test('textquestion', () => {
          const question = {
            key: '2301',
            question: 'text label',
            type: 'textquestion',
            required: true,
            writable: true,
            shareable: true,
            commentField: false,
            selectedValues: [],
            textAnswer: 'current answer',
            comment: 'current comment',
            answers: [],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2301',
                label: 'text label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2301',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2301',
                    dataType: 'text',
                    validation: {
                      mandatory: true
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2301: {
                  type: 'text',
                  value: 'current answer',
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('singlechoice', () => {
          const question = {
            key: '2297',
            question: 'single-choice label',
            type: 'singlechoice',
            required: true,
            writable: true,
            shareable: false,
            commentField: false,
            selectedValues: ['2319'],
            textAnswer: '',
            comment: '',
            answers: [
              {key: '2318', label: 'Yes'},
              {key: '2319', label: 'No'}
            ],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2297',
                label: 'single-choice label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2297',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2297',
                    dataType: 'choice',
                    validation: {
                      mandatory: true
                    },
                    selectionType: 'single'
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2297: {
                  type: 'choice',
                  value: {
                    options: [
                      {
                        id: '2318',
                        label: 'Yes',
                        checked: false
                      },
                      {
                        id: '2319',
                        label: 'No',
                        checked: true
                      }
                    ]
                  },
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('multiplechoice', () => {
          const question = {
            key: '2300',
            question: 'multiple-choice label',
            type: 'multiplechoice',
            required: true,
            writable: true,
            shareable: false,
            commentField: false,
            selectedValues: ['2324', '2325'],
            textAnswer: '',
            comment: '',
            answers: [
              {key: '2324', label: 'First option'},
              {key: '2325', label: 'Second option'}
            ],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2300',
                label: 'multiple-choice label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2300',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2300',
                    dataType: 'choice',
                    validation: {
                      mandatory: true
                    },
                    selectionType: 'multiple'
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2300: {
                  type: 'choice',
                  value: {
                    options: [
                      {
                        id: '2324',
                        label: 'First option',
                        checked: true
                      },
                      {
                        id: '2325',
                        label: 'Second option',
                        checked: true
                      }
                    ]
                  },
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('singlechoicedropdown', () => {
          const question = {
            key: '2299',
            question: 'single-choice dropdown label',
            type: 'singlechoicedropdown',
            required: true,
            writable: true,
            shareable: false,
            commentField: false,
            selectedValues: ['2322'],
            textAnswer: '',
            comment: '',
            answers: [
              {key: '2322', label: 'Yes'},
              {key: '2323', label: 'No'}
            ],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2299',
                label: 'single-choice dropdown label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2299',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2299',
                    dataType: 'pseudo-select',
                    validation: {
                      mandatory: true
                    },
                    isMulti: false
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2299: {
                  type: 'pseudo-select',
                  value: {
                    options: [
                      {
                        key: '2322',
                        display: 'Yes',
                        selected: true
                      },
                      {
                        key: '2323',
                        display: 'No',
                        selected: false
                      }
                    ]
                  },
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('multiplechoicedropdown', () => {
          const question = {
            key: '2298',
            question: 'multiple-choice dropdown label',
            type: 'multiplechoicedropdown',
            required: true,
            writable: true,
            shareable: false,
            commentField: false,
            selectedValues: ['2321'],
            textAnswer: '',
            comment: '',
            answers: [
              {key: '2320', label: 'First Option'},
              {key: '2321', label: 'Second Option'}
            ],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2298',
                label: 'multiple-choice dropdown label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2298',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2298',
                    dataType: 'pseudo-select',
                    validation: {
                      mandatory: true
                    },
                    isMulti: true
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2298: {
                  type: 'pseudo-select',
                  value: {
                    options: [
                      {
                        key: '2320',
                        display: 'First Option',
                        selected: false
                      },
                      {
                        key: '2321',
                        display: 'Second Option',
                        selected: true
                      }
                    ]
                  },
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('suggestedtextquestion', () => {
          const question = {
            key: '2302',
            question: 'text with suggestion label',
            type: 'suggestedtextquestion',
            required: false,
            writable: true,
            shareable: false,
            commentField: false,
            selectedValues: [],
            textAnswer: 'Current Answer',
            comment: '',
            answers: [{key: '2326', label: 'Suggestion'}],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_suggestion_2302',
                label: 'suggestions label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_suggestion_2302',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_suggestion_2302',
                    dataType: 'pseudo-select',
                    validation: {
                      mandatory: false
                    },
                    isMulti: false
                  }
                ]
              },
              entityPath: {
                pseudoField_suggestion_2302: {
                  type: 'pseudo-select',
                  value: {
                    options: [
                      {
                        key: '2326',
                        display: 'Suggestion',
                        selected: false
                      }
                    ]
                  },
                  writable: true
                }
              }
            },
            {
              formField: {
                id: 'pseudoField_question_2302',
                label: 'text with suggestion label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2302',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2302',
                    dataType: 'text',
                    validation: {
                      mandatory: false
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2302: {
                  type: 'text',
                  value: 'Current Answer',
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('show comment field', () => {
          const question = {
            key: '2301',
            question: 'text label',
            type: 'textquestion',
            required: true,
            writable: true,
            shareable: true,
            commentField: true,
            selectedValues: [],
            textAnswer: 'current answer',
            comment: 'current comment',
            answers: [],
            parallelQuestions: []
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2301',
                label: 'text label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2301',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2301',
                    dataType: 'text',
                    validation: {
                      mandatory: true
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2301: {
                  type: 'text',
                  value: 'current answer',
                  writable: true
                }
              }
            },
            {
              formField: {
                id: 'pseudoField_comment_2301',
                label: 'comment label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_comment_2301',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_comment_2301',
                    dataType: 'text',
                    validation: {
                      mandatory: false
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_comment_2301: {
                  type: 'text',
                  value: 'current comment',
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('show parallel question', () => {
          const question = {
            key: '2301',
            question: 'text label',
            type: 'textquestion',
            required: true,
            writable: true,
            shareable: true,
            commentField: false,
            selectedValues: [],
            textAnswer: 'current answer',
            comment: 'current comment',
            answers: [],
            parallelQuestions: [
              {
                key: '9999',
                question: 'parallel question label',
                type: 'textquestion',
                required: false,
                writable: false,
                shareable: false,
                commentField: false,
                selectedValues: [],
                textAnswer: 'parallel answer',
                comment: '',
                answers: [],
                parallelQuestions: []
              }
            ]
          }
          const expected = [
            {
              formField: {
                id: 'pseudoField_question_2301',
                label: 'text label',
                componentType: 'field-set',
                readonly: false,
                children: [
                  {
                    id: 'pseudoField_question_2301',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_2301',
                    dataType: 'text',
                    validation: {
                      mandatory: true
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_question_2301: {
                  type: 'text',
                  value: 'current answer',
                  writable: true
                }
              }
            },
            {
              formField: {
                id: 'pseudoField_question_9999',
                label: 'parallel question label',
                componentType: 'field-set',
                readonly: true,
                children: [
                  {
                    id: 'pseudoField_question_9999',
                    componentType: 'field',
                    pseudoField: true,
                    path: 'pseudoField_question_9999',
                    dataType: 'text',
                    validation: {
                      mandatory: false
                    }
                  }
                ]
              },
              entityPath: {
                pseudoField_question_9999: {
                  type: 'text',
                  value: 'parallel answer',
                  writable: true
                }
              }
            }
          ]
          expect(mapQuestion(question, 'comment label', 'suggestions label')).to.be.eql(expected)
        })

        test('invalid type', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          const question = {
            type: 'otherType',
            parallelQuestions: []
          }

          mapQuestion(question, 'comment label', 'suggestions label')

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Type "otherType" is not supported')
        })
      })

      describe('getAnswerValue', () => {
        test('textquestion', () => {
          const value = 'text string'
          const expected = 'text string'
          expect(getAnswerValue(value, 'textquestion')).to.be.eql(expected)
        })

        test('suggestedtextquestion', () => {
          const value = 'text string'
          const expected = 'text string'
          expect(getAnswerValue(value, 'suggestedtextquestion')).to.be.eql(expected)
        })

        test('singlechoice', () => {
          const value = {
            options: [
              {id: '1', checked: true},
              {id: '2', checked: false}
            ]
          }
          const expected = '1'
          expect(getAnswerValue(value, 'singlechoice')).to.be.eql(expected)
        })

        test('singlechoice nothing selected', () => {
          const value = {
            options: [
              {id: '1', checked: false},
              {id: '2', checked: false}
            ]
          }
          const expected = null
          expect(getAnswerValue(value, 'singlechoice')).to.be.eql(expected)
        })

        test('multiplechoice', () => {
          const value = {
            options: [
              {id: '1', checked: true},
              {id: '2', checked: true}
            ]
          }
          const expected = ['1', '2']
          expect(getAnswerValue(value, 'multiplechoice')).to.be.eql(expected)
        })

        test('multiplechoice nothing selected', () => {
          const value = {
            options: [
              {id: '1', checked: false},
              {id: '2', checked: false}
            ]
          }
          const expected = []
          expect(getAnswerValue(value, 'multiplechoice')).to.be.eql(expected)
        })

        test('singlechoicedropdown', () => {
          const value = {
            options: [
              {key: '1', selected: true},
              {key: '2', selected: false}
            ]
          }
          const expected = '1'
          expect(getAnswerValue(value, 'singlechoicedropdown')).to.be.eql(expected)
        })

        test('singlechoicedropdown nothing selected', () => {
          const value = {
            options: [
              {key: '1', selected: false},
              {key: '2', selected: false}
            ]
          }
          const expected = null
          expect(getAnswerValue(value, 'singlechoicedropdown')).to.be.eql(expected)
        })

        test('multiplechoicedropdown', () => {
          const value = {
            options: [
              {key: '1', selected: true},
              {key: '2', selected: true}
            ]
          }
          const expected = ['1', '2']
          expect(getAnswerValue(value, 'multiplechoicedropdown')).to.be.eql(expected)
        })

        test('multiplechoicedropdown nothing selected', () => {
          const value = {
            options: [
              {key: '1', selected: false},
              {key: '2', selected: false}
            ]
          }
          const expected = []
          expect(getAnswerValue(value, 'multiplechoicedropdown')).to.be.eql(expected)
        })

        test('invalid type', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          getAnswerValue(null, 'otherType')

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Type "otherType" is not supported')
        })
      })
    })
  })
})
