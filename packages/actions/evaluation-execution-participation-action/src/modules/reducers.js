import {reducer as form} from 'redux-form'

import evaluationExecutionParticipationActionReducer from './evaluationExecutionParticipationAction/reducer'
import evaluationExecutionParticipationActionSagas from './evaluationExecutionParticipationAction/sagas'

export default {
  evaluationExecutionParticipationAction: evaluationExecutionParticipationActionReducer,
  form
}

export const sagas = [evaluationExecutionParticipationActionSagas]
