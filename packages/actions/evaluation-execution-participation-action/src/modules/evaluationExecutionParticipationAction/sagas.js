import {FormattedMessage} from 'react-intl'
import {actions as formActions, getFormValues} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {channel} from 'redux-saga'
import {all, call, delay, put, select, take, takeLatest} from 'redux-saga/effects'
import {appFactory, externalEvents, form, notification, rest, selection as selectionUtil} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ParticipationForm'
import ShareAnswerModal from '../../components/ShareAnswerModal/ShareAnswerModal'
import {getDispatchActions} from '../../input'
import {
  createEmptyForm,
  createVerticalBox,
  getShareableQuestions,
  mapQuestion,
  getQuestionByKey,
  getAnswerValue
} from '../../utils/form'

import * as actions from './actions'

export const evaluationExecutionParticipationActionSelector = state => state.evaluationExecutionParticipationAction
export const inputSelector = state => state.input
export const textResourceSelector = (state, key) => state.intl.messages[key]

export const PSEUDO_FIELD_SUGGESTION_PREFIX = 'pseudoField_suggestion_'
export const PSEUDO_FIELD_COMMENT_PREFIX = 'pseudoField_comment_'
export const PSEUDO_FIELD_QUESTION_PREFIX = 'pseudoField_question_'

export default function* sagas() {
  yield all([
    takeLatest(appFactory.INPUT_CHANGED, inputChanged),
    takeLatest(actions.LOAD_UUID, loadUuid),
    takeLatest(actions.LOAD_FORM_INFO, loadFormInfo),
    takeLatest(actions.SUBMIT_FORM, submitForm),
    takeLatest(actions.DISPLAY_SHARE_ANSWER_MODAL, displayShareAnswerModal),
    takeLatest(actions.FINISH_EVALUATION, finishEvaluation),
    takeLatest(formActionTypes.CHANGE, onChange)
  ])
}

export function* inputChanged({payload: {input}}) {
  const derivedActions = getDispatchActions(input)
  yield all(derivedActions.map(action => put(action)))
}

export function* loadUuid({payload: {selection}}) {
  const key = selectionUtil.getSingleKey(selection, 'Evaluation_party_assignment')
  const entity = yield call(rest.fetchEntity, 'Evaluation_party_assignment', key, {paths: ['uuid']})
  yield put(actions.setUuid(entity.paths.uuid.value))
}

export function* loadFormInfo() {
  const evaluation = yield call(loadEvaluation)

  const children = []
  for (const c of evaluation.questionCategories) {
    children.push(createVerticalBox(c.label, c.label, yield call(mapCategory, c.questions)))
  }
  const formDefinition = createEmptyForm('pseudoFormDefinition', evaluation.readOnly, children)
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)
  const {actionProperties} = yield select(inputSelector)
  const modifiedFormDefinition = yield call(addReportsToForm, actionProperties?.reportDisplayId, formDefinition)

  yield all([
    put(actions.setFormDefinition(modifiedFormDefinition)),
    put(actions.setFieldDefinitions(fieldDefinitions))
  ])

  const {pseudoEntityPaths} = yield select(evaluationExecutionParticipationActionSelector)
  const entityValues = yield call(api.getFlattenEntity, {model: '', paths: {...pseudoEntityPaths}})
  const formValues = yield call(form.entityToFormValues, entityValues, fieldDefinitions)
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))

  yield put(actions.setLoaded(true))
}

export function* loadEvaluation() {
  const {uuid} = yield select(evaluationExecutionParticipationActionSelector)

  const response = yield call(rest.requestSaga, `widgets/evaluationExecutionParticipation/${uuid}`, {
    acceptedErrorCodes: ['LOADING_EVALUATION_FAILED']
  })
  if (response.status === 200) {
    const evaluation = response.body
    yield put(actions.setEvaluation(evaluation))
    return evaluation
  } else if (response.body.errorCode === 'LOADING_EVALUATION_FAILED') {
    yield put(notification.toaster({type: 'error', title: response.body.message}))
  }
}

export function* mapCategory(questions) {
  const boxes = []

  const commentLabel = yield select(
    textResourceSelector,
    'client.actions.evaluation-execution-participation-action.comment'
  )
  const suggestionLabel = yield select(
    textResourceSelector,
    'client.actions.evaluation-execution-participation-action.suggestion'
  )
  for (const question of questions) {
    const pseudoFields = [...mapQuestion(question, commentLabel, suggestionLabel)]
    yield put(actions.addPseudoEntityPaths(form.getMergedEntityPaths(pseudoFields)))
    boxes.push(
      createVerticalBox(
        question.key,
        ' ',
        pseudoFields.map(f => f.formField)
      )
    )
  }

  return boxes
}

export function* addReportsToForm(reportDisplayId, formDefinition) {
  if (reportDisplayId) {
    const queries = yield call(getReportDisplayQueries, reportDisplayId)
    const fullfilledReportDisplayQueriesKeys = yield call(evaluateConditions, queries)
    const reports = yield call(getReports, fullfilledReportDisplayQueriesKeys)

    const groupLabel = yield select(textResourceSelector, 'client.actiongroup.output')
    return yield call(form.addReports, formDefinition, reports, groupLabel)
  } else {
    return formDefinition
  }
}

export function* getReportDisplayQueries(reportDisplayId) {
  const reportDisplayQueries = yield call(
    rest.fetchAllEntities,
    'Report_display_query',
    {
      where: `relReport_display.unique_id =="${reportDisplayId}"`,
      paths: 'query'
    },
    {method: 'GET'}
  )

  return reportDisplayQueries.map(e => ({key: e.key, query: e.paths.query.value}))
}

export function* evaluateConditions(queries) {
  const {evaluation} = yield select(evaluationExecutionParticipationActionSelector)
  const fullFilledReportDisplayQueries = []
  for (const query of queries) {
    const fullfilled =
      (yield call(rest.fetchEntityCount, 'Evaluation_party_assignment', {
        where: `pk == ${evaluation.key} and (${query.query})`
      })) === 1
    if (fullfilled) {
      fullFilledReportDisplayQueries.push(query.key)
    }
  }
  return fullFilledReportDisplayQueries
}

export function* getReports(fullfilledReportDisplayQueriesKeys) {
  if (fullfilledReportDisplayQueriesKeys.length === 0) {
    return []
  }

  const reports = yield call(
    rest.fetchAllEntities,
    'Report',
    {
      where: `IN(relReport_display_query.pk, ${fullfilledReportDisplayQueriesKeys.join(', ')})`,
      paths: 'label,unique_id'
    },
    {method: 'GET'}
  )

  return reports.map(r => ({
    actionType: 'report',
    componentType: 'report',
    id: r.paths.unique_id.value,
    label: r.paths.label.value,
    reportId: r.paths.unique_id.value,
    showConfirmation: false,
    icon: 'file-pdf'
  }))
}

export function* submitForm() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))
  yield delay(250)
  yield put(formActions.stopSubmit(REDUX_FORM_NAME))
}

export function* displayShareAnswerModal() {
  const {evaluation} = yield select(evaluationExecutionParticipationActionSelector)
  const answerChannel = yield call(channel)

  yield put(
    notification.modal(
      'evaluation-execution-participation-action-share-answers',
      'client.actions.evaluation-execution-participation-action.shareAnswer.title',
      null,
      ({close}) => {
        const onOk = questionKeys => {
          close()
          answerChannel.put(questionKeys)
        }

        return <ShareAnswerModal questions={getShareableQuestions(evaluation)} onOk={onOk} />
      },
      true
    )
  )

  const selectedQuestionKeys = yield take(answerChannel)
  yield call(shareAnswers, selectedQuestionKeys)
}

export function* shareAnswers(questionKeys) {
  const {uuid} = yield select(evaluationExecutionParticipationActionSelector)

  const resource = `widgets/evaluationExecutionParticipation/${uuid}/shareAnswers`
  const options = {
    method: 'POST',
    body: {
      questionKeys
    }
  }

  const response = yield call(rest.requestSaga, resource, options)

  if (response.status === 204) {
    yield put(actions.removeShareableAnswers(questionKeys))

    const shareSuccess = yield select(
      textResourceSelector,
      'client.actions.evaluation-execution-participation-action.shareAnswer.success'
    )
    yield put(notification.toaster({type: 'success', title: shareSuccess}))
  }
}

export function* finishEvaluation() {
  const answerChannel = yield call(channel)
  const onYes = () => answerChannel.put(true)
  const onNo = () => answerChannel.put(false)
  const onCancel = () => answerChannel.put(false)

  yield put(
    notification.yesNoQuestion(
      <FormattedMessage id="client.actions.evaluation-execution-participation-action.finish.title" />,
      <FormattedMessage id="client.actions.evaluation-execution-participation-action.finish.message" />,
      <FormattedMessage id="client.common.yes" />,
      <FormattedMessage id="client.common.no" />,
      onYes,
      onNo,
      onCancel
    )
  )

  const confirmed = yield take(answerChannel)
  if (confirmed) {
    yield call(executeFinishEvaluation)
  }
}

export function* executeFinishEvaluation() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const {uuid} = yield select(evaluationExecutionParticipationActionSelector)
  const resource = `widgets/evaluationExecutionParticipation/${uuid}/finish`
  const options = {
    method: 'POST',
    acceptedErrorCodes: ['FINISHING_EVALUATION_FAILED']
  }

  const response = yield call(rest.requestSaga, resource, options)
  if (response.status === 204) {
    const {selection} = yield select(inputSelector)
    // if fullscreen action was opened from list navigate back to list
    // if fullscreen action was opened via uuid show empty page and fire visibility status
    if (selection) {
      yield put(externalEvents.fireExternalEvent('onCancel'))
    } else {
      yield put(externalEvents.fireExternalEvent('onSuccess'))
    }
  } else if (response.body.errorCode === 'FINISHING_EVALUATION_FAILED') {
    yield put(notification.toaster({type: 'error', title: response.body.message}))
  }

  yield put(formActions.stopSubmit(REDUX_FORM_NAME))
}

export function* onChange({payload, meta}) {
  if (meta.form === REDUX_FORM_NAME) {
    if (meta.field.startsWith(PSEUDO_FIELD_SUGGESTION_PREFIX)) {
      yield call(onChangeSuggestion, meta.field, payload)
    } else if (meta.field.startsWith(PSEUDO_FIELD_COMMENT_PREFIX)) {
      yield call(onChangeComment, meta.field, payload)
    } else if (meta.field.startsWith(PSEUDO_FIELD_QUESTION_PREFIX)) {
      yield call(onChangeAnswer, meta.field, payload)
    }
  }
}

export function* onChangeSuggestion(fieldName, payload) {
  const textFieldName = fieldName.replace(PSEUDO_FIELD_SUGGESTION_PREFIX, PSEUDO_FIELD_QUESTION_PREFIX)
  const formValues = yield call(loadFormValues, REDUX_FORM_NAME)
  const selectedSuggestion = payload.options.find(o => o.selected)?.display
  const newValue = `${formValues[textFieldName]} ${selectedSuggestion}`
  yield put(formActions.change(REDUX_FORM_NAME, textFieldName, newValue))
}

// helper function to allow easy mocking in test
export function* loadFormValues(formId) {
  return yield select(getFormValues(formId))
}

export function* onChangeComment(fieldName, comment) {
  const questionKey = fieldName.replace(PSEUDO_FIELD_COMMENT_PREFIX, '')
  yield call(saveChangedField, questionKey, {comment}, 'saveComment')
}

export function* onChangeAnswer(fieldName, payload) {
  const {evaluation} = yield select(evaluationExecutionParticipationActionSelector)
  const questionKey = fieldName.replace(PSEUDO_FIELD_QUESTION_PREFIX, '')
  const question = getQuestionByKey(evaluation, questionKey)
  const answer = getAnswerValue(payload, question.type)
  yield call(saveChangedField, questionKey, {answer}, 'saveAnswer')
}

export function* saveChangedField(questionKey, body, endpoint) {
  const {uuid} = yield select(evaluationExecutionParticipationActionSelector)
  const resource = `widgets/evaluationExecutionParticipation/${uuid}/questions/${questionKey}/${endpoint}`
  const options = {
    method: 'PUT',
    body
  }

  yield call(rest.requestSaga, resource, options)
}
