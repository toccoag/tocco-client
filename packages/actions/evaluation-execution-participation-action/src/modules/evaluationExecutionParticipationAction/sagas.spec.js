import {actions as formActions} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {channel} from 'redux-saga'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, appFactory, externalEvents, notification} from 'tocco-app-extensions'

import {REDUX_FORM_NAME} from '../../components/ParticipationForm'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-execution-participation-action', () => {
  describe('modules', () => {
    describe('evaluationExecutionParticipationAction', () => {
      describe('sagas', () => {
        const evaluation = {
          key: '2371',
          questionnaireLabel: 'stwu',
          description: 'xxxxxxxxxxxxxxxxxxxxx',
          user: 'Tocco AG, Support',
          sourceEntity: 'XYZ / xyz',
          readOnly: false,
          isFinishable: true,
          questionCategories: [
            {
              label: 'category label',
              questions: [
                {
                  key: '2301',
                  question: 'text label',
                  type: 'textquestion',
                  required: true,
                  writable: true,
                  shareable: true,
                  commentField: false,
                  selectedValues: [],
                  textAnswer: '',
                  comment: '',
                  answers: {},
                  parallelQuestions: []
                }
              ]
            }
          ]
        }

        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(appFactory.INPUT_CHANGED, sagas.inputChanged),
                takeLatest(actions.LOAD_UUID, sagas.loadUuid),
                takeLatest(actions.LOAD_FORM_INFO, sagas.loadFormInfo),
                takeLatest(actions.SUBMIT_FORM, sagas.submitForm),
                takeLatest(actions.DISPLAY_SHARE_ANSWER_MODAL, sagas.displayShareAnswerModal),
                takeLatest(actions.FINISH_EVALUATION, sagas.finishEvaluation),
                takeLatest(formActionTypes.CHANGE, sagas.onChange)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('inputChanged', () => {
          test('should loadUuid from selection', () => {
            const selection = {ids: ['1'], entityName: 'Evaluation_party_assignment', type: 'ID'}
            return expectSaga(sagas.inputChanged, {
              payload: {input: {selection}}
            })
              .put(actions.loadUuid(selection))
              .run()
          })

          test('should set uuid from uuid', () => {
            const uuid = '6f1803a0-8742-4747-8a1f-b4a875aed4fa'
            return expectSaga(sagas.inputChanged, {
              payload: {input: {uuid}}
            })
              .put(actions.setUuid('6f1803a0-8742-4747-8a1f-b4a875aed4fa'))
              .run()
          })
        })

        describe('loadUuid', () => {
          test('should loadUuid from selection', () => {
            const selection = {ids: ['1'], entityName: 'Evaluation_party_assignment', type: 'ID'}
            const response = {
              paths: {
                uuid: {
                  value: '6f1803a0-8742-4747-8a1f-b4a875aed4fa'
                }
              }
            }
            return expectSaga(sagas.loadUuid, {payload: {selection}})
              .provide([
                [matchers.call(rest.fetchEntity, 'Evaluation_party_assignment', '1', {paths: ['uuid']}), response]
              ])
              .put(actions.setUuid('6f1803a0-8742-4747-8a1f-b4a875aed4fa'))
              .run()
          })
        })

        describe('loadFormInfo', () => {
          test('load evaluation and define form', () => {
            const pseudoEntityPaths = {
              pseudoField_question_2301: {
                type: 'text',
                value: '',
                writable: true
              }
            }
            const formDefinition = {
              id: 'pseudoFormDefinition',
              componentType: 'form',
              children: [
                {
                  id: 'box1',
                  label: null,
                  componentType: 'layout',
                  children: [
                    {
                      id: 'box1',
                      label: null,
                      componentType: 'layout',
                      children: [
                        {
                          id: 'category label',
                          label: 'category label',
                          componentType: 'layout',
                          children: [
                            {
                              id: '2301',
                              label: ' ',
                              componentType: 'layout',
                              children: [
                                {
                                  id: 'pseudoField_question_2301',
                                  label: 'text label',
                                  componentType: 'field-set',
                                  readonly: false,
                                  children: [
                                    {
                                      id: 'pseudoField_question_2301',
                                      componentType: 'field',
                                      pseudoField: true,
                                      path: 'pseudoField_question_2301',
                                      dataType: 'text',
                                      validation: {
                                        mandatory: true
                                      }
                                    }
                                  ]
                                }
                              ],
                              layoutType: 'vertical-box'
                            }
                          ],
                          layoutType: 'vertical-box'
                        }
                      ],
                      layoutType: 'vertical-box'
                    }
                  ],
                  layoutType: 'horizontal-box'
                }
              ],
              readonly: false
            }
            const fieldDefinitions = [
              {
                id: 'pseudoField_question_2301',
                componentType: 'field',
                pseudoField: true,
                path: 'pseudoField_question_2301',
                dataType: 'text',
                validation: {
                  mandatory: true
                },
                readonly: false,
                ignoreCopy: undefined
              }
            ]
            const formValues = {
              __key: undefined,
              __model: '',
              __version: undefined,
              pseudoField_question_2301: ''
            }

            return expectSaga(sagas.loadFormInfo)
              .provide([
                [matchers.call(sagas.loadEvaluation), evaluation],
                [
                  select(
                    sagas.textResourceSelector,
                    'client.actions.evaluation-execution-participation-action.comment'
                  ),
                  'comment'
                ],
                [
                  select(
                    sagas.textResourceSelector,
                    'client.actions.evaluation-execution-participation-action.suggestion'
                  ),
                  'suggestion'
                ],
                [select(sagas.inputSelector), {actionProperties: {}}],
                [select(sagas.evaluationExecutionParticipationActionSelector), {pseudoEntityPaths}]
              ])
              .put(actions.addPseudoEntityPaths(pseudoEntityPaths))
              .put(actions.setFormDefinition(formDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .put(actions.setLoaded(true))
              .run()
          })
        })

        describe('loadEvaluation', () => {
          const uuid = '6f1803a0-8742-4747-8a1f-b4a875aed4fa'
          const resource = 'widgets/evaluationExecutionParticipation/6f1803a0-8742-4747-8a1f-b4a875aed4fa'
          const options = {
            acceptedErrorCodes: ['LOADING_EVALUATION_FAILED']
          }
          test('load evaluation sucessfully', () => {
            const response = {
              status: 200,
              body: {...evaluation}
            }
            return expectSaga(sagas.loadEvaluation)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [matchers.call(rest.requestSaga, resource, options), response]
              ])
              .put(actions.setEvaluation(evaluation))
              .run()
          })

          test('loading evaluation failed', () => {
            const response = {
              status: 400,
              body: {
                errorCode: 'LOADING_EVALUATION_FAILED',
                message: 'execution already done'
              }
            }
            return expectSaga(sagas.loadEvaluation)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [matchers.call(rest.requestSaga, resource, options), response]
              ])
              .put(notification.toaster({type: 'error', title: 'execution already done'}))
              .run()
          })
        })

        describe('addReportsToForm', () => {
          const formDefinition = {
            children: []
          }

          test('add report to form and create main action bar', () => {
            const reportDisplayId = 'report-display-id'
            const queries = {}
            const fullfilledReportDisplayQueriesKeys = {}
            const reports = [
              {
                actionType: 'report',
                componentType: 'report',
                id: 'my-report',
                label: 'my-report',
                reportId: 'my-report'
              }
            ]
            return expectSaga(sagas.addReportsToForm, reportDisplayId, formDefinition)
              .provide([
                [matchers.call.fn(sagas.getReportDisplayQueries, reportDisplayId), queries],
                [matchers.call.fn(sagas.evaluateConditions, queries), fullfilledReportDisplayQueriesKeys],
                [matchers.call.fn(sagas.getReports, fullfilledReportDisplayQueriesKeys), reports],
                [select(sagas.textResourceSelector, 'client.actiongroup.output'), 'output']
              ])
              .returns({
                children: [
                  {
                    id: 'main-action-bar',
                    componentType: 'action-bar',
                    children: [
                      {
                        actionType: 'report',
                        componentType: 'report',
                        id: 'my-report',
                        label: 'my-report',
                        reportId: 'my-report'
                      }
                    ]
                  }
                ]
              })
              .run()
          })

          test('do not modified form definition', () => {
            const reportDisplayId = undefined
            return expectSaga(sagas.addReportsToForm, reportDisplayId, formDefinition)
              .not.call.like({fn: sagas.getReportDisplayQueries})
              .returns(formDefinition)
              .run()
          })
        })

        describe('getReportDisplayQueries', () => {
          test('getReportDisplayQueries', () => {
            const reportDisplayId = 'report-display-id'
            const response = [
              {
                key: '123',
                paths: {
                  query: {
                    value: 'field == "abc"'
                  }
                }
              }
            ]
            const returnValue = [
              {
                key: '123',
                query: 'field == "abc"'
              }
            ]
            return expectSaga(sagas.getReportDisplayQueries, reportDisplayId)
              .provide([
                [
                  matchers.call(
                    rest.fetchAllEntities,
                    'Report_display_query',
                    {
                      where: 'relReport_display.unique_id =="report-display-id"',
                      paths: 'query'
                    },
                    {method: 'GET'}
                  ),
                  response
                ]
              ])
              .returns(returnValue)
              .run()
          })
        })

        describe('evaluateConditions', () => {
          test('evaluateConditions', () => {
            const queries = [
              {
                key: '123',
                query: 'field == "abc"'
              },
              {
                key: '456',
                query: 'field == "def"'
              }
            ]
            return expectSaga(sagas.evaluateConditions, queries)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {evaluation}],
                [
                  matchers.call(rest.fetchEntityCount, 'Evaluation_party_assignment', {
                    where: 'pk == 2371 and (field == "abc")'
                  }),
                  1
                ],
                [
                  matchers.call(rest.fetchEntityCount, 'Evaluation_party_assignment', {
                    where: 'pk == 2371 and (field == "def")'
                  }),
                  0
                ]
              ])
              .returns(['123'])
              .run()
          })
        })

        describe('getReports', () => {
          test('getReports', () => {
            const fullfilledReportDisplayQueriesKeys = ['123', '456']
            const response = [
              {
                paths: {
                  unique_id: {
                    value: 'first-report'
                  },
                  label: {
                    value: 'first-report-label'
                  }
                }
              },
              {
                paths: {
                  unique_id: {
                    value: 'second-report'
                  },
                  label: {
                    value: 'second-report-label'
                  }
                }
              }
            ]
            const returnValue = [
              {
                actionType: 'report',
                componentType: 'report',
                id: 'first-report',
                label: 'first-report-label',
                reportId: 'first-report',
                showConfirmation: false,
                icon: 'file-pdf'
              },
              {
                actionType: 'report',
                componentType: 'report',
                id: 'second-report',
                label: 'second-report-label',
                reportId: 'second-report',
                showConfirmation: false,
                icon: 'file-pdf'
              }
            ]
            return expectSaga(sagas.getReports, fullfilledReportDisplayQueriesKeys)
              .provide([
                [
                  matchers.call(
                    rest.fetchAllEntities,
                    'Report',
                    {
                      where: 'IN(relReport_display_query.pk, 123, 456)',
                      paths: 'label,unique_id'
                    },
                    {method: 'GET'}
                  ),
                  response
                ]
              ])
              .returns(returnValue)
              .run()
          })
        })

        describe('submitForm', () => {
          test('submitForm', () => {
            return expectSaga(sagas.submitForm)
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .delay(250)
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .run()
          })
        })

        describe('displayShareAnswerModal', () => {
          test('displayShareAnswerModal', () => {
            return expectSaga(sagas.displayShareAnswerModal)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {evaluation}],
                [channel, {}],
                {
                  take() {
                    return ['2301']
                  }
                },
                [matchers.call.fn(sagas.shareAnswers, ['2301']), {}]
              ])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'evaluation-execution-participation-action-share-answers',
                    title: 'client.actions.evaluation-execution-participation-action.shareAnswer.title',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .call.like({fn: channel})
              .run()
          })
        })

        describe('shareAnswers', () => {
          test('shareAnswers', () => {
            const uuid = '6f1803a0-8742-4747-8a1f-b4a875aed4fa'
            const resource = `widgets/evaluationExecutionParticipation/${uuid}/shareAnswers`
            const options = {
              method: 'POST',
              body: {
                questionKeys: ['2301']
              }
            }

            return expectSaga(sagas.shareAnswers, ['2301'])
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [matchers.call(rest.requestSaga, resource, options), {status: 204}],
                [
                  select(
                    sagas.textResourceSelector,
                    'client.actions.evaluation-execution-participation-action.shareAnswer.success'
                  ),
                  'shareAnswer success'
                ]
              ])
              .put(actions.removeShareableAnswers(['2301']))
              .put(notification.toaster({type: 'success', title: 'shareAnswer success'}))
              .run()
          })
        })

        describe('finishEvaluation', () => {
          test('yes no question confirmed', () => {
            return expectSaga(sagas.finishEvaluation)
              .provide([
                [channel, {}],
                {
                  take() {
                    return true
                  }
                },
                [matchers.call(sagas.executeFinishEvaluation), {}]
              ])
              .put.like({
                action: {
                  type: 'notification/YES_NO_QUESTION'
                }
              })
              .call.like({fn: channel})
              .call(sagas.executeFinishEvaluation)
              .run()
          })

          test('yes no question canceled', () => {
            return expectSaga(sagas.finishEvaluation)
              .provide([
                [channel, {}],
                {
                  take() {
                    return false
                  }
                }
              ])
              .put.like({
                action: {
                  type: 'notification/YES_NO_QUESTION'
                }
              })
              .call.like({fn: channel})
              .not.call(sagas.executeFinishEvaluation)
              .run()
          })
        })

        describe('executeFinishEvaluation', () => {
          const uuid = '6f1803a0-8742-4747-8a1f-b4a875aed4fa'
          const resource = `widgets/evaluationExecutionParticipation/${uuid}/finish`
          const options = {
            method: 'POST',
            acceptedErrorCodes: ['FINISHING_EVALUATION_FAILED']
          }

          test('successful finish evaluation with selection', () => {
            const selection = {
              entityName: 'Evaluation_party_assignment',
              type: 'ID',
              ids: ['1']
            }
            return expectSaga(sagas.executeFinishEvaluation)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.requestSaga, resource, options), {status: 204}]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(externalEvents.fireExternalEvent('onCancel'))
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .run()
          })

          test('successful finish evaluation with uuid', () => {
            const selection = undefined
            return expectSaga(sagas.executeFinishEvaluation)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.requestSaga, resource, options), {status: 204}]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(externalEvents.fireExternalEvent('onSuccess'))
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .run()
          })

          test('finish evaluation failed', () => {
            const response = {
              status: 400,
              body: {
                errorCode: 'FINISHING_EVALUATION_FAILED',
                message: 'finishing evaluation failed'
              }
            }

            return expectSaga(sagas.executeFinishEvaluation)
              .provide([
                [select(sagas.evaluationExecutionParticipationActionSelector), {uuid}],
                [matchers.call(rest.requestSaga, resource, options), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .put(notification.toaster({type: 'error', title: 'finishing evaluation failed'}))
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .run()
          })
        })

        describe('onChange', () => {
          test('change suggestion dropdown', () => {
            const payload = {
              options: [
                {
                  display: 'selected-display',
                  selected: true
                },
                {
                  display: 'other-display',
                  selected: false
                }
              ]
            }
            const meta = {
              form: 'evaluation-execution-participation-action',
              field: 'pseudoField_suggestion_1'
            }
            const formValues = {
              pseudoField_question_1: 'current-text'
            }
            return expectSaga(sagas.onChange, {payload, meta})
              .provide([[matchers.call(sagas.loadFormValues, 'evaluation-execution-participation-action'), formValues]])
              .call(sagas.onChangeSuggestion, 'pseudoField_suggestion_1', payload)
              .put(
                formActions.change(
                  'evaluation-execution-participation-action',
                  'pseudoField_question_1',
                  'current-text selected-display'
                )
              )
              .run()
          })

          test('change comment', () => {
            const payload = 'new comment'
            const meta = {
              form: 'evaluation-execution-participation-action',
              field: 'pseudoField_comment_1'
            }

            return expectSaga(sagas.onChange, {payload, meta})
              .provide([[matchers.call(sagas.saveChangedField, '1', {comment: 'new comment'}, 'saveComment'), {}]])
              .call(sagas.onChangeComment, 'pseudoField_comment_1', 'new comment')
              .run()
          })

          test('change multi select field', () => {
            const simpleEvaluation = {
              questionCategories: [
                {
                  questions: [
                    {
                      key: '1',
                      type: 'multiplechoicedropdown'
                    }
                  ]
                }
              ]
            }
            const payload = {
              options: [
                {key: '2', selected: true},
                {key: '3', selected: true}
              ]
            }
            const meta = {
              form: 'evaluation-execution-participation-action',
              field: 'pseudoField_question_1'
            }

            return expectSaga(sagas.onChange, {payload, meta})
              .provide([
                [matchers.call(sagas.saveChangedField, '1', {answer: ['2', '3']}, 'saveAnswer'), {}],
                [select(sagas.evaluationExecutionParticipationActionSelector), {evaluation: simpleEvaluation}]
              ])
              .call(sagas.onChangeAnswer, 'pseudoField_question_1', payload)
              .run()
          })
        })
      })
    })
  })
})
