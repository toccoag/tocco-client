import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  uuid: null,
  formDefinition: null,
  fieldDefinitions: null,
  evaluation: {},
  loaded: false,
  pseudoEntityPaths: {}
}

const addPseudoEntityPaths = (state, {payload: {pseudoEntityPaths}}) => ({
  ...state,
  pseudoEntityPaths: {...state.pseudoEntityPaths, ...pseudoEntityPaths}
})

const removeShareableAnswers = (state, {payload: {answerIds}}) => ({
  ...state,
  evaluation: {
    ...state.evaluation,
    questionCategories: state.evaluation.questionCategories.map(c => ({
      ...c,
      questions: c.questions.map(q => ({
        ...q,
        ...(answerIds.includes(q.key) ? {shareable: false} : {})
      }))
    }))
  }
})

const ACTION_HANDLERS = {
  [actions.SET_UUID]: reducerUtil.singleTransferReducer('uuid'),
  [actions.SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [actions.SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions'),
  [actions.SET_EVALUATION]: reducerUtil.singleTransferReducer('evaluation'),
  [actions.SET_LOADED]: reducerUtil.singleTransferReducer('loaded'),
  [actions.ADD_PSEUDO_ENTITY_PATHS]: addPseudoEntityPaths,
  [actions.REMOVE_SHAREABLE_ANSWERS]: removeShareableAnswers
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
