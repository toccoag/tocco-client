import * as actions from './actions'
import reducer from './reducer'

const INITIAL_STATE = {
  evaluation: {
    key: '2371',
    questionCategories: [
      {
        label: 'category label',
        questions: [
          {
            key: '1',
            shareable: true
          },
          {
            key: '2',
            shareable: true
          },
          {
            key: '3',
            shareable: false
          }
        ]
      }
    ]
  }
}

describe('evaluation-execution-participation-action', () => {
  describe('modules', () => {
    describe('evaluationExecutionParticipationAction', () => {
      describe('reducer', () => {
        test('removeShareableAnswers', () => {
          const stateBefore = INITIAL_STATE
          const answerIds = ['1']
          const expectedStateAfter = {
            evaluation: {
              key: '2371',
              questionCategories: [
                {
                  label: 'category label',
                  questions: [
                    {
                      key: '1',
                      shareable: false
                    },
                    {
                      key: '2',
                      shareable: true
                    },
                    {
                      key: '3',
                      shareable: false
                    }
                  ]
                }
              ]
            }
          }

          expect(reducer(stateBefore, actions.removeShareableAnswers(answerIds))).to.deep.equal(expectedStateAfter)
        })
      })
    })
  })
})
