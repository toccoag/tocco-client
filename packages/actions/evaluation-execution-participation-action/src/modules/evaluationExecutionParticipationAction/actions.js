export const LOAD_FORM_INFO = 'evaluationExecutionParticipationAction/LOAD_FORM_INFO'
export const SET_UUID = 'evaluationExecutionParticipationAction/SET_UUID'
export const LOAD_UUID = 'evaluationExecutionParticipationAction/LOAD_UUID'
export const SET_FORM_DEFINITION = 'evaluationExecutionParticipationAction/SET_FORM_DEFINITION'
export const SET_FIELD_DEFINITIONS = 'evaluationExecutionParticipationAction/SET_FIELD_DEFINITIONS'
export const SET_EVALUATION = 'evaluationExecutionParticipationAction/SET_EVALUATION'
export const SET_LOADED = 'evaluationExecutionParticipationAction/SET_LOADED'
export const ADD_PSEUDO_ENTITY_PATHS = 'evaluationExecutionParticipationAction/ADD_PSEUDO_ENTITY_PATHS'
export const SUBMIT_FORM = 'evaluationExecutionParticipationAction/SUBMIT_FORM'
export const DISPLAY_SHARE_ANSWER_MODAL = 'evaluationExecutionParticipationAction/DISPLAY_SHARE_ANSWER_MODAL'
export const REMOVE_SHAREABLE_ANSWERS = 'evaluationExecutionParticipationAction/REMOVE_SHAREABLE_ANSWERS'
export const FINISH_EVALUATION = 'evaluationExecutionParticipationAction/FINISH_EVALUATION'

export const loadFormInfo = () => ({
  type: LOAD_FORM_INFO
})

export const setUuid = uuid => ({
  type: SET_UUID,
  payload: {uuid}
})

export const loadUuid = selection => ({
  type: LOAD_UUID,
  payload: {selection}
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const setEvaluation = evaluation => ({
  type: SET_EVALUATION,
  payload: {evaluation}
})

export const setLoaded = loaded => ({
  type: SET_LOADED,
  payload: {loaded}
})

export const addPseudoEntityPaths = pseudoEntityPaths => ({
  type: ADD_PSEUDO_ENTITY_PATHS,
  payload: {
    pseudoEntityPaths
  }
})

export const submitForm = () => ({
  type: SUBMIT_FORM
})

export const displayShareAnswerModal = () => ({
  type: DISPLAY_SHARE_ANSWER_MODAL
})

export const removeShareableAnswers = answerIds => ({
  type: REMOVE_SHAREABLE_ANSWERS,
  payload: {
    answerIds
  }
})

export const finishEvaluation = () => ({
  type: FINISH_EVALUATION
})
