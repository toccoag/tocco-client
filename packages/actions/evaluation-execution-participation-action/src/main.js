import PropTypes from 'prop-types'
import {fork} from 'redux-saga/effects'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  formData,
  notification,
  selection
} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {appContext, reducer as reducerUtil, saga as sagaUtil} from 'tocco-util'

import ParticipationView from './components/ParticipationView'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'evaluation-execution-participation-action'
const EXTERNAL_EVENTS = ['onSuccess', 'onCancel', 'emitAction']

const addToStore = store => {
  reducerUtil.injectReducers(store, reducers)

  const rootSaga = sagaUtil.createGenerator(sagas.map(s => fork(s)))
  store.sagaMiddleware.run(sagaUtil.autoRestartSaga(rootSaga, null, errorLogging.logError))
}

const setUpStore = (input, events) => {
  if (input.store) {
    addToStore(input.store)
    return input.store
  }

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({listApp: EntityListApp}))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  return store
}

const initApp = (id, input, events, publicPath) => {
  const content = <ParticipationView FormButtonsComponent={input.FormButtonsComponent} />

  const store = setUpStore(input, events)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['actiongroup', 'component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EvaluationExecutionParticipationAction = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

EvaluationExecutionParticipationAction.propTypes = {
  appContext: appContext.propTypes.isRequired,
  selection: selection.propType,
  uuid: PropTypes.string,
  actionProperties: PropTypes.shape({
    reportDisplayId: PropTypes.string,
    /**
     * Set to `true` to hide the submit button (default: false)
     * everything is always saved automatically anyway, the submit button is just a dummy
     */
    hideSubmitButton: PropTypes.bool
  }),
  /**
   * Custom form buttons component underneath the form
   */
  FormButtonsComponent: PropTypes.elementType,
  /**
   * Store instance to use
   * only needed if you customize the action and use custom form buttons otherwise the action creates its own store
   */
  store: PropTypes.object
}

export default EvaluationExecutionParticipationAction

export {REDUX_FORM_NAME} from './components/ParticipationForm'
export {evaluationExecutionParticipationActionSelector} from './modules/evaluationExecutionParticipationAction'
