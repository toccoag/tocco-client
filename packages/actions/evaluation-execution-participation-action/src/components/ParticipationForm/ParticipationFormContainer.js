import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form} from 'tocco-app-extensions'

import {submitForm} from '../../modules/evaluationExecutionParticipationAction/actions'

import ParticipationForm, {REDUX_FORM_NAME} from './ParticipationForm'

const mapActionCreators = {
  submitForm
}

const mapStateToProps = state => ({
  formDefinition: state.evaluationExecutionParticipationAction.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  evaluation: state.evaluationExecutionParticipationAction.evaluation,
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state),
  selection: state.input.selection
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ParticipationForm))
