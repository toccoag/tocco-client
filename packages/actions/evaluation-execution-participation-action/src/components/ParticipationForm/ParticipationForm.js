import PropTypes from 'prop-types'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'

import ParticipationFormButtons from '../ParticipationFormButtons/'

export const REDUX_FORM_NAME = 'evaluation-execution-participation-action'

const ParticipationForm = ({
  formDefinition,
  formValues,
  evaluation,
  FormButtonsComponent = ParticipationFormButtons,
  submitForm
}) => {
  const formEventProps = form.hooks.useFormEvents({submitForm})
  return (
    <form {...formEventProps}>
      <form.FormBuilder
        entity={{key: evaluation.key, model: 'Evaluation_party_assignment'}}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        mode="create"
      />
      <FormButtonsComponent />
    </form>
  )
}

ParticipationForm.propTypes = {
  formDefinition: PropTypes.object,
  formValues: PropTypes.object,
  evaluation: PropTypes.object,
  FormButtonsComponent: PropTypes.elementType,
  submitForm: PropTypes.func.isRequired
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(ParticipationForm)
