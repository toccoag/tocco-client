import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage, injectIntl} from 'react-intl'
import {Button, ColumnPicker, StyledStickyButtonWrapper} from 'tocco-ui'

const ShareAnswerModal = ({questions, onOk, intl}) => {
  const initialColumns = questions.map(q => ({
    id: q.key,
    label: q.question,
    hidden: true
  }))
  const [columns, setColumns] = useState(initialColumns)

  const handleOk = () => {
    const selectedQuestionKeys = columns.filter(c => !c.hidden).map(c => c.id)
    onOk(selectedQuestionKeys)
  }

  return (
    <>
      <ColumnPicker intl={intl} columns={columns} onColumnsChange={setColumns} dndEnabled={false} />
      <StyledStickyButtonWrapper>
        <Button onClick={handleOk} ink="primary" look="raised">
          <FormattedMessage id="client.actions.evaluation-execution-participation-action.shareable" />
        </Button>
      </StyledStickyButtonWrapper>
    </>
  )
}

ShareAnswerModal.propTypes = {
  questions: PropTypes.array,
  onOk: PropTypes.func,
  intl: PropTypes.object.isRequired
}

export default injectIntl(ShareAnswerModal)
