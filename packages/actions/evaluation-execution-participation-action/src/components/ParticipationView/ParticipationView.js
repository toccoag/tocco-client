import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {form} from 'tocco-app-extensions'
import {LoadMask, Typography} from 'tocco-ui'

import ParticipationForm from '../ParticipationForm'

const ParticipationView = ({
  loadFormInfo,
  fieldDefinitions,
  formDefinition,
  uuid,
  evaluation,
  loaded,
  FormButtonsComponent
}) => {
  useEffect(() => {
    if (uuid) {
      loadFormInfo()
    }
  }, [loadFormInfo, uuid])

  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  return (
    <LoadMask required={[loaded]}>
      <>
        <Typography.H1>{evaluation.questionnaireLabel}</Typography.H1>
        <Typography.H5>{evaluation.user}</Typography.H5>
        {evaluation.sourceEntity ? <Typography.H5>{evaluation.sourceEntity}</Typography.H5> : null}
        {evaluation.description ? <Typography.P>{evaluation.description}</Typography.P> : null}
      </>
      <ParticipationForm validate={handleSyncValidate} FormButtonsComponent={FormButtonsComponent} />
    </LoadMask>
  )
}

ParticipationView.propTypes = {
  loadFormInfo: PropTypes.func.isRequired,
  formDefinition: PropTypes.object,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object),
  evaluation: PropTypes.shape({
    questionnaireLabel: PropTypes.string,
    user: PropTypes.string,
    sourceEntity: PropTypes.string,
    description: PropTypes.string
  }),
  uuid: PropTypes.string,
  loaded: PropTypes.bool,
  FormButtonsComponent: PropTypes.elementType
}

export default ParticipationView
