import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadFormInfo} from '../../modules/evaluationExecutionParticipationAction/actions'

import ParticipationView from './ParticipationView'

const mapActionCreators = {
  loadFormInfo
}
const mapStateToProps = state => ({
  formDefinition: state.evaluationExecutionParticipationAction.formDefinition,
  fieldDefinitions: state.evaluationExecutionParticipationAction.fieldDefinitions,
  uuid: state.evaluationExecutionParticipationAction.uuid,
  evaluation: state.evaluationExecutionParticipationAction.evaluation,
  loaded: state.evaluationExecutionParticipationAction.loaded
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ParticipationView))
