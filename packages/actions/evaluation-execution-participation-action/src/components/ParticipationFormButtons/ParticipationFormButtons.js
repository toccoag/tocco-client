import PropTypes from 'prop-types'
import {selection as selectionUtil} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

import {getShareableQuestions} from '../../utils/form'

const ParticipationFormButtons = ({
  selection,
  evaluation,
  submitting,
  valid,
  hideSubmitButton,
  intl,
  displayShareAnswerModal,
  finishEvaluation,
  fireExternalEvent
}) => {
  const handleBack = () => fireExternalEvent('onCancel')
  return (
    <>
      {selection ? (
        <Button
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.back'})}
          look="raised"
          onClick={handleBack}
        />
      ) : null}
      {!evaluation.readOnly && hideSubmitButton !== true ? (
        <Button
          disabled={submitting}
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.submit'})}
          type="submit"
          look="raised"
          ink="primary"
        />
      ) : null}
      {!evaluation.readOnly && getShareableQuestions(evaluation).length > 0 ? (
        <Button
          disabled={submitting}
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.shareable'})}
          look="raised"
          onClick={displayShareAnswerModal}
        />
      ) : null}
      {!evaluation.readOnly && evaluation.isFinishable ? (
        <Button
          disabled={submitting || !valid}
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.finish'})}
          look="raised"
          onClick={finishEvaluation}
        />
      ) : null}
    </>
  )
}

ParticipationFormButtons.propTypes = {
  evaluation: PropTypes.object,
  selection: selectionUtil.propType,
  intl: PropTypes.object.isRequired,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  hideSubmitButton: PropTypes.bool,
  displayShareAnswerModal: PropTypes.func.isRequired,
  finishEvaluation: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired
}

export default ParticipationFormButtons
