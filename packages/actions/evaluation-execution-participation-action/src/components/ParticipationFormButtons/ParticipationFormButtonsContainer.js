import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues, isSubmitting, isValid} from 'redux-form'
import {externalEvents, form} from 'tocco-app-extensions'

import {
  displayShareAnswerModal,
  finishEvaluation,
  submitForm
} from '../../modules/evaluationExecutionParticipationAction/actions'
import {REDUX_FORM_NAME} from '../ParticipationForm'

import ParticipationFormButtons from './ParticipationFormButtons'

const mapActionCreators = {
  displayShareAnswerModal,
  submitForm,
  finishEvaluation,
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  formDefinition: state.evaluationExecutionParticipationAction.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  evaluation: state.evaluationExecutionParticipationAction.evaluation,
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state),
  selection: state.input.selection,
  valid: isValid(REDUX_FORM_NAME)(state),
  submitting: isSubmitting(REDUX_FORM_NAME)(state),
  hideSubmitButton: state.input.actionProperties?.hideSubmitButton
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ParticipationFormButtons))
