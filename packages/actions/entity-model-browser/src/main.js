import PropTypes from 'prop-types'
import {actions, appFactory, actionEmitter, notification, externalEvents} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import EntityModelBrowser from './components/EntityModelBrowser'
import reducers, {sagas} from './modules/reducers'

const packageName = 'entity-model-browser'

const EXTERNAL_EVENTS = ['emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <EntityModelBrowser />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  notification.addToStore(store, false)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EntityModelBrowserApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

EntityModelBrowserApp.propTypes = {
  pathPrefix: PropTypes.string
}
export default EntityModelBrowserApp
