import {IntlStub} from 'tocco-test-util'

import {getBreadcrumbsInfo} from './breadcrumbs'

const entityModelTitle = 'client.entity-model-browser.title'
const entityModelInfo = {
  display: entityModelTitle,
  title: entityModelTitle,
  path: '/em',
  type: null,
  clickable: false
}

describe('entity-model-browser', () => {
  describe('utils', () => {
    describe('breadcrumbs', () => {
      describe('getBreadcrumbsInfo', () => {
        test('should create breadcrumbs info', () => {
          const expected = [
            entityModelInfo,
            {
              display: 'User',
              title: `${entityModelTitle} - User`,
              path: '/em/User/model',
              state: {relations: []},
              type: 'table',
              clickable: false
            },
            {
              display: 'Business_unit (relBusiness_unit / 1:n)',
              title: `${entityModelTitle} - Business_unit`,
              path: '/em/User/Business_unit/model',
              state: {
                relations: [
                  {
                    name: 'relBusiness_unit',
                    typeDescription: '1:n',
                    targetEntity: 'Business_unit'
                  }
                ]
              },
              type: 'link-simple',
              clickable: false
            }
          ]

          const result = getBreadcrumbsInfo(
            '/em',
            ['User', 'Business_unit'],
            [
              {
                name: 'relBusiness_unit',
                typeDescription: '1:n',
                targetEntity: 'Business_unit'
              }
            ],
            IntlStub
          )

          expect(result).to.eql(expected)
        })

        test('should ignore relation information when not available', () => {
          const expected = [
            entityModelInfo,
            {
              display: 'User',
              title: `${entityModelTitle} - User`,
              path: '/em/User/model',
              state: {relations: []},
              type: 'table',
              clickable: false
            },
            {
              display: 'Business_unit',
              title: `${entityModelTitle} - Business_unit`,
              path: '/em/User/Business_unit/model',
              state: {relations: []},
              type: 'link-simple',
              clickable: false
            }
          ]

          const result = getBreadcrumbsInfo('/em', ['User', 'Business_unit'], [], IntlStub)

          expect(result).to.eql(expected)
        })
      })
    })
  })
})
