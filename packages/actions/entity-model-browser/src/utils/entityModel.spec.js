import {createEntityModelData} from './entityModel'

describe('entity-model-browser', () => {
  describe('utils', () => {
    describe('entityModel', () => {
      describe('createEntityModelData', () => {
        test('should create header, field, relations rows with label per locale', () => {
          const entityModelPerLocale = {
            de: {
              name: 'User',
              label: 'Person',
              documentation: 'This is some `documentation`.',
              definingModule: 'core.user',
              entityType: 'TABLE',
              fields: [
                {
                  fieldName: 'firstname',
                  documentation: 'Firstname of person',
                  label: 'Vorname',
                  type: 'string',
                  localized: false,
                  definingModule: 'core.address',
                  validation: {mandatory: true}
                }
              ],
              relations: [
                {
                  relationName: 'image',
                  documentation: 'Relation',
                  relationDisplay: {
                    cardinality: '1:n'
                  },
                  label: 'Vorschaubild',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                }
              ]
            },
            en: {
              name: 'User',
              label: 'User',
              documentation: 'This is some `documentation`.',
              definingModule: 'core.user',
              entityType: 'TABLE',
              fields: [
                {
                  fieldName: 'firstname',
                  documentation: 'Firstname of person',
                  label: 'Firstname',
                  type: 'string',
                  localized: false,
                  definingModule: 'core.address',
                  validation: {mandatory: true}
                }
              ],
              relations: [
                {
                  relationName: 'image',
                  documentation: 'Relation',
                  relationDisplay: {
                    cardinality: '1:n'
                  },
                  label: 'Preview picture',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                }
              ]
            }
          }

          const expected = [
            {
              __key: 'header',
              rowType: 'header',
              documentation: 'This is some `documentation`.',
              name: 'User',
              label_de: 'Person',
              label_en: 'User',
              typeDescription: 'TABLE',
              mandatory: null,
              localized: null,
              definingModule: 'core.user'
            },
            {
              __key: 'firstname',
              rowType: '',
              documentation: 'Firstname of person',
              name: 'firstname',
              label_de: 'Vorname',
              label_en: 'Firstname',
              typeDescription: 'string',
              mandatory: true,
              localized: false,
              definingModule: 'core.address'
            },
            {
              __key: 'image',
              rowType: 'relation',
              documentation: 'Relation',
              name: 'image',
              label_de: 'Vorschaubild',
              label_en: 'Preview picture',
              typeDescription: '1:n',
              mandatory: false,
              localized: null,
              definingModule: 'core.image',
              targetEntity: 'Image'
            }
          ]

          const result = createEntityModelData(entityModelPerLocale)

          expect(result).to.eql(expected)
        })

        test('should create key row', () => {
          const entityModelPerLocale = {
            de: {
              name: 'User',
              label: 'Person',
              keyField: 'pk',
              entityType: 'TABLE',
              fields: [
                {
                  fieldName: 'pk',
                  label: 'Primärschlüssel',
                  type: 'serial',
                  localized: false,
                  definingModule: 'core.user',
                  validation: {mandatory: true}
                }
              ],
              relations: []
            }
          }

          const expected = [
            {
              __key: 'header',
              rowType: 'header',
              documentation: undefined,
              name: 'User',
              label_de: 'Person',
              typeDescription: 'TABLE',
              mandatory: null,
              localized: null,
              definingModule: undefined
            },
            {
              __key: 'pk',
              rowType: 'key',
              documentation: undefined,
              name: 'pk',
              label_de: 'Primärschlüssel',
              typeDescription: 'serial',
              mandatory: true,
              localized: false,
              definingModule: 'core.user'
            }
          ]

          const result = createEntityModelData(entityModelPerLocale)

          expect(result).to.eql(expected)
        })

        test('should create all different types of relation rows in alphabetical order', () => {
          const entityModelPerLocale = {
            de: {
              name: 'User',
              label: 'Person',
              fields: [],
              relations: [
                {
                  relationName: 'd',
                  documentation: 'Relation',
                  relationDisplay: {cardinality: '1:n'},
                  label: 'Vorschaubild',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                },
                {
                  relationName: 'a',
                  documentation: 'Relation',
                  relationDisplay: {cardinality: '1:1'},
                  label: 'Vorschaubild',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                },
                {
                  relationName: 'c',
                  documentation: 'Relation',
                  relationDisplay: {cardinality: 'n:n'},
                  label: 'Vorschaubild',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                },
                {
                  relationName: 'b',
                  documentation: 'Relation',
                  relationDisplay: {cardinality: 'n:1'},
                  label: 'Vorschaubild',
                  definingModule: 'core.image',
                  targetEntity: 'Image'
                }
              ]
            }
          }

          const expected = [
            {
              __key: 'header',
              rowType: 'header',
              documentation: undefined,
              name: 'User',
              label_de: 'Person',
              typeDescription: undefined,
              mandatory: null,
              localized: null,
              definingModule: undefined
            },
            {
              __key: 'a',
              rowType: 'relation',
              documentation: 'Relation',
              name: 'a',
              label_de: 'Vorschaubild',
              mandatory: false,
              localized: null,
              definingModule: 'core.image',
              typeDescription: '1:1',
              targetEntity: 'Image'
            },
            {
              __key: 'b',
              rowType: 'relation',
              documentation: 'Relation',
              name: 'b',
              label_de: 'Vorschaubild',
              mandatory: true,
              localized: null,
              definingModule: 'core.image',
              typeDescription: 'n:1',
              targetEntity: 'Image'
            },
            {
              __key: 'c',
              rowType: 'relation',
              documentation: 'Relation',
              name: 'c',
              label_de: 'Vorschaubild',
              mandatory: false,
              localized: null,
              definingModule: 'core.image',
              typeDescription: 'n:n',
              targetEntity: 'Image'
            },
            {
              __key: 'd',
              rowType: 'relation',
              documentation: 'Relation',
              name: 'd',
              label_de: 'Vorschaubild',
              mandatory: false,
              localized: null,
              definingModule: 'core.image',
              typeDescription: '1:n',
              targetEntity: 'Image'
            }
          ]

          const result = createEntityModelData(entityModelPerLocale)

          expect(result).to.eql(expected)
        })

        test('should sort fields by key and alphabetical', () => {
          const entityModelPerLocale = {
            de: {
              name: 'User',
              label: 'Person',
              documentation: 'This is some `documentation`.',
              definingModule: 'core.user',
              keyField: 'pk',
              fields: [
                {
                  fieldName: 'lastname',
                  documentation: 'Lastname of person',
                  label: 'Nachname',
                  type: 'string',
                  localized: false,
                  definingModule: 'core.address',
                  validation: {mandatory: true}
                },
                {
                  fieldName: 'firstname',
                  documentation: 'Firstname of person',
                  label: 'Vorname',
                  type: 'string',
                  localized: false,
                  definingModule: 'core.address',
                  validation: {mandatory: true}
                },
                {
                  fieldName: 'pk',
                  label: 'Primärschlüssel',
                  type: 'serial',
                  localized: false,
                  definingModule: 'core.user',
                  validation: {mandatory: true}
                }
              ],
              relations: []
            }
          }

          const expected = [
            {
              __key: 'header',
              rowType: 'header',
              documentation: 'This is some `documentation`.',
              name: 'User',
              label_de: 'Person',
              typeDescription: undefined,
              mandatory: null,
              localized: null,
              definingModule: 'core.user'
            },
            {
              __key: 'pk',
              rowType: 'key',
              documentation: undefined,
              name: 'pk',
              label_de: 'Primärschlüssel',
              typeDescription: 'serial',
              mandatory: true,
              localized: false,
              definingModule: 'core.user'
            },
            {
              __key: 'firstname',
              rowType: '',
              documentation: 'Firstname of person',
              name: 'firstname',
              label_de: 'Vorname',
              typeDescription: 'string',
              mandatory: true,
              localized: false,
              definingModule: 'core.address'
            },
            {
              __key: 'lastname',
              rowType: '',
              documentation: 'Lastname of person',
              name: 'lastname',
              label_de: 'Nachname',
              typeDescription: 'string',
              mandatory: true,
              localized: false,
              definingModule: 'core.address'
            }
          ]

          const result = createEntityModelData(entityModelPerLocale)

          expect(result).to.eql(expected)
        })
      })
    })
  })
})
