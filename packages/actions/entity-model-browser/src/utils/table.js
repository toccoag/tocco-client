/* eslint-disable react/prop-types */
import {Typography} from 'tocco-ui'

export const createColumn = (intl, id, key) => {
  const msg = msgKey => intl.formatMessage({id: msgKey})

  return {
    id,
    label: key ? msg(key) : '',
    resizable: true,
    sorting: {sortable: false},
    dynamic: true,
    readOnly: true,
    alignment: 'left',
    CellRenderer: ({rowData, column}) => <Typography.Span breakWords={false}>{rowData[column.id]}</Typography.Span>
  }
}
