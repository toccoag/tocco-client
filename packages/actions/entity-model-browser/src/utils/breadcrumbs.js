/**
 * Returns breadcrumbs info for current entity model browser path.
 *
 * @param {string} pathPrefix
 *  - e.g. `/em`
 * @param {Array} modelNames
 * `modelNames` is the list of the entities from the path
 *   - e.g. `['Event', 'Email_template', 'Event_type']`
 * @param {Array} relations
 * `relations` is the detailed relation info from the routing state
 *   - the first entry of the `modelNames` does not have an entry in the `relations`
 *   - therefore `relations` (if available) should always have one item less than `modelNames`
 *   - e.g.
 * ```
 * [
 *   {
 *       "name": "relEmail_template_waiting_list",
 *       "typeDescription": "n:0..1",
 *       "targetEntity": "Email_template"
 *   },
 *   {
 *       "name": "relEvent_type_waiting_list",
 *       "typeDescription": "0..1:n",
 *       "targetEntity": "Event_type"
 *   }
 * ]
 * ```
 * @param {Object} intl
 * @returns Array
 */
export const getBreadcrumbsInfo = (pathPrefix, modelNames, relations, intl) => {
  const entityModelTitle = intl.formatMessage({id: 'client.entity-model-browser.title'})

  return [
    {
      display: entityModelTitle,
      title: entityModelTitle,
      path: pathPrefix,
      type: null,
      clickable: false
    },
    ...modelNames.reduce(
      ({breadcrumbs, prevPath}, modelName, index) => {
        const path = `${prevPath}/${modelName}`

        const state = {relations: relations.slice(0, index)}

        const relation = relations ? relations[index - 1] : undefined
        const relationInfo = relation ? ` (${relation.name} / ${relation.typeDescription})` : ''

        return {
          prevPath: path,
          breadcrumbs: [
            ...breadcrumbs,
            {
              display: `${modelName}${relationInfo}`,
              title: `${entityModelTitle} - ${modelName}`,
              path: `${path}/model`,
              state,
              type: index === 0 ? 'table' : 'link-simple',
              clickable: false
            }
          ]
        }
      },
      {breadcrumbs: [], prevPath: pathPrefix}
    ).breadcrumbs
  ]
}
