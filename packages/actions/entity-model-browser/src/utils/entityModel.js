export const compareEntityField = (a, b) => {
  if (a.rowType === 'key') {
    return -1
  }
  if (b.rowType === 'key') {
    return 1
  }
  if (a.name < b.name) {
    return -1
  }
  if (a.name > b.name) {
    return 1
  }
  return 0
}

export const compareEntityRelation = (a, b) => {
  if (a.name < b.name) {
    return -1
  }
  if (a.name > b.name) {
    return 1
  }
  return 0
}

export const createEntityModelData = entityModelPerLocale => {
  const locales = Object.keys(entityModelPerLocale)
  const referenceEntityModel = entityModelPerLocale[locales[0]]

  const header = {
    __key: 'header',
    rowType: 'header',
    documentation: referenceEntityModel.documentation,
    name: referenceEntityModel.name,
    ...locales.reduce((acc, locale) => ({...acc, [`label_${locale}`]: entityModelPerLocale[locale].label}), {}),
    typeDescription: referenceEntityModel.entityType,
    mandatory: null,
    localized: null,
    definingModule: referenceEntityModel.definingModule
  }

  const keyField = referenceEntityModel.keyField
  const fields = referenceEntityModel.fields
    .map((field, index) => ({
      __key: field.fieldName,
      rowType: field.fieldName === keyField ? 'key' : '',
      documentation: field.documentation,
      name: field.fieldName,
      ...locales.reduce(
        (acc, locale) => ({...acc, [`label_${locale}`]: entityModelPerLocale[locale].fields[index].label}),
        {}
      ),
      typeDescription: field.type,
      mandatory: field.validation.mandatory,
      localized: field.localized,
      definingModule: field.definingModule
    }))
    .sort(compareEntityField)

  const relations = referenceEntityModel.relations
    .map((relation, index) => ({
      __key: relation.relationName,
      rowType: 'relation',
      documentation: relation.documentation,
      name: relation.relationName,
      ...locales.reduce(
        (acc, locale) => ({...acc, [`label_${locale}`]: entityModelPerLocale[locale].relations[index].label}),
        {}
      ),
      typeDescription: relation.relationDisplay.cardinality,
      mandatory: relation.relationDisplay.cardinality === 'n:1',
      localized: null,
      definingModule: relation.definingModule,
      targetEntity: relation.targetEntity
    }))
    .sort(compareEntityRelation)

  return [header].concat(fields).concat(relations)
}
