import entityModelBrowserReducer, {sagas as entityModelBrowserSagas, initialize} from './entityModelBrowser'

export default {
  entityModelBrowser: entityModelBrowserReducer
}

export const sagas = [entityModelBrowserSagas]
export {initialize}
