import * as actions from './actions'
import reducer from './reducer'

describe('entity-model-browser', () => {
  describe('modules', () => {
    describe('entityModelBrowser', () => {
      describe('reducer', () => {
        describe('setEntityModelData', () => {
          test('should set new model data', () => {
            const entityModelData = [{name: 'User'}]
            const stateBefore = {
              entityModelData: [],
              loadedModels: {}
            }
            const stateAfter = {
              entityModelData,
              loadedModels: {User: entityModelData}
            }
            expect(reducer(stateBefore, actions.setEntityModelData('User', entityModelData))).to.deep.equal(stateAfter)
          })

          test('should overwrite model data', () => {
            const entityModelData = [{name: 'User'}]
            const stateBefore = {
              entityModelData: [{name: 'User1'}],
              loadedModels: {User: [{name: 'User1'}]}
            }
            const stateAfter = {
              entityModelData,
              loadedModels: {User: entityModelData}
            }
            expect(reducer(stateBefore, actions.setEntityModelData('User', entityModelData))).to.deep.equal(stateAfter)
          })

          test('should append model data', () => {
            const entityModelData = [{name: 'User'}]
            const stateBefore = {
              entityModelData: [{name: 'Address'}],
              loadedModels: {Address: [{name: 'Address'}]}
            }
            const stateAfter = {
              entityModelData,
              loadedModels: {User: entityModelData, Address: [{name: 'Address'}]}
            }
            expect(reducer(stateBefore, actions.setEntityModelData('User', entityModelData))).to.deep.equal(stateAfter)
          })
        })

        describe('clearEntityModelData', () => {
          test('should clear current model data', () => {
            const stateBefore = {
              entityModelData: [[{name: 'User'}]]
            }
            const stateAfter = {
              entityModelData: []
            }
            expect(reducer(stateBefore, actions.clearEntityModelData())).to.deep.equal(stateAfter)
          })
        })
      })
    })
  })
})
