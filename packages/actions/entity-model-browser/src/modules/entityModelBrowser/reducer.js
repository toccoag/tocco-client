import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  initialized: false,
  searchValue: '',
  searchList: undefined,
  selectedEntity: null,
  locales: undefined,
  entityModelData: [],
  loadedModels: {}
}

const setEntityModelData = (state, {payload: {entityModel, entityModelData}}) => {
  return {
    ...state,
    entityModelData,
    loadedModels: {...state.loadedModels, [entityModel]: entityModelData}
  }
}

const clearEntityModelData = state => {
  return {
    ...state,
    entityModelData: []
  }
}

const ACTION_HANDLERS = {
  [actions.SET_INIT]: reducerUtil.singleTransferReducer('initialized'),
  [actions.SET_SEARCH_VALUE]: reducerUtil.singleTransferReducer('searchValue'),
  [actions.SET_SEARCH_LIST]: reducerUtil.singleTransferReducer('searchList'),
  [actions.SET_SELECTED_ENTITY]: reducerUtil.singleTransferReducer('selectedEntity'),
  [actions.SET_LOCALES]: reducerUtil.singleTransferReducer('locales'),
  [actions.SET_ENTITY_MODEL_DATA]: setEntityModelData,
  [actions.CLEAR_ENTITY_MODEL_DATA]: clearEntityModelData
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
