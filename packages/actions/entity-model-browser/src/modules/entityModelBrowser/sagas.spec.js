import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest, notification} from 'tocco-app-extensions'

import {createEntityModelData} from '../../utils/entityModel'

import * as actions from './actions'
import * as sagas from './sagas'

describe('entity-model-browser', () => {
  describe('modules', () => {
    describe('entityModelBrowser', () => {
      describe('sagas', () => {
        describe('initialize', () => {
          test('should initialize entity model browser', () => {
            return expectSaga(sagas.initialize, actions.initialize(''))
              .provide([[matchers.call.fn(sagas.fetchLocales)], [matchers.call.fn(sagas.fetchSearchList)]])
              .call(sagas.fetchLocales)
              .call(sagas.fetchSearchList)
              .put(actions.setSelectedEntity(null))
              .put(actions.setInitialized(true))
              .run()
          })

          test('should initialize entity model browser and load preselected entity', () => {
            return expectSaga(sagas.initialize, actions.initialize('User'))
              .provide([[matchers.call.fn(sagas.fetchLocales)], [matchers.call.fn(sagas.fetchSearchList)]])
              .call(sagas.fetchLocales)
              .call(sagas.fetchSearchList)
              .put(actions.setSelectedEntity('User'))
              .put(actions.setInitialized(true))
              .run()
          })
        })

        describe('fetchLocales', () => {
          test('should fetch all available locales', () => {
            const response = [
              {
                paths: {
                  unique_id: {
                    value: 'de'
                  }
                }
              },
              {
                paths: {
                  unique_id: {
                    value: 'en'
                  }
                }
              }
            ]
            return expectSaga(sagas.fetchLocales)
              .provide([
                [matchers.select(sagas.entityModelBrowserSelector), {locales: undefined}],
                [matchers.call.fn(rest.fetchAllEntities), response]
              ])
              .put(actions.setLocales(['de', 'en']))
              .run()
          })

          test('should use cached locales', () => {
            return expectSaga(sagas.fetchLocales)
              .provide([[matchers.select(sagas.entityModelBrowserSelector), {locales: ['de', 'en']}]])
              .not.put(actions.setLocales(['de', 'en']))
              .run()
          })
        })

        describe('fetchSearchList', () => {
          test('should fetch all entities for the searchList', () => {
            const response = {
              body: {
                entities: {
                  Absence_report: {
                    metaData: {
                      modelName: 'Absence_report',
                      label: 'Abwesenheitsmeldung',
                      entityType: 'STANDARD'
                    },
                    entityModel: null
                  },
                  Absence_status: {
                    metaData: {
                      modelName: 'Absence_status',
                      label: 'Status Abwesenheit',
                      entityType: 'LOOKUP'
                    },
                    entityModel: null
                  }
                }
              }
            }
            return expectSaga(sagas.fetchSearchList)
              .provide([
                [matchers.select(sagas.entityModelBrowserSelector), {searchList: undefined}],
                [matchers.call.fn(rest.simpleRequest), response]
              ])
              .put(actions.setSearchList(response.body.entities))
              .run()
          })

          test('should handle error', () => {
            return expectSaga(sagas.fetchSearchList)
              .provide([
                [matchers.select(sagas.entityModelBrowserSelector), {searchList: undefined}],
                [matchers.call.fn(rest.simpleRequest), throwError(new Error('Failed to fetch data'))]
              ])
              .put(actions.setSearchList({}))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.actions.entitymodel.error.loadSearchList.header',
                  body: 'client.actions.entitymodel.error.loadSearchList.body'
                })
              )
              .run()
          })

          test('should use cached search list', () => {
            return expectSaga(sagas.fetchSearchList)
              .provide([[matchers.select(sagas.entityModelBrowserSelector), {searchList: {}}]])
              .not.put(actions.setSearchList({}))
              .run()
          })
        })

        describe('setSelectedEntity', () => {
          test('should fetch entity model when entity has been selected', () => {
            const entityName = 'User'
            return expectSaga(sagas.setSelectedEntity, {payload: {selectedEntity: entityName}})
              .put(actions.fetchEntityModel(entityName))
              .run()
          })
          test('should reset current entity model when selected has been cleared', () => {
            return expectSaga(sagas.setSelectedEntity, {payload: {selectedEntity: null}})
              .put(actions.resetTable())
              .run()
          })
        })

        describe('fetchEntityModel', () => {
          test('should fetch entity model per locale', () => {
            const state = {
              locales: ['de', 'en'],
              loadedModels: {}
            }
            const entityModel = 'User'
            const entityModelPerLocale = {
              de: {name: entityModel, label: 'Person', fields: [], relations: []},
              en: {name: entityModel, label: 'User', fields: [], relations: []}
            }
            return expectSaga(sagas.fetchEntityModel, actions.fetchEntityModel(entityModel))
              .provide([
                [matchers.select(sagas.entityModelBrowserSelector), state],
                [matchers.call(sagas.fetchEntityModelForLocale, entityModel, 'de'), entityModelPerLocale.de],
                [matchers.call(sagas.fetchEntityModelForLocale, entityModel, 'en'), entityModelPerLocale.en],
                [matchers.call(createEntityModelData, entityModelPerLocale), []]
              ])
              .call(createEntityModelData, entityModelPerLocale)
              .put(actions.setEntityModelData(entityModel, []))
              .run()
          })

          test('should load entity model from state', () => {
            const entityModel = 'User'
            const state = {
              locales: ['de', 'en'],
              loadedModels: {
                User: [{name: 'User'}]
              }
            }
            return expectSaga(sagas.fetchEntityModel, actions.fetchEntityModel(entityModel))
              .provide([[matchers.select(sagas.entityModelBrowserSelector), state]])
              .not.call(sagas.fetchEntityModelForLocale, entityModel, 'de')
              .not.call(sagas.fetchEntityModelForLocale, entityModel, 'en')
              .put(actions.setEntityModelData(entityModel, state.loadedModels.User))
              .run()
          })

          test('should handle error', () => {
            const entityModel = 'User'
            const state = {
              locales: ['de', 'en'],
              loadedModels: {}
            }
            return expectSaga(sagas.fetchEntityModel, actions.fetchEntityModel(entityModel))
              .provide([
                [matchers.select(sagas.entityModelBrowserSelector), state],
                [matchers.call.fn(rest.simpleRequest), throwError(new Error('Failed to fetch data'))]
              ])
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.actions.entitymodel.error.loadTableList.header',
                  body: 'client.actions.entitymodel.error.loadTableList.body'
                })
              )
              .put(actions.setEntityModelData(entityModel, []))
              .run()
          })
        })
      })
    })
  })
})
