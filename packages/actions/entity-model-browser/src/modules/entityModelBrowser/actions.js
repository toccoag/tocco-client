export const INIT = 'entityModelBrowser/INIT'
export const SET_INIT = 'entityModelBrowser/SET_INIT'
export const SET_SEARCH_VALUE = 'entityModelBrowser/SET_SEARCH_VALUE'
export const SET_SEARCH_LIST = 'entityModelBrowser/SET_SEARCH_LIST'
export const SET_SELECTED_ENTITY = 'entityModelBrowser/SET_SELECTED_ENTITY'
export const SET_LOCALES = 'entityModelBrowser/SET_LOCALES'
export const SET_ENTITY_MODEL_DATA = 'entityModelBrowser/SET_ENTITY_MODEL_DATA'
export const CLEAR_ENTITY_MODEL_DATA = 'entityModelBrowser/CLEAR_ENTITY_MODEL_DATA'
export const FETCH_ENTITY_MODEL = 'entityModelBrowser/FETCH_ENTITY_MODEL'
export const RESET_TABLE = 'entityModelBrowser/RESET_TABLE'

export const initialize = entityModel => ({
  type: INIT,
  payload: {entityModel}
})

export const setInitialized = initialized => ({
  type: SET_INIT,
  payload: {initialized}
})

export const setSearchValue = searchValue => ({
  type: SET_SEARCH_VALUE,
  payload: {searchValue}
})

export const setSelectedEntity = selectedEntity => ({
  type: SET_SELECTED_ENTITY,
  payload: {selectedEntity}
})

export const setSearchList = searchList => ({
  type: SET_SEARCH_LIST,
  payload: {searchList}
})

export const setLocales = locales => ({
  type: SET_LOCALES,
  payload: {locales}
})

export const setEntityModelData = (entityModel, entityModelData) => ({
  type: SET_ENTITY_MODEL_DATA,
  payload: {entityModel, entityModelData}
})

export const clearEntityModelData = () => ({
  type: CLEAR_ENTITY_MODEL_DATA
})

export const fetchEntityModel = entityModel => ({
  type: FETCH_ENTITY_MODEL,
  payload: {entityModel}
})

export const resetTable = () => ({
  type: RESET_TABLE
})
