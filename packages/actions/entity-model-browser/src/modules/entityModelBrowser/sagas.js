import {all, takeLatest, call, put, takeEvery, select} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'

import {createEntityModelData} from '../../utils/entityModel'

import * as actions from './actions'

export const entityModelBrowserSelector = state => state.entityModelBrowser

export default function* sagas() {
  yield all([
    takeEvery(actions.INIT, initialize),
    takeLatest(actions.SET_SELECTED_ENTITY, setSelectedEntity),
    takeLatest(actions.FETCH_ENTITY_MODEL, fetchEntityModel),
    takeLatest(actions.RESET_TABLE, reset)
  ])
}

export function* fetchLocales() {
  const {locales: cachedLocales} = yield select(entityModelBrowserSelector)
  if (cachedLocales) {
    return
  }

  const result = yield call(rest.fetchAllEntities, 'Interface_language', {paths: ['unique_id']}, {method: 'GET'})
  const locales = result.map(data => data.paths.unique_id.value)
  yield put(actions.setLocales(locales))
}

export function* initialize({payload: {entityModel: selectedEntity}}) {
  yield all([call(fetchLocales), call(fetchSearchList)])

  if (selectedEntity) {
    yield put(actions.setSelectedEntity(selectedEntity))
  } else {
    yield put(actions.setSelectedEntity(null))
  }

  yield put(actions.setInitialized(true))
}

export function* setSelectedEntity({payload: {selectedEntity}}) {
  if (selectedEntity) {
    yield put(actions.fetchEntityModel(selectedEntity))
  } else {
    yield put(actions.resetTable())
  }
}

export function* fetchSearchList() {
  try {
    const {searchList: cachedSearchList} = yield select(entityModelBrowserSelector)
    if (cachedSearchList) {
      return
    }

    const response = yield call(rest.simpleRequest, '/entities')
    const list = response?.body?.entities || {}

    yield put(actions.setSearchList(list))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.entitymodel.error.loadSearchList.header',
        body: 'client.actions.entitymodel.error.loadSearchList.body'
      })
    )
    yield put(actions.setSearchList({}))
  }
}

export function* fetchEntityModelForLocale(entityModel, locale) {
  const url = `/entities/${entityModel}/model`

  const options = {
    method: 'GET',
    queryParams: {
      locale
    }
  }

  const response = yield call(rest.simpleRequest, url, options)
  return response?.body || {}
}

export function* fetchEntityModel({payload: {entityModel}}) {
  const {locales, loadedModels} = yield select(entityModelBrowserSelector)

  if (loadedModels[entityModel]) {
    yield put(actions.setEntityModelData(entityModel, loadedModels[entityModel]))
    return
  }

  try {
    const entityModelPerLocale = yield all(
      locales.reduce((acc, locale) => ({...acc, [locale]: call(fetchEntityModelForLocale, entityModel, locale)}), {})
    )

    const entityModelData = yield call(createEntityModelData, entityModelPerLocale)
    yield put(actions.setEntityModelData(entityModel, entityModelData))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.entitymodel.error.loadTableList.header',
        body: 'client.actions.entitymodel.error.loadTableList.body'
      })
    )
    yield put(actions.setEntityModelData(entityModel, []))
  }
}

export function* reset() {
  yield put(actions.clearEntityModelData())
}
