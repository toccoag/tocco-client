import PropTypes from 'prop-types'
import {Typography} from 'tocco-ui'

import {StyledHeadline, StyledWrapper} from './StyledComponents'

const EntityModelPopover = ({documentation}) => {
  return (
    <div>
      {Object.keys(documentation).map(keyName => (
        <StyledWrapper key={keyName}>
          <StyledHeadline>{keyName}</StyledHeadline>
          <Typography.Span>{documentation[keyName]}</Typography.Span>
        </StyledWrapper>
      ))}
    </div>
  )
}

EntityModelPopover.propTypes = {
  documentation: PropTypes.object
}

export default EntityModelPopover
