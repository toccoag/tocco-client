import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import EntityModelPopover from './EntityModelPopover'

describe('entity-model-browser', () => {
  describe('components', () => {
    describe('EntityModelPopover', () => {
      const documentation = {
        'nice.core.userbase': 'User Info',
        'nice.optional.address': 'Address Info'
      }

      test('Should load the entity model popover with given data', () => {
        testingLibrary.renderWithTheme(<EntityModelPopover documentation={documentation} />)
        expect(screen.getByText('nice.core.userbase')).exist
        expect(screen.getByText('nice.optional.address')).exist
        expect(screen.getByText('User Info')).exist
        expect(screen.getByText('Address Info')).exist
      })
    })
  })
})
