import styled from 'styled-components'
import {declareFont, themeSelector} from 'tocco-ui'

export const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
`

export const StyledHeadline = styled.span`
  ${declareFont({
    fontWeight: themeSelector.fontWeight('bold')
  })}
`
