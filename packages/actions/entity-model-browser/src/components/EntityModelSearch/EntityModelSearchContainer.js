import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setSearchValue} from '../../modules/entityModelBrowser'

import EntityModelSearch from './EntityModelSearch'

const mapActionCreators = {
  setSearchValue
}

const mapStateToProps = state => ({
  pathPrefix: state.input.pathPrefix,
  searchValue: state.entityModelBrowser.searchValue,
  searchList: state.entityModelBrowser.searchList,
  selectedEntity: state.entityModelBrowser.selectedEntity
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityModelSearch))
