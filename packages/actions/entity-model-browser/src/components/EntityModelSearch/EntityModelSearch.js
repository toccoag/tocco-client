import PropTypes from 'prop-types'
import {useRef} from 'react'
import {useNavigate} from 'react-router-dom'
import {Table, SearchBox} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import {Box, StyledSearchWrapper, StyledTableWrapper} from './StyledEntityModelSearch'
import {createColumns, getTableData} from './table'

const EntityModelSearch = ({pathPrefix, searchList, selectedEntity, intl, searchValue, setSearchValue}) => {
  const msg = id => intl.formatMessage({id})
  const navigate = useNavigate()

  const containerRef = useRef(null)
  customHooks.useAutofocus(containerRef)

  const columns = createColumns(intl)

  const tableData = getTableData(searchList, searchValue)

  const handleRowClick = entityModel => {
    navigate(`/${pathPrefix}/${entityModel}/model`)
  }

  return (
    <Box>
      <StyledSearchWrapper ref={containerRef}>
        <SearchBox
          value={searchValue}
          minInputLength={2}
          onSearch={setSearchValue}
          placeholder={msg('client.entity-model-browser.searchField')}
          aria-label={msg('client.entity-model-browser.searchField')}
        />
      </StyledSearchWrapper>
      <StyledTableWrapper>
        <Table
          dataLoadingInProgress={!searchList}
          columns={columns}
          data={tableData}
          selectionStyle="none"
          scrollBehaviour="inline"
          clickable
          onRowClick={handleRowClick}
          lastOpened={selectedEntity}
          onColumnPositionChange={() => {}}
        />
      </StyledTableWrapper>
    </Box>
  )
}

EntityModelSearch.propTypes = {
  intl: PropTypes.object,
  pathPrefix: PropTypes.string,
  searchValue: PropTypes.string,
  setSearchValue: PropTypes.func,
  selectedEntity: PropTypes.string,
  searchList: PropTypes.object
}

export default EntityModelSearch
