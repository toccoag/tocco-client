import {createColumn} from '../../utils/table'

export const createColumns = intl => [
  createColumn(intl, 'modelName', 'client.entity-model-browser.entityName'),
  createColumn(intl, 'label', 'client.entity-model-browser.label')
]

const tableDataMatchesSearch = (data, searchInputValue) => {
  const noramlizedSearchValue = searchInputValue.toLowerCase()
  return (
    data.modelName.toLowerCase().includes(noramlizedSearchValue) ||
    data.label.toLowerCase().includes(noramlizedSearchValue)
  )
}

export const getTableData = (entityList, searchInputValue) => {
  const persitedEntities = entityList
    ? Object.values(entityList).filter(e => e.metaData.entityType !== 'SESSION_ONLY')
    : []

  const tableData = persitedEntities.map(e => ({
    __key: e.metaData.modelName,
    entityType: e.metaData.entityType,
    label: e.metaData.label.startsWith('##') ? '' : e.metaData.label,
    modelName: e.metaData.modelName
  }))

  return searchInputValue ? tableData.filter(data => tableDataMatchesSearch(data, searchInputValue)) : tableData
}
