import {screen, fireEvent} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary, IntlStub} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import EntityModelSearch from './EntityModelSearch'

describe('entity-model-browser', () => {
  describe('components', () => {
    describe('EntityModelSearch', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      const searchList = {
        Absence_report: {
          metaData: {
            modelName: 'Absence_report',
            label: 'Abwesenheitsmeldung',
            entityType: 'STANDARD'
          },
          entityModel: null
        },
        Absence_status: {
          metaData: {
            modelName: 'Absence_status',
            label: 'Status Abwesenheit',
            entityType: 'LOOKUP'
          },
          entityModel: null
        }
      }
      const selectedEntity = null

      test('should show the whole table with all rows and cells', async () => {
        const setSearchValue = sinon.spy()

        testingLibrary.renderWithIntl(
          <MemoryRouter>
            <EntityModelSearch
              intl={IntlStub}
              searchList={searchList}
              selectedEntity={selectedEntity}
              setSearchValue={setSearchValue}
              initialized
            />
          </MemoryRouter>
        )
        await screen.findAllByTestId('icon')

        fireEvent.click(screen.getByText('Absence_status'))
        const inputFields = screen.getAllByRole('searchbox')
        const tables = screen.getAllByRole('table')
        const rows = screen.getAllByRole('row')
        const cells = screen.getAllByRole('cell')

        expect(inputFields).to.have.length(1)
        expect(tables).to.have.length(1)
        expect(rows).to.have.length(3)
        expect(cells).to.have.length(4)
      })
    })
  })
})
