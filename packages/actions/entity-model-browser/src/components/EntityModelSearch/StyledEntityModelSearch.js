import styled from 'styled-components'
import {scale, theme, StretchingTableContainer} from 'tocco-ui'

export const Box = styled.div`
  background-color: ${theme.color('paper')};
  position: relative;
  height: calc(100% - 43px);
  display: grid;
  grid-template-rows: auto 1fr;

  &:first-of-type {
    z-index: 2; /* higher than StyledTether to prevent cover on scroll */
  }
`

export const StyledSearchWrapper = styled.div`
  padding: ${scale.space(-1.1)} ${scale.space(-0.375)} 0 ${scale.space(-1.1)};
`

export const StyledTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  ${StretchingTableContainer} {
    overflow-x: hidden;
  }
`
