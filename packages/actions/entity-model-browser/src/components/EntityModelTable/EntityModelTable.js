import PropTypes from 'prop-types'
import {Table} from 'tocco-ui'

import {createColumns} from './table'

const EntityModelTable = ({intl, locales, entityModelData}) => {
  const columns = createColumns(intl, locales)

  return (
    <Table
      dataLoadingInProgress={!entityModelData}
      columns={columns}
      data={entityModelData}
      selectionStyle="none"
      scrollBehaviour="inline"
      onColumnPositionChange={() => {}}
      disableVirtualTable
    />
  )
}

EntityModelTable.propTypes = {
  intl: PropTypes.object,
  locales: PropTypes.array,
  entityModelData: PropTypes.array
}

export default EntityModelTable
