import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {declareFont, scale, themeSelector} from 'tocco-ui'

export const StyledIconsWrapper = styled.span`
  display: flex;
  flex-direction: row-reverse;
  align-items: flex-end;
`
export const StyledIconWrapper = styled.span`
  font-size: ${scale.font(1)};
`

export const StyledLink = styled(Link)`
  color: ${themeSelector.color('text')};
  ${declareFont()}
`
