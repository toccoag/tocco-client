import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import EntityModelTable from './EntityModelTable'

describe('entity-model-browser', () => {
  describe('components', () => {
    describe('EntityModelTable', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      const entityModelData = [
        {
          __key: 'header',
          rowType: 'header',
          documentation: 'This is some `documentation`.',
          name: 'User',
          label_de: 'Person',
          label_en: 'User',
          typeDescription: 'LOOKUP',
          mandatory: null,
          localized: null,
          definingModule: 'nice.core.userbase'
        },
        {
          fieldName: 'pk',
          label_de: 'Primärschlüssel',
          label_en: 'Primarykey',
          type: 'serial',
          localized: false,
          validation: {mandatory: true},
          definingModule: 'nice.core.userbase'
        },
        {
          __key: 'firstname',
          rowType: '',
          documentation: 'Firstname of person',
          name: 'firstname',
          label_de: 'Vorname',
          label_en: 'Firstname',
          typeDescription: 'string',
          mandatory: true,
          localized: false,
          definingModule: 'nice.core.address'
        },
        {
          __key: 'relCc_recipient_mail_form_conf',
          rowType: 'relation',
          documentation: undefined,
          name: 'relCc_recipient_mail_form_conf',
          label_de: 'E-Mail-Formular',
          label_en: 'E-mail form',
          typeDescription: 'n:n',
          mandatory: false,
          localized: null,
          definingModule: 'nice.optional.cms',
          targetEntity: 'Mail_form_conf'
        }
      ]

      test('should show the whole main table with all rows and cells', async () => {
        testingLibrary.renderWithIntl(
          <MemoryRouter>
            <EntityModelTable intl={IntlStub} locales={['de', 'en']} entityModelData={entityModelData} />
          </MemoryRouter>
        )
        await screen.findAllByTestId('icon')

        const tables = screen.getAllByRole('table')
        const rows = screen.getAllByRole('row')
        const cells = screen.getAllByRole('cell')
        const columnsHeader = screen.getAllByRole('columnheader')

        expect(tables).to.have.length(1)
        expect(columnsHeader).to.have.length(10)
        expect(rows).to.have.length(5)
        expect(cells).to.have.length(40)
      })
    })
  })
})
