import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {useLocation} from 'react-router-dom'
import {Icon, Popover, Typography} from 'tocco-ui'

import EntityModelPopover from '../EntityModelPopover'

import {StyledLink, StyledIconWrapper, StyledIconsWrapper} from './StyledEntityModelTable'

export const KeyIcon = ({rowType}) => {
  if (rowType === 'key') {
    return (
      <StyledIconWrapper>
        <Icon icon="key" />
      </StyledIconWrapper>
    )
  }

  return null
}

KeyIcon.propTypes = {
  rowType: PropTypes.string
}

export const TypeDescription = ({rowType, typeDescription}) => {
  if (rowType === 'header') {
    return (
      <Typography.Span>
        <FormattedMessage id={`client.entity-model-browser.entityType.${typeDescription}`} />
      </Typography.Span>
    )
  }
  return <Typography.Span>{typeDescription}</Typography.Span>
}

TypeDescription.propTypes = {
  rowType: PropTypes.string,
  typeDescription: PropTypes.string
}

export const EntityModelLink = ({rowType, name, targetEntity, typeDescription}) => {
  const location = useLocation()

  if (rowType === 'header') {
    return (
      <StyledLink to={`/e/${name}`} target="_blank" rel="noreferrer">
        <StyledIconWrapper>
          <Icon icon="external-link" />
        </StyledIconWrapper>
      </StyledLink>
    )
  } else if (rowType === 'relation') {
    return (
      <StyledLink
        to={`../${targetEntity}/model`}
        state={{relations: [...(location.state?.relations || []), {name, typeDescription, targetEntity}]}}
      >
        <StyledIconWrapper>
          <Icon icon="link-simple" />
        </StyledIconWrapper>
      </StyledLink>
    )
  }
  return null
}

EntityModelLink.propTypes = {
  rowType: PropTypes.string,
  name: PropTypes.string,
  targetEntity: PropTypes.string,
  typeDescription: PropTypes.string
}

export const EntityLink = ({name, rowType}) => {
  if (rowType === 'header') {
    return <Typography.Span>{name}</Typography.Span>
  }
  return <Typography.Span>└─ {name}</Typography.Span>
}

EntityLink.propTypes = {
  rowType: PropTypes.string,
  name: PropTypes.string
}

export const DocumentationValue = ({documentation}) => {
  return (
    <StyledIconsWrapper>
      {documentation && (
        <Popover content={<EntityModelPopover documentation={documentation} />}>
          <StyledIconWrapper>
            <Icon icon="question-circle" />
          </StyledIconWrapper>
        </Popover>
      )}
    </StyledIconsWrapper>
  )
}

DocumentationValue.propTypes = {
  documentation: PropTypes.object
}

export const BooleanValue = ({bool}) =>
  bool === true ? (
    <StyledIconWrapper>
      <Icon icon="check" />
    </StyledIconWrapper>
  ) : null

BooleanValue.propTypes = {
  bool: PropTypes.bool
}
