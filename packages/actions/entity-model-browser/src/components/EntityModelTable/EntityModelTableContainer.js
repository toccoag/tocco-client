import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import EntityModelTable from './EntityModelTable'

const mapActionCreators = {}

const mapStateToProps = state => ({
  locales: state.entityModelBrowser.locales,
  entityModelData: state.entityModelBrowser.entityModelData
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityModelTable))
