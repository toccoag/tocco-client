/* eslint-disable react/prop-types */
import {createColumn} from '../../utils/table'

import {BooleanValue, DocumentationValue, EntityLink, EntityModelLink, KeyIcon, TypeDescription} from './ColumnRenderer'

export const createColumns = (intl, locales) => [
  {
    ...createColumn(intl, 'entityModelLink', ''),
    width: 27,
    resizable: false,
    CellRenderer: ({rowData}) => <EntityModelLink {...rowData} />
  },
  {
    ...createColumn(intl, 'name', 'client.entity-model-browser.entityName'),
    shrinkToContent: true,
    CellRenderer: ({rowData}) => <EntityLink {...rowData} />
  },
  {
    ...createColumn(intl, 'typeDescription', 'client.entity-model-browser.typeDescription'),
    CellRenderer: ({rowData}) => <TypeDescription {...rowData} />
  },
  ...locales.map(locale => ({...createColumn(intl, `label_${locale}`, ''), label: locale})),
  {
    ...createColumn(intl, 'mandatory', 'client.entity-model-browser.mandatory'),
    width: 50,
    resizable: false,
    alignment: 'center',
    CellRenderer: ({rowData}) => <BooleanValue bool={rowData.mandatory} />
  },
  {
    ...createColumn(intl, 'localized', 'client.entity-model-browser.localized'),
    width: 75,
    resizable: false,
    alignment: 'center',
    CellRenderer: ({rowData}) => <BooleanValue bool={rowData.localized} />
  },
  {
    ...createColumn(intl, 'key', 'client.entity-model-browser.key'),
    width: 65,
    resizable: false,
    alignment: 'center',
    CellRenderer: ({rowData}) => <KeyIcon {...rowData} />
  },
  createColumn(intl, 'definingModule', 'client.entity-model-browser.moduleInfo'),
  {
    ...createColumn(intl, 'docs', ''),
    width: 27,
    resizable: false,
    alignment: 'right',
    CellRenderer: ({rowData}) => <DocumentationValue {...rowData} />
  }
]
