import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import EntityModelBreadcrumbs from './EntityModelBreadcrumbs'

const mapActionCreators = {}

const mapStateToProps = state => ({
  pathPrefix: state.input.pathPrefix
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityModelBreadcrumbs))
