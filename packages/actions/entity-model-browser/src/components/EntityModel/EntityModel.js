import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {useLocation, useParams} from 'react-router-dom'
import {actions} from 'tocco-app-extensions'
import {
  SidepanelMainContent,
  Sidepanel,
  SidepanelContainer,
  SidepanelHeader,
  LoadMask,
  ActionBarMainContent,
  ActionBar
} from 'tocco-ui'
import {route} from 'tocco-util'

import EntityModelSearch from '../EntityModelSearch'
import EntityModelTable from '../EntityModelTable'

import EntityModelBreadcrumbs from './EntityModelBreadcrumbsContainer'
import {StyledPaneWrapper} from './StyledEntityModel'

const EntityModel = ({pathPrefix, initialize, initialized, intl}) => {
  const msg = id => intl.formatMessage({id})

  const params = useParams()
  const location = useLocation()
  const [isCollapsed, setIsCollapsed] = useState(false)

  useEffect(() => {
    initialize(params.entityModel)
  }, [initialize, params.entityModel])

  const pathParams = route.extractParamsFromPath(`/${pathPrefix}{/:entityModel}*/model`, location.pathname)
  const modelNames = pathParams?.entityModel || []

  const exportActionDefinition = {
    id: 'entity-model-export',
    label: msg('client.entity-model-browser.export'),
    icon: 'file-export',
    actionType: 'simple',
    runInBackgroundTask: true,
    endpoint: 'actions/entityModelExport',
    properties: {
      model: modelNames.at(-1)
    }
  }

  const ExportAction = (
    <actions.Action
      key={exportActionDefinition.id}
      definition={exportActionDefinition}
      selection={{}}
      disabled={modelNames.length === 0}
    />
  )

  return (
    <LoadMask required={[initialized]}>
      <EntityModelBreadcrumbs modelNames={modelNames} />
      <StyledPaneWrapper>
        <SidepanelContainer
          sidepanelPosition="left"
          sidepanelCollapsed={isCollapsed}
          setSidepanelCollapsed={setIsCollapsed}
          scrollBehaviour="inline"
        >
          <Sidepanel>
            <SidepanelHeader />
            <EntityModelSearch />
          </Sidepanel>

          <SidepanelMainContent actionBar>
            <ActionBar>{ExportAction}</ActionBar>
            <ActionBarMainContent>
              <EntityModelTable />
            </ActionBarMainContent>
          </SidepanelMainContent>
        </SidepanelContainer>
      </StyledPaneWrapper>
    </LoadMask>
  )
}

EntityModel.propTypes = {
  pathPrefix: PropTypes.string,
  initialized: PropTypes.bool,
  initialize: PropTypes.func,
  intl: PropTypes.object.isRequired
}

export default EntityModel
