import PropTypes from 'prop-types'
import {useLocation} from 'react-router-dom'
import {Breadcrumbs} from 'tocco-ui'

import {getBreadcrumbsInfo} from '../../utils/breadcrumbs'

const EntityModelBreadcrumbs = ({pathPrefix, modelNames, intl}) => {
  const location = useLocation()
  const relations = location.state?.relations || []

  /**
   * Usually for every nested navigation a relation info is pushed to the state.
   * For example:
   *  - the path is `/Event/Email_template/Event_type/model`
   *  - the `modelNames` therefore are `['Event', 'Email_template', 'Event_type']`
   *  - and the relations state is `[{targetEntity: 'Email_template', ...}, {targetEntity: 'Event_type', ...}]`
   *
   * But there is a scenario when the relations state and the path diverge:
   *  - when a nested path is opened on a new tab/browser (e.g. by copy and paste the url)
   *   - no relation state available
   *  - the user then continues browsing (e.g. via Breadcrumbs and Relations)
   *   - new relation state form this point on is pushed to the state
   *
   * In this case the relations state does not reflect the path anymore and we cannot
   * show further relation information in the breadcrumbs.
   */
  const validRelations = relations.length === modelNames.length - 1

  const breadcrumbsInfo = getBreadcrumbsInfo(pathPrefix, modelNames, validRelations ? relations : [], intl)

  return <Breadcrumbs updateDocumentTitle breadcrumbsInfo={breadcrumbsInfo} showToccoHome />
}

EntityModelBreadcrumbs.propTypes = {
  intl: PropTypes.object,
  pathPrefix: PropTypes.string,
  modelNames: PropTypes.array
}

export default EntityModelBreadcrumbs
