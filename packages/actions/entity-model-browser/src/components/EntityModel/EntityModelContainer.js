import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initialize} from '../../modules/entityModelBrowser/actions'

import EntityModel from './EntityModel'

const mapActionCreators = {
  initialize
}

const mapStateToProps = state => ({
  pathPrefix: state.input.pathPrefix,
  initialized: state.entityModelBrowser.initialized
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityModel))
