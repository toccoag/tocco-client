import {Route, Routes} from 'react-router-dom'

import EntityModel from '../EntityModel'

const EntityModelBrowser = () => (
  <Routes>
    <Route exact path="/" element={<EntityModel />} />
    <Route exact path="model" element={<EntityModel />} />
    <Route exact path=":entityModel/*" element={<EntityModelBrowser />} />
  </Routes>
)

EntityModelBrowser.propTypes = {}

export default EntityModelBrowser
