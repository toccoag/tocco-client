import initialvalueGeneratorReducer, {sagas as initialvalueGeneratorSagas} from './initialvalueGenerator'

export default {
  initialvalueGenerator: initialvalueGeneratorReducer
}

export const sagas = [initialvalueGeneratorSagas]
