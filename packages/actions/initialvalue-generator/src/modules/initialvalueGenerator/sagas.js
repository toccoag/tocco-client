import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {consoleLogger} from 'tocco-util'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(actions.FETCH_DATA, fetchData)])
}

export function* fetchData() {
  const {selection} = yield select(inputSelector)

  try {
    const response = yield call(rest.requestSaga, '/action/initialvalues/generate', {method: 'POST', body: selection})

    yield put(actions.setData(response.body.data))
  } catch (e) {
    consoleLogger.logError('Failed to fetch data', e)
    yield put(actions.setData(null))
  }
}
