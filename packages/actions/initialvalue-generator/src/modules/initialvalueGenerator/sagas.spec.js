import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('initialvalue-generator', () => {
  describe('modules', () => {
    describe('initialvalueGenerator', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(all([takeEvery(actions.FETCH_DATA, sagas.fetchData)]))
          expect(generator.next().done).to.equal(true)
        })
      })

      describe('sagas', () => {
        describe('fetchData', () => {
          const selection = {entityName: 'User', type: 'ID', ids: ['1']}

          test('sucessful load data', () => {
            const response = {
              body: {
                data: []
              }
            }
            expectSaga(sagas.fetchData)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [
                  matchers.call(rest.requestSaga, '/action/initialvalues/generate', {
                    method: 'POST',
                    body: selection
                  }),
                  response
                ]
              ])
              .put(actions.setData([]))
              .run()
          })

          test('should handle error', () => {
            return expectSaga(sagas.fetchData)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call.fn(rest.requestSaga), throwError(new Error('Failed to fetch data'))]
              ])
              .put(actions.setData(null))
              .run()
          })
        })
      })
    })
  })
})
