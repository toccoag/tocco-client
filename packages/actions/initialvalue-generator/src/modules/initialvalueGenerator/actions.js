export const FETCH_DATA = 'initialvalueGenerator/FETCH_DATA'
export const SET_DATA = 'initialvalueGenerator/SET_DATA'

export const fetchData = () => ({
  type: FETCH_DATA
})

export const setData = data => ({
  type: SET_DATA,
  payload: {
    data
  }
})
