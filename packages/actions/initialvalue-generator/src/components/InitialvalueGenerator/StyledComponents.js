import styled from 'styled-components'
import {theme, scale} from 'tocco-ui'

export const StyledContainer = styled.div`
  background-color: ${theme.color('paper')};
  overflow-y: auto;
  display: flex;
  justify-content: center;
  height: 100dvh;
  padding: 0;
`

export const StyledWrapper = styled.div`
  margin: ${scale.space(0.5)};
  width: 100%;
`
