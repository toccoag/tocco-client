import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {fetchData} from '../../modules/initialvalueGenerator'

import InitialvalueGenerator from './InitialvalueGenerator'

const mapActionCreators = {
  fetchData
}

const mapStateToProps = state => ({
  data: state.initialvalueGenerator.data
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(InitialvalueGenerator))
