import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {Layout, LoadMask} from 'tocco-ui'

import InitialvalueGeneratorItem from '../InitialvalueGeneratorItem'

import {StyledContainer, StyledWrapper} from './StyledComponents'

const InitialvalueGenerator = ({data, fetchData, intl}) => {
  useEffect(() => {
    fetchData()
  }, [fetchData])
  return (
    <LoadMask required={[data]}>
      <StyledContainer>
        <StyledWrapper>
          {data?.map(item => (
            <Layout.Container key={item.filename}>
              <InitialvalueGeneratorItem filename={item.filename} content={item.content} intl={intl} />
            </Layout.Container>
          ))}
        </StyledWrapper>
      </StyledContainer>
    </LoadMask>
  )
}

InitialvalueGenerator.propTypes = {
  intl: PropTypes.object.isRequired,
  fetchData: PropTypes.func,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      filename: PropTypes.string,
      content: PropTypes.string
    })
  )
}

export default InitialvalueGenerator
