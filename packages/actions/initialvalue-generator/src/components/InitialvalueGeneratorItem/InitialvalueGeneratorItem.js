import PropTypes from 'prop-types'
import {Typography, Layout, Panel} from 'tocco-ui'
import {js} from 'tocco-util'

import {StyledButton, StyledTextAreaItem, StyledTextarea} from './StyledComponents'

const InitialvalueGeneratorItem = ({content, filename, intl}) => {
  const numberOfLines = content?.split(/\r\n|\r|\n/).length

  const copyChangesets = e => {
    e.stopPropagation()
    js.copyToClipboard(content)
  }

  return (
    <Layout.Box>
      <Layout.Box>
        <Panel.Wrapper isToggleable={true}>
          <Panel.Header>
            <Typography.H4>
              {filename}
              <StyledButton
                ink="base"
                look="raised"
                onClick={copyChangesets}
                label={intl.formatMessage({id: 'client.initialvalue-generator.copy'})}
              />
            </Typography.H4>
          </Panel.Header>
          <Panel.Body>
            <StyledTextAreaItem>
              <StyledTextarea value={content || ''} readOnly={true} rows={numberOfLines} />
            </StyledTextAreaItem>
          </Panel.Body>
        </Panel.Wrapper>
      </Layout.Box>
    </Layout.Box>
  )
}

InitialvalueGeneratorItem.propTypes = {
  intl: PropTypes.object.isRequired,
  content: PropTypes.string,
  filename: PropTypes.string
}

export default InitialvalueGeneratorItem
