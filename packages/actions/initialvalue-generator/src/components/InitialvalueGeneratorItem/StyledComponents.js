import styled from 'styled-components'
import {theme, scale, declareFont, Button} from 'tocco-ui'

export const StyledButton = styled(Button)`
  float: right;
`

export const StyledTextAreaItem = styled.div`
  text-align: start;
  height: 100%;
`
export const StyledTextarea = styled.textarea`
  resize: none;
  margin: 0;
  overflow-y: hidden;
  min-height: 60px;
  width: 100%;
  height: 100%;
  box-sizing: border-box;
  background-color: transparent;
  border: 0;
  cursor: ${({immutable}) => (immutable ? 'not-allowed' : 'default')};
  min-width: 100%;
  outline: 0;
  padding: 0;
  padding-top: ${scale.space(-0.5)};
  padding-left: ${scale.space(0.5)};
  padding-right: ${scale.space(-0.5)};
  ${() =>
    declareFont({
      color: theme.color('text'),
      fontFamily: theme.fontFamily('monospace')
    })}

  &::-ms-clear {
    display: none;
  }

  &::-webkit-clear-button {
    display: none;
  }

  &::-webkit-inner-spin-button {
    display: none;
  }

  &:disabled {
    -webkit-text-fill-color: ${theme.color('text')}; /* Safari fix */
    opacity: 1; /* iOS fix */
  }

  /* allow pointer event only on touch devices */
  @media (pointer: coarse) {
    pointer-events: ${({immutable}) => (immutable ? 'none' : 'auto')};
  }
`
