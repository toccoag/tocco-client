/* eslint-disable react/prop-types */
import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {field} from 'tocco-app-extensions'
import {Table, LoadMask} from 'tocco-ui'

import {StyledTableWrapper} from './StyledComponents'

const FieldComponent = ({type, value}) => {
  const Field = field.factory('readOnly', type)
  return <Field formField={{dataType: type}} value={value} mappingType="list" />
}

FieldComponent.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}

const DisplayAccountings = ({displayAccountings, loadDisplayAccountings, intl}) => {
  const columns = [
    {
      idx: 0,
      id: 'debitAccount',
      label: intl.formatMessage({id: 'client.actions.display-accountings.debitAccount'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => <FieldComponent type={'integer'} value={rowData.debitAccount} />
    },
    {
      idx: 1,
      id: 'creditAccount',
      label: intl.formatMessage({id: 'client.actions.display-accountings.creditAccount'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => <FieldComponent type={'integer'} value={rowData.creditAccount} />
    },
    {
      idx: 2,
      id: 'amount',
      label: intl.formatMessage({id: 'client.actions.display-accountings.amount'}),
      alignment: 'left',
      CellRenderer: props => <FieldComponent type={'moneyamount'} value={props.rowData.amount} />
    },
    {
      idx: 3,
      id: 'currency',
      label: intl.formatMessage({id: 'client.actions.display-accountings.currency'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => <FieldComponent type={'string'} value={rowData.currency} />
    }
  ]

  useEffect(() => {
    loadDisplayAccountings()
  }, [loadDisplayAccountings])

  return (
    <LoadMask required={[displayAccountings]}>
      {displayAccountings && (
        <StyledTableWrapper>
          <Table
            scrollBehaviour="none"
            columns={columns}
            data={displayAccountings.map((displayAccounting, index) => ({...displayAccounting, __key: index}))}
          />
        </StyledTableWrapper>
      )}
    </LoadMask>
  )
}
DisplayAccountings.propTypes = {
  displayAccountings: PropTypes.arrayOf(PropTypes.object),
  loadDisplayAccountings: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}
export default DisplayAccountings
