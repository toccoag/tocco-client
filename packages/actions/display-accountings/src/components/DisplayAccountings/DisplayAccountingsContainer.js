import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadDisplayAccountings} from '../../modules/displayAccountings/actions'

import DisplayAccountings from './DisplayAccountings'

const mapActionCreators = {
  loadDisplayAccountings
}

const mapStateToProps = state => ({
  displayAccountings: state.displayAccountings.displayAccountings
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(DisplayAccountings))
