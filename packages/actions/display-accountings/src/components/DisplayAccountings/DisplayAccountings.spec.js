import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import DisplayAccountings from './DisplayAccountings'

describe('display-accountings', () => {
  describe('components', () => {
    describe('DisplayAccountings', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })
      test('should render accountings', () => {
        testingLibrary.renderWithIntl(
          <DisplayAccountings
            intl={IntlStub}
            displayAccountings={[
              {
                debitAccount: '1100',
                creditAccount: '3000',
                amount: '90',
                currency: 'CHF'
              }
            ]}
            loadDisplayAccountings={() => {}}
          />
        )

        expect(screen.getAllByText('client.actions.display-accountings.debitAccount')).to.exist
        expect(screen.getAllByText('client.actions.display-accountings.creditAccount')).to.exist
        expect(screen.getAllByText('client.actions.display-accountings.amount')).to.exist
        expect(screen.getAllByText('client.actions.display-accountings.currency')).to.exist

        expect(screen.findAllByDisplayValue('1100')).to.exist
        expect(screen.findAllByDisplayValue('3000')).to.exist
        expect(screen.findAllByDisplayValue('90')).to.exist
        expect(screen.findAllByDisplayValue('CHF')).to.exist
      })
    })
  })
})
