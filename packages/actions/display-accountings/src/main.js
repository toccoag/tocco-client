import {appFactory, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import DisplayAccountings from './components/DisplayAccountings'
import reducers, {sagas} from './modules/reducers'

const packageName = 'display-accountings'

const initApp = (id, input, events, publicPath) => {
  const content = <DisplayAccountings />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const DisplayAccountingsApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

DisplayAccountingsApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  /**
   * Selection of one or multiple `Voucher`
   */
  selection: selection.propType.isRequired
}

export default DisplayAccountingsApp
