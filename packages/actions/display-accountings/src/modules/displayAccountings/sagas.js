import {all, call, put, takeEvery, select} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(actions.LOAD_DISPLAY_ACCOUNTINGS, loadDisplayAccountings)])
}

export function* loadDisplayAccountings() {
  const {selection} = yield select(inputSelector)

  const response = yield call(rest.requestSaga, 'finance/actions/displayAccountings', {
    method: 'POST',
    body: selection
  })

  yield put(actions.setDisplayAccountings(response.body.items))
}
