import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('display-accountings', () => {
  describe('modules', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([takeEvery(actions.LOAD_DISPLAY_ACCOUNTINGS, sagas.loadDisplayAccountings)])
          )
          expect(generator.next().done).to.equal(true)
        })
      })
      describe('loadDisplayAccountings', () => {
        test('should set data', () => {
          const selection = {entityName: 'Voucher', type: 'ID', ids: ['5202']}
          const displayAccountingsLoaded = [
            {
              debitAccount: '1100',
              creditAccount: '3000',
              amount: '90',
              currency: 'CHF'
            }
          ]
          return expectSaga(sagas.loadDisplayAccountings)
            .provide([
              [select(sagas.inputSelector), {selection}],
              [
                matchers.call(rest.requestSaga, 'finance/actions/displayAccountings', {
                  method: 'POST',
                  body: selection
                }),
                {body: {items: displayAccountingsLoaded}}
              ]
            ])
            .put(actions.setDisplayAccountings(displayAccountingsLoaded))
            .run()
        })
      })
    })
  })
})
