export const LOAD_DISPLAY_ACCOUNTINGS = 'displayAccountings/LOAD_DISPLAY_ACCOUNTINGS'
export const SET_DISPLAY_ACCOUNTINGS = 'displayAccountings/SET_DISPLAY_ACCOUNTINGS'

export const loadDisplayAccountings = () => ({
  type: LOAD_DISPLAY_ACCOUNTINGS
})

export const setDisplayAccountings = displayAccountings => ({
  type: SET_DISPLAY_ACCOUNTINGS,
  payload: {displayAccountings}
})
