import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  displayAccountings: null
}

const ACTION_HANDLERS = {
  [actions.SET_DISPLAY_ACCOUNTINGS]: reducerUtil.singleTransferReducer('displayAccountings')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
