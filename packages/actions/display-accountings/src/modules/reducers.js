import displayAccountingsReducer, {sagas as displayAccountingsSagas} from './displayAccountings'

export default {
  displayAccountings: displayAccountingsReducer
}

export const sagas = [displayAccountingsSagas]
