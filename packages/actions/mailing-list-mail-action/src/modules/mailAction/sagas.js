import _isEmpty from 'lodash/isEmpty'
import {getFormValues, actions as formActions} from 'redux-form'
import {all, call, put, select, takeLatest, debounce} from 'redux-saga/effects'
import {rest, form, externalEvents} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/MailAction'

import * as actions from './actions'

export const mailActionSelector = state => state.mailAction
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([
    takeLatest(actions.INIT_MAIL_ACTION, initMailAction),
    takeLatest(actions.SEND_MAIL, sendMail),
    debounce(500, actions.VALIDATE, validate)
  ])
}

export function* initMailAction() {
  yield call(loadFormDefinition)
  const emailTemplate = yield call(loadEmailTemplate)
  const formValues = {
    subject: emailTemplate?.subject?.value,
    cc: emailTemplate?.cc?.value,
    template: emailTemplate?.mail_text?.value
  }
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* loadFormDefinition() {
  const formDefinition = yield call(rest.fetchForm, 'Mailing_list_mail_action', 'create')
  yield put(actions.setFormDefinition(formDefinition))
}

export function* loadEmailTemplate() {
  const {
    actionProperties: {emailTemplateUniqueId}
  } = yield select(inputSelector)

  if (!emailTemplateUniqueId) {
    return {}
  }

  const where = `unique_id=="${emailTemplateUniqueId}"`
  const emailTemplate = yield call(
    rest.fetchEntitiesPage,
    'Email_template',
    {where, paths: ['subject', 'cc', 'mail_text']},
    {method: 'GET'}
  )
  return emailTemplate.length > 0 ? emailTemplate[0].paths : {}
}

export function* validate() {
  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {formDefinition} = yield select(mailActionSelector)
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)
  const validator = yield call(form.syncValidation, fieldDefinitions, formDefinition)
  const errors = yield call(validator, formValues)
  yield put(actions.setFormValid(_isEmpty(errors)))
}

export function* sendMail() {
  yield put(actions.setFormValid(false))
  const {
    selection,
    actionProperties: {eventKey}
  } = yield select(inputSelector)
  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {formDefinition} = yield select(mailActionSelector)
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)
  const flattened = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const mailSettings = api.toEntity(flattened)
  const resource = '/actions/MailingListMailAction/send'
  const options = {
    method: 'POST',
    body: {
      selection,
      mailSettings,
      eventKey
    }
  }
  const response = yield call(rest.requestSaga, resource, options)
  const type = response.body.success ? 'onSuccess' : 'onError'
  yield put(
    externalEvents.fireExternalEvent(type, {
      title: response.body.message
    })
  )
}
