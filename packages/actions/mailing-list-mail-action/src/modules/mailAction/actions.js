export const INIT_MAIL_ACTION = 'mailAction/INIT_MAIL_ACTION'
export const SET_FORM_DEFINITION = 'mailAction/SET_FORM_DEFINITION'
export const SEND_MAIL = 'mailAction/SEND_MAIL'
export const VALIDATE = 'mailAction/VALIDATE'
export const SET_FORM_VALID = 'mailAction/SET_FORM_VALID'

export const initMailAction = () => ({
  type: INIT_MAIL_ACTION
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const sendMail = () => ({
  type: SEND_MAIL
})

export const validate = () => ({
  type: VALIDATE
})

export const setFormValid = formValid => ({
  type: SET_FORM_VALID,
  payload: {formValid}
})
