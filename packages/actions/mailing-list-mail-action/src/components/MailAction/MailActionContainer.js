import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initMailAction, sendMail, validate} from '../../modules/mailAction/actions'

import MailAction from './MailAction'

const mapActionCreators = {
  initMailAction,
  sendMail,
  validate
}

const mapStateToProps = state => ({
  formDefinition: state.mailAction.formDefinition,
  formValid: state.mailAction.formValid
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(MailAction))
