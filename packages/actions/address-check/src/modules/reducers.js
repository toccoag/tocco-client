import AddressCheckReducer, {sagas as addressCheckSagas} from './addressCheck'

export default {
  addressCheck: AddressCheckReducer
}

export const sagas = [addressCheckSagas]
