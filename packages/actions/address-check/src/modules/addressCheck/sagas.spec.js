import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('address-check', () => {
  describe('modules', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([takeEvery(actions.LOAD_ADDRESS_CHECK, sagas.loadAddressCheck)])
          )
          expect(generator.next().done).to.equal(true)
        })
      })
      describe('loadAddressCheck', () => {
        test('should set data', () => {
          const addressCheckLoaded = {
            salutation: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
            correspondenceAddress: 'BBC<br/>test<br/>TV<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
            deliveryAddress: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
            billingAddress: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>'
          }
          const response = {body: {addressCheck: addressCheckLoaded}}
          return expectSaga(sagas.loadAddressCheck)
            .provide([
              [select(sagas.inputSelector), {selection: {entityName: 'User', type: 'ID', ids: ['13959']}}],
              [matchers.call.fn(rest.requestSaga), response]
            ])
            .call(rest.requestSaga, '/address/actions/addressCheck/13959', {method: 'GET'})
            .put(actions.setAddressCheck({addressCheck: addressCheckLoaded}))
            .run()
        })
      })
    })
  })
})
