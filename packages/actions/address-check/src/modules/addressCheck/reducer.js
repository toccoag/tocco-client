import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  addressCheck: null
}

const ACTION_HANDLERS = {
  [actions.SET_ADDRESS_CHECK]: reducerUtil.singleTransferReducer('addressCheck')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
