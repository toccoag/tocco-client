export const LOAD_ADDRESS_CHECK = 'tipgame/LOAD_ADDRESS_CHECK'
export const SET_ADDRESS_CHECK = 'tipgame/SET_ADDRESS_CHECK'

export const loadAddressCheck = () => ({
  type: LOAD_ADDRESS_CHECK
})

export const setAddressCheck = addressCheck => ({
  type: SET_ADDRESS_CHECK,
  payload: {addressCheck}
})
