import {setAddressCheck} from './actions'
import reducer from './reducer'

describe('address-check', () => {
  describe('modules', () => {
    describe('reducer', () => {
      test('should set data', () => {
        const addressCheckLoaded = {
          salutation: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
          correspondenceAddress: 'BBC<br/>test<br/>TV<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
          deliveryAddress: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
          billingAddress: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>'
        }
        const result = reducer({}, setAddressCheck(addressCheckLoaded))
        expect(result).to.be.eql({addressCheck: addressCheckLoaded})
      })
    })
  })
})
