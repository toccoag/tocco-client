import {appFactory, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import AddressCheck from './components/AddressCheckAction'
import reducers, {sagas} from './modules/reducers'

const packageName = 'address-check'

const initApp = (id, input, events, publicPath) => {
  const content = <AddressCheck />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const AddressCheckApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

AddressCheckApp.propTypes = {
  /**
   * Selection of a single `User` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired
}

export default AddressCheckApp
