import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadAddressCheck} from '../../modules/addressCheck/actions'

import AddressCheck from './AddressCheck'

const mapActionCreators = {
  loadAddressCheck
}

const mapStateToProps = state => ({
  addressCheck: state.addressCheck.addressCheck
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(AddressCheck))
