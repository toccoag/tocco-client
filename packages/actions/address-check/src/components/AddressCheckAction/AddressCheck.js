import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import {field} from 'tocco-app-extensions'
import {Typography, LoadMask, StatedValue, Layout, Panel} from 'tocco-ui'

const FieldComponent = ({type, value}) => {
  const Field = field.factory('readOnly', type)
  return <Field formField={{dataType: type}} value={value} mappingType="readOnly" />
}
FieldComponent.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}
const ValueComponent = ({id, type, value}) => {
  return (
    <StatedValue key={id} immutable={true} hasValue={true} labelPosition="inside">
      <FieldComponent type={type} value={value} />
    </StatedValue>
  )
}

ValueComponent.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}

const AddressCheck = ({addressCheck, loadAddressCheck}) => {
  useEffect(() => {
    loadAddressCheck()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <LoadMask required={[addressCheck]}>
      {addressCheck && (
        <Layout.Container key={'addressCheck'}>
          <Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H4>
                    <FormattedMessage id={'client.actions.address-check.salutation'} />
                  </Typography.H4>
                </Panel.Header>
                <Panel.Body>
                  <ValueComponent id={'salutation'} type="html" value={addressCheck.salutation} />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H4>
                    <FormattedMessage id={'client.actions.address-check.correspondenceAddress'} />
                  </Typography.H4>
                </Panel.Header>
                <Panel.Body>
                  <ValueComponent id={'correspondenceAddress'} type="html" value={addressCheck.correspondenceAddress} />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
          </Layout.Box>
          <Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H4>
                    <FormattedMessage id={'client.actions.address-check.deliveryAddress'} />
                  </Typography.H4>
                </Panel.Header>
                <Panel.Body>
                  <ValueComponent id={'deliveryAddress'} type="html" value={addressCheck.deliveryAddress} />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H4>
                    <FormattedMessage id={'client.actions.address-check.billingAddress'} />
                  </Typography.H4>
                </Panel.Header>
                <Panel.Body>
                  <ValueComponent id={'billingAddress'} type="html" value={addressCheck.billingAddress} />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
          </Layout.Box>
        </Layout.Container>
      )}
    </LoadMask>
  )
}
AddressCheck.propTypes = {
  addressCheck: PropTypes.object,
  loadAddressCheck: PropTypes.func.isRequired
}
export default AddressCheck
