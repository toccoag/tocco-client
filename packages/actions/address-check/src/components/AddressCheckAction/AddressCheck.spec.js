import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import AddressCheck from './AddressCheck'

describe('address-check', () => {
  describe('components', () => {
    describe('Action', () => {
      test('should render salutation and addresses', () => {
        testingLibrary.renderWithIntl(
          <AddressCheck
            addressCheck={{
              salutation: 'BBA<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
              correspondenceAddress: 'BBB<br/>test<br/>TV<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
              deliveryAddress: 'BBC<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>',
              billingAddress: 'BBD<br/>test<br/>TV<br/>test<br/>test<br/>Am Ende<br/>6016 Hellbühl<br/><br/>'
            }}
            loadAddressCheck={() => {}}
          />
        )

        expect(screen.getAllByText('client.actions.address-check.salutation')).to.exist
        expect(screen.getAllByText('client.actions.address-check.correspondenceAddress')).to.exist
        expect(screen.getAllByText('client.actions.address-check.deliveryAddress')).to.exist
        expect(screen.getAllByText('client.actions.address-check.billingAddress')).to.exist

        expect(screen.findAllByDisplayValue('BBA')).to.exist
        expect(screen.findAllByDisplayValue('BBB')).to.exist
        expect(screen.findAllByDisplayValue('BBC')).to.exist
        expect(screen.findAllByDisplayValue('BBD')).to.exist
        expect(screen.findAllByDisplayValue('6016 Hellbühl')).to.exist
      })
    })
  })
})
