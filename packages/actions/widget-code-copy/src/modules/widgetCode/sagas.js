import {all, call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {rest, notification, selection as selectionUtil, externalEvents} from 'tocco-app-extensions'
import {js, consoleLogger} from 'tocco-util'

import {generateWidgetCode} from '../../utils/widgetCode'

import * as actions from './actions'

export const inputSelector = state => state.input
export const widgetCodeSelector = state => state.widgetCode

export default function* sagas() {
  yield all([
    takeEvery(actions.FETCH_WIDGET_CONFIG, fetchWidgetConfig),
    takeLatest(actions.COPY_WIDGET_CODE, copyWidgetCode)
  ])
}

export function* fetchWidgetConfig() {
  const {selection} = yield select(inputSelector)

  try {
    const entities = yield call(selectionUtil.getEntities, selection, rest.fetchEntitiesPage)
    if (entities.keys.length === 1 && entities.entityName === 'Widget_config') {
      const widgetConfigKey = entities.keys[0]
      const widgetConfig = yield call(rest.fetchEntity, 'Widget_config', widgetConfigKey, {paths: ['unique_id']})
      yield put(actions.setWidgetConfig(widgetConfig))
    } else {
      throw new Error('Invalid selection')
    }
  } catch (err) {
    consoleLogger.logError('Failed to get selected Widget_config', err)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.widget-code-copy.toasterTitle',
        body: 'client.actions.widget-code-copy.fetchFailedMessage'
      })
    )
  }
}

export function* copyWidgetCode() {
  const {widgetConfig} = yield select(widgetCodeSelector)
  try {
    const widgetCode = generateWidgetCode(widgetConfig)
    yield call(js.copyToClipboard, widgetCode)
    yield put(
      externalEvents.fireExternalEvent('onSuccess', {
        title: 'client.actions.widget-code-copy.toasterTitle',
        message: 'client.actions.widget-code-copy.copiedMessage'
      })
    )
  } catch (err) {
    consoleLogger.logError('Failed to generate and copy widget code', err)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.widget-code-copy.toasterTitle',
        body: 'client.actions.widget-code-copy.copyFailedMessage'
      })
    )
  }
}
