export const generateWidgetCode = widgetConfig => {
  const widgetCode = `<div data-tocco-widget-key="${widgetConfig.paths.unique_id.value}"></div>`
  return widgetCode
}
