import PropTypes from 'prop-types'
import {
  appFactory,
  selection,
  actions,
  formData,
  notification,
  form,
  externalEvents,
  actionEmitter
} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import Massmutation from './components/MassMutationContainer'
import reducers, {sagas, formSagaConfig} from './modules/reducers'

const packageName = 'massmutation'

const EXTERNAL_EVENTS = ['onSuccess', 'onError', 'emitAction']
const initApp = (id, input, events, publicPath) => {
  const content = <Massmutation />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  formData.addToStore(store, () => ({
    listApp: EntityListApp
  }))
  notification.addToStore(store, false)
  form.addToStore(store, formSagaConfig)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const MassmutationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

MassmutationApp.propTypes = {
  selection: selection.propType.isRequired,
  actionProperties: PropTypes.shape({
    type: PropTypes.string.isRequired
  })
}

export default MassmutationApp
