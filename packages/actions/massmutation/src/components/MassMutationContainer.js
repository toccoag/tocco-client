import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadMassMutationForm, applyMutation} from '../modules/massMutation/actions'

import MassMutation from './MassMutation'

const mapActionCreators = {
  loadMassMutationForm,
  applyMutation
}

const mapStateToProps = state => ({
  formDefinition: state.massMutation.formDefinition,
  pending: state.massMutation.pending,
  type: state.input.actionProperties.type
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(MassMutation))
