import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button, LoadMask, StyledStickyButtonWrapper} from 'tocco-ui'

import {formSagaConfig} from '../modules/massMutation'

const MassMutation = ({intl, pending, formDefinition, formValues, loadMassMutationForm, applyMutation, type}) => {
  useEffect(() => {
    loadMassMutationForm()
  }, [loadMassMutationForm])

  return (
    <LoadMask required={[formDefinition]}>
      <form.FormBuilder
        entity={{}}
        formName={formSagaConfig.formId}
        formDefinition={formDefinition}
        formValues={formValues || {}}
        mode="create"
        fieldMappingType="editable"
        readonly={pending}
      />
      <StyledStickyButtonWrapper>
        <Button
          look="raised"
          onClick={applyMutation}
          ink="primary"
          pending={pending}
          disabled={pending}
          label={intl.formatMessage({id: `client.massmutation.${type}`})}
        />
      </StyledStickyButtonWrapper>
    </LoadMask>
  )
}

MassMutation.propTypes = {
  intl: PropTypes.object.isRequired,
  pending: PropTypes.bool,
  formDefinition: PropTypes.object,
  formValues: PropTypes.object,
  loadMassMutationForm: PropTypes.func.isRequired,
  applyMutation: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired
}

export default reduxForm({form: formSagaConfig.formId, destroyOnUnmount: false})(MassMutation)
