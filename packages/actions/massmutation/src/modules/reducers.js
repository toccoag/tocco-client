import {reducer as form} from 'redux-form'

import massMutationReducer, {sagas as massMutationSagas, formSagaConfig} from './massMutation'

export default {
  massMutation: massMutationReducer,
  form
}

export const sagas = [massMutationSagas]
export {formSagaConfig}
