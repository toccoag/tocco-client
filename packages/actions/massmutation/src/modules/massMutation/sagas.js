import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, select, takeEvery, put} from 'redux-saga/effects'
import {selection as selectionUtil, form, rest, externalEvents, notification} from 'tocco-app-extensions'
import {api, validation} from 'tocco-util'

import * as actions from './actions'

export const inputSelector = state => state.input
export const massMutationSelector = state => state.massMutation

export const formStateSelector = state => ({
  ...inputSelector(state),
  ...massMutationSelector(state)
})

export const FORM_ID = 'massMutation'

export const formSagaConfig = {
  formId: FORM_ID,
  stateSelector: formStateSelector
}

export default function* sagas() {
  yield all([
    takeEvery(actions.APPLY_MUTATION, applyMutation),
    takeEvery(actions.LOAD_MASS_MUTATION_FORM, loadMassMutationForm)
  ])
}

export function* applyMutation() {
  yield put(actions.setPending(true))

  const {fieldDefinitions} = yield select(massMutationSelector)
  const formValues = yield select(getFormValues(FORM_ID))
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const entity = yield call(api.toEntity, flatEntity)

  const {selection, actionProperties} = yield select(inputSelector)
  const options = {
    method: 'POST',
    body: {entity, selection},
    acceptedErrorCodes: ['VALIDATION_FAILED'],
    acceptedStatusCodes: [400]
  }

  const response = yield call(runRequest, actionProperties.type, options)

  if (response.status === 400 && response.body.errorCode === 'VALIDATION_FAILED') {
    const errorBody = yield call(validation.getErrorCompact, response.body.errors)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.component.actions.validationError',
        body: errorBody
      })
    )
    yield put(actions.setPending(false))
  } else if (response.ok) {
    const {entityName, keys} = yield call(selectionUtil.getEntities, selection, rest.fetchAllEntities)
    const remoteEvents = [
      {
        type: 'entity-update-event',
        payload: {
          entities: keys.map(key => ({entityName, key}))
        }
      }
    ]

    yield put(externalEvents.fireExternalEvent('onSuccess', {remoteEvents}))
  }
}

function* runRequest(type, options) {
  try {
    return yield call(rest.requestSaga, `/client/massMutation/${type}`, options)
  } catch (error) {
    yield put(externalEvents.fireExternalEvent('onError'))
    return {}
  }
}

export function* loadMassMutationForm() {
  const {selection, actionProperties} = yield select(inputSelector)
  const {entityName} = selection
  const detailForm = yield call(rest.fetchForm, entityName, 'update')
  const {
    body: {paths: ignoredPaths}
  } = yield call(rest.requestSaga, `/client/massMutation/ignoredPaths/${entityName}`, {
    method: 'GET'
  })
  const massMutationForm = yield call(modifyFormDefinition, actionProperties.type, detailForm, ignoredPaths)
  const fieldDefinitions = yield call(form.getFieldDefinitions, massMutationForm)

  const flattenEntity = yield call(api.getFlattenEntity, {model: entityName, paths: {}})
  const formValues = yield call(form.entityToFormValues, flattenEntity, fieldDefinitions)
  yield put(formActions.initialize(FORM_ID, formValues))

  yield put(actions.setFieldDefinitions(fieldDefinitions))
  yield put(actions.setFormDefinition(massMutationForm))
}

export function* modifyFormDefinition(type, formDefinition, ignoredPaths) {
  switch (type) {
    case 'replace':
      return yield call(modifyReplaceFormDefinition, formDefinition, ignoredPaths)
    case 'add':
      return yield call(modifyAddFormDefinition, formDefinition, ignoredPaths)
    case 'remove':
      return yield call(modifyRemoveFormDefinition, formDefinition, ignoredPaths)
  }
}

const modifyReplaceFormDefinition = (formDefinition, ignoredPaths) => {
  return [
    fd => form.adjustFields(fd, getFieldAsOptional),
    fd => form.adjustAllActions(fd, () => false),
    fd => form.removeChildren(fd, item => item.path && ignoredPaths.includes(item.path))
  ].reduce((fd, adjuster) => adjuster(fd), formDefinition)
}

const modifyAddFormDefinition = (formDefinition, ignoredPaths) => {
  const showedDataTypes = [
    // string based (e.g. email or phone are not shown as a change results normally in a validation error)
    'string',
    'text',
    'html',
    // numeric
    'integer',
    'decimal',
    'percent',
    'duration',
    'moneyamount',
    'sorting',
    // to many relations
    'multi-select-box',
    'multi-remote-field'
  ]

  return [
    fd => form.adjustFields(fd, getFieldAsOptional),
    fd => form.adjustAllActions(fd, () => false),
    fd => form.removeChildren(fd, item => item.path && ignoredPaths.includes(item.path)),
    fd => form.removeChildren(fd, item => item.dataType && !showedDataTypes.includes(item.dataType))
  ].reduce((fd, adjuster) => adjuster(fd), formDefinition)
}

const modifyRemoveFormDefinition = (formDefinition, ignoredPaths) => {
  return [
    fd => form.removeChildren(fd, item => item?.validation?.mandatory === true),
    fd => form.adjustAllActions(fd, () => false),
    fd => form.removeChildren(fd, item => item.path && ignoredPaths.includes(item.path)),
    fd => form.adjustFields(fd, mapFieldsToCheckbox)
  ].reduce((fd, adjuster) => adjuster(fd), formDefinition)
}

const getFieldAsOptional = field => ({
  ...field,
  validation: {
    ...field.validation,
    mandatory: false
  }
})

const mapFieldsToCheckbox = field => {
  if (field.dataType === 'multi-remote-field' || field.dataType === 'multi-select-box') {
    return field
  }

  return {
    ...field,
    dataType: 'boolean'
  }
}
