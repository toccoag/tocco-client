export const APPLY_MUTATION = 'massMutation/APPLY_MUTATION'
export const LOAD_MASS_MUTATION_FORM = 'massMutation/LOAD_MASS_MUTATION_FORM'
export const SET_FIELD_DEFINITIONS = 'massMutation/SET_FIELD_DEFINITIONS'
export const SET_FORM_DEFINITION = 'massMutation/SET_FORM_DEFINITION'
export const SET_PENDING = 'massMutation/SET_PENDING'

export const loadMassMutationForm = () => ({
  type: LOAD_MASS_MUTATION_FORM
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const applyMutation = () => ({
  type: APPLY_MUTATION
})

export const setPending = pending => ({
  type: SET_PENDING,
  payload: {pending}
})
