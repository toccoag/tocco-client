import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {form, rest, externalEvents, notification, selection as selectionUtil} from 'tocco-app-extensions'
import {api, validation} from 'tocco-util'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('massmutation', () => {
  describe('sagas', () => {
    describe('rootSaga', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([
            takeEvery(actions.APPLY_MUTATION, sagas.applyMutation),
            takeEvery(actions.LOAD_MASS_MUTATION_FORM, sagas.loadMassMutationForm)
          ])
        )
        expect(generator.next().done).to.equal(true)
      })
    })

    describe('loadMassMutationForm', () => {
      test('should load form and adjust', () => {
        const selection = {entityName: 'User', type: 'ID', ids: ['13959']}
        const formDefinition = {
          children: [
            {
              componentType: 'field',
              validation: {mandatory: false}
            },
            {
              id: 'save',
              componentType: 'action'
            },
            {
              path: 'field',
              componentType: 'field',
              validation: {mandatory: false}
            }
          ]
        }
        const fieldDefinitions = [{id: 'field'}]
        const flatEntity = {path: 'flat'}
        const formValues = {path: 'flat'}
        return expectSaga(sagas.loadMassMutationForm)
          .provide([
            [select(sagas.inputSelector), {selection, actionProperties: {type: 'replace'}}],
            [matchers.call(rest.fetchForm, 'User', 'update'), formDefinition],
            [matchers.call.fn(form.getFieldDefinitions), fieldDefinitions],
            [matchers.call.fn(api.getFlattenEntity), flatEntity],
            [matchers.call.fn(form.entityToFormValues), formValues],
            [matchers.call.fn(sagas.modifyFormDefinition), formDefinition],
            [
              matchers.call(rest.requestSaga, `/client/massMutation/ignoredPaths/User`, {
                method: 'GET'
              }),
              {body: {paths: ['excluded']}}
            ]
          ])
          .put(actions.setFieldDefinitions(fieldDefinitions))
          .put(actions.setFormDefinition(formDefinition))
          .run()
      })
    })

    describe('modifyFormDefinition', () => {
      const formDefinition = {
        children: [
          {
            componentType: 'field',
            validation: {mandatory: true}
          },
          {
            id: 'save',
            componentType: 'action'
          },
          {
            id: 'action',
            componentType: 'action'
          },
          {
            path: 'field',
            componentType: 'field'
          },
          {
            path: 'excluded',
            componentType: 'field'
          },
          {
            path: 'email',
            componentType: 'field',
            dataType: 'email'
          },
          {
            path: 'single-relation-optional',
            componentType: 'field',
            dataType: 'single-select-box'
          },
          {
            path: 'single-relation-mandatory',
            componentType: 'field',
            dataType: 'single-select-box',
            validation: {mandatory: true}
          },
          {
            path: 'multi-relation-optional',
            componentType: 'field',
            dataType: 'multi-select-box'
          },
          {
            path: 'multi-relation-mandatory',
            componentType: 'field',
            dataType: 'multi-select-box',
            validation: {mandatory: true}
          }
        ]
      }

      test('replace', () => {
        const expectedFormDefinition = {
          children: [
            {
              componentType: 'field',
              validation: {mandatory: false}
            },
            {
              path: 'field',
              componentType: 'field',
              validation: {mandatory: false}
            },
            {
              path: 'email',
              componentType: 'field',
              dataType: 'email',
              validation: {mandatory: false}
            },
            {
              path: 'single-relation-optional',
              componentType: 'field',
              dataType: 'single-select-box',
              validation: {mandatory: false}
            },
            {
              path: 'single-relation-mandatory',
              componentType: 'field',
              dataType: 'single-select-box',
              validation: {mandatory: false}
            },
            {
              path: 'multi-relation-optional',
              componentType: 'field',
              dataType: 'multi-select-box',
              validation: {mandatory: false}
            },
            {
              path: 'multi-relation-mandatory',
              componentType: 'field',
              dataType: 'multi-select-box',
              validation: {mandatory: false}
            }
          ]
        }
        const ignoredPaths = ['excluded']
        return expectSaga(sagas.modifyFormDefinition, 'replace', formDefinition, ignoredPaths)
          .returns(expectedFormDefinition)
          .run()
      })

      test('add', () => {
        const expectedFormDefinition = {
          children: [
            {
              componentType: 'field',
              validation: {mandatory: false}
            },
            {
              path: 'field',
              componentType: 'field',
              validation: {mandatory: false}
            },
            {
              path: 'multi-relation-optional',
              componentType: 'field',
              dataType: 'multi-select-box',
              validation: {mandatory: false}
            },
            {
              path: 'multi-relation-mandatory',
              componentType: 'field',
              dataType: 'multi-select-box',
              validation: {mandatory: false}
            }
          ]
        }
        const ignoredPaths = ['excluded']
        return expectSaga(sagas.modifyFormDefinition, 'add', formDefinition, ignoredPaths)
          .returns(expectedFormDefinition)
          .run()
      })

      test('remove', () => {
        const expectedFormDefinition = {
          children: [
            {
              path: 'field',
              componentType: 'field',
              dataType: 'boolean'
            },
            {
              path: 'email',
              componentType: 'field',
              dataType: 'boolean'
            },
            {
              path: 'single-relation-optional',
              componentType: 'field',
              dataType: 'boolean'
            },
            {
              path: 'multi-relation-optional',
              componentType: 'field',
              dataType: 'multi-select-box'
            }
          ]
        }
        const ignoredPaths = ['excluded']
        return expectSaga(sagas.modifyFormDefinition, 'remove', formDefinition, ignoredPaths)
          .returns(expectedFormDefinition)
          .run()
      })
    })

    describe('applyMutation', () => {
      const fieldDefinitions = [{id: 'field'}]
      const entity = {path: 'value'}
      const flatEntity = {path: 'flat'}
      const selection = {entityName: 'User', type: 'ID', ids: ['13959']}
      const entities = {entityName: 'User', keys: ['13959']}
      const options = {
        method: 'POST',
        body: {
          entity,
          selection
        },
        acceptedErrorCodes: ['VALIDATION_FAILED'],
        acceptedStatusCodes: [400]
      }

      test('should send update request', () => {
        return expectSaga(sagas.applyMutation)
          .provide([
            [select(sagas.massMutationSelector), {fieldDefinitions}],
            [select(sagas.inputSelector), {selection, actionProperties: {type: 'replace'}}],
            [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
            [matchers.call.fn(api.toEntity), entity],
            [matchers.call.fn(rest.requestSaga), {ok: true}],
            [matchers.call.fn(selectionUtil.getEntities), entities]
          ])
          .call(rest.requestSaga, '/client/massMutation/replace', options)
          .put(actions.setPending(true))
          .put(
            externalEvents.fireExternalEvent('onSuccess', {
              remoteEvents: [
                {
                  type: 'entity-update-event',
                  payload: {
                    entities: [{entityName: 'User', key: '13959'}]
                  }
                }
              ]
            })
          )
          .run()
      })

      test('should send error event', () => {
        return expectSaga(sagas.applyMutation)
          .provide([
            [select(sagas.massMutationSelector), {fieldDefinitions}],
            [select(sagas.inputSelector), {selection, actionProperties: {type: 'replace'}}],
            [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
            [matchers.call.fn(api.toEntity), entity],
            [matchers.call.fn(rest.requestSaga), throwError(new Error('error'))]
          ])
          .call(rest.requestSaga, '/client/massMutation/replace', options)
          .put(actions.setPending(true))
          .put(externalEvents.fireExternalEvent('onError'))
          .run()
      })

      test('should show toaster for validation', () => {
        const errors = {id: 'error'}
        return expectSaga(sagas.applyMutation)
          .provide([
            [select(sagas.massMutationSelector), {fieldDefinitions}],
            [select(sagas.inputSelector), {selection, actionProperties: {type: 'replace'}}],
            [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
            [matchers.call.fn(api.toEntity), entity],
            [matchers.call.fn(rest.requestSaga), {status: 400, body: {errorCode: 'VALIDATION_FAILED'}}],
            [matchers.call.fn(validation.getErrorCompact), errors]
          ])
          .call(rest.requestSaga, '/client/massMutation/replace', options)
          .put(actions.setPending(true))
          .put(
            notification.toaster({
              type: 'error',
              title: 'client.component.actions.validationError',
              body: errors
            })
          )
          .put(actions.setPending(false))
          .run()
      })
    })
  })
})
