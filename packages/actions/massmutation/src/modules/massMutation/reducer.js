import {reducer as reducerUtil} from 'tocco-util'

import {SET_FIELD_DEFINITIONS, SET_FORM_DEFINITION, SET_PENDING} from './actions'

const initialState = {
  fieldDefinitions: null,
  formDefinition: null,
  pending: false
}

const ACTION_HANDLERS = {
  [SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions'),
  [SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [SET_PENDING]: reducerUtil.singleTransferReducer('pending')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
