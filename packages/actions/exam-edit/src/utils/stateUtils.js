export const getTotalPoints = state =>
  state.examEdit.exams
    .map(exam => exam.maxPoints)
    .filter(points => !!points)
    .reduce((sum, points) => sum + points, 0)
export const arePointsValid = state => state.examEdit.maxPoints === getTotalPoints(state)
