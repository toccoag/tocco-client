import {getTotalPoints, arePointsValid} from './stateUtils'

describe('exam-edit', () => {
  describe('utils', () => {
    describe('stateUtils', () => {
      describe('getTotalPoints', () => {
        test('should sum max points', () => {
          const state = {
            examEdit: {
              exams: [{maxPoints: 10}, {maxPoints: 5}]
            }
          }
          const result = getTotalPoints(state)
          expect(result).to.eq(15)
        })
        test('should calculate zero without exams', () => {
          const state = {
            examEdit: {
              exams: []
            }
          }
          const result = getTotalPoints(state)
          expect(result).to.eq(0)
        })
        test('should calculate zero with no max points', () => {
          const state = {
            examEdit: {
              exams: [{nr: 1}, {nr: 2}]
            }
          }
          const result = getTotalPoints(state)
          expect(result).to.eq(0)
        })
      })

      describe('arePointsValid', () => {
        test('should be true if max points equal total', () => {
          const state = {
            examEdit: {
              exams: [{maxPoints: 10}, {maxPoints: 5}],
              maxPoints: 15
            }
          }
          const result = arePointsValid(state)
          expect(result).to.be.true
        })
        test('should be false if max points is null but total is not', () => {
          const state = {
            examEdit: {
              exams: [{maxPoints: 10}, {maxPoints: 5}]
            }
          }
          const result = arePointsValid(state)
          expect(result).to.be.false
        })
      })
    })
  })
})
