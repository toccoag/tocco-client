const inputTypes = {
  POINTS: 'POINTS',
  POINTS_THRESHOLD: 'POINTS_THRESHOLD',
  POINTS_AVERAGE: 'POINTS_AVERAGE',
  POINTS_AVERAGE_THRESHOLD: 'POINTS_AVERAGE_THRESHOLD',
  GRADES: 'GRADES',
  GRADES_MAX: 'GRADES_MAX',
  GRADES_MANDATORY: 'GRADES_MANDATORY',
  GRADES_DROP: 'GRADES_DROP'
}
const pointsSumTypes = [inputTypes.POINTS, inputTypes.POINTS_THRESHOLD]

export const isPointsSumType = type => pointsSumTypes.includes(type)
export default inputTypes
