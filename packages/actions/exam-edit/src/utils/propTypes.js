import PropTypes from 'prop-types'

export const examPropType = PropTypes.shape({
  nr: PropTypes.number.isRequired,
  label: PropTypes.string,
  maxPoints: PropTypes.number,
  weight: PropTypes.number,
  date: PropTypes.string
})
