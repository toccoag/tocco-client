import examEditReducer from './examEdit/reducer'
import examEditSagas from './examEdit/sagas'

export default {
  examEdit: examEditReducer
}

export const sagas = [examEditSagas]
