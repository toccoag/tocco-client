import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('exam-edit', () => {
  describe('modules', () => {
    describe('examEdit', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeLatest(actions.LOAD_DATA, sagas.loadData), takeLatest(actions.SAVE_DATA, sagas.saveData)])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadData', () => {
          test('should set data', () => {
            const selection = {ids: ['1'], entityName: 'Input', type: 'ID'}
            const result = {
              body: {
                maxPoints: 10,
                exams: [{nr: 1}],
                type: 'POINTS',
                isTemplate: true
              }
            }
            return expectSaga(sagas.loadData)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/load', {
                    method: 'POST',
                    body: selection
                  }),
                  result
                ],
                [select(sagas.selectionSelector), selection]
              ])
              .put(actions.setMaxPoints(10))
              .put(actions.setExams([{nr: 1, __key: 1}]))
              .put(actions.setType('POINTS'))
              .put(actions.setIsTemplate(true))
              .run()
          })
        })

        describe('saveData', () => {
          test('should save data and send success message', () => {
            const selection = {ids: ['1'], entityName: 'Input', type: 'ID'}
            const exams = [{nr: 1, __key: 1}]
            const request = {
              selection,
              exams: [{nr: 1}]
            }
            const response = {
              ok: true
            }
            return expectSaga(sagas.saveData)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/save', {
                    method: 'POST',
                    body: request,
                    returnCancelledClientQuestion: true
                  }),
                  response
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), exams]
              ])
              .put(actions.toggleSaving())
              .put(
                externalEvents.fireExternalEvent('onSuccess', {
                  message: 'client.actions.exam-edit.success'
                })
              )
              .run()
          })

          test('should send error message on failure', () => {
            const selection = {ids: ['1'], entityName: 'Input', type: 'ID'}
            const exams = []
            const request = {
              selection,
              exams
            }
            const response = {
              ok: false
            }
            return expectSaga(sagas.saveData)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/save', {
                    method: 'POST',
                    body: request,
                    returnCancelledClientQuestion: true
                  }),
                  response
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), exams]
              ])
              .put(actions.toggleSaving())
              .put(externalEvents.fireExternalEvent('onError'))
              .put(actions.toggleSaving())
              .run()
          })

          test('should reset on cancelled client question', () => {
            const selection = {ids: ['1'], entityName: 'Input', type: 'ID'}
            const exams = []
            const request = {
              selection,
              exams
            }
            const response = {
              ok: false,
              status: 412
            }
            return expectSaga(sagas.saveData)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/save', {
                    method: 'POST',
                    body: request,
                    returnCancelledClientQuestion: true
                  }),
                  response
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), exams],
                [matchers.call.fn(sagas.resetDeletedExams)]
              ])
              .put(actions.toggleSaving())
              .call(sagas.resetDeletedExams)
              .put(actions.toggleSaving())
              .run()
          })
        })

        describe('resetDeletedExams', () => {
          const selection = {ids: ['1'], entityName: 'Input', type: 'ID'}

          test('should recreate deleted exams from initial state', () => {
            const initialExams = [{key: 1, nr: 1}]
            const result = {
              body: {
                exams: initialExams
              }
            }
            const currentExams = []
            const expectedExams = [{key: 1, nr: 1, __key: 1}]
            return expectSaga(sagas.resetDeletedExams)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/load', {
                    method: 'POST',
                    body: selection
                  }),
                  result
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), currentExams]
              ])
              .put(actions.setExams(expectedExams))
              .run()
          })

          test('should keep changes to exams from initial state', () => {
            const initialExams = [{key: 3, nr: 1, value: 'old'}]
            const result = {
              body: {
                exams: initialExams
              }
            }
            const currentExams = [{key: 3, nr: 1, value: 'new'}]
            const expectedExams = [{key: 3, nr: 1, __key: 1, value: 'new'}]
            return expectSaga(sagas.resetDeletedExams)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/load', {
                    method: 'POST',
                    body: selection
                  }),
                  result
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), currentExams]
              ])
              .put(actions.setExams(expectedExams))
              .run()
          })

          test('should leave created exams unchanged', () => {
            const initialExams = []
            const result = {
              body: {
                exams: initialExams
              }
            }
            const currentExams = [{nr: 1, __key: 1, value: 'new'}]
            return expectSaga(sagas.resetDeletedExams)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/load', {
                    method: 'POST',
                    body: selection
                  }),
                  result
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), currentExams]
              ])
              .put(actions.setExams(currentExams))
              .run()
          })

          test('should reorder initial exams to go before newly created exams', () => {
            const initialExams = [
              {key: 3, nr: 1},
              {key: 5, nr: 2}
            ]
            const result = {
              body: {
                exams: initialExams
              }
            }
            const currentExams = [{nr: 1}]
            const expectedExams = [
              {key: 3, nr: 1, __key: 1},
              {key: 5, nr: 2, __key: 2},
              {nr: 3, __key: 3}
            ]
            return expectSaga(sagas.resetDeletedExams)
              .provide([
                [
                  matchers.call(rest.requestSaga, 'qualification/actions/examEdit/load', {
                    method: 'POST',
                    body: selection
                  }),
                  result
                ],
                [select(sagas.selectionSelector), selection],
                [select(sagas.examsSelector), currentExams]
              ])
              .put(actions.setExams(expectedExams))
              .run()
          })
        })
      })
    })
  })
})
