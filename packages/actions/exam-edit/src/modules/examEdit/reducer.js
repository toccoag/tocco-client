import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  maxPoints: null,
  totalPoints: null,
  exams: [],
  type: null,
  isTemplate: null,
  saving: false
}

const addExam = state => ({
  ...state,
  exams: [
    ...state.exams,
    {
      nr: state.exams.length + 1,
      __key: state.exams.map(exam => exam.__key).reduce((max, key) => Math.max(max, key), 0) + 1
    }
  ]
})

const updateExam = (state, {payload}) => ({
  ...state,
  exams: state.exams.map(exam => {
    if (exam.nr === payload.nr) {
      return {
        ...exam,
        [payload.field]: payload.value
      }
    } else {
      return exam
    }
  })
})

const removeExam = (state, {payload}) => ({
  ...state,
  exams: state.exams.filter(exam => exam.nr !== payload.exam.nr).map((exam, index) => ({...exam, nr: index + 1}))
})

const ACTION_HANDLERS = {
  [actions.SET_MAX_POINTS]: reducerUtil.singleTransferReducer('maxPoints'),
  [actions.SET_EXAMS]: reducerUtil.singleTransferReducer('exams'),
  [actions.SET_TYPE]: reducerUtil.singleTransferReducer('type'),
  [actions.SET_IS_TEMPLATE]: reducerUtil.singleTransferReducer('isTemplate'),
  [actions.TOGGLE_SAVING]: reducerUtil.toggleReducer('saving'),
  [actions.ADD_EXAM]: addExam,
  [actions.UPDATE_EXAM]: updateExam,
  [actions.REMOVE_EXAM]: removeExam
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
