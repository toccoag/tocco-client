export const LOAD_DATA = 'examEdit/LOAD_DATA'
export const SAVE_DATA = 'examEdit/SAVE_DATA'
export const TOGGLE_SAVING = 'examEdit/TOGGLE_SAVING'
export const SET_MAX_POINTS = 'examEdit/SET_MAX_POINTS'
export const SET_EXAMS = 'examEdit/SET_EXAMS'
export const SET_TYPE = 'examEdit/SET_TYPE'
export const SET_IS_TEMPLATE = 'examEdit/SET_IS_TEMPLATE'
export const ADD_EXAM = 'examEdit/ADD_EXAM'
export const UPDATE_EXAM = 'examEdit/UPDATE_EXAM'
export const REMOVE_EXAM = 'examEdit/REMOVE_EXAM'

export const loadData = () => ({
  type: LOAD_DATA
})

export const saveData = () => ({
  type: SAVE_DATA
})

export const toggleSaving = () => ({
  type: TOGGLE_SAVING
})

export const setMaxPoints = maxPoints => ({
  type: SET_MAX_POINTS,
  payload: {maxPoints}
})

export const setExams = exams => ({
  type: SET_EXAMS,
  payload: {exams}
})

export const setType = type => ({
  type: SET_TYPE,
  payload: {type}
})

export const setIsTemplate = isTemplate => ({
  type: SET_IS_TEMPLATE,
  payload: {isTemplate}
})

export const addExam = () => ({
  type: ADD_EXAM
})

export const updateExam = (nr, field, value) => ({
  type: UPDATE_EXAM,
  payload: {nr, field, value}
})

export const removeExam = exam => ({
  type: REMOVE_EXAM,
  payload: {exam}
})
