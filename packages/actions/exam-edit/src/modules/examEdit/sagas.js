import _find from 'lodash/find'
import _omit from 'lodash/omit'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'

export const examsSelector = state => state.examEdit.exams
export const selectionSelector = state => state.input.selection

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_DATA, loadData), takeLatest(actions.SAVE_DATA, saveData)])
}

export function* loadData() {
  const selection = yield select(selectionSelector)
  const {
    body: {maxPoints, exams, type, isTemplate}
  } = yield call(rest.requestSaga, 'qualification/actions/examEdit/load', {method: 'POST', body: selection})
  yield put(actions.setMaxPoints(maxPoints))
  yield put(actions.setExams(exams.map(exam => ({...exam, __key: exam.nr}))))
  yield put(actions.setIsTemplate(isTemplate))
  yield put(actions.setType(type))
}

export function* saveData() {
  yield put(actions.toggleSaving())
  const selection = yield select(selectionSelector)
  const exams = (yield select(examsSelector)).map(exam => _omit(exam, ['__key']))
  const response = yield call(rest.requestSaga, 'qualification/actions/examEdit/save', {
    method: 'POST',
    body: {selection, exams},
    returnCancelledClientQuestion: true
  })
  if (response.ok) {
    yield put(
      externalEvents.fireExternalEvent('onSuccess', {
        message: 'client.actions.exam-edit.success'
      })
    )
  } else if (response.status === 412) {
    yield call(resetDeletedExams)
    yield put(actions.toggleSaving())
  } else {
    yield put(externalEvents.fireExternalEvent('onError'))
    yield put(actions.toggleSaving())
  }
}

export function* resetDeletedExams() {
  const currentExams = yield select(examsSelector)
  const selection = yield select(selectionSelector)
  const {
    body: {exams: originalExams}
  } = yield call(rest.requestSaga, 'qualification/actions/examEdit/load', {method: 'POST', body: selection})
  const exams = [
    ...originalExams.map(originalExam => {
      const currentExam = _find(currentExams, ['key', originalExam.key])
      if (currentExam) {
        return currentExam
      } else {
        return originalExam
      }
    }),
    ...currentExams.filter(currentExam => !currentExam.key)
  ].map((exam, index) => ({...exam, nr: index + 1, __key: index + 1}))
  yield put(actions.setExams(exams))
}
