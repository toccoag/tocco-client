import {addExam, updateExam, removeExam} from './actions'
import reducer from './reducer'

describe('exam-edit', () => {
  describe('modules', () => {
    describe('examEdit', () => {
      describe('reducer', () => {
        describe('addExam', () => {
          test('should create first exam', () => {
            const initialState = {
              exams: []
            }
            const {exams} = reducer(initialState, addExam())
            expect(exams).to.eql([{nr: 1, __key: 1}])
          })
          test('should increment __key based on existing values', () => {
            const initialState = {
              exams: [
                {nr: 1, __key: 11},
                {nr: 2, __key: 12}
              ]
            }
            const {exams} = reducer(initialState, addExam())
            expect(exams).to.deep.include({nr: 3, __key: 13})
          })
        })

        describe('updateExam', () => {
          test('should set field of single exam to value', () => {
            const initialState = {
              exams: [{nr: 1}, {nr: 3}, {nr: 5}]
            }
            const {exams} = reducer(initialState, updateExam(3, 'field', 'value'))
            expect(exams).to.eql([{nr: 1}, {nr: 3, field: 'value'}, {nr: 5}])
          })
        })

        describe('removeExam', () => {
          test('should remove exam', () => {
            const initialState = {
              exams: [{nr: 3}]
            }
            const {exams} = reducer(initialState, removeExam({nr: 3}))
            expect(exams).to.be.empty
          })
          test('should reset numbers of remaining exams', () => {
            const initialState = {
              exams: [
                {nr: 3, field: 'first'},
                {nr: 5, field: 'second'},
                {nr: 7, field: 'third'}
              ]
            }
            const {exams} = reducer(initialState, removeExam({nr: 5}))
            expect(exams).to.eql([
              {nr: 1, field: 'first'},
              {nr: 2, field: 'third'}
            ])
          })
        })
      })
    })
  })
})
