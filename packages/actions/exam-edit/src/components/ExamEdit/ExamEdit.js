import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import {LoadMask, Button, StyledStickyButtonWrapper} from 'tocco-ui'

import {isPointsSumType} from '../../utils/inputTypes'
import ExamEditInfo from '../ExamEditInfo'
import ExamEditTable from '../ExamEditTable'

const ExamEdit = ({type, pointsValid, saving, loadData, saveData}) => {
  useEffect(() => {
    loadData()
  }, [loadData])
  return (
    <LoadMask required={[type]}>
      <ExamEditInfo />
      <ExamEditTable />
      <StyledStickyButtonWrapper>
        <Button
          onClick={saveData}
          look="raised"
          ink="primary"
          disabled={isPointsSumType(type) && !pointsValid}
          pending={saving}
        >
          <FormattedMessage id="client.actions.exam-edit.save" />
        </Button>
      </StyledStickyButtonWrapper>
    </LoadMask>
  )
}

ExamEdit.propTypes = {
  type: PropTypes.string,
  pointsValid: PropTypes.bool,
  saving: PropTypes.bool,
  loadData: PropTypes.func.isRequired,
  saveData: PropTypes.func.isRequired
}

export default ExamEdit
