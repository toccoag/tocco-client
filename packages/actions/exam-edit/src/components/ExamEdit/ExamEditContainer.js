import {connect} from 'react-redux'

import {loadData, saveData} from '../../modules/examEdit/actions'
import {arePointsValid} from '../../utils/stateUtils'

import ExamEdit from './ExamEdit'

const mapActionCreators = {
  loadData,
  saveData
}

const mapStateToProps = state => ({
  type: state.examEdit.type,
  pointsValid: arePointsValid(state),
  saving: state.examEdit.saving
})

export default connect(mapStateToProps, mapActionCreators)(ExamEdit)
