import {configureStore} from '@reduxjs/toolkit'
import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import ExamEdit from './ExamEdit'

describe('exam-edit', () => {
  describe('components', () => {
    describe('ExamEdit', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      test('renders sub components', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'POINTS',
              exams: []
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEdit loadData={() => {}} saveData={() => {}} type={'GRADES'} />, {store})

        expect(screen.findByRole('table')).to.exist
        expect(screen.queryByRole('heading', {name: 'client.actions.exam-edit.infoTitle'})).to.exist
        expect(screen.queryByRole('button', {name: 'client.actions.exam-edit.save'})).to.exist
      })

      test('save should be called', () => {
        const saveFn = sinon.spy()
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'GRADES',
              exams: []
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEdit loadData={() => {}} saveData={saveFn} type={'GRADES'} />, {store})

        const saveButton = screen.queryByRole('button', {name: 'client.actions.exam-edit.save'})
        expect(saveButton).to.exist
        fireEvent.click(saveButton)
        expect(saveFn).to.have.been.calledOnce
      })

      test('save should be disabled for wrong points', () => {
        const saveFn = sinon.spy()
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'POINTS',
              exams: []
            }
          })
        })
        testingLibrary.renderWithStore(
          <ExamEdit loadData={() => {}} saveData={saveFn} type={'POINTS'} pointsValid={false} />,
          {store}
        )

        const saveButton = screen.queryByRole('button', {name: 'client.actions.exam-edit.save'})
        expect(saveButton).to.exist
        expect(saveButton.disabled).to.be.true
        fireEvent.click(saveButton)
        expect(saveFn).to.not.have.been.called
      })
    })
  })
})
