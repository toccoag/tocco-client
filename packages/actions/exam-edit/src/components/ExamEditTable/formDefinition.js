import {form} from 'tocco-app-extensions'

import inputTypes from '../../utils/inputTypes'

const createTypeColumns = (intl, type) => {
  switch (type) {
    case inputTypes.POINTS:
    case inputTypes.POINTS_THRESHOLD:
      return [
        form.createColumn({
          label: intl.formatMessage({id: 'client.actions.exam-edit.maxPoints'}),
          id: 'maxPoints',
          dataType: 'decimal',
          readonly: false,
          validation: form.createValidation(
            form.createDecimalDigitsValidation(3),
            form.createNumberRangeValidation(0, null)
          ),
          alignment: 'right',
          width: 100
        })
      ]
    case inputTypes.POINTS_AVERAGE:
    case inputTypes.POINTS_AVERAGE_THRESHOLD:
    case inputTypes.GRADES:
    case inputTypes.GRADES_DROP:
    case inputTypes.GRADES_MANDATORY:
    case inputTypes.GRADES_MAX:
      return [
        form.createColumn({
          label: intl.formatMessage({id: 'client.actions.exam-edit.weight'}),
          id: 'weight',
          dataType: 'decimal',
          readonly: false,
          validation: form.createValidation(
            form.createDecimalDigitsValidation(2),
            form.createNumberRangeValidation(0, null)
          ),
          alignment: 'right',
          shrinkToContent: true
        })
      ]
    default:
      return []
  }
}

export const createTableFormDefinition = (intl, isTemplate, type) => {
  const msg = id => intl.formatMessage({id})

  const formDefinition = form.createSimpleForm({
    children: [
      form.createTable({
        children: [
          form.createColumn({
            label: msg('client.actions.exam-edit.nr'),
            id: 'nr',
            dataType: 'integer',
            shrinkToContent: true
          }),
          form.createColumn({
            label: msg('client.actions.exam-edit.label'),
            id: 'label',
            dataType: 'string',
            readonly: false
          }),
          ...(isTemplate
            ? []
            : [
                form.createColumn({
                  label: msg('client.actions.exam-edit.date'),
                  id: 'date',
                  dataType: 'date',
                  width: 200,
                  readonly: false
                })
              ]),
          ...createTypeColumns(intl, type),
          form.createColumn({
            label: '',
            id: 'remove',
            dataType: 'action',
            children: [
              form.createButtonCell({
                id: 'remove',
                icon: 'trash',
                label: msg('client.actions.exam-edit.removeExam')
              })
            ],
            shrinkToContent: true
          })
        ]
      })
    ]
  })

  return formDefinition
}
