import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {addExam, removeExam, updateExam} from '../../modules/examEdit/actions'

import ExamEditTable from './ExamEditTable'

const mapActionCreators = {
  addExam,
  removeExam,
  updateExam
}

const mapStateToProps = state => ({
  type: state.examEdit.type,
  isTemplate: state.examEdit.isTemplate,
  exams: state.examEdit.exams,
  saving: state.examEdit.saving
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ExamEditTable))
