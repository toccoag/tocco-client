import PropTypes from 'prop-types'
import {useMemo} from 'react'
import SimpleTableFormApp from 'tocco-simple-table-form/src/main'
import {Button} from 'tocco-ui'
import {keyboard} from 'tocco-util'

import {examPropType} from '../../utils/propTypes'

import {createTableFormDefinition} from './formDefinition'
import {StyledButtonWrapper, StyledExamsTableWrapper} from './StyledComponents'

const fieldsToFocus = ['label', 'date-datepicker', 'weight', 'maxPoints']

const ExamEditTable = ({type, isTemplate, exams, saving, addExam, removeExam, updateExam, intl}) => {
  const tableFormDefinition = useMemo(() => createTableFormDefinition(intl, isTemplate, type), [intl, isTemplate, type])

  const customButtons = useMemo(
    () => ({
      remove: rowData => {
        removeExam(rowData)
      }
    }),
    [removeExam]
  )

  return (
    <>
      <StyledButtonWrapper>
        <Button
          onClick={addExam}
          look="raised"
          ink="base"
          icon="plus"
          disabled={saving}
          label={intl.formatMessage({id: 'client.actions.exam-edit.addExam'})}
        />
      </StyledButtonWrapper>
      <StyledExamsTableWrapper onKeyDown={keyboard.navigateTable(fieldsToFocus)}>
        <SimpleTableFormApp
          disabled={saving}
          formDefinition={tableFormDefinition}
          data={exams}
          customButtons={customButtons}
          onChange={({key, field, value}) => {
            updateExam(key, field, value)
          }}
        />
      </StyledExamsTableWrapper>
    </>
  )
}

ExamEditTable.propTypes = {
  type: PropTypes.string.isRequired,
  isTemplate: PropTypes.bool,
  exams: PropTypes.arrayOf(examPropType).isRequired,
  saving: PropTypes.bool,
  addExam: PropTypes.func.isRequired,
  removeExam: PropTypes.func.isRequired,
  updateExam: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default ExamEditTable
