import styled from 'styled-components'
import {scale, themeSelector, declareFocus, StyledLoadMask} from 'tocco-ui'

export const StyledExamsTableWrapper = styled.div`
  width: 100%;
  height: 400px;
  overflow: auto;

  td {
    display: flex;
    align-items: center;
  }

  /* since this wrapper could contain nested Load Masks the height must be set to 100% to prevent collapsing */
  ${StyledLoadMask} {
    height: 100%;
  }
`

export const StyledButtonWrapper = styled.div`
  display: flex;
`

export const StyledFieldWrapper = styled.span`
  border: 1px solid ${themeSelector.color('border')};
  width: 100%;
  ${declareFocus}

  &&&&& {
    input,
    textarea {
      padding-left: ${scale.space(-1)};
      padding-right: ${scale.space(-1)};

      &:not([disabled]):hover {
        cursor: pointer;
      }
    }

    .react-datepicker-wrapper {
      margin-right: ${scale.space(-0.5)};
    }

    input.react-datepicker__input {
      padding-left: ${scale.space(-1)};
      padding-right: 0;
    }
  }

  button {
    background-color: transparent;
  }
`
