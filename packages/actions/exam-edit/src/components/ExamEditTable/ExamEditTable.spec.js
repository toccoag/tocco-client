import {configureStore} from '@reduxjs/toolkit'
import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary, appUtils} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'
import examEditReducer from '../../modules/examEdit/reducer'

import ExamEditTable from './ExamEditTableContainer'

describe('exam-edit', () => {
  describe('components', () => {
    describe('ExamEditTable', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
        appUtils.populateInitialWindowState()
      })
      afterEach(() => {
        appUtils.restoreInitialWindowState()
      })

      test('should render general table components', async () => {
        const store = configureStore({
          reducer: {
            examEdit: examEditReducer
          },
          preloadedState: {
            examEdit: {
              type: 'GRADES',
              isTemplate: true,
              exams: [{nr: 13, label: 'text label'}]
            }
          }
        })
        testingLibrary.renderWithStore(<ExamEditTable />, {store})

        await screen.findAllByTestId('icon')
        expect(screen.queryByRole('table')).to.exist
        const addButton = screen.queryByRole('button', {name: 'client.actions.exam-edit.addExam'})
        expect(addButton).to.exist
        expect(screen.queryByText('client.actions.exam-edit.nr')).to.exist
        expect(screen.queryByRole('cell', {name: '13'})).to.exist
        expect(screen.queryByText('client.actions.exam-edit.label')).to.exist
        expect(screen.queryByRole('cell', {name: 'text label'})).to.exist
        expect(screen.queryByText('client.actions.exam-edit.label')).to.exist
        const removeButton = screen.queryByTestId('icon-trash')
        expect(removeButton).to.exist

        fireEvent.click(removeButton)
        expect(screen.findByText('client.component.table.noData')).to.exist

        fireEvent.click(addButton)
        fireEvent.click(addButton)
        expect(screen.findByRole('cell', {name: '1'})).to.exist
        expect(screen.findByRole('cell', {name: '2'})).to.exist
      })

      test('should render template table components', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'GRADES',
              isTemplate: false,
              exams: [{nr: 1, date: '2023-01-01'}]
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEditTable />, {store})

        expect(screen.queryByText('client.actions.exam-edit.date')).to.exist
        expect(screen.queryByRole('cell', {name: '01/01/2023'})).to.exist
      })

      test('should render points table components', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'POINTS',
              exams: [{nr: 1, maxPoints: 10}]
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEditTable />, {store})

        expect(screen.queryByText('client.actions.exam-edit.maxPoints')).to.exist
        expect(screen.queryByRole('cell', {name: '10'})).to.exist
      })

      test('should render grades table components', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'GRADES',
              exams: [{nr: 1, weight: 10}]
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEditTable />, {store})

        expect(screen.queryByText('client.actions.exam-edit.weight')).to.exist
        expect(screen.queryByRole('cell', {name: '10'})).to.exist
      })
    })
  })
})
