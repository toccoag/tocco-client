import styled from 'styled-components'
import {themeSelector, StyledSpan} from 'tocco-ui'

export const StyledErrorMessage = styled(StyledSpan)`
  && {
    color: ${themeSelector.color('signal.danger')};
  }
`
