import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {field} from 'tocco-app-extensions'
import {Typography, StatedValue, Layout, Panel} from 'tocco-ui'

import {isPointsSumType} from '../../utils/inputTypes'

import {StyledErrorMessage} from './StyledComponents'

const FieldComponent = ({type, value}) => {
  const Field = field.factory('readOnly', type)
  return <Field formField={{dataType: type}} value={value} mappingType="readOnly" />
}

FieldComponent.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}

const ValueComponent = ({id, label, type, value}) => {
  return (
    <StatedValue key={id} label={label} immutable={true} hasValue={true} labelPosition="inside">
      <FieldComponent type={type} value={value} />
    </StatedValue>
  )
}

ValueComponent.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}

const ExamEditInfo = ({type, maxPoints, totalPoints, pointsValid, intl}) => {
  if (!isPointsSumType(type)) {
    return null
  }

  return (
    <Layout.Container>
      <Layout.Box>
        <Panel.Wrapper>
          <Panel.Header>
            <Typography.H4>
              <FormattedMessage id="client.actions.exam-edit.infoTitle" />
            </Typography.H4>
          </Panel.Header>
          <Panel.Body>
            <ValueComponent
              id="max"
              label={intl.formatMessage({id: 'client.actions.exam-edit.maxPoints'})}
              type="decimal"
              value={maxPoints}
            />
            <ValueComponent
              id="total"
              label={intl.formatMessage({id: 'client.actions.exam-edit.totalPoints'})}
              type="decimal"
              value={totalPoints}
            />
            {!pointsValid ? (
              <StyledErrorMessage>
                <FormattedMessage id="client.actions.exam-edit.mismatchingPoints" />
              </StyledErrorMessage>
            ) : null}
          </Panel.Body>
        </Panel.Wrapper>
      </Layout.Box>
    </Layout.Container>
  )
}

ExamEditInfo.propTypes = {
  type: PropTypes.string,
  maxPoints: PropTypes.number,
  totalPoints: PropTypes.number,
  pointsValid: PropTypes.bool,
  intl: PropTypes.object.isRequired
}

export default ExamEditInfo
