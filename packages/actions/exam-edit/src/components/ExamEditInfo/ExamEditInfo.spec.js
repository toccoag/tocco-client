import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ExamEditInfo from './ExamEditInfoContainer'

describe('exam-edit', () => {
  describe('components', () => {
    describe('ExamEditInfo', () => {
      test('should render nothing for grades', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'GRADES',
              exams: []
            }
          })
        })
        const {container} = testingLibrary.renderWithStore(<ExamEditInfo />, {store})

        jestExpect(container).toBeEmptyDOMElement()
      })

      test('should render points info', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'POINTS',
              maxPoints: 10,
              exams: [{maxPoints: 10}]
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEditInfo />, {store})

        expect(screen.findByRole('heading', {name: 'client.actions.exam-edit.infoTitle'})).to.exist
        const maxPointsLabel = screen.queryByTitle('client.actions.exam-edit.maxPoints')
        expect(maxPointsLabel).to.exist
        expect(maxPointsLabel.parentElement.textContent).to.eq('client.actions.exam-edit.maxPoints10.00')
        const totalPointsLabel = screen.queryByTitle('client.actions.exam-edit.totalPoints')
        expect(totalPointsLabel).to.exist
        expect(totalPointsLabel.parentElement.textContent).to.eq('client.actions.exam-edit.totalPoints10.00')
      })

      test('should render warning when points do not match', () => {
        const store = configureStore({
          reducer: () => ({
            examEdit: {
              type: 'POINTS',
              maxPoints: 10,
              exams: [{maxPoints: 15}]
            }
          })
        })
        testingLibrary.renderWithStore(<ExamEditInfo />, {store})

        expect(screen.findByText('client.actions.exam-edit.mismatchingPoints')).to.exist
      })
    })
  })
})
