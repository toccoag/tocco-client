import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {getTotalPoints, arePointsValid} from '../../utils/stateUtils'

import ExamEditInfo from './ExamEditInfo'

const mapStateToProps = state => ({
  type: state.examEdit.type,
  maxPoints: state.examEdit.maxPoints,
  totalPoints: getTotalPoints(state),
  pointsValid: arePointsValid(state)
})

export default connect(mapStateToProps)(injectIntl(ExamEditInfo))
