import {appFactory, selection, externalEvents, actionEmitter, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import ExamEdit from './components/ExamEdit'
import reducers, {sagas} from './modules/reducers'

const packageName = 'exam-edit'

const EXTERNAL_EVENTS = ['onSuccess', 'onError', 'onCancel', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <ExamEdit />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const ExamEditApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

ExamEditApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  selection: selection.propType.isRequired
}

export default ExamEditApp
