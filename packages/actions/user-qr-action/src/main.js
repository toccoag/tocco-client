import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import UserQrCode from './containers/UserQrCodeContainer'
import reducers, {sagas} from './modules/reducers'

const packageName = 'user-qr-action'

const initApp = (id, input, events, publicPath) => {
  const content = <UserQrCode />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const UserQrActionApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

UserQrActionApp.propTypes = {
  /**
   * Selection of a single `User` (only type `ID` is supported and exacte one key is required)
   */
  selection: PropTypes.object
}

export default UserQrActionApp
