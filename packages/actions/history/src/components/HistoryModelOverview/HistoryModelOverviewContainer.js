import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initializeModelOverview} from '../../modules/historyModelOverview/actions'
import {setSelection} from '../../modules/historyTable/actions'

import HistoryModelOverview from './HistoryModelOverview'

const mapActionCreators = {
  initializeModelOverview,
  setSelection
}

const mapStateToProps = state => ({
  modelName: state.history.modelName,
  tableData: state.historyTable.tableData,
  searchFormDefinition: state.historyModelOverview.searchForm,
  listFormDefinition: state.historyModelOverview.listForm
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(HistoryModelOverview))
