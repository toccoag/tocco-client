import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import {LoadMask} from 'tocco-ui'

import HistoryLayout from '../HistoryLayout'
import HistorySearch from '../HistorySearch'
import HistoryTable from '../HistoryTable'

const HistoryModelOverview = ({
  initializeModelOverview,
  modelName,
  tableData,
  searchFormDefinition,
  listFormDefinition,
  setSelection
}) => {
  const navigate = useNavigate()

  useEffect(() => {
    initializeModelOverview()
    setSelection([]) // clear selection and does not keep selection from entity overview
  }, [initializeModelOverview, setSelection])

  const onRowClick = key => {
    const rowData = tableData.find(r => r.__key === key)
    navigate(`/history/${modelName}/${rowData.entity_key}`)
    setSelection([key]) // keep selection for entity overview
  }

  const DetailComponent = (
    <HistoryTable listFormDefinition={listFormDefinition} selectionStyle="none" onRowClick={onRowClick} />
  )

  return (
    <LoadMask required={[searchFormDefinition, listFormDefinition]}>
      <HistoryLayout
        SidepanelComponent={<HistorySearch searchFormDefinition={searchFormDefinition} />}
        DetailComponent={DetailComponent}
      />
    </LoadMask>
  )
}

HistoryModelOverview.propTypes = {
  initializeModelOverview: PropTypes.func.isRequired,
  modelName: PropTypes.string,
  tableData: PropTypes.arrayOf(
    PropTypes.shape({
      __key: PropTypes.number.isRequired,
      entity_key: PropTypes.string.isRequired
    })
  ),
  searchFormDefinition: PropTypes.object,
  listFormDefinition: PropTypes.object,
  setSelection: PropTypes.func
}

export default HistoryModelOverview
