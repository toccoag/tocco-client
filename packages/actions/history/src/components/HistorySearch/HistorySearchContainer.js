import {connect} from 'react-redux'

import {setCurrentPage, setSearchFields} from '../../modules/historyTable/actions'

import HistorySearch from './HistorySearch'

const mapActionCreators = {
  setSearchFields,
  setCurrentPage
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapActionCreators)(HistorySearch)
