import _delay from 'lodash/delay'
import PropTypes from 'prop-types'
import {useCallback} from 'react'
import SimpleFormApp from 'tocco-simple-form/src/main'

import {Box} from './StyledComponents'

const HistorySearch = ({searchFormDefinition, setSearchFields, setCurrentPage}) => {
  const handleChange = useCallback(
    ({values, event}) => {
      const hasValues = Object.keys(values).length > 0

      /**
       * set search field on init immediately to trigger data load
       * and empty search form
       *  - table and search form is reused on model and entity overview
       */
      if (event === 'init') {
        setCurrentPage(1)
        setSearchFields(hasValues ? values : {})
      } else {
        _delay(() => {
          setCurrentPage(1)
          setSearchFields(hasValues ? values : {})
        }, 500)
      }
    },
    [setSearchFields, setCurrentPage]
  )

  if (!searchFormDefinition.children) {
    return null
  }

  return (
    <Box>
      <SimpleFormApp
        form={searchFormDefinition}
        noButtons
        validate={false}
        mappingType="search"
        mode="search"
        onChange={handleChange}
        onSubmit={handleChange}
        labelPosition="inside"
      />
    </Box>
  )
}

HistorySearch.propTypes = {
  searchFormDefinition: PropTypes.object.isRequired,
  setSearchFields: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired
}

export default HistorySearch
