import PropTypes from 'prop-types'
import {selectionStylePropType} from 'tocco-entity-list/src/main'
import {Table} from 'tocco-ui'

import {getColumnDefinition, getTable} from '../../util/form'

import {CellRenderer} from './CellRenderer'
import {StyledTableWrapper} from './StyledComponents'

const HistoryTable = ({
  tableData,
  listFormDefinition,
  totalCount,
  currentPage,
  recordsPerPage,
  loadNewPage,
  selection,
  onSelectChange,
  selectionStyle,
  loading,
  onRowClick
}) => {
  // data does not have to be loaded here since it already gets loaded through HistorySearch

  if (!listFormDefinition.children) {
    return null
  }
  const table = getTable(listFormDefinition)

  const columnsDefinitions = getColumnDefinition(
    {
      table
    },
    CellRenderer
  )

  return (
    <StyledTableWrapper>
      <Table
        columns={columnsDefinitions}
        data={tableData}
        scrollBehaviour="inline"
        paginationInfo={{
          currentPage,
          recordsPerPage,
          totalCount
        }}
        onPageChange={loadNewPage}
        selectionStyle={selectionStyle}
        selection={selection}
        onSelectionChange={onSelectChange}
        hasSelectionHeader={false}
        onRowClick={onRowClick}
        clickable={Boolean(onRowClick)}
        dataLoadingInProgress={loading}
      />
    </StyledTableWrapper>
  )
}

HistoryTable.propTypes = {
  listFormDefinition: PropTypes.object.isRequired,
  tableData: PropTypes.array,
  totalCount: PropTypes.number,
  currentPage: PropTypes.number.isRequired,
  recordsPerPage: PropTypes.number.isRequired,
  loadNewPage: PropTypes.func.isRequired,
  selection: PropTypes.array.isRequired,
  onSelectChange: PropTypes.func.isRequired,
  selectionStyle: selectionStylePropType,
  loading: PropTypes.bool,
  onRowClick: PropTypes.func
}

export default HistoryTable
