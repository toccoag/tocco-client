import {connect} from 'react-redux'

import {loadNewPage, onSelectChange} from '../../modules/historyTable/actions'

import HistoryTable from './HistoryTable'

const mapActionCreators = {
  loadNewPage,
  onSelectChange
}

const mapStateToProps = state => ({
  loading: state.historyTable.loading,
  tableData: state.historyTable.tableData,
  totalCount: state.historyTable.totalCount,
  currentPage: state.historyTable.currentPage,
  recordsPerPage: state.historyTable.recordsPerPage,
  selection: state.historyTable.selection
})

export default connect(mapStateToProps, mapActionCreators)(HistoryTable)
