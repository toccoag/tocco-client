import styled from 'styled-components'
import {StyledSidepanelMainContent} from 'tocco-ui'

export const StyledPaneWrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`

export const StyledListView = styled.div`
  display: grid;
  grid-template-rows: [action-start] auto [table-start] minmax(0, 1fr);
  height: 100%;
  width: 100%;
`

export const StyledListWrapper = styled.div`
  grid-row-start: table-start;
`
export const StyledSidepanelContent = styled(StyledSidepanelMainContent)`
  overflow-x: auto;
`
