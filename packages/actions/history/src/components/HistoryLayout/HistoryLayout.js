import PropTypes from 'prop-types'
import {useState} from 'react'
import {Sidepanel, SidepanelContainer, SidepanelHeader} from 'tocco-ui'

import {StyledPaneWrapper, StyledListWrapper, StyledListView, StyledSidepanelContent} from './StyledComponents'

const HistoryLayout = ({SidepanelComponent, DetailComponent}) => {
  const [isCollapsed, setIsCollapsed] = useState(false)

  return (
    <StyledPaneWrapper>
      <SidepanelContainer
        sidepanelPosition="left"
        sidepanelCollapsed={isCollapsed}
        setSidepanelCollapsed={setIsCollapsed}
        scrollBehaviour="inline"
      >
        <Sidepanel>
          <SidepanelHeader />
          {SidepanelComponent}
        </Sidepanel>

        <StyledSidepanelContent>
          <StyledListView>
            <StyledListWrapper>{DetailComponent}</StyledListWrapper>
          </StyledListView>
        </StyledSidepanelContent>
      </SidepanelContainer>
    </StyledPaneWrapper>
  )
}

HistoryLayout.propTypes = {
  SidepanelComponent: PropTypes.object,
  DetailComponent: PropTypes.object
}

export default HistoryLayout
