import PropTypes from 'prop-types'
import {LoadMask} from 'tocco-ui'

import DiffViewer from './DiffViewer'
import {getDiffTitle} from './utils'

const HistoryEntityDetail = ({selection, entries}) => {
  const entry = entries[selection[0]]

  return (
    <LoadMask required={[entry]}>
      <DiffViewer
        oldValue={entry?.xmlContent}
        newValue={entry?.xmlContent}
        splitView={false}
        showDiffOnly={false}
        hideLineNumbers={true}
        leftTitle={getDiffTitle(entry)}
      />
    </LoadMask>
  )
}

HistoryEntityDetail.propTypes = {
  selection: PropTypes.arrayOf(PropTypes.number).isRequired,
  entries: PropTypes.objectOf(
    PropTypes.shape({
      xmlContent: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      commitId: PropTypes.string.isRequired
    })
  ).isRequired
}

export default HistoryEntityDetail
