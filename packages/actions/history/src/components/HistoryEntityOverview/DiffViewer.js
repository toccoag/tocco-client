import PropTypes from 'prop-types'
import ReactDiffViewer from 'react-diff-viewer-continued'
import {withTheme} from 'styled-components'

const DiffViewer = ({theme, ...props}) => (
  <ReactDiffViewer
    useDarkTheme={theme.diffViewerDarkTheme}
    styles={{
      variables: {
        light: theme.diffViewer,
        dark: theme.diffViewer
      },
      diffContainer: {
        position: 'absolute'
      },
      codeFold: {
        color: theme.colors.text
      },
      lineNumber: {
        color: theme.colors.text
      }
    }}
    {...props}
  />
)

DiffViewer.propTypes = {
  theme: PropTypes.shape({
    colors: PropTypes.object,
    diffViewerDarkTheme: PropTypes.bool,
    diffViewer: PropTypes.object
  })
}

export default withTheme(DiffViewer)
