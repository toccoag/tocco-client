import {FormattedValue} from 'tocco-ui'

import {StyledTitleWrapper, StyledTitleText} from './StyledComponents'

export const getDiffTitle = entry => (
  <>
    <StyledTitleWrapper>
      <FormattedValue value={entry?.created} type="datetime" />
      <StyledTitleText>({entry?.commitId})</StyledTitleText>
      <br />
      <StyledTitleText>
        {entry?.username}@{entry?.ipAddress}
      </StyledTitleText>
    </StyledTitleWrapper>
  </>
)
