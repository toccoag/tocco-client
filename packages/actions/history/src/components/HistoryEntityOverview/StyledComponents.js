import styled from 'styled-components'
import {scale, StretchingTableContainer, StyledSpan, theme} from 'tocco-ui'

export const StyledSidepanelSearchWrapper = styled.div`
  display: grid;
  position: relative;
  grid-template-rows: auto 1fr;
  height: calc(100% - 43px);
`

export const StyledSearchFormWrapper = styled.div``

export const StyledTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  ${StretchingTableContainer} {
    overflow-x: hidden;
  }
`

export const StyledContainer = styled.div`
  background-color: ${theme.color('paper')};
  position: relative;
`

export const StyledHintContainer = styled.div`
  background-color: ${theme.color('paper')};
  height: 100%;
  padding: ${scale.space(1)};
  text-align: center;
`

export const StyledTitleWrapper = styled.span`
  text-align: center;
  display: block;
`

export const StyledTitleText = styled(StyledSpan)`
  && {
    margin-left: ${scale.space(-1.37)};
  }
`
