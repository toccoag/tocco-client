import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initializeEntityOverview} from '../../modules/historyEntityOverview/actions'

import HistoryEntityOverview from './HistoryEntityOverview'

const mapActionCreators = {
  initializeEntityOverview
}

const mapStateToProps = state => ({
  searchFormDefinition: state.historyEntityOverview.searchForm,
  listFormDefinition: state.historyEntityOverview.listForm,
  selection: state.historyTable.selection,
  entries: state.historyEntityOverview.entries,
  maxAge: state.historyEntityOverview.maxAge
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(HistoryEntityOverview))
