import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Typography} from 'tocco-ui'

import {StyledHintContainer} from './StyledComponents'

const HistoryEntityHint = ({maxAge}) => (
  <StyledHintContainer>
    <Typography.P>
      <FormattedMessage id="client.actions.history.overviewEmpty" />
    </Typography.P>
    <Typography.P>
      <FormattedMessage id="client.actions.history.maxAge" values={{months: maxAge}} />
    </Typography.P>
  </StyledHintContainer>
)

HistoryEntityHint.propTypes = {
  maxAge: PropTypes.number.isRequired
}

export default HistoryEntityHint
