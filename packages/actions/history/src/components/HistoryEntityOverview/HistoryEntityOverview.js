import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'

import HistoryLayout from '../HistoryLayout'
import HistorySearch from '../HistorySearch'
import HistoryTable from '../HistoryTable'

import HistoryEntityDetail from './HistoryEntityDetail'
import HistoryEntityDiff from './HistoryEntityDiff'
import HistoryEntityHint from './HistoryEntityHint'
import {
  StyledTableWrapper,
  StyledContainer,
  StyledSidepanelSearchWrapper,
  StyledSearchFormWrapper
} from './StyledComponents'

const HistoryEntityOverview = ({
  initializeEntityOverview,
  searchFormDefinition,
  listFormDefinition,
  entries,
  selection,
  maxAge
}) => {
  useEffect(() => {
    initializeEntityOverview()
  }, [initializeEntityOverview])

  const SidepanelComponent = (
    <StyledSidepanelSearchWrapper>
      <StyledSearchFormWrapper>
        <HistorySearch searchFormDefinition={searchFormDefinition} />
      </StyledSearchFormWrapper>
      <StyledTableWrapper>
        <HistoryTable listFormDefinition={listFormDefinition} selectionStyle="multi" />
      </StyledTableWrapper>
    </StyledSidepanelSearchWrapper>
  )

  const DetailComponent = (
    <StyledContainer>
      {selection.length === 0 && <HistoryEntityHint maxAge={maxAge} />}
      {selection.length === 1 && <HistoryEntityDetail selection={selection} entries={entries} />}
      {selection.length === 2 && <HistoryEntityDiff selection={selection} entries={entries} />}
    </StyledContainer>
  )

  return (
    <LoadMask required={[searchFormDefinition, listFormDefinition]}>
      <HistoryLayout SidepanelComponent={SidepanelComponent} DetailComponent={DetailComponent} />
    </LoadMask>
  )
}

HistoryEntityOverview.propTypes = {
  initializeEntityOverview: PropTypes.func.isRequired,
  searchFormDefinition: PropTypes.object,
  listFormDefinition: PropTypes.object,
  selection: PropTypes.arrayOf(PropTypes.number).isRequired,
  entries: PropTypes.objectOf(
    PropTypes.shape({
      xmlContent: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      commitId: PropTypes.string.isRequired
    })
  ).isRequired,
  maxAge: PropTypes.number.isRequired
}

export default HistoryEntityOverview
