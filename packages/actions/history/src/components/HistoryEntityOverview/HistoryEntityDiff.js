import PropTypes from 'prop-types'
import {DiffMethod} from 'react-diff-viewer-continued'
import {FormattedMessage} from 'react-intl'
import {LoadMask, Typography} from 'tocco-ui'

import DiffViewer from './DiffViewer'
import {getDiffTitle} from './utils'

const comparator = (a, b) => a - b

const cleanContent = input => input?.replaceAll(' changed="true"', '')

const HistoryEntityDiff = ({selection, entries}) => {
  const [leftIndex, rightIndex] = [...selection].sort(comparator) // ensure left entry is always the older one
  const leftEntry = entries[leftIndex]
  const rightEntry = entries[rightIndex]

  return (
    <LoadMask required={[leftEntry, rightEntry]}>
      <DiffViewer
        oldValue={cleanContent(leftEntry?.xmlContent)}
        newValue={cleanContent(rightEntry?.xmlContent)}
        leftTitle={getDiffTitle(leftEntry)}
        rightTitle={getDiffTitle(rightEntry)}
        compareMethod={DiffMethod.WORDS}
        codeFoldMessageRenderer={number => (
          <Typography.P>
            <FormattedMessage id={'client.actions.history.moreLines'} values={{number}} />
          </Typography.P>
        )}
      />
    </LoadMask>
  )
}

HistoryEntityDiff.propTypes = {
  selection: PropTypes.arrayOf(PropTypes.number).isRequired,
  entries: PropTypes.objectOf(
    PropTypes.shape({
      xmlContent: PropTypes.string.isRequired,
      created: PropTypes.string.isRequired,
      commitId: PropTypes.string.isRequired
    })
  ).isRequired
}

export default HistoryEntityDiff
