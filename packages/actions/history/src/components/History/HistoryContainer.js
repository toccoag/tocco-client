import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {updateHistory} from '../../modules/history/actions'

import History from './History'

const mapActionCreators = {
  updateHistory
}

const mapStateToProps = state => ({
  view: state.history.view,
  breadcrumbsInfo: state.history.breadcrumbsInfo
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(History))
