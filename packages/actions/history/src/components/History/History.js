import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useLocation} from 'react-router-dom'
import {Breadcrumbs} from 'tocco-ui'

import HistoryEntityOverview from '../HistoryEntityOverview'
import HistoryModelOverview from '../HistoryModelOverview'

const History = ({updateHistory, view, breadcrumbsInfo}) => {
  const location = useLocation()

  useEffect(() => {
    updateHistory(location.pathname)
  }, [updateHistory, location.pathname])

  return (
    <>
      <Breadcrumbs breadcrumbsInfo={breadcrumbsInfo} updateDocumentTitle showToccoHome />
      {view === 'entityOverview' ? <HistoryEntityOverview /> : <HistoryModelOverview />}
    </>
  )
}

History.propTypes = {
  updateHistory: PropTypes.func.isRequired,
  view: PropTypes.string,
  breadcrumbsInfo: PropTypes.array
}

export default History
