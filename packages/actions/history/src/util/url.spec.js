import {getModelOverviewUrl, getEntityOverviewUrl} from './url'

describe('history', () => {
  describe('util', () => {
    describe('url', () => {
      describe('getModelOverviewUrl', () => {
        test('getModelOverviewUrl', () => {
          const selection = {entityName: 'User'}
          expect(getModelOverviewUrl(selection)).to.eql('/history/User')
        })
      })

      describe('getEntityOverviewUrl', () => {
        test('get url', () => {
          const selection = {entityName: 'User', type: 'ID', ids: [1]}
          expect(getEntityOverviewUrl(selection)).to.eql('/history/User/1')
        })

        test('query selection', () => {
          const selection = {entityName: 'User', type: 'QUERY'}
          expect(() => getEntityOverviewUrl(selection)).to.throw('Only selection with type ID is suppored')
        })

        test('multiple items selected', () => {
          const selection = {entityName: 'User', type: 'ID', ids: [1, 2]}
          expect(() => getEntityOverviewUrl(selection)).to.throw('Selection must contain exact one item')
        })
      })
    })
  })
})
