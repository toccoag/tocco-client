import {addDays, parseISO} from 'date-fns'
import {form} from 'tocco-app-extensions'

export const getTable = formDefinition =>
  formDefinition.children.find(child => child.componentType === form.componentTypes.TABLE)

export const getColumnDefinition = ({table}, CellRenderer) =>
  table.children.map(column => ({
    id: column.id,
    label: column.label,
    dynamic: true,
    sorting: {
      sortable: false
    },
    shrinkToContent: column.shrinkToContent || false,
    sticky: column.sticky || false,
    children: column.children.filter(isDisplayableChild),
    resizable: !column.widthFixed,
    fixedPosition: true,
    alignment: 'left',
    CellRenderer
  }))

const isDisplayableChild = child => !child.hidden

// rest endpoint returns camelcase but in form field names have underscores
export const mapToFormNames = entry => ({
  __key: entry.pk,
  entity_key: entry.entityKey,
  operation: entry.operation,
  username: entry.username,
  ip_address: entry.ipAddress,
  date: entry.created
})

// map underscore field name to camelcase for rest endpoint
export const mapToBeanNames = (entityModel, searchFields) => ({
  username: searchFields.username,
  entityModel,
  entityKey: searchFields.entity_key,
  ipAddress: searchFields.ip_address,
  operation: searchFields.operation,
  dateFrom: getDate(searchFields, 'from'),
  dateTo: getDate(searchFields, 'to')
})

// if date fields is collapsed data structure is different
const getDate = (searchFields, field) => {
  if (searchFields?.date?.isRangeValue) {
    return searchFields.date[field]
  } else if (!searchFields?.date) {
    // date-fns does not support undefined as input, so directly return undefined
    return searchFields?.date
  } else if (field === 'to') {
    return addDays(searchFields?.date, 1).toISOString()
  }
  return parseISO(searchFields?.date).toISOString()
}
