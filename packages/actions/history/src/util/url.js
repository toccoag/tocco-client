export const getModelOverviewUrl = selection => `/history/${selection.entityName}`

export const getEntityOverviewUrl = selection => {
  if (selection.type !== 'ID') {
    throw new Error('Only selection with type ID is suppored')
  }

  if (selection.ids.length !== 1) {
    throw new Error('Selection must contain exact one item')
  }

  return `/history/${selection.entityName}/${selection.ids[0]}`
}
