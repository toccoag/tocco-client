import {getColumnDefinition, mapToFormNames, mapToBeanNames} from './form'

describe('history', () => {
  describe('util', () => {
    describe('form', () => {
      describe('getColumnDefinition', () => {
        it('should build column definitions', () => {
          const table = {
            children: [
              {
                id: 'first',
                label: 'first label',
                shrinkToContent: true,
                sticky: true,
                widthFixed: true,
                children: [{id: 'child', hidden: false}]
              },
              {
                id: 'sorting',
                label: 'sorting label',
                shrinkToContent: false,
                widthFixed: false,
                children: [{hidden: true}]
              },
              {id: 'no children', label: 'no children label', shrinkToContent: false, widthFixed: false, children: []}
            ]
          }
          const cellRenderer = {}
          const expectedColumnDefinitions = [
            {
              id: 'first',
              label: 'first label',
              dynamic: true,
              sorting: {sortable: false},
              shrinkToContent: true,
              sticky: true,
              children: [{id: 'child', hidden: false}],
              resizable: false,
              fixedPosition: true,
              alignment: 'left',
              CellRenderer: cellRenderer
            },
            {
              id: 'sorting',
              label: 'sorting label',
              dynamic: true,
              sorting: {sortable: false},
              shrinkToContent: false,
              sticky: false,
              children: [],
              resizable: true,
              fixedPosition: true,
              alignment: 'left',
              CellRenderer: cellRenderer
            },
            {
              id: 'no children',
              label: 'no children label',
              dynamic: true,
              sorting: {sortable: false},
              shrinkToContent: false,
              sticky: false,
              children: [],
              resizable: true,
              fixedPosition: true,
              alignment: 'left',
              CellRenderer: cellRenderer
            }
          ]
          const columnDefinitions = getColumnDefinition({table}, cellRenderer, true, 'sorting')
          expect(columnDefinitions).to.eql(expectedColumnDefinitions)
        })
      })

      describe('mapToFormNames', () => {
        it('mapToFormNames', () => {
          const entry = {
            pk: 1234,
            entityKey: '1',
            operation: 'UPDATED',
            username: 'admin@tocco.ch',
            ipAddress: '127.0.0.1',
            created: '2024-06-17T05:42:16.870Z'
          }
          const expected = {
            __key: 1234,
            entity_key: '1',
            operation: 'UPDATED',
            username: 'admin@tocco.ch',
            ip_address: '127.0.0.1',
            date: '2024-06-17T05:42:16.870Z'
          }
          expect(mapToFormNames(entry)).to.be.eql(expected)
        })
      })

      describe('mapToBeanNames', () => {
        const entityModel = 'User'

        it('no searchFields ', () => {
          const searchFields = {}

          const expected = {
            username: undefined,
            entityModel,
            entityKey: undefined,
            ipAddress: undefined,
            operation: undefined,
            dateFrom: undefined,
            dateTo: undefined
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })

        it('map text fields', () => {
          const searchFields = {
            entity_key: '1',
            operation: 'UPDATED',
            username: 'admin@tocco.ch',
            ip_address: '127.0.0.1'
          }

          const expected = {
            username: 'admin@tocco.ch',
            entityModel,
            entityKey: '1',
            ipAddress: '127.0.0.1',
            operation: 'UPDATED',
            dateFrom: undefined,
            dateTo: undefined
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })

        it('date not range, search full day', () => {
          const searchFields = {
            date: '2024-06-16T00:00:00.000Z'
          }

          const expected = {
            username: undefined,
            entityModel,
            entityKey: undefined,
            ipAddress: undefined,
            operation: undefined,
            dateFrom: '2024-06-16T00:00:00.000Z',
            dateTo: '2024-06-17T00:00:00.000Z'
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })

        it('date is range, both fields have a value', () => {
          const searchFields = {
            date: {
              from: '2024-06-16T00:00:00.000Z',
              to: '2024-06-17T00:00:00.000Z',
              isRangeValue: true
            }
          }

          const expected = {
            username: undefined,
            entityModel,
            entityKey: undefined,
            ipAddress: undefined,
            operation: undefined,
            dateFrom: '2024-06-16T00:00:00.000Z',
            dateTo: '2024-06-17T00:00:00.000Z'
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })

        it('date is range, only from has a value', () => {
          const searchFields = {
            date: {
              from: '2024-06-16T00:00:00.000Z',
              to: null,
              isRangeValue: true
            }
          }

          const expected = {
            username: undefined,
            entityModel,
            entityKey: undefined,
            ipAddress: undefined,
            operation: undefined,
            dateFrom: '2024-06-16T00:00:00.000Z',
            dateTo: null
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })

        it('handle date correctly if range is closed', () => {
          const searchFields = {
            date: '2024-06-16'
          }

          const expected = {
            username: undefined,
            entityModel,
            entityKey: undefined,
            ipAddress: undefined,
            operation: undefined,
            dateFrom: '2024-06-15T22:00:00.000Z',
            dateTo: '2024-06-17T00:00:00.000Z'
          }
          expect(mapToBeanNames(entityModel, searchFields)).to.be.eql(expected)
        })
      })
    })
  })
})
