import {combineSelection} from './selection'

describe('history', () => {
  describe('util', () => {
    describe('selection', () => {
      describe('combineSelection', () => {
        test('should apply on empty selection', () => {
          expect(combineSelection([], ['1', '2'], true)).to.eql(['1', '2'])
        })
        test('should keep only two and drop oldest', () => {
          expect(combineSelection([], ['1', '2', '3'], true)).to.eql(['2', '3'])
          expect(combineSelection(['1'], ['2', '3'], true)).to.eql(['2', '3'])
        })
        test('keep order', () => {
          expect(combineSelection(['2', '4'], ['1'], true)).to.eql(['4', '1'])
        })
        test('add one if possible w/o dropping', () => {
          expect(combineSelection([], ['1'], true)).to.eql(['1'])
          expect(combineSelection(['2'], ['1'], true)).to.eql(['2', '1'])
        })
        test('deselect properly', () => {
          expect(combineSelection([], ['1'], false)).to.eql([])
          expect(combineSelection(['2'], ['2'], false)).to.eql([])
          expect(combineSelection(['1', '2'], ['1'], false)).to.eql(['2'])
          expect(combineSelection(['1', '2'], ['2', '1'], false)).to.eql([])
        })
      })
    })
  })
})
