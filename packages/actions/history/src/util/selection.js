import _uniq from 'lodash/uniq'

/**
 * Updates selection according to selected/deselected keys.
 * Only keeps two entries selected - drops oldest selection.
 *
 * @param {Array} currentSelection
 * @param {Array} keys
 * @param {boolean} isSelected
 * @returns Array
 */
export const combineSelection = (currentSelection, keys, isSelected) => {
  if (!isSelected) {
    return currentSelection.filter(k => !keys.includes(k))
  }

  return _uniq([...currentSelection, ...keys]).slice(-2)
}
