import historyReducer, {sagas as historySagas} from './history'
import historyEntityOverviewReducer, {sagas as historyEntityOverviewSagas} from './historyEntityOverview'
import historyModelOverviewReducer, {sagas as historyModelOverviewSagas} from './historyModelOverview'
import historyTableReducer, {sagas as historyTableSagas} from './historyTable'

export default {
  history: historyReducer,
  historyEntityOverview: historyEntityOverviewReducer,
  historyModelOverview: historyModelOverviewReducer,
  historyTable: historyTableReducer
}

export const sagas = [historySagas, historyEntityOverviewSagas, historyModelOverviewSagas, historyTableSagas]
