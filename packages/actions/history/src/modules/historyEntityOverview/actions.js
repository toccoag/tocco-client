export const INITIALIZE_ENTITY_OVERVIEW = 'historyEntityOverview/INITIALIZE_ENTITY_OVERVIEW'
export const SET_SEARCH_FORM = 'historyEntityOverview/SET_SEARCH_FORM'
export const SET_LIST_FORM = 'historyEntityOverview/SET_LIST_FORM'
export const ADD_ENTRY = 'historyEntityOverview/ADD_ENTRY'
export const SET_MAX_AGE = 'historyEntityOverview/SET_MAX_AGE'

export const initializeEntityOverview = () => ({
  type: INITIALIZE_ENTITY_OVERVIEW
})

export const setSearchForm = searchForm => ({
  type: SET_SEARCH_FORM,
  payload: {searchForm}
})

export const setListForm = listForm => ({
  type: SET_LIST_FORM,
  payload: {listForm}
})

export const addEntry = (pk, entry) => ({
  type: ADD_ENTRY,
  payload: {pk, entry}
})

export const setMaxAge = maxAge => ({
  type: SET_MAX_AGE,
  payload: {maxAge}
})
