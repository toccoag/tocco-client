import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  searchForm: undefined,
  listForm: undefined,
  entries: {},
  maxAge: undefined
}

describe('history', () => {
  describe('historyEntityOverview', () => {
    describe('reducer', () => {
      test('should create a valid initial state', () => {
        expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
      })

      test('should handle an action', () => {
        const stateBefore = {
          searchForm: {},
          listForm: {},
          entries: {
            1: {key: '1'}
          },
          maxAge: 24
        }

        const expectedStateAfter = {
          searchForm: {},
          listForm: {},
          entries: {
            1: {key: '1'},
            2: {key: '2'}
          },
          maxAge: 24
        }

        expect(reducer(stateBefore, actions.addEntry('2', {key: '2'}))).to.deep.equal(expectedStateAfter)
      })
    })
  })
})
