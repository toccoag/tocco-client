import {takeLatest, all, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {notification, rest} from 'tocco-app-extensions'

import * as tableActions from '../historyTable/actions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('history', () => {
  describe('historyEntityOverview', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([
              takeLatest(actions.INITIALIZE_ENTITY_OVERVIEW, sagas.initializeEntityOverview),
              takeLatest(tableActions.SET_SELECTION, sagas.setSelection)
            ])
          )
          expect(generator.next().done).to.be.true
        })
      })

      describe('initializeEntityOverview', () => {
        test('initializeEntityOverview', () => {
          const expectedSearchForm = {id: 'search'}
          const expectedListForm = {id: 'list'}
          return expectSaga(sagas.initializeEntityOverview)
            .provide([
              [matchers.call(rest.fetchForm, 'History_action_entity_overview', 'search'), expectedSearchForm],
              [matchers.call(rest.fetchForm, 'History_action_entity_overview', 'list'), expectedListForm],
              [matchers.call(sagas.fetchSettings)]
            ])
            .put(actions.setSearchForm(expectedSearchForm))
            .put(actions.setListForm(expectedListForm))
            .run()
        })
      })

      describe('setSelection', () => {
        test('setSelection', () => {
          return expectSaga(sagas.setSelection, {payload: {selection: ['1', '2']}})
            .provide([[matchers.call.fn(sagas.fetchEntry), {}]])
            .call(sagas.fetchEntry, '1')
            .call(sagas.fetchEntry, '2')
            .run()
        })
      })

      describe('fetchEntry', () => {
        test('load from backend', () => {
          const response = {
            body: {
              xmlContent: '<content/>',
              entry: {
                pk: 1234,
                entityKey: '1',
                operation: 'UPDATED',
                username: 'admin@tocco.ch',
                ipAddress: '127.0.0.1',
                created: '2024-06-17T05:42:16.870Z'
              }
            }
          }
          const expectedEntry = {
            xmlContent: '<content/>',
            pk: 1234,
            entityKey: '1',
            operation: 'UPDATED',
            username: 'admin@tocco.ch',
            ipAddress: '127.0.0.1',
            created: '2024-06-17T05:42:16.870Z'
          }
          return expectSaga(sagas.fetchEntry, '1')
            .provide([
              [
                select(sagas.historyEntityOverviewSelector),
                {
                  entries: {}
                }
              ],
              [matchers.call.fn(rest.requestSaga), response]
            ])
            .put(actions.addEntry('1', expectedEntry))
            .run()
        })

        test('already loaded', () => {
          return expectSaga(sagas.fetchEntry, '1')
            .provide([
              [
                select(sagas.historyEntityOverviewSelector),
                {
                  entries: {
                    1: {}
                  }
                }
              ]
            ])
            .not.put.like({action: {type: actions.ADD_ENTRY}})
            .run()
        })

        test('error', () => {
          return expectSaga(sagas.fetchEntry, '1')
            .provide([
              [
                select(sagas.historyEntityOverviewSelector),
                {
                  entries: {}
                }
              ],
              [matchers.call.fn(rest.requestSaga), throwError(new Error('testException'))]
            ])
            .put(
              notification.toaster({
                type: 'error',
                title: 'client.actions.history.error.table.header',
                body: 'client.actions.history.error.table.body'
              })
            )
            .run()
        })
      })

      describe('fetchSettings', () => {
        test('load from backend', () => {
          return expectSaga(sagas.fetchSettings)
            .provide([
              [select(sagas.historyEntityOverviewSelector), {maxAge: undefined}],
              [matchers.call(rest.requestSaga, 'actions/history', {method: 'GET'}), {body: {maxAge: 24}}]
            ])
            .put(actions.setMaxAge(24))
            .run()
        })

        test('caching', () => {
          return expectSaga(sagas.fetchSettings)
            .provide([[select(sagas.historyEntityOverviewSelector), {maxAge: 3}]])
            .not.put(actions.setMaxAge(24))
            .run()
        })
      })
    })
  })
})
