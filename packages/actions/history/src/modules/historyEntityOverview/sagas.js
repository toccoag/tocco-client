import {all, takeLatest, call, put, select} from 'redux-saga/effects'
import {notification, rest} from 'tocco-app-extensions'

import * as tableActions from '../historyTable/actions'

import * as actions from './actions'

export const historyEntityOverviewSelector = state => state.historyEntityOverview

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_ENTITY_OVERVIEW, initializeEntityOverview),
    takeLatest(tableActions.SET_SELECTION, setSelection)
  ])
}

export function* initializeEntityOverview() {
  const [searchForm, listForm] = yield all([
    call(rest.fetchForm, 'History_action_entity_overview', 'search'),
    call(rest.fetchForm, 'History_action_entity_overview', 'list'),
    call(fetchSettings)
  ])
  yield put(actions.setSearchForm(searchForm))
  yield put(actions.setListForm(listForm))
}

export function* setSelection({payload: {selection}}) {
  for (const pk of selection) {
    yield call(fetchEntry, pk)
  }
}

export function* fetchEntry(pk) {
  const {entries} = yield select(historyEntityOverviewSelector)

  if (entries[pk]) {
    return
  }

  try {
    const response = yield call(rest.requestSaga, `actions/history/${pk}`, {method: 'GET'})
    const entry = {
      xmlContent: response.body.xmlContent,
      ...response.body.entry
    }
    yield put(actions.addEntry(pk, entry))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.history.error.table.header',
        body: 'client.actions.history.error.table.body'
      })
    )
  }
}

export function* fetchSettings() {
  const {maxAge} = yield select(historyEntityOverviewSelector)
  if (!maxAge) {
    const response = yield call(rest.requestSaga, 'actions/history', {method: 'GET'})
    yield put(actions.setMaxAge(response.body.maxAge))
  }
}
