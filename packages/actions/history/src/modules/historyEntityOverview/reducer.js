import {reducer as reducerUtil} from 'tocco-util'

import {SET_SEARCH_FORM, SET_LIST_FORM, ADD_ENTRY, SET_MAX_AGE} from './actions'

const initialState = {
  searchForm: undefined,
  listForm: undefined,
  entries: {},
  maxAge: undefined
}

const addEntry = (state, {payload: {pk, entry}}) => {
  return {
    ...state,
    entries: {...state.entries, [pk]: entry}
  }
}

const ACTION_HANDLERS = {
  [SET_SEARCH_FORM]: reducerUtil.singleTransferReducer('searchForm'),
  [SET_LIST_FORM]: reducerUtil.singleTransferReducer('listForm'),
  [ADD_ENTRY]: addEntry,
  [SET_MAX_AGE]: reducerUtil.singleTransferReducer('maxAge')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
