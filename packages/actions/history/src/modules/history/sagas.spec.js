import {select, takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {route} from 'tocco-util'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('history', () => {
  describe('history', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(all([takeLatest(actions.UPDATE_HISTORY, sagas.updateHistory)]))
          expect(generator.next().done).to.be.true
        })
      })

      describe('updateHistory', () => {
        test('with passed key', () => {
          return expectSaga(sagas.updateHistory, {payload: {pathname: ''}})
            .provide([
              [matchers.call.fn(route.extractParamsFromPath), {model: 'User', key: '1'}],
              [matchers.call.fn(sagas.updateBreadcrumbsInfo)]
            ])
            .put(actions.setModelName('User'))
            .put(actions.setView('entityOverview'))
            .put(actions.setCurrentEntity('1'))
            .run()
        })

        test('without passed key', () => {
          return expectSaga(sagas.updateHistory, {payload: {pathname: ''}})
            .provide([
              [matchers.call.fn(route.extractParamsFromPath), {model: 'User'}],
              [matchers.call.fn(sagas.updateBreadcrumbsInfo)]
            ])
            .put(actions.setModelName('User'))
            .put(actions.setView('modelOverview'))
            .put(actions.setCurrentEntity(undefined))
            .run()
        })
      })

      describe('updateBreadcrumbsInfo', () => {
        test('breadcrumb for model overview', () => {
          const breadcrumbsInfo = [
            {
              display: 'History - User',
              path: 'history/User',
              title: 'History - User',
              type: 'list'
            }
          ]
          return expectSaga(sagas.updateBreadcrumbsInfo, 'User', undefined)
            .provide([
              [select(sagas.textResourceSelector, 'client.actions.history.title'), 'History'],
              [matchers.call.fn(rest.fetchModel), {label: 'User'}]
            ])
            .put(actions.setBreadcrumbsInfo(breadcrumbsInfo))
            .run()
        })

        test('breadcrumb for entity overview', () => {
          const breadcrumbsInfo = [
            {
              display: 'History - User',
              path: 'history/User',
              title: 'History - User',
              type: 'list'
            },
            {
              display: '1 (Default Display)',
              title: 'History - User - 1 (Default Display)'
            }
          ]
          return expectSaga(sagas.updateBreadcrumbsInfo, 'User', '1')
            .provide([
              [select(sagas.textResourceSelector, 'client.actions.history.title'), 'History'],
              [matchers.call.fn(rest.fetchModel), {label: 'User'}],
              [matchers.call.fn(rest.fetchDisplay), 'Default Display']
            ])
            .put(actions.setBreadcrumbsInfo(breadcrumbsInfo))
            .run()
        })

        test('breadcrumb for entity overview without display (entity no longer exists)', () => {
          const breadcrumbsInfo = [
            {
              display: 'History - User',
              path: 'history/User',
              title: 'History - User',
              type: 'list'
            },
            {
              display: '1',
              title: 'History - User - 1'
            }
          ]
          return expectSaga(sagas.updateBreadcrumbsInfo, 'User', '1')
            .provide([
              [select(sagas.textResourceSelector, 'client.actions.history.title'), 'History'],
              [matchers.call.fn(rest.fetchModel), {label: 'User'}],
              [matchers.call.fn(rest.fetchDisplay), null]
            ])
            .put(actions.setBreadcrumbsInfo(breadcrumbsInfo))
            .run()
        })
      })
    })
  })
})
