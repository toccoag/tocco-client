import {all, call, takeLatest, put, select} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {route} from 'tocco-util'

import * as actions from './actions'

export const textResourceSelector = (state, key) => state.intl.messages[key]

export default function* sagas() {
  yield all([takeLatest(actions.UPDATE_HISTORY, updateHistory)])
}

export function* updateHistory({payload: {pathname}}) {
  const pathParams = yield call(route.extractParamsFromPath, '/history/:model{/:key}?', pathname)
  yield put(actions.setModelName(pathParams.model))
  yield put(actions.setView(!pathParams.key ? 'modelOverview' : 'entityOverview'))
  yield put(actions.setCurrentEntity(pathParams.key))
  yield call(updateBreadcrumbsInfo, pathParams.model, pathParams.key)
}

export function* updateBreadcrumbsInfo(modelName, key) {
  const historyTitle = yield select(textResourceSelector, 'client.actions.history.title')
  const entityModel = yield call(rest.fetchModel, modelName)

  const breadcrumbsInfo = [
    {
      display: `${historyTitle} - ${entityModel.label}`,
      path: `history/${modelName}`,
      title: `${historyTitle} - ${entityModel.label}`,
      type: 'list'
    }
  ]

  if (key) {
    const defaultDisplay = yield call(rest.fetchDisplay, modelName, key)
    const display = defaultDisplay ? `${key} (${defaultDisplay})` : key
    breadcrumbsInfo.push({
      display,
      title: `${historyTitle} - ${entityModel.label} - ${display}`
    })
  }

  yield put(actions.setBreadcrumbsInfo(breadcrumbsInfo))
}
