import {reducer as reducerUtil} from 'tocco-util'

import {SET_MODEL_NAME, SET_CURRENT_ENTITY, SET_VIEW, SET_BREADCRUMBS_INFO} from './actions'

const initialState = {
  modelName: undefined,
  currentEntity: undefined,
  view: undefined,
  breadcrumbsInfo: []
}

const ACTION_HANDLERS = {
  [SET_MODEL_NAME]: reducerUtil.singleTransferReducer('modelName'),
  [SET_CURRENT_ENTITY]: reducerUtil.singleTransferReducer('currentEntity'),
  [SET_VIEW]: reducerUtil.singleTransferReducer('view'),
  [SET_BREADCRUMBS_INFO]: reducerUtil.singleTransferReducer('breadcrumbsInfo')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
