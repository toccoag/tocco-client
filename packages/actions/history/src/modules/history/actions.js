export const UPDATE_HISTORY = 'history/UPDATE_HISTORY'
export const SET_MODEL_NAME = 'history/SET_MODEL_NAME'
export const SET_CURRENT_ENTITY = 'history/SET_CURRENT_ENTITY'
export const SET_VIEW = 'history/SET_VIEW'
export const SET_BREADCRUMBS_INFO = 'history/SET_BREADCRUMBS_INFO'

export const updateHistory = pathname => ({
  type: UPDATE_HISTORY,
  payload: {pathname}
})

export const setModelName = modelName => ({
  type: SET_MODEL_NAME,
  payload: {modelName}
})

export const setCurrentEntity = currentEntity => ({
  type: SET_CURRENT_ENTITY,
  payload: {currentEntity}
})

export const setView = view => ({
  type: SET_VIEW,
  payload: {view}
})

export const setBreadcrumbsInfo = breadcrumbsInfo => ({
  type: SET_BREADCRUMBS_INFO,
  payload: {breadcrumbsInfo}
})
