import {all, takeLatest, call, put} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.INITIALIZE_MODEL_OVERVIEW, initializeModelOverview)])
}

export function* initializeModelOverview() {
  const [searchForm, listForm] = yield all([
    call(rest.fetchForm, 'History_action_model_overview', 'search'),
    call(rest.fetchForm, 'History_action_model_overview', 'list')
  ])
  yield put(actions.setSearchForm(searchForm))
  yield put(actions.setListForm(listForm))
}
