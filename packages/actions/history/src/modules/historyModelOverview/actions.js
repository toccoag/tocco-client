export const INITIALIZE_MODEL_OVERVIEW = 'historyModelOverview/INITIALIZE_MODEL_OVERVIEW'
export const SET_SEARCH_FORM = 'historyModelOverview/SET_SEARCH_FORM'
export const SET_LIST_FORM = 'historyModelOverview/SET_LIST_FORM'

export const initializeModelOverview = () => ({
  type: INITIALIZE_MODEL_OVERVIEW
})

export const setSearchForm = searchForm => ({
  type: SET_SEARCH_FORM,
  payload: {searchForm}
})

export const setListForm = listForm => ({
  type: SET_LIST_FORM,
  payload: {listForm}
})
