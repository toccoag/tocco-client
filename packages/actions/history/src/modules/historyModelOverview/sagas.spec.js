import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('history', () => {
  describe('historyModelOverview', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([takeLatest(actions.INITIALIZE_MODEL_OVERVIEW, sagas.initializeModelOverview)])
          )
          expect(generator.next().done).to.be.true
        })
      })

      describe('initializeModelOverview', () => {
        test('initializeModelOverview', () => {
          const expectedSearchForm = {id: 'search'}
          const expectedListForm = {id: 'list'}
          return expectSaga(sagas.initializeModelOverview)
            .provide([
              [matchers.call(rest.fetchForm, 'History_action_model_overview', 'search'), expectedSearchForm],
              [matchers.call(rest.fetchForm, 'History_action_model_overview', 'list'), expectedListForm]
            ])
            .put(actions.setSearchForm(expectedSearchForm))
            .put(actions.setListForm(expectedListForm))
            .run()
        })
      })
    })
  })
})
