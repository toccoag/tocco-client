import {reducer as reducerUtil} from 'tocco-util'

import {SET_SEARCH_FORM, SET_LIST_FORM} from './actions'

const initialState = {
  searchForm: undefined,
  listForm: undefined
}

const ACTION_HANDLERS = {
  [SET_SEARCH_FORM]: reducerUtil.singleTransferReducer('searchForm'),
  [SET_LIST_FORM]: reducerUtil.singleTransferReducer('listForm')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
