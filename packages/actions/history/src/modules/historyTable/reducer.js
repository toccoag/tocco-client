import {reducer as reducerUtil} from 'tocco-util'

import {
  SET_CURRENT_PAGE,
  LOAD_NEW_PAGE,
  SET_TOTAL_COUNT,
  SET_SEARCH_FIELDS,
  SET_TABLE_DATA,
  SET_SELECTION,
  SET_LOADING
} from './actions'

const initialState = {
  totalCount: 0,
  currentPage: 1,
  recordsPerPage: 25,
  searchFields: {},
  loading: false,
  tableData: [],
  selection: []
}

const ACTION_HANDLERS = {
  [SET_LOADING]: reducerUtil.singleTransferReducer('loading'),
  [SET_TOTAL_COUNT]: reducerUtil.singleTransferReducer('totalCount'),
  [SET_CURRENT_PAGE]: reducerUtil.singleTransferReducer('currentPage'),
  [LOAD_NEW_PAGE]: reducerUtil.mappingTransferReducer('newPage', 'currentPage'),
  [SET_SEARCH_FIELDS]: reducerUtil.singleTransferReducer('searchFields'),
  [SET_TABLE_DATA]: reducerUtil.singleTransferReducer('tableData'),
  [SET_SELECTION]: reducerUtil.singleTransferReducer('selection')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
