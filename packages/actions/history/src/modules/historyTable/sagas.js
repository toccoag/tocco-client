import {all, takeLatest, call, put, select} from 'redux-saga/effects'
import {notification, rest} from 'tocco-app-extensions'

import {mapToBeanNames, mapToFormNames} from '../../util/form'
import {combineSelection} from '../../util/selection'

import * as actions from './actions'

export const historySelector = state => state.history
export const historyTableSelector = state => state.historyTable

export default function* sagas() {
  yield all([
    takeLatest(actions.ON_SELECT_CHANGE, onSelectChange),
    takeLatest(actions.SET_SEARCH_FIELDS, loadData),
    takeLatest(actions.LOAD_NEW_PAGE, loadData)
  ])
}

export function* onSelectChange({payload: {keys, isSelected}}) {
  const {selection: currentSelection} = yield select(historyTableSelector)
  const newSelection = yield call(combineSelection, currentSelection, keys, isSelected)

  yield put(actions.setSelection(newSelection))
}

export function* loadData() {
  const {modelName, currentEntity} = yield select(historySelector)
  const {recordsPerPage, currentPage, searchFields} = yield select(historyTableSelector)

  const body = {
    ...mapToBeanNames(modelName, searchFields),
    ...(currentEntity ? {entityKey: currentEntity} : {}),
    offset: (currentPage - 1) * recordsPerPage,
    limit: recordsPerPage
  }

  try {
    yield put(actions.setLoading(true))
    const response = yield call(rest.requestSaga, '/actions/history', {method: 'POST', body})
    const entries = response.body.entries
    const totalRecords = response.body.totalCount

    yield put(actions.setTableData(entries.map(e => mapToFormNames(e))))
    yield put(actions.setTotalCount(totalRecords))
    yield put(actions.setLoading(false))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.history.error.table.header',
        body: 'client.actions.history.error.table.body'
      })
    )
    yield put(actions.setTableData([]))
    yield put(actions.setTotalCount(0))
    yield put(actions.setLoading(false))
  }
}
