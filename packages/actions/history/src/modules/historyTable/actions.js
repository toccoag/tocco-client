export const SET_CURRENT_PAGE = 'historyTable/SET_CURRENT_PAGE'
export const LOAD_NEW_PAGE = 'historyTable/LOAD_NEW_PAGE'
export const SET_TOTAL_COUNT = 'historyTable/SET_TOTAL_COUNT'
export const SET_SEARCH_FIELDS = 'historyTable/SET_SEARCH_FIELDS'
export const SET_TABLE_DATA = 'historyTable/SET_TABLE_DATA'
export const SET_LOADING = 'historyTable/SET_LOADING'
export const ON_SELECT_CHANGE = 'historyTable/ON_SELECT_CHANGE'
export const SET_SELECTION = 'historyTable/SET_SELECTION'

export const setCurrentPage = currentPage => ({
  type: SET_CURRENT_PAGE,
  payload: {currentPage}
})

export const loadNewPage = newPage => ({
  type: LOAD_NEW_PAGE,
  payload: {newPage}
})

export const setTotalCount = totalCount => ({
  type: SET_TOTAL_COUNT,
  payload: {
    totalCount
  }
})

export const setSearchFields = searchFields => ({
  type: SET_SEARCH_FIELDS,
  payload: {searchFields}
})

export const setTableData = tableData => ({
  type: SET_TABLE_DATA,
  payload: {tableData}
})

export const setLoading = loading => ({
  type: SET_LOADING,
  payload: {loading}
})

export const onSelectChange = (keys, isSelected) => ({
  type: ON_SELECT_CHANGE,
  payload: {
    keys,
    isSelected
  }
})

export const setSelection = selection => ({
  type: SET_SELECTION,
  payload: {
    selection
  }
})
