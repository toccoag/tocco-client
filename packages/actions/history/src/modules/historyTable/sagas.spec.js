import {select, takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {notification, rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('history', () => {
  describe('historyTable', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([
              takeLatest(actions.ON_SELECT_CHANGE, sagas.onSelectChange),
              takeLatest(actions.SET_SEARCH_FIELDS, sagas.loadData),
              takeLatest(actions.LOAD_NEW_PAGE, sagas.loadData)
            ])
          )
          expect(generator.next().done).to.be.true
        })
      })

      describe('onSelectChange', () => {
        test('one item currently selected and one item new selected', () => {
          return expectSaga(sagas.onSelectChange, {payload: {keys: ['2'], isSelected: true}})
            .provide([[select(sagas.historyTableSelector), {selection: ['1']}]])
            .put(actions.setSelection(['1', '2']))
            .run()
        })

        test('two items currently selected and one item new selected (oldest key)', () => {
          return expectSaga(sagas.onSelectChange, {payload: {keys: ['3'], isSelected: true}})
            .provide([[select(sagas.historyTableSelector), {selection: ['1', '2']}]])
            .put(actions.setSelection(['2', '3']))
            .run()
        })

        test('two items currently selected and one item new selected (oldest key)', () => {
          return expectSaga(sagas.onSelectChange, {payload: {keys: ['1'], isSelected: true}})
            .provide([[select(sagas.historyTableSelector), {selection: ['2', '3']}]])
            .put(actions.setSelection(['3', '1']))
            .run()
        })

        test('two items currently selected and item deselected', () => {
          return expectSaga(sagas.onSelectChange, {payload: {keys: ['2'], isSelected: false}})
            .provide([[select(sagas.historyTableSelector), {selection: ['1', '2']}]])
            .put(actions.setSelection(['1']))
            .run()
        })

        test('add three items', () => {
          return expectSaga(sagas.onSelectChange, {payload: {keys: ['1', '2', '3'], isSelected: true}})
            .provide([[select(sagas.historyTableSelector), {selection: []}]])
            .put(actions.setSelection(['2', '3']))
            .run()
        })
      })

      describe('loadData', () => {
        test('success without entity key', () => {
          const response = {
            body: {
              entries: [
                {
                  pk: 1234,
                  entityKey: '1',
                  operation: 'UPDATED',
                  username: 'admin@tocco.ch',
                  ipAddress: '127.0.0.1',
                  created: '2024-06-17T05:42:16.870Z'
                }
              ],
              totalCount: 1
            }
          }
          const expectedTableData = [
            {
              __key: 1234,
              entity_key: '1',
              operation: 'UPDATED',
              username: 'admin@tocco.ch',
              ip_address: '127.0.0.1',
              date: '2024-06-17T05:42:16.870Z'
            }
          ]
          return expectSaga(sagas.loadData, {})
            .provide([
              [
                select(sagas.historySelector),
                {
                  modelName: 'User'
                }
              ],
              [
                select(sagas.historyTableSelector),
                {
                  recordsPerPage: 25,
                  currentPage: 1,
                  searchFields: {}
                }
              ],
              [matchers.call.fn(rest.requestSaga), response]
            ])
            .put(actions.setLoading(true))
            .put(actions.setTableData(expectedTableData))
            .put(actions.setTotalCount(1))
            .put(actions.setLoading(false))
            .run()
        })

        test('success with entity key', () => {
          const response = {
            body: {
              entries: [],
              totalCount: 0
            }
          }
          const expectedTableData = []
          return expectSaga(sagas.loadData, {})
            .provide([
              [
                select(sagas.historySelector),
                {
                  modelName: 'User',
                  currentEntity: '6789'
                }
              ],
              [
                select(sagas.historyTableSelector),
                {
                  recordsPerPage: 25,
                  currentPage: 1,
                  searchFields: {}
                }
              ],
              [matchers.call.fn(rest.requestSaga), response]
            ])
            .put(actions.setLoading(true))
            .put(actions.setTableData(expectedTableData))
            .put(actions.setTotalCount(0))
            .put(actions.setLoading(false))
            .run()
        })

        test('error', () => {
          return expectSaga(sagas.loadData, {})
            .provide([
              [
                select(sagas.historySelector),
                {
                  modelName: 'User'
                }
              ],
              [
                select(sagas.historyTableSelector),
                {
                  recordsPerPage: 25,
                  currentPage: 1,
                  searchFields: {}
                }
              ],
              [matchers.call.fn(rest.requestSaga), throwError(new Error('testException'))]
            ])
            .put(actions.setLoading(true))
            .put(
              notification.toaster({
                type: 'error',
                title: 'client.actions.history.error.table.header',
                body: 'client.actions.history.error.table.body'
              })
            )
            .put(actions.setTableData([]))
            .put(actions.setTotalCount(0))
            .put(actions.setLoading(false))
            .run()
        })
      })
    })
  })
})
