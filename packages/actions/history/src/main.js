import {appFactory, actionEmitter, notification, externalEvents, errorLogging} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import History from './components/History'
import reducers, {sagas} from './modules/reducers'
import {getModelOverviewUrl, getEntityOverviewUrl} from './util/url'

const packageName = 'history'

const EXTERNAL_EVENTS = ['emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <History />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  errorLogging.addToStore(store, handleNotifications, ['console', 'remote', 'notification'])
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const HistoryApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

HistoryApp.propTypes = {}

export default HistoryApp
export {getModelOverviewUrl, getEntityOverviewUrl}
