import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {EditableValue, Button, LoadMask, StatedValue} from 'tocco-ui'

import {
  StyledContainer,
  StyledLogWrapper,
  StyledHeader,
  StyledLogFileStatedValue,
  StyledLogFileReadonlyStatedValue,
  StyledButtonItem,
  StyledTextAreaItem,
  StyledTextarea,
  StyledHostNameWrapper
} from './StyledComponents'

const LogFile = ({data, currentFile, setCurrentFile, msg}) => {
  const options = data?.map(fileNames => ({key: fileNames, display: fileNames})) || null

  return data?.length === 1 ? (
    <StyledLogFileReadonlyStatedValue>
      <StatedValue label={msg('client.log.logfile')} immutable>
        <EditableValue value={currentFile?.display} type="string" id="fileSelectField" readOnly />
      </StatedValue>
    </StyledLogFileReadonlyStatedValue>
  ) : (
    <StyledLogFileStatedValue>
      <StatedValue label={msg('client.log.logfile')}>
        <EditableValue
          type="single-select"
          value={currentFile}
          options={options ? {options} : null}
          events={{onChange: setCurrentFile}}
          id="fileSelectField"
        />
      </StatedValue>
    </StyledLogFileStatedValue>
  )
}

LogFile.propTypes = {
  data: PropTypes.array,
  currentFile: PropTypes.shape({
    display: PropTypes.string
  }),
  setCurrentFile: PropTypes.func.isRequired,
  msg: PropTypes.func.isRequired
}

const Log = ({data, fetchData, fileContent, fetchFileContent, intl}) => {
  const [lines, setLines] = useState(500)
  const [currentFile, setCurrentFile] = useState({key: 'nice.log', display: 'nice.log'})

  const conditionalString = `${currentFile?.key}?lines=${lines}`

  useEffect(() => {
    fetchData()
  }, [fetchData])

  // load file content on mount
  useEffect(() => {
    fetchFileContent(conditionalString)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fetchFileContent])

  const msg = id => intl.formatMessage({id})

  return (
    <LoadMask required={[data, fileContent]}>
      <StyledContainer>
        <StyledLogWrapper>
          <StyledHeader>
            <LogFile data={data} currentFile={currentFile} setCurrentFile={setCurrentFile} msg={msg} />
            <StatedValue label={msg('client.log.filecount')}>
              <EditableValue type="integer" value={lines} events={{onChange: setLines}} id="fileCountField" />
            </StatedValue>
            <StyledHostNameWrapper>
              <StatedValue label={msg('client.log.hostname')} immutable>
                <EditableValue value={fileContent?.hostName || null} type="string" id="hostnameField" readOnly />
              </StatedValue>
            </StyledHostNameWrapper>
            <StyledButtonItem>
              <Button ink="primary" look="raised" onClick={() => fetchFileContent(conditionalString)}>
                <FormattedMessage id="client.log.reload" />
              </Button>
            </StyledButtonItem>
          </StyledHeader>
          <StyledTextAreaItem>
            <StyledTextarea value={fileContent?.fileContent || ''} readOnly={true} />
          </StyledTextAreaItem>
        </StyledLogWrapper>
      </StyledContainer>
    </LoadMask>
  )
}

Log.propTypes = {
  fetchData: PropTypes.func.isRequired,
  fetchFileContent: PropTypes.func.isRequired,
  data: PropTypes.array,
  fileContent: PropTypes.shape({
    fileContent: PropTypes.string,
    hostName: PropTypes.string
  }),
  intl: PropTypes.any
}

export default Log
