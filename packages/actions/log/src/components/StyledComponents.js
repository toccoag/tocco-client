import styled from 'styled-components'
import {theme, declareFont, scale} from 'tocco-ui'

export const StyledContainer = styled.div`
  background-color: ${theme.color('paper')};
  display: flex;
  justify-content: center;
  height: 100dvh;
  padding: 0;
`

export const StyledHeader = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${scale.space(0)};
  padding-left: ${scale.space(0.5)};
  padding-right: ${scale.space(0.5)};
  padding-bottom: ${scale.space(0.5)};
  border-bottom: 1px solid ${theme.color('borderLight')};

  @media (max-width: 890px) {
    flex-direction: column;
    gap: ${scale.space(-1.5)};
  }
`

export const StyledLogWrapper = styled.div`
  margin-top: ${scale.space(0.5)};
  width: 100%;
`

export const StyledLogFileStatedValue = styled.div`
  min-width: 300px;
`

export const StyledLogFileReadonlyStatedValue = styled.div`
  &&&& input {
    min-height: 2.6rem;
  }
`

export const StyledHostNameWrapper = styled.div`
  &&&& input {
    min-height: 2.6rem;
  }
`

export const StyledButtonItem = styled.div`
  margin-top: ${scale.space(0.7)};

  @media (max-width: 890px) {
    margin-top: 0;
    align-self: flex-end;
  }
`

export const StyledTextAreaItem = styled.div`
  text-align: start;
  height: 100%;
`

export const StyledTextarea = styled.textarea`
  resize: none;
  margin: 0;
  min-height: 4em;
  width: 100%;
  height: calc(100% - 100px); /* height of header + spacing */
  box-sizing: border-box;
  background-color: transparent;
  border: 0;
  cursor: ${({immutable}) => immutable && 'text'};
  min-width: 100%;
  outline: 0;
  padding: 0;
  padding-top: ${scale.space(-0.5)};
  padding-left: ${scale.space(0.5)};
  padding-right: ${scale.space(-0.5)};
  ${() =>
    declareFont({
      color: theme.color('text'),
      fontFamily: theme.fontFamily('monospace')
    })}

  &::-ms-clear {
    display: none;
  }

  &::-webkit-clear-button {
    display: none;
  }

  &::-webkit-inner-spin-button {
    display: none;
  }

  &:disabled {
    -webkit-text-fill-color: ${theme.color('text')}; /* Safari fix */
    opacity: 1; /* iOS fix */
  }

  /* allow pointer event only on touch devices */
  @media (pointer: coarse) {
    pointer-events: ${({immutable}) => (immutable ? 'none' : 'auto')};
  }
`
