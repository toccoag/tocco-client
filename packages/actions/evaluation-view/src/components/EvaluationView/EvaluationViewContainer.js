import {connect} from 'react-redux'
import {actions} from 'tocco-app-extensions'

import {initializeSearch} from '../../modules/evaluationViewSearch/actions'
import {initializeTable} from '../../modules/evaluationViewTable/actions'

import EvaluationView from './EvaluationView'

const mapActionCreators = {
  initializeTable,
  initializeSearch,
  onActionClick: actions.actions.actionInvoke
}

const mapStateToProps = state => ({
  selection: state.input.selection,
  actionDefinitions: state.evaluationViewTable.actionDefinitions,
  handleNotifications: state.evaluationView.handleNotifications,
  evaluationInProgress: state.evaluationView.evaluationInProgress
})

export default connect(mapStateToProps, mapActionCreators)(EvaluationView)
