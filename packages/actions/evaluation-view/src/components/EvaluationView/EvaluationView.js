import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {actions, notification, selection as selectionPropType} from 'tocco-app-extensions'
import {
  SidepanelMainContent,
  Sidepanel,
  SidepanelContainer,
  SidepanelHeader,
  Button,
  ActionBarMainContent,
  ActionBar
} from 'tocco-ui'
import {env} from 'tocco-util'

import EvaluationViewSearch from '../EvaluationViewSearch'
import EvaluationViewTable from '../EvaluationViewTable/EvaluationViewTableContainer'

import {StyledPaneWrapper} from './StyledEvaluationView'

const EvaluationView = ({
  selection,
  handleNotifications,
  initializeTable,
  initializeSearch,
  actionDefinitions,
  evaluationInProgress,
  onActionClick
}) => {
  useEffect(() => {
    initializeTable()
    initializeSearch()
  }, [selection, initializeTable, initializeSearch])

  const [isCollapsed, setIsCollapsed] = useState(false)

  const customRenderedActions = {
    calculate_evaluation: definition => (
      <Button
        onClick={() => onActionClick(definition)}
        icon={definition.icon}
        label={definition.label}
        disabled={evaluationInProgress}
        pending={evaluationInProgress}
        look={'raised'}
      />
    )
  }

  const Actions = actionDefinitions.map(definition => (
    <actions.Action
      key={definition.id}
      definition={definition}
      selection={selection}
      customRenderedActions={customRenderedActions}
    />
  ))

  const isAdmin = env.isInAdminEmbedded()

  return (
    <>
      {handleNotifications && <notification.Notifications />}
      <StyledPaneWrapper>
        <SidepanelContainer
          sidepanelPosition={isAdmin ? 'left' : 'top'}
          sidepanelCollapsed={isCollapsed}
          setSidepanelCollapsed={setIsCollapsed}
          scrollBehaviour={isAdmin ? 'inline' : 'none'}
        >
          <Sidepanel>
            <SidepanelHeader />
            <EvaluationViewSearch />
          </Sidepanel>

          <SidepanelMainContent actionBar>
            <ActionBar>{Actions}</ActionBar>
            <ActionBarMainContent>
              <EvaluationViewTable />
            </ActionBarMainContent>
          </SidepanelMainContent>
        </SidepanelContainer>
      </StyledPaneWrapper>
    </>
  )
}

EvaluationView.propTypes = {
  selection: selectionPropType.propType.isRequired,
  handleNotifications: PropTypes.bool,
  initializeTable: PropTypes.func.isRequired,
  initializeSearch: PropTypes.func.isRequired,
  actionDefinitions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  ),
  evaluationInProgress: PropTypes.bool,
  onActionClick: PropTypes.func.isRequired
}

export default EvaluationView
