import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setSearchQueries} from '../../modules/evaluationViewSearch/actions'

import EvaluationViewSearch from './EvaluationViewSearch'

const mapActionCreators = {
  setSearchFields: setSearchQueries
}

const mapStateToProps = state => ({
  form: state.evaluationViewSearch.form
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EvaluationViewSearch))
