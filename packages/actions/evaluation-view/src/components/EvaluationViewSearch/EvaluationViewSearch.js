import PropTypes from 'prop-types'
import {searchform} from 'tocco-app-extensions'
import SimpleFormApp from 'tocco-simple-form/src/main'

const EvaluationViewSearch = ({intl, form: searchFormDefinition, setSearchFields}) => (
  <searchform.BasicSearchForm
    searchFormDefinition={searchFormDefinition}
    setSearchFields={setSearchFields}
    FormApp={SimpleFormApp}
    intl={intl}
  />
)

EvaluationViewSearch.propTypes = {
  intl: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  setSearchFields: PropTypes.func.isRequired
}

export default EvaluationViewSearch
