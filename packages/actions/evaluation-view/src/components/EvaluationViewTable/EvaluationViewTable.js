import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {actions, field} from 'tocco-app-extensions'
import {Table} from 'tocco-ui'
import {api, env, table} from 'tocco-util'

import {buildPromotionStatusReason} from './promotionStatusHelper'
import {StyledCell, StyledTableWrapper} from './StyledComponents'

const dataCellRenderer = (fieldDefinition, data, intl) => {
  const {path, dataType, componentType} = fieldDefinition
  const value = data[path]

  const isPromotionStatus = path === 'relPromotion_status.label' && data.relPromotion_reason?.length > 0

  if (componentType === 'action') {
    return (
      <actions.Action
        key={`${fieldDefinition.id}-${data.pk}`}
        definition={fieldDefinition}
        selection={{entityName: 'Evaluation_data', ids: [`${data.pk}`], type: 'ID'}}
      />
    )
  }

  const Field = field.factory('list', dataType)
  return (
    <StyledCell key={path} title={isPromotionStatus ? buildPromotionStatusReason(data, intl) : null}>
      <Field
        type={dataType}
        mappingType="list"
        formField={fieldDefinition}
        value={value}
        breakWords={false}
        useTitle={!isPromotionStatus}
      />
    </StyledCell>
  )
}

const evaluationCellRenderer = (fieldDefinition, data) => {
  const path = fieldDefinition.id
  const {dataType} = fieldDefinition.definition
  const Field = field.factory('list', dataType)
  const value = data[path]

  return (
    <StyledCell key={path}>
      <Field
        type={dataType}
        mappingType="list"
        formField={fieldDefinition.definition}
        value={value}
        breakWords={false}
      />
    </StyledCell>
  )
}

const EvaluationViewTable = ({
  data,
  dataFormColumns,
  evaluationViewForm,
  sorting,
  setSorting,
  dataLoadingInProgress,
  totalCount,
  currentPage,
  recordsPerPage,
  setCurrentPage,
  intl
}) => {
  const [columns, setColumns, onColumnPositionChange] = table.useColumnPosition()

  useEffect(() => {
    const dataColumns = api.getColumnDefinition(dataFormColumns, sorting, dataCellRenderer, intl)

    const evaluationColumns = evaluationViewForm.map((c, idx) => ({
      idx,
      id: c.id,
      label: c.label,
      sorting: {
        sortable: false
      },
      definition: c,
      resizable: true,
      dynamic: true,
      alignment: 'left',
      CellRenderer: ({rowData, column}) => evaluationCellRenderer(column, rowData)
    }))
    setColumns([...dataColumns, ...evaluationColumns])
  }, [dataFormColumns, evaluationViewForm, sorting, setColumns, intl])

  if (columns.length === 0) {
    return null
  }

  const isAdmin = env.isInAdminEmbedded()

  return (
    <StyledTableWrapper>
      <Table
        dataLoadingInProgress={dataLoadingInProgress}
        columns={columns}
        data={data}
        onSortingChange={setSorting}
        onColumnPositionChange={onColumnPositionChange}
        paginationInfo={{
          currentPage,
          recordsPerPage,
          totalCount
        }}
        onPageChange={setCurrentPage}
        scrollBehaviour={isAdmin ? 'inline' : 'none'}
      />
    </StyledTableWrapper>
  )
}

EvaluationViewTable.propTypes = {
  data: PropTypes.array.isRequired,
  dataFormColumns: PropTypes.arrayOf(PropTypes.shape({id: PropTypes.string})),
  sorting: PropTypes.arrayOf(
    PropTypes.shape({
      field: PropTypes.string,
      order: PropTypes.string
    })
  ),
  evaluationViewForm: PropTypes.array.isRequired,
  setSorting: PropTypes.func.isRequired,
  dataLoadingInProgress: PropTypes.bool,
  totalCount: PropTypes.number,
  currentPage: PropTypes.number.isRequired,
  recordsPerPage: PropTypes.number.isRequired,
  setCurrentPage: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default EvaluationViewTable
