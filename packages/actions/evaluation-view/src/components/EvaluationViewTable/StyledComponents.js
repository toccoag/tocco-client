import styled from 'styled-components'
import {scale, themeSelector, declareFocus, StyledReactSelectOuterWrapper} from 'tocco-ui'

export const StyledTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`

export const StyledCell = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  ${StyledReactSelectOuterWrapper} {
    margin-right: 10px;
  }

  &&&& {
    input,
    textarea,
    &.single-select > span > div {
      border: 1px solid ${themeSelector.color('border')};
      padding-left: ${scale.space(-1)};

      &:not([disabled]):hover {
        cursor: pointer;
      }

      /* prevent focus style within react select components */
      :not([id*='react-select']) {
        ${declareFocus}
      }
    }

    &.single-select > span {
      display: inline-block;
      width: 100%;
      margin-left: 5px;
      margin-bottom: 5px;
      background-color: ${themeSelector.color('paper')};
    }

    &.single-select button {
      margin-bottom: 2px;
      margin-right: 2px;
    }
  }
`
