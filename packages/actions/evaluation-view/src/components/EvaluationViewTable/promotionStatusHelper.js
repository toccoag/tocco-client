const promotionReasonPaths = ['positive', 'context', 'relPromotion_rule.label']

const flattenPromotionReason = data =>
  data.relPromotion_reason?.map((_, index) =>
    promotionReasonPaths.reduce(
      (flattened, path) => ({
        ...flattened,
        [path]: data['relPromotion_reason.' + path][index]
      }),
      {}
    )
  ) || []

export const buildPromotionStatusReason = (data, intl) => {
  const positiveLabel = intl.formatMessage({id: 'client.actions.evaluation-view.conclusion.positive'})
  const negativeLabel = intl.formatMessage({id: 'client.actions.evaluation-view.conclusion.negative'})
  const contextLabel = intl.formatMessage({id: 'client.actions.evaluation-view.conclusion.context'})
  return flattenPromotionReason(data)
    .map(
      reason =>
        `${reason.positive ? positiveLabel : negativeLabel}: ${reason['relPromotion_rule.label']} ${
          reason.context ? `${contextLabel}: ${reason.context}` : ''
        }`
    )
    .map(string => string.trim())
    .join('<br>')
}
