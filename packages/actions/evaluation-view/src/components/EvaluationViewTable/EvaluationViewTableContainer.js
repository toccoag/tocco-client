import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setCurrentPage} from '../../modules/evaluationViewPagination/actions'
import {setSorting} from '../../modules/evaluationViewTable/actions'

import EvaluationViewTable from './EvaluationViewTable'

const mapActionCreators = {
  setSorting,
  setCurrentPage
}

const mapStateToProps = state => {
  return {
    data: state.evaluationViewTable.data,
    dataFormColumns: state.evaluationViewTable.dataFormColumns,
    evaluationViewForm: state.evaluationViewTable.evaluationViewForm,
    sorting: state.evaluationViewTable.sorting,
    dataLoadingInProgress: state.evaluationViewTable.dataLoadingInProgress,
    totalCount: state.evaluationViewPagination.totalCount,
    currentPage: state.evaluationViewPagination.currentPage,
    recordsPerPage: state.evaluationViewPagination.recordsPerPage
  }
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EvaluationViewTable))
