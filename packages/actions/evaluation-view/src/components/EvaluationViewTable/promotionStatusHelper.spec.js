import {IntlStub} from 'tocco-test-util'

import {buildPromotionStatusReason} from './promotionStatusHelper'

describe('evaluation-view', () => {
  describe('components', () => {
    describe('EvaluationViewTable', () => {
      describe('promotionStatusHelper', () => {
        const positiveLabel = 'client.actions.evaluation-view.conclusion.positive'
        const negativeLabel = 'client.actions.evaluation-view.conclusion.negative'
        const contextLabel = 'client.actions.evaluation-view.conclusion.context'
        test('combines reason data into string', () => {
          const data = {
            relPromotion_reason: [{key: 1}, {key: 2}, {key: 3}],
            'relPromotion_reason.positive': [true, false, true],
            'relPromotion_reason.context': ['', '', 'context'],
            'relPromotion_reason.relPromotion_rule.label': ['promotion rule 1', 'promotion rule 2', 'promotion rule 3']
          }
          const result = buildPromotionStatusReason(data, IntlStub)
          expect(result).to.eql(
            `${positiveLabel}: promotion rule 1<br>` +
              `${negativeLabel}: promotion rule 2<br>` +
              `${positiveLabel}: promotion rule 3 ${contextLabel}: context`
          )
        })
        test('returns empty string on no reason', () => {
          const data = {
            relPromotion_reason: [],
            'relPromotion_reason.positive': [],
            'relPromotion_reason.context': [],
            'relPromotion_reason.relPromotion_rule.label': []
          }
          const result = buildPromotionStatusReason(data, IntlStub)
          expect(result).to.be.empty
        })
        test('returns empty string on missing reason', () => {
          const data = {}
          const result = buildPromotionStatusReason(data, IntlStub)
          expect(result).to.be.empty
        })
      })
    })
  })
})
