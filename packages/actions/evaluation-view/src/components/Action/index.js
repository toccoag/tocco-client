import {lazy} from 'react'
import {actions} from 'tocco-app-extensions'

const actionMap = {
  'evaluation-view-info': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EvaluationViewInfo')),
  'evaluation-view-tree': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EvaluationViewTree'))
}

const LazyAction = actions.actionFactory(actionMap)

export default LazyAction
