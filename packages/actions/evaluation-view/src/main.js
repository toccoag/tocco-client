import PropTypes from 'prop-types'
import {actionEmitter, actions, appFactory, errorLogging, notification} from 'tocco-app-extensions'
import {appContext, reducer as reducerUtil, env} from 'tocco-util'

import Action from './components/Action'
import EvaluationView from './components/EvaluationView/EvaluationViewContainer'
import customActions from './customActions'
import {setHandleNotifications} from './modules/evaluationView/actions'
import reducers, {sagas} from './modules/reducers'

const packageName = 'evaluation-view'

const EXTERNAL_EVENTS = ['emitAction']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <EvaluationView />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  actions.dynamicActionsAddToStore(store)
  actions.addToStore(store, () => ({
    customActions,
    navigationStrategy: input.navigationStrategy,
    appComponent: Action
  }))
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  errorLogging.addToStore(store, handleNotifications, ['console', 'remote', 'notification'])

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [setHandleNotifications(handleNotifications)],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EvaluationViewApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

EvaluationViewApp.propTypes = {
  selection: PropTypes.object,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {}),
  appContext: appContext.propTypes
}

export default EvaluationViewApp
