import * as actions from './actions'
import reducer from './reducer'

describe('evaluation-view', () => {
  describe('evaluation-view-table', () => {
    describe('reducer', () => {
      describe('addStructureData', () => {
        test('should add data', () => {
          const {data} = reducer({data: [{pk: 1}]}, actions.addStructureData({1: {columnId: 'column', value: 12}}))
          expect(data).to.have.length(1)
          expect(data[0]).to.be.eql({pk: 1, column: 12})
        })
        test('should overwrite data', () => {
          const {data} = reducer(
            {data: [{pk: 1, column: 34}]},
            actions.addStructureData({1: {columnId: 'column', value: 12}})
          )
          expect(data).to.have.length(1)
          expect(data[0]).to.be.eql({pk: 1, column: 12})
        })
        test('should not touch other data', () => {
          const {data} = reducer({data: [{pk: 2}]}, actions.addStructureData({1: {columnId: 'column', value: 12}}))
          expect(data).to.have.length(1)
          expect(data[0]).to.be.eql({pk: 2})
        })
      })
    })
  })
})
