import {reducer as reducerUtil, sorting} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  actionDefinitions: [],
  dataFormColumns: [],
  data: [],
  evaluationViewForm: [],
  sorting: [],
  dataLoadingInProgress: false
}

export const addStructureData = (state, {payload: {structureData}}) => {
  return {
    ...state,
    data: state.data.map(entity => {
      const structure = structureData[entity.pk]
      if (structure) {
        return {
          ...entity,
          [structure.columnId]: structure.value
        }
      } else {
        return entity
      }
    })
  }
}

const ACTION_HANDLERS = {
  [actions.SET_DATA_FORM_COLUMNS]: reducerUtil.singleTransferReducer('dataFormColumns'),
  [actions.SET_ACTION_DEFINITIONS]: reducerUtil.singleTransferReducer('actionDefinitions'),
  [actions.SET_EVALUATION_FORM]: reducerUtil.singleTransferReducer('evaluationViewForm'),
  [actions.SET_DATA]: reducerUtil.singleTransferReducer('data'),
  [actions.SET_DATA_LOADING_IN_PROGRESS]: reducerUtil.singleTransferReducer('dataLoadingInProgress'),
  [actions.SET_SORTING]: sorting.reducer,
  [actions.ADD_STRUCTURE_DATA]: addStructureData
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
