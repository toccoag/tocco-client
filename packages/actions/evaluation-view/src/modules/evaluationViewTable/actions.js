export const INITIALIZE_TABLE = 'evaluationViewTable/INITIALIZE_TABLE'
export const LOAD_DATA = 'evaluationViewTable/LOAD_DATA'
export const SET_DATA = 'evaluationViewTable/SET_DATA'
export const SET_EVALUATION_FORM = 'evaluationViewTable/SET_EVALUATION_FORM'
export const SET_SORTING = 'evaluationViewTable/SET_SORTING'
export const SET_DATA_FORM_COLUMNS = 'evaluationViewTable/SET_DATA_FORM_COLUMNS'
export const SET_ACTION_DEFINITIONS = 'evaluationViewTable/SET_ACTION_DEFINITIONS'
export const SET_DATA_LOADING_IN_PROGRESS = 'evaluationViewTable/SET_DATA_LOADING_IN_PROGRESS'
export const ADD_STRUCTURE_DATA = 'evaluationViewTable/ADD_STRUCTURE_DATA'

export const initializeTable = () => ({
  type: INITIALIZE_TABLE
})

export const loadData = () => ({
  type: LOAD_DATA
})

export const setData = data => ({
  type: SET_DATA,
  payload: {data}
})

export const setEvaluationForm = form => ({
  type: SET_EVALUATION_FORM,
  payload: form
})

export const setSorting = (field, add) => ({
  type: SET_SORTING,
  payload: {
    field,
    add
  }
})

export const setDataFormColumns = dataFormColumns => ({
  type: SET_DATA_FORM_COLUMNS,
  payload: {
    dataFormColumns
  }
})

export const setActionDefinitions = actionDefinitions => ({
  type: SET_ACTION_DEFINITIONS,
  payload: {
    actionDefinitions
  }
})

export const setDataLoadingInProgress = dataLoadingInProgress => ({
  type: SET_DATA_LOADING_IN_PROGRESS,
  payload: {
    dataLoadingInProgress
  }
})

export const addStructureData = structureData => ({
  type: ADD_STRUCTURE_DATA,
  payload: {
    structureData
  }
})
