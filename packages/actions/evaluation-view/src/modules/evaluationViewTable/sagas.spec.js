import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {qualification as qualificationUtil} from 'tocco-util'

import * as searchActions from '../evaluationViewSearch/actions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view', () => {
  describe('evaluation-view-table', () => {
    describe('sagas', () => {
      const fakeDataColumns = [
        {
          children: [
            {
              path: 'first field'
            }
          ]
        },
        {
          children: [
            {
              path: 'second field'
            }
          ]
        }
      ]

      const fakeAction = {
        id: 'action',
        componentType: 'action',
        scope: 'detail'
      }

      const fakeDataForm = {
        children: [
          {
            id: 'main-action-bar',
            label: null,
            componentType: 'action-bar',
            children: [
              {
                id: 'output',
                label: 'Ausgabe'
              },
              {
                id: 'nice2.reporting.actions.ChangelogExportAction',
                label: 'Changelog exportieren'
              },
              fakeAction
            ],
            actions: [],
            defaultAction: null
          },
          {
            componentType: 'table',
            children: fakeDataColumns
          }
        ]
      }

      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([
            takeLatest(actions.INITIALIZE_TABLE, sagas.initialize),
            takeLatest(actions.LOAD_DATA, sagas.loadData),
            takeLatest(actions.SET_SORTING, sagas.loadData)
          ])
        )
        expect(generator.next().done).to.be.true
      })

      describe('initialize', () => {
        test('should load forms', () => {
          const expectedEvaluationForm = [
            {
              evaluationForm: 'evaluationForm'
            }
          ]
          const expectedDataForm = {
            dataform: 'dataform'
          }
          return expectSaga(sagas.initialize)
            .provide([
              [select(sagas.inputSelector), {selection: [12]}],
              [select(sagas.evaluationViewSearchSelector), {initialized: true}],
              [select(sagas.evaluationViewSelector), {validation: {valid: true}}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    evaluationColumns: expectedEvaluationForm
                  }
                }
              ],
              [matchers.call.fn(rest.fetchForm), expectedDataForm],
              [matchers.call.fn(sagas.loadData)],
              [matchers.call.fn(sagas.processDataForm)]
            ])
            .call(rest.fetchForm, 'Evaluation_view_data', 'list')
            .put(actions.setEvaluationForm({evaluationViewForm: expectedEvaluationForm}))
            .call(sagas.processDataForm, expectedDataForm)
            .call(sagas.loadData)
            .run()
        })

        test('should load data form from evaluation', () => {
          const expectedEvaluationForm = [
            {
              evaluationForm: 'evaluationForm'
            }
          ]
          const expectedDataForm = {
            dataform: 'dataform'
          }
          return expectSaga(sagas.initialize)
            .provide([
              [
                select(sagas.inputSelector),
                {
                  selection: [12]
                }
              ],
              [select(sagas.evaluationViewSearchSelector), {initialized: false}],
              [select(sagas.evaluationViewSelector), {validation: {valid: true}, updateInProgress: false}],
              [
                matchers.call.fn(rest.requestSaga),
                {
                  body: {
                    evaluationColumns: expectedEvaluationForm
                  }
                }
              ],
              [matchers.call.fn(sagas.processDataForm), {}],
              [matchers.call.fn(rest.fetchForm), expectedDataForm],
              [matchers.call.fn(sagas.loadData)]
            ])
            .dispatch(searchActions.initializeSearch(true))
            .call(rest.fetchForm, 'Evaluation_view_data', 'list')
            .run()
        })
      })

      describe('load-data', () => {
        const fakeResponse = [
          {
            pk: {
              value: 11
            },
            relUser: {
              value: {
                key: 12
              }
            }
          },
          {
            pk: {
              value: 21
            },
            relUser: {
              value: {
                key: 22
              }
            }
          }
        ]
        test('should load data from state', () => {
          const fakeSelection = {ids: ['12']}
          return expectSaga(sagas.loadData)
            .provide([
              [select(sagas.inputSelector), {selection: fakeSelection}],
              [select(sagas.evaluationViewSelector), {validation: {valid: true}}],
              [
                select(sagas.evaluationViewTableSelector),
                {
                  dataFormColumns: fakeDataColumns,
                  sorting: [{field: 'field', order: 'asc'}]
                }
              ],
              [select(sagas.searchQueriesSelector), ["firstname == 'whatever'"]],
              [
                select(sagas.evaluationViewPaginationSelector),
                {
                  count: 0,
                  currentPage: 1,
                  recordsPerPage: 25
                }
              ],
              [matchers.call.fn(rest.requestSaga), {body: {data: fakeResponse}}],
              [matchers.fork.fn(sagas.loadStructureData)]
            ])
            .call.like({
              fn: rest.requestSaga,
              args: [
                'evaluationView/data/search',
                {
                  method: 'POST',
                  body: {
                    selection: fakeSelection,
                    searchBean: {
                      paths: ['first field', 'second field', 'pk'],
                      sort: 'field asc',
                      where: "firstname == 'whatever'",
                      limit: 25,
                      offset: 0
                    }
                  }
                }
              ]
            })
            .run()
        })
      })

      describe('processDataForm', () => {
        test('should set action and column defintions', () => {
          const actionDefinitions = [{id: 'action'}]
          const dataFormColumns = [{id: 'column'}]
          return expectSaga(sagas.processDataForm, fakeDataForm)
            .provide([[matchers.call.fn(qualificationUtil.splitDataForm), {actionDefinitions, dataFormColumns}]])
            .put(actions.setActionDefinitions(actionDefinitions))
            .put(actions.setDataFormColumns(dataFormColumns))
            .run()
        })
      })

      describe('loadStructureData', () => {
        test('should load all columns', () => {
          const userDataKeys = {1: 2, 3: 4}
          const evaluationViewForm = [{id: 'Evaluation-1'}, {id: 'Input-2'}]
          return expectSaga(sagas.loadStructureData, userDataKeys)
            .provide([
              [select(sagas.evaluationViewTableSelector), {evaluationViewForm}],
              [matchers.call.fn(sagas.loadColumnData)]
            ])
            .call(sagas.loadColumnData, 'Evaluation', '1', userDataKeys)
            .call(sagas.loadColumnData, 'Input', '2', userDataKeys)
            .run()
        })
      })

      describe('loadColumnData', () => {
        test('should load all column data', () => {
          const selection = {ids: [1]}
          const userDataKeys = {1: 2, 3: 4}
          const data = {1: 12, 3: 'value'}
          return expectSaga(sagas.loadColumnData, 'Evaluation', '1', userDataKeys)
            .provide([
              [matchers.call.fn(rest.requestSaga), {body: {data}}],
              [select(sagas.inputSelector), {selection}]
            ])
            .call(rest.requestSaga, 'evaluationView/data/structure', {
              method: 'POST',
              body: {
                selection,
                structureModel: 'Evaluation',
                structureKey: '1',
                userDataKeys: ['1', '3']
              }
            })
            .put(
              actions.addStructureData({
                2: {columnId: 'Evaluation-1', value: 12},
                4: {columnId: 'Evaluation-1', value: 'value'}
              })
            )
            .run()
        })
      })
    })
  })
})
