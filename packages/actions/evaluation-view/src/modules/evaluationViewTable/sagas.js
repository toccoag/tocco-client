import {all, call, put, select, take, takeLatest, fork} from 'redux-saga/effects'
import {rest, qualification} from 'tocco-app-extensions'
import {api, qualification as qualificationUtil} from 'tocco-util'

import {setTotalCount} from '../evaluationViewPagination/actions'
import * as searchFormActions from '../evaluationViewSearch/actions'

import * as actions from './actions'

export const inputSelector = state => state.input
export const evaluationViewSelector = state => state.evaluationView
export const evaluationViewTableSelector = state => state.evaluationViewTable
export const evaluationViewSearchSelector = state => state.evaluationViewSearch
export const searchQueriesSelector = state => state.evaluationViewSearch.searchQueries
export const evaluationViewPaginationSelector = state => state.evaluationViewPagination

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_TABLE, initialize),
    takeLatest(actions.LOAD_DATA, loadData),
    takeLatest(actions.SET_SORTING, loadData)
  ])
}

export function* processDataForm(dataForm) {
  const {actionDefinitions, dataFormColumns} = yield call(qualificationUtil.splitDataForm, dataForm)

  yield put(actions.setActionDefinitions(actionDefinitions))
  yield put(actions.setDataFormColumns(dataFormColumns))
}

export function* initialize() {
  yield put(actions.setDataLoadingInProgress(true))
  const {selection} = yield select(inputSelector)
  const [evaluationFormDefinition, dataForm] = yield all([
    call(rest.requestSaga, 'evaluationView/form', {method: 'POST', body: selection}),
    call(rest.fetchForm, 'Evaluation_view_data', 'list')
  ])
  const {evaluationColumns} = evaluationFormDefinition.body
  yield put(actions.setEvaluationForm({evaluationViewForm: evaluationColumns}))
  yield call(processDataForm, dataForm)

  const {initialized: searchFormInitialized} = yield select(evaluationViewSearchSelector)
  if (!searchFormInitialized) {
    yield take(searchFormActions.SET_INITIALIZED)
  }
  yield call(loadData)
}

export function* loadData() {
  yield put(actions.setDataLoadingInProgress(true))
  const {selection} = yield select(inputSelector)
  const {sorting, dataFormColumns} = yield select(evaluationViewTableSelector)
  const searchQueries = yield select(searchQueriesSelector)
  const {recordsPerPage, currentPage} = yield select(evaluationViewPaginationSelector)
  const response = yield call(
    qualification.requestData,
    'evaluationView/data/search',
    selection,
    sorting,
    dataFormColumns,
    searchQueries,
    recordsPerPage,
    currentPage
  )
  const data = response.body.data
    .map(dataRecord => api.flattenPaths(dataRecord))
    .map(dataRecord => ({...dataRecord, __key: dataRecord.pk}))
  yield put(actions.setData(data))
  yield put(setTotalCount(response.body.count))
  yield put(actions.setDataLoadingInProgress(false))
  const userDataKeys = data.reduce((acc, entry) => {
    return {
      ...acc,
      [entry.relUser.key]: entry.pk
    }
  }, {})
  yield fork(loadStructureData, userDataKeys)
}

export function* loadStructureData(userDataKeys) {
  const {evaluationViewForm} = yield select(evaluationViewTableSelector)
  const loaders = evaluationViewForm
    .map(column => column.id.split('-'))
    .map(([structureModel, structureKey]) => call(loadColumnData, structureModel, structureKey, userDataKeys))
  yield all(loaders)
}

export function* loadColumnData(structureModel, structureKey, userDataKeys) {
  const {selection} = yield select(inputSelector)
  const response = yield call(rest.requestSaga, 'evaluationView/data/structure', {
    method: 'POST',
    body: {
      selection,
      structureModel,
      structureKey,
      userDataKeys: Object.keys(userDataKeys)
    }
  })
  const columnId = `${structureModel}-${structureKey}`
  const data = Object.entries(response.body.data).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [userDataKeys[key]]: {columnId, value}
    }),
    {}
  )
  yield put(actions.addStructureData(data))
}
