import {all, takeLatest, call, put, select} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import {loadData} from '../evaluationViewTable/sagas'

import * as actions from './actions'

export const evaluationViewSelector = state => state.evaluationView
export const evaluationViewSearchSelector = state => state.evaluationViewSearch

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_SEARCH, initialize),
    takeLatest(actions.SET_SEARCH_QUERIES, setSearchQueries)
  ])
}

export function* initialize() {
  const form = yield call(rest.fetchForm, 'Evaluation_view_data', 'search')
  yield put(actions.setForm(form))
}

export function* setSearchQueries() {
  const {initialized} = yield select(evaluationViewSearchSelector)
  if (initialized) {
    yield call(loadData)
  } else {
    yield put(actions.setInitialized(true))
  }
}
