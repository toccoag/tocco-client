import {takeLatest, all, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import {loadData} from '../evaluationViewTable/sagas'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view', () => {
  describe('evaluation-view-search', () => {
    describe('sagas', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([
            takeLatest(actions.INITIALIZE_SEARCH, sagas.initialize),
            takeLatest(actions.SET_SEARCH_QUERIES, sagas.setSearchQueries)
          ])
        )
        expect(generator.next().done).to.be.true
      })

      describe('initialize', () => {
        test('should load forms', () => {
          const expectedForm = {}
          return expectSaga(sagas.initialize)
            .provide([
              [matchers.call.fn(rest.fetchForm), expectedForm],
              [select(sagas.evaluationViewSelector), {validation: {valid: true}}]
            ])
            .put(actions.setForm(expectedForm))
            .run()
        })
      })

      describe('setSearchQueries', () => {
        test('should pass search queries to loadData if initialized', () => {
          return expectSaga(sagas.setSearchQueries)
            .provide([[matchers.call.fn(loadData)], [select(sagas.evaluationViewSearchSelector), {initialized: true}]])
            .call(loadData)
            .run()
        })

        test('should set initialized true on first call', () => {
          return expectSaga(sagas.setSearchQueries)
            .provide([[matchers.call.fn(loadData)], [select(sagas.evaluationViewSearchSelector), {initialized: false}]])
            .put(actions.setInitialized(true))
            .run()
        })
      })
    })
  })
})
