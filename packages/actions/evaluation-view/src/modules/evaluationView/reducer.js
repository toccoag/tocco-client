import {reducer as reducerUtil} from 'tocco-util'

import {SET_HANDLE_NOTIFICATIONS, TOGGLE_EVALUATION_IN_PROGRESS} from './actions'

const initialState = {
  handleNotifications: false,
  evaluationInProgress: false
}

const ACTION_HANDLERS = {
  [SET_HANDLE_NOTIFICATIONS]: reducerUtil.singleTransferReducer('handleNotifications'),
  [TOGGLE_EVALUATION_IN_PROGRESS]: reducerUtil.toggleReducer('evaluationInProgress')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
