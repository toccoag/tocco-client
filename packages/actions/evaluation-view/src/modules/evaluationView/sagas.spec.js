import {all, takeLatest, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, notification, actions as actionsHelper} from 'tocco-app-extensions'

import * as tableSagas from '../evaluationViewTable/sagas'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view', () => {
  describe('modules', () => {
    describe('evaluationView', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actionsHelper.actions.ACTION_INVOKED, sagas.reload),
                takeLatest(actions.RUN_EVALUATION, sagas.runEvaluation)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('runEvaluation', () => {
          test('should run evaluation', () => {
            const evaluationKey = '1'
            const selection = {ids: [evaluationKey], entityName: 'Evaluation', type: 'ID'}
            return expectSaga(sagas.runEvaluation)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call.fn(rest.requestSaga), {status: 200}],
                [matchers.call.fn(tableSagas.loadData)]
              ])
              .call(rest.requestSaga, `/qualification/evaluation/${evaluationKey}`, {method: 'POST'})
              .put(actions.toggleEvaluationInProgress())
              .put(
                notification.toaster({
                  type: 'success',
                  title: 'client.actions.EvaluationView.evaluation_completed'
                })
              )
              .call(tableSagas.loadData)
              .put(actions.toggleEvaluationInProgress())
              .run()
          })
        })
      })
    })
  })
})
