export const SET_HANDLE_NOTIFICATIONS = 'evaluationView/SET_HANDLE_NOTIFICATIONS'
export const RUN_EVALUATION = 'evaluationView/RUN_EVALUATION'
export const TOGGLE_EVALUATION_IN_PROGRESS = 'evaluationView/TOGGLE_EVALUATION_IN_PROGRESS'

export const setHandleNotifications = handleNotifications => ({
  type: SET_HANDLE_NOTIFICATIONS,
  payload: {
    handleNotifications
  }
})

export const runEvaluation = () => ({
  type: RUN_EVALUATION,
  payload: {}
})

export const toggleEvaluationInProgress = () => ({
  type: TOGGLE_EVALUATION_IN_PROGRESS
})
