import {all, put, takeLatest, select, call} from 'redux-saga/effects'
import {actions as actionsHelper, selection as selectionUtil, rest, notification} from 'tocco-app-extensions'

import * as evaluationViewTableActions from '../evaluationViewTable/actions'
import {loadData} from '../evaluationViewTable/sagas'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([
    takeLatest(actionsHelper.actions.ACTION_INVOKED, reload),
    takeLatest(actions.RUN_EVALUATION, runEvaluation)
  ])
}

export function* reload() {
  yield put(evaluationViewTableActions.initializeTable())
}

export function* runEvaluation() {
  yield put(actions.toggleEvaluationInProgress())
  const {selection} = yield select(inputSelector)
  const evaluationKey = selectionUtil.getSingleKey(selection, 'Evaluation')
  const response = yield call(rest.requestSaga, `/qualification/evaluation/${evaluationKey}`, {method: 'POST'})
  if (response.status === 200) {
    yield put(
      notification.toaster({
        type: 'success',
        title: 'client.actions.EvaluationView.evaluation_completed'
      })
    )
    yield call(loadData)
  }
  yield put(actions.toggleEvaluationInProgress())
}
