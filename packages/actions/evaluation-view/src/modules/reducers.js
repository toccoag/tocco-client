import evaluationViewReducer, {sagas as evaluationViewSagas} from './evaluationView'
import evaluationViewPaginationReducer, {sagas as evaluationViewPaginationSagas} from './evaluationViewPagination'
import evaluationViewSearchReducer, {sagas as evaluationViewSearchSagas} from './evaluationViewSearch'
import evaluationViewTableReducer, {sagas as evaluationViewTableSagas} from './evaluationViewTable'

export default {
  evaluationView: evaluationViewReducer,
  evaluationViewTable: evaluationViewTableReducer,
  evaluationViewSearch: evaluationViewSearchReducer,
  evaluationViewPagination: evaluationViewPaginationReducer
}

export const sagas = [
  evaluationViewSagas,
  evaluationViewTableSagas,
  evaluationViewSearchSagas,
  evaluationViewPaginationSagas
]
