import {all, takeLatest, call} from 'redux-saga/effects'

import {loadData} from '../evaluationViewTable/sagas'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.SET_CURRENT_PAGE, setCurrentPage)])
}

export function* setCurrentPage() {
  yield call(loadData)
}
