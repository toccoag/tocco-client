export const SET_CURRENT_PAGE = 'evaluationViewPagination/SET_CURRENT_PAGE'
export const SET_TOTAL_COUNT = 'evaluationViewPagination/SET_TOTAL_COUNT'

export const setCurrentPage = currentPage => ({
  type: SET_CURRENT_PAGE,
  payload: {currentPage}
})

export const setTotalCount = totalCount => ({
  type: SET_TOTAL_COUNT,
  payload: {
    totalCount
  }
})
