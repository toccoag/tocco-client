import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'

import {loadData} from '../evaluationViewTable/sagas'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view', () => {
  describe('evaluation-view-pagination', () => {
    describe('sagas', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(all([takeLatest(actions.SET_CURRENT_PAGE, sagas.setCurrentPage)]))
        expect(generator.next().done).to.be.true
      })

      describe('setCurrentPage', () => {
        test('should set page', () => {
          return expectSaga(sagas.setCurrentPage)
            .provide([[matchers.call.fn(loadData)]])
            .call(loadData)
            .run()
        })
      })
    })
  })
})
