import {v4 as uuid} from 'uuid'

import EvaluationViewApp from './main'

export default {
  title: 'Apps/Evaluation View',
  component: EvaluationViewApp,
  argTypes: {
    selection: {
      type: 'object',
      defaultValue: {type: 'ID', entityName: 'Evaluation', ids: ['11']}
    }
  }
}

export const Basic = args => <EvaluationViewApp key={uuid()} {...args} />
