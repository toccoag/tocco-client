import {put} from 'redux-saga/effects'

import {runEvaluation} from './modules/evaluationView/actions'

export default {
  calculate_evaluation: function* (definition, selection) {
    yield put(runEvaluation())
  }
}
