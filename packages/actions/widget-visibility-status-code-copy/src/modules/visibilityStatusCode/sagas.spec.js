import {all, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {notification, rest} from 'tocco-app-extensions'
import {js} from 'tocco-util'

import {generateVisibilityStatusCode} from '../../utils/visibilityStatusCode'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('widget-visibility-status-code-copy', () => {
  describe('modules', () => {
    describe('visibilityStatusCode', () => {
      describe('sagas', () => {
        describe('root saga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeEvery(actions.FETCH_WIDGET_CONFIG, sagas.fetchWidgetConfig),
                takeLatest(actions.COPY_VISIBILITY_STATUS_CODE, sagas.copyVisibilityStatusCode)
              ])
            )
            expect(generator.next().done).to.equal(true)
          })
        })

        describe('fetchWidgetConfig', () => {
          const widgetConfig = {
            key: '163',
            model: 'Widget_config',
            paths: {
              relWidget: {
                value: {
                  key: '27',
                  model: 'Widget',
                  paths: {
                    relWidget_app: {
                      type: 'entity',
                      value: {
                        key: '1',
                        model: 'Widget_app',
                        paths: {
                          relWidget_visibility_status: {
                            value: [
                              {
                                key: '1',
                                model: 'Widget_visibility_status',
                                paths: {visibility_status: {value: 'list'}, label: {value: 'Liste'}}
                              },
                              {
                                key: '2',
                                model: 'Widget_visibility_status',
                                paths: {visibility_status: {value: 'detail'}, label: {value: 'Detail'}}
                              }
                            ]
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          const expectedVisibilityStatus = [
            {
              key: '1',
              model: 'Widget_visibility_status',
              paths: {visibility_status: {value: 'list'}, label: {value: 'Liste'}}
            },
            {
              key: '2',
              model: 'Widget_visibility_status',
              paths: {visibility_status: {value: 'detail'}, label: {value: 'Detail'}}
            }
          ]

          test('should fetch widget config', () => {
            return expectSaga(sagas.fetchWidgetConfig)
              .provide([
                [select(sagas.inputSelector), {selection: {entityName: 'Widget_config', type: 'ID', ids: ['6']}}],
                [matchers.call.fn(rest.fetchEntity), widgetConfig]
              ])
              .put(actions.setWidgetVisibilityStatus(expectedVisibilityStatus))
              .run()
          })

          test('should throw error when multiple configs are selected', () => {
            return expectSaga(sagas.fetchWidgetConfig)
              .provide([
                [select(sagas.inputSelector), {selection: {entityName: 'Widget_config', type: 'ID', ids: ['6', '8']}}]
              ])
              .not.call.fn(rest.fetchEntity)
              .put.like({action: notification.toaster({type: 'error'})})
              .run()
          })

          test('should throw error when a wrong entity is selected', () => {
            return expectSaga(sagas.fetchWidgetConfig)
              .provide([[select(sagas.inputSelector), {selection: {entityName: 'User', type: 'ID', ids: ['6']}}]])
              .not.call.fn(rest.fetchEntity)
              .put.like({action: notification.toaster({type: 'error'})})
              .run()
          })

          test('should fetch widget config with selection via query selection', () => {
            return expectSaga(sagas.fetchWidgetConfig)
              .provide([
                [select(sagas.inputSelector), {selection: {entityName: 'Widget_config', type: 'QUERY', query: {}}}],
                [matchers.call.fn(rest.fetchEntitiesPage), [{key: '78'}]],
                [matchers.call.fn(rest.fetchEntity), widgetConfig]
              ])
              .put(actions.setWidgetVisibilityStatus(expectedVisibilityStatus))
              .run()
          })

          test('should throw error when multiple configs are selected via query selection', () => {
            return expectSaga(sagas.fetchWidgetConfig)
              .provide([
                [select(sagas.inputSelector), {selection: {entityName: 'Widget_config', type: 'QUERY', query: {}}}],
                [matchers.call.fn(rest.fetchEntitiesPage), [{key: '78'}, {key: '23'}]]
              ])
              .not.call.fn(rest.fetchEntity)
              .put.like({action: notification.toaster({type: 'error'})})
              .run()
          })
        })

        describe('copyVisibilityStatusCode', () => {
          test('should show success toaster', () => {
            return expectSaga(sagas.copyVisibilityStatusCode, actions.copyVisibilityStatusCode(['list']))
              .provide([
                [select(sagas.visibilityStatusCodeSelector), {widgetUniqueId: '6'}],
                [matchers.call.like(js.copyToClipboard)]
              ])
              .run()
          })

          test('should generate visibility status code', () => {
            const visibilityStatus = ['list']
            const visibilityStatusCode = generateVisibilityStatusCode('6', visibilityStatus)
            return expectSaga(sagas.copyVisibilityStatusCode, actions.copyVisibilityStatusCode(visibilityStatus))
              .provide([
                [select(sagas.visibilityStatusCodeSelector), {widgetUniqueId: '6'}],
                [matchers.call(js.copyToClipboard, visibilityStatusCode)]
              ])
              .run()
          })

          test('should show error toaster', () => {
            return expectSaga(sagas.copyVisibilityStatusCode, actions.copyVisibilityStatusCode(['list']))
              .provide([
                [select(sagas.inputSelector), {selection: {entityName: 'Widget_config', type: 'ID', ids: ['6']}}],
                [matchers.call.like(js.copyToClipboard), throwError(new Error('copy failed'))]
              ])
              .put.like({action: notification.toaster({type: 'error'})})
              .run()
          })
        })
      })
    })
  })
})
