export const FETCH_WIDGET_CONFIG = 'visibilityStatusCode/FETCH_WIDGET_CONFIG'
export const SET_WIDGET_VISIBILITY_STATUS = 'visibilityStatusCode/SET_WIDGET_VISIBILITY_STATUS'
export const COPY_VISIBILITY_STATUS_CODE = 'visibilityStatusCode/COPY_VISIBILITY_STATUS_CODE'
export const SET_WIDGET_UNIQUE_ID = 'visibilityStatusCode/SET_WIDGET_UNIQUE_ID'

export const fetchWidgetConfig = () => ({
  type: FETCH_WIDGET_CONFIG
})

export const setWidgetVisibilityStatus = visibilityStatus => ({
  type: SET_WIDGET_VISIBILITY_STATUS,
  payload: {
    visibilityStatus
  }
})

export const copyVisibilityStatusCode = visibilityStatus => ({
  type: COPY_VISIBILITY_STATUS_CODE,
  payload: {
    visibilityStatus
  }
})

export const setWidgetUniqueId = widgetUniqueId => ({
  type: SET_WIDGET_UNIQUE_ID,
  payload: {
    widgetUniqueId
  }
})
