import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  visibilityStatus: undefined,
  widgetUniqueId: undefined
}

describe('widget-visibility-status-code-copy', () => {
  describe('modules', () => {
    describe('visibilityStatusCode', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        test('should handle SET_WIDGET_VISIBILITY_STATUS', () => {
          const visibilityStatus = [
            {
              key: '1',
              model: 'Widget_visibility_status',
              paths: {
                visibility_status: {
                  type: 'string',
                  writable: false,
                  value: 'list'
                },
                label: {
                  type: 'string',
                  writable: false,
                  value: 'Liste'
                }
              }
            },
            {
              key: '2',
              model: 'Widget_visibility_status',
              paths: {
                visibility_status: {
                  type: 'string',
                  writable: false,
                  value: 'detail'
                },
                label: {
                  type: 'string',
                  writable: false,
                  value: 'Detail'
                }
              }
            }
          ]

          const stateBefore = {
            visibilityStatus: undefined
          }

          const expectedState = {
            visibilityStatus
          }

          expect(reducer(stateBefore, actions.setWidgetVisibilityStatus(visibilityStatus))).to.deep.equal(expectedState)
        })
      })
    })
  })
})
