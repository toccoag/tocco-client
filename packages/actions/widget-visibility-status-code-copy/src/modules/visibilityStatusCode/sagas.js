import {all, call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {rest, notification, selection as selectionUtil, externalEvents} from 'tocco-app-extensions'
import {js, consoleLogger} from 'tocco-util'

import {generateVisibilityStatusCode} from '../../utils/visibilityStatusCode'

import * as actions from './actions'

export const inputSelector = state => state.input
export const visibilityStatusCodeSelector = state => state.visibilityStatusCode

export default function* sagas() {
  yield all([
    takeEvery(actions.FETCH_WIDGET_CONFIG, fetchWidgetConfig),
    takeLatest(actions.COPY_VISIBILITY_STATUS_CODE, copyVisibilityStatusCode)
  ])
}

export function* fetchWidgetConfig() {
  const {selection} = yield select(inputSelector)

  try {
    const entities = yield call(selectionUtil.getEntities, selection, rest.fetchEntitiesPage)
    if (entities.keys.length === 1 && entities.entityName === 'Widget_config') {
      const widgetConfigKey = entities.keys[0]
      const widgetConfig = yield call(rest.fetchEntity, 'Widget_config', widgetConfigKey, {
        paths: [
          'relWidget.relWidget_app.relWidget_visibility_status.label',
          'relWidget.relWidget_app.relWidget_visibility_status.visibility_status',
          'unique_id'
        ]
      })
      yield put(
        actions.setWidgetVisibilityStatus(
          widgetConfig.paths.relWidget.value.paths.relWidget_app.value.paths.relWidget_visibility_status.value
        )
      )
      yield put(actions.setWidgetUniqueId(widgetConfig.paths.unique_id.value))
    } else {
      throw new Error('Invalid selection')
    }
  } catch (err) {
    consoleLogger.logError('Failed to get selected Widget_config', err)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.widget-visibility-status-code-copy.toasterTitle',
        body: 'client.actions.widget-visibility-status-code-copy.fetchFailedMessage'
      })
    )
  }
}

export function* copyVisibilityStatusCode({payload}) {
  const {visibilityStatus} = payload
  try {
    const {widgetUniqueId} = yield select(visibilityStatusCodeSelector)
    const visibilityStatusCode = generateVisibilityStatusCode(widgetUniqueId, visibilityStatus)
    yield call(js.copyToClipboard, visibilityStatusCode)
    yield put(
      externalEvents.fireExternalEvent('onSuccess', {
        title: 'client.actions.widget-visibility-status-code-copy.toasterTitle',
        message: 'client.actions.widget-visibility-status-code-copy.copiedMessage'
      })
    )
  } catch (err) {
    consoleLogger.logError('Failed to generate and copy widget code', err)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.widget-visibility-status-code-copy.toasterTitle',
        body: 'client.actions.widget-visibility-status-code-copy.copyFailedMessage'
      })
    )
  }
}
