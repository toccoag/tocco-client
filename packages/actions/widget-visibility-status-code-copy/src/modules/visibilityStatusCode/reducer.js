import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_WIDGET_VISIBILITY_STATUS]: reducerUtil.singleTransferReducer('visibilityStatus'),
  [actions.SET_WIDGET_UNIQUE_ID]: reducerUtil.singleTransferReducer('widgetUniqueId')
}

const initialState = {
  visibilityStatus: undefined,
  widgetUniqueId: undefined
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
