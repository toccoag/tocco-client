import visibilityStatusCodeReducer, {sagas as visibilityStatusCodeSagas} from './visibilityStatusCode'

export default {
  visibilityStatusCode: visibilityStatusCodeReducer
}

export const sagas = [visibilityStatusCodeSagas]
