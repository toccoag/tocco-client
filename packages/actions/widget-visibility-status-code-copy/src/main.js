import {appFactory, actionEmitter, externalEvents, notification, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import VisibilityStatusCodeCopy from './components/VisibilityStatusCodeCopy'
import reducers, {sagas} from './modules/reducers'

const packageName = 'widget-visibility-status-code-copy'

const EXTERNAL_EVENTS = ['onSuccess', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <VisibilityStatusCodeCopy />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  notification.addToStore(store, false)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const WidgetVisibilityStatusCodeCopyApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

WidgetVisibilityStatusCodeCopyApp.propTypes = {
  selection: selection.propType.isRequired
}

export default WidgetVisibilityStatusCodeCopyApp
