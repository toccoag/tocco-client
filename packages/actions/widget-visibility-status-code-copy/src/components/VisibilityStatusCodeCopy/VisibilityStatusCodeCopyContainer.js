import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {copyVisibilityStatusCode, fetchWidgetConfig} from '../../modules/visibilityStatusCode/actions'

import VisibilityStatusCodeCopy from './VisibilityStatusCodeCopy'

const mapActionCreators = {
  fetchWidgetConfig,
  copyVisibilityStatusCode
}

const mapStateToProps = state => ({
  widgetConfigKey: state.visibilityStatusCode.widgetUniqueId,
  visibilityStatus: state.visibilityStatusCode.visibilityStatus
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(VisibilityStatusCodeCopy))
