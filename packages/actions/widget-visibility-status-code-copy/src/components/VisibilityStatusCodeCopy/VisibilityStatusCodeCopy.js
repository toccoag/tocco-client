import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, EditableValue, LoadMask, StatedValue, Typography, useOnEnterHandler} from 'tocco-ui'

import {generateVisibilityStatusCode} from '../../utils/visibilityStatusCode'

import {StyledButtonWrapper, StyledWidgetCode} from './StyledComponents'

const VisibilityStatusCodeCopy = ({
  fetchWidgetConfig,
  widgetConfigKey,
  visibilityStatus,
  copyVisibilityStatusCode,
  intl
}) => {
  const [selectedStatus, setSelectedStatus] = useState([])

  useEffect(() => {
    fetchWidgetConfig()
  }, [fetchWidgetConfig])

  const options =
    visibilityStatus?.map(val => ({
      key: val.paths.visibility_status.value,
      display: val.paths.label.value
    })) ?? []

  const selectedStates = selectedStatus.map(d => d.key)
  const msg = id => intl.formatMessage({id})
  const handleClick = () => copyVisibilityStatusCode(selectedStates)
  useOnEnterHandler(handleClick)

  return (
    <LoadMask required={[visibilityStatus]}>
      {visibilityStatus?.length > 0 ? (
        <>
          <Typography.P>{msg('client.widget-visibility-status-code-copy.infoText')}</Typography.P>
          <StatedValue
            label={msg('client.widget-visibility-status-code-copy.visibilityStatusLabel')}
            labelPosition="inside"
          >
            <EditableValue
              type="multi-select"
              value={selectedStatus}
              options={{options}}
              events={{onChange: setSelectedStatus}}
              id="visibility-status"
            />
          </StatedValue>
          {selectedStatus.length > 0 && (
            <>
              <StyledWidgetCode>{generateVisibilityStatusCode(widgetConfigKey, selectedStates)}</StyledWidgetCode>
              <StyledButtonWrapper>
                <Button type="button" onClick={handleClick} ink="primary" look="raised">
                  <FormattedMessage id="client.widget-visibility-status-code-copy.copy" />
                </Button>
              </StyledButtonWrapper>
            </>
          )}
        </>
      ) : (
        <Typography.P>
          <FormattedMessage id="client.widget-visibility-status-code-copy.noVisibilityStatus" />
        </Typography.P>
      )}
    </LoadMask>
  )
}

VisibilityStatusCodeCopy.propTypes = {
  intl: PropTypes.object.isRequired,
  fetchWidgetConfig: PropTypes.func.isRequired,
  copyVisibilityStatusCode: PropTypes.func.isRequired,
  widgetConfigKey: PropTypes.string,
  visibilityStatus: PropTypes.array
}

export default VisibilityStatusCodeCopy
