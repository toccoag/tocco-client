export const generateVisibilityStatusCode = (widgetConfigKey, visibilityStatus) => {
  const states = visibilityStatus ? visibilityStatus.join(' ') : ''
  // eslint-disable-next-line max-len
  const visibilityStatusCode = `<div data-tocco-widget-ref="${widgetConfigKey}" data-tocco-visibility-status="${states}"></div>`
  return visibilityStatusCode
}
