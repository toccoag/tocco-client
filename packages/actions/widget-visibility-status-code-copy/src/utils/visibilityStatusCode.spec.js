import {generateVisibilityStatusCode} from './visibilityStatusCode'

const trim = val => val.replace(/\s/g, '')

describe('widget-visibility-status-code-copy', () => {
  describe('utils', () => {
    describe('visibilityStatusCode', () => {
      describe('generateVisibilityStatusCode', () => {
        test('should set correct widget-ref', () => {
          const widgetConfigKey = '1'
          const visibilityStatus = []

          const expectedWidgetCode = trim(`
            <div data-tocco-widget-ref="1" data-tocco-visibility-status=""></div>
          `)

          expect(trim(generateVisibilityStatusCode(widgetConfigKey, visibilityStatus))).to.equal(expectedWidgetCode)
        })

        test('should ignore unset visibilityStatus', () => {
          const widgetConfigKey = '1'
          const visibilityStatus = undefined

          const expectedWidgetCode = trim(`
            <div data-tocco-widget-ref="1" data-tocco-visibility-status=""></div>
          `)

          expect(trim(generateVisibilityStatusCode(widgetConfigKey, visibilityStatus))).to.equal(expectedWidgetCode)
        })

        test('should set one visibilityStatus', () => {
          const widgetConfigKey = '1'
          const visibilityStatus = ['list']

          const expectedWidgetCode = trim(`
            <div data-tocco-widget-ref="1" data-tocco-visibility-status="list"></div>
          `)

          expect(trim(generateVisibilityStatusCode(widgetConfigKey, visibilityStatus))).to.equal(expectedWidgetCode)
        })

        test('should set multiple visibilityStatus white-space separated', () => {
          const widgetConfigKey = '1'
          const visibilityStatus = ['list', 'detail', 'search']

          const expectedWidgetCode = trim(`
            <div data-tocco-widget-ref="1" data-tocco-visibility-status="list detail search"></div>
          `)

          expect(trim(generateVisibilityStatusCode(widgetConfigKey, visibilityStatus))).to.equal(expectedWidgetCode)
        })
      })
    })
  })
})
