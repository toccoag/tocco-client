import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {selection as selectionUtil, form} from 'tocco-app-extensions'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {LoadMask} from 'tocco-ui'

import Form from './FormContainer'

const ApplicationForm = ({
  selection,
  formBase,
  fireExternalEvent,
  formDefinition,
  fieldDefinitions,
  formInitialValues,
  loadFormDefinition,
  locale
}) => {
  useEffect(() => {
    fireExternalEvent('onApplicationForm')
  }, [fireExternalEvent])
  useEffect(() => {
    loadFormDefinition()
  }, [loadFormDefinition])

  const jobOfferKey = selectionUtil.getSingleKey(selection, 'Job_offer')

  const handleAsyncValidate = form.hooks.useAsyncValidation({
    formInitialValues,
    fieldDefinitions,
    formDefinition,
    mode: 'create'
  })
  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  return (
    <LoadMask required={[formDefinition, fieldDefinitions]}>
      <EntityDetailApp
        entityName="Job_offer"
        formName={formBase}
        entityId={jobOfferKey}
        mode="update"
        locale={locale}
      />
      <Form
        validate={handleSyncValidate}
        asyncValidate={handleAsyncValidate}
        asyncBlurFields={fieldDefinitions ? form.getUsedPaths(fieldDefinitions).map(form.transformFieldName) : null}
      />
    </LoadMask>
  )
}

ApplicationForm.propTypes = {
  selection: selectionUtil.propType.isRequired,
  formBase: PropTypes.string.isRequired,
  formDefinition: PropTypes.object,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object),
  formInitialValues: PropTypes.object,
  loadFormDefinition: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default ApplicationForm
