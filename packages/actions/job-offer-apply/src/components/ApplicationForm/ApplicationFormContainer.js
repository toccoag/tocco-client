import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'
import {externalEvents} from 'tocco-app-extensions'

import {loadFormDefinition} from '../../modules/jobOfferApply/actions'

import ApplicationForm from './ApplicationForm'
import {REDUX_FORM_NAME} from './Form'

const mapActionCreators = {
  loadFormDefinition,
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  selection: state.input.selection,
  formBase: state.input.actionProperties.formBase,
  formDefinition: state.jobOfferApply.formDefinition,
  fieldDefinitions: state.jobOfferApply.fieldDefinitions,
  locale: state.intl.locale,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ApplicationForm))
