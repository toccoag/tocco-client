import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form} from 'tocco-app-extensions'

import {sendApplication} from '../../modules/jobOfferApply/actions'

import Form, {REDUX_FORM_NAME} from './Form'

const mapActionCreators = {
  sendApplication
}

const mapStateToProps = state => ({
  formDefinition: state.jobOfferApply.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Form))
