import {reducer as form} from 'redux-form'

import jobOfferApplyReducer from './jobOfferApply/reducer'
import jobOfferApplySagas from './jobOfferApply/sagas'

export default {
  jobOfferApply: jobOfferApplyReducer,
  form
}

export const sagas = [jobOfferApplySagas]
