import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, form, externalEvents, notification, selection as selectionUtil} from 'tocco-app-extensions'
import {api, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ApplicationForm'

import * as actions from './actions'

export const jobOfferApplySelector = state => state.jobOfferApply
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([
    takeLatest(actions.LOAD_FORM_DEFINITION, loadFormDefinition),
    takeLatest(actions.SEND_APPLICATION, sendApplication)
  ])
}

export function* loadFormDefinition() {
  const {selection} = yield select(inputSelector)
  const jobOfferKey = selectionUtil.getSingleKey(selection, 'Job_offer')
  const formDefinition = {
    ...(yield call(rest.fetchForm, 'Job_application', 'create')),
    createEndpoint: `/widgets/jobOfferApply/${jobOfferKey}`
  }

  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)

  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const entityValues = yield call(api.getFlattenEntity, {model: 'Application'})
  const formValues = yield call(form.entityToFormValues, {...entityValues, ...defaultValues}, fieldDefinitions)
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))

  yield all([put(actions.setFormDefinition(formDefinition)), put(actions.setFieldDefinitions(fieldDefinitions))])
}

export function* sendApplication() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {fieldDefinitions} = yield select(jobOfferApplySelector)
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const application = yield call(api.toEntity, flatEntity)

  const {selection} = yield select(inputSelector)
  const jobOfferKey = selectionUtil.getSingleKey(selection, 'Job_offer')
  const response = yield call(rest.requestSaga, `widgets/jobOfferApply/${jobOfferKey}`, {
    method: 'POST',
    body: application,
    acceptedErrorCodes: ['VALIDATION_FAILED']
  })
  if (response.status === 201) {
    yield put(externalEvents.fireExternalEvent('onSuccess'))
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    if (response.body.errorCode === 'VALIDATION_FAILED') {
      yield put(
        notification.toaster({
          type: 'error',
          title: 'client.component.actions.validationError',
          body: validation.getErrorCompact(response.body.errors)
        })
      )
    } else {
      yield put(externalEvents.fireExternalEvent('onError'))
    }
  }
}
