export const LOAD_FORM_DEFINITION = 'jobOfferApply/LOAD_FORM_DEFINITION'
export const SET_FORM_DEFINITION = 'jobOfferApply/SET_FORM_DEFINITION'
export const SET_FIELD_DEFINITIONS = 'jobOfferApply/SET_FIELD_DEFINITIONS'
export const SEND_APPLICATION = 'jobOfferApply/SEND_APPLICATION'

export const loadFormDefinition = () => ({
  type: LOAD_FORM_DEFINITION
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const sendApplication = () => ({
  type: SEND_APPLICATION
})
