import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  formDefinition: null,
  fieldDefinitions: null
}

const ACTION_HANDLERS = {
  [actions.SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [actions.SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
