import {actions as formActions} from 'redux-form'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, env, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ApplicationForm'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('job-offer-apply', () => {
  describe('modules', () => {
    describe('jobOfferApply', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_FORM_DEFINITION, sagas.loadFormDefinition),
                takeLatest(actions.SEND_APPLICATION, sagas.sendApplication)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadFormDefinition', () => {
          test('should load form and field definitions and initialize form', () => {
            const key = '1'
            const selection = {entityName: 'Job_offer', type: 'ID', ids: [key]}
            const formDefinition = {}
            const expectedFormDefinition = {createEndpoint: `/widgets/jobOfferApply/${key}`}
            const fieldDefinitions = [{id: 'field_1'}, {id: 'field_2'}]
            const defaultValues = {field_1: 'value 1'}
            const entityValues = {field_2: 'value 2'}
            const formValues = {field: 'value'}
            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.fetchForm, 'Job_application', 'create'), formDefinition],
                [matchers.call(form.getFieldDefinitions, expectedFormDefinition), fieldDefinitions],
                [matchers.call(form.getDefaultValues, fieldDefinitions), defaultValues],
                [matchers.call(api.getFlattenEntity, {model: 'Application'}), entityValues],
                [
                  matchers.call(form.entityToFormValues, {...entityValues, ...defaultValues}, fieldDefinitions),
                  formValues
                ]
              ])
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .put(actions.setFormDefinition(expectedFormDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .run()
          })
        })

        describe('sendApplication', () => {
          const key = '1'
          const selection = {entityName: 'Job_offer', type: 'ID', ids: [key]}
          const fieldDefinitions = [{id: 'field'}]
          const flatEntity = {
            field: 'value'
          }
          const application = {
            pk: null,
            field: 'value'
          }
          const widgetConfigKey = '456'
          env.setWidgetConfigKey(widgetConfigKey)

          test('should send application', () => {
            const response = {status: 201}
            return expectSaga(sagas.sendApplication)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [select(sagas.jobOfferApplySelector), {fieldDefinitions}],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, `widgets/jobOfferApply/${key}`, {
                method: 'POST',
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(externalEvents.fireExternalEvent('onSuccess'))
              .run()
          })

          test('should handle validation', () => {
            const response = {status: 400, body: {errorCode: 'VALIDATION_FAILED', errors: []}}
            return expectSaga(sagas.sendApplication)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [select(sagas.jobOfferApplySelector), {fieldDefinitions}],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, `widgets/jobOfferApply/${key}`, {
                method: 'POST',
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: validation.getErrorCompact(response.body.errors)
                })
              )
              .run()
          })

          test('should handle error', () => {
            const response = {status: 400, body: {}}
            return expectSaga(sagas.sendApplication)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [select(sagas.jobOfferApplySelector), {fieldDefinitions}],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, `widgets/jobOfferApply/${key}`, {
                method: 'POST',
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(externalEvents.fireExternalEvent('onError'))
              .run()
          })
        })
      })
    })
  })
})
