import PropTypes from 'prop-types'
import {appFactory, selection, externalEvents, actionEmitter, notification, formData} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, appContext} from 'tocco-util'

import ApplicationForm from './components/ApplicationForm'
import reducers, {sagas} from './modules/reducers'

const packageName = 'job-offer-apply'
const EXTERNAL_EVENTS = [
  'onSuccess',
  /**
   * Is fired when the application form is rendered
   */
  'onApplicationForm',
  'emitAction'
]

const initApp = (id, input, events, publicPath) => {
  const content = <ApplicationForm />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({listApp: EntityListApp}))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const JobOfferApplyApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

JobOfferApplyApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  /**
   * Selection of a single `Application` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired,
  actionProperties: PropTypes.shape({
    formBase: PropTypes.string.isRequired
  })
}

export default JobOfferApplyApp
