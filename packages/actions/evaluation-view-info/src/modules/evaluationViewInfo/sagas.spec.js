import {all, takeLatest, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view-info', () => {
  describe('modules', () => {
    describe('evaluationViewInfo', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeLatest(actions.INITIALIZE_INFORMATION, sagas.initialize)])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('initialize', () => {
          test('should load info', () => {
            const selection = {}
            const fields = []
            const body = {fields}
            return expectSaga(sagas.initialize)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call.fn(rest.requestSaga), {body}]
              ])
              .call(rest.requestSaga, 'evaluationView/information', {method: 'POST', body: selection})
              .put(actions.setInformation(fields))
              .run()
          })
        })
      })
    })
  })
})
