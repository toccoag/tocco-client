export const INITIALIZE_INFORMATION = 'evaluationViewInfo/INITIALIZE_INFORMATION'
export const SET_INFORMATION = 'evaluationViewInfo/SET_INFORMATION'

export const initializeInformation = () => ({
  type: INITIALIZE_INFORMATION
})

export const setInformation = information => ({
  type: SET_INFORMATION,
  payload: {information}
})
