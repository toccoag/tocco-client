import evaluationViewInfoReducer, {sagas as evaluationViewInfoSaga} from './evaluationViewInfo'

export default {
  evaluationViewInfo: evaluationViewInfoReducer
}

export const sagas = [evaluationViewInfoSaga]
