import {appFactory, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import EvaluationViewInformation from './components/EvaluationViewInformation'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'evaluation-view-info'

const initApp = (id, input, events, publicPath) => {
  const content = <EvaluationViewInformation />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(),
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EvaluationViewInfoApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

EvaluationViewInfoApp.propTypes = {
  /**
   * Selection of one or multiple `Evaluation_data`
   */
  selection: selection.propType.isRequired
}

export default EvaluationViewInfoApp
