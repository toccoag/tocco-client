import {initializeInformation} from './modules/evaluationViewInfo/actions'

export const getDispatchActions = () => [initializeInformation()]
