import {connect} from 'react-redux'

import EvaluationViewInformation from './EvaluationViewInformation'

const mapStateToProps = state => ({
  information: state.evaluationViewInfo.information
})

export default connect(mapStateToProps)(EvaluationViewInformation)
