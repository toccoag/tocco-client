import acceptConflict, {sagas as acceptConflictSagas} from './acceptConflict'

export default {
  acceptConflict
}

export const sagas = [acceptConflictSagas]
