import * as actions from './actions'
import reducer from './reducer'

const INITIAL_STATE = {
  conflicts: undefined
}

describe('accept-conflict', () => {
  describe('modules', () => {
    describe('acceptConflict', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(INITIAL_STATE)
        })

        describe('setAccepted', () => {
          test('should change status of certain ids', () => {
            const stateBefore = {
              conflicts: [
                {
                  source: 'Raum: Halle 22',
                  firstName: 'AAA1',
                  secondName: 'AAA3',
                  time: {firstItem: '19.11.24 09:38 - 24.11.24 09:38', secondItem: '21.11.24 12:59 - 30.11.24 12:59'},
                  status: true,
                  id: 'd903d32b-8b2a-4b05-aa39-9f69a1c8293f',
                  index: 0,
                  firstCalendarEventKey: '222746',
                  secondCalendarEventKey: '222756'
                }
              ]
            }

            const ids = ['d903d32b-8b2a-4b05-aa39-9f69a1c8293f']
            const isAccepted = false

            const expectedStateAfter = {
              ...stateBefore,
              conflicts: stateBefore.conflicts.map(c => ({...c, status: false}))
            }

            expect(reducer(stateBefore, actions.updateConflict(ids, isAccepted))).to.deep.equal(expectedStateAfter)
          })
        })
      })
    })
  })
})
