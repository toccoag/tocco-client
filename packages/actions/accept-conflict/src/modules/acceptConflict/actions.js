export const GET_CONFLICTS = 'acceptConflict/GET_CONFLICTS'
export const SET_CONFLICTS = 'acceptConflict/SET_CONFLICTS'
export const UPDATE_CONFLICT = 'acceptConflict/UPDATE_CONFLICT'

export const getConflicts = () => ({
  type: GET_CONFLICTS
})

export const setConflicts = conflicts => ({
  type: SET_CONFLICTS,
  payload: {conflicts}
})

export const updateConflict = (ids, isAccepted) => ({
  type: UPDATE_CONFLICT,
  payload: {ids, isAccepted}
})
