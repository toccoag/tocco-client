import {all, call, put, takeEvery, select} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input
export const acceptConflictSelector = state => state.acceptConflict

export default function* sagas() {
  yield all([takeEvery(actions.GET_CONFLICTS, loadConflicts), takeEvery(actions.UPDATE_CONFLICT, updateConflict)])
}

export function* loadConflicts() {
  const {selection} = yield select(inputSelector)
  try {
    const response = yield call(rest.requestSaga, '/reservation/actions/acceptConflict/loadConflicts', {
      method: 'POST',
      body: selection
    })
    const returnValue = mapToConflicts(response)

    yield put(actions.setConflicts(returnValue))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.accept-conflict.loadConflictsFailure.title',
        body: 'client.actions.accept-conflict.loadConflictsFailure.info'
      })
    )
  }
}

const mapToConflicts = response => {
  return response.body.map((item, index) => {
    return {
      source: item.conflictItemModel + ': ' + item.conflictItem,
      firstName: item.firstItem.name,
      secondName: item.secondItem.name,
      time: {
        firstItem: item.firstItem.timeFrom + ' - ' + item.firstItem.timeTo,
        secondItem: item.secondItem.timeFrom + ' - ' + item.secondItem.timeTo
      },
      status: item.accepted,
      id: item.id,
      index,
      firstLinkEntityModel: item.firstItem.entityModel,
      firstLinkEntityKey: item.firstItem.entityKey,
      secondLinkEntityModel: item.secondItem.entityModel,
      secondCalendarEventKey: item.secondItem.entityKey,
      __key: item.id
    }
  })
}

export function* updateConflict({payload: {ids, isAccepted}}) {
  try {
    yield call(rest.requestSaga, '/reservation/actions/acceptConflict/updateConflicts', {
      method: 'POST',
      body: {ids, accepted: isAccepted}
    })
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.accept-conflict.updateConflictFailure.title',
        body: 'client.actions.accept-conflict.updateConflictFailure.info'
      })
    )
  }
}
