import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  conflicts: undefined
}

const updateConflict = (state, {payload: {ids, isAccepted}}) => ({
  ...state,
  conflicts: state.conflicts.map(c => ({
    ...c,
    status: ids.includes(c.id) ? isAccepted : c.status
  }))
})

const ACTION_HANDLERS = {
  [actions.SET_CONFLICTS]: reducerUtil.singleTransferReducer('conflicts'),
  [actions.UPDATE_CONFLICT]: updateConflict
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
