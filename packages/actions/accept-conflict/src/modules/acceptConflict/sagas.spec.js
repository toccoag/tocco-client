import {select, takeEvery, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('accept-conflict', () => {
  describe('modules', () => {
    describe('acceptConflict', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeEvery(actions.GET_CONFLICTS, sagas.loadConflicts),
                takeEvery(actions.UPDATE_CONFLICT, sagas.updateConflict)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadConflicts', () => {
          const selection = {
            entityName: 'Reservation',
            type: 'ID',
            ids: ['12828'],
            query: {
              sort: 'update_timestamp desc'
            },
            count: 1
          }
          const resource = '/reservation/actions/acceptConflict/loadConflicts'
          const options = {
            method: 'POST',
            body: selection
          }
          const body = [
            {
              id: 'd903d32b-8b2a-4b05-aa39-9f69a1c8293f',
              conflictItem: 'Halle 22',
              conflictItemModel: 'Raum',
              accepted: true,
              firstItem: {
                id: 'ec511b5c-7f7d-4910-9d61-550dea8d0a8a',
                name: 'AAA1',
                timeFrom: '19.11.24 09:38',
                timeTo: '24.11.24 09:38',
                entityModel: 'Reservation',
                entityKey: '222746'
              },
              secondItem: {
                id: '9ecbf225-04e8-4839-8333-d8c39fb337d1',
                name: 'AAA3',
                timeFrom: '21.11.24 12:59',
                timeTo: '30.11.24 12:59',
                entityModel: 'Calendar_event',
                entityKey: '222756'
              }
            }
          ]

          const returnValue = [
            {
              source: 'Raum: Halle 22',
              firstName: 'AAA1',
              secondName: 'AAA3',
              time: {firstItem: '19.11.24 09:38 - 24.11.24 09:38', secondItem: '21.11.24 12:59 - 30.11.24 12:59'},
              status: true,
              id: 'd903d32b-8b2a-4b05-aa39-9f69a1c8293f',
              index: 0,
              firstLinkEntityModel: 'Reservation',
              firstLinkEntityKey: '222746',
              secondLinkEntityModel: 'Calendar_event',
              secondCalendarEventKey: '222756',
              __key: 'd903d32b-8b2a-4b05-aa39-9f69a1c8293f'
            }
          ]

          test('Should get the data from the backend and pass it to setConflicts', () => {
            return expectSaga(sagas.loadConflicts)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.requestSaga, resource, options), {body}]
              ])
              .put(actions.setConflicts(returnValue))
              .run()
          })
        })

        describe('updateConflicts', () => {
          const ids = ['d903d32b-8b2a-4b05-aa39-9f69a1c8293f']
          const isAccepted = false
          const data = {
            ids,
            accepted: isAccepted
          }

          const resource = '/reservation/actions/acceptConflict/updateConflicts'
          const options = {
            method: 'POST',
            body: data
          }

          test('should change the value accepted in the backend and the state', () => {
            return expectSaga(sagas.updateConflict, {payload: {ids, isAccepted}})
              .provide([[matchers.call(rest.requestSaga, resource, options), {}]])
              .run()
          })
        })
      })
    })
  })
})
