import PropTypes from 'prop-types'
import {appFactory, selection, actionEmitter, externalEvents, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger, env} from 'tocco-util'

import AcceptConflict from './components/AcceptConflict'
import reducers, {sagas} from './modules/reducers'
const packageName = 'accept-conflict'

const EXTERNAL_EVENTS = ['emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <AcceptConflict />

  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  actionEmitter.addToStore(store, state => state.input.emitAction)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const AcceptConflictApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

AcceptConflictApp.propTypes = {
  selection: selection.propType.isRequired,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}

export default AcceptConflictApp
