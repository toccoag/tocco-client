import styled from 'styled-components'

export const StyledTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`
export const StyledButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
`
