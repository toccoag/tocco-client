import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Table, Button} from 'tocco-ui'

import createColumns from './columns'
import {StyledTableWrapper, StyledButtonWrapper} from './StyledComponents'

function ConflictsTable({updateConflict, conflictData, intl, navigationStrategy}) {
  const getAllIds = () => {
    return conflictData.map(el => el.id)
  }

  const handleAcceptAll = () => {
    updateConflict(getAllIds(), true)
  }

  const handleDenyAll = () => {
    updateConflict(getAllIds(), false)
  }

  return (
    <>
      <StyledButtonWrapper>
        <Button onClick={handleAcceptAll} look="raised">
          <FormattedMessage id={'client.actions.accept-conflict.acceptAllButton.label'} />
        </Button>
        <Button onClick={handleDenyAll} look="raised">
          <FormattedMessage id={'client.actions.accept-conflict.denyAllButton.label'} />
        </Button>
      </StyledButtonWrapper>
      <StyledTableWrapper>
        {conflictData && (
          <Table
            scrollBehaviour="none"
            columns={createColumns(intl, navigationStrategy, updateConflict)}
            data={conflictData}
            onColumnPositionChange={() => {}}
            disableVirtualTable
          />
        )}
      </StyledTableWrapper>
    </>
  )
}

ConflictsTable.propTypes = {
  updateConflict: PropTypes.func.isRequired,
  conflictData: PropTypes.arrayOf(
    PropTypes.shape({
      source: PropTypes.string.isRequired,
      firstName: PropTypes.string.isRequired,
      secondName: PropTypes.string.isRequired,
      time: PropTypes.shape({
        firstItem: PropTypes.string.isRequired,
        secondItem: PropTypes.string.isRequired
      }),
      status: PropTypes.bool.isRequired,
      id: PropTypes.string.isRequired,
      index: PropTypes.number,
      firstCalendarEventKey: PropTypes.string,
      secondCalendarEventKey: PropTypes.string
    })
  ),
  intl: PropTypes.object.isRequired,
  navigationStrategy: PropTypes.object.isRequired
}

export default ConflictsTable
