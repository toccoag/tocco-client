/* eslint-disable react/prop-types */
import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {field} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

const FieldComponent = ({type, value}) => {
  const Field = field.factory('readOnly', type)
  return <Field formField={{dataType: type}} value={value} mappingType="list" />
}

FieldComponent.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.any.isRequired
}

const createColumns = (intl, navigationStrategy, updateConflict) => {
  return [
    {
      idx: 0,
      id: 'source',
      dynamic: true,
      sorting: {sortable: false},
      label: intl.formatMessage({id: 'client.actions.accept-conflict.source'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => <FieldComponent type={'string'} value={rowData.source} />
    },
    {
      idx: 1,
      id: 'name',
      dynamic: true,
      sorting: {sortable: false},
      label: intl.formatMessage({id: 'client.actions.accept-conflict.name'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => {
        return (
          <>
            <navigationStrategy.DetailLink
              entityName={rowData.firstLinkEntityModel}
              entityKey={rowData.firstLinkEntityKey}
            >
              <FieldComponent type={'string'} value={rowData.firstName} />
            </navigationStrategy.DetailLink>
            <br />
            <navigationStrategy.DetailLink
              entityName={rowData.secondLinkEntityModel}
              entityKey={rowData.secondCalendarEventKey}
            >
              <FieldComponent type={'string'} value={rowData.secondName} />
            </navigationStrategy.DetailLink>
          </>
        )
      }
    },
    {
      idx: 2,
      id: 'time',
      shrinkToContent: true,
      dynamic: true,
      sorting: {sortable: false},
      label: intl.formatMessage({id: 'client.actions.accept-conflict.time'}),
      alignment: 'left',
      CellRenderer: props => (
        <>
          <FieldComponent type={'string'} value={props.rowData.time.firstItem} />
          <br />
          <FieldComponent type={'string'} value={props.rowData.time.secondItem} />
        </>
      )
    },
    {
      idx: 3,
      id: 'status',
      dynamic: true,
      sorting: {sortable: false},
      label: intl.formatMessage({id: 'client.actions.accept-conflict.status'}),
      alignment: 'left',
      CellRenderer: props => (
        <FieldComponent
          type={'string'}
          value={
            props.rowData.status
              ? intl.formatMessage({id: 'client.actions.accept-conflict.status.true'})
              : intl.formatMessage({id: 'client.actions.accept-conflict.status.false'})
          }
        />
      )
    },
    {
      idx: 4,
      id: 'status-btn',
      shrinkToContent: true,
      dynamic: true,
      sorting: {sortable: false},
      label: intl.formatMessage({id: 'client.actions.accept-conflict.status-btn'}),
      alignment: 'left',
      CellRenderer: ({rowData}) => (
        <Button
          look="raised"
          onClick={() => {
            updateConflict([rowData.id], !rowData.status)
          }}
        >
          {rowData.status ? (
            <FormattedMessage id={'client.actions.accept-conflict.status-btn.true'} />
          ) : (
            <FormattedMessage id={'client.actions.accept-conflict.status-btn.false'} />
          )}
        </Button>
      )
    }
  ]
}

export default createColumns
