import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {updateConflict} from '../../modules/acceptConflict/actions'

import ConflictsTable from './ConflictsTable'

const mapActionCreators = {
  updateConflict
}

const mapStateToProps = state => ({
  conflictData: state.acceptConflict.conflicts,
  navigationStrategy: state.input.navigationStrategy
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ConflictsTable))
