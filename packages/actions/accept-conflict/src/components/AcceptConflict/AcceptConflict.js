import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'

import ConflictsTable from '../ConflictsTable'

function AcceptConflict({getConflicts, conflictData}) {
  useEffect(() => {
    getConflicts()
  }, [getConflicts])

  return (
    <LoadMask required={[conflictData]}>
      <ConflictsTable />
    </LoadMask>
  )
}

AcceptConflict.propTypes = {
  getConflicts: PropTypes.func.isRequired,
  conflictData: PropTypes.arrayOf(PropTypes.object)
}

export default AcceptConflict
