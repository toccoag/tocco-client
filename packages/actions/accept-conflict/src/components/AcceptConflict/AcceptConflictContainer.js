import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {getConflicts} from '../../modules/acceptConflict/actions'

import AcceptConflict from './AcceptConflict'

const mapActionCreators = {
  getConflicts
}

const mapStateToProps = state => ({
  conflictData: state.acceptConflict.conflicts
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(AcceptConflict))
