import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'
import {navigationStrategy as navigationStrategyUtil} from 'tocco-util'

import ViewContent from './ViewContent'

const ConfigureAfterAutomationLogic = ({
  specificEntityId,
  createEndpoint,
  initialize,
  fireSuccess,
  emitAction,
  listApp,
  navigationStrategy
}) => {
  useEffect(() => {
    initialize()
  }, [initialize])

  return (
    <LoadMask required={[specificEntityId]}>
      <ViewContent
        specificEntityId={specificEntityId}
        createEndpoint={createEndpoint}
        emitAction={emitAction}
        listApp={listApp}
        fireSuccess={fireSuccess}
        navigationStrategy={navigationStrategy}
      />
    </LoadMask>
  )
}

ConfigureAfterAutomationLogic.propTypes = {
  specificEntityId: PropTypes.shape({
    entityName: PropTypes.string,
    key: PropTypes.string
  }),
  createEndpoint: PropTypes.string,
  navigationStrategy: navigationStrategyUtil.propTypes.isRequired,
  initialize: PropTypes.func.isRequired,
  fireSuccess: PropTypes.func.isRequired,
  emitAction: PropTypes.func.isRequired,
  docsApp: PropTypes.func,
  listApp: PropTypes.func
}

export default ConfigureAfterAutomationLogic
