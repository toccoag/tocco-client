import PropTypes from 'prop-types'
import {form} from 'tocco-app-extensions'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {navigationStrategy as navigationStrategyUtil} from 'tocco-util'

const ViewContent = ({specificEntityId, createEndpoint, fireSuccess, emitAction, listApp, navigationStrategy}) => (
  <EntityDetailApp
    entityName={specificEntityId.entityName}
    formName={specificEntityId.entityName}
    entityId={specificEntityId.key}
    mode={specificEntityId.key ? 'update' : 'create'}
    onEntityCreated={fireSuccess}
    onEntityUpdated={fireSuccess}
    emitAction={emitAction}
    listApp={listApp}
    navigationStrategy={navigationStrategy}
    labelPosition="inside"
    hideFooter={true}
    modifyFormDefinition={formDefinition => ({
      ...form.removeActions(formDefinition, ['new', 'copy', 'delete']),
      createEndpoint
    })}
  />
)

ViewContent.propTypes = {
  specificEntityId: PropTypes.shape({
    entityName: PropTypes.string,
    key: PropTypes.string
  }),
  createEndpoint: PropTypes.string,
  navigationStrategy: navigationStrategyUtil.propTypes.isRequired,
  fireSuccess: PropTypes.func,
  emitAction: PropTypes.func,
  listApp: PropTypes.func
}

export default ViewContent
