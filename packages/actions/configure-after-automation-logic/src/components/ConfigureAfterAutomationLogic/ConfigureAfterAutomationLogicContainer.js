import {connect} from 'react-redux'
import {actionEmitter} from 'tocco-app-extensions'

import {initialize, fireSuccess} from '../../modules/configureAfterAutomationLogic/actions'

import ConfigureAfterAutomationLogic from './ConfigureAfterAutomationLogic'

const mapActionCreators = {
  initialize,
  fireSuccess,
  emitAction: action => actionEmitter.dispatchEmittedAction(action)
}

const mapStateToProps = state => ({
  specificEntityId: state.configureAfterAutomationLogic.specificEntityId,
  createEndpoint: state.configureAfterAutomationLogic.createEndpoint,
  listApp: state.input.listApp,
  navigationStrategy: state.input.navigationStrategy
})

export default connect(mapStateToProps, mapActionCreators)(ConfigureAfterAutomationLogic)
