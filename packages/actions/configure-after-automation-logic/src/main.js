import PropTypes from 'prop-types'
import {appFactory, selection, externalEvents, actionEmitter, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, navigationStrategy as navigationStrategyUtil, consoleLogger} from 'tocco-util'

import ConfigureAfterAutomationLogic from './components/ConfigureAfterAutomationLogic'
import reducers, {sagas} from './modules/reducers'

const packageName = 'configure-after-automation-logic'
const EXTERNAL_EVENTS = ['onSuccess', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <ConfigureAfterAutomationLogic />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  notification.addToStore(store, false)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const ConfigureAfterAutomationLogicApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

ConfigureAfterAutomationLogicApp.propTypes = {
  selection: selection.propType.isRequired,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {}),
  /**
   * List app (tocco-entity-list) must be provided to support remote fields
   */
  listApp: PropTypes.func,
  navigationStrategy: navigationStrategyUtil.propTypes.isRequired
}

export default ConfigureAfterAutomationLogicApp
