import configureAfterAutomationLogicReducer, {
  sagas as configureAfterAutomationLogicSagas
} from './configureAfterAutomationLogic'

export default {
  configureAfterAutomationLogic: configureAfterAutomationLogicReducer
}

export const sagas = [configureAfterAutomationLogicSagas]
