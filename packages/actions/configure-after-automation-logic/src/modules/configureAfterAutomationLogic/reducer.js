import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_SPECIFIC_ENTITY_ID]: reducerUtil.singleTransferReducer('specificEntityId'),
  [actions.SET_CREATE_ENDPOINT]: reducerUtil.singleTransferReducer('createEndpoint')
}

const initialState = {
  specificEntityId: undefined,
  createEndpoint: undefined
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
