export const INITIALIZE = 'configureAfterAutomationLogic/INITIALIZE'
export const SET_SPECIFIC_ENTITY_ID = 'configureAfterAutomationLogic/SET_SPECIFIC_ENTITY_ID'
export const SET_CREATE_ENDPOINT = 'configureAfterAutomationLogic/SET_CREATE_ENDPOINT'
export const FIRE_SUCCESS = 'configureAfterAutomationLogic/FIRE_SUCCESS'

export const initialize = () => ({
  type: INITIALIZE
})

export const setSpecificEntityId = specificEntityId => ({
  type: SET_SPECIFIC_ENTITY_ID,
  payload: {
    specificEntityId
  }
})

export const setCreateEndpoint = createEndpoint => ({
  type: SET_CREATE_ENDPOINT,
  payload: {
    createEndpoint
  }
})

export const fireSuccess = () => ({
  type: FIRE_SUCCESS
})
