import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import {externalEvents, rest, selection as selectionUtil} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(actions.INITIALIZE, initialize), takeEvery(actions.FIRE_SUCCESS, fireSuccess)])
}

export function* initialize() {
  const {selection} = yield select(inputSelector)
  const key = selectionUtil.getSingleKey(selection, 'After_automation_logic')

  const endpoint = `automation/actions/configureAfterAutomationLogic/${key}`

  const {body} = yield call(rest.requestSaga, endpoint, {method: 'GET'})
  yield put(actions.setSpecificEntityId(body))
  yield put(actions.setCreateEndpoint(endpoint))
}

export function* fireSuccess() {
  yield put(externalEvents.fireExternalEvent('onSuccess', {title: null})) // disable success toaster
}
