import {all, select, takeEvery} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {externalEvents, rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('configure-after-automation-logic', () => {
  describe('modules', () => {
    describe('detailView', () => {
      describe('sagas', () => {
        describe('root saga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeEvery(actions.INITIALIZE, sagas.initialize), takeEvery(actions.FIRE_SUCCESS, sagas.fireSuccess)])
            )
            expect(generator.next().done).to.equal(true)
          })
        })

        describe('initialize', () => {
          test('initialize action', () => {
            const selection = {
              entityName: 'After_automation_logic',
              type: 'ID',
              ids: ['123']
            }
            const entityId = [
              {
                entityName: 'After_automation_logic_date_now',
                key: '2'
              }
            ]

            return expectSaga(sagas.initialize)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [
                  matchers.call(rest.requestSaga, 'automation/actions/configureAfterAutomationLogic/123', {
                    method: 'GET'
                  }),
                  {body: entityId}
                ]
              ])
              .put(actions.setSpecificEntityId(entityId))
              .put(actions.setCreateEndpoint('automation/actions/configureAfterAutomationLogic/123'))
              .run()
          })
        })

        describe('fireSuccess', () => {
          test('should fire success event', () => {
            return expectSaga(sagas.fireSuccess)
              .put(externalEvents.fireExternalEvent('onSuccess', {title: null}))
              .run()
          })
        })
      })
    })
  })
})
