import styled from 'styled-components'

export const StyledWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 10px;
  overflow: auto;
  margin: 10px;
`
