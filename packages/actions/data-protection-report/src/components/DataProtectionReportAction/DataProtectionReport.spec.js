import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DataProtectionReport from './DataProtectionReport'

describe('data-protection-report', () => {
  describe('components', () => {
    describe('Action', () => {
      test('should render data extract of the selected user', () => {
        const loadSpy = sinon.spy()

        testingLibrary.renderWithIntl(
          <DataProtectionReport
            dataProtectionReport={{
              entityData: {
                User: [
                  {
                    modelLabel: 'User',
                    defaultDisplay: 'User 1',
                    values: [
                      {fieldLabel: 'firstname', fieldValue: 'testname', type: 'text'},
                      {fieldLabel: 'candidate_number', fieldValue: 12, type: 'integer'}
                    ]
                  }
                ],

                Address: [
                  {
                    modelLabel: 'Address',
                    defaultDisplay: 'Address 1',
                    values: [
                      {fieldLabel: 'canton', fieldValue: 'aargau', type: 'text'},
                      {fieldLabel: 'address_nr', fieldValue: 15, type: 'integer'}
                    ]
                  },
                  {
                    modelLabel: 'Address',
                    defaultDisplay: 'Address 2',
                    values: [
                      {fieldLabel: 'canton', fieldValue: 'zürich', type: 'text'},
                      {fieldLabel: 'address_nr', fieldValue: 18, type: 'integer'}
                    ]
                  }
                ]
              }
            }}
            loadDataProtectionReport={loadSpy}
          />
        )

        expect(loadSpy).to.have.been.calledOnce

        expect(screen.getByText('User')).to.exist

        expect(screen.getAllByText('firstname')).to.have.length(1)
        expect(screen.getByText('testname')).to.exist

        expect(screen.getAllByText('candidate_number')).to.have.length(1)
        expect(screen.getByText('12')).to.exist

        expect(screen.getByText('Address')).to.exist

        expect(screen.getAllByText('canton')).to.have.length(2)
        expect(screen.getByText('aargau')).to.exist
        expect(screen.getByText('zürich')).to.exist

        expect(screen.getAllByText('address_nr')).to.have.length(2)
        expect(screen.getByText('15')).to.exist
        expect(screen.getByText('18')).to.exist
      })
    })
  })
})
