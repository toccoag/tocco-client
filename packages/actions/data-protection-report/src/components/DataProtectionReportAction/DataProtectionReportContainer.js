import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadDataProtectionReport} from '../../modules/dataProtectionReport/actions'

import DataProtectionReport from './DataProtectionReport'

const mapActionCreators = {
  loadDataProtectionReport
}

const mapStateToProps = state => ({
  dataProtectionReport: state.dataProtectionReport.dataProtectionReport
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(DataProtectionReport))
