import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {field} from 'tocco-app-extensions'
import {Typography, LoadMask, StatedValue, Panel, Layout} from 'tocco-ui'

import {StyledWrapper} from './StyledComponents'

const FieldComponent = ({type, value}) => {
  const Field = field.factory('readOnly', type)
  return <Field formField={{dataType: type}} value={value} mappingType="readOnly" />
}

FieldComponent.propTypes = {
  type: PropTypes.string.isRequired,
  value: PropTypes.any
}

const ValueComponent = ({id, type, value, label}) => {
  return (
    <StatedValue
      modelKey={id}
      immutable={true}
      hasValue={Boolean(value)}
      labelPosition="outside-responsive"
      label={label}
    >
      <FieldComponent type={type} value={value} />
    </StatedValue>
  )
}

ValueComponent.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.any,
  label: PropTypes.string.isRequired
}

const DataProtectionReport = ({dataProtectionReport, loadDataProtectionReport}) => {
  useEffect(() => {
    loadDataProtectionReport()
  }, [loadDataProtectionReport])

  return (
    <LoadMask required={[dataProtectionReport]}>
      {dataProtectionReport && dataProtectionReport.entityData && (
        <StyledWrapper>
          {Object.entries(dataProtectionReport.entityData).map(([modelLabel, entities], modelIndex) => (
            <div key={modelIndex}>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H4>{modelLabel}</Typography.H4>
                </Panel.Header>
                <Panel.Body>
                  {entities.map(({defaultDisplay, values}, entityIndex) => (
                    <Layout.Box key={entityIndex}>
                      <Typography.H5>{defaultDisplay}</Typography.H5>
                      {values.map(({fieldLabel, fieldValue, type}, fieldIndex) => (
                        <ValueComponent
                          key={fieldIndex}
                          id={`${modelIndex}-${entityIndex}-${fieldIndex}`}
                          type={type}
                          value={fieldValue}
                          label={fieldLabel}
                        />
                      ))}
                    </Layout.Box>
                  ))}
                </Panel.Body>
              </Panel.Wrapper>
            </div>
          ))}
        </StyledWrapper>
      )}
    </LoadMask>
  )
}
DataProtectionReport.propTypes = {
  dataProtectionReport: PropTypes.shape({
    entityData: PropTypes.objectOf(
      PropTypes.arrayOf(
        PropTypes.shape({
          modelLabel: PropTypes.string.isRequired,
          defaultDisplay: PropTypes.string.isRequired,
          values: PropTypes.arrayOf(
            PropTypes.shape({
              fieldLabel: PropTypes.string.isRequired,
              fieldValue: PropTypes.any,
              type: PropTypes.string.isRequired
            })
          )
        })
      )
    )
  }),
  loadDataProtectionReport: PropTypes.func.isRequired
}
export default DataProtectionReport
