import reducer, {sagas as dataProtectionReportSagas} from './dataProtectionReport'

export default {
  dataProtectionReport: reducer
}

export const sagas = [dataProtectionReportSagas]
