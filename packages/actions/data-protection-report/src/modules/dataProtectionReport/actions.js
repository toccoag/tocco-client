export const LOAD_DATA_PROTECTION_REPORT = 'dataProtectionReport/LOAD_DATA_PROTECTION_REPORT'
export const SET_DATA_PROTECTION_REPORT = 'dataProtectionReport/SET_DATA_PROTECTION_REPORT'

export const loadDataProtectionReport = () => ({
  type: LOAD_DATA_PROTECTION_REPORT
})

export const setDataProtectionReport = dataProtectionReport => ({
  type: SET_DATA_PROTECTION_REPORT,
  payload: {dataProtectionReport}
})
