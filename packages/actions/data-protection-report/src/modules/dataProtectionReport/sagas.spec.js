import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('data-protection-report', () => {
  describe('modules', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([takeEvery(actions.LOAD_DATA_PROTECTION_REPORT, sagas.loadDataProtectionReport)])
          )
          expect(generator.next().done).to.equal(true)
        })
      })

      describe('loadDataProtectionReport', () => {
        test('should set data', () => {
          const dataProtectionReportLoaded = {
            entityData: {
              User: {
                testUser: {
                  1: {
                    field: 'firstname',
                    data: 'testname'
                  },
                  2: {
                    field: 'candidate_number',
                    data: 12
                  }
                }
              },
              Address: {
                testAddress1: {
                  1: {
                    field: 'canton',
                    data: 'aargau'
                  },
                  2: {
                    field: 'address_nr',
                    data: 15
                  }
                },
                testAddress2: {
                  1: {
                    field: 'canton',
                    data: 'zürich'
                  },
                  2: {
                    field: 'address_nr',
                    data: 18
                  }
                }
              }
            }
          }
          const response = {body: {dataProtectionReport: dataProtectionReportLoaded}}
          return expectSaga(sagas.loadDataProtectionReport)
            .provide([
              [select(sagas.inputSelector), {selection: {entityName: 'User', type: 'ID', ids: ['13959']}}],
              [matchers.call.fn(rest.requestSaga), response]
            ])
            .call(rest.requestSaga, 'actions/dataProtectionReport/13959', {method: 'GET'})
            .put(actions.setDataProtectionReport({dataProtectionReport: dataProtectionReportLoaded}))
            .run()
        })
      })
    })
  })
})
