import {all, call, put, takeEvery, select} from 'redux-saga/effects'
import {rest, selection as selectionUtil} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(actions.LOAD_DATA_PROTECTION_REPORT, loadDataProtectionReport)])
}

export function* loadDataProtectionReport() {
  const {selection} = yield select(inputSelector)
  const key = selectionUtil.getSingleKey(selection, 'User')

  const result = yield call(rest.requestSaga, `actions/dataProtectionReport/${key}`, {method: 'GET'})

  yield put(actions.setDataProtectionReport(result.body))
}
