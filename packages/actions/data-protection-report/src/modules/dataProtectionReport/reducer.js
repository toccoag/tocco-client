import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  dataProtectionReport: null
}

const ACTION_HANDLERS = {
  [actions.SET_DATA_PROTECTION_REPORT]: reducerUtil.singleTransferReducer('dataProtectionReport')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
