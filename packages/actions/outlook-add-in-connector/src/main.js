import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import OutlookAddInConnector from './components/OutlookAddInConnector'
import reducers, {sagas} from './modules/reducers'

const packageName = 'outlook-add-in-connector'

const EXTERNAL_EVENTS = ['onSuccess']

const initApp = (id, input, events, publicPath) => {
  const content = <OutlookAddInConnector />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const OutlookAddInConnectorApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

OutlookAddInConnectorApp.propTypes = {
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => ({...propTypes, [event]: PropTypes.func}), {})
}

export default OutlookAddInConnectorApp
