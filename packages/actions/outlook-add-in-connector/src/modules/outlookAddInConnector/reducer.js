import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_URL]: reducerUtil.singleTransferReducer('url'),
  [actions.SET_USERNAME]: reducerUtil.singleTransferReducer('username'),
  [actions.SET_API_KEY]: reducerUtil.singleTransferReducer('apiKey'),
  [actions.SHOW_RESULT]: reducerUtil.singleTransferReducer('showResult')
}

const initialState = {
  url: null,
  username: null,
  apiKey: null,
  showResult: false
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
