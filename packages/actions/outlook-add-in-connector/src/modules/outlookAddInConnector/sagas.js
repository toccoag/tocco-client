import {takeLatest, all, call, put} from 'redux-saga/effects'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'

export default function* mainSagas() {
  yield all([takeLatest(actions.REQUEST_RESULT, requestResult)])
}

export function* requestResult() {
  yield put(actions.setShowResult(true))
  yield put(actions.setUrl(window.location.origin))
  try {
    const response = yield call(rest.requestSaga, 'officeintegration/apiKey', {method: 'POST'})
    yield put(actions.setUsername(response.body.username))
    yield put(actions.setApiKey(response.body.apiKey))
  } catch (error) {
    yield put(externalEvents.fireExternalEvent('onError'))
  }
}
