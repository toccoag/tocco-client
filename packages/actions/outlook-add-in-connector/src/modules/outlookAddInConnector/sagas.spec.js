import {all, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('outlook-add-in-connector', () => {
  describe('modules', () => {
    describe('sagas', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(all([takeLatest(actions.REQUEST_RESULT, sagas.requestResult)]))
        expect(generator.next().done).to.be.true
      })

      describe('requestResult', () => {
        test('success', () => {
          return expectSaga(sagas.requestResult)
            .provide([[matchers.call.fn(rest.requestSaga), {body: {apiKey: '1234567890', username: 'info@tocco.ch'}}]])
            .put(actions.setShowResult(true))
            .put(actions.setUrl('http://localhost'))
            .put(actions.setUsername('info@tocco.ch'))
            .put(actions.setApiKey('1234567890'))
            .run()
        })

        test('error', () => {
          return expectSaga(sagas.requestResult)
            .provide([[matchers.call.fn(rest.requestSaga), throwError(new Error('Failed to fetch data'))]])
            .put(actions.setShowResult(true))
            .put(actions.setUrl('http://localhost'))
            .put(externalEvents.fireExternalEvent('onError'))
            .run()
        })
      })
    })
  })
})
