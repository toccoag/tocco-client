export const SET_URL = 'outlook-add-in-connector/SET_URL'
export const SET_USERNAME = 'outlook-add-in-connector/SET_USERNAME'
export const SET_API_KEY = 'outlook-add-in-connector/SET_API_KEY'
export const SHOW_RESULT = 'outlook-add-in-connector/SHOW_RESULT'
export const REQUEST_RESULT = 'outlook-add-in-connector/REQUEST_RESULT'

export const setUrl = url => ({
  type: SET_URL,
  payload: {
    url
  }
})

export const setUsername = username => ({
  type: SET_USERNAME,
  payload: {
    username
  }
})

export const setApiKey = apiKey => ({
  type: SET_API_KEY,
  payload: {
    apiKey
  }
})

export const setShowResult = showResult => ({
  type: SHOW_RESULT,
  payload: {
    showResult
  }
})

export const requestResult = () => ({
  type: REQUEST_RESULT
})
