import outlookAddInConnector, {sagas as outlookAddInConnectorSagas} from './outlookAddInConnector'

export default {
  outlookAddInConnector
}

export const sagas = [outlookAddInConnectorSagas]
