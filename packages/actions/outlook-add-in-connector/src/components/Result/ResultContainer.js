import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import Result from './Result'

const mapActionCreators = {
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  url: state.outlookAddInConnector.url,
  username: state.outlookAddInConnector.username,
  apiKey: state.outlookAddInConnector.apiKey
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Result))
