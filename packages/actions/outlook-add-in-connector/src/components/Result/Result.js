import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Button, Typography, LoadMask} from 'tocco-ui'

import {StyledButtonWrapper} from '../GlobalStyledComponents'

import CopyIcon from './CopyIcon'

const Result = ({fireExternalEvent, url, username, apiKey, intl}) => {
  const onSuccess = () => {
    fireExternalEvent('onSuccess')
  }

  const msg = id => intl.formatMessage({id})

  return (
    <LoadMask required={[url, username, apiKey]}>
      <Typography.B>
        <FormattedMessage id="client.outlook-add-in-connector.url" />
      </Typography.B>
      <Typography.P>
        {url} <CopyIcon content={url} />
      </Typography.P>
      <Typography.B>
        <FormattedMessage id="client.outlook-add-in-connector.username" />
      </Typography.B>
      <Typography.P>
        {username} <CopyIcon content={username} />
      </Typography.P>
      <Typography.B>
        <FormattedMessage id="client.outlook-add-in-connector.apiKey" />
      </Typography.B>
      <Typography.P>
        {apiKey} <CopyIcon content={apiKey} />
      </Typography.P>
      <StyledButtonWrapper>
        <Button label={msg('client.outlook-add-in-connector.okButton')} look="raised" onClick={onSuccess} />
      </StyledButtonWrapper>
    </LoadMask>
  )
}

Result.propTypes = {
  intl: PropTypes.object.isRequired,
  fireExternalEvent: PropTypes.func.isRequired,
  url: PropTypes.string,
  username: PropTypes.string,
  apiKey: PropTypes.string
}

export default Result
