import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import OutlookAddInConnector from './OutlookAddInConnector'

const mapActionCreators = {}

const mapStateToProps = state => ({
  showResult: state.outlookAddInConnector.showResult
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(OutlookAddInConnector))
