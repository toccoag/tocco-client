import PropTypes from 'prop-types'

import Result from '../Result'
import Start from '../Start'

const OutlookAddInConnector = ({showResult}) => {
  if (showResult) {
    return <Result />
  }
  return <Start />
}

OutlookAddInConnector.propTypes = {
  showResult: PropTypes.bool
}

export default OutlookAddInConnector
