import PropTypes from 'prop-types'
import {Button, Typography, useOnEnterHandler} from 'tocco-ui'

import {StyledButtonWrapper} from '../GlobalStyledComponents'

const Start = ({requestResult, intl}) => {
  useOnEnterHandler(requestResult)
  const msg = id => intl.formatMessage({id})

  return (
    <>
      <Typography.P>
        <span dangerouslySetInnerHTML={{__html: msg('client.outlook-add-in-connector.info')}} />
      </Typography.P>
      <StyledButtonWrapper>
        <Button
          label={msg('client.outlook-add-in-connector.nextButton')}
          ink="primary"
          look="raised"
          onClick={requestResult}
        />
      </StyledButtonWrapper>
    </>
  )
}

Start.propTypes = {
  intl: PropTypes.object.isRequired,
  requestResult: PropTypes.func.isRequired
}

export default Start
