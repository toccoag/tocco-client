import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {requestResult} from '../../modules/outlookAddInConnector/actions'

import Start from './Start'

const mapActionCreators = {
  requestResult
}

export default connect(null, mapActionCreators)(injectIntl(Start))
