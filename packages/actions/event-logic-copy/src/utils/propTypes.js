import PropTypes from 'prop-types'

export const dataPropType = PropTypes.shape({
  key: PropTypes.string.isRequired,
  __key: PropTypes.string.isRequired,
  display: PropTypes.string.isRequired,
  level: PropTypes.number.isRequired,
  label: PropTypes.string,
  abbreviation: PropTypes.string,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  configuration: PropTypes.objectOf(PropTypes.bool)
})
export const pathPropType = PropTypes.shape({
  path: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  default: PropTypes.bool
})
