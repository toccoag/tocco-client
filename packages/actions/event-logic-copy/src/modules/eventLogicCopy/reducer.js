import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  initialized: false,
  eventData: [],
  configurationPaths: {
    relations: [],
    extensions: []
  },
  isValid: true,
  submitting: false
}

const setData = (state, keys, field, value) => ({
  ...state,
  eventData: state.eventData.map(event =>
    keys.includes(event.key)
      ? {
          ...event,
          [field]: value
        }
      : event
  )
})

const setValue = (state, {payload: {key, field, value}}) => setData(state, [key], field, value)

const getParentKeys = (childKey, eventData) => {
  if (!childKey) {
    return []
  }

  const child = eventData.find(e => e.key === childKey)
  const parentKeys = child ? getParentKeys(child.parentKey, eventData) : []

  return [childKey, ...parentKeys]
}

const getChildKeys = (rootKey, eventData) => {
  const keys = [rootKey]
  eventData.forEach(event => {
    if (keys.includes(event.parentKey)) {
      keys.push(event.key)
    }
  })
  return keys
}

// select all parents when selecting child, deselect all children when deselecting parent
const setSelection = (state, {payload: {key, value}}) => {
  if (value) {
    const keys = getParentKeys(key, state.eventData)
    return setData(state, keys, 'selected', value)
  } else {
    const keys = getChildKeys(key, state.eventData)
    return setData(state, keys, 'selected', value)
  }
}

// set all extension paths from configuration on event configuration
const replaceExtensionConfiguration = (event, configuration, extensionPaths) => ({
  ...event,
  configuration: {
    ...event.configuration,
    ...Object.entries(configuration)
      .filter(([path]) => extensionPaths.includes(path))
      .reduce(
        (curr, [path, value]) => ({
          ...curr,
          [path]: value
        }),
        {}
      )
  }
})

const setExtensions = (state, keys, extensionPaths, configuration) => ({
  ...state,
  eventData: state.eventData.map(event =>
    keys.includes(event.key) ? replaceExtensionConfiguration(event, configuration, extensionPaths) : event
  )
})

// always set extensions recursively, since children always use extension config of their parents
const setConfiguration = (state, {payload: {key, configuration, recursive}}) => {
  const keys = getChildKeys(key, state.eventData)
  if (recursive) {
    return setData(state, keys, 'configuration', configuration)
  } else {
    const rootState = setData(state, [key], 'configuration', configuration)
    const extensionPaths = state.configurationPaths.extensions.map(e => e.path)
    return setExtensions(rootState, keys, extensionPaths, configuration)
  }
}

const ACTION_HANDLERS = {
  [actions.SET_INITIALIZED]: reducerUtil.singleTransferReducer('initialized'),
  [actions.SET_IS_VALID]: reducerUtil.singleTransferReducer('isValid'),
  [actions.SET_EVENT_DATA]: reducerUtil.singleTransferReducer('eventData'),
  [actions.SET_EVENT_VALUE]: setValue,
  [actions.SET_SELECTION]: setSelection,
  [actions.SET_CONFIGURATION]: setConfiguration,
  [actions.SET_CONFIGURATION_PATHS]: reducerUtil.singleTransferReducer('configurationPaths'),
  [actions.SUBMIT]: reducerUtil.toggleReducer('submitting')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
