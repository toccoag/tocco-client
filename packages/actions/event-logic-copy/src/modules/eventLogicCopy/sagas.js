import _omit from 'lodash/omit'
import {all, put, takeLatest, call, select} from 'redux-saga/effects'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input
export const eventLogicCopySelector = state => state.eventLogicCopy

export default function* sagas() {
  yield all([takeLatest(actions.INITIALIZE, initialize), takeLatest(actions.SUBMIT, submit)])
}

export function* initialize() {
  const {selection} = yield select(inputSelector)
  const {
    body: {relations, extensions}
  } = yield call(rest.requestSaga, 'event/actions/eventLogicCopy/configurationPaths', {method: 'GET'})
  yield put(actions.setConfigurationPaths({relations, extensions}))
  const defaultConfigurationValues = [...relations, ...extensions].reduce(
    (values, path) => ({...values, [path.path]: path.defaultValue}),
    {}
  )
  const {
    body: {data}
  } = yield call(rest.requestSaga, 'event/actions/eventLogicCopy/loadData', {method: 'POST', body: selection})
  const eventData = data.map((event, index) => ({
    ...event,
    __key: event.key, // __key is used for react key prop on table row
    index: index + 1,
    selected: true,
    configuration: defaultConfigurationValues
  }))
  yield put(actions.setEventData(eventData))
  yield put(actions.setInitialized(true))
}

const mapConfigurations = (relationPaths, extensionPaths) => event => ({
  ...event,
  configuration: Object.entries(event.configuration).reduce(
    (curr, [path, value]) => {
      return {
        ...curr,
        relations: relationPaths.includes(path) ? {...curr.relations, [path]: value} : curr.relations,
        extensions: extensionPaths.includes(path) ? {...curr.extensions, [path]: value} : curr.extensions
      }
    },
    {relations: {}, extensions: {}}
  )
})

export function* submit() {
  const {eventData, configurationPaths} = yield select(eventLogicCopySelector)
  const relationPaths = configurationPaths.relations.map(relation => relation.path)
  const extensionPaths = configurationPaths.extensions.map(extension => extension.path)
  const data = eventData
    .filter(event => event.selected)
    .map(event => _omit(event, ['__key', 'index', 'selected']))
    .map(mapConfigurations(relationPaths, extensionPaths))
  yield call(rest.requestSaga, 'event/actions/eventLogicCopy/copy', {method: 'POST', body: {data}})
  // copying happens in background task, so we can just exit here
  yield put(externalEvents.fireExternalEvent('onSuccess'))
}
