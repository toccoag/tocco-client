import {takeLatest, select, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {externalEvents, rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('event-logic-copy', () => {
  describe('sagas', () => {
    describe('root saga', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([takeLatest(actions.INITIALIZE, sagas.initialize), takeLatest(actions.SUBMIT, sagas.submit)])
        )
        expect(generator.next().done).to.equal(true)
      })
    })

    const configurationPaths = {
      relations: [
        {path: 'relation1', label: 'label', defaultValue: true},
        {path: 'relation2', label: 'label', defaultValue: false}
      ],
      extensions: [
        {path: 'extension1', label: 'label', defaultValue: true},
        {path: 'extension2', label: 'label', defaultValue: false}
      ]
    }

    describe('initialize', () => {
      test('should load and set data', () => {
        const selection = {}
        const data = [
          {key: '1', label: 'first', display: 'first display'},
          {key: '2', label: 'second', display: 'second display'}
        ]
        const defaultConfiguration = {
          relation1: true,
          relation2: false,
          extension1: true,
          extension2: false
        }
        const expectedData = [
          {
            key: '1',
            __key: '1',
            label: 'first',
            display: 'first display',
            index: 1,
            selected: true,
            configuration: defaultConfiguration
          },
          {
            key: '2',
            __key: '2',
            label: 'second',
            display: 'second display',
            index: 2,
            selected: true,
            configuration: defaultConfiguration
          }
        ]
        return expectSaga(sagas.initialize)
          .provide([
            [select(sagas.inputSelector), {selection}],
            [
              matchers.call(rest.requestSaga, 'event/actions/eventLogicCopy/loadData', {
                method: 'POST',
                body: selection
              }),
              {body: {data}}
            ],
            [
              matchers.call(rest.requestSaga, 'event/actions/eventLogicCopy/configurationPaths', {
                method: 'GET'
              }),
              {body: configurationPaths}
            ]
          ])
          .put(actions.setEventData(expectedData))
          .put(actions.setConfigurationPaths(configurationPaths))
          .put(actions.setInitialized(true))
          .run()
      })
    })

    describe('submit', () => {
      test('should submit data and fire onSuccess', () => {
        const data = [
          {
            label: 'first',
            display: 'first display',
            configuration: {relations: {relation1: true}, extensions: {extension1: false}}
          },
          {
            label: 'second',
            display: 'second display',
            configuration: {relations: {relation2: false}, extensions: {extension2: true}}
          }
        ]
        const eventData = [
          {
            label: 'first',
            display: 'first display',
            index: 0,
            selected: true,
            configuration: {relation1: true, extension1: false, ignored: true}
          },
          {
            label: 'second',
            display: 'second display',
            index: 1,
            selected: true,
            configuration: {relation2: false, extension2: true, ignored: true}
          },
          {label: 'unselected', display: 'unselected display', index: 2, selected: false}
        ]
        return expectSaga(sagas.submit)
          .provide([
            [select(sagas.eventLogicCopySelector), {eventData, configurationPaths}],
            [matchers.call.fn(rest.requestSaga), {}]
          ])
          .call(rest.requestSaga, 'event/actions/eventLogicCopy/copy', {method: 'POST', body: {data}})
          .put(externalEvents.fireExternalEvent('onSuccess'))
          .run()
      })
    })
  })
})
