export const INITIALIZE = 'eventLogicCopy/INITIALIZE'
export const SET_INITIALIZED = 'eventLogicCopy/SET_INITIALIZED'
export const SET_EVENT_DATA = 'eventLogicCopy/SET_EVENT_DATA'
export const SET_EVENT_VALUE = 'eventLogicCopy/SET_EVENT_VALUE'
export const SET_SELECTION = 'eventLogicCopy/SET_SELECTION'
export const SET_CONFIGURATION = 'eventLogicCopy/SET_CONFIGURATION'
export const SET_CONFIGURATION_PATHS = 'eventLogicCopy/SET_CONFIGURATION_PATHS'
export const SET_IS_VALID = 'eventLogicCopy/SET_IS_VALID'
export const SUBMIT = 'eventLogicCopy/SUBMIT'

export const initialize = () => ({
  type: INITIALIZE
})

export const setInitialized = initialized => ({
  type: SET_INITIALIZED,
  payload: {initialized}
})

export const setEventData = eventData => ({
  type: SET_EVENT_DATA,
  payload: {eventData}
})

export const setValue = (key, field, value) => ({
  type: SET_EVENT_VALUE,
  payload: {key, field, value}
})

export const setSelection = (key, value) => ({
  type: SET_SELECTION,
  payload: {key, value}
})

export const setConfiguration = (key, configuration, recursive) => ({
  type: SET_CONFIGURATION,
  payload: {key, configuration, recursive}
})

export const setConfigurationPaths = configurationPaths => ({
  type: SET_CONFIGURATION_PATHS,
  payload: {configurationPaths}
})

export const setIsValid = isValid => ({
  type: SET_IS_VALID,
  payload: {isValid}
})

export const submit = () => ({
  type: SUBMIT
})
