import * as actions from './actions'
import reducer from './reducer'

describe('event-logic-copy', () => {
  describe('reducer', () => {
    const initialState = {
      eventData: [
        {
          key: '1'
        },
        {
          key: '2',
          parentKey: '1'
        },
        {
          key: '3'
        }
      ],
      configurationPaths: {
        relations: [{path: 'relation'}],
        extensions: [{path: 'extension'}]
      }
    }

    describe('setValue', () => {
      test('should set value', () => {
        const newState = reducer(initialState, actions.setValue('1', 'field', 'value'))
        expect(newState.eventData[0].field).to.eql('value')
        expect(newState.eventData[1].field).to.be.undefined
      })

      test('should overwrite value', () => {
        const newState = reducer(
          {
            ...initialState,
            field: 'old'
          },
          actions.setValue('1', 'field', 'value')
        )
        expect(newState.eventData[0].field).to.eql('value')
        expect(newState.eventData[1].field).to.be.undefined
      })
    })

    describe('setSelection', () => {
      test('should set selection', () => {
        const newState = reducer(
          {
            ...initialState
          },
          actions.setSelection('1', true)
        )
        expect(newState.eventData[0].selected).to.be.true
      })

      test('should select parents', () => {
        const newState = reducer(
          {
            ...initialState
          },
          actions.setSelection('2', true)
        )
        expect(newState.eventData[0].selected).to.be.true
        expect(newState.eventData[1].selected).to.be.true
        expect(newState.eventData[2].selected).to.be.undefined
      })

      test('should deselect children', () => {
        const allSelectedState = {
          eventData: initialState.eventData.map(event => ({...event, selected: true}))
        }
        const newState = reducer(
          {
            ...allSelectedState
          },
          actions.setSelection('1', false)
        )
        expect(newState.eventData[0].selected).to.be.false
        expect(newState.eventData[1].selected).to.be.false
        expect(newState.eventData[2].selected).to.be.true
      })
    })

    describe('setConfiguration', () => {
      const configuration = {
        relation: true,
        extension: true
      }

      test('should set configuration recursively', () => {
        const newState = reducer(
          {
            ...initialState
          },
          actions.setConfiguration('1', configuration, true)
        )
        expect(newState.eventData[0].configuration).to.deep.eql(configuration)
        expect(newState.eventData[1].configuration).to.deep.eql(configuration)
        expect(newState.eventData[2].configuration).to.be.undefined
      })

      test('should set relations directly and extension recursively', () => {
        const newState = reducer(
          {
            ...initialState
          },
          actions.setConfiguration('1', configuration, false)
        )
        expect(newState.eventData[0].configuration).to.deep.eql(configuration)
        expect(newState.eventData[1].configuration).to.deep.eql({extension: true})
        expect(newState.eventData[2].configuration).to.deep.undefined
      })
    })
  })
})
