import eventLogicCopyReducer from './eventLogicCopy/reducer'
import eventLogicCopySagas from './eventLogicCopy/sagas'

export default {
  eventLogicCopy: eventLogicCopyReducer
}

export const sagas = [eventLogicCopySagas]
