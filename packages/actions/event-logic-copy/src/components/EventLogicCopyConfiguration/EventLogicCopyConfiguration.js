import PropTypes from 'prop-types'
import {useCallback, useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {form} from 'tocco-app-extensions'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {Button, StyledStickyButtonWrapper, useOnEnterHandler} from 'tocco-ui'

import {dataPropType} from '../../utils/propTypes'

const setExtensionsConfigurationReadonlyOnChildren = (rowData, formDefinition) =>
  rowData.level > 0
    ? form.adjustByIds(formDefinition, ['extensions'], box => ({
        ...box,
        readonly: true
      }))
    : formDefinition

const EventLogicCopyConfiguration = ({close, setConfiguration, rowData, configurationFormDefinition, intl}) => {
  const [configuration, updateConfiguration] = useState(rowData.configuration)
  const handleChange = ({values, event}) => {
    if (event === 'change') {
      updateConfiguration(values)
    }
  }

  const onSave = useCallback(
    (recursive = false) => {
      setConfiguration(rowData.key, configuration, recursive)
      close()
    },
    [rowData.key, configuration, setConfiguration, close]
  )

  useOnEnterHandler(onSave)

  return (
    <>
      <EntityDetailApp
        entityName={rowData.relationshipKey ? 'Event_relationship' : 'Event'}
        formName={rowData.relationshipKey ? 'Event_logic_copy_child' : 'Event_logic_copy_root'}
        entityId={rowData.relationshipKey || rowData.key}
        mode="update"
        hideFooter={true}
        modifyFormDefinition={formDefinition => form.removeActions(formDefinition, [])}
      />

      <SimpleFormApp
        form={setExtensionsConfigurationReadonlyOnChildren(rowData, configurationFormDefinition)}
        mode="create"
        onChange={handleChange}
        noButtons={true}
        defaultValues={rowData.configuration}
      />
      <StyledStickyButtonWrapper>
        <Button onClick={() => onSave(true)} data-cy="btn-config-save">
          <FormattedMessage id="client.actions.event-logic-copy.configuration.save-recursive" />
        </Button>
        <Button autoFocus ink="primary" look="raised" onClick={() => onSave(false)}>
          <FormattedMessage id="client.actions.event-logic-copy.configuration.save" />
        </Button>
      </StyledStickyButtonWrapper>
    </>
  )
}

EventLogicCopyConfiguration.propTypes = {
  rowData: dataPropType,
  setConfiguration: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
  configurationFormDefinition: PropTypes.object.isRequired,
  intl: PropTypes.object.isRequired
}

export default EventLogicCopyConfiguration
