import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import PropTypes from 'prop-types'
import {form} from 'tocco-app-extensions'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import EventLogicCopyConfiguration from './EventLogicCopyConfiguration'

const emptyConfiguration = {relation: false, extension: false}
const filledConfiguration = {relation: true, extension: true}

jest.mock('tocco-entity-detail/src/main', () => () => <div data-testid="entity-detail" />)

const FakeForm = props => (
  <div data-testid="simple-form">
    <button
      data-testid="changeValue"
      onClick={() => props.onChange({values: filledConfiguration, event: 'change'})}
    ></button>
    {props.form.children.map(child => (
      <p key="child">{`${child.id} ${child.readonly ? 'readonly' : 'editable'}`}</p>
    ))}
  </div>
)
FakeForm.propTypes = {
  onChange: PropTypes.func,
  form: PropTypes.object
}
jest.mock('tocco-simple-form/src/main', () => props => <FakeForm {...props} />)

describe('event-logic-copy', () => {
  describe('components', () => {
    describe('EventLogicCopyConfiguration', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      const formDefinition = form.createSimpleForm({
        children: [
          form.createVerticalBox({
            id: 'extensions'
          })
        ]
      })

      const rowData = {key: '1', __key: '1', display: 'display', level: 0, configuration: emptyConfiguration}

      test('should render detail and form', async () => {
        testingLibrary.renderWithIntl(
          <EventLogicCopyConfiguration
            rowData={rowData}
            setConfiguration={() => {}}
            close={() => {}}
            configurationFormDefinition={formDefinition}
            intl={IntlStub}
          />
        )

        expect(screen.getByTestId('entity-detail')).to.exist
        expect(screen.getByTestId('simple-form')).to.exist
        expect(screen.getByText('client.actions.event-logic-copy.configuration.save-recursive')).to.exist
        expect(screen.getByText('client.actions.event-logic-copy.configuration.save')).to.exist
        expect(screen.getByText('extensions editable')).to.exist
      })

      test('should render extensions form readonly on children', async () => {
        testingLibrary.renderWithIntl(
          <EventLogicCopyConfiguration
            rowData={{...rowData, level: 1}}
            setConfiguration={() => {}}
            close={() => {}}
            configurationFormDefinition={formDefinition}
            intl={IntlStub}
          />
        )

        expect(screen.getByText('extensions readonly')).to.exist
      })

      test('should set configuration', async () => {
        const setConfiguration = sinon.spy()
        testingLibrary.renderWithIntl(
          <EventLogicCopyConfiguration
            rowData={rowData}
            setConfiguration={setConfiguration}
            close={() => {}}
            configurationFormDefinition={formDefinition}
            intl={IntlStub}
          />
        )

        const user = userEvent.setup()
        await user.click(screen.getByText('client.actions.event-logic-copy.configuration.save'))
        expect(setConfiguration).to.have.been.calledWith(rowData.key, emptyConfiguration, false)
      })

      test('should update configuration', async () => {
        const setConfiguration = sinon.spy()
        testingLibrary.renderWithIntl(
          <EventLogicCopyConfiguration
            rowData={rowData}
            setConfiguration={setConfiguration}
            close={() => {}}
            configurationFormDefinition={formDefinition}
            intl={IntlStub}
          />
        )

        const user = userEvent.setup()
        await user.click(screen.getByTestId('changeValue'))
        await user.click(screen.getByText('client.actions.event-logic-copy.configuration.save'))
        expect(setConfiguration).to.have.been.calledWith(rowData.key, filledConfiguration, false)
      })

      test('should set configuration recursively', async () => {
        const setConfiguration = sinon.spy()
        testingLibrary.renderWithIntl(
          <EventLogicCopyConfiguration
            rowData={rowData}
            setConfiguration={setConfiguration}
            close={() => {}}
            configurationFormDefinition={formDefinition}
            intl={IntlStub}
          />
        )

        const user = userEvent.setup()
        await user.click(screen.getByText('client.actions.event-logic-copy.configuration.save-recursive'))
        expect(setConfiguration).to.have.been.calledWith(rowData.key, emptyConfiguration, true)
      })
    })
  })
})
