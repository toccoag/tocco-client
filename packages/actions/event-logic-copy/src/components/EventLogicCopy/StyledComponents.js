import styled from 'styled-components'
import {themeSelector} from 'tocco-ui'

export const StyledPaneWrapper = styled.div`
  display: flex;
  height: 100%;
  flex-direction: column;
`

export const StyledListView = styled.div`
  display: grid;
  grid-template-rows: [action-start] auto [table-start] minmax(0, 1fr);
  height: 100%;
  width: 100%;
`

export const StyledActionsWrapper = styled.div`
  display: flex;
  background-color: ${themeSelector.color('paper')};
  margin-bottom: 3px;
  padding: 8px;
  grid-row-start: action-start;
  flex-wrap: wrap;
`

export const StyledListWrapper = styled.div`
  grid-row-start: table-start;
  background-color: ${themeSelector.color('paper')};
`
