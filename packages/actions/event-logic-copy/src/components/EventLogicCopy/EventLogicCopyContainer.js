import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {submit} from '../../modules/eventLogicCopy/actions'

import EventLogicCopy from './EventLogicCopy'

const mapActionCreators = {
  submit
}

const mapStateToProps = state => ({
  tableValid: state.eventLogicCopy.isValid,
  submitting: state.eventLogicCopy.submitting,
  anySelected: state.eventLogicCopy.eventData.some(event => event.selected)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EventLogicCopy))
