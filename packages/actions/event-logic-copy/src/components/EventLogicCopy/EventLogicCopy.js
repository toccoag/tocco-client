import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Button} from 'tocco-ui'

import EventLogicCopyTable from '../EventLogicCopyTable'

import {StyledActionsWrapper, StyledListView, StyledListWrapper, StyledPaneWrapper} from './StyledComponents'

const EventLogicCopy = ({tableValid, submitting, anySelected, submit}) => {
  return (
    <StyledPaneWrapper>
      <StyledListView>
        <StyledActionsWrapper>
          <Button
            onClick={submit}
            look="raised"
            ink="primary"
            disabled={!tableValid || submitting || !anySelected}
            pending={submitting}
            icon="copy"
            data-cy="btn-logic-copy"
          >
            <FormattedMessage id="client.actions.event-logic-copy.submit" />
          </Button>
        </StyledActionsWrapper>
        <StyledListWrapper>
          <EventLogicCopyTable />
        </StyledListWrapper>
      </StyledListView>
    </StyledPaneWrapper>
  )
}

EventLogicCopy.propTypes = {
  tableValid: PropTypes.bool,
  submitting: PropTypes.bool,
  anySelected: PropTypes.bool,
  submit: PropTypes.func.isRequired
}

export default EventLogicCopy
