import {form} from 'tocco-app-extensions'

export const buildConfigurationForm = (configurationPaths, intl) => {
  const msg = id => intl.formatMessage({id})
  return form.createSimpleForm({
    children: [
      form.createHorizontalBox({
        children: [
          form.createVerticalBox({
            children: [
              form.createVerticalBox({
                label: msg('client.actions.event-logic-copy.configuration.relations'),
                children: [
                  form.createVerticalBox({
                    children: configurationPaths.relations.map(path =>
                      form.createFieldSet({
                        label: path.label,
                        path: path.path,
                        dataType: 'boolean'
                      })
                    )
                  })
                ]
              }),
              form.createVerticalBox({
                id: 'extensions',
                label: msg('client.actions.event-logic-copy.configuration.extensions'),
                children: [
                  form.createVerticalBox({
                    children: configurationPaths.extensions.map(path =>
                      form.createFieldSet({
                        label: path.label,
                        path: path.path,
                        dataType: 'boolean'
                      })
                    )
                  })
                ]
              })
            ]
          })
        ]
      })
    ]
  })
}

export const createTableFormDefinition = intl => {
  const msg = id => intl.formatMessage({id})

  const formDefinition = form.createSimpleForm({
    children: [
      form.createTable({
        children: [
          form.createColumn({
            label: msg('client.actions.event-logic-copy.selected'),
            id: 'selected',
            dataType: 'boolean',
            shrinkToContent: true,
            readonly: false,
            alignment: 'center'
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.configuration'),
            id: 'configuration',
            dataType: 'action',
            children: [
              form.createButtonCell({
                id: 'configuration',
                icon: 'gear',
                label: msg('client.actions.event-logic-copy.configuration')
              })
            ],
            shrinkToContent: true
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.display'),
            id: 'display',
            dataType: 'string',
            clientRenderer: 'tree'
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.abbreviation'),
            id: 'abbreviation',
            dataType: 'string',
            readonly: false
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.label'),
            id: 'label',
            dataType: 'string',
            readonly: false,
            validation: form.createValidation(form.createMandatoryValidation())
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.start-date'),
            id: 'startDate',
            dataType: 'date',
            readonly: false
          }),
          form.createColumn({
            label: msg('client.actions.event-logic-copy.end-date'),
            id: 'endDate',
            dataType: 'date',
            readonly: false
          })
        ]
      })
    ]
  })

  return formDefinition
}
