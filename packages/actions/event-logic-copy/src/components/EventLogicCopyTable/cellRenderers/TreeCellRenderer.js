import PropTypes from 'prop-types'
import {Typography} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import {StyledLabelCellRenderer} from './StyledComponents'

const TreeCellRenderer = ({rowData, column, navigationStrategy}) => (
  <StyledLabelCellRenderer level={rowData.level}>
    {rowData.level > 0 ? '└─ ' : ''}
    <navigationStrategy.DetailLink entityName={'Event'} entityKey={rowData.key}>
      <Typography.Span>{rowData[column.id]}</Typography.Span>
    </navigationStrategy.DetailLink>
  </StyledLabelCellRenderer>
)

TreeCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes.isRequired
}

export default TreeCellRenderer
