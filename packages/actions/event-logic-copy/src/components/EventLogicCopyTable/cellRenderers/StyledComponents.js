import styled from 'styled-components'
import {declareFont, scale} from 'tocco-ui'

export const StyledLabelCellRenderer = styled.div`
  && {
    ${declareFont()}
    text-indent: ${({level}) => Math.max(0, (level - 1) * 25)}px;
    padding-top: ${scale.space(-1.3)};
  }
`
