import {screen} from '@testing-library/react'
import PropTypes from 'prop-types'
import {testingLibrary} from 'tocco-test-util'

import TreeCellRenderer from './TreeCellRenderer'

describe('event-logic-copy', () => {
  describe('components', () => {
    describe('cellRenderers', () => {
      describe('TreeCellRenderer', () => {
        const FakeLink = ({entityKey, entityName, children}) => (
          <>
            <span>
              Link: {entityKey} {entityName}
            </span>
            {children}
          </>
        )

        FakeLink.propTypes = {
          entityKey: PropTypes.string,
          entityName: PropTypes.string,
          children: PropTypes.object
        }

        test('should display value', async () => {
          testingLibrary.renderWithIntl(
            <TreeCellRenderer
              rowData={{column: 'value'}}
              column={{id: 'column'}}
              type={'string'}
              navigationStrategy={{DetailLink: FakeLink}}
            />
          )
          expect(screen.getByText('value')).to.exist
        })

        test('should display link', async () => {
          testingLibrary.renderWithIntl(
            <TreeCellRenderer
              rowData={{column: 'value', key: '1'}}
              column={{id: 'column'}}
              type={'string'}
              navigationStrategy={{DetailLink: FakeLink}}
            />
          )
          expect(screen.getByText('Link: 1 Event')).to.exist
        })

        test('should display tree symbols', async () => {
          const {container} = testingLibrary.renderWithIntl(
            <TreeCellRenderer
              rowData={{column: 'value', level: 2}}
              column={{id: 'column'}}
              type={'string'}
              navigationStrategy={{DetailLink: FakeLink}}
            />
          )
          expect(container.textContent.startsWith('└─')).to.be.true
        })
      })
    })
  })
})
