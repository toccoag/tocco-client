import {IntlStub} from 'tocco-test-util'

import {buildConfigurationForm} from './formDefinition'

describe('event-logic-copy', () => {
  describe('components', () => {
    describe('EventLogicCopyTable', () => {
      describe('formDefinition', () => {
        const assertFieldSet = (fieldSet, id) => {
          expect(fieldSet.id).to.be.eql(id)
          expect(fieldSet.label).to.be.eql(id)
          expect(fieldSet.children[0].componentType).to.be.eql('field')
          expect(fieldSet.children[0].dataType).to.be.eql('boolean')
        }

        test('should create boolean field for each configuration', () => {
          const form = buildConfigurationForm(
            {
              relations: [
                {path: 'first relation', label: 'first relation'},
                {path: 'second relation', label: 'second relation'}
              ],
              extensions: [
                {path: 'first extension', label: 'first extension'},
                {path: 'second extension', label: 'second extension'}
              ]
            },
            IntlStub
          )
          const relationsBox = form.children[0].children[0].children[0]
          const firstRelationFieldSet = relationsBox.children[0].children[0]
          const secondRelationFieldSet = relationsBox.children[0].children[1]
          const extensionsBox = form.children[0].children[0].children[1]
          const firstExtensionFieldSet = extensionsBox.children[0].children[0]
          const secondExtensionFieldSet = extensionsBox.children[0].children[1]
          expect(relationsBox.label).to.be.eql('client.actions.event-logic-copy.configuration.relations')
          expect(extensionsBox.label).to.be.eql('client.actions.event-logic-copy.configuration.extensions')
          assertFieldSet(firstRelationFieldSet, 'first relation')
          assertFieldSet(secondRelationFieldSet, 'second relation')
          assertFieldSet(firstExtensionFieldSet, 'first extension')
          assertFieldSet(secondExtensionFieldSet, 'second extension')
        })
      })
    })
  })
})
