import styled from 'styled-components'
import {scale, StretchingTableContainer} from 'tocco-ui'

export const StyledEventLogicCopyTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  td {
    display: flex;
  }

  tr > th:first-of-type {
    padding-left: ${scale.space(-0.7)};
  }

  tr > td:first-of-type {
    padding-left: ${scale.space(0)};
  }

  tr > th:last-of-type {
    padding-right: ${scale.space(-0.7)};
  }

  tr > td:last-of-type {
    padding-right: ${scale.space(0)};
  }

  ${StretchingTableContainer} {
    overflow-x: hidden;
  }
`
