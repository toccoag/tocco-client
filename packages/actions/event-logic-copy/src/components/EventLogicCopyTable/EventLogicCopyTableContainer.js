import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {notification} from 'tocco-app-extensions'

import {initialize, setValue, setSelection, setConfiguration, setIsValid} from '../../modules/eventLogicCopy/actions'

import EventLogicCopyTable from './EventLogicCopyTable'

const mapActionCreators = {
  initialize,
  setValue,
  setSelection,
  setConfiguration,
  setIsValid,
  openModal: notification.modal
}

const mapStateToProps = state => ({
  initialized: state.eventLogicCopy.initialized,
  eventData: state.eventLogicCopy.eventData,
  configurationPaths: state.eventLogicCopy.configurationPaths,
  submitting: state.eventLogicCopy.submitting,
  navigationStrategy: state.input.navigationStrategy
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EventLogicCopyTable))
