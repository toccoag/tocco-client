import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import PropTypes from 'prop-types'
import {IntlStub, testingLibrary, appUtils} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import EventLogicCopyTable from './EventLogicCopyTable'

describe('event-logic-copy', () => {
  describe('components', () => {
    describe('EventLogicCopyTable', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
        appUtils.populateInitialWindowState()
      })
      afterEach(() => {
        appUtils.restoreInitialWindowState()
      })

      const FakeLink = ({children}) => <>{children}</>

      FakeLink.propTypes = {
        children: PropTypes.object
      }

      const eventData = [
        {
          level: 0,
          index: 0,
          key: '1',
          __key: '1',
          abbreviation: 'abbreviation',
          label: 'label',
          display: 'display',
          startDate: '2024-01-02',
          endDate: '2024-03-04',
          errors: {},
          selected: true
        }
      ]

      test('should display values', async () => {
        testingLibrary.renderWithIntl(
          <EventLogicCopyTable
            eventData={eventData}
            configurationPaths={{relations: [], extensions: []}}
            submitting={false}
            initialized
            intl={IntlStub}
            initialize={() => {}}
            setValue={() => {}}
            setSelection={() => {}}
            setConfiguration={() => {}}
            setErrors={() => {}}
            setIsValid={() => {}}
            openModal={() => {}}
            navigationStrategy={{DetailLink: FakeLink}}
          />
        )
        await screen.findAllByTestId('icon')

        expect(screen.getByText('display')).to.exist
        expect(await screen.findByTestId('icon-gear')).to.exist
        expect(screen.getByRole('checkbox').checked).to.be.true
        expect(screen.getByDisplayValue('abbreviation')).to.exist
        expect(screen.getByDisplayValue('label')).to.exist
        expect(screen.getByDisplayValue('01/02/2024')).to.exist
        expect(screen.getByDisplayValue('03/04/2024')).to.exist
      })

      test('should call setSelection', async () => {
        const setSelection = sinon.spy()
        testingLibrary.renderWithIntl(
          <EventLogicCopyTable
            eventData={eventData}
            configurationPaths={{relations: [], extensions: []}}
            submitting={false}
            initialized
            intl={IntlStub}
            initialize={() => {}}
            setValue={() => {}}
            setSelection={setSelection}
            setConfiguration={() => {}}
            setErrors={() => {}}
            setIsValid={() => {}}
            openModal={() => {}}
            navigationStrategy={{DetailLink: FakeLink}}
          />
        )
        await screen.findAllByTestId('icon')

        const user = userEvent.setup()
        await user.click(screen.getByRole('checkbox'))
        expect(setSelection).to.have.been.calledWith('1', false)
      })

      test('should call openModal', async () => {
        const openModal = sinon.spy()
        testingLibrary.renderWithIntl(
          <EventLogicCopyTable
            eventData={eventData}
            configurationPaths={{relations: [], extensions: []}}
            submitting={false}
            initialized
            intl={IntlStub}
            initialize={() => {}}
            setValue={() => {}}
            setSelection={() => {}}
            setConfiguration={() => {}}
            setErrors={() => {}}
            setIsValid={() => {}}
            openModal={openModal}
            navigationStrategy={{DetailLink: FakeLink}}
          />
        )
        await screen.findAllByTestId('icon')

        const user = userEvent.setup()
        await user.click(await screen.findByTestId('icon-gear'))
        expect(openModal).to.have.been.calledWith(
          'event-logic-copy-configure',
          'client.actions.event-logic-copy.configuration.title',
          null
        )
        expect(openModal.getCall(0).args[3]).to.be.a('function')
      })
    })
  })
})
