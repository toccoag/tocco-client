import PropTypes from 'prop-types'
import {useEffect, useCallback, useMemo} from 'react'
import SimpleTableFormApp from 'tocco-simple-table-form/src/main'
import {LoadMask} from 'tocco-ui'
import {keyboard, navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import {dataPropType, pathPropType} from '../../utils/propTypes'
import EventLogicCopyConfiguration from '../EventLogicCopyConfiguration'

import {TreeCellRenderer} from './cellRenderers'
import {buildConfigurationForm, createTableFormDefinition} from './formDefinition'
import {StyledEventLogicCopyTableWrapper} from './StyledComponents'

const fieldsToFocus = ['abbreviation', 'label', 'startDate-datepicker', 'endDate-datepicker']

const EventLogicCopyTable = ({
  initialized,
  eventData,
  configurationPaths,
  submitting,
  intl,
  initialize,
  setValue,
  setSelection,
  setIsValid,
  openModal,
  setConfiguration,
  navigationStrategy
}) => {
  useEffect(() => {
    initialize()
  }, [initialize])

  const configurationFormDefinition = useMemo(
    () => buildConfigurationForm(configurationPaths, intl),
    [configurationPaths, intl]
  )
  const configure = useCallback(
    rowData => {
      openModal(
        'event-logic-copy-configure',
        'client.actions.event-logic-copy.configuration.title',
        null,
        ({close}) => {
          return (
            <EventLogicCopyConfiguration
              close={close}
              rowData={rowData}
              setConfiguration={setConfiguration}
              configurationFormDefinition={configurationFormDefinition}
              intl={intl}
            />
          )
        }
      )
    },
    [openModal, setConfiguration, configurationFormDefinition, intl]
  )

  const tableFormDefinition = useMemo(() => createTableFormDefinition(intl), [intl])
  const cellRenderers = useMemo(
    () => ({tree: props => <TreeCellRenderer navigationStrategy={navigationStrategy} {...props} />}),
    [navigationStrategy]
  )
  const customButtons = useMemo(
    () => ({
      configuration: rowData => configure(rowData)
    }),
    [configure]
  )

  return (
    <LoadMask required={[initialized]}>
      <StyledEventLogicCopyTableWrapper onKeyDown={keyboard.navigateTable(fieldsToFocus)}>
        <SimpleTableFormApp
          disabled={submitting}
          formDefinition={tableFormDefinition}
          data={eventData}
          cellRenderers={cellRenderers}
          customButtons={customButtons}
          onChange={({key, field, value}) => {
            if (field === 'selected') {
              setSelection(key, value)
            } else {
              setValue(key, field, value)
            }
          }}
          onValidate={({isValid}) => {
            setIsValid(isValid)
          }}
        />
      </StyledEventLogicCopyTableWrapper>
    </LoadMask>
  )
}

EventLogicCopyTable.propTypes = {
  initialized: PropTypes.bool,
  eventData: PropTypes.arrayOf(dataPropType),
  configurationPaths: PropTypes.shape({
    relations: PropTypes.arrayOf(pathPropType),
    extensions: PropTypes.arrayOf(pathPropType)
  }),
  submitting: PropTypes.bool,
  intl: PropTypes.object.isRequired,
  initialize: PropTypes.func.isRequired,
  setIsValid: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  setSelection: PropTypes.func.isRequired,
  setConfiguration: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes.isRequired
}

export default EventLogicCopyTable
