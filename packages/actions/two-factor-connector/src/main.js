import PropTypes from 'prop-types'
import {appFactory, errorLogging, externalEvents, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import TwoFactorConnector from './components/TwoFactorConnector'
import reducers, {sagas} from './modules'

const packageName = 'two-factor-connector'

const initApp = (id, input, events, publicPath) => {
  const content = <TwoFactorConnector />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  notification.addToStore(store, true)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducer', () => {
          const hotReloadedReducers = require('./modules/reducer').default
          reducerUtil.hotReloadReducers(store, hotReloadedReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const EXTERNAL_EVENTS = ['onSuccess', 'onCancel', 'onResize']

const TwoFactorConnectorApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

TwoFactorConnectorApp.propTypes = {
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => ({...propTypes, [event]: PropTypes.func}), {}),
  /**
   * Username to use, will be loaded from current principal if not available
   */
  username: PropTypes.string,
  /**
   * Password to use, only necessary if no active session is available.
   * for example in login app when two-factor activation is forced
   */
  password: PropTypes.string,
  /**
   * Secret to use for activation. If not set, a new secret is loaded from the server
   */
  secret: PropTypes.shape({
    secret: PropTypes.string.isRequired,
    uri: PropTypes.string.isRequired
  }),
  /**
   * Whether two factor authentication was forced on user, meaning they couldn't login.
   * Will be used to display alternate info message and run activation without session.
   */
  forced: PropTypes.bool
}

export default TwoFactorConnectorApp
export const app = appFactory.createBundleableApp(packageName, initApp, TwoFactorConnectorApp)
