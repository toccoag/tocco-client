import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, EditableValue, StatedValue, Typography, useOnEnterHandler} from 'tocco-ui'
import {env} from 'tocco-util'

import {StyledButtonWrapper} from '../GlobalStyledComponents'

import {StyledLoginFormInputWrapper} from './StyledComponents'

const Verification = ({verifyCode, intl}) => {
  const [userCode, setUserCode] = useState(null)
  const msg = id => intl.formatMessage({id})
  const handleVerifyClick = () => verifyCode(userCode)
  const isSubmittable = userCode && userCode.toString().length >= 6
  const handleSubmit = e => {
    if (e) {
      e.preventDefault()
      e.stopPropagation()
    }

    if (isSubmittable) {
      handleVerifyClick()
    }
  }

  useOnEnterHandler(handleSubmit)

  const isAdmin = env.isInAdminEmbedded()

  return (
    <>
      <Typography.P>
        <FormattedMessage id="client.two-factor-connector.verificationText" />
      </Typography.P>
      <form onSubmit={handleSubmit}>
        <StatedValue
          hasValue={Boolean(userCode)}
          id="verification-input"
          label={msg('client.two-factor-connector.verificationInputLabel')}
          labelPosition={isAdmin ? 'inside' : 'outside'}
        >
          <StyledLoginFormInputWrapper>
            <EditableValue
              type="integer"
              value={userCode}
              events={{onChange: setUserCode}}
              options={{format: '### ###', allowLeadingZeros: true}}
            />
          </StyledLoginFormInputWrapper>
        </StatedValue>
        <StyledButtonWrapper>
          <Button
            label={msg('client.two-factor-connector.okButton')}
            ink="primary"
            look="raised"
            type="submit"
            disabled={!isSubmittable}
          />
        </StyledButtonWrapper>
      </form>
    </>
  )
}

Verification.propTypes = {
  intl: PropTypes.object.isRequired,
  verifyCode: PropTypes.func.isRequired
}

export default Verification
