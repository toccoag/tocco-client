import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {Table} from 'tocco-ui'
import {table, navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import {DateCellRenderer, DefaultCellRenderer, LabelCellRenderer, TransferCellRenderer} from './cellrenderers'

const labelCellRenderer =
  ({navigationStrategy}) =>
  props => <LabelCellRenderer navigationStrategy={navigationStrategy} {...props} />
const transferCellRenderer =
  ({navigationStrategy}) =>
  props => <TransferCellRenderer navigationStrategy={navigationStrategy} {...props} />
const dateCellRenderer =
  ({type}) =>
  props => <DateCellRenderer type={type} {...props} />
const defaultCellRenderer =
  ({type}) =>
  props => <DefaultCellRenderer type={type} {...props} />

const createColumn = (id, label, type, options, navigationStrategy, cellRenderer = defaultCellRenderer) => ({
  id,
  label,
  resizable: true,
  sorting: {
    sortable: false
  },
  dynamic: true,
  readOnly: true,
  CellRenderer: cellRenderer({type, navigationStrategy}),
  ...options
})

const getColumnIfRequired = (dependency, column) => (dependency ? [column] : [])

const buildColumns = (columnConfig, navigationStrategy, intl) => [
  createColumn(
    'label',
    intl.formatMessage({id: 'client.actions.evaluation-view-tree.label'}),
    undefined,
    {
      alignment: 'left'
    },
    navigationStrategy,
    labelCellRenderer
  ),
  createColumn(
    'date',
    intl.formatMessage({id: 'client.actions.evaluation-view-tree.date'}),
    'date',
    {
      alignment: 'left',
      shrinkToContent: true
    },
    undefined,
    dateCellRenderer
  ),
  createColumn('dispense', intl.formatMessage({id: 'client.actions.evaluation-view-tree.dispense'}), 'boolean', {
    alignment: 'center',
    shrinkToContent: true
  }),

  ...getColumnIfRequired(
    columnConfig.hasGrade,
    createColumn('grade', intl.formatMessage({id: 'client.actions.evaluation-view-tree.grade'}), 'string', {
      alignment: 'right',
      shrinkToContent: true
    })
  ),
  ...getColumnIfRequired(
    columnConfig.hasPoints,
    createColumn('points', intl.formatMessage({id: 'client.actions.evaluation-view-tree.points'}), 'string', {
      alignment: 'right',
      shrinkToContent: true
    })
  ),
  ...getColumnIfRequired(
    columnConfig.hasPercentage,
    createColumn('percentage', intl.formatMessage({id: 'client.actions.evaluation-view-tree.percentage'}), 'string', {
      alignment: 'right',
      shrinkToContent: true
    })
  ),
  ...getColumnIfRequired(
    columnConfig.hasText,
    createColumn('text', intl.formatMessage({id: 'client.actions.evaluation-view-tree.text'}), 'string', {
      alignment: 'left'
    })
  ),
  ...getColumnIfRequired(
    columnConfig.hasTransfer,
    createColumn(
      'transfer',
      intl.formatMessage({id: 'client.actions.evaluation-view-tree.transfer'}),
      undefined,
      {
        alignment: 'center',
        shrinkToContent: true
      },
      navigationStrategy,
      transferCellRenderer
    )
  )
]

const getRequiredColumns = tableData =>
  tableData
    .map(({grade, percentage, points, text, instanceKey, valueKey}) => ({
      hasGrade: !!grade,
      hasPercentage: !!percentage,
      hasPoints: !!points,
      hasText: !!text,
      hasTransfer: valueKey && valueKey !== instanceKey
    }))
    .reduce(
      (acc, curr) => ({
        hasGrade: acc.hasGrade || curr.hasGrade,
        hasPercentage: acc.hasPercentage || curr.hasPercentage,
        hasPoints: acc.hasPoints || curr.hasPoints,
        hasText: acc.hasText || curr.hasText,
        hasTransfer: acc.hasTransfer || curr.hasTransfer
      }),
      {hasGrade: false, hasPercentage: false, hasPoints: false, hasText: false, hasTransfer: false}
    )

const EvaluationViewTreeInfo = ({tableData, navigationStrategy, intl}) => {
  const [columns, setColumns, onColumnPositionChange] = table.useColumnPosition()

  useEffect(() => {
    const tableConfig = getRequiredColumns(tableData)
    setColumns(buildColumns(tableConfig, navigationStrategy, intl))
  }, [setColumns, tableData, navigationStrategy, intl])

  return (
    <Table columns={columns} data={tableData} selectionStyle="none" onColumnPositionChange={onColumnPositionChange} />
  )
}

EvaluationViewTreeInfo.propTypes = {
  tableData: PropTypes.arrayOf(
    PropTypes.shape({
      instanceKey: PropTypes.string.isRequired,
      instanceLabel: PropTypes.string.isRequired,
      valueKey: PropTypes.string.isRequired,
      valueLabel: PropTypes.string.isRequired,
      level: PropTypes.number.isRequired,
      date: PropTypes.string,
      isFutureDate: PropTypes.bool,
      dispense: PropTypes.bool,
      grade: PropTypes.number,
      points: PropTypes.number,
      percentage: PropTypes.string,
      text: PropTypes.string
    })
  ),
  navigationStrategy: navigationStrategyPropTypes.propTypes,
  intl: PropTypes.object.isRequired
}

export default EvaluationViewTreeInfo
