import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import EvaluationViewTreeInfo from './EvaluationViewTreeTable'

const mapStateToProps = state => ({
  tableData: state.evaluationViewTree.tableData,
  navigationStrategy: state.input.navigationStrategy
})

export default connect(mapStateToProps)(injectIntl(EvaluationViewTreeInfo))
