import PropTypes from 'prop-types'
import {Popover} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import LinkIcon from './LinkIcon'
import {StyledLabelCellRenderer} from './StyledComponents'

const TransferCellRenderer = ({rowData, navigationStrategy}) =>
  rowData.valueKey && rowData.instanceKey !== rowData.valueKey ? (
    <StyledLabelCellRenderer>
      <Popover content={rowData.valueLabel}>
        <LinkIcon model={rowData.model} entityKey={rowData.valueKey} navigationStrategy={navigationStrategy} />
      </Popover>
    </StyledLabelCellRenderer>
  ) : null

TransferCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export default TransferCellRenderer
