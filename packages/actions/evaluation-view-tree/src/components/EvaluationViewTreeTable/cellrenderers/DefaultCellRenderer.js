import PropTypes from 'prop-types'
import {field} from 'tocco-app-extensions'

const DefaultCellRenderer = ({rowData, column, type}) => {
  const ReadOnlyField = field.factory('readOnly', type)
  return (
    <ReadOnlyField formField={{dataType: type}} value={rowData[column.id]} mappingType="readOnly" breakWords={false} />
  )
}

DefaultCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired
}

export default DefaultCellRenderer
