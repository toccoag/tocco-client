import styled, {css} from 'styled-components'
import {declareColoredLabel, declareFont, declareNoneWrappingText, scale, themeSelector, Icon} from 'tocco-ui'

export const StyledLabelCellRenderer = styled.div`
  && {
    ${declareFont()}
    ${declareNoneWrappingText()}
    text-indent: ${({level}) => Math.max(0, (level - 1) * 25)}px;
  }

  * {
    /* reset text-indent for non-text children */
    text-indent: 0;
  }
`

export const StyledIconContainer = styled.span`
  display: inline-block;
  padding: 0 ${scale.space(-1.1)};
`

export const StyledDate = styled.div`
  ${({isFutureDate}) => declareColoredLabel(isFutureDate ? 'danger' : '')}
  display: flex;
`

export const StyledInactiveIcon = styled(Icon)`
  ${({disabled}) =>
    disabled &&
    css`
      color: ${themeSelector.color('textDisabled')};
    `}
`
