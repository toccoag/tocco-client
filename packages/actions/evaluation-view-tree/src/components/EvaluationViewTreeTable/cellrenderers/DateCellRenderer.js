import PropTypes from 'prop-types'

import DefaultCellRenderer from './DefaultCellRenderer'
import {StyledDate} from './StyledComponents'

const DateCellRenderer = props => {
  const {rowData} = props
  return (
    <StyledDate isFutureDate={rowData.isFutureDate}>
      <DefaultCellRenderer {...props} />
    </StyledDate>
  )
}

DateCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired
}
export default DateCellRenderer
