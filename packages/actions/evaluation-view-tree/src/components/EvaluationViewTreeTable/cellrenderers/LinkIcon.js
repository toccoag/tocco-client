import PropTypes from 'prop-types'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import {StyledInactiveIcon} from './StyledComponents'

const LinkIcon = ({model, entityKey, navigationStrategy}) => {
  const Icon = <StyledInactiveIcon icon={model === 'Evaluation' ? 'calculator' : 'pen-field'} disabled={!entityKey} />

  return entityKey ? (
    <navigationStrategy.DetailLink entityName={model} entityKey={entityKey}>
      {Icon}
    </navigationStrategy.DetailLink>
  ) : (
    Icon
  )
}
LinkIcon.propTypes = {
  model: PropTypes.string.isRequired,
  entityKey: PropTypes.string,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export default LinkIcon
