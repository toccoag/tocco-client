import PropTypes from 'prop-types'
import {Popover} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

import LinkIcon from './LinkIcon'
import {StyledIconContainer, StyledLabelCellRenderer} from './StyledComponents'

const LabelCellRenderer = ({rowData, navigationStrategy}) => (
  <StyledLabelCellRenderer level={rowData.level}>
    {rowData.level > 0 ? '└─ ' : ''}
    {rowData.level > 0 && (
      <StyledIconContainer>
        <LinkIcon model={rowData.model} entityKey={rowData.instanceKey} navigationStrategy={navigationStrategy} />
      </StyledIconContainer>
    )}
    <Popover content={rowData.instanceLabel}>{rowData.instanceLabel}</Popover>
  </StyledLabelCellRenderer>
)

LabelCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}
export default LabelCellRenderer
