import {screen} from '@testing-library/react'
import PropTypes from 'prop-types'
import {testingLibrary, IntlStub} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import EvaluationViewTreeTable from './EvaluationViewTreeTable'

describe('evaluation-view-tree', () => {
  describe('components', () => {
    describe('EvaluationViewTreeTable', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      const FakeDetailLink = ({entityName, entityKey, children}) => (
        <div>
          <p>
            link: {entityName}:{entityKey}
          </p>
          {children}
        </div>
      )
      FakeDetailLink.propTypes = {
        entityName: PropTypes.string.isRequired,
        entityKey: PropTypes.string.isRequired,
        children: PropTypes.any
      }

      test('should render data', async () => {
        const data = [
          {
            model: 'Input',
            instanceKey: '1',
            instanceLabel: 'instance label',
            valueKey: '2',
            valueLabel: 'value label',
            level: 1,
            date: '2024-01-01',
            isFutureDate: false,
            dispense: true,
            grade: 1.234,
            points: 43.21,
            percentage: '85%',
            text: 'text value'
          }
        ]
        testingLibrary.renderWithIntl(
          <EvaluationViewTreeTable navigationStrategy={{DetailLink: FakeDetailLink}} intl={IntlStub} tableData={data} />
        )

        await screen.findAllByTestId('icon')
        expect(screen.getByText('link: Input:1')).to.exist
        expect(screen.getByText('└─')).to.exist
        expect(screen.getByText('instance label')).to.exist
        expect(screen.getByText('01/01/2024')).to.exist
        expect(screen.getByTestId('icon-check')).to.exist // dispense
        expect(screen.getByText('01/01/2024')).to.exist
        expect(screen.getByText('1.234')).to.exist
        expect(screen.getByText('43.21')).to.exist
        expect(screen.getByText('85%')).to.exist
        expect(screen.getByText('text value')).to.exist
        expect(screen.getByText('link: Input:2')).to.exist
        expect(screen.queryByText('value label')).to.not.exist // hidden on popover
        expect(screen.getAllByTestId('icon')).to.have.length(3) // instance icon, value icon and dispense
      })

      test('should hide columns without data', async () => {
        const data = [
          {
            model: 'Input',
            instanceKey: '1',
            instanceLabel: '',
            valueKey: '1',
            valueLabel: '',
            level: 1,
            isFutureDate: false,
            text: ''
          }
        ]
        testingLibrary.renderWithIntl(
          <EvaluationViewTreeTable navigationStrategy={{DetailLink: FakeDetailLink}} intl={IntlStub} tableData={data} />
        )
        await screen.findAllByTestId('icon')
        expect(screen.getByText('client.actions.evaluation-view-tree.label')).to.exist
        expect(screen.getByText('client.actions.evaluation-view-tree.date')).to.exist
        expect(screen.getByText('client.actions.evaluation-view-tree.dispense')).to.exist
        expect(screen.queryByText('client.actions.evaluation-view-tree.grade')).to.not.exist
        expect(screen.queryByText('client.actions.evaluation-view-tree.text')).to.not.exist
        expect(screen.queryByText('client.actions.evaluation-view-tree.percentage')).to.not.exist
        expect(screen.queryByText('client.actions.evaluation-view-tree.points')).to.not.exist
        expect(screen.queryByText('client.actions.evaluation-view-tree.transfer')).to.not.exist
      })

      test('should hide icon and indent if root', async () => {
        const data = [
          {
            model: 'Input',
            instanceKey: '1',
            instanceLabel: 'instance label',
            valueKey: '1',
            valueLabel: 'value label',
            level: 0
          }
        ]
        testingLibrary.renderWithIntl(
          <EvaluationViewTreeTable navigationStrategy={{DetailLink: FakeDetailLink}} intl={IntlStub} tableData={data} />
        )
        expect(screen.queryByText('link: Input:1')).to.not.exist
        expect(screen.getByText('instance label')).to.exist
        expect(screen.queryByTestId('icon')).to.not.exist
      })

      test('should hide transfer if same as instance', async () => {
        const data = [
          {
            model: 'Input',
            instanceKey: '1',
            instanceLabel: 'instance label',
            valueKey: '1',
            valueLabel: 'value label',
            level: 1
          }
        ]
        testingLibrary.renderWithIntl(
          <EvaluationViewTreeTable navigationStrategy={{DetailLink: FakeDetailLink}} intl={IntlStub} tableData={data} />
        )
        await screen.findAllByTestId('icon')
        expect(screen.getByText('link: Input:1')).to.exist
        expect(screen.getByText('└─')).to.exist
        expect(screen.getByText('instance label')).to.exist
        expect(screen.getByTestId('icon-pen-field')).to.exist
        expect(screen.queryByText('link: Input:2')).to.not.exist
        expect(screen.queryByText('value label')).to.not.exist
      })

      test('should use different icons for models', async () => {
        const data = [
          {
            __key: '1',
            model: 'Input',
            instanceKey: '1',
            instanceLabel: 'instance label',
            valueKey: '1',
            valueLabel: 'value label',
            level: 1
          },
          {
            __key: '2',
            model: 'Evaluation',
            instanceKey: '1',
            instanceLabel: 'instance label',
            valueKey: '1',
            valueLabel: 'value label',
            level: 1
          }
        ]
        testingLibrary.renderWithIntl(
          <EvaluationViewTreeTable navigationStrategy={{DetailLink: FakeDetailLink}} intl={IntlStub} tableData={data} />
        )
        await screen.findAllByTestId('icon')
        expect(screen.getByTestId('icon-pen-field')).to.exist
        expect(screen.getByTestId('icon-calculator')).to.exist
      })
    })
  })
})
