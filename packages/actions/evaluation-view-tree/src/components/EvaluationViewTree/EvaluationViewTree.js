import PropTypes from 'prop-types'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {LoadMask} from 'tocco-ui'

import EvaluationViewTreeTable from '../EvaluationViewTreeTable'

import {StyledEvaluationViewTreeWrapper} from './StyledComponents'

const EvaluationViewTree = ({evaluationDataKey}) => {
  return (
    <LoadMask required={[evaluationDataKey]}>
      <StyledEvaluationViewTreeWrapper>
        <EntityDetailApp
          entityName="Evaluation_data"
          formName="Evaluation_view_tree"
          entityId={evaluationDataKey}
          mode="update"
          hideFooter={true}
        />
        <EvaluationViewTreeTable />
      </StyledEvaluationViewTreeWrapper>
    </LoadMask>
  )
}

EvaluationViewTree.propTypes = {
  evaluationDataKey: PropTypes.string
}

export default EvaluationViewTree
