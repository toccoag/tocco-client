import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import EvaluationViewTree from './EvaluationViewTree'

const mapStateToProps = state => ({
  evaluationDataKey: state.evaluationViewTree.evaluationDataKey
})

export default connect(mapStateToProps)(injectIntl(EvaluationViewTree))
