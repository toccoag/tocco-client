import styled from 'styled-components'

export const StyledEvaluationViewTreeWrapper = styled.div`
  display: grid;
  grid-template-rows: 1fr minmax(200px, 40dvh);
`
