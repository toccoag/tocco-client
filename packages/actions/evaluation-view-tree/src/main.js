import {appFactory, selection} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import EvaluationViewTree from './components/EvaluationViewTree'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'evaluation-view-tree'

const initApp = (id, input, events, publicPath) => {
  const content = <EvaluationViewTree />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(),
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const EvaluationViewTreeApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

EvaluationViewTreeApp.propTypes = {
  /**
   * Selection of a single `Evaluation_data` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired
}

export default EvaluationViewTreeApp
