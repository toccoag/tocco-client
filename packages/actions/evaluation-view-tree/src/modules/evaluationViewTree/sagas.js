import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, selection as selectionUtil} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input
export const evaluationViewTreeSelector = state => state.evaluationViewTree

export default function* sagas() {
  yield all([takeLatest(actions.INITIALIZE, initialize)])
}

export function* initialize() {
  const {selection} = yield select(inputSelector)
  const key = selectionUtil.getSingleKey(selection, 'Evaluation_data')
  yield put(actions.setEvaluationDataKey(key))

  const {
    body: {rows}
  } = yield call(rest.requestSaga, `qualification/action/evaluationViewTree/${key}`, {method: 'GET'})

  yield put(actions.setTableData(rows.map((element, index) => ({...element, __key: index}))))
}
