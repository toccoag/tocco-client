export const INITIALIZE = 'evaluationViewTree/INITIALIZE'
export const SET_EVALUATION_DATA_KEY = 'evaluationViewTree/SET_EVALUATION_DATA_KEY'
export const SET_TABLE_DATA = 'evaluationViewTree/SET_TABLE_DATA'

export const initialize = () => ({
  type: INITIALIZE
})

export const setEvaluationDataKey = evaluationDataKey => ({
  type: SET_EVALUATION_DATA_KEY,
  payload: {evaluationDataKey}
})

export const setTableData = tableData => ({
  type: SET_TABLE_DATA,
  payload: {tableData}
})
