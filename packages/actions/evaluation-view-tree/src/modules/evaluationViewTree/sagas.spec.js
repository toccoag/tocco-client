import {all, takeLatest, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('evaluation-view-tree', () => {
  describe('modules', () => {
    describe('evaluationViewTree', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(all([takeLatest(actions.INITIALIZE, sagas.initialize)]))
            expect(generator.next().done).to.be.true
          })
        })

        describe('initialize', () => {
          test('should initialize and load table data', () => {
            const selection = {ids: ['1'], entityName: 'Evaluation_data', type: 'ID'}
            const body = {
              rows: [
                {
                  label: 'Level 1',
                  level: 1,
                  dispense: false,
                  grade: 4.5,
                  points: null,
                  percentage: null,
                  text: null
                },
                {
                  label: 'Level 2',
                  level: 2,
                  dispense: false,
                  grade: 4.5,
                  points: null,
                  percentage: null,
                  text: null
                }
              ]
            }
            const tableData = [
              {
                __key: 0,
                label: 'Level 1',
                level: 1,
                dispense: false,
                grade: 4.5,
                points: null,
                percentage: null,
                text: null
              },
              {
                __key: 1,
                label: 'Level 2',
                level: 2,
                dispense: false,
                grade: 4.5,
                points: null,
                percentage: null,
                text: null
              }
            ]
            return expectSaga(sagas.initialize)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call.fn(rest.requestSaga), {body}]
              ])
              .put(actions.setEvaluationDataKey('1'))
              .put(actions.setTableData(tableData))
              .run()
          })
        })
      })
    })
  })
})
