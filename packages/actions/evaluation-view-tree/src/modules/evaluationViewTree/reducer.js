import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  evaluationDataKey: null,
  tableData: []
}

const ACTION_HANDLERS = {
  [actions.SET_EVALUATION_DATA_KEY]: reducerUtil.singleTransferReducer('evaluationDataKey'),
  [actions.SET_TABLE_DATA]: reducerUtil.singleTransferReducer('tableData')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
