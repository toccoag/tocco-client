import evaluationViewTreeReducer from './evaluationViewTree/reducer'
import evaluationViewTreeSagas from './evaluationViewTree/sagas'

export default {
  evaluationViewTree: evaluationViewTreeReducer
}

export const sagas = [evaluationViewTreeSagas]
