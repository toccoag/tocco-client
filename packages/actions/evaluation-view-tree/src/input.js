import {initialize} from './modules/evaluationViewTree/actions'

export const getDispatchActions = () => [initialize()]
