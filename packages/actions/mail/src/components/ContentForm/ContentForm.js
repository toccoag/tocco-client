import PropTypes from 'prop-types'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button, StyledStickyButtonWrapper} from 'tocco-ui'

export const REDUX_FORM_NAME = 'mail-content'

const ContentForm = ({formDefinition, formValues, submitForm, submitting, valid, intl}) => {
  const formEventProps = form.hooks.useFormEvents({submitForm})
  return (
    <form {...formEventProps}>
      <form.FormBuilder
        entity={{model: 'Mail_action_settings'}}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        labelPosition="inside"
        mode="create"
      />
      <StyledStickyButtonWrapper>
        <Button
          disabled={submitting || !valid}
          label={intl.formatMessage({id: 'client.actions.mail.send'})}
          type="submit"
          look="raised"
          ink="primary"
        />
      </StyledStickyButtonWrapper>
    </form>
  )
}

ContentForm.propTypes = {
  formDefinition: PropTypes.object,
  formValues: PropTypes.object,
  submitForm: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  intl: PropTypes.object.isRequired
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(ContentForm)
