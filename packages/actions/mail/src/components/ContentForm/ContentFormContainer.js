import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues, isSubmitting, isValid} from 'redux-form'
import {form} from 'tocco-app-extensions'

import {submitContentForm} from '../../modules/mail/actions'

import ContentForm, {REDUX_FORM_NAME} from './ContentForm'

const mapActionCreators = {
  submitForm: submitContentForm
}

const mapStateToProps = state => ({
  formDefinition: state.mail.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state),
  valid: isValid(REDUX_FORM_NAME)(state),
  submitting: isSubmitting(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ContentForm))
