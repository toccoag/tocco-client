import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {form} from 'tocco-app-extensions'
import {LoadMask} from 'tocco-ui'

import ContentForm from '../ContentForm'

const ContentView = ({initialize, formDefinition, fieldDefinitions}) => {
  useEffect(() => {
    initialize()
  }, [initialize])

  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  return (
    <LoadMask required={[formDefinition]}>
      <ContentForm validate={handleSyncValidate} />
    </LoadMask>
  )
}

ContentView.propTypes = {
  initialize: PropTypes.func.isRequired,
  formDefinition: PropTypes.object,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object)
}

export default ContentView
