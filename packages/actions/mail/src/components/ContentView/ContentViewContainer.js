import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initialize} from '../../modules/mail/actions'

import ContentView from './ContentView'

const mapActionCreators = {
  initialize
}
const mapStateToProps = state => ({
  formDefinition: state.mail.formDefinition,
  fieldDefinitions: state.mail.fieldDefinitions
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ContentView))
