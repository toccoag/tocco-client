import {reducer as form} from 'redux-form'

import mail, {sagas as mailSagas, formSagaConfig} from './mail'

export default {
  mail,
  form
}

export const sagas = [mailSagas]
export {formSagaConfig}
