import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initializeLocale = (state, {payload: {recipientLocales, locales}}) => {
  const availableLocales = locales.map(l => l.uniqueId)
  return {
    ...state,
    // initially all locale fields must be shown that they are registered by redux-forms
    // after the initialization only the recipientLocales are shown (handled by the saga)
    showedLocales: availableLocales,
    recipientLocales,
    availableLocales,
    localeMap: locales.reduce((acc, curr) => ({...acc, [curr.key]: curr.uniqueId}), {})
  }
}

const setSelectedLocale = (state, {payload: {selectedLocale}}) => ({
  ...state,
  selectedLocale,
  showedLocales: selectedLocale ? [selectedLocale] : state.recipientLocales
})

const initializeProviders = (state, {payload: {providers}}) => {
  return {
    ...state,
    providerMap: providers.reduce((acc, curr) => ({...acc, [curr.key]: curr.uniqueId}), {})
  }
}

const ACTION_HANDLERS = {
  [actions.SET_INITIAL_FORM_VALUES]: reducerUtil.singleTransferReducer('initalFormValues'),
  [actions.SET_INITIAL_FORM_DEFINITION]: reducerUtil.singleTransferReducer('initialFormDefinition'),
  [actions.SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [actions.SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions'),
  [actions.INITIALIZE_LOCALE]: initializeLocale,
  [actions.SET_SELECTED_LOCALE]: setSelectedLocale,
  [actions.INITIALIZE_PROVIDERS]: initializeProviders,
  [actions.SET_SELECTED_PROVIDER]: reducerUtil.singleTransferReducer('selectedProvider'),
  [actions.SET_AVAILABLE_EMAIL_SENDERS]: reducerUtil.singleTransferReducer('availableEmailSenders')
}

const initialState = {
  initalFormValues: {},
  initialFormDefinition: undefined,
  formDefinition: undefined,
  fieldDefinitions: undefined,
  // locale selected in the dropdown
  selectedLocale: undefined,
  // locales currently shown
  showedLocales: undefined,
  // locales required over the selected recipients
  recipientLocales: undefined,
  // all available locales in the backend (=all correspondence language entities)
  availableLocales: undefined,
  // helper map to resolve from correspondence language key to unique_id
  localeMap: undefined,
  // required that initial auto complete for default template is working
  mode: 'create',
  selectedProvider: 'default',
  providerMap: undefined,
  availableEmailSenders: {}
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
