import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  initalFormValues: {},
  initialFormDefinition: undefined,
  formDefinition: undefined,
  fieldDefinitions: undefined,
  selectedLocale: undefined,
  showedLocales: undefined,
  recipientLocales: undefined,
  availableLocales: undefined,
  localeMap: undefined,
  mode: 'create',
  selectedProvider: 'default',
  providerMap: undefined,
  availableEmailSenders: {}
}

describe('mail', () => {
  describe('modules', () => {
    describe('mail', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        describe('INITIALIZE_LOCALE', () => {
          test('should handle INITIALIZE_LOCALE', () => {
            const recipientLocales = ['de']
            const locales = [
              {key: 1, uniqueId: 'de'},
              {key: 2, uniqueId: 'en'}
            ]

            const stateBefore = {
              initialFormDefinition: undefined,
              showedLocales: undefined,
              recipientLocales: undefined,
              availableLocales: undefined,
              localeMap: undefined
            }
            const expectedStateAfter = {
              initialFormDefinition: undefined,
              showedLocales: ['de', 'en'],
              recipientLocales,
              availableLocales: ['de', 'en'],
              localeMap: {1: 'de', 2: 'en'}
            }

            expect(reducer(stateBefore, actions.initializeLocale(recipientLocales, locales))).to.deep.equal(
              expectedStateAfter
            )
          })
        })

        describe('SET_SELECTED_LOCALE', () => {
          test('should handle if a locale is selected', () => {
            const stateBefore = {
              initialFormDefinition: {},
              selectedLocale: null,
              showedLocales: ['de', 'en'],
              recipientLocales: ['de', 'en']
            }
            const expectedStateAfter = {
              initialFormDefinition: {},
              selectedLocale: 'de',
              showedLocales: ['de'],
              recipientLocales: ['de', 'en']
            }

            expect(reducer(stateBefore, actions.setSelectedLocale('de'))).to.deep.equal(expectedStateAfter)
          })

          test('should handle if a locale dropdown is cleared', () => {
            const stateBefore = {
              initialFormDefinition: {},
              selectedLocale: 'de',
              showedLocales: ['de'],
              recipientLocales: ['de', 'en']
            }
            const expectedStateAfter = {
              initialFormDefinition: {},
              selectedLocale: null,
              showedLocales: ['de', 'en'],
              recipientLocales: ['de', 'en']
            }

            expect(reducer(stateBefore, actions.setSelectedLocale(null))).to.deep.equal(expectedStateAfter)
          })
        })

        describe('INITIALIZE_PROVIDERS', () => {
          test('initialize providers', () => {
            const stateBefore = {
              initialFormDefinition: {},
              providerMap: undefined
            }
            const providers = [
              {key: 1, uniqueId: 'default'},
              {key: 2, uniqueId: 'incamail'}
            ]
            const expectedStateAfter = {
              initialFormDefinition: {},
              providerMap: {
                1: 'default',
                2: 'incamail'
              }
            }

            expect(reducer(stateBefore, actions.initializeProviders(providers))).to.deep.equal(expectedStateAfter)
          })
        })
      })
    })
  })
})
