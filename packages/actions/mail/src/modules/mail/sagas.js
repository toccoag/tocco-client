import {actions as formActions, getFormValues} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {takeLatest, all, call, debounce, put, select, take} from 'redux-saga/effects'
import {rest, externalEvents, form, notification} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ContentForm'
import {getInitialFormDefinition} from '../../util/form'

import * as actions from './actions'

export const inputSelector = state => state.input
export const mailSelector = state => state.mail

export const formSagaConfig = {
  formId: REDUX_FORM_NAME,
  stateSelector: mailSelector
}

export default function* mainSagas() {
  yield all([
    takeLatest(actions.INITIALIZE, initialize),
    takeLatest(actions.SUMBIT_CONTENT_FORM, submitContentForm),
    takeLatest(actions.SET_SELECTED_LOCALE, updateDisplayedFields),
    takeLatest(actions.SET_SELECTED_PROVIDER, updateDisplayedFields),
    debounce(500, formActionTypes.CHANGE, onChange)
  ])
}

export function* initialize() {
  yield call(initializeAction)
  yield call(initializeForm)

  // initially all locale fields must be shown that they are registered by redux-forms
  // else the mandatory validation does not work for the selected correspondence language in the dropdown
  // (e.g. the subject is mandatory, so if recipient locales are german & english and the correspondence language french
  // is selected, the mandatory of the french subject is ignored during validating the form). To avoid such issues
  // wait until the registration is done by redux forms and then only show recipient locales, so dropdown must be empty
  yield take(formActionTypes.UPDATE_SYNC_ERRORS)
  yield put(actions.setSelectedLocale(null))
}

export function* initializeAction() {
  const {selection, actionProperties} = yield select(inputSelector)

  const {
    body: {recipientLocales, locales, providers, defaultTemplate, emailSenders}
  } = yield call(rest.requestSaga, 'address/actions/mail/initialize', {
    method: 'POST',
    body: {
      selection,
      formProperties: actionProperties
    }
  })

  yield put(actions.initializeLocale(recipientLocales, locales))
  yield put(actions.initializeProviders(providers))
  const initalFormValues = defaultTemplate ? {relEmail_template: defaultTemplate} : {}
  yield put(actions.setInitalFormValues(initalFormValues))
  yield put(actions.setAvailableEmailSenders(emailSenders))
}

export function* initializeForm() {
  const {selection} = yield select(inputSelector)
  const {availableLocales, initalFormValues} = yield select(mailSelector)

  const formDefinition = yield call(rest.fetchForm, 'Mail_action_settings', 'create')
  const {pseudoEntityPaths, formReplacements} = yield call(getPseudoFields)
  const initialFormDefinition = yield call(
    getInitialFormDefinition,
    formDefinition,
    selection,
    availableLocales,
    formReplacements
  )
  yield put(actions.setFormDefinition(initialFormDefinition))
  yield put(actions.setInitialFormDefinition(initialFormDefinition))

  const fieldDefinitions = yield call(form.getFieldDefinitions, initialFormDefinition)
  yield put(actions.setFieldDefinitions(fieldDefinitions))
  const settings = {model: 'Mail_action_settings', paths: pseudoEntityPaths}
  const entityValues = yield call(api.getFlattenEntity, settings)
  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const values = {...entityValues, ...defaultValues, ...initalFormValues}
  const formValues = yield call(form.entityToFormValues, values, fieldDefinitions)

  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* getPseudoFields() {
  const {availableEmailSenders} = yield select(mailSelector)

  const pseudoFields = [
    getSenderPseudoField('relEmail_sender', 'email_sender', availableEmailSenders, 'default'),
    ...(Object.keys(availableEmailSenders).includes('incamail')
      ? [getSenderPseudoField('relInca_mail_address', 'inca_mail_address', availableEmailSenders, 'incamail')]
      : [])
  ]

  return {
    pseudoEntityPaths: form.getMergedEntityPaths(pseudoFields),
    formReplacements: pseudoFields.reduce(
      (curr, {idToReplace, formField, adjuster = () => {}}) => ({...curr, [idToReplace]: {formField, adjuster}}),
      {}
    )
  }
}

const getSenderPseudoField = (path, label, availableEmailSenders, provider) => {
  const emailSenders = availableEmailSenders[provider]
  const senderOptions = emailSenders.map(({id, label: senderLabel, selected}) =>
    form.createSelectOption(id, senderLabel, selected)
  )

  return {
    ...form.createSelectField(path, senderOptions, label, false, true),
    idToReplace: path,
    adjuster: (original, replacment) => {
      replacment.label = original.label
    }
  }
}

export function* onChange({meta}) {
  const formValues = yield call(loadFormValues)
  const {localeMap, providerMap} = yield select(mailSelector)
  if (meta.field === 'relCorrespondence_language') {
    const uniqueId = localeMap[formValues.relCorrespondence_language?.key]
    yield put(actions.setSelectedLocale(uniqueId))
  } else if (meta.field === 'relEmail_provider') {
    const uniqueId = providerMap[formValues.relEmail_provider?.key]
    yield put(actions.setSelectedProvider(uniqueId))
  }
}

// helper function to allow easy mocking in test
export function* loadFormValues() {
  return yield select(getFormValues(REDUX_FORM_NAME))
}

export function* updateDisplayedFields() {
  const {initialFormDefinition, showedLocales, selectedProvider} = yield select(mailSelector)
  const modifiedFormDefinition = form.adjustFieldSets(initialFormDefinition, item => {
    if (item.id === form.transformPath('relEmail_sender') && selectedProvider === 'incamail') {
      return {
        ...item,
        hidden: true
      }
    }

    if (item.id === form.transformPath('relInca_mail_address') && selectedProvider !== 'incamail') {
      return {
        ...item,
        hidden: true
      }
    }

    if (
      !item.id.startsWith('subject_') &&
      !item.id.startsWith('html_text_') &&
      !item.id.startsWith('plain_text_') &&
      !item.id.startsWith('attachment_')
    ) {
      return item
    }

    return {
      ...item,
      hidden: !showedLocales.some(locale => item.id.endsWith(locale))
    }
  })

  const fieldDefinitions = yield call(form.getFieldDefinitions, modifiedFormDefinition)

  yield put(actions.setFormDefinition(modifiedFormDefinition))
  yield put(actions.setFieldDefinitions(fieldDefinitions))
}

export function* submitContentForm() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const bean = yield call(mapFormValuesToBean, formValues)

  const response = yield call(rest.requestSaga, 'address/actions/mail/send', {
    method: 'POST',
    body: bean,
    acceptedStatusCodes: [412]
  })

  if (response.status === 204) {
    yield put(externalEvents.fireExternalEvent('onSuccess'))
  } else if (response.status === 412 && response.body?.errorMessageKey) {
    yield put(
      notification.toaster({
        type: 'warning',
        title: 'client.actions.mail.error.header',
        body: `client.actions.mail.error.${response.body.errorMessageKey}`
      })
    )
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    yield put(externalEvents.fireExternalEvent('onError'))
  }
}

export function* mapFormValuesToBean(formValues) {
  const {selection, actionProperties} = yield select(inputSelector)
  const {availableLocales, selectedProvider} = yield select(mailSelector)

  return {
    mailSettings: {
      provider: selectedProvider,
      senderId: getSelectedSender(formValues, selectedProvider),
      expiryDate: formValues.expiry_date,
      contactCategoryKey: formValues.relContact_category?.key,
      languageKey: formValues.relCorrespondence_language?.key,
      localized: availableLocales.reduce(
        (acc, locale) => ({
          ...acc,
          [locale]: {
            subject: formValues[`subject_${locale}`],
            htmlText: formValues[`html_text_${locale}`],
            plainText: formValues[`plain_text_${locale}`],
            ...(formValues[`attachment_${locale}`] ? {attachments: formValues[`attachment_${locale}`]} : {})
          }
        }),
        {}
      ),
      formProperties: actionProperties
    },
    selection
  }
}

const getSelectedSender = (formValues, selectedProvider) => {
  let field
  if (selectedProvider === 'incamail') {
    field = form.transformPath('relInca_mail_address')
  } else {
    field = form.transformPath('relEmail_sender')
  }
  const [{key}] = formValues[field].options.filter(({selected}) => selected)
  return key
}
