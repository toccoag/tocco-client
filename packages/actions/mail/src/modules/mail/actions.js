export const INITIALIZE = 'mail/INITIALIZE'
export const SET_INITIAL_FORM_VALUES = 'mail/SET_INITIAL_FORM_VALUES'
export const SET_INITIAL_FORM_DEFINITION = 'mail/SET_INITIAL_FORM_DEFINITION'
export const SET_FORM_DEFINITION = 'mail/SET_FORM_DEFINITION'
export const SET_FIELD_DEFINITIONS = 'mail/SET_FIELD_DEFINITIONS'
export const SUMBIT_CONTENT_FORM = 'mail/SUMBIT_CONTENT_FORM'
export const INITIALIZE_LOCALE = 'mail/INITIALIZE_LOCALE'
export const SET_SELECTED_LOCALE = 'mail/SET_SELECTED_LOCALE'
export const INITIALIZE_PROVIDERS = 'mail/INITIALIZE_PROVIDERS'
export const SET_SELECTED_PROVIDER = 'mail/SET_SELECTED_PROVIDER'
export const SET_AVAILABLE_EMAIL_SENDERS = 'mail/SET_AVAILABLE_EMAIL_SENDERS'

export const initialize = () => ({
  type: INITIALIZE
})

export const setInitalFormValues = initalFormValues => ({
  type: SET_INITIAL_FORM_VALUES,
  payload: {
    initalFormValues
  }
})

export const setInitialFormDefinition = initialFormDefinition => ({
  type: SET_INITIAL_FORM_DEFINITION,
  payload: {
    initialFormDefinition
  }
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {
    formDefinition
  }
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {
    fieldDefinitions
  }
})

export const submitContentForm = () => ({
  type: SUMBIT_CONTENT_FORM
})

export const initializeLocale = (recipientLocales, locales) => ({
  type: INITIALIZE_LOCALE,
  payload: {
    recipientLocales,
    locales
  }
})

export const setSelectedLocale = selectedLocale => ({
  type: SET_SELECTED_LOCALE,
  payload: {
    selectedLocale
  }
})

export const initializeProviders = providers => ({
  type: INITIALIZE_PROVIDERS,
  payload: {
    providers
  }
})

export const setSelectedProvider = selectedProvider => ({
  type: SET_SELECTED_PROVIDER,
  payload: {
    selectedProvider
  }
})

export const setAvailableEmailSenders = availableEmailSenders => ({
  type: SET_AVAILABLE_EMAIL_SENDERS,
  payload: {
    availableEmailSenders
  }
})
