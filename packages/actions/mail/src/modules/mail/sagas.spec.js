import {actions as formActions} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {all, debounce, takeLatest, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {form, externalEvents, rest, notification} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ContentForm'
import {getInitialFormDefinition} from '../../util/form'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('mail', () => {
  describe('sagas', () => {
    describe('rootSaga', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([
            takeLatest(actions.INITIALIZE, sagas.initialize),
            takeLatest(actions.SUMBIT_CONTENT_FORM, sagas.submitContentForm),
            takeLatest(actions.SET_SELECTED_LOCALE, sagas.updateDisplayedFields),
            takeLatest(actions.SET_SELECTED_PROVIDER, sagas.updateDisplayedFields),
            debounce(500, formActionTypes.CHANGE, sagas.onChange)
          ])
        )
        expect(generator.next().done).to.equal(true)
      })
    })

    describe('initialize', () => {
      test('initialize', () => {
        return expectSaga(sagas.initialize)
          .provide([
            [matchers.call.fn(sagas.initializeAction), {}],
            [matchers.call.fn(sagas.initializeForm), {}],
            {
              take() {}
            }
          ])
          .take(formActionTypes.UPDATE_SYNC_ERRORS)
          .put(actions.setSelectedLocale(null))
          .run()
      })
    })

    describe('initializeAction', () => {
      test('initializeAction', () => {
        const selection = {entityName: 'User', type: 'ID', ids: ['1']}
        const actionProperties = {
          outputLanguagePath: 'relCorrespondence_language'
        }
        const bean = {
          selection,
          formProperties: actionProperties
        }
        const recipientLocales = ['de']
        const locales = [
          {key: 1, uniqueId: 'de'},
          {key: 2, uniqueId: 'en'}
        ]
        const defaultTemplate = {key: '1', display: 'default template'}
        const initalFormValues = {
          relEmail_template: defaultTemplate
        }
        const providers = [{key: '1', uniqueId: 'default'}]
        const emailSenders = [{key: 'sender'}]
        const response = {body: {recipientLocales, locales, defaultTemplate, emailSenders, providers}}
        return expectSaga(sagas.initializeAction)
          .provide([
            [select(sagas.inputSelector), {selection, actionProperties}],
            [matchers.call.fn(rest.requestSaga), response]
          ])
          .call(rest.requestSaga, 'address/actions/mail/initialize', {method: 'POST', body: bean})
          .put(actions.initializeLocale(recipientLocales, locales))
          .put(actions.initializeProviders(providers))
          .put(actions.setInitalFormValues(initalFormValues))
          .put(actions.setAvailableEmailSenders(emailSenders))
          .run()
      })
    })

    describe('initializeForm', () => {
      test('initializeForm', () => {
        const selection = {entityName: 'User', type: 'ID', ids: ['1']}
        const formDefinition = {
          id: 'Mail_action_settings_create',
          children: []
        }
        const formValues = {
          subject_de: 'Subject'
        }
        const availableLocales = ['de', 'en']
        const initalFormValues = {}
        const pseudoEntityPaths = {}
        const formReplacements = {}
        return expectSaga(sagas.initializeForm)
          .provide([
            [select(sagas.inputSelector), {selection}],
            [select(sagas.mailSelector), {availableLocales, initalFormValues}],
            [matchers.call(rest.fetchForm, 'Mail_action_settings', 'create'), formDefinition],
            [matchers.call.fn(getInitialFormDefinition), formDefinition],
            [matchers.call.fn(form.getFieldDefinitions), []],
            [matchers.call.fn(api.getFlattenEntity), {}],
            [matchers.call.fn(form.getDefaultValues), {}],
            [matchers.call.fn(form.entityToFormValues), formValues],
            [matchers.call.fn(sagas.getPseudoFields), {pseudoEntityPaths, formReplacements}]
          ])
          .put(actions.setFormDefinition(formDefinition))
          .put(actions.setInitialFormDefinition(formDefinition))
          .put(formActions.initialize(REDUX_FORM_NAME, formValues))
          .put(actions.setFieldDefinitions([]))
          .run()
      })
    })

    describe('getPseudoFields', () => {
      test('should create email sender field', async () => {
        const availableEmailSenders = {
          default: [
            {id: 'sender selected', label: 'sender selected label', selected: true},
            {id: 'sender available', label: 'sender available label', selected: false}
          ],
          incamail: []
        }
        const expectedOptions = [
          {key: 'sender selected', display: 'sender selected label', selected: true},
          {key: 'sender available', display: 'sender available label', selected: false}
        ]
        return expectSaga(sagas.getPseudoFields)
          .provide([[select(sagas.mailSelector), {availableEmailSenders}]])
          .run()
          .then(({returnValue: {pseudoEntityPaths, formReplacements}}) => {
            const senderPseudoEntityPath = pseudoEntityPaths[form.transformPath('relEmail_sender')]
            expect(senderPseudoEntityPath.type).eql('pseudo-select')
            expect(senderPseudoEntityPath.value.options).eql(expectedOptions)
            expect(senderPseudoEntityPath.writable).eql(true)
            const {formField: senderFormField, adjuster: senderAdjuster} = formReplacements.relEmail_sender
            senderAdjuster({label: 'adjusted label'}, senderFormField)
            expect(senderFormField.id).eql(form.transformPath('relEmail_sender'))
            expect(senderFormField.label).eql('adjusted label')
            expect(senderFormField.children[0].dataType).eql('pseudo-select')
            expect(senderFormField.children[0].validation.mandatory).eql(true)

            const incamailPseudoEntityPath = pseudoEntityPaths[form.transformPath('relInca_mail_address')]
            expect(incamailPseudoEntityPath.type).eql('pseudo-select')
            expect(incamailPseudoEntityPath.value.options).eql([])
            expect(incamailPseudoEntityPath.writable).eql(true)
            const {formField: incamailFormField, adjuster: incamailAdjuster} = formReplacements.relInca_mail_address
            incamailAdjuster({label: 'adjusted label'}, incamailFormField)
            expect(incamailFormField.id).eql(form.transformPath('relInca_mail_address'))
            expect(incamailFormField.label).eql('adjusted label')
            expect(incamailFormField.children[0].dataType).eql('pseudo-select')
            expect(incamailFormField.children[0].validation.mandatory).eql(true)
          })
      })
    })

    describe('onChange', () => {
      test('correspondence language changed', () => {
        const meta = {field: 'relCorrespondence_language'}
        const formValues = {
          relCorrespondence_language: {
            key: 2
          }
        }
        const localeMap = {
          1: 'de',
          2: 'en'
        }
        return expectSaga(sagas.onChange, {meta})
          .provide([
            [matchers.call.fn(sagas.loadFormValues), formValues],
            [select(sagas.mailSelector), {localeMap}]
          ])
          .put(actions.setSelectedLocale('en'))
          .run()
      })

      test('email provider changed', () => {
        const meta = {field: 'relEmail_provider'}
        const formValues = {
          relEmail_provider: {
            key: 1
          }
        }
        const providerMap = {
          1: 'default',
          2: 'incamail'
        }
        return expectSaga(sagas.onChange, {meta})
          .provide([
            [matchers.call.fn(sagas.loadFormValues), formValues],
            [select(sagas.mailSelector), {providerMap}]
          ])
          .put(actions.setSelectedProvider('default'))
          .run()
      })

      test('other field changed', () => {
        const meta = {field: 'other_field'}
        const formValues = {}
        return expectSaga(sagas.onChange, {meta})
          .provide([
            [matchers.call.fn(sagas.loadFormValues), formValues],
            [select(sagas.mailSelector), {}]
          ])
          .not.put.like({action: {type: actions.SET_SELECTED_LOCALE}})
          .run()
      })
    })

    describe('updateDisplayedFields', () => {
      test('should only localized fields of selected locale', () => {
        const initialFormDefinition = {
          children: [
            {
              componentType: 'field-set',
              id: 'expiry_date'
            },
            {
              componentType: 'field-set',
              id: 'subject_de',
              hidden: true
            },
            {
              componentType: 'field-set',
              id: 'subject_en',
              hidden: false
            }
          ]
        }
        const showedLocales = ['de']
        const fieldDefinitions = [{id: 'expiry_date'}, {id: 'subject_de'}]
        const modifiedFormDefinition = {
          children: [
            {
              componentType: 'field-set',
              id: 'expiry_date'
            },
            {
              componentType: 'field-set',
              id: 'subject_de',
              hidden: false
            },
            {
              componentType: 'field-set',
              id: 'subject_en',
              hidden: true
            }
          ]
        }
        return expectSaga(sagas.updateDisplayedFields)
          .provide([
            [select(sagas.mailSelector), {initialFormDefinition, showedLocales}],
            [matchers.call.fn(form.getFieldDefinitions), fieldDefinitions]
          ])
          .put(actions.setFormDefinition(modifiedFormDefinition))
          .put(actions.setFieldDefinitions(fieldDefinitions))
          .run()
      })

      describe('mail provider', () => {
        const initialFormDefinition = {
          children: [
            {
              componentType: 'field-set',
              id: form.transformPath('relEmail_sender')
            },
            {
              componentType: 'field-set',
              id: form.transformPath('relInca_mail_address')
            }
          ]
        }
        const fieldDefinitions = [{id: 'relEmail_sender'}, {id: 'relInca_mail_address'}]

        const expectUpdateDisplayedFields = (selectedProvider, modifiedFormDefinition) =>
          expectSaga(sagas.updateDisplayedFields)
            .provide([
              [select(sagas.mailSelector), {initialFormDefinition, selectedProvider}],
              [matchers.call.fn(form.getFieldDefinitions), fieldDefinitions]
            ])
            .put(actions.setFormDefinition(modifiedFormDefinition))
            .put(actions.setFieldDefinitions(fieldDefinitions))
            .run()

        test('should only show default sender', () => {
          const modifiedFormDefinition = {
            children: [
              {
                componentType: 'field-set',
                id: form.transformPath('relEmail_sender')
              },
              {
                componentType: 'field-set',
                id: form.transformPath('relInca_mail_address'),
                hidden: true
              }
            ]
          }
          return expectUpdateDisplayedFields('default', modifiedFormDefinition)
        })

        test('should show default sender if no provider is selected', () => {
          const modifiedFormDefinition = {
            children: [
              {
                componentType: 'field-set',
                id: form.transformPath('relEmail_sender')
              },
              {
                componentType: 'field-set',
                id: form.transformPath('relInca_mail_address'),
                hidden: true
              }
            ]
          }
          return expectUpdateDisplayedFields(null, modifiedFormDefinition)
        })

        test('should only show incamail sender', () => {
          const modifiedFormDefinition = {
            children: [
              {
                componentType: 'field-set',
                id: form.transformPath('relEmail_sender'),
                hidden: true
              },
              {
                componentType: 'field-set',
                id: form.transformPath('relInca_mail_address')
              }
            ]
          }
          return expectUpdateDisplayedFields('incamail', modifiedFormDefinition)
        })
      })
    })

    describe('submitContentForm', () => {
      const bean = {
        subjectDe: 'Subject'
      }

      test('successful', () => {
        const response = {status: 204}
        return expectSaga(sagas.submitContentForm)
          .provide([
            [matchers.call.fn(sagas.mapFormValuesToBean), bean],
            [matchers.call.fn(rest.requestSaga), response]
          ])
          .put(formActions.startSubmit(REDUX_FORM_NAME))
          .call(rest.requestSaga, 'address/actions/mail/send', {method: 'POST', body: bean, acceptedStatusCodes: [412]})
          .put(externalEvents.fireExternalEvent('onSuccess'))
          .run()
      })

      test('validation', () => {
        const response = {status: 412, body: {errorMessageKey: 'message'}}
        return expectSaga(sagas.submitContentForm)
          .provide([
            [matchers.call.fn(sagas.mapFormValuesToBean), bean],
            [matchers.call.fn(rest.requestSaga), response]
          ])
          .put(formActions.startSubmit(REDUX_FORM_NAME))
          .call(rest.requestSaga, 'address/actions/mail/send', {method: 'POST', body: bean, acceptedStatusCodes: [412]})
          .put(formActions.stopSubmit(REDUX_FORM_NAME))
          .put(
            notification.toaster({
              type: 'warning',
              title: 'client.actions.mail.error.header',
              body: 'client.actions.mail.error.message'
            })
          )
          .run()
      })

      test('412 without message', () => {
        const response = {status: 412}
        return expectSaga(sagas.submitContentForm)
          .provide([
            [matchers.call.fn(sagas.mapFormValuesToBean), bean],
            [matchers.call.fn(rest.requestSaga), response]
          ])
          .put(formActions.startSubmit(REDUX_FORM_NAME))
          .call(rest.requestSaga, 'address/actions/mail/send', {method: 'POST', body: bean, acceptedStatusCodes: [412]})
          .put(formActions.stopSubmit(REDUX_FORM_NAME))
          .put(externalEvents.fireExternalEvent('onError'))
          .run()
      })

      test('error', () => {
        const response = {status: 400}
        return expectSaga(sagas.submitContentForm)
          .provide([
            [matchers.call.fn(sagas.mapFormValuesToBean), bean],
            [matchers.call.fn(rest.requestSaga), response]
          ])
          .put(formActions.startSubmit(REDUX_FORM_NAME))
          .call(rest.requestSaga, 'address/actions/mail/send', {method: 'POST', body: bean, acceptedStatusCodes: [412]})
          .put(formActions.stopSubmit(REDUX_FORM_NAME))
          .put(externalEvents.fireExternalEvent('onError'))
          .run()
      })
    })

    describe('mapFormValuesToBean', () => {
      const selection = {entityName: 'User', type: 'ID', ids: ['1']}
      const actionProperties = {
        outputLanguagePath: 'relCorrespondence_language'
      }
      const availableLocales = ['de', 'en']
      const selectedProvider = 'default'

      test('only mandatory fields', () => {
        const formValues = {
          relIrrelevant: {key: '2'},
          relUser: {key: '1'},
          subject_de: 'subject de',
          subject_en: 'subject en',
          html_text_de: 'html de',
          html_text_en: 'html en',
          plain_text_de: 'plain de',
          plain_text_en: 'plain en',
          [form.transformPath('relEmail_sender')]: {
            options: [
              {key: 'selected', selected: true},
              {key: 'not-selected', selected: false}
            ]
          }
        }
        const expectedReturned = {
          mailSettings: {
            provider: 'default',
            senderId: 'selected',
            expiryDate: undefined,
            contactCategoryKey: undefined,
            languageKey: undefined,
            localized: {
              de: {
                subject: 'subject de',
                htmlText: 'html de',
                plainText: 'plain de'
              },
              en: {
                subject: 'subject en',
                htmlText: 'html en',
                plainText: 'plain en'
              }
            },
            formProperties: actionProperties
          },
          selection
        }
        return expectSaga(sagas.mapFormValuesToBean, formValues)
          .provide([
            [select(sagas.inputSelector), {selection, actionProperties}],
            [select(sagas.mailSelector), {availableLocales, selectedProvider}]
          ])
          .returns(expectedReturned)
          .run()
      })

      test('all fields', () => {
        const formValues = {
          relUser: {key: '1'},
          expiry_date: '2024-12-17',
          relContact_category: {key: '2'},
          relCorrespondence_language: {key: '3'},
          subject_de: 'subject de',
          subject_en: 'subject en',
          html_text_de: 'html de',
          html_text_en: 'html en',
          plain_text_de: 'plain de',
          plain_text_en: 'plain en',
          attachment_de: [{id: 'attachment-de'}],
          attachment_en: [{id: 'attachment-en'}],
          [form.transformPath('relEmail_sender')]: {
            options: [
              {key: 'selected', selected: true},
              {key: 'not-selected', selected: false}
            ]
          }
        }
        const expectedReturned = {
          mailSettings: {
            provider: 'default',
            senderId: 'selected',
            expiryDate: '2024-12-17',
            contactCategoryKey: '2',
            languageKey: '3',
            localized: {
              de: {
                subject: 'subject de',
                htmlText: 'html de',
                plainText: 'plain de',
                attachments: [{id: 'attachment-de'}]
              },
              en: {
                subject: 'subject en',
                htmlText: 'html en',
                plainText: 'plain en',
                attachments: [{id: 'attachment-en'}]
              }
            },
            formProperties: actionProperties
          },
          selection
        }
        return expectSaga(sagas.mapFormValuesToBean, formValues)
          .provide([
            [select(sagas.inputSelector), {selection, actionProperties}],
            [select(sagas.mailSelector), {availableLocales, selectedProvider}]
          ])
          .returns(expectedReturned)
          .run()
      })
    })
  })
})
