import {form} from 'tocco-app-extensions'

export const getInitialFormDefinition = (formDefinition, selection, availableLocales, formReplacements) => {
  return [
    fd => form.adjustFields(fd, field => addEmailTemplateCondition(field, selection)),
    fd => form.adjustFieldSets(fd, fieldSet => replaceFieldSet(fieldSet, formReplacements)),
    fd => form.replaceBoxChildren(fd, 'content', getLocalizedContentBoxes(formDefinition, availableLocales)),
    fd => form.adjustFields(fd, field => mapBinaryToPseudoMultiBinary(field))
  ].reduce((fd, adjuster) => {
    return adjuster(fd)
  }, formDefinition)
}

const addEmailTemplateCondition = (field, selection) => {
  if (field.path !== 'relEmail_template') {
    return field
  }
  return {
    ...field,
    condition: `
      active and
      (not(exists(relEmail_module)) or relEmail_module.entity_name == "${selection.entityName}") and
      not IN(relEmail_template_type.unique_id, "fix", "fix_without_contact") and
      (relUser.pk == :currentUser or
        relRole.relLogin_role.relPrincipal.relUser.pk == :currentUser or
        (not(exists(relUser)) and not(exists(relRole)))
      )
    `
  }
}

const replaceFieldSet = (fieldSet, formReplacements) => {
  const replacement = formReplacements[fieldSet.id]
  if (replacement) {
    const {formField, adjuster} = replacement
    adjuster(fieldSet, formField)
    return formField
  } else {
    return fieldSet
  }
}

const getLocalizedContentBoxes = (formDefinition, availableLocales) => {
  const contentTemplate = form.findInChildren(formDefinition, form.idSelector('content_template'))[0]
  return availableLocales.map(locale => getLocalizedContentBox(contentTemplate, locale))
}

const getLocalizedContentBox = (contentTemplate, locale) => {
  const result = form.adjustFieldSets(contentTemplate, item => {
    return {
      ...item,
      id: `${item.id}_${locale}`,
      children: item.children.map(c => ({
        ...c,
        id: `${c.id}_${locale}`,
        path: `${c.path}_${locale}`
      }))
    }
  })

  return {
    ...result,
    id: `content_${locale}`,
    label: `${result.label} (${locale.toUpperCase()})`
  }
}

const mapBinaryToPseudoMultiBinary = field => {
  if (field.id?.startsWith('attachment')) {
    return {
      ...field,
      dataType: 'pseudo-multi-document'
    }
  } else {
    return field
  }
}
