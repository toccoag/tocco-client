import {getInitialFormDefinition} from './form'

describe('mail', () => {
  describe('util', () => {
    describe('form', () => {
      describe('getInitialFormDefinition', () => {
        const selection = {entityName: 'User', type: 'ID', ids: ['1']}
        const availableLocales = ['de', 'en']

        test('add condition to email template field', () => {
          const formDefinition = {
            children: [
              {
                path: 'relEmail_template',
                componentType: 'field'
              },
              {
                id: 'content_template',
                children: []
              }
            ]
          }
          const expectedFormDefinition = {
            children: [
              {
                path: 'relEmail_template',
                componentType: 'field',
                condition: `
      active and
      (not(exists(relEmail_module)) or relEmail_module.entity_name == "User") and
      not IN(relEmail_template_type.unique_id, "fix", "fix_without_contact") and
      (relUser.pk == :currentUser or
        relRole.relLogin_role.relPrincipal.relUser.pk == :currentUser or
        (not(exists(relUser)) and not(exists(relRole)))
      )
    `
              },
              {
                id: 'content_template',
                children: []
              }
            ]
          }
          expect(getInitialFormDefinition(formDefinition, selection, availableLocales, {})).to.eql(
            expectedFormDefinition
          )
        })

        test('create a content box per locale', () => {
          const createBox = (id, labelSuffix = '', idSuffix = '', isExpected = false) => ({
            id,
            label: `label${labelSuffix}`,
            children: [
              {
                id: `subject${idSuffix}`,
                componentType: 'field-set',
                children: [
                  {
                    id: `subject${idSuffix}`,
                    path: `subject${idSuffix}`,
                    componentType: 'field'
                  }
                ]
              },
              {
                id: `html_text${idSuffix}`,
                componentType: 'field-set',
                children: [
                  {
                    id: `html_text${idSuffix}`,
                    path: `html_text${idSuffix}`,
                    componentType: 'field'
                  }
                ]
              },
              {
                id: `attachment${idSuffix}`,
                componentType: 'field-set',
                children: [
                  {
                    id: `attachment${idSuffix}`,
                    path: `attachment${idSuffix}`,
                    componentType: 'field',
                    ...(isExpected ? {dataType: 'pseudo-multi-document'} : {})
                  }
                ]
              }
            ]
          })

          const formDefinition = {
            children: [
              {
                id: 'content',
                children: [createBox('content_template')]
              }
            ]
          }
          const expectedFormDefinition = {
            children: [
              {
                id: 'content',
                children: [createBox('content_de', ' (DE)', '_de', true), createBox('content_en', ' (EN)', '_en', true)]
              }
            ]
          }
          expect(getInitialFormDefinition(formDefinition, selection, availableLocales, {})).to.eql(
            expectedFormDefinition
          )
        })

        test('replace field sets', () => {
          const formDefinition = {
            children: [
              {
                id: 'toReplace',
                label: 'previous label',
                componentType: 'field-set'
              },
              {
                id: 'content_template',
                children: []
              }
            ]
          }
          const expectedFormDefinition = {
            children: [
              {
                id: 'replaced',
                label: 'previous label'
              },
              {
                id: 'content_template',
                children: []
              }
            ]
          }
          expect(
            getInitialFormDefinition(formDefinition, selection, availableLocales, {
              toReplace: {
                formField: {
                  id: 'replaced'
                },
                adjuster: (previous, replacement) => {
                  replacement.label = previous.label
                }
              }
            })
          ).to.eql(expectedFormDefinition)
        })

        test('do nothing for other fields', () => {
          const formDefinition = {
            children: [
              {
                path: 'other_field',
                componentType: 'field'
              },
              {
                id: 'content_template',
                children: []
              }
            ]
          }
          expect(getInitialFormDefinition(formDefinition, selection, availableLocales, {})).to.eql(formDefinition)
        })
      })
    })
  })
})
