import PropTypes from 'prop-types'
import {
  appFactory,
  selection,
  errorLogging,
  externalEvents,
  actionEmitter,
  notification,
  form,
  formData
} from 'tocco-app-extensions'
import {chooseDocument} from 'tocco-docs-browser/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import ContentView from './components/ContentView'
import reducers, {sagas, formSagaConfig} from './modules/reducers'

const packageName = 'mail'

const EXTERNAL_EVENTS = ['onSuccess', 'onError', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  const content = <ContentView />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({
    listApp: EntityListApp,
    chooseDocument: (...args) => store.dispatch(chooseDocument.actions.chooseDocument(...args))
  }))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  errorLogging.addToStore(store, handleNotifications, ['console', 'remote', 'notification'])
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  form.addToStore(store, formSagaConfig)
  chooseDocument.addToStore(store)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const MailApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

MailApp.propTypes = {
  selection: selection.propType.isRequired,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}

export default MailApp
