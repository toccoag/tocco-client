import _capitalize from 'lodash/capitalize'
import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useSearchParams} from 'react-router-dom'

const validPaymentStatus = ['success', 'failed']

/**
 * this component currently displays nothing and only fires an external event
 * it might be expanded in future to show some info from the payment provider
 * and this ensures that no other page is displayed when returning from payment provider
 */
const PaymentResult = ({fireExternalEvent}) => {
  const [searchParams, setSearchParams] = useSearchParams()
  const paymentStatus = searchParams.get('paymentStatus')
  useEffect(() => {
    if (validPaymentStatus.includes(paymentStatus)) {
      fireExternalEvent(`onPayment${_capitalize(paymentStatus)}Page`)
    } else if (paymentStatus === 'none') {
      // only fire success if payment method does not use payment provider (e.g. invoice)
      fireExternalEvent('onSuccess')
    } else if (!paymentStatus) {
      // update search params so a reload will still display the success page
      setSearchParams(previous => ({...Object.fromEntries(previous.entries()), paymentStatus: 'none'}))
    } else {
      fireExternalEvent('onError', {message: `Unknown payment status '${paymentStatus}'`})
    }
  }, [paymentStatus, fireExternalEvent, setSearchParams])
  return <></>
}

PaymentResult.propTypes = {
  fireExternalEvent: PropTypes.func.isRequired
}

export default PaymentResult
