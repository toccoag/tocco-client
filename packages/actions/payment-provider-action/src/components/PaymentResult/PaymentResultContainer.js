import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import PaymentResult from './PaymentResult'

const mapActionCreators = {
  fireExternalEvent: externalEvents.fireExternalEvent
}

export default connect(null, mapActionCreators)(PaymentResult)
