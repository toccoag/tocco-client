import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import PaymentStatusHelper from '../../utils/PaymentStatusHelper'

import PaymentResult from './PaymentResult'

describe('payment-provider-action', () => {
  describe('PaymentResult', () => {
    test('should fire external payment event when result page is displayed', () => {
      const fireExternalEvent = sinon.spy()
      testingLibrary.renderWithIntl(
        <MemoryRouter>
          <PaymentStatusHelper initialStatus={'success'} />
          <PaymentResult fireExternalEvent={fireExternalEvent} />
        </MemoryRouter>
      )
      expect(fireExternalEvent).to.have.been.calledWith('onPaymentSuccessPage')
    })

    test('should not fire external payment event when bogus result is set', () => {
      const fireExternalEvent = sinon.spy()
      testingLibrary.renderWithIntl(
        <MemoryRouter>
          <PaymentStatusHelper initialStatus={'boop'} />
          <PaymentResult fireExternalEvent={fireExternalEvent} />
        </MemoryRouter>
      )
      expect(fireExternalEvent).to.have.been.calledWith('onError', {message: "Unknown payment status 'boop'"})
    })

    test('should fire external success as no payment provider is used', () => {
      const fireExternalEvent = sinon.spy()
      testingLibrary.renderWithIntl(
        <MemoryRouter>
          <PaymentStatusHelper />
          <PaymentResult fireExternalEvent={fireExternalEvent} />
        </MemoryRouter>
      )
      expect(fireExternalEvent).to.have.been.calledWith('onSuccess')
      expect(screen.getByText('payment status: none')).to.exist
    })
  })
})
