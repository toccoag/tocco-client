import {connect} from 'react-redux'

import {init} from '../../modules/paymentProviderAction/actions'

import PaymentProvider from './PaymentProvider'

const mapActionCreators = {
  init
}

const mapStateToProps = state => ({
  page: state.paymentProviderAction.page,
  orderUuid: state.paymentProviderAction.orderUuid
})

export default connect(mapStateToProps, mapActionCreators)(PaymentProvider)
