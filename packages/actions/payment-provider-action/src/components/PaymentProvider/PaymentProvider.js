import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useSearchParams} from 'react-router-dom'
import {LoadMask} from 'tocco-ui'

import {Pages} from '../../utils/constants'
import PaymentProviderLoad from '../PaymentProviderLoad'
import PaymentResult from '../PaymentResult'
import PaymentSummary from '../PaymentSummary'

const PaymentProvider = ({init, page, orderUuid}) => {
  const [searchParams] = useSearchParams()
  const paymentStatus = searchParams.get('paymentStatus')
  useEffect(() => {
    init(paymentStatus)
  }, [init, paymentStatus])

  return (
    <LoadMask required={[page, orderUuid]}>
      {(() => {
        switch (page) {
          case Pages.PaymentSummary:
            return <PaymentSummary />
          case Pages.PaymentProviderLoad:
            return <PaymentProviderLoad />
          case Pages.PaymentResult:
            return <PaymentResult />
          default:
            return null
        }
      })()}
    </LoadMask>
  )
}

PaymentProvider.propTypes = {
  init: PropTypes.func.isRequired,
  page: PropTypes.string,
  orderUuid: PropTypes.string
}

export default PaymentProvider
