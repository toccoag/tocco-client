import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import PaymentStatusHelper from '../../utils/PaymentStatusHelper'

import PaymentProvider from './PaymentProvider'

describe('payment-provider-action', () => {
  describe('PaymentProvider', () => {
    test('should run init on mount', async () => {
      const props = {
        init: sinon.spy(),
        setPage: sinon.spy(),
        page: undefined
      }

      testingLibrary.renderWithIntl(
        <MemoryRouter>
          <PaymentStatusHelper initialStatus="paymentStatus" />
          <PaymentProvider {...props} />
        </MemoryRouter>
      )
      await screen.findAllByTestId('icon')

      expect(props.init).to.have.been.calledWith('paymentStatus')
    })
  })
})
