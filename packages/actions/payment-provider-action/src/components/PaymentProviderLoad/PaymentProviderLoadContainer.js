import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import PaymentProviderLoad from './PaymentProviderLoad'

const mapStateToProps = state => ({
  paymentLink: state.paymentProviderAction.paymentLink
})

export default connect(mapStateToProps)(injectIntl(PaymentProviderLoad))
