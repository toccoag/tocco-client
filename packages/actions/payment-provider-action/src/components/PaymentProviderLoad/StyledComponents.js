import styled from 'styled-components'

export const StyledFallbackLink = styled.div`
  display: flex;
  justify-content: center;
`
