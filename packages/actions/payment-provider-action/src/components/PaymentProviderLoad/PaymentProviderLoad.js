import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {LoadMask, Link} from 'tocco-ui'

import {StyledFallbackLink} from './StyledComponents'

const PaymentProviderLoad = ({intl, paymentLink}) => {
  const msg = id => intl.formatMessage({id})
  return (
    <LoadMask
      required={[paymentLink]}
      loadingText={msg('client.actions.payment-provider-action.payment-provider-loading')}
    >
      {paymentLink && (
        <StyledFallbackLink>
          <Link href={paymentLink}>
            <FormattedMessage id="client.actions.payment-provider-action.payment-provider-fallback" />
          </Link>
        </StyledFallbackLink>
      )}
    </LoadMask>
  )
}

PaymentProviderLoad.propTypes = {
  paymentLink: PropTypes.string,
  intl: PropTypes.object.isRequired
}

export default PaymentProviderLoad
