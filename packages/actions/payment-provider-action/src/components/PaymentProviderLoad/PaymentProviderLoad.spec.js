import {screen} from '@testing-library/react'
import {testingLibrary, IntlStub} from 'tocco-test-util'

import PaymentProviderLoad from './PaymentProviderLoad'

describe('payment-provider-action', () => {
  describe('PaymentProviderLoad', () => {
    test('should show loading mask when waiting for payment provider', async () => {
      const props = {
        paymentLink: null,
        intl: IntlStub
      }

      testingLibrary.renderWithIntl(<PaymentProviderLoad {...props} />)
      await screen.findAllByTestId('icon')

      expect(screen.queryByText('client.actions.payment-provider-action.payment-provider-loading')).to.exist
    })

    test('should show payment link on loading page', () => {
      const props = {
        paymentLink: 'link',
        intl: IntlStub
      }

      testingLibrary.renderWithIntl(<PaymentProviderLoad {...props} />)

      const link = screen.queryByRole('link', {
        name: 'client.actions.payment-provider-action.payment-provider-fallback'
      })
      expect(link).to.exist
      jestExpect(link).toHaveAttribute('href', 'link')
    })
  })
})
