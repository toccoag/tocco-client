import styled from 'styled-components'
import {scale} from 'tocco-ui'

export const StyledPaymentMethod = styled.div`
  display: inline-flex;
  align-items: center;
  gap: ${scale.space(0)};
`

export const StyledPaymentMethodLogo = styled.img`
  height: 40px;
`
