import PropTypes from 'prop-types'
import {form} from 'tocco-app-extensions'
import {EditableValue, Typography} from 'tocco-ui'

import {StyledPaymentMethodLogo, StyledPaymentMethod} from './StyledComponents'

const PaymentMethod = ({label, logoUrl}) => {
  return (
    <StyledPaymentMethod>
      {logoUrl && <StyledPaymentMethodLogo src={logoUrl} alt={`${label} Logo`} />}
      <Typography.Span>{label}</Typography.Span>
    </StyledPaymentMethod>
  )
}

PaymentMethod.propTypes = {
  label: PropTypes.string.isRequired,
  logoUrl: PropTypes.string
}

const PaymentMethods = ({paymentMethods, selectedPaymentMethod, setSelectedPaymentMethod}) => (
  <EditableValue
    value={{
      options: paymentMethods.map(({key, ...paymentMethodProps}) => {
        return form.createChoiceOption(`${key}`, {
          labelComponent: <PaymentMethod key={key} {...paymentMethodProps} />,
          checked: selectedPaymentMethod === `${key}`
        })
      })
    }}
    type="choice"
    id="paymentMethods"
    events={{
      onChange: changedValue => {
        setSelectedPaymentMethod(changedValue.options.find(v => v.checked).id)
      }
    }}
    options={{selectionType: 'single'}}
  />
)

PaymentMethods.propTypes = {
  paymentMethods: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.number.isRequired,
      label: PropTypes.string.isRequired,
      logoUrl: PropTypes.string
    })
  ),
  selectedPaymentMethod: PropTypes.string,
  setSelectedPaymentMethod: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default PaymentMethods
