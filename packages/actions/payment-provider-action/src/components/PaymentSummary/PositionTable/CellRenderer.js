import PropTypes from 'prop-types'
import {FormattedNumber} from 'react-intl'
import {Typography} from 'tocco-ui'

const CellRenderer = ({rowData, column, type}) => {
  let value = rowData[column.id]
  const isFormattedNumber = type === 'moneyamount' && rowData[column.id]

  if (isFormattedNumber) {
    value = <FormattedNumber value={rowData[column.id]} style="decimal" minimumFractionDigits={2} />
  }

  const TypographyComponent = rowData.bold ? Typography.B : Typography.P

  return <TypographyComponent>{value}</TypographyComponent>
}

CellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired
}

export default CellRenderer
