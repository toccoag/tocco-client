import PropTypes from 'prop-types'
import {useMemo} from 'react'
import {Table} from 'tocco-ui'

import CellRenderer from './CellRenderer'

const createColumn = (id, label, type, options) => {
  return {
    id,
    label,
    resizable: false,
    fixedPosition: true,
    sorting: {
      sortable: false
    },
    dynamic: true,
    readOnly: true,
    CellRenderer: props => <CellRenderer type={type} options={{}} {...props} />,
    ...options
  }
}

const buildColumns = intl => {
  return [
    createColumn('label', intl.formatMessage({id: 'client.actions.payment-provider-action.summary.label'}), 'string', {
      alignment: 'left'
    }),
    createColumn('quantity', '', 'string', {
      alignment: 'right',
      width: 50
    }),
    createColumn('unitPrice', '', 'moneyamount', {
      alignment: 'right',
      width: 100
    }),
    createColumn(
      'total',
      intl.formatMessage({id: 'client.actions.payment-provider-action.summary.total'}),
      'moneyamount',
      {
        alignment: 'right',
        width: 100
      }
    )
  ]
}

const getDataRows = (order, intl) => [
  ...order.orderPositions.map((op, index) => ({
    __key: `order-position-${index}`,
    bold: false,
    label: op.label,
    quantity: `${op.quantity}x`,
    unitPrice: op.unitPrice,
    total: op.totalPrice
  })),
  ...(order.hasVat
    ? [
        {
          __key: `totalExclusive`,
          bold: true,
          label: intl.formatMessage({id: 'client.actions.payment-provider-action.summary.totalExclusive'}),
          total: order.totalNet
        },
        ...order.vatTotals.map((v, index) => ({
          __key: `vat-${index}`,
          bold: false,
          label: v.label,
          total: v.amount
        })),
        {
          __key: `totalInclusive`,
          bold: true,
          label: intl.formatMessage({id: 'client.actions.payment-provider-action.summary.totalInclusive'}),
          total: order.totalGross
        }
      ]
    : [
        {
          __key: `total`,
          bold: true,
          label: intl.formatMessage({id: 'client.actions.payment-provider-action.summary.total'}),
          total: order.totalGross
        }
      ])
]

const PositionTable = ({order, intl}) => {
  const data = useMemo(() => (order ? getDataRows(order, intl) : []), [order, intl])

  return (
    <Table
      columns={buildColumns(intl)}
      data={data}
      selectionStyle="none"
      onColumnPositionChange={() => {}}
      scrollBehaviour="none"
    />
  )
}

PositionTable.propTypes = {
  order: PropTypes.shape({
    pk: PropTypes.string.isRequired,
    hasVat: PropTypes.bool.isRequired,
    totalNet: PropTypes.number.isRequired,
    totalGross: PropTypes.number.isRequired,
    vatTotals: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        amount: PropTypes.number.isRequired
      })
    ),
    orderPositions: PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string.isRequired,
        quantity: PropTypes.number.isRequired,
        unitPrice: PropTypes.number.isRequired,
        totalPrice: PropTypes.number.isRequired
      })
    )
  }),
  intl: PropTypes.object.isRequired
}

export default PositionTable
