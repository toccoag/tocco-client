import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {Button, LoadMask, Panel, Typography, Layout} from 'tocco-ui'

import PaymentMethods from './PaymentMethods'
import PositionTable from './PositionTable'
import {StyledButtonWrapper, StyledPaymentSummaryWrapper} from './StyledComponents'

const PaymentSummary = ({
  overviewFormName,
  overviewEntityKey,
  overviewEntityName,
  order,
  paymentMethods,
  locale,
  initPaymentSummary,
  redirectToPayment,
  fireExternalEvent,
  intl
}) => {
  useEffect(() => {
    initPaymentSummary()
  }, [initPaymentSummary])

  useEffect(() => {
    fireExternalEvent('onPaymentSummaryPage')
  }, [fireExternalEvent])

  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState(null)

  return (
    <LoadMask required={[order, paymentMethods]}>
      <StyledPaymentSummaryWrapper>
        {overviewEntityKey && (
          <EntityDetailApp
            entityName={overviewEntityName}
            formName={overviewFormName}
            entityId={overviewEntityKey}
            mode="update"
            locale={locale}
          />
        )}
        <Layout.Container>
          <Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H3>
                    {intl.formatMessage({id: 'client.actions.payment-provider-action.summary.article'})}
                  </Typography.H3>
                </Panel.Header>
                <Panel.Body>
                  <PositionTable order={order} intl={intl} />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
            <Layout.Box>
              <Panel.Wrapper isToggleable={false}>
                <Panel.Header>
                  <Typography.H3>
                    {intl.formatMessage({id: 'client.actions.payment-provider-action.summary.paymentMethod'})}
                  </Typography.H3>
                </Panel.Header>
                <Panel.Body>
                  <PaymentMethods
                    paymentMethods={paymentMethods}
                    selectedPaymentMethod={selectedPaymentMethod}
                    setSelectedPaymentMethod={setSelectedPaymentMethod}
                    intl={intl}
                  />
                </Panel.Body>
              </Panel.Wrapper>
            </Layout.Box>
          </Layout.Box>
        </Layout.Container>
        <StyledButtonWrapper>
          <Button
            disabled={selectedPaymentMethod === null}
            label={intl.formatMessage({id: 'client.actions.payment-provider-action.summary.next'})}
            type="submit"
            look="raised"
            ink="primary"
            onClick={() => redirectToPayment(selectedPaymentMethod)}
            data-cy="payment-provider-next"
          />
        </StyledButtonWrapper>
      </StyledPaymentSummaryWrapper>
    </LoadMask>
  )
}

PaymentSummary.propTypes = {
  overviewFormName: PropTypes.string,
  overviewEntityKey: PropTypes.string,
  overviewEntityName: PropTypes.string,
  order: PropTypes.object,
  paymentMethods: PropTypes.array,
  locale: PropTypes.string,
  initPaymentSummary: PropTypes.func.isRequired,
  redirectToPayment: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default PaymentSummary
