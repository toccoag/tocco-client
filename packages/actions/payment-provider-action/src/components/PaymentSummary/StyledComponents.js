import styled from 'styled-components'

export const StyledButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
`

export const StyledPaymentSummaryWrapper = styled.div`
  form {
    padding-bottom: 0;
  }
`
