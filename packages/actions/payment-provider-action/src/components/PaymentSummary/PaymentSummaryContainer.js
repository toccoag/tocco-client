import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import {initPaymentSummary, redirectToPayment} from '../../modules/paymentProviderAction/actions'

import PaymentSummary from './PaymentSummary'

const mapActionCreators = {
  fireExternalEvent: externalEvents.fireExternalEvent,
  initPaymentSummary,
  redirectToPayment
}

const mapStateToProps = state => ({
  overviewFormName: state.paymentProviderAction.overviewEntity.formName,
  overviewEntityKey: state.paymentProviderAction.overviewEntity.key,
  overviewEntityName: state.paymentProviderAction.overviewEntity.modelName,
  order: state.paymentProviderAction.order,
  paymentMethods: state.paymentProviderAction.paymentMethods,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(PaymentSummary))
