import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  page: null,
  paymentLink: null,
  paymentMethods: [],
  orderUuid: null,
  order: undefined,
  overviewEntity: {}
}

const ACTION_HANDLERS = {
  [actions.SET_PAGE]: reducerUtil.singleTransferReducer('page'),
  [actions.SET_PAYMENT_LINK]: reducerUtil.singleTransferReducer('paymentLink'),
  [actions.SET_PAYMENT_METHODS]: reducerUtil.singleTransferReducer('paymentMethods'),
  [actions.SET_ORDER_UUID]: reducerUtil.singleTransferReducer('orderUuid'),
  [actions.SET_ORDER]: reducerUtil.singleTransferReducer('order'),
  [actions.SET_OVERVIEW_ENTITY]: reducerUtil.singleTransferReducer('overviewEntity')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
