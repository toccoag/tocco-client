import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, appFactory} from 'tocco-app-extensions'

import {Pages} from '../../utils/constants'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('payment-provider-action', () => {
  describe('modules', () => {
    describe('paymentProviderAction', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(appFactory.INPUT_CHANGED, sagas.inputChanged),
                takeLatest(actions.INIT, sagas.init),
                takeLatest(actions.INIT_PAYMENT_SUMMARY, sagas.initPaymentSummary),
                takeLatest(actions.REDIRECT_TO_PAYMENT, sagas.redirectToPayment),
                takeLatest(actions.HANDLE_ORDER_KEY, sagas.loadOrderUuid)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('init', () => {
          test('should show payment summary page when status is not set', () => {
            return expectSaga(sagas.init, {payload: {}}).put(actions.setPage(Pages.PaymentSummary)).run()
          })
          test('should show payment status page when status is set', () => {
            return expectSaga(sagas.init, {payload: {paymentStatus: 'queryParamStatus'}})
              .put(actions.setPage(Pages.PaymentResult))
              .run()
          })

          test('should not show payment status page when status is cancelled', () => {
            return expectSaga(sagas.init, {payload: {paymentStatus: 'cancelled'}})
              .put(actions.setPage(Pages.PaymentSummary))
              .run()
          })
        })

        describe('inputChanged', () => {
          test('should handle orderKey from selection', () => {
            const orderKey = '123'
            return expectSaga(sagas.inputChanged, {
              payload: {input: {selection: {ids: [orderKey], entityName: 'Order', type: 'ID'}}}
            })
              .put(actions.handleOrderKey('123'))
              .run()
          })

          test('should set orderUuid', () => {
            const orderUuid = '123'
            return expectSaga(sagas.inputChanged, {
              payload: {input: {orderUuid}}
            })
              .put(actions.setOrderUuid('123'))
              .run()
          })
        })

        describe('initPaymentSummary', () => {
          test('should load and set data', () => {
            const orderUuid = '123'
            const order = {key: '321'}
            const overviewEntity = {model: 'Event', formName: 'formname', key: '234'}
            const paymentMethods = [{id: 'visa'}]
            return expectSaga(sagas.initPaymentSummary)
              .provide([
                [select(sagas.paymentProviderActionSelector), {orderUuid}],
                [
                  matchers.call(rest.requestSaga, `/actions/paymentProvider/${orderUuid}/data`),
                  {
                    body: {order, overviewEntity, paymentMethods}
                  }
                ]
              ])
              .put(actions.setOrder(order))
              .put(actions.setPaymentMethods(paymentMethods))
              .put(actions.setOverviewEntity(overviewEntity))
              .run()
          })

          test('should not use null overview entity', () => {
            const orderUuid = '123'
            const order = {}
            const overviewEntity = null
            const paymentMethods = []
            return expectSaga(sagas.initPaymentSummary)
              .provide([
                [select(sagas.paymentProviderActionSelector), {orderUuid}],
                [
                  matchers.call(rest.requestSaga, `/actions/paymentProvider/${orderUuid}/data`),
                  {
                    body: {order, overviewEntity, paymentMethods}
                  }
                ]
              ])
              .not.put(actions.setOverviewEntity(null))
              .run()
          })
        })

        describe('redirectToPayment', () => {
          const orderUuid = '123'
          const paymentMethods = [
            {key: 321, label: 'invoice', usePaymentProvider: false},
            {key: 654, label: 'VISA', usePaymentProvider: true}
          ]
          test('should set result page if no payment provider is used', () => {
            return expectSaga(sagas.redirectToPayment, {payload: {selectedPaymentMethodKey: '321'}})
              .provide([[select(sagas.paymentProviderActionSelector), {orderUuid, paymentMethods}]])
              .put(actions.setPage(Pages.PaymentResult))
              .not.put(actions.setPage(Pages.PaymentProviderLoad))
              .run()
          })

          test('should redirect to payment link', async () => {
            // reset window.location to simple object for manipulation
            delete window.location
            window.location = {
              href: 'http://localhost'
            }

            const payLink = 'http://payrexx.ch'
            return expectSaga(sagas.redirectToPayment, {payload: {selectedPaymentMethodKey: '654'}})
              .provide([
                [select(sagas.paymentProviderActionSelector), {orderUuid, paymentMethods}],
                [
                  matchers.call(rest.requestSaga, '/actions/paymentProvider/123/payLink', {
                    method: 'POST',
                    body: {
                      paymentMethod: 654,
                      successRedirectUrl: 'http://localhost/?orderUuid=123&paymentStatus=success',
                      failedRedirectUrl: 'http://localhost/?orderUuid=123&paymentStatus=failed',
                      cancelledRedirectUrl: 'http://localhost/?orderUuid=123&paymentStatus=cancelled'
                    }
                  }),
                  {body: {payLink}}
                ]
              ])
              .put(actions.setPage(Pages.PaymentProviderLoad))
              .put(actions.setPaymentLink(payLink))
              .run()
              .then(() => {
                expect(window.location.href).to.eql(payLink)
              })
          })
        })

        describe('loadOrderUuid', () => {
          test('should set orderUuid', () => {
            const orderKey = 123
            const uuid = '321'
            const response = {
              status: 200,
              body: {
                uuid
              }
            }
            return expectSaga(sagas.loadOrderUuid, {payload: {orderKey}})
              .provide([[matchers.call(rest.requestSaga, `/actions/paymentProvider/${orderKey}/uuid`), response]])
              .put(actions.setOrderUuid(uuid))
              .run()
          })
        })
      })
    })
  })
})
