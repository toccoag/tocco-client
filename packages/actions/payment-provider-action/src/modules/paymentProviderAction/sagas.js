import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {appFactory, rest} from 'tocco-app-extensions'

import {getDispatchActions} from '../../input'
import {Pages} from '../../utils/constants'
import {buildRedirectUrl} from '../../utils/paymentMethods'

import * as actions from './actions'

export const paymentProviderActionSelector = state => state.paymentProviderAction

export default function* sagas() {
  yield all([
    takeLatest(appFactory.INPUT_CHANGED, inputChanged),
    takeLatest(actions.INIT, init),
    takeLatest(actions.INIT_PAYMENT_SUMMARY, initPaymentSummary),
    takeLatest(actions.REDIRECT_TO_PAYMENT, redirectToPayment),
    takeLatest(actions.HANDLE_ORDER_KEY, loadOrderUuid)
  ])
}

export function* inputChanged({payload}) {
  const {input} = payload

  const derivedActions = getDispatchActions(input)
  yield all(derivedActions.map(action => put(action)))
}

export function* init({payload: {paymentStatus}}) {
  if (paymentStatus && paymentStatus !== 'cancelled') {
    yield put(actions.setPage(Pages.PaymentResult))
  } else {
    yield put(actions.setPage(Pages.PaymentSummary))
  }
}

export function* initPaymentSummary() {
  const {orderUuid} = yield select(paymentProviderActionSelector)
  const {
    body: {order, overviewEntity, paymentMethods}
  } = yield call(rest.requestSaga, `/actions/paymentProvider/${orderUuid}/data`)
  yield all([
    put(actions.setOrder(order)),
    put(actions.setPaymentMethods(paymentMethods)),
    call(handleOverviewEntity, overviewEntity)
  ])
}

export function* redirectToPayment({payload: {selectedPaymentMethodKey}}) {
  const {orderUuid, paymentMethods} = yield select(paymentProviderActionSelector)
  const selectedPaymentMethod = paymentMethods.find(pm => `${pm.key}` === selectedPaymentMethodKey)
  if (!selectedPaymentMethod.usePaymentProvider) {
    yield put(actions.setPage(Pages.PaymentResult))
    return
  }
  yield put(actions.setPage(Pages.PaymentProviderLoad))

  const body = {
    paymentMethod: selectedPaymentMethod.key,
    successRedirectUrl: buildRedirectUrl('success', orderUuid),
    failedRedirectUrl: buildRedirectUrl('failed', orderUuid),
    cancelledRedirectUrl: buildRedirectUrl('cancelled', orderUuid)
  }
  const {
    body: {payLink}
  } = yield call(rest.requestSaga, `/actions/paymentProvider/${orderUuid}/payLink`, {
    method: 'POST',
    body
  })
  yield put(actions.setPaymentLink(payLink))
  window.location.href = payLink
}

export function* handleOverviewEntity(overviewEntity) {
  if (overviewEntity) {
    yield put(actions.setOverviewEntity(overviewEntity))
  }
}

export function* loadOrderUuid({payload: {orderKey}}) {
  const {
    body: {uuid}
  } = yield call(rest.requestSaga, `/actions/paymentProvider/${orderKey}/uuid`)
  yield put(actions.setOrderUuid(uuid))
}
