export const INIT = 'paymentProviderAction/INIT'
export const HANDLE_ORDER_KEY = 'paymentProviderAction/HANDLE_ORDER_KEY'
export const SET_ORDER_UUID = 'paymentProviderAction/SET_ORDER_UUID'
export const SET_PAGE = 'paymentProviderAction/SET_PAGE'
export const SET_PAYMENT_LINK = 'paymentProviderAction/SET_PAYMENT_LINK'
export const SET_PAYMENT_STATUS = 'paymentProviderAction/SET_PAYMENT_STATUS'
export const SET_PAYMENT_METHODS = 'paymentProviderAction/SET_PAYMENT_METHODS'
export const INIT_PAYMENT_SUMMARY = 'paymentProviderAction/INIT_PAYMENT_SUMMARY'
export const SET_ORDER = 'paymentProviderAction/SET_ORDER'
export const REDIRECT_TO_PAYMENT = 'paymentProviderAction/REDIRECT_TO_PAYMENT'
export const SET_OVERVIEW_ENTITY = 'paymentProviderAction/SET_OVERVIEW_ENTITY'

export const init = paymentStatus => ({
  type: INIT,
  payload: {
    paymentStatus
  }
})

export const handleOrderKey = orderKey => ({
  type: HANDLE_ORDER_KEY,
  payload: {
    orderKey
  }
})

export const setOrderUuid = orderUuid => ({
  type: SET_ORDER_UUID,
  payload: {
    orderUuid
  }
})

export const setPage = page => ({
  type: SET_PAGE,
  payload: {
    page
  }
})

export const setPaymentLink = paymentLink => ({
  type: SET_PAYMENT_LINK,
  payload: {
    paymentLink
  }
})

export const setPaymentMethods = paymentMethods => ({
  type: SET_PAYMENT_METHODS,
  payload: {
    paymentMethods
  }
})

export const initPaymentSummary = () => ({
  type: INIT_PAYMENT_SUMMARY
})

export const setOrder = order => ({
  type: SET_ORDER,
  payload: {
    order
  }
})

export const redirectToPayment = selectedPaymentMethodKey => ({
  type: REDIRECT_TO_PAYMENT,
  payload: {
    selectedPaymentMethodKey
  }
})

export const setOverviewEntity = overviewEntity => ({
  type: SET_OVERVIEW_ENTITY,
  payload: {overviewEntity}
})
