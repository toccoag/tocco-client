import {reducer as form} from 'redux-form'

import paymentProviderActionReducer from './paymentProviderAction/reducer'
import paymentProviderActionSagas from './paymentProviderAction/sagas'

export default {
  paymentProviderAction: paymentProviderActionReducer,
  form
}

export const sagas = [paymentProviderActionSagas]
