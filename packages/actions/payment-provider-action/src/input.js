import {selection as selectionUtil} from 'tocco-app-extensions'
import {action} from 'tocco-util'

import {handleOrderKey, setOrderUuid} from './modules/paymentProviderAction/actions'

export const getDispatchActions = input => action.getDispatchActions(input, actionSettings)

const actionSettings = [
  {
    name: 'selection',
    action: handleOrderKey,
    argsFactory: input => [selectionUtil.getSingleKey(input.selection, 'Order')]
  },
  {
    name: 'orderUuid',
    action: setOrderUuid,
    argsFactory: input => [input.orderUuid]
  }
]
