import PropTypes from 'prop-types'
import {appFactory, selection, externalEvents, actionEmitter, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import PaymentProvider from './components/PaymentProvider'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'payment-provider-action'
const EXTERNAL_EVENTS = [
  /**
   * Is fired when payment method without payment provider is selected
   */
  'onSuccess',
  'onError',
  /**
   * Is fired when the summary page is rendered
   */
  'onPaymentSummaryPage',
  /**
   * Is fired when the the result page with status success is rendered
   */
  'onPaymentSuccessPage',
  /**
   * Is fired when the the result page with status failed is rendered
   */
  'onPaymentFailedPage',
  'emitAction'
]

const initApp = (id, input, events, publicPath) => {
  const content = <PaymentProvider />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const PaymentProviderActionApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

PaymentProviderActionApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  /**
   * Selection of a single `Event` (only type `ID` is supported and exacte one key is required)
   * either `selection` or `orderUuid` is mandatory
   */
  selection: selection.propType.isRequired,
  /**
   * `online_payment_uuid` of `Order`
   * either `selection` or `orderUuid` is mandatory
   */
  orderUuid: PropTypes.string
}

export default PaymentProviderActionApp
