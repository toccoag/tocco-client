import {buildRedirectUrl} from './paymentMethods'

describe('payment-provider-action', () => {
  describe('utils', () => {
    describe('paymentMethods', () => {
      describe('buildRedirectUrl', () => {
        beforeEach(() => {
          // reset window.location to simple object for manipulation
          delete window.location
        })

        test('should set orderUuid and paymentStatus', () => {
          window.location = {
            href: 'http://localhost'
          }

          expect(buildRedirectUrl('success', 123)).to.be.eql('http://localhost/?orderUuid=123&paymentStatus=success')
        })

        test('should remove hash', () => {
          window.location = {
            href: 'http://localhost#test=removethis'
          }

          expect(buildRedirectUrl('success', 123)).to.be.eql('http://localhost/?orderUuid=123&paymentStatus=success')
        })

        test('should replace existing orderUuid and paymentStatus', () => {
          window.location = {
            href: 'http://localhost?orderUuid=321&paymentStatus=failed'
          }

          expect(buildRedirectUrl('success', 123)).to.be.eql('http://localhost/?orderUuid=123&paymentStatus=success')
        })
      })
    })
  })
})
