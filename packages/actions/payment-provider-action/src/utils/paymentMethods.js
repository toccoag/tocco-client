export const buildRedirectUrl = (result, orderUuid) => {
  const url = new URL(window.location.href)

  url.hash = '' // clear complicated hash history /#action/payment-provider-action and use orderUuid instead

  // might have redirected already if user cancelled previously, replace leftover params instead of appending
  url.searchParams.set('orderUuid', orderUuid)
  url.searchParams.set('paymentStatus', result)

  return url.toString()
}
