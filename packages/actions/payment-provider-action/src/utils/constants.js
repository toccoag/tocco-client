export const Pages = {
  PaymentProviderLoad: 'PaymentProviderLoad',
  PaymentSummary: 'PaymentSummary',
  PaymentResult: 'PaymentResult'
}
