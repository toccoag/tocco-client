import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useSearchParams} from 'react-router-dom'

// used in tests
const PaymentStatusHelper = ({initialStatus}) => {
  const [searchParams, setSearchParams] = useSearchParams()
  useEffect(() => {
    if (initialStatus) {
      setSearchParams({paymentStatus: initialStatus})
    }
  }, [initialStatus, setSearchParams])
  const paymentStatus = searchParams.get('paymentStatus')
  return <span>payment status: {paymentStatus}</span>
}

PaymentStatusHelper.propTypes = {
  initialStatus: PropTypes.string
}

export default PaymentStatusHelper
