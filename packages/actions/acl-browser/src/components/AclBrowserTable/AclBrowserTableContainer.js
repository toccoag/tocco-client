import {connect} from 'react-redux'

import {loadNewPage} from '../../modules/aclBrowserPagination'
import {setSortingObject} from '../../modules/aclBrowserSearch'
import {loadAclData} from '../../modules/aclBrowserTable'

import AclBrowserTable from './AclBrowserTable'

const mapActionCreators = {
  loadAclData,
  loadNewPage,
  setSortingObject
}

const mapStateToProps = state => ({
  form: state.aclBrowserTable.form,
  tableData: state.aclBrowserTable.tableData,
  totalCount: state.aclBrowserPagination.totalCount,
  currentPage: state.aclBrowserPagination.currentPage,
  recordsPerPage: state.aclBrowserPagination.recordsPerPage,
  sortAscending: state.aclBrowserSearch.sortAscending,
  sortingObject: state.aclBrowserSearch.sortingObject
})

export default connect(mapStateToProps, mapActionCreators)(AclBrowserTable)
