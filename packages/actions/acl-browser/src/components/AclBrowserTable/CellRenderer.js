import PropTypes from 'prop-types'
import {field} from 'tocco-app-extensions'

import {StyledCell} from './StyledComponents'

const CellRenderer = ({column, rowData: data}) => {
  if (!column?.children) {
    return null
  }
  return (
    <>
      {column.children.map(fieldDefinition => {
        const {path, dataType, id} = fieldDefinition
        const Field = field.factory('list', dataType)
        const value = data[path]

        return (
          <StyledCell key={id} cellColor={value} strikethrough={!data.isAppliedToUser}>
            <Field type={dataType} mappingType="list" formField={fieldDefinition} value={value} breakWords={false} />
          </StyledCell>
        )
      })}
    </>
  )
}

export {CellRenderer}

CellRenderer.propTypes = {
  column: PropTypes.object,
  rowData: PropTypes.object
}
