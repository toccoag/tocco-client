import styled from 'styled-components'
import {scale, StyledReactSelectOuterWrapper, themeSelector} from 'tocco-ui'

export const StyledTableWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;

  && th {
    border-bottom: 3px solid ${themeSelector.color('backgroundBody')};
    padding: ${scale.space(-1.1)} 0 ${scale.space(-1.1)} 0;
  }
`
export const StyledColumnContentWrapper = styled.div`
  display: flex;
  align-items: center;

  > * {
    margin-right: ${scale.space(-2)};
  }
`

export const StyledCell = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  text-decoration: ${({strikethrough}) => (strikethrough ? 'line-through' : 'none')};
  background-color: ${({cellColor, theme}) =>
    (cellColor === 'grant' && theme.colors.signal.successLight) ||
    (cellColor === 'deny' && theme.colors.signal.dangerLight)};

  ${StyledReactSelectOuterWrapper} {
    margin-right: 10px;
  }
`
