import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {Table} from 'tocco-ui'
import {table} from 'tocco-util'

import {getColumnDefinition, getTable} from '../../util/form'

import {CellRenderer} from './CellRenderer'
import {StyledTableWrapper} from './StyledComponents'

const AclBrowserTable = ({
  tableData,
  loadAclData,
  form: listFormDefinition,
  totalCount,
  currentPage,
  recordsPerPage,
  loadNewPage,
  setSortingObject,
  sortAscending,
  sortingObject
}) => {
  const [columns, setColumns, onColumnPositionChange] = table.useColumnPosition()

  useEffect(() => {
    loadAclData()
  }, [loadAclData])

  useEffect(() => {
    if (listFormDefinition.children) {
      const tableDefinition = getTable(listFormDefinition)
      const columnsDefinitions = getColumnDefinition(
        {
          table: tableDefinition
        },
        CellRenderer,
        sortAscending ? 'asc' : 'desc',
        sortingObject
      )

      setColumns([...columnsDefinitions])
    }
  }, [listFormDefinition, setColumns, sortAscending, sortingObject])

  if (!listFormDefinition.children) {
    return null
  }

  const data = tableData.map((row, index) => ({
    ...row,
    __key: `${index}-${row.selector}-${row.entity}`
  }))
  return (
    <StyledTableWrapper>
      <Table
        columns={columns}
        data={data}
        onSortingChange={setSortingObject}
        onColumnPositionChange={onColumnPositionChange}
        scrollBehaviour="inline"
        paginationInfo={{
          currentPage,
          recordsPerPage,
          totalCount
        }}
        onPageChange={loadNewPage}
      />
    </StyledTableWrapper>
  )
}

AclBrowserTable.propTypes = {
  form: PropTypes.object.isRequired,
  loadAclData: PropTypes.func.isRequired,
  tableData: PropTypes.array,
  totalCount: PropTypes.number,
  currentPage: PropTypes.number.isRequired,
  recordsPerPage: PropTypes.number.isRequired,
  loadNewPage: PropTypes.func.isRequired,
  setSortingObject: PropTypes.func.isRequired,
  sortAscending: PropTypes.bool,
  sortingObject: PropTypes.string
}

export default AclBrowserTable
