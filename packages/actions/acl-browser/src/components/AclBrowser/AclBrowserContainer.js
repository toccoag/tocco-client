import {connect} from 'react-redux'

import {initializeSearch} from '../../modules/aclBrowserSearch/actions'
import {initializeAclBrowserTable} from '../../modules/aclBrowserTable/actions'

import AclBrowser from './AclBrowser'

const mapActionCreators = {
  initializeSearch,
  initializeAclBrowserTable
}

export default connect(null, mapActionCreators)(AclBrowser)
