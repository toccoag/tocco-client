import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {SidepanelMainContent, Sidepanel, SidepanelContainer, SidepanelHeader} from 'tocco-ui'

import AclBrowserSearch from '../AclBrowserSearch'
import AclBrowserTable from '../AclBrowserTable'

import {StyledPaneWrapper} from './StyledComponents'

const AclBrowser = ({initializeSearch, initializeAclBrowserTable}) => {
  useEffect(() => {
    initializeSearch()
    initializeAclBrowserTable()
  }, [initializeSearch, initializeAclBrowserTable])

  const [isCollapsed, setIsCollapsed] = useState(false)

  return (
    <>
      <StyledPaneWrapper>
        <SidepanelContainer
          sidepanelPosition="left"
          sidepanelCollapsed={isCollapsed}
          setSidepanelCollapsed={setIsCollapsed}
          scrollBehaviour="inline"
        >
          <Sidepanel>
            <SidepanelHeader />
            <AclBrowserSearch />
          </Sidepanel>

          <SidepanelMainContent>
            <AclBrowserTable />
          </SidepanelMainContent>
        </SidepanelContainer>
      </StyledPaneWrapper>
    </>
  )
}

AclBrowser.propTypes = {
  initializeSearch: PropTypes.func.isRequired,
  initializeAclBrowserTable: PropTypes.func.isRequired
}

export default AclBrowser
