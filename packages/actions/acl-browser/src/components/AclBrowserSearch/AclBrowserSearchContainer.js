import {connect} from 'react-redux'

import {setCurrentPage} from '../../modules/aclBrowserPagination'
import {setSearchFields} from '../../modules/aclBrowserSearch'

import AclBrowserSearch from './AclBrowserSearch'

const mapActionCreators = {
  setSearchFieldValue: setSearchFields,
  setCurrentPage
}

const mapStateToProps = state => ({
  form: state.aclBrowserSearch.form
})

export default connect(mapStateToProps, mapActionCreators)(AclBrowserSearch)
