import _debounce from 'lodash/debounce'
import PropTypes from 'prop-types'
import {useCallback} from 'react'
import SimpleFormApp from 'tocco-simple-form/src/main'

import {Box} from './StyledComponents'

const AclBrowserSearch = ({form: searchFormDefinition, setSearchFieldValue, setCurrentPage}) => {
  const handleChange = useCallback(
    ({values, event}) => {
      const hasValues = Object.keys(values).length > 0
      /**
       * ignore onChange event on init
       *  - data gets loaded already in AclBrowserTable component (w/d debounced)
       *  - only handle event on init when default values are applied
       */
      if (event !== 'init' || hasValues) {
        setCurrentPage(1)
        setSearchFieldValue(hasValues ? values : {})
      }
    },
    [setSearchFieldValue, setCurrentPage]
  )

  if (!searchFormDefinition.children) {
    return null
  }

  return (
    <Box>
      <SimpleFormApp
        form={searchFormDefinition}
        noButtons
        validate={false}
        mappingType="search"
        mode="search"
        onChange={_debounce(handleChange, 500)}
        onSubmit={handleChange}
        labelPosition="inside"
      />
    </Box>
  )
}

AclBrowserSearch.propTypes = {
  form: PropTypes.object.isRequired,
  setSearchFieldValue: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired
}

export default AclBrowserSearch
