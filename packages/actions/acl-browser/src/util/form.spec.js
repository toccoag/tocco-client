import {getColumnDefinition} from './form'

describe('acl-browser', () => {
  describe('acl-browser-table', () => {
    describe('util', () => {
      describe('form', () => {
        describe('getColumnDefinition', () => {
          it('should build column definitions', () => {
            const table = {
              children: [
                {
                  id: 'first',
                  label: 'first label',
                  shrinkToContent: true,
                  sticky: true,
                  widthFixed: true,
                  children: [{id: 'child', hidden: false}]
                },
                {
                  id: 'sorting',
                  label: 'sorting label',
                  shrinkToContent: false,
                  widthFixed: false,
                  children: [{hidden: true}]
                },
                {id: 'no children', label: 'no children label', shrinkToContent: false, widthFixed: false, children: []}
              ]
            }
            const cellRenderer = {}
            const expectedColumnDefinitions = [
              {
                id: 'first',
                label: 'first label',
                dynamic: true,
                sorting: {sortable: true},
                shrinkToContent: true,
                sticky: true,
                children: [{id: 'child', hidden: false}],
                resizable: false,
                alignment: 'left',
                CellRenderer: cellRenderer
              },
              {
                id: 'sorting',
                label: 'sorting label',
                dynamic: true,
                sorting: {sortable: true, sortRank: 1, order: true},
                shrinkToContent: false,
                sticky: false,
                children: [],
                resizable: true,
                alignment: 'left',
                CellRenderer: cellRenderer
              },
              {
                id: 'no children',
                label: 'no children label',
                dynamic: true,
                sorting: {sortable: true},
                shrinkToContent: false,
                sticky: false,
                children: [],
                resizable: true,
                alignment: 'left',
                CellRenderer: cellRenderer
              }
            ]
            const columnDefinitions = getColumnDefinition({table}, cellRenderer, true, 'sorting')
            expect(columnDefinitions).to.eql(expectedColumnDefinitions)
          })
        })
      })
    })
  })
})
