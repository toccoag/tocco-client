import {form} from 'tocco-app-extensions'

export const getTable = formDefinition =>
  formDefinition.children.find(child => child.componentType === form.componentTypes.TABLE)

export const getColumnDefinition = ({table}, CellRenderer, sortAscending, sortingObject) =>
  table.children.map(column => ({
    id: column.id,
    label: column.label,
    dynamic: true,
    sorting: {
      sortable: true,
      ...getSortingAttributes(column, sortAscending, sortingObject)
    },
    shrinkToContent: column.shrinkToContent || false,
    sticky: column.sticky || false,
    children: column.children.filter(isDisplayableChild),
    resizable: !column.widthFixed,
    alignment: 'left',
    CellRenderer
  }))

const isDisplayableChild = child => !child.hidden

const getSortingAttributes = (column, sortAscending, sortingObject) => {
  return sortingObject === column.id
    ? {
        sortRank: 1,
        order: sortAscending
      }
    : null
}
