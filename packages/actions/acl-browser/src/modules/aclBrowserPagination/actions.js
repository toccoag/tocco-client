export const SET_CURRENT_PAGE = 'aclBrowserPagination/SET_CURRENT_PAGE'
export const LOAD_NEW_PAGE = 'aclBrowserPagination/LOAD_NEW_PAGE'
export const SET_TOTAL_COUNT = 'aclBrowserPagination/SET_TOTAL_COUNT'

export const setCurrentPage = currentPage => ({
  type: SET_CURRENT_PAGE,
  payload: {currentPage}
})

export const loadNewPage = newPage => ({
  type: LOAD_NEW_PAGE,
  payload: {newPage}
})

export const setTotalCount = totalCount => ({
  type: SET_TOTAL_COUNT,
  payload: {
    totalCount
  }
})
