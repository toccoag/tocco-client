import {all, takeLatest} from 'redux-saga/effects'

import {loadData} from '../aclBrowserTable/sagas'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_NEW_PAGE, loadData)])
}
