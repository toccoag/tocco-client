import {setCurrentPage, loadNewPage} from './actions'
import reducer from './reducer'
import sagas from './sagas'

export {setCurrentPage, loadNewPage}
export {sagas}
export default reducer
