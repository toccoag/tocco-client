import {setSearchFields, setSortingObject} from './actions'
import reducer from './reducer'
import sagas from './sagas'

export {setSearchFields, setSortingObject}
export {sagas}
export default reducer
