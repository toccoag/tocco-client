import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  form: {},
  searchFields: {},
  sortingObject: '',
  sortAscending: true
}

const isSameSortingObject = (state, payload) => state.sortingObject === payload.sortingObject

const ACTION_HANDLERS = {
  [actions.SET_FORM]: reducerUtil.singleTransferReducer('form'),
  [actions.SET_SEARCH_FIELDS]: reducerUtil.singleTransferReducer('searchFields'),
  [actions.SET_SORTING_OBJECT]: (state, {payload}) => ({
    ...state,
    ...(isSameSortingObject(state, payload)
      ? {sortAscending: !state.sortAscending}
      : {sortingObject: payload.sortingObject, sortAscending: true})
  })
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
