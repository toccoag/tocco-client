import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import * as sagas from './sagas'

describe('acl-browser', () => {
  describe('acl-browser-search', () => {
    describe('sagas', () => {
      describe('initialize', () => {
        test('should load forms', () => {
          const expectedForm = {}
          return expectSaga(sagas.initialize)
            .provide([[matchers.call.fn(rest.fetchForm), expectedForm]])
            .call(rest.fetchForm, 'Acl_browser_action', 'search')
            .put(actions.setForm(expectedForm))
            .run()
        })
      })
    })
  })
})
