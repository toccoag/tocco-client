import {all, takeLatest, call, put} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import {loadData} from '../aclBrowserTable/sagas'

import * as actions from './actions'

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE_SEARCH, initialize),
    takeLatest(actions.SET_SEARCH_FIELDS, loadData),
    takeLatest(actions.SET_SORTING_OBJECT, loadData)
  ])
}

export function* initialize() {
  const form = yield call(rest.fetchForm, 'Acl_browser_action', 'search')
  yield put(actions.setForm(form))
}
