import * as actions from './actions'
import reducer from './reducer'

describe('acl-browser', () => {
  describe('acl-browser-search', () => {
    describe('reducer', () => {
      describe('setSortingObject', () => {
        const type = actions.SET_SORTING_OBJECT
        test('should set sorting property and direction', () => {
          const payload = {sortingObject: 'property'}
          const expectedState = {
            sortingObject: 'property',
            sortAscending: true
          }
          expect(reducer({}, {type, payload})).to.eql(expectedState)
        })
        test('should switch sorting direction when same property', () => {
          const payload = {sortingObject: 'property'}
          const initialState = {
            sortingObject: 'property',
            sortAscending: true
          }
          const expectedState = {
            sortingObject: 'property',
            sortAscending: false
          }
          expect(reducer(initialState, {type, payload})).to.eql(expectedState)
          expect(reducer(expectedState, {type, payload})).to.eql(initialState)
        })
        test('should reset sorting direction when new property', () => {
          const payload = {sortingObject: 'new'}
          const initialState = {
            sortingObject: 'old',
            sortAscending: false
          }
          const expectedState = {
            sortingObject: 'new',
            sortAscending: true
          }
          expect(reducer(initialState, {type, payload})).to.eql(expectedState)
        })
      })
    })
  })
})
