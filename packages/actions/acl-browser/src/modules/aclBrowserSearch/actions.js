export const INITIALIZE_SEARCH = 'aclBrowserSearch/INITIALIZE_SEARCH'
export const SET_FORM = 'aclBrowserSearch/SET_FORM'
export const SET_SEARCH_FIELDS = 'aclBrowserSearch/SET_SEARCH_FIELDS'
export const SET_SORTING_OBJECT = 'aclBrowserSearch/SET_SORTING_OBJECT'

export const initializeSearch = () => ({
  type: INITIALIZE_SEARCH
})

export const setForm = form => ({
  type: SET_FORM,
  payload: {form}
})

export const setSearchFields = searchFields => ({
  type: SET_SEARCH_FIELDS,
  payload: {searchFields}
})

export const setSortingObject = sortingObject => ({
  type: SET_SORTING_OBJECT,
  payload: {
    sortingObject
  }
})
