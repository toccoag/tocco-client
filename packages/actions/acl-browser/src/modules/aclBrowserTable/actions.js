export const INITIALIZE_ACLBROWSERTABLE = 'aclBrowserTable/INITIALIZE_ACLBROWSERTABLE'
export const SET_ACLBROWSERTABLE = 'aclBrowserTable/SET_ACLBROWSERTABLE'
export const SET_ACLBROWSERTABLEDATA = 'aclBrowserTable/SET_ACLBROWSERTABLEDATA'
export const LOAD_ACLBROWSERDATA = 'aclBrowserTable/LOAD_ACLBROWSERDATA'

export const initializeAclBrowserTable = () => ({
  type: INITIALIZE_ACLBROWSERTABLE
})

export const setAclBrowserTable = form => ({
  type: SET_ACLBROWSERTABLE,
  payload: {form}
})

export const setAclBrowserTableData = tableData => ({
  type: SET_ACLBROWSERTABLEDATA,
  payload: {tableData}
})

export const loadAclData = () => ({
  type: LOAD_ACLBROWSERDATA
})
