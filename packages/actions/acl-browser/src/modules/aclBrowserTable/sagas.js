import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'

import {setTotalCount} from '../aclBrowserPagination/actions'

import * as actions from './actions'

export const aclBrowserPaginationSelector = state => state.aclBrowserPagination
export const aclBrowserSearchSelector = state => state.aclBrowserSearch

export default function* sagas() {
  yield all([
    takeEvery(actions.INITIALIZE_ACLBROWSERTABLE, initializeAclBrowserTable),
    takeEvery(actions.LOAD_ACLBROWSERDATA, loadData)
  ])
}

export function* initializeAclBrowserTable() {
  const form = yield call(rest.fetchForm, 'Acl_browser_action', 'list')
  yield put(actions.setAclBrowserTable(form))
}

export function* loadData() {
  const {recordsPerPage, currentPage} = yield select(aclBrowserPaginationSelector)
  const {searchFields, sortingObject, sortAscending} = yield select(aclBrowserSearchSelector)

  const fromIndexNumber = (currentPage - 1) * recordsPerPage
  const toIndexNumber = fromIndexNumber + recordsPerPage

  const body = {
    fromIndex: fromIndexNumber,
    toIndex: toIndexNumber,
    sortingProperty: sortingObject,
    ascending: sortAscending,
    filters: searchFields
  }

  try {
    const response = yield call(rest.requestSaga, '/acl/rules', {method: 'POST', body})
    const tableData = response.body.rules
    const totalRecords = response.body.totalCount

    yield put(actions.setAclBrowserTableData(tableData))
    yield put(setTotalCount(totalRecords))
  } catch (e) {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.log.error.loading-aclBrowserData.header',
        body: 'client.actions.log.error-aclBrowserData.body'
      })
    )
    yield put(actions.setAclBrowserTableData(null))
    yield put(setTotalCount(0))
  }
}
