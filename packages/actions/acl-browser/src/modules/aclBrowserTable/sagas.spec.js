import {select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as aclBrowserPaginationAction from '../aclBrowserPagination/actions'

import * as actions from './actions'
import * as sagas from './sagas'

describe('acl-browser', () => {
  describe('acl-browser-table', () => {
    describe('sagas', () => {
      describe('initialize', () => {
        test('should load table-form', () => {
          const expectedForm = {}
          return expectSaga(sagas.initializeAclBrowserTable)
            .provide([[matchers.call.fn(rest.fetchForm), expectedForm]])
            .put(actions.setAclBrowserTable(expectedForm))
            .run()
        })
      })
      describe('fetch acl-browser data', () => {
        test('should load table-form', () => {
          const rules = [
            {
              selector: 'entityPath(system_entity)',
              entity: 'Working_day_time'
            }
          ]
          const response = {
            body: {
              rules,
              totalCount: 2
            }
          }

          const body = {
            fromIndex: 0,
            toIndex: 1,
            sortingProperty: undefined,
            ascending: false,
            filters: undefined
          }

          return expectSaga(sagas.loadData, {})
            .provide([
              [matchers.call.fn(rest.requestSaga), response],
              [
                select(sagas.aclBrowserPaginationSelector),
                {
                  totalCount: 2,
                  currentPage: 1,
                  recordsPerPage: 1
                }
              ],
              [
                select(sagas.aclBrowserSearchSelector),
                {
                  sortAscending: false
                }
              ]
            ])
            .call(rest.requestSaga, '/acl/rules', {method: 'POST', body})
            .put(actions.setAclBrowserTableData(rules))
            .put(aclBrowserPaginationAction.setTotalCount(2))
            .run()
        })
        test('should load table-data from current Page', () => {
          const rules = [
            {
              selector: 'entityPath(system_entity)',
              entity: 'Working_day_time'
            }
          ]
          const response = {
            body: {
              rules,
              totalCount: 2
            }
          }

          const body = {
            fromIndex: 1,
            toIndex: 2,
            sortingProperty: undefined,
            ascending: false,
            filters: undefined
          }

          return expectSaga(sagas.loadData)
            .provide([
              [matchers.call.fn(rest.requestSaga), response],
              [
                select(sagas.aclBrowserPaginationSelector),
                {
                  totalCount: 2,
                  currentPage: 2,
                  recordsPerPage: 1
                }
              ],
              [
                select(sagas.aclBrowserSearchSelector),
                {
                  sortAscending: false
                }
              ]
            ])
            .call(rest.requestSaga, '/acl/rules', {method: 'POST', body})
            .put(actions.setAclBrowserTableData(rules))
            .put(aclBrowserPaginationAction.setTotalCount(2))
            .run()
        })
        test('should load table-data from with given filter', () => {
          const rules = [
            {
              selector: 'entityPath(system_entity)',
              entity: 'Working_day_time'
            }
          ]
          const response = {
            body: {
              rules,
              totalCount: 2
            }
          }

          const searchData = [
            {
              entity: 'Working_day_time'
            }
          ]

          const body = {
            fromIndex: 0,
            toIndex: 1,
            sortingProperty: undefined,
            ascending: false,
            filters: searchData
          }

          return expectSaga(sagas.loadData)
            .provide([
              [matchers.call.fn(rest.requestSaga), response],
              [
                select(sagas.aclBrowserPaginationSelector),
                {
                  totalCount: 2,
                  currentPage: 1,
                  recordsPerPage: 1
                }
              ],
              [
                select(sagas.aclBrowserSearchSelector),
                {
                  sortAscending: false,
                  searchFields: searchData
                }
              ]
            ])
            .call(rest.requestSaga, '/acl/rules', {method: 'POST', body})
            .put(actions.setAclBrowserTableData(rules))
            .put(aclBrowserPaginationAction.setTotalCount(2))
            .run()
        })
        test('should load table-data from with sorting', () => {
          const rules = [
            {
              selector: 'entityPath(system_entity)',
              entity: 'Working_day_time'
            }
          ]
          const response = {
            body: {
              rules,
              totalCount: 2
            }
          }

          const sortingObject = 'entity'

          const body = {
            fromIndex: 0,
            toIndex: 1,
            sortingProperty: sortingObject,
            ascending: true,
            filters: undefined
          }

          return expectSaga(sagas.loadData)
            .provide([
              [matchers.call.fn(rest.requestSaga), response],
              [
                select(sagas.aclBrowserPaginationSelector),
                {
                  totalCount: 2,
                  currentPage: 1,
                  recordsPerPage: 1
                }
              ],
              [
                select(sagas.aclBrowserSearchSelector),
                {
                  sortAscending: true,
                  sortingObject
                }
              ]
            ])
            .call(rest.requestSaga, '/acl/rules', {method: 'POST', body})
            .put(actions.setAclBrowserTableData(rules))
            .put(aclBrowserPaginationAction.setTotalCount(2))
            .run()
        })
      })
    })
  })
})
