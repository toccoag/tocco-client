import aclBrowserPaginationReducer, {sagas as aclBrowserPaginationSagas} from './aclBrowserPagination'
import aclBrowserSearchReducer, {sagas as aclBrowserSearchSagas} from './aclBrowserSearch'
import aclBrowserTableReducer, {sagas as aclBrowserTableSagas} from './aclBrowserTable'

export default {
  aclBrowserSearch: aclBrowserSearchReducer,
  aclBrowserTable: aclBrowserTableReducer,
  aclBrowserPagination: aclBrowserPaginationReducer
}

export const sagas = [aclBrowserSearchSagas, aclBrowserTableSagas, aclBrowserPaginationSagas]
