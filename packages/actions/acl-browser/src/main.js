import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import AclBrowser from './components/AclBrowser'
import reducers, {sagas} from './modules/reducers'

const packageName = 'acl-browser'

const initApp = (id, input, events, publicPath) => {
  const content = <AclBrowser />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const AclBrowserApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

AclBrowserApp.propTypes = {}

export default AclBrowserApp
