import PropTypes from 'prop-types'
import {useEffect} from 'react'
import SsoLoginApp, {extendUrlForSuccessHandling} from 'tocco-sso-login/src/main'
import {LoadMask} from 'tocco-ui'

const getRedirectUrl = () => {
  const url = new URL(window.location.href)
  url.searchParams.append('action', 'connectPrincipal')
  return extendUrlForSuccessHandling(url.href)
}

const Progress = ({checkAccessRights, connectPrincipal, setShowSsoLoginApp, showSsoLoginApp}) => {
  useEffect(() => {
    checkAccessRights()
  }, [checkAccessRights])

  const loginCompleted = ({provider, sub: ssoSubject}) => {
    setShowSsoLoginApp(false)
    connectPrincipal(provider, ssoSubject)
  }
  return (
    <LoadMask required={[showSsoLoginApp]}>
      <SsoLoginApp ssoLoginEndpoint="/sso-lookup" redirectUrl={getRedirectUrl()} loginCompleted={loginCompleted} />
    </LoadMask>
  )
}

Progress.propTypes = {
  checkAccessRights: PropTypes.func.isRequired,
  connectPrincipal: PropTypes.func.isRequired,
  showSsoLoginApp: PropTypes.bool.isRequired,
  setShowSsoLoginApp: PropTypes.func.isRequired
}

export default Progress
