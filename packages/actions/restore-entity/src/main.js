import {appFactory, selection, externalEvents, actionEmitter, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import RestoreEntity from './components/RestoreEntity'
import reducers, {sagas} from './modules/reducers'

const packageName = 'restore-entity'
const EXTERNAL_EVENTS = ['emitAction', 'onSuccess']

const initApp = (id, input, events, publicPath) => {
  const content = <RestoreEntity />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  notification.addToStore(store, false)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const RestoreEntityApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

RestoreEntityApp.propTypes = {
  /**
   * Selection of a single `Report`, `Corporate_design`, `Output_template` or `Output_template_field`
   * (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired
}

export default RestoreEntityApp
