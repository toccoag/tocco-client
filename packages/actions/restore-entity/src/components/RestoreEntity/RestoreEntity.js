import _merge from 'lodash/merge'
import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {LoadingSpinner, Typography, StatedValue, EditableValue, Button, Layout, Panel} from 'tocco-ui'

import {StyledButtonWrapper} from './StyledComponents'

const RestoreEntity = ({data, fetchData, intl, postData, isLoading}) => {
  useEffect(() => {
    fetchData()
  }, [fetchData])

  const [fields, setFields] = useState({})
  const msg = id => intl.formatMessage({id})

  if (data === undefined || isLoading) {
    return <LoadingSpinner />
  }
  const content = Object.keys(data).map(id => (
    <StatedValue label={msg(`client.actions.restore-entity.${id}`)} key={id} labelPosition="inside">
      <EditableValue
        type="boolean"
        value={fields[id] === undefined ? data[id] : fields[id]}
        events={{onChange: isChecked => setFields(f => ({...f, [id]: isChecked}))}}
        id={`editable-value-${id}`}
      />
    </StatedValue>
  ))

  const restore = () => {
    const selectedKeys = Object.entries(_merge(data, fields))
      .filter(([key, value]) => value === true)
      .map(([key, value]) => key)
    postData(selectedKeys)
  }

  return (
    <>
      <Layout.Container>
        <Layout.Box>
          <Panel.Wrapper>
            <Panel.Header>
              <Typography.H4>
                <FormattedMessage id="client.actions.restore-entity.subtitle" />
              </Typography.H4>
            </Panel.Header>
            <Panel.Body>
              <Typography.P>
                <FormattedMessage id="client.actions.restore-entity.description" />
              </Typography.P>
              <>{content}</>
            </Panel.Body>
          </Panel.Wrapper>
        </Layout.Box>
      </Layout.Container>
      <StyledButtonWrapper>
        <Button ink="primary" look="raised" onClick={restore} disabled={isLoading}>
          <FormattedMessage id="client.actions.restore-entity.reload.chosen" />
        </Button>
      </StyledButtonWrapper>
    </>
  )
}

RestoreEntity.propTypes = {
  data: PropTypes.object,
  fetchData: PropTypes.func.isRequired,
  intl: PropTypes.any,
  postData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool
}

export default RestoreEntity
