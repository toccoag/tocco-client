import {screen, fireEvent} from '@testing-library/react'
import {expect} from 'chai'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import RestoreEntity from './RestoreEntity'

describe('restore-entity', () => {
  describe('components', () => {
    describe('RestoreEntity', () => {
      test('should check if the right content is printed with one set of data', async () => {
        testingLibrary.renderWithIntl(
          <RestoreEntity
            data={{
              Report: true,
              Output_template: false,
              Business_unit_corporate_design: false,
              Output_template_field: false
            }}
            fetchData={() => {}}
            intl={IntlStub}
            postData={() => {}}
            isLoading={false}
          />
        )
        await screen.findAllByTestId('icon')

        expect(screen.getAllByText('client.actions.restore-entity.Report')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.Output_template')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.Business_unit_corporate_design')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.Output_template_field')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.reload.chosen')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.subtitle')).to.have.length(1)
        expect(screen.getAllByText('client.actions.restore-entity.description')).to.have.length(1)
      })

      test('should fetch data on mount', async () => {
        const fetchDataSpy = sinon.spy()

        testingLibrary.renderWithIntl(
          <RestoreEntity fetchData={fetchDataSpy} intl={IntlStub} postData={() => {}} isLoading={false} />
        )
        await screen.findAllByTestId('icon')

        expect(fetchDataSpy).to.have.been.calledOnce
      })

      test('should reload selected data', async () => {
        const postDataSpy = sinon.spy()
        const fetchData = () => {}

        testingLibrary.renderWithIntl(
          <RestoreEntity
            data={{
              Report: true,
              Output_template: false
            }}
            postData={postDataSpy}
            fetchData={fetchData}
            intl={IntlStub}
            isLoading={false}
          />
        )
        await screen.findAllByTestId('icon')

        const checkboxes = screen.getAllByRole('checkbox')

        // select Output_template
        fireEvent.click(checkboxes[0])
        fireEvent.click(checkboxes[1])

        // reload
        fireEvent.click(screen.getByText('client.actions.restore-entity.reload.chosen'))
        expect(postDataSpy).to.have.been.calledWith(['Output_template'])
      })
    })
  })
})
