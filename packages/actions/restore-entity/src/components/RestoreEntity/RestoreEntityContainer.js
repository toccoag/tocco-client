import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {fetchData, postData} from '../../modules/restoreEntity'

import RestoreEntity from './RestoreEntity'

const mapActionCreators = {
  fetchData,
  postData
}

const mapStateToProps = state => ({
  data: state.restoreEntity.data,
  isLoading: state.restoreEntity.isLoading
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(RestoreEntity))
