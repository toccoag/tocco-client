import {all, takeEvery, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, externalEvents, notification} from 'tocco-app-extensions'

import * as actions from './action'
import rootSaga, * as sagas from './sagas'

describe('restore-entity', () => {
  describe('modules', () => {
    describe('restoreEntity', () => {
      describe('sagas', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([
              takeEvery(actions.FETCH_DATA, sagas.fetchData),
              takeEvery(actions.RESTORE_ENTITIES, sagas.restoreEntity)
            ])
          )
          expect(generator.next().done).to.be.true
        })

        const selection = {entityName: 'Report'}

        describe('fetchData', () => {
          const fakeResponse = {
            body: {
              selectedOptions: {
                Report: true,
                Business_unit_corporate_design: false,
                Output_template_field: false,
                Output_template: false
              }
            }
          }

          test('should fetch user data', () =>
            expectSaga(sagas.fetchData)
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call.fn(rest.simpleRequest), fakeResponse]
              ])
              .put(actions.setData(fakeResponse.body.selectedOptions))
              .run())
        })

        describe('restoreEntity', () => {
          const selectedKeys = {
            Report: true,
            Output_template: false
          }

          const resource = `/synchronisation/actions/restoreEntity/${selection.entityName}`
          const options = {
            method: 'POST',
            body: {
              selectedKeys,
              selection
            },
            acceptedErrorCodes: ['VALIDATION_FAILED']
          }

          test('successful restore', () => {
            const body = {}
            const status = 204
            return expectSaga(sagas.restoreEntity, {payload: {selectedKeys}})
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.simpleRequest, resource, options), {body, status}]
              ])
              .put(actions.setLoading(true))
              .put(
                externalEvents.fireExternalEvent('onSuccess', {
                  title: 'client.actions.restore-entity.succes-message.header',
                  message: 'client.actions.restore-entity.succes-message.body'
                })
              )
              .put(actions.setLoading(false))
              .run()
          })

          test('validation error', () => {
            const body = {
              errorCode: 'VALIDATION_FAILED',
              errors: [
                {
                  model: 'Report',
                  entityValidatorErrors: {
                    customValidator: ['failed']
                  }
                }
              ]
            }
            const status = 400
            return expectSaga(sagas.restoreEntity, {payload: {selectedKeys}})
              .provide([
                [select(sagas.inputSelector), {selection}],
                [matchers.call(rest.simpleRequest, resource, options), {body, status}]
              ])
              .put(actions.setLoading(true))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: 'failed'
                })
              )
              .put(actions.setLoading(false))
              .run()
          })
        })
      })
    })
  })
})
