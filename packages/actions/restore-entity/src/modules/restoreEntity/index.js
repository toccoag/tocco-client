import {fetchData, postData} from './action'
import reducer from './reducer'
import sagas from './sagas'

export {fetchData, postData}
export {sagas}
export default reducer
