export const FETCH_DATA = 'restoreEntity/FETCH_DATA'
export const SET_DATA = 'restoreEntity/SET_DATA'
export const RESTORE_ENTITIES = 'restoreEntity/RESTORE_ENTITIES'
export const IS_LOADING = 'restoreEntity/IS_LOADING'

export const fetchData = () => ({
  type: FETCH_DATA
})

export const postData = selectedKeys => ({
  type: RESTORE_ENTITIES,
  payload: {
    selectedKeys
  }
})

export const setData = data => ({
  type: SET_DATA,
  payload: {
    data
  }
})

export const setLoading = isLoading => ({
  type: IS_LOADING,
  payload: {
    isLoading
  }
})
