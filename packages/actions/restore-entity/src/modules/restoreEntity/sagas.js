import {all, call, put, takeEvery, select} from 'redux-saga/effects'
import {rest, notification, externalEvents} from 'tocco-app-extensions'
import {validation, consoleLogger} from 'tocco-util'

import * as action from './action'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(action.FETCH_DATA, fetchData), takeEvery(action.RESTORE_ENTITIES, restoreEntity)])
}

export function* fetchData() {
  const {selection} = yield select(inputSelector)
  const url = `/synchronisation/actions/restoreEntity/${selection.entityName}`
  try {
    const response = yield call(rest.simpleRequest, url)
    const data = response.body.selectedOptions
    yield put(action.setData(data))
  } catch (e) {
    consoleLogger.logError('Failed to fetch data', e)
    yield put(action.setData(null))
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.actions.restore-entity.fetch-error-message.header',
        body: 'client.actions.restore-entity.fetch-error-message.body'
      })
    )
  }
}

export function* restoreEntity({payload: {selectedKeys}}) {
  const {selection} = yield select(inputSelector)
  const url = `/synchronisation/actions/restoreEntity/${selection.entityName}`
  const options = {
    method: 'POST',
    body: {
      selectedKeys,
      selection
    },
    acceptedErrorCodes: ['VALIDATION_FAILED']
  }
  yield put(action.setLoading(true))
  const response = yield call(rest.simpleRequest, url, options)
  if (response.status === 204) {
    yield put(
      externalEvents.fireExternalEvent('onSuccess', {
        title: 'client.actions.restore-entity.succes-message.header',
        message: 'client.actions.restore-entity.succes-message.body'
      })
    )
  } else if (response.body.errorCode === 'VALIDATION_FAILED') {
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.component.actions.validationError',
        body: validation.getErrorCompact(response.body.errors)
      })
    )
  }
  yield put(action.setLoading(false))
}
