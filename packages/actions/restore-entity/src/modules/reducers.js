import reducer, {sagas as restoreEntitySagas} from './restoreEntity'

export default {
  restoreEntity: reducer
}

export const sagas = [restoreEntitySagas]
