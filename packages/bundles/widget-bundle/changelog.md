3.13.53
- fix wrong redirect sso login url

3.13.52
- remove widget and bundle for customer azek

3.13.51
- make widget date picker more compact

3.13.50
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- adjust widgets search
- align ranges horizontally for widgets

3.13.49
- handle layout-scope
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.48
- add isTransparent prop to Ball to enable transparent backgrounds
- add transparent background

3.13.47
- use formSuffix for navigation cell

3.13.46
- fix document component

3.13.45
- fix modal in routerless widgets

3.13.44
- show icon on tiles
- add icons

3.13.43
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- able to hide the entity browser and be aware of route changes
- able to navigate back to list on all visibitlity status
- add week numbers to date picker

3.13.42
- fix documnet upload in dms
- display input fields in basic search form as grid layout
- improve styling for search form on widgets
- enhance responsiveness of search form in entity list
- improve responsiveness of entity-list search form
- add labelVisibility prop to Button to enable omitting the prop in specific cases
- improve label for extended search button
- harmonize search form button spacing and positioning
- set optional limit to undefined if obsolete because of passed modules

3.13.41
- fix sidepanel button overflow
- reduce limit on optional modules for passed modules
- able to set justify alingment on tile column
- handle use label properly

3.13.40
- harmonize react select spacing

3.13.39
- fix focus styling

3.13.38
- close all modals on route change
- add title to actions pending changes modal
- add title to navigate pending changes modal
- add pending changes modal on dms
- fix navigation cell header in dms
- adapt component list text resources

3.13.37
- scroll on correct position after page transition
- fix adress / location browser autofill
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.36
- force rerender of ckeditor with auto-complete
- export idSelector
- fix ckeditor and initial auto complete

3.13.35
- pass rating ids to keyboard navigation

3.13.34
- improve label / form field component accessibility
- improve tile responsiveness on mobile devices

3.13.33
- fix stylelint error by removing obsolete styling

3.13.32
- remove useSelectorsByDefault property

3.13.31
- allow setting title for button
- pass inWrongBusinessUnit

3.13.30
- add SimpleFormApp

3.13.29
- add data-cy to table refresh button

3.13.28
- add background color for table tiles
- improve tile styling

3.13.27
- no error message on column resize

3.13.26
- preserve splat path after creating entity

3.13.25
- added data-cy to mail-action button
- handle null minChars in formDefinition

3.13.24
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.23
- adjust min char search field validation
- clean up searchFormValiation

3.13.22
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.21
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.20
- fix stated value labels spacing
- fix color of label-neutral

3.13.19
- harmonize google maps icon positioning in LocationEdit

3.13.18
- add timezone header

3.13.17
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to history

3.13.16
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.15
- add data-cy attributes
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.14
- cleanup save button code

3.13.13
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.12
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.11
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.10
- cleanup marking code
- add native date(time)picker for touch devices
- remove custom _widget_key param

3.13.9
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.8
- add _widget_key query param to all requests

3.13.7
- pass escapeHtml to display formatter

3.13.6
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.5
- fix growing textarea
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.4
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper

3.13.3
- disable register button when required modules are forbidden

3.13.2
- Add support for `searchFormType` input prop

3.13.1
- fix sorting of answer options
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.33
- revert focus fix

3.12.32
- do not auto focus anything on touch devices

3.12.31
- improve UX for simple selects on touch devices

3.12.30
- fix double scrollbars in some modals
- fix height of exam edit

3.12.29
- add paymentStatus to initial action
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.28
- add paymentStatus to initial action

3.12.27
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.26
- add tooltip texts for buttons of upload component

3.12.25
- Make submit button hideable

3.12.24
- fix text select input edit and show available options
- fix table edit styling

3.12.23
- set checkbox field to touched on safari/ipad/iphone

3.12.22
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.21
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.20
- add copy function to entity-browser

3.12.19
- show correct link for subtables

3.12.18
- hide table refresh button when nothing happens

3.12.17
- able to navigate to create view on detail

3.12.16
- Make buttons in evaluation-execution-participation-action customizable

3.12.15
- remove selection controller in dms

3.12.14
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on toaster close buttons

3.12.13
- transform initial value paths

3.12.12
- add context to auto-complete
- use ListView helpers

3.12.11
- add sorting, pagination and column widths
- use SimpleTableFormApp
- show correct select field in export action
- show max age hint

3.12.10
- use error message when action fires error event

3.12.9
- support report actions in adjustAllActions

3.12.8
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- add simple table form app
- use core components
- remove unused / broken styling
- disable virtual table

3.12.7
- be able to accept prompt with enter

3.12.6
- extend font sizes with missing sizes

3.12.5
- trim searchInput value
- improve line numbers color in dark mode
- save column width in preferences

3.12.4
- add declareColoredLabel util
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix sorting of columns with missing preferences
- fix sorting of columns with missing preferences
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.37
- do not apply btn hover style on disable buttons

3.11.36
- handle query changes properly

3.11.35
- se correct color for promotion status
- add copy icon
- improve history action ui
- pass origin event for onChange
- improve history action data loading
- preselect row
- fix outputjob in synchron simple action
- fix navigatino to document edit page
- fix initial navigation to detail view or action
- improve diff viewer theme

3.11.34
- support search fields in useAutofocus

3.11.33
- add legacy font sizes to HtmlEditor
- fix selection handling
- cleanup transient props
- adjust toaster for action validation

3.11.32
- use adjustAction
- use adjustAction
- use adjustAction
- use adjustAction
- TOCDEV-9023

3.11.31
- improve selection in history table
- adjust diff view
- implement breadcrumb and adjust navigation
- add url helpers to diff view
- support history action
- clear selection if new table is loaded

3.11.30
- add mailbox icon

3.11.29
- read formName from input

3.11.28
- add default values as input

3.11.27
- navigate to parent folder after deletion

3.11.26
- use correct shouldForwardProp implementation

3.11.25
- export combineSelection
- add new property to theme
- add navigateToPath to navigationStrategy
- add icon
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18

3.11.24
- update react-router v6
- update react-router v6
- remove unused history

3.11.23
- use transient props for styled components
- align stated value boxes content vertically

3.11.22
- add new icons

3.11.21
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.20
- migrate to react router v6

3.11.19
- add truck-medical icon

3.11.18
- add id to formatted values
- fix user menu widget not opening
- replace keyHandler with navigateTable util
- replace keyHandler with navigateTable util

3.11.17
- add active condition for loading page limit options
- add active condition for loading page limit options

3.11.16
- standardize sticky button wrapper styling
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes

3.11.15
- standardize sticky button wrapper styling
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic

3.11.14
- migrate routes to v5 compat package

3.11.13
- support post flush validation
- replace default props in tocco-ui with javascript default parameters
- replace default props in entity-detail with javascript default parameters

3.11.12
- ignore undefined optionDisplay

3.11.11
- implement validation for simple actions

3.11.10
- update to yarn 4

3.11.9
- handle rule provider in widgets
- handle optionDisplay in remote and select fields

3.11.8
- do not overwrite toasters from sync simple actions
- add react-router compatibility package

3.11.7
- fix tooltips freeze by replacing popper library with floating-ui library

3.11.6
- fix range mapping after upgrade to date-fns 3.x

3.11.5
- fix formatted value colors in dark mode labels

3.11.4
- allow auto focus to be disabled
- disable search field focusing in sub grids in widgets
- set isSubGrid property

3.11.3
- fix button group not being displayed correctly after styled components update

3.11.2
- fix react-select menu after upgrade to styled component

3.11.1
- merge ModalDisplay and BlockingDisplay into OverlayDisplay
- show error id for actions
- fix clear number field via auto complete

3.11.0
- initial release for version 3.11

3.10.33
- map postcode fields to string tql
- reset to default when deselecting template
- add option to reevalute action condition
- reevaluate action conditions if detail reloaded

3.10.32
- enhance toasters with large content by adding vertical scrollbar

3.10.31
- fix popover text and stated value text not wrapping

3.10.30
- render html in toaster body
- return horizontal drop position in useDnD
- allow dropping and display drop indicator on both sides in ColumnPicker

3.10.29
- add row numbering to ui table
- add row numbering

3.10.28
- rename property description to title
- show label and description in search filter tooltip
- move rest endpoint to client "route"
- fix side panel vertical scrolling in create view

3.10.27
- fix hostname for custom actions in cms
- add method to extract env as input props
- pass env to dynamic action

3.10.26
- fix datepicker styling in widgets by providing the tocco-app classname which prevents webkinder styles being applied
- let requestSaga handle blocking info in simpleAction

3.10.25
- always require captcha for anonymous
- disable auto-complete when selecting template
- map phone fields to string tql

3.10.24
- place date picker directly in body to prevent z-index problems in widgets
- identify exams by key instead of nr

3.10.23
- set max height for info boxes
- disable cache for create forms

3.10.22
- do not show same tooltip twice

3.10.21
- delay the display of popovers to prevent distracting flashing effect in large lists on hover
- style link inside label duplicate
- do not show same tooltip twice

3.10.20
- add payment-provider-action
- add queryParams to navigateToAction
- handle separate payment-provider-action
- remove payment logic
- use breaks instead of separate paragraphs for readonly text
- add isDefined helper
- accept falsy values as settings for reports

3.10.19
- fix draggable file/image

3.10.18
- change link color back to secondary'
- harmonize icon list spacing
- style duplicate warning
- optimise last opened color for dark theme
- fix selected day color in date picker
- fix draggable in preview

3.10.17
- enhance breadcrumbs und popover colors for dark mode
- decrease spacing of menu tree items
- enhance dark dark theme colors

3.10.16
- fix save button coloring in dark mode
- make submitbutton customizable

3.10.15
- fix clear of remote field
- improve dark theme color harmony
- enhance table aesthetic and readability

3.10.14
- fix disabled buttons disappearing in widgets
- fix sso login in old cms

3.10.13
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation

3.10.12
- add navigationStrategy as input prop
- pass navigationStrategy to simple action form
- add report to output group instead of new group

3.10.11
- adjust isReadOnly for address and location field
- fix rounding issue for sticky columns
- allow not whitelisted style properties for svg element

3.10.10
- remove obsolete bottom padding in form
- extend widget theme with missing text colors
- add file circle plus icon

3.10.9
- style payment summary view

3.10.8
- do not try to access captcha when hidden
- style fixed columns in table with border at the right edge
- add coloration for number of attempts

3.10.7
- only use captcha when updating password if the target is the current user or the current user is no usermanager
- add title to description element
- add acknowledge notification and entities message
- remove description title again
- handle custom title in action pre checks
- remove ignoreFormDefaultValues property

3.10.6
- disable CKEditor version check
- fix dark theme icon issues within buttons

3.10.5
- set min-height on lower div
- respect itemCount as limit for remote fields
- default disable remote create in widgets

3.10.4
- enable whole area of menu entries to be clicked
- fix dark theme color issues
- apply corresponding theme background color to login screen

3.10.3
- only use AutoSizer with new windowed table

3.10.2
- fix colors in signal list items for dark mode

3.10.1
- respect searchFormType from config

3.10.0
- initial release for version 3.10

3.9.40
- add summary page
- adjust visibility status

3.9.39
- style focused menu elements the same as on hover

3.9.38
- set referrer policy to `no-referrer-when-downgrade`
- add sticky column to ui table
- use other endpoint for delete button
- apply theme themeswitching to ace code editor as well

3.9.37
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin
- fix blocking display disappearing in widgets on long pages

3.9.36
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin

3.9.35
- check rights to remove delete button
- fix selection in remotefield
- fix selection in remotefield

3.9.34
- fix menu items wobble on hover in firefox browser
- fix dark theme color issues
- dynamic determine selectable in table

3.9.33
- change last opened color to increase readability

3.9.32
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- add listApp
- add listApp
- add listApp
- add listApp
- add listApp
- add externalEvents to appFactory
- add externalEvents to store
- add externalEvents to store
- use Debouncer in CodeEdit instead of custom debounce

3.9.31
- use correct search form name as fallback
- enable hover on parent items of menu tree

3.9.30
- map shrinkToContent in getColumnDefinition
- fix jumping buttons in tocco header when opening help menu
- add feature

3.9.29
- improve menu ux by adding hover background and expanding clickable area
- add dark mode
- fix widget theme

3.9.28
- fix intermediate values in time edit

3.9.27
- add missing external events

3.9.26
- add no button option for simple action form

3.9.25
- map email fields to string tql

3.9.24
- add payment methods and payment provider handling
- add payment visibility status

3.9.23
- add new freemarker code widget

3.9.22
- add non breaking space to ensure correct spacing in multiple fields separator

3.9.21
- fix column order resetting when moving multiple columns after another

3.9.20
- add drag'n'drop upload

3.9.19
- support custom report settings in widgets
- harmonize exam edit spacing

3.9.18
- enhance last openend color for better visibility

3.9.17
- limit watchForHover to activate in window.onload

3.9.16
- Add `watchForHover` styling utility

3.9.15
- shrink size of datepicker

3.9.14
- handle 403 error in delete

3.9.13
- add formBase property
- add formBase property
- add formBase property
- add formBase property
- add formBase property
- add formBase property
- add formBase property

3.9.12
- fix width of exam edit table ipunt fields in firefox
- add last opened to ui table
- add last opened to entity-list
- align stated value content verically with label

3.9.11
- use path for setting placeholder terms value
- fix multi-select-field

3.9.10
- hide readonly terms
- fix locale for anonymous
- fix locale for anonymous
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser
- pass locale to entity-browser

3.9.9
- harmonize date picker styling and implement time picker
- handle searchFormType
- fix several selection issues in the dms
- fix selection in docs tree search

3.9.8
- load row number options from Page_limit entities
- fix multivalue separator span alignment

3.9.7
- add updateDocumentTitle prop to Breadcrumbs
- add updateDocumentTitle prop to Breadcrumbs

3.9.6
- register gear icon

3.9.5
- split styling of blocking display for admin and widget
- fix multivalue label text overflow

3.9.4
- add readonly terms config and use pristine value change for placeholders
- handle immutable prop in TermsEdit
- check for writable paths in form instead of actions
- use formBase from widget config
- only set form builder readonly if mode is not create

3.9.3
- use formBase in formModifier

3.9.2
- split toaster into widget and admin components
- adjust height of evaluation tree table to scale depending on available viewport height

3.9.1
- TOCDEV-7717, TOCDEV-7983

3.9.0
- initial release for version 3.9

3.8.22
- move logic from reports saga to backend
- Show error message if too many entities are selected for deletion

3.8.21
- align widget labels at flex start position
- add hideFooter property to entity-detail
- register list-tree icon

3.8.20
- harmonize status label border radii
- add border around exam edit datepicker inputs

3.8.19
- load row number options from Page_limit entities
- fix resizing of ui table

3.8.18
- hide attempts on hidden checkbox
- add download icon to DocumentFormatter
- allow multiple paths and disallow navigation for custom table on SubGrid
- disable navigation if parent defines it
- do not add search value for parent relation if no reverse relation was defined
- handle multiple relation paths by resolving all of them and handle custom relation paths by load the table form

3.8.17
- fix terms edit link color
- navigate to next field without selecting with tab

3.8.16
- only use visible search fields for building tql
- escape front-slash in fulltext tql query

3.8.15
- vertically center input edit table cells
- force underscore styling of terms edit links

3.8.14
- hide overflow of column headers
- register gear icon
- disable sideEffects optimization in webpack
- fix css styles not being passed down to multi value label span
- fix sticky buttons within modal cutting off content
- fix terms edit link styling and refactor component

3.8.13
- register calculator icon

3.8.12
- fix themes by adding missing status label radius
- extract nested ternary operation
- improve exam edit action styling
- fix vertical alignment of html formatter

3.8.11
- add data-cy attributes

3.8.10
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add BasicSearchForm
- add qualification dataRequest
- make select work without formData
- refactor input-edit to use more shared code
- use column position hook
- add useTitle to FormattedValue
- count hidden required checkbox as optional and ignore it in validation
- fix checking if form contains only readonly
- add additional units

3.8.9
- refactor panel group to remove duplications

3.8.8
- fix long lines in code edit being cut off by showing scrollbar
- add id to ReactDatePicker
- add keyboard navigation, editable value focus and postPointDigits
- decouple docs-browser from entity-list
- add default link target and protocol to html editor

3.8.7
- fix disabled button and menu item styles
- fix broken prop types
- do not use array index in keys
- fix legacy styles being applied to status labels

3.8.6
- change fields separator
- change fields separator
- fix test for new separator
- make editableValueFactory work without formField
- create exam-edit
- add exam-edit action
- add exam-edit action

3.8.5
- load email template for non-admins as well
- remove obsolete padding around styled entity browser
- fix disabled textarea on safari

3.8.4
- fix menu menu links not opening in new tab when icon is clicked

3.8.3
- ignore default search filter if tql is passed

3.8.2
- if a form contains only readonly field the save button should be removed
- if a form contains only readonly field the save button should be removed

3.8.1
- use widget prefix for report location
- lighten status label colors and harmonize spacing

3.8.0
- initial release for version 3.8

0.1.211
- harmonize menu layout by moving technical name below label
- fix scrollbars on disabled textareas with content

0.1.210
- reset disabled min-height on textareas

0.1.209
- growing subtables
- show full username
- load email template properly

0.1.208
- harmonize panel spacing and font weights
- make actionConditions work in the children of action groups
- handle follow up actions

0.1.207
- build up modifiers from smaller independent functions

0.1.206
- use new formSuffix to build formBase of SubGrids
- continue handling form elements after a custom component was encountered
- do not crash if no formValues exists when checking terms data
- pass formValues to formBuilder
- pass formValues to formBuilder

0.1.205
- enable co-existence with legacy ckeditor
- improve readability of status labels
- adjust disabled textarea height to match vertical label position

0.1.204
- do not render terms if no conditions were configured
- create function-mutation
- add function-mutation
- check for empty string that element is only added if necessary

0.1.203
- fix getTextOfChildren
- replace deprecated selectUnit

0.1.202
- fix buggy scroll to top for modals
- remove delete action from all forms and add searchFormType
- handle phone validation for relations
- handle validation response properly
- fix broken prop types

0.1.201
- dependency update
- dependency update
- dependency update
- align data-cy naming
- add data-cy attributes

0.1.200
- handle de-DE locale for datepicker
- align terms condition with checkbox properly

0.1.199
- increase left padding of merge table

0.1.198
- add min width to toaster box

0.1.197
- handle attempt and max attempts

0.1.196
- get all address suggestions within country
- render sidepanel initially with correct collapse state
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- add terms component
- add terms component
- add pickAllProperties to getFlattenEntity
- use new terms

0.1.195
- handle sso with redirects
- handle auto login properly
- connect principal with sso login properly
- handle sso login properly

0.1.194
- export logged in action type
- handle showAutentication flag

0.1.193
- add reports after new button
- place label in widgets on left side
- move question code to tocco-util
- add membership questions to membership-registration

0.1.192
- flag selection as deleted on duplicate detail
- navigate to list after fullscreen action deleted selection

0.1.191
- accept limit as string

0.1.190
- handle search form type properly

0.1.189
- increase toaster width to prevent early line breaks within titles
- fix acl browser styling
- add membership-registration widget
- add membership-registration to bundle

0.1.188
- add icon for action group
- fix immutable label positioned inside
- fix log input height
- fix immutable height of integer edit by passing immutable prop

0.1.187
- remove list numbering
- clear cached view stores
- re-load view on locale change

0.1.186
- style edit text within html formatter
- add formApp to actions
- reset value properly

0.1.185
- fix docs browser label alignment
- set correct label position
- fix build for customer bundles

0.1.184
- adjust useSyncValidation
- fix action order
-  add data-cy attr to confirm buttons
- use comon text resources
- use text resources for extended search
- fix hostname field in log view
- load actual question instead of label of the question

0.1.183
- add data-cy attr to delete buttons
- add data-cy attr to submit button

0.1.182
- edit and delete entity template

0.1.181
- fix disabled select spacing
- adjust widget stated value label color
- add action form modifier fn
- add simple form helper functions
- add onFormValuesChange event
- action can ignore pending changes prompt

0.1.180
- fix choice edit label alignment
- fix light cellrenderer width

0.1.179
- handle new formBase attribute
- add core sidepanel horizontal button comp
- use core button for search filters
- handle input default values exclusivly
- add save action with follow up that can start an action after save

0.1.178

0.1.177
- add education-portfolio widget
- add education-portfolio widget
- adjust google maps address link
- allow all html content
- align editor config with legacy editor
- add advanced color picker
- add paste as plaintext
- correct display of numbering
- add upload to editableMapping

0.1.176
- fix null values in percent field

0.1.175
- use new navigation cell to navigate in cypress test
- handle new flags object in action response
- allow hard reload after action and ignore invisible meta fields on soft reload

0.1.174
- add font awesome eye icon
- use eye icon for documents
- export StyledTableRow
- add row numbering and hover link that opens entity in new tab

0.1.173
- fix action button styling

0.1.172
- style promotion status badge
- adjust font factor of widget theme

0.1.171
- extend embed type
- add widget panel
- handle label position via prop
- handle label color for widget and admin differently
- handle different form components for widgets
- treat admin embedType logic as well for legacy-admin
- load formComponents on runtime
- add selectionDeleted for simple action

0.1.170
- add action button styling

0.1.169
- support hide and read-only for location/address

0.1.168
- copy only changesets function
- position toasters at bottom right to prevent interference with interaction elements
- remove x-backend-url header
- align labels in center of input box
- harmonize positioning of sticky buttons within modal

0.1.167
- hide rule checking for difference to old password if old password field is not shown
- copy changelog to clipboard
- add cypress data attribute to breadcrumb

0.1.166
- handle remote create on simpel actions
- handle entity model corretly to support remote create
- handle entity model corretly on form builder
- provide detail app to actions
- provide detail app to entity-list
- show advancedSearch properly

0.1.165
- fix country cache
- expand checkbox and label options for ChoiceEdit
- use new loadModules endpoint

0.1.164
- handle new points types with averages
- move field definition cache to object cache

0.1.163
- move country cache to object cache

0.1.162
- harmonize menutree spacing and implement default icon

0.1.161
- enable enter handling again

0.1.160
- Make sure that address field is visible also without value

0.1.159
- show multiline lables with correct line breaks
- run query automatically
- keep sorting preferences up to date
- Added new component to search for full address

0.1.158
- fix client questions with cancel
- do not save form on open dropdown

0.1.157
- show entity-docs in email attachements

0.1.156
- remove top and left padding within immutable stated value box
- fix alignment of input fields within input edit table
- show back action always before reports
- fix fulltext search

0.1.155
- harmonize sso login spacing and color
- also implement border radius for indicator container wrapper to prevent cover of input field edges
- fix subfolder search
- add cancel button to confirm modals
- show correct pending changes modal on widgets
- fix border radius calculation of panel headers

0.1.154
- fix baseline of code edit

0.1.153
- fix input width of login form

0.1.152
- copy documents and binary

0.1.151
- position input label at baseline of input

0.1.150
- replace ckeditor5 with ckeditor4
- fix PropTypes - date to string
- fix PropTypes - function to conditions list
- don't show prompt after entity creation
- rename wrapper
- do not infinite reload fullscreen action on reload

0.1.149
- add additional data-cy attributes

0.1.148
- fix height rendering of load mask and spacing of layout boxes within modal

0.1.147
- fix duplicated key on list

0.1.146
- remove duplicated enter handling

0.1.145
- remove minimum height for modal body which is not needed
- add error message if login failed
- remove sso error message
- stop action on failed sso login
- remove [object Object] button title
- ensure pending changes prompt is fired first

0.1.144
- fix missaligned dropdown indicators within select
- fix border radii of input edit select
- add fetchAllEntities which handles paging by itself
- replace fetchEntities
- replace fetchEntities in selection
- replace custom paging in notifications
- replace fetchEntities in formData

0.1.143
- improve readability of disabled multi value label text
- be able to tab through mulit-remote-selects
- navigate to next field with tab on select

0.1.142
- add modifyEntityPaths input parameter
- add modifyEntityPaths input parameter
- add initialAction
- remove history in action and use initialAction of entity-browser
- use initialAction of entity-browser

0.1.141
- add global classname for all wrapping elements

0.1.140
- fix address-update after adding action conditions
- use global scrollbar style instead of specific imports
- support column action in adjustActions
- pass reportDisplayId property to action
- support (dynamic) reports in evaluation-execution-participation-action

0.1.139
- fix miscellaneous ui bugs in widgets

0.1.138
- adjust input field z-index and harmonize padding within input edit
- harmonize modal spacing and prevent scrollbar with minimum height introduction

0.1.137
- add action conditions
- harmonize label spacing in choice edit
- refactor simple form so that submit via enter key works and prevent nested forms
- add outputgroup when multiple reports

0.1.136
- show reset sorting again
-
- do not pass meta fields to onSubmit

0.1.135
- fix report generation with empty model

0.1.134
- invoke action only once
- only update input when app is mounted
- restructure cache for widget use cases
- remove revision from object cache
- fix text resource in evaluation-execution-participation-action
- ignore meta fields on values
- add form to store and add form model name

0.1.133
- handle expanded range field via default value
- remove unused expanded property

0.1.132
- add X-Backend-Url to all requests
- adjust X-Backend-Url to all requests
- add share ansers functionality
- add finish evaluation
- add save on change
- add visbility status for evaluation-execution-participation
- add visbility status for evaluation-execution-participation-action

0.1.131
- do not show pending changes prompt twice

0.1.130
- show error message when deletion is not possible

0.1.129
- add error handling in evaluation-execution-participation-action
- add synchronous validation for pseudoSelect
- add error handling during loading
- add pseudo submitting which just blur form
- fix mandatory validation for pseudoSelect
- add back button
- add short link

0.1.128
- fix border radius of search boxes
- fix autofocus on search form
- fix select fields exceeding the new maximum limit and crashing

0.1.127
- fix phone edit
- fix widget code copy action
- fix datepicker input styling

0.1.126
- do not submit form on select item
- do not propagate and use default for enter key

0.1.125
- refactor errors in pseudo form fields
- refactor errors in event-registration-action
- allow actions to be hidden by backend

0.1.124
- Button as well as code are only shown when visibility-status selected
- removed filename

0.1.123
- add evaluation-execution-participation widget
- add evaluation-execution-participation widget to widget-bundle
- add pseudo select ui component
- add pseudo select field
- add evaluation-execution-participation-action action
- add evaluation-execution-participation-action to entity-browser

0.1.122
- create job-offer-apply action
- create job-application widget

0.1.121
- upgrade to react v18

0.1.120
- allow customization of validation messages
- add checkbox pseudo field helper
- use checkbox pseudo field for terms to use custom validation message

0.1.119
- show correct border-radius on panel header
- enforce css selectors for textarea

0.1.118
- improve navigation on login workflow

0.1.117
- add events for authentication and register page
- add visibility status for authentication and register
- fix light cellrenderer font sizing to prevent icon overflow within cells

0.1.116
- add geosearch field

0.1.115
- trigger visibility status change only for widget embed type
- add visibility status to password update
- add visibility status to mailing-list

0.1.114
- add onSuccess to synchronous simple actions
- enable custom action event handlers in entity-detail and entity-list
- pass customActionEventHandlers to store instead of actionFactory
- add visibility status

0.1.113
- fix init redux form
- style label based on publish status as in old client

0.1.112
- fix cancelling client questions

0.1.111
- when creating a new entity, trigger auto-complete for fields with default values

0.1.110
- fix header and content cell aligment of table
- show toaster with validation errors
- harmonize nav and search box styling

0.1.109
- show correct search icon

0.1.108
- fix bundling apps

0.1.107
- make yes button in YesNo client question work again
- re-init cache instead of clear cache
- fix caching and intl race condition

0.1.106
- navigate back to list when state is lost
- remove unnecessary usage of darken and lighten functions and use theme colors instead
- fix move action for multiple root folders
- limit login container width

0.1.105
- add option to disable onclick on breadcrumb
- disable clicking on search result in breadcrumb

0.1.104
- map function to props correctly
- remove light cellrenderer flex so that it does not cut off content in table
- enforce border of input fields within input edit table

0.1.103
- remove usage of shade color function and extend theme instead
- introduce radii

0.1.102
- tqlBuilder now builds multiple conditions connected by 'or' when passed localized paths
- search fields for a localized field now search in every available locale
- show validation errors on deletion
- accept enter two-year date without punctation

0.1.101
- use correct locale for two factor connector
- clean up shade color function use and switch to theme colors instead
- add on enter feature to advanced search
- add on enter feature to two factor connector action

0.1.100
- extend theme with more specific colors to better target elements
- adapt new specific theme colors to table, select and column picker
- handle sub module questions
- pass all query params to action
- simplify action url

0.1.99
- hide empty submodules table
- handle url search fields as strings
- be able to have uppercase urls

0.1.98
- standardize yes no question modal buttons

0.1.97
- fix theme selectors
- add agb and privacy protection fields

0.1.96
- add detail for event the user is registering too
- render fields reached through an empty but writable path
- improve global styles

0.1.95
- use correct fonts for choices
- add modules
- use correct locale for login comp
- create custom event registration widget
- add event-registration widget
- add visibility status and hide after register

0.1.94
- harmonize spacing of breadcrumbs

0.1.93
- pass widget key as query param so backend can read it in a standardized way
- add min/max selection option for multiple-choice
- support min and max selection in validation
- add login component

0.1.92
- add username form to login
- restructre login page input props
- fix paddings for user-menu button
- fix error message alignment
- add the possibility to close some modals with enter key if button is primary
- add choice ui component
- add choice field
- add form pseudo field logic
- add additional condition to getUsedPaths

0.1.91
- create event-registration-action
- add event-registration-action

0.1.90
- standardize page overlay and holder components for modal and blocking info

0.1.89
- datepicker focus handling on ffox and date only inputs
- lower height threshold of button context

0.1.88
- show proper error msg for 413
- fix react attribute error

0.1.87
- fix default confirm button
- fix sidepanel height
- enable scrollBehaviour on detail
- extend theme
- add sticky action bar
- simplify dynamic action configuration

0.1.86
- add icons for user menu and business unit menu inside header

0.1.85
- display more custom icons in Breadcrumbs as defined by backend, specifically domains and folders in dms

0.1.84
- display percent fields correctly

0.1.83
- fix login page height not filling full page
- be able to embed bootstrap script relative

0.1.82
- add autoFocusOnMount prop to ace editor to set focus manually since the useAutoFocus hook is not working in this case
- replace range type icons with generic icon and harmonize alignment to improve UX
- fix modal position in widget context and use vh units
- add feature to save number of rows as user preference in entity list

0.1.81
- add visibility status code copy action
- rename onStateChange to onVisibilityStatusChange
- rename onStateChange to onVisibilityStatusChange
- set correct user locale after login

0.1.80
- pass navigation strategy to actions
- fix table randomly jumping scroll positions when switching pages

0.1.79
- fix autofocus by wrapping selector in timeout

0.1.78
- add possibility to show reload prompt
- add prompt component
- show user confirmation on browser-routing
- show user confirmation without blur
- pass selection from url to fullscreen action

0.1.77
- user now gets new widget forms for personal-dms

0.1.76
- add back button
- use core back button
- use core back button for cancel

0.1.75
- enhance external events
- add states (list, detail, fullscreen-action)
- add onStateChange event

0.1.74
- fix object cache

0.1.73
- re-load data on login
- add object cache
- use global object cache

0.1.72
- allow padding of Button to be removed like when used as icon
- allow Action to be rendered as text without button visual
- export action types to use same names everywhere
- confirm modals now use whichever default action the backend declares, which means switching the styling and keyboard shortcut

0.1.71
- prevent action buttons within table of being missaligned in widgets

0.1.70
- order range values on top instead of side by side to improve ux
- create ENTITY_DOCS selection for legacy actions
- move submit button to parent
- add sync validation to template form
- disable submit button for invalid form

0.1.69
- support identifier type in tql builder
- enforce css selectors
- enforce css selectors

0.1.68
- fixed width for user-menu

0.1.67
- fix save button being clipped and change hover on error
- fix react-select dropdown being clipped inside input-edit table
- link for max 100 created entities
- add docs-tree search
- add conditional selection for tables
- only select entities for docs tree search
- set folders as root folders

0.1.66
- allow target attribute in html
- harmonize spacing of input edit action wrapper

0.1.65
- do not use default display for tooltips again

0.1.64
- remove div wrapper of entity-browser as it was preventing it from filling the full available height when embedded in the old client
- send selection to adjusted POST endpoint when loading report settings
- use a new custom display when displaying entities in select boxes

0.1.63
- harmonize spacing of ranged dates by increasing search panel width
- add public dms widget
- add public-dms widget to bundle
- simplify widget code

0.1.62
- reintroduce min-width for Ball component, since normal width caused rendering issues in the main menu
- replace useWindowWidth hook with universal useWindowSize hook
- possible to define custom render for actions
- define back button through custom render

0.1.61
- do not collapse sidepanel on top

0.1.60
- show time in 24h format for all locales

0.1.59
- move toasters on mobile to bottom for better ux as most of the interaction elements (e.g actions) are on top and should not be covered
- setup envs for entity-dms
- setup envs for personal-dms

0.1.58
- input edit info action for showing meta info
- add input-edit-info action
- do not reload fullscreen action on single action
- be able to click on row in action cell

0.1.57
- add widget entity-dms
- add collapsible sidepanel component
- possible to pass beforeRenderField func
- introduce legacy-admin embed type
- same sidepanel behaviour as in admin
- use reusable sidepanel component
- use simple_advanced search form
- autofocus form fields
- prepare sso-login widget for widget embedType

0.1.56
- remove tocco subdomain

0.1.55
- do not use unnecessary generator function
- use Icon component in LoadingSpinner
- replace values of readonly input fields with loading spinner while the update request is processing

0.1.54
- add missing setInputEnvs

0.1.53
- move component specific event handlers
- pass redirectUri for sso logins in widgets
- add user menu widget
- add sso-login and login package to widget bundle

0.1.52
- fix null values in field name transformation
- support nested paths in location field
- provide entityField for create field config
- use long as datetime value

0.1.51
- allow componentConfigs to adjust value used
- use correct decimal digits and % suffix for percentage fields
- use FormattedNumber in percent mode in PercentFormatter
- always show search filter button icons on touch devices because of missing hover function
- increase top padding of input label to prevent cut off

0.1.50
- add personal dms widget

0.1.49
- disable removing outputjob toasters in widgets
- fix performance of fulltext search

0.1.48
- add export action to entity-browser
- prevent body scroll when modal is opened

0.1.47
- use back button in main action bar

0.1.46
- improve responsiveness of table component
- datepicker keyboard handling
- do not lazy load datepicker
- autofocus datepickers

0.1.45
- add helpers to remove fields from forms
- create user-grades widget

0.1.44
- fix escape handling in tql fulltext search

0.1.43
- improve save button look on error

0.1.42
- time input component
- fix scroll on select

0.1.41
- add mobile collapse feature on list view
- add widget config key to env
- use widget config key from env
- add custom create endpoint to validation
- add custom create endpoint
- add copy action to entity-browser
- remove removeCreate form modifier
- add addCreate form modifier
- use addCreate form modifier
- use addCreate form modifier
- load defalut values on action forms

0.1.40
- do not refresh list multiple times
- show correct search form in subgrid

0.1.39
- add user availability widget
- add pen-field icon
- increase menu item padding on touch devices for better usability

0.1.38
- transform QUERY to ID selection for actions (if custom endpoint)

0.1.37
- let entity list handle changed props properly
- ignore any text and textarea fields of a datepicker when autofocusing

0.1.36
- fix date picker icon

0.1.35
- fix null pointer when starting in standalone mode
- added action callbacks to navigate back

0.1.34
- fix datepicker year dropdown closing on scroll
- fix whitespace in notification

0.1.33
- close datepicker on scroll
- add calendar icon
- add icon to date picker

0.1.32
- add model name to notification

0.1.31
- add 'cancel' custom action to go back

0.1.30
- implement sticky buttons for simple-form modal
- reset select dropdown to older state and implement close on scroll feature
- fix select dropdown dimension calculations

0.1.29
- adjust sizing of datepicker on mobile screens
- make constriction, search filters, tql and keys passed through input props work again

0.1.28
- generate reports without dialog in widgets

0.1.27
- collapse buttons when wrapper is too small
- use responsiveness buttons on action bar
- use responsiveness buttons for actions

0.1.26
- add ballot-check icon
- use more generic error message when no Calendar_export_conf could be found

0.1.25
- quick fix that onChange is not infinite often triggered

0.1.24
- HTML editor: fix saving the form in source editing mode

0.1.23
- apply global styles to fix datepicker
- enable fullscreen modals for mobile/touch devices

0.1.22
- show collapse/expand icon on touch devices in panel header/footer
- harmonize modal spacing of modal message

0.1.21
- remove date picker arrow as the positioning seems to be off when fields are too long

0.1.20
- add business unit to input props
- fix position of reports in form modifier
- fix datetime range with single default value
- do not clear date input on blur

0.1.19
- improve datepicker styling and fix bugs in widgets
- force hover styles of upload button when hovering upload field
- remove advanced search text in modal title
- fix input label being cut in widgets

0.1.18
- remount entity-detail app after navigation
- change signal box background colors for more consistency

0.1.17
- add stint auction widget
- fix change date on range date picker
- fix datetime search field with default value
- keep ckeditor as similar as legacy editor

0.1.16
- check form definition if form is markable

0.1.15
- only cache displays as long as tab is open

0.1.14
- use constrictions from form in remotefield as well

0.1.13
- fix navigation to subfolder inside search results

0.1.12
- remotefields now sort the same way in select box and advanced search (form sorting first, then last updated)

0.1.11
- use save aborted key as textresource instead of message directly

0.1.10
- reports added through formModifier are now placed directly into the action bar, instead of the output group
- report definitions now contain an icon as defined by the backend

0.1.9
- entity-browser can now display reports passed as ids in input
- remote fields now honor the constrictions defined on forms with remotefield scope
- advanced search now uses form with remotefield scope if it exists
- remotefields now use sorting of their forms or entity models instead of only their timestamp

0.1.8
- fix remote error handler
- create removeBoxes to help adjust forms
- create mailing-list widget
- use reports from widget config to user list
- create mailing-list-mail-action for mailing-list flow
- fix error logging app extension
- add error boundary

0.1.7
- prepare for customer actions
- prepare for customer actions

0.1.6
- add condition to remote and select field
- simple actions add conditions

0.1.5
- add css classes for display expressions styling

0.1.4
- Length and size validators no longer trigger if the max or min is not defined

0.1.3
- refactor business unit input properties

0.1.2
- add disableDetailView property
- add reservation-lecturer-booking-view widget
- add subscribe-calendar action to entity-browser
- add reservation-lecturer-booking-view widget
- add form properties to prepare request

0.1.1
- fix state handling for report settings
- fix form mapping for search componentConfig
- use boolean search component for marking

0.1.0
- Initial release
