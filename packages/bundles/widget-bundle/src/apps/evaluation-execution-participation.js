import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-evaluation-execution-participation/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
