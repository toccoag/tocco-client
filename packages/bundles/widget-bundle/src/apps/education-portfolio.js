import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-education-portfolio/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
