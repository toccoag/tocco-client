import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-freemarker-code/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
