3.13.51
- fix wrong redirect sso login url

3.13.50
- make widget date picker more compact

3.13.49
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- adjust widgets search
- align ranges horizontally for widgets

3.13.48
- handle layout-scope
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.47
- add isTransparent prop to Ball to enable transparent backgrounds
- add transparent background

3.13.46
- use formSuffix for navigation cell

3.13.45
- fix document component

3.13.44
- refactor full calendar navigation and add week number
- fix modal in routerless widgets

3.13.43
- show icon on tiles
- add icons

3.13.42
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- able to hide the entity browser and be aware of route changes
- add week numbers to date picker

3.13.41
- fix documnet upload in dms
- display input fields in basic search form as grid layout
- improve styling for search form on widgets
- enhance responsiveness of search form in entity list
- improve responsiveness of entity-list search form
- add labelVisibility prop to Button to enable omitting the prop in specific cases
- improve label for extended search button
- harmonize search form button spacing and positioning
- set optional limit to undefined if obsolete because of passed modules

3.13.40
- fix sidepanel button overflow
- reduce limit on optional modules for passed modules
- able to set justify alingment on tile column
- handle use label properly

3.13.39
- harmonize react select spacing

3.13.38
- fix focus styling

3.13.37
- close all modals on route change
- add title to actions pending changes modal
- add title to navigate pending changes modal
- add pending changes modal on dms
- fix navigation cell header in dms
- adapt component list text resources

3.13.36
- scroll on correct position after page transition
- fix adress / location browser autofill
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.35
- force rerender of ckeditor with auto-complete
- export idSelector
- fix ckeditor and initial auto complete

3.13.34
- pass rating ids to keyboard navigation

3.13.33
- improve label / form field component accessibility
- improve tile responsiveness on mobile devices

3.13.32
- fix stylelint error by removing obsolete styling

3.13.31
- remove useSelectorsByDefault property

3.13.30
- allow setting title for button
- pass inWrongBusinessUnit

3.13.29
- add SimpleFormApp

3.13.28
- add data-cy to table refresh button

3.13.27
- add background color for table tiles
- improve tile styling

3.13.26
- no error message on column resize

3.13.25
- preserve splat path after creating entity

3.13.24
- added data-cy to mail-action button
- handle null minChars in formDefinition

3.13.23
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.22
- adjust min char search field validation
- clean up searchFormValiation

3.13.21
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.20
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.19
- fix stated value labels spacing
- fix color of label-neutral

3.13.18
- harmonize google maps icon positioning in LocationEdit

3.13.17
- add timezone header

3.13.16
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to history

3.13.15
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.14
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.13
- cleanup save button code

3.13.12
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.11
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.10
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.9
- cleanup marking code
- add native date(time)picker for touch devices
- remove custom _widget_key param

3.13.8
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.7
- add _widget_key query param to all requests

3.13.6
- pass escapeHtml to display formatter

3.13.5
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.4
- fix growing textarea
- add left cell alignment for table
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.3
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper

3.13.2
- disable register button when required modules are forbidden

3.13.1
- fix sorting of answer options
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.32
- revert focus fix

3.12.31
- do not auto focus anything on touch devices

3.12.30
- improve UX for simple selects on touch devices

3.12.29
- fix double scrollbars in some modals
- fix height of exam edit

3.12.28
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.27
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.26
- add tooltip texts for buttons of upload component

3.12.25
- Make submit button hideable

3.12.24
- fix text select input edit and show available options
- fix table edit styling

3.12.23
- set checkbox field to touched on safari/ipad/iphone

3.12.22
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.21
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.20
- add copy function to entity-browser

3.12.19
- show correct link for subtables

3.12.18
- hide table refresh button when nothing happens

3.12.17
- able to navigate to create view on detail

3.12.16
- Make buttons in evaluation-execution-participation-action customizable

3.12.15
- remove selection controller in dms

3.12.14
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on toaster close buttons

3.12.13
- transform initial value paths

3.12.12
- add context to auto-complete
- use ListView helpers

3.12.11
- add sorting, pagination and column widths
- use SimpleTableFormApp
- show correct select field in export action
- show max age hint

3.12.10
- use error message when action fires error event

3.12.9
- support report actions in adjustAllActions

3.12.8
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- add simple table form app
- use core components
- remove unused / broken styling
- disable virtual table

3.12.7
- be able to accept prompt with enter

3.12.6
- extend font sizes with missing sizes

3.12.5
- trim searchInput value
- improve line numbers color in dark mode
- save column width in preferences

3.12.4
- add declareColoredLabel util
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix sorting of columns with missing preferences
- fix sorting of columns with missing preferences
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.35
- do not apply btn hover style on disable buttons

3.11.34
- handle query changes properly

3.11.33
- se correct color for promotion status
- add copy icon
- improve history action ui
- pass origin event for onChange
- improve history action data loading
- preselect row
- fix outputjob in synchron simple action
- fix navigatino to document edit page
- fix initial navigation to detail view or action
- improve diff viewer theme

3.11.32
- support search fields in useAutofocus

3.11.31
- add legacy font sizes to HtmlEditor
- fix selection handling
- cleanup transient props
- adjust toaster for action validation

3.11.30
- TOCDEV-9023

3.11.29
- improve selection in history table
- adjust diff view
- implement breadcrumb and adjust navigation
- add url helpers to diff view
- support history action
- clear selection if new table is loaded

3.11.28
- add mailbox icon

3.11.27
- navigate to parent folder after deletion

3.11.26
- use correct shouldForwardProp implementation

3.11.25
- export combineSelection
- add new property to theme
- add navigateToPath to navigationStrategy
- add icon
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18

3.11.24
- update react-router v6
- update react-router v6
- remove unused history

3.11.23
- use transient props for styled components
- align stated value boxes content vertically

3.11.22
- add new icons

3.11.21
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.20
- migrate to react router v6

3.11.19
- add truck-medical icon

3.11.18
- add id to formatted values
- replace keyHandler with navigateTable util
- replace keyHandler with navigateTable util

3.11.17
- add active condition for loading page limit options
- add active condition for loading page limit options

3.11.16
- standardize sticky button wrapper styling
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes

3.11.15
- standardize sticky button wrapper styling
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic

3.11.14
- migrate routes to v5 compat package

3.11.13
- support post flush validation
- replace default props in tocco-ui with javascript default parameters
- replace default props in entity-detail with javascript default parameters
- replace default props in scheduler with javascript default parameters

3.11.12
- ignore undefined optionDisplay

3.11.11
- implement validation for simple actions

3.11.10
- update to yarn 4

3.11.9
- handle rule provider in widgets
- handle optionDisplay in remote and select fields

3.11.8
- do not overwrite toasters from sync simple actions
- add react-router compatibility package

3.11.7
- fix tooltips freeze by replacing popper library with floating-ui library

3.11.6
- fix range mapping after upgrade to date-fns 3.x

3.11.5
- fix formatted value colors in dark mode labels

3.11.4
- allow auto focus to be disabled
- disable search field focusing in sub grids in widgets
- set isSubGrid property

3.11.3
- fix button group not being displayed correctly after styled components update

3.11.2
- fix react-select menu after upgrade to styled component

3.11.1
- merge ModalDisplay and BlockingDisplay into OverlayDisplay
- show error id for actions
- fix clear number field via auto complete

3.11.0
- initial release for version 3.11

3.10.32
- map postcode fields to string tql
- reset to default when deselecting template
- add option to reevalute action condition
- reevaluate action conditions if detail reloaded

3.10.31
- enhance toasters with large content by adding vertical scrollbar

3.10.30
- fix popover text and stated value text not wrapping

3.10.29
- render html in toaster body
- return horizontal drop position in useDnD
- allow dropping and display drop indicator on both sides in ColumnPicker

3.10.28
- add row numbering to ui table
- add row numbering

3.10.27
- rename property description to title
- show label and description in search filter tooltip
- move rest endpoint to client "route"
- fix side panel vertical scrolling in create view

3.10.26
- fix hostname for custom actions in cms
- add method to extract env as input props
- pass env to dynamic action

3.10.25
- fix datepicker styling in widgets by providing the tocco-app classname which prevents webkinder styles being applied
- let requestSaga handle blocking info in simpleAction

3.10.24
- always require captcha for anonymous
- disable auto-complete when selecting template
- map phone fields to string tql

3.10.23
- place date picker directly in body to prevent z-index problems in widgets
- identify exams by key instead of nr

3.10.22
- set max height for info boxes
- disable cache for create forms

3.10.21
- do not show same tooltip twice

3.10.20
- delay the display of popovers to prevent distracting flashing effect in large lists on hover
- style link inside label duplicate
- do not show same tooltip twice

3.10.19
- add payment-provider-action
- add queryParams to navigateToAction
- remove payment logic
- use breaks instead of separate paragraphs for readonly text
- add isDefined helper
- accept falsy values as settings for reports

3.10.18
- fix draggable file/image

3.10.17
- change link color back to secondary'
- harmonize icon list spacing
- style duplicate warning
- optimise last opened color for dark theme
- fix selected day color in date picker
- fix draggable in preview

3.10.16
- enhance breadcrumbs und popover colors for dark mode
- decrease spacing of menu tree items
- enhance dark dark theme colors

3.10.15
- fix save button coloring in dark mode

3.10.14
- fix clear of remote field
- improve dark theme color harmony
- enhance table aesthetic and readability

3.10.13
- fix disabled buttons disappearing in widgets
- fix sso login in old cms

3.10.12
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation

3.10.11
- add navigationStrategy as input prop
- pass navigationStrategy to simple action form
- add report to output group instead of new group

3.10.10
- adjust isReadOnly for address and location field
- fix rounding issue for sticky columns
- allow not whitelisted style properties for svg element

3.10.9
- remove obsolete bottom padding in form
- extend widget theme with missing text colors
- add file circle plus icon

3.10.8
- style payment summary view

3.10.7
- do not try to access captcha when hidden
- style fixed columns in table with border at the right edge
- add coloration for number of attempts

3.10.6
- only use captcha when updating password if the target is the current user or the current user is no usermanager
- add title to description element
- add acknowledge notification and entities message
- remove description title again
- handle custom title in action pre checks
- remove ignoreFormDefaultValues property

3.10.5
- disable CKEditor version check
- fix dark theme icon issues within buttons

3.10.4
- set min-height on lower div
- respect itemCount as limit for remote fields
- default disable remote create in widgets

3.10.3
- enable whole area of menu entries to be clicked
- fix dark theme color issues
- apply corresponding theme background color to login screen

3.10.2
- only use AutoSizer with new windowed table

3.10.1
- fix colors in signal list items for dark mode

3.10.0
- initial release for version 3.10

3.9.37
- add summary page

3.9.36
- style focused menu elements the same as on hover

3.9.35
- set referrer policy to `no-referrer-when-downgrade`
- add sticky column to ui table
- use other endpoint for delete button
- apply theme themeswitching to ace code editor as well

3.9.34
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin
- fix blocking display disappearing in widgets on long pages

3.9.33
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin

3.9.32
- check rights to remove delete button
- fix selection in remotefield
- fix selection in remotefield

3.9.31
- fix menu items wobble on hover in firefox browser
- fix dark theme color issues
- dynamic determine selectable in table

3.9.30
- change last opened color to increase readability

3.9.29
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- add listApp
- add listApp
- add listApp
- add listApp
- add listApp
- add externalEvents to appFactory
- use Debouncer in CodeEdit instead of custom debounce

3.9.28
- use correct search form name as fallback
- enable hover on parent items of menu tree

3.9.27
- map shrinkToContent in getColumnDefinition
- fix jumping buttons in tocco header when opening help menu
- add feature

3.9.26
- improve menu ux by adding hover background and expanding clickable area
- add dark mode
- fix widget theme

3.9.25
- fix intermediate values in time edit

3.9.24
- add no button option for simple action form

3.9.23
- map email fields to string tql

3.9.22
- add payment methods and payment provider handling

3.9.21
- add non breaking space to ensure correct spacing in multiple fields separator

3.9.20
- fix column order resetting when moving multiple columns after another

3.9.19
- add drag'n'drop upload

3.9.18
- support custom report settings in widgets
- harmonize exam edit spacing

3.9.17
- enhance last openend color for better visibility

3.9.16
- limit watchForHover to activate in window.onload

3.9.15
- Add `watchForHover` styling utility

3.9.14
- shrink size of datepicker

3.9.13
- handle 403 error in delete

3.9.12
- add formBase property

3.9.11
- fix width of exam edit table ipunt fields in firefox
- add last opened to ui table
- add last opened to entity-list
- align stated value content verically with label

3.9.10
- use path for setting placeholder terms value
- fix multi-select-field

3.9.9
- hide readonly terms
- fix locale for anonymous
- fix locale for anonymous

3.9.8
- harmonize date picker styling and implement time picker
- fix several selection issues in the dms
- fix selection in docs tree search

3.9.7
- load row number options from Page_limit entities
- fix multivalue separator span alignment

3.9.6
- add updateDocumentTitle prop to Breadcrumbs
- add updateDocumentTitle prop to Breadcrumbs

3.9.5
- register gear icon

3.9.4
- split styling of blocking display for admin and widget
- fix multivalue label text overflow

3.9.3
- add readonly terms config and use pristine value change for placeholders
- handle immutable prop in TermsEdit
- check for writable paths in form instead of actions
- only set form builder readonly if mode is not create

3.9.2
- split toaster into widget and admin components
- adjust height of evaluation tree table to scale depending on available viewport height

3.9.1
- TOCDEV-7717, TOCDEV-7983

3.9.0
- initial release for version 3.9

3.8.23
- move logic from reports saga to backend
- Show error message if too many entities are selected for deletion

3.8.22
- align widget labels at flex start position
- add hideFooter property to entity-detail
- register list-tree icon

3.8.21
- harmonize status label border radii
- add border around exam edit datepicker inputs

3.8.20
- load row number options from Page_limit entities
- fix resizing of ui table

3.8.19
- hide attempts on hidden checkbox
- add download icon to DocumentFormatter
- allow multiple paths and disallow navigation for custom table on SubGrid
- disable navigation if parent defines it
- do not add search value for parent relation if no reverse relation was defined
- handle multiple relation paths by resolving all of them and handle custom relation paths by load the table form

3.8.18
- fix terms edit link color
- navigate to next field without selecting with tab

3.8.17
- only use visible search fields for building tql
- escape front-slash in fulltext tql query

3.8.16
- vertically center input edit table cells
- force underscore styling of terms edit links

3.8.15
- hide overflow of column headers
- register gear icon
- disable sideEffects optimization in webpack
- fix css styles not being passed down to multi value label span
- fix sticky buttons within modal cutting off content
- fix terms edit link styling and refactor component

3.8.14
- register calculator icon

3.8.13
- fix themes by adding missing status label radius
- extract nested ternary operation
- improve exam edit action styling
- fix vertical alignment of html formatter

3.8.12
- add data-cy attributes

3.8.11
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add BasicSearchForm
- add qualification dataRequest
- make select work without formData
- refactor input-edit to use more shared code
- use column position hook
- add useTitle to FormattedValue
- count hidden required checkbox as optional and ignore it in validation
- fix checking if form contains only readonly
- add additional units

3.8.10
- refactor panel group to remove duplications

3.8.9
- fix long lines in code edit being cut off by showing scrollbar
- add id to ReactDatePicker
- add keyboard navigation, editable value focus and postPointDigits
- decouple docs-browser from entity-list
- add default link target and protocol to html editor

3.8.8
- fix disabled button and menu item styles
- fix broken prop types
- do not use array index in keys
- fix legacy styles being applied to status labels

3.8.7
- change fields separator
- change fields separator
- fix test for new separator
- make editableValueFactory work without formField
- create exam-edit
- add exam-edit action
- add exam-edit action

3.8.6
- load email template for non-admins as well
- remove obsolete padding around styled entity browser
- fix disabled textarea on safari

3.8.5
- fix menu menu links not opening in new tab when icon is clicked

3.8.4
- ignore default search filter if tql is passed

3.8.3
- if a form contains only readonly field the save button should be removed

3.8.2
- add icon to merge button

3.8.1
- use widget prefix for report location
- lighten status label colors and harmonize spacing

3.8.0
- initial release for version 3.8

0.1.156
- harmonize menu layout by moving technical name below label
- fix scrollbars on disabled textareas with content

0.1.155
- reset disabled min-height on textareas

0.1.154
- growing subtables
- load email template properly

0.1.153
- harmonize panel spacing and font weights
- make actionConditions work in the children of action groups
- handle follow up actions

0.1.152
- build up modifiers from smaller independent functions

0.1.151
- use new formSuffix to build formBase of SubGrids
- continue handling form elements after a custom component was encountered
- do not crash if no formValues exists when checking terms data
- pass formValues to formBuilder

0.1.150
- enable co-existence with legacy ckeditor
- improve readability of status labels
- adjust disabled textarea height to match vertical label position

0.1.149
- do not render terms if no conditions were configured
- check for empty string that element is only added if necessary

0.1.148
- fix getTextOfChildren
- replace deprecated selectUnit

0.1.147
- fix buggy scroll to top for modals
- remove delete action from all forms and add searchFormType
- handle phone validation for relations
- handle validation response properly
- fix broken prop types

0.1.146
- dependency update
- dependency update
- fullcalendar dependency update
- dependency update
- add data-cy attributes

0.1.145
- handle de-DE locale for datepicker
- align terms condition with checkbox properly

0.1.144
- increase left padding of merge table

0.1.143
- add min width to toaster box

0.1.142
- handle attempt and max attempts

0.1.141
- get all address suggestions within country
- render sidepanel initially with correct collapse state
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- add terms component
- add terms component
- add pickAllProperties to getFlattenEntity
- use new terms

0.1.140
- handle sso with redirects
- handle auto login properly
- connect principal with sso login properly
- handle sso login properly

0.1.139
- export logged in action type
- handle showAutentication flag

0.1.138
- add reports after new button
- place label in widgets on left side
- move question code to tocco-util

0.1.137
- flag selection as deleted on duplicate detail
- navigate to list after fullscreen action deleted selection

0.1.136
- show correct relation link for related entitites

0.1.135
- accept limit as string

0.1.134
- increase toaster width to prevent early line breaks within titles
- fix acl browser styling

0.1.133
- add icon for action group
- fix immutable label positioned inside
- fix log input height
- fix immutable height of integer edit by passing immutable prop

0.1.132
- remove list numbering
- clear cached view stores

0.1.131
- style edit text within html formatter
- render resource-rows in correct heights
- add formApp to actions
- reset value properly

0.1.130
- fix fullcalendar license

0.1.129
- fix docs browser label alignment
- set correct label position
- fix build for customer bundles

0.1.128
- adjust useSyncValidation
- fix action order
-  add data-cy attr to confirm buttons
- use comon text resources
- use text resources for extended search
- fix hostname field in log view
- load actual question instead of label of the question

0.1.127
- add data-cy attr to delete buttons
- add data-cy attr to submit button

0.1.126
- edit and delete entity template

0.1.125
- fix disabled select spacing
- adjust widget stated value label color
- add action form modifier fn
- add simple form helper functions
- add onFormValuesChange event
- action can ignore pending changes prompt

0.1.124
- fix choice edit label alignment
- fix light cellrenderer width

0.1.123
- add core sidepanel horizontal button comp
- use core button for search filters
- handle input default values exclusivly
- add save action with follow up that can start an action after save

0.1.122

0.1.121
- adjust google maps address link
- allow all html content
- align editor config with legacy editor
- add advanced color picker
- add paste as plaintext
- correct display of numbering
- add upload to editableMapping

0.1.120
- fix null values in percent field

0.1.119
- use new navigation cell to navigate in cypress test
- handle new flags object in action response
- allow hard reload after action and ignore invisible meta fields on soft reload

0.1.118
- add font awesome eye icon
- use eye icon for documents
- export StyledTableRow
- add row numbering and hover link that opens entity in new tab

0.1.117
- fix action button styling

0.1.116
- style promotion status badge
- adjust font factor of widget theme

0.1.115
- extend embed type
- add widget panel
- handle label position via prop
- handle label color for widget and admin differently
- handle different form components for widgets
- treat admin embedType logic as well for legacy-admin
- load formComponents on runtime
- add selectionDeleted for simple action

0.1.114
- add action button styling

0.1.113
- support hide and read-only for location/address

0.1.112
- copy only changesets function
- position toasters at bottom right to prevent interference with interaction elements
- remove x-backend-url header
- align labels in center of input box
- harmonize positioning of sticky buttons within modal

0.1.111
- hide rule checking for difference to old password if old password field is not shown
- copy changelog to clipboard
- add cypress data attribute to breadcrumb

0.1.110
- handle remote create on simpel actions
- handle entity model corretly to support remote create
- handle entity model corretly on form builder
- provide detail app to actions
- provide detail app to entity-list
- show advancedSearch properly

0.1.109
- fix country cache
- expand checkbox and label options for ChoiceEdit
- use new loadModules endpoint

0.1.108
- move field definition cache to object cache

0.1.107
- move country cache to object cache

0.1.106
- harmonize menutree spacing and implement default icon

0.1.105
- enable enter handling again

0.1.104
- Make sure that address field is visible also without value

0.1.103
- show multiline lables with correct line breaks
- run query automatically
- keep sorting preferences up to date
- Added new component to search for full address

0.1.102
- fix client questions with cancel
- do not save form on open dropdown

0.1.101
- show entity-docs in email attachements

0.1.100
- remove top and left padding within immutable stated value box
- fix alignment of input fields within input edit table
- show back action always before reports
- fix fulltext search

0.1.99
- harmonize sso login spacing and color
- also implement border radius for indicator container wrapper to prevent cover of input field edges
- fix subfolder search
- add cancel button to confirm modals
- show correct pending changes modal on widgets
- fix border radius calculation of panel headers

0.1.98
- fix baseline of code edit

0.1.97
- fix input width of login form

0.1.96
- copy documents and binary

0.1.95
- position input label at baseline of input

0.1.94
- replace ckeditor5 with ckeditor4
- fix PropTypes - date to string
- fix PropTypes - function to conditions list
- don't show prompt after entity creation
- rename wrapper
- do not infinite reload fullscreen action on reload

0.1.93
- add additional data-cy attributes

0.1.92
- fix height rendering of load mask and spacing of layout boxes within modal

0.1.91
- fix duplicated key on list

0.1.90
- remove duplicated enter handling

0.1.89
- remove minimum height for modal body which is not needed
- add error message if login failed
- remove sso error message
- stop action on failed sso login
- remove [object Object] button title
- ensure pending changes prompt is fired first

0.1.88
- fix missaligned dropdown indicators within select
- fix border radii of input edit select
- add fetchAllEntities which handles paging by itself
- replace fetchEntities
- replace fetchEntities in selection
- replace custom paging in notifications
- replace fetchEntities in formData

0.1.87
- improve readability of disabled multi value label text
- be able to tab through mulit-remote-selects
- navigate to next field with tab on select

0.1.86
- add modifyEntityPaths input parameter
- add modifyEntityPaths input parameter
- add initialAction
- remove history in action and use initialAction of entity-browser
- use initialAction of entity-browser

0.1.85
- add global classname for all wrapping elements

0.1.84
- use global scrollbar style instead of specific imports
- support column action in adjustActions
- support (dynamic) reports in evaluation-execution-participation-action

0.1.83
- fix miscellaneous ui bugs in widgets

0.1.82
- adjust input field z-index and harmonize padding within input edit
- harmonize modal spacing and prevent scrollbar with minimum height introduction

0.1.81
- add action conditions
- harmonize label spacing in choice edit
- refactor simple form so that submit via enter key works and prevent nested forms
- add outputgroup when multiple reports

0.1.80
- show reset sorting again
-
- do not pass meta fields to onSubmit

0.1.79
- fix report generation with empty model

0.1.78
- invoke action only once
- only update input when app is mounted
- restructure cache for widget use cases
- remove revision from object cache
- fix text resource in evaluation-execution-participation-action
- ignore meta fields on values
- add form to store and add form model name

0.1.77
- handle expanded range field via default value
- remove unused expanded property

0.1.76
- add X-Backend-Url to all requests
- adjust X-Backend-Url to all requests
- add share ansers functionality
- add finish evaluation
- add save on change
- add visbility status for evaluation-execution-participation-action

0.1.75
- do not show pending changes prompt twice

0.1.74
- show error message when deletion is not possible

0.1.73
- add error handling in evaluation-execution-participation-action
- add synchronous validation for pseudoSelect
- add error handling during loading
- add pseudo submitting which just blur form
- fix mandatory validation for pseudoSelect
- add back button
- add short link

0.1.72
- fix border radius of search boxes
- fix autofocus on search form
- fix select fields exceeding the new maximum limit and crashing

0.1.71
- fix phone edit
- fix widget code copy action
- fix datepicker input styling

0.1.70
- do not submit form on select item
- do not propagate and use default for enter key

0.1.69
- refactor errors in pseudo form fields
- refactor errors in event-registration-action
- allow actions to be hidden by backend

0.1.68
- Button as well as code are only shown when visibility-status selected
- removed filename

0.1.67
- add pseudo select ui component
- add pseudo select field
- add evaluation-execution-participation-action action
- add evaluation-execution-participation-action to entity-browser

0.1.66
- create job-offer-apply action

0.1.65
- upgrade to react v18

0.1.64
- allow customization of validation messages
- add checkbox pseudo field helper
- use checkbox pseudo field for terms to use custom validation message

0.1.63
- show correct border-radius on panel header
- enforce css selectors for textarea

0.1.62
- improve navigation on login workflow

0.1.61
- add events for authentication and register page
- fix light cellrenderer font sizing to prevent icon overflow within cells

0.1.60
- add geosearch field

0.1.59
- trigger visibility status change only for widget embed type
- add visibility status to password update

0.1.58
- add onSuccess to synchronous simple actions
- enable custom action event handlers in entity-detail and entity-list
- pass customActionEventHandlers to store instead of actionFactory

0.1.57
- fix init redux form
- style label based on publish status as in old client

0.1.56
- fix cancelling client questions

0.1.55
- when creating a new entity, trigger auto-complete for fields with default values

0.1.54
- fix header and content cell aligment of table
- show toaster with validation errors
- harmonize nav and search box styling

0.1.53
- show correct search icon

0.1.52
- fix bundling apps

0.1.51
- make yes button in YesNo client question work again
- re-init cache instead of clear cache
- fix caching and intl race condition

0.1.50
- navigate back to list when state is lost
- remove unnecessary usage of darken and lighten functions and use theme colors instead
- fix move action for multiple root folders
- limit login container width

0.1.49
- add option to disable onclick on breadcrumb
- disable clicking on search result in breadcrumb

0.1.48
- map function to props correctly
- remove light cellrenderer flex so that it does not cut off content in table
- enforce border of input fields within input edit table

0.1.47
- remove usage of shade color function and extend theme instead
- introduce radii

0.1.46
- tqlBuilder now builds multiple conditions connected by 'or' when passed localized paths
- search fields for a localized field now search in every available locale
- show validation errors on deletion
- accept enter two-year date without punctation

0.1.45
- use correct locale for two factor connector
- clean up shade color function use and switch to theme colors instead
- add on enter feature to advanced search
- add on enter feature to two factor connector action

0.1.44
- extend theme with more specific colors to better target elements
- adapt new specific theme colors to table, select and column picker
- handle sub module questions
- pass all query params to action
- simplify action url

0.1.43
- handle url search fields as strings
- be able to have uppercase urls

0.1.42
- standardize yes no question modal buttons

0.1.41
- fix theme selectors
- add agb and privacy protection fields

0.1.40
- add detail for event the user is registering too
- render fields reached through an empty but writable path
- improve global styles

0.1.39
- use correct fonts for choices
- add modules
- use correct locale for login comp
- harmonize scheduler navigation on smaller screen
- add visibility status and hide after register

0.1.38
- harmonize spacing of breadcrumbs

0.1.37
- pass widget key as query param so backend can read it in a standardized way
- add min/max selection option for multiple-choice
- support min and max selection in validation
- add login component

0.1.36
- add username form to login
- restructre login page input props
- fix error message alignment
- add the possibility to close some modals with enter key if button is primary
- add choice ui component
- add choice field
- add form pseudo field logic
- add additional condition to getUsedPaths

0.1.35
- create event-registration-action
- add event-registration-action

0.1.34
- standardize page overlay and holder components for modal and blocking info

0.1.33
- datepicker focus handling on ffox and date only inputs
- lower height threshold of button context

0.1.32
- show proper error msg for 413
- fix react attribute error

0.1.31
- fix default confirm button
- fix sidepanel height
- enable scrollBehaviour on detail
- extend theme
- add sticky action bar
- simplify dynamic action configuration

0.1.30
- add icons for user menu and business unit menu inside header

0.1.29
- display more custom icons in Breadcrumbs as defined by backend, specifically domains and folders in dms

0.1.28
- display percent fields correctly

0.1.27
- fix login page height not filling full page
- be able to embed bootstrap script relative

0.1.26
- add autoFocusOnMount prop to ace editor to set focus manually since the useAutoFocus hook is not working in this case
- replace range type icons with generic icon and harmonize alignment to improve UX
- fix modal position in widget context and use vh units
- add feature to save number of rows as user preference in entity list

0.1.25
- add visibility status code copy action
- rename onStateChange to onVisibilityStatusChange
- set correct user locale after login

0.1.24
- pass navigation strategy to actions
- fix table randomly jumping scroll positions when switching pages

0.1.23
- fix autofocus by wrapping selector in timeout

0.1.22
- add possibility to show reload prompt
- add prompt component
- show user confirmation on browser-routing
- show user confirmation without blur
- pass selection from url to fullscreen action

0.1.21
- add back button
- use core back button
- use core back button for cancel

0.1.20
- enhance external events
- add states (list, detail, fullscreen-action)
- add onStateChange event

0.1.19
- fix object cache

0.1.18
- re-load data on login
- add object cache
- use global object cache

0.1.17
- allow padding of Button to be removed like when used as icon
- allow Action to be rendered as text without button visual
- export action types to use same names everywhere
- confirm modals now use whichever default action the backend declares, which means switching the styling and keyboard shortcut

0.1.16
- prevent action buttons within table of being missaligned in widgets

0.1.15
- order range values on top instead of side by side to improve ux
- remove the split pane feature as it doesn not improve UX and wrap scheduler in generic sidepanel component
- create ENTITY_DOCS selection for legacy actions
- move submit button to parent
- add sync validation to template form
- disable submit button for invalid form

0.1.14
- support identifier type in tql builder
- enforce css selectors
- enforce css selectors
- fix null as end date

0.1.13
- fix save button being clipped and change hover on error
- fix react-select dropdown being clipped inside input-edit table
- link for max 100 created entities
- add docs-tree search
- add conditional selection for tables
- only select entities for docs tree search

0.1.12
- allow target attribute in html
- harmonize spacing of input edit action wrapper

0.1.11
- do not use default display for tooltips again

0.1.10
- remove div wrapper of entity-browser as it was preventing it from filling the full available height when embedded in the old client
- send selection to adjusted POST endpoint when loading report settings
- use a new custom display when displaying entities in select boxes

0.1.9
- harmonize spacing of ranged dates by increasing search panel width
- simplify widget code

0.1.8
- reintroduce min-width for Ball component, since normal width caused rendering issues in the main menu
- replace useWindowWidth hook with universal useWindowSize hook
- possible to define custom render for actions
- define back button through custom render

0.1.7
- do not collapse sidepanel on top

0.1.6
- show time in 24h format for all locales
- show time in 24h format for all locales

0.1.5
- move toasters on mobile to bottom for better ux as most of the interaction elements (e.g actions) are on top and should not be covered

0.1.4
- input edit info action for showing meta info
- add input-edit-info action
- do not reload fullscreen action on single action
- be able to click on row in action cell

0.1.3
- add collapsible sidepanel component
- possible to pass beforeRenderField func
- introduce legacy-admin embed type
- same sidepanel behaviour as in admin
- use reusable sidepanel component
- use simple_advanced search form
- autofocus form fields
- prepare sso-login widget for widget embedType

0.1.2
- remove tocco subdomain

0.1.1
- use Icon component in LoadingSpinner
- replace values of readonly input fields with loading spinner while the update request is processing

0.1.0
- Initial release
