import showcaseReducer from './showcase/reducer'
import showcaseSagas from './showcase/sagas'
import showcaseActionsSagas from './showcaseActions/sagas'
import widgetConfigReducer from './widgetConfig/reducer'
import widgetConfigSagas from './widgetConfig/sagas'

export default {
  showcase: showcaseReducer,
  widgetConfig: widgetConfigReducer
}

export const sagas = [showcaseSagas, showcaseActionsSagas, widgetConfigSagas]
