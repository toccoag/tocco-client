import {all, put, select, takeLatest} from 'redux-saga/effects'
import {notification} from 'tocco-app-extensions'

import ThemeEditAction from '../../components/Showcase/Actions/theme-edit'
import WidgetConfigEditAction from '../../components/Showcase/Actions/widget-config-edit'
import WidgetInputViewAction from '../../components/Showcase/Actions/widget-input-view'

import {EDIT_THEME, EDIT_WIDGET_CONFIG, VIEW_WIDGET_INPUT} from './actions'

export const textResourceSelector = state => state.intl.messages

export default function* sagas() {
  yield all([
    takeLatest(EDIT_WIDGET_CONFIG, editWidgetConfig),
    takeLatest(EDIT_THEME, editTheme),
    takeLatest(VIEW_WIDGET_INPUT, viewIput)
  ])
}

export function* editWidgetConfig() {
  const textResources = yield select(textResourceSelector)
  yield put(
    notification.modal(
      'widget-config-edit',
      textResources['client.widget-showcase.widgetConfiguration'],
      null,
      WidgetConfigEditAction,
      true
    )
  )
}

export function* editTheme() {
  const textResources = yield select(textResourceSelector)
  yield put(
    notification.modal('theme-edit', textResources['client.widget-showcase.theme'], null, ThemeEditAction, true)
  )
}

export function* viewIput({payload: {input}}) {
  const textResources = yield select(textResourceSelector)
  const Action = props => <WidgetInputViewAction input={input} {...props} />
  yield put(
    notification.modal('widget-input-view', textResources['client.widget-showcase.widgetInput'], null, Action, true)
  )
}
