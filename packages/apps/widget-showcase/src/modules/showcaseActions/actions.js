export const EDIT_WIDGET_CONFIG = 'showcaseActions/EDIT_WIDGET_CONFIG'
export const EDIT_THEME = 'showcaseActions/EDIT_THEME'
export const VIEW_WIDGET_INPUT = 'showcaseActions/VIEW_WIDGET_INPUT'

export const editWidgetConfig = () => ({
  type: EDIT_WIDGET_CONFIG
})

export const editTheme = () => ({
  type: EDIT_THEME
})

export const viewWidgetInput = input => ({
  type: VIEW_WIDGET_INPUT,
  payload: {input}
})
