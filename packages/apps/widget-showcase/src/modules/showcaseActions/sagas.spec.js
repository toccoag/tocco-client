import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('widget-showcase', () => {
  describe('modules', () => {
    describe('showcaseActions', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.EDIT_WIDGET_CONFIG, sagas.editWidgetConfig),
                takeLatest(actions.EDIT_THEME, sagas.editTheme),
                takeLatest(actions.VIEW_WIDGET_INPUT, sagas.viewIput)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('editWidgetConfig', () => {
          test('should show Widget Config Edit modal', () => {
            return expectSaga(sagas.editWidgetConfig)
              .provide([
                [
                  matchers.select(sagas.textResourceSelector),
                  {'client.widget-showcase.widgetConfiguration': 'Widget-Konfiguration'}
                ]
              ])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'widget-config-edit',
                    title: 'Widget-Konfiguration',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .run()
          })
        })

        describe('editTheme', () => {
          test('should show Theme Edit modal', () => {
            return expectSaga(sagas.editTheme)
              .provide([[matchers.select(sagas.textResourceSelector), {'client.widget-showcase.theme': 'Theme'}]])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'theme-edit',
                    title: 'Theme',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .run()
          })
        })

        describe('viewInput', () => {
          test('should show Input modal', () => {
            return expectSaga(sagas.viewIput, actions.viewWidgetInput({}))
              .provide([[matchers.select(sagas.textResourceSelector), {'client.widget-showcase.widgetInput': 'Input'}]])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'widget-input-view',
                    title: 'Input',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .run()
          })
        })
      })
    })
  })
})
