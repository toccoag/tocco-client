import {WidgetTheme} from 'tocco-theme'
import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const setAccess = (state, {payload: {isDevelopmentRunEnv, isWidgetManager}}) => ({
  ...state,
  access: {
    isDevelopmentRunEnv,
    isWidgetManager
  }
})

const ACTION_HANDLERS = {
  [actions.SET_WIDGETS]: reducerUtil.singleTransferReducer('widgets'),
  [actions.SET_SELECTED_WIDGET]: reducerUtil.singleTransferReducer('selectedWidget'),
  [actions.SET_WIDGET_CONFIGS]: reducerUtil.singleTransferReducer('widgetConfigs'),
  [actions.SET_SELECTED_WIDGET_CONFIG]: reducerUtil.singleTransferReducer('selectedWidgetConfig'),
  [actions.SET_WIDGET_CONFIG]: reducerUtil.singleTransferReducer('widgetConfig'),
  [actions.SET_THEME]: reducerUtil.singleTransferReducer('theme'),
  [actions.SET_ACCESS]: setAccess,
  [actions.SET_INITIAL_MODE]: reducerUtil.singleTransferReducer('mode'),
  [actions.SET_MODE]: reducerUtil.singleTransferReducer('mode')
}

const initialState = {
  widgets: [],
  selectedWidget: null,
  widgetConfigs: [],
  selectedWidgetConfig: null,
  widgetConfig: null,
  theme: WidgetTheme,
  mode: 'simple',
  access: {
    isDevelopmentRunEnv: undefined,
    isWidgetManager: undefined
  }
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
