import * as actions from './actions'
import reducer from './reducer'

describe('widget-showcase', () => {
  describe('modules', () => {
    describe('showcase', () => {
      describe('reducer', () => {
        describe('SET_ACCESS', () => {
          test('should handle SET_ACCESS action', () => {
            const stateBefore = {
              widgets: [],
              selectedWidget: null,
              access: {
                isDevelopmentRunEnv: undefined,
                isWidgetManager: undefined
              }
            }
            const expectedStateAfter = {
              widgets: [],
              selectedWidget: null,
              access: {
                isDevelopmentRunEnv: true,
                isWidgetManager: false
              }
            }

            expect(reducer(stateBefore, actions.setAccess(true, false))).to.deep.equal(expectedStateAfter)
          })
        })
      })
    })
  })
})
