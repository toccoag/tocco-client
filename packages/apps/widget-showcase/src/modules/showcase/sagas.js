import {all, call, put, takeLatest} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import * as modeUtils from '../../utils/mode'
import {setSpecificConfigEntityId} from '../widgetConfig/actions'

import {
  INIT,
  CHECK_ACCESS,
  LOAD_WIDGETS,
  LOAD_WIDGET_CONFIG,
  LOAD_WIDGET_CONFIGS,
  SET_SELECTED_WIDGET,
  SET_SELECTED_WIDGET_CONFIG,
  SET_MODE,
  setAccess,
  setWidgetConfig,
  setWidgetConfigs,
  setWidgets
} from './actions'
import * as actions from './actions'

export default function* sagas() {
  yield all([
    takeLatest(INIT, init),
    takeLatest(SET_SELECTED_WIDGET, setSelectedWidget),
    takeLatest(SET_SELECTED_WIDGET_CONFIG, setSelectedWidgetConfig),
    takeLatest(LOAD_WIDGETS, loadWidgets),
    takeLatest(LOAD_WIDGET_CONFIGS, loadWidgetConfigs),
    takeLatest(LOAD_WIDGET_CONFIG, loadWidgetConfig),
    takeLatest(CHECK_ACCESS, checkAccess),
    takeLatest(SET_MODE, persistMode)
  ])
}

export function* setSelectedWidget({payload: {selectedWidget}}) {
  yield put(actions.setSelectedWidgetConfig(null))
  yield put(setSpecificConfigEntityId(null))
  yield put(setWidgetConfig(null))
  yield put(actions.loadWidgetConfigs(selectedWidget))
}

export function* setSelectedWidgetConfig({payload: {selectedWidgetConfig}}) {
  yield put(setWidgetConfig(null))
  if (selectedWidgetConfig) {
    yield put(actions.loadWidgetConfig(selectedWidgetConfig.uniqueId))
  }
}

export function* loadWidgets() {
  const resource = 'showcase/widgets/'
  const options = {
    method: 'GET'
  }

  const response = yield call(rest.requestSaga, resource, options)
  yield put(setWidgets(response.body.widgets))
}

export function* loadWidgetConfigs({payload: {widget}}) {
  const resource = `showcase/widgets/${widget.key}`
  const options = {
    method: 'GET'
  }

  const response = yield call(rest.requestSaga, resource, options)
  yield put(setWidgetConfigs(response.body.configs))
}

export function* loadWidgetConfig({payload: {widgetConfigUniqueId}}) {
  const resource = `widget/configs/${widgetConfigUniqueId}`
  const options = {
    method: 'GET',
    queryParams: {
      _showcase: true
    }
  }

  const response = yield call(rest.requestSaga, resource, options)
  yield put(setWidgetConfig(response.body))
}

export function* checkAccess() {
  const resource = 'showcase/checkAccess'
  const options = {
    method: 'GET'
  }

  const response = yield call(rest.requestSaga, resource, options)
  yield put(setAccess(response.body.isDevelopmentRunEnv, response.body.isWidgetManager))
}

export function* init() {
  yield put(actions.checkAccess())

  const mode = yield call(modeUtils.getPersistedMode)
  if (mode) {
    yield put(actions.setInitialMode(mode))
  }
}

export function* persistMode({payload: {mode}}) {
  yield call(modeUtils.persistMode, mode)
}
