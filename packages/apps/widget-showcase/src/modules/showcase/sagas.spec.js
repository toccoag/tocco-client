import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import {getPersistedMode} from '../../utils/mode'
import * as widgetConfigActions from '../widgetConfig/actions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('widget-showcase', () => {
  describe('modules', () => {
    describe('showcase', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.INIT, sagas.init),
                takeLatest(actions.SET_SELECTED_WIDGET, sagas.setSelectedWidget),
                takeLatest(actions.SET_SELECTED_WIDGET_CONFIG, sagas.setSelectedWidgetConfig),
                takeLatest(actions.LOAD_WIDGETS, sagas.loadWidgets),
                takeLatest(actions.LOAD_WIDGET_CONFIGS, sagas.loadWidgetConfigs),
                takeLatest(actions.LOAD_WIDGET_CONFIG, sagas.loadWidgetConfig),
                takeLatest(actions.CHECK_ACCESS, sagas.checkAccess),
                takeLatest(actions.SET_MODE, sagas.persistMode)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('setSelectedWidget saga', () => {
          test('should load widget configs for selected widget and reset dependent state', () => {
            const selectedWidget = {key: '1'}

            return expectSaga(sagas.setSelectedWidget, actions.setSelectedWidget(selectedWidget))
              .put(actions.setSelectedWidgetConfig(null))
              .put(widgetConfigActions.setSpecificConfigEntityId(null))
              .put(actions.setWidgetConfig(null))
              .put(actions.loadWidgetConfigs(selectedWidget))
              .run()
          })
        })

        describe('setSelectedWidgetConfig saga', () => {
          test('should load widget config for selected widget config', () => {
            const selectedWidgetConfig = {key: '1', uniqueId: 'widget'}

            return expectSaga(sagas.setSelectedWidgetConfig, actions.setSelectedWidgetConfig(selectedWidgetConfig))
              .put(actions.setWidgetConfig(null))
              .put(actions.loadWidgetConfig('widget'))
              .run()
          })

          test('should reset state if widget config is reset', () => {
            const selectedWidgetConfig = null

            return expectSaga(sagas.setSelectedWidgetConfig, actions.setSelectedWidgetConfig(selectedWidgetConfig))
              .put(actions.setWidgetConfig(null))
              .not.put(actions.loadWidgetConfig(selectedWidgetConfig))
              .run()
          })
        })

        describe('loadWidgets saga', () => {
          test('should call load widgets', () => {
            const widgets = []

            return expectSaga(sagas.loadWidgets)
              .provide([[matchers.call.fn(rest.requestSaga, 'showcase/widgets/'), {body: {widgets}}]])
              .put(actions.setWidgets(widgets))
              .run()
          })
        })

        describe('loadWidgetConfigs saga', () => {
          test('should call load widget configs', () => {
            const configs = []

            return expectSaga(sagas.loadWidgetConfigs, {payload: {widget: {key: '1'}}})
              .provide([[matchers.call.fn(rest.requestSaga, 'showcase/widgets/1'), {body: {configs}}]])
              .put(actions.setWidgetConfigs(configs))
              .run()
          })
        })

        describe('loadWidgetConfig saga', () => {
          test('should call load widget config', () => {
            const config = {}
            const options = {
              method: 'GET',
              queryParams: {
                _showcase: true
              }
            }

            return expectSaga(sagas.loadWidgetConfig, {payload: {widgetConfigUniqueId: '1'}})
              .provide([[matchers.call.fn(rest.requestSaga, 'widget/configs/1', options), {body: config}]])
              .put(actions.setWidgetConfig(config))
              .run()
          })
        })

        describe('checkAccess saga', () => {
          test('should call checkAccess', () => {
            const isDevelopmentRunEnv = true
            const isWidgetManager = false
            const body = {
              isDevelopmentRunEnv,
              isWidgetManager
            }

            return expectSaga(sagas.checkAccess)
              .provide([[matchers.call.fn(rest.requestSaga, 'showcase/widgets/checkAccess'), {body}]])
              .put(actions.setAccess(isDevelopmentRunEnv, isWidgetManager))
              .run()
          })
        })

        describe('init saga', () => {
          test('should check access and init mode', () => {
            return expectSaga(sagas.init)
              .provide([[matchers.call.fn(getPersistedMode), 'advanced']])
              .put(actions.checkAccess())
              .put(actions.setInitialMode('advanced'))
              .run()
          })

          test('should only init mode if persisted mode is available', () => {
            return expectSaga(sagas.init)
              .provide([[matchers.call.fn(getPersistedMode), undefined]])
              .put(actions.checkAccess())
              .not.put(actions.setInitialMode('advanced'))
              .run()
          })
        })
      })
    })
  })
})
