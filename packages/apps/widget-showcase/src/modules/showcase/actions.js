import {WidgetTheme} from 'tocco-theme'

export const INIT = 'showcase/INIT'
export const SET_WIDGETS = 'showcase/SET_WIDGETS'
export const LOAD_WIDGETS = 'showcase/LOAD_WIDGETS'
export const SET_SELECTED_WIDGET = 'showcase/SET_SELECTED_WIDGET'
export const SET_WIDGET_CONFIGS = 'showcase/SET_WIDGET_CONFIGS'
export const LOAD_WIDGET_CONFIGS = 'showcase/LOAD_WIDGET_CONFIGS'
export const SET_SELECTED_WIDGET_CONFIG = 'showcase/SET_SELECTED_WIDGET_CONFIG'
export const SET_WIDGET_CONFIG = 'showcase/SET_WIDGET_CONFIG'
export const LOAD_WIDGET_CONFIG = 'showcase/LOAD_WIDGET_CONFIG'
export const SET_THEME = 'showcase/SET_THEME'
export const RESET_THEME = 'showcase/RESET_THEME'
export const CHECK_ACCESS = 'showcase/CHECK_ACCESS'
export const SET_ACCESS = 'showcase/SET_ACCESS'
export const SET_MODE = 'showcase/SET_MODE'
export const SET_INITIAL_MODE = 'showcase/SET_INITIAL_MODE'

export const init = () => ({
  type: INIT
})

export const setWidgets = widgets => ({
  type: SET_WIDGETS,
  payload: {widgets}
})

export const loadWidgets = () => ({
  type: LOAD_WIDGETS
})

export const setSelectedWidget = selectedWidget => ({
  type: SET_SELECTED_WIDGET,
  payload: {selectedWidget}
})

export const setWidgetConfigs = widgetConfigs => ({
  type: SET_WIDGET_CONFIGS,
  payload: {widgetConfigs}
})

export const loadWidgetConfigs = widget => ({
  type: LOAD_WIDGET_CONFIGS,
  payload: {widget}
})

export const setSelectedWidgetConfig = selectedWidgetConfig => ({
  type: SET_SELECTED_WIDGET_CONFIG,
  payload: {selectedWidgetConfig}
})

export const setWidgetConfig = widgetConfig => ({
  type: SET_WIDGET_CONFIG,
  payload: {widgetConfig}
})

export const loadWidgetConfig = widgetConfigUniqueId => ({
  type: LOAD_WIDGET_CONFIG,
  payload: {widgetConfigUniqueId}
})

export const setTheme = theme => ({
  type: SET_THEME,
  payload: {theme}
})

export const resetTheme = () => ({
  type: SET_THEME,
  payload: {theme: WidgetTheme}
})

export const checkAccess = () => ({
  type: CHECK_ACCESS
})

export const setAccess = (isDevelopmentRunEnv, isWidgetManager) => ({
  type: SET_ACCESS,
  payload: {
    isDevelopmentRunEnv,
    isWidgetManager
  }
})

export const setMode = mode => ({
  type: SET_MODE,
  payload: {mode}
})

export const setInitialMode = mode => ({
  type: SET_INITIAL_MODE,
  payload: {mode}
})
