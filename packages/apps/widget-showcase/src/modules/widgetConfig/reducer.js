import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_SPECIFIC_CONFIG_ENTITY_ID]: reducerUtil.singleTransferReducer('specificConfigEntityId')
}

const initialState = {
  specificConfigEntityId: null
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
