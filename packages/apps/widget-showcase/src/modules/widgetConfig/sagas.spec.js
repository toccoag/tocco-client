import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('widget-showcase', () => {
  describe('modules', () => {
    describe('widgetConfig', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeLatest(actions.FETCH_SPECIFIC_CONFIG_ENTITY_ID, sagas.fetchSpecificConfigEntityId)])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('fetchSpecificConfigEntityId saga', () => {
          test('should call load specific config', () => {
            const config = {
              entityName: 'Password_update_widget_config',
              key: '1'
            }

            return expectSaga(sagas.fetchSpecificConfigEntityId, {payload: {widgetConfigKey: 1}})
              .provide([[matchers.call.fn(rest.requestSaga, '/widget/configs/1/specific-config'), {body: config}]])
              .put(actions.setSpecificConfigEntityId(config))
              .run()
          })
        })
      })
    })
  })
})
