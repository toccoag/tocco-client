export const FETCH_SPECIFIC_CONFIG_ENTITY_ID = 'widgetConfig/FETCH_SPECIFIC_CONFIG_ENTITY_ID'
export const SET_SPECIFIC_CONFIG_ENTITY_ID = 'widgetConfig/SET_SPECIFIC_CONFIG_ENTITY_ID'

export const fetchSpecificConfigEntityId = widgetConfigKey => ({
  type: FETCH_SPECIFIC_CONFIG_ENTITY_ID,
  payload: {widgetConfigKey}
})

export const setSpecificConfigEntityId = specificConfigEntityId => ({
  type: SET_SPECIFIC_CONFIG_ENTITY_ID,
  payload: {
    specificConfigEntityId
  }
})
