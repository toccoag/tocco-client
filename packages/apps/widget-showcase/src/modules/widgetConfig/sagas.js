import {all, call, put, takeLatest} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import {FETCH_SPECIFIC_CONFIG_ENTITY_ID, setSpecificConfigEntityId} from './actions'

export default function* sagas() {
  yield all([takeLatest(FETCH_SPECIFIC_CONFIG_ENTITY_ID, fetchSpecificConfigEntityId)])
}

export function* fetchSpecificConfigEntityId({payload: {widgetConfigKey}}) {
  const response = yield call(rest.requestSaga, `/widget/configs/${widgetConfigKey}/specific-config`)
  yield put(setSpecificConfigEntityId(response.body))
}
