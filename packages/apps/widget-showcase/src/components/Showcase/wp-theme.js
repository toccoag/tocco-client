export default {
  name: 'wk-widget',
  colors: {
    primary: '#0057ad',
    backgroundPanelHeaderFooter: '#ededed'
  },
  radii: {
    button: '16px',
    buttonLarge: '24px',
    form: '8px',
    dashboard: 0,
    modal: '8px',
    menu: '16px'
  }
}
