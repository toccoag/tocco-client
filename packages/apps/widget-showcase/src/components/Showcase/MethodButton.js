import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, EditableValue, StatedValue, Typography} from 'tocco-ui'

const MethodButton = ({label, onClick, intl}) => {
  const msg = id => intl.formatMessage({id})

  const [args, setArgs] = useState('[]')
  return (
    <>
      <Typography.H3>{label}</Typography.H3>
      <StatedValue label={msg('client.widget-showcase.methodArguments')}>
        <EditableValue
          type="code"
          value={args}
          events={{onChange: setArgs}}
          options={{autoFocusOnMount: false, mode: 'json'}}
        />
      </StatedValue>
      <Button type="button" onClick={() => onClick(...JSON.parse(args))} look="raised">
        <FormattedMessage id="client.widget-showcase.execute" />
      </Button>
    </>
  )
}

MethodButton.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  intl: PropTypes.object.isRequired
}

export default MethodButton
