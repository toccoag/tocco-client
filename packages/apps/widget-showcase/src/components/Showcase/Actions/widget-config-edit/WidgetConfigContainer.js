import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actionEmitter} from 'tocco-app-extensions'

import {loadWidgetConfig} from '../../../../modules/showcase/actions'
import {fetchSpecificConfigEntityId} from '../../../../modules/widgetConfig/actions'

import WidgetConfig from './WidgetConfig'

const mapActionCreators = {
  fetchSpecificConfigEntityId,
  loadWidgetConfig,
  dispatchEmittedAction: actionEmitter.dispatchEmittedAction
}

const mapStateToProps = state => ({
  widgetConfigKey: state.showcase.selectedWidgetConfig.key,
  widgetConfigUniqueId: state.showcase.selectedWidgetConfig.uniqueId,
  specificConfigEntityId: state.widgetConfig.specificConfigEntityId
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(WidgetConfig))
