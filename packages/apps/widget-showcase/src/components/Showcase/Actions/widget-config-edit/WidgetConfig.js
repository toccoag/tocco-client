import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import DocsBrowserApp from 'tocco-docs-browser/src/main'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import {Typography} from 'tocco-ui'

const WidgetConfig = ({
  fetchSpecificConfigEntityId,
  loadWidgetConfig,
  widgetConfigKey,
  widgetConfigUniqueId,
  specificConfigEntityId,
  dispatchEmittedAction
}) => {
  useEffect(() => {
    if (widgetConfigKey) {
      fetchSpecificConfigEntityId(widgetConfigKey)
    }
  }, [fetchSpecificConfigEntityId, widgetConfigKey])

  const handleWidgetConfigUpdated = () => {
    loadWidgetConfig(widgetConfigUniqueId)
  }

  if (!specificConfigEntityId) {
    return null
  }

  if (!specificConfigEntityId.entityName) {
    return (
      <Typography.P>
        <FormattedMessage id="client.widget-showcase.widgetConfiguration.noConfig" />
      </Typography.P>
    )
  }

  return (
    <EntityDetailApp
      entityName={specificConfigEntityId.entityName}
      formName={specificConfigEntityId.entityName}
      entityId={specificConfigEntityId.key}
      mode="update"
      onEntityUpdated={handleWidgetConfigUpdated}
      labelPosition="inside"
      docsApp={DocsBrowserApp}
      emitAction={action => {
        dispatchEmittedAction(action)
      }}
    />
  )
}

WidgetConfig.propTypes = {
  widgetConfigKey: PropTypes.string,
  widgetConfigUniqueId: PropTypes.string,
  specificConfigEntityId: PropTypes.object,
  fetchSpecificConfigEntityId: PropTypes.func,
  loadWidgetConfig: PropTypes.func,
  dispatchEmittedAction: PropTypes.func.isRequired
}

export default WidgetConfig
