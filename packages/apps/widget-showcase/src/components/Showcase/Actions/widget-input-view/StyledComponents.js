import styled from 'styled-components'
import {StyledCode, scale} from 'tocco-ui'

export const StyledInputCode = styled(StyledCode)`
  && {
    padding: ${scale.space(0.3)};
  }
`
