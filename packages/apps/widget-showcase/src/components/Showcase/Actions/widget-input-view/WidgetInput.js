import _omit from 'lodash/omit'
import PropTypes from 'prop-types'

import {StyledInputCode} from './StyledComponents'

const WidgetInput = ({input}) => {
  const actualInput = _omit(input, ['customTheme'])
  return <StyledInputCode>{JSON.stringify(actualInput, null, 4)}</StyledInputCode>
}

WidgetInput.propTypes = {
  input: PropTypes.object
}

export default WidgetInput
