import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {WidgetTheme} from 'tocco-theme'
import {Button, EditableValue, Panel, StatedValue, Typography, StyledStickyButtonWrapper} from 'tocco-ui'

const Theme = ({intl, theme, setTheme, close}) => {
  const [tmpTheme, setTmpTheme] = useState(JSON.stringify(theme, null, 4))
  const msg = id => intl.formatMessage({id})

  return (
    <div>
      <Panel.Wrapper>
        <Panel.Header>
          <Typography.H4>
            <FormattedMessage id="client.widget-showcase.theme" />
          </Typography.H4>
        </Panel.Header>
        <Panel.Body>
          <StatedValue label={msg('client.widget-showcase.theme')} labelPosition="inside">
            <EditableValue
              type="code"
              value={tmpTheme}
              events={{onChange: setTmpTheme}}
              options={{autoFocusOnMount: false, mode: 'json'}}
            />
          </StatedValue>
        </Panel.Body>
      </Panel.Wrapper>

      <StyledStickyButtonWrapper>
        <Button
          onClick={() => {
            setTmpTheme(JSON.stringify(WidgetTheme, null, 4))
          }}
        >
          <FormattedMessage id="client.widget-showcase.theme.reset" />
        </Button>
        <Button
          ink="primary"
          look="raised"
          onClick={() => {
            setTheme(JSON.parse(tmpTheme))
            close()
          }}
        >
          <FormattedMessage id="client.common.ok" />
        </Button>
      </StyledStickyButtonWrapper>
    </div>
  )
}

Theme.propTypes = {
  intl: PropTypes.object,
  theme: PropTypes.object,
  setTheme: PropTypes.func,
  close: PropTypes.func
}

export default Theme
