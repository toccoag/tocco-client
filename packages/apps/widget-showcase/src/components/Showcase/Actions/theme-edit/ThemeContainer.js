import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setTheme} from '../../../../modules/showcase/actions'

import Theme from './Theme'

const mapActionCreators = {
  setTheme
}

const mapStateToProps = state => ({
  theme: state.showcase.theme
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Theme))
