import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import {injectIntl} from 'react-intl'
import {Button} from 'tocco-ui'

import {StyledFullscreenToolboxContainer, StyledFullscreenWidgetContainer} from './StyledComponents'

const Fullscreen = ({children, onExit, fullscreen = false, toolbox, intl}) => {
  if (!fullscreen) {
    return <>{children}</>
  }

  return ReactDOM.createPortal(
    <StyledFullscreenWidgetContainer>
      <StyledFullscreenToolboxContainer>
        <Button
          type="button"
          onClick={() => onExit()}
          look="raised"
          icon="arrows-minimize"
          label={intl.formatMessage({id: 'client.widget-showcase.exitFullscreen'})}
        />

        {toolbox}
      </StyledFullscreenToolboxContainer>
      {children}
    </StyledFullscreenWidgetContainer>,
    document.body
  )
}

Fullscreen.propTypes = {
  intl: PropTypes.object.isRequired,
  children: PropTypes.any,
  fullscreen: PropTypes.bool,
  onExit: PropTypes.func,
  toolbox: PropTypes.object
}

export default injectIntl(Fullscreen)
