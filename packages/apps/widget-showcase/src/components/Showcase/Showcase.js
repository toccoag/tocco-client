import _merge from 'lodash/merge'
import PropTypes from 'prop-types'
import {Fragment, useCallback, useEffect, useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, EditableValue, StatedValue, Typography} from 'tocco-ui'
import {v4 as uuid} from 'uuid'

import {renderWidget, unmountWidget, getWidgetContainerElement} from '../../utils/bootstrap'
import {ID_WIDGET_CONTAINER} from '../../utils/constants'
import {isAdvanced} from '../../utils/mode'

import Fullscreen from './Fullscreen'
import MethodButton from './MethodButton'
import {
  Container,
  StyledEmptyInput,
  StyledSection,
  StyledToolboxContainer,
  StyledVisibilityStatus,
  StyledWidgetContainer
} from './StyledComponents'
import wpTheme from './wp-theme'

const Showcase = ({
  selectedWidget,
  setSelectedWidget,
  selectedWidgetConfig,
  setSelectedWidgetConfig,
  loadWidgets,
  widgets,
  widgetConfigs,
  widgetConfig,
  editWidgetConfig,
  theme,
  editTheme,
  setTheme,
  resetTheme,
  viewWidgetInput,
  isWidgetManager,
  mode,
  intl
}) => {
  const msg = id => intl.formatMessage({id})

  useEffect(() => {
    loadWidgets()
  }, [loadWidgets])

  const widgetSelectOptions = widgets.map(val => ({key: val.key, display: val.label}))
  const widgetConfigSelectOptions = widgetConfigs.map(val => ({
    key: val.key,
    uniqueId: val.uniqueId,
    display: `${val.uniqueId} - ${val.label}`
  }))

  const [visibilityStatus, setVisibilityStatus] = useState([])
  const [eventLog, setEventLog] = useState([])
  const [methods, setMethods] = useState(null)
  const [input, setInput] = useState(null)
  const [showFullscreen, setShowFullscreen] = useState(false)
  const addEventLogEntry = (event, args) => setEventLog(log => [...log, {key: uuid(), name: event, args}])

  const widgetConfigUniqueId = selectedWidgetConfig?.uniqueId
  const handleRender = useCallback(async () => {
    const container = getWidgetContainerElement()
    const customTheme = theme
    const events = {
      onVisibilityStatusChange: (...args) => {
        const [{status}] = args
        setVisibilityStatus(status)
        addEventLogEntry('onVisibilityStatusChange', args)
      }
    }
    const response = await renderWidget(container, widgetConfigUniqueId, widgetConfig, customTheme, events)
    const {input: widgetInput, methods: widgetMethods} = response
    setMethods(widgetMethods)
    setInput(widgetInput)
  }, [theme, widgetConfigUniqueId, widgetConfig])

  const handleUnmounWidget = useCallback(async () => {
    // clear hash location on config change
    history.pushState('', document.title, window.location.pathname + window.location.search)
    setVisibilityStatus([])
    setEventLog([])
    setMethods(null)
    setInput(null)
    await unmountWidget()
  }, [])

  const handleRerender = useCallback(async () => {
    await handleUnmounWidget()
    handleRender()
  }, [handleRender, handleUnmounWidget])

  const handleApplyWordPressTheme = () => {
    setTheme(_merge({}, theme, wpTheme))
  }

  const isWpThemeApplied = theme.name === wpTheme.name

  useEffect(() => {
    if (widgetConfig) {
      handleRerender()
    } else {
      handleUnmounWidget()
    }
  }, [widgetConfig, showFullscreen, handleUnmounWidget, handleRerender])

  const widgetToolbox = (
    <>
      {visibilityStatus && visibilityStatus.length > 0 && (
        <StyledVisibilityStatus>
          <FormattedMessage id="client.widget-showcase.visibilityStatus" />: {visibilityStatus.join(',')}
        </StyledVisibilityStatus>
      )}
    </>
  )

  return (
    <Container>
      <StatedValue label={msg('client.widget-showcase.widget')}>
        <EditableValue
          type="single-select"
          value={selectedWidget}
          options={{options: widgetSelectOptions}}
          events={{onChange: setSelectedWidget}}
        />
      </StatedValue>
      <StatedValue label={msg('client.widget-showcase.widgetConfiguration')}>
        {selectedWidget ? (
          <EditableValue
            type="single-select"
            value={selectedWidgetConfig}
            options={{options: widgetConfigSelectOptions}}
            events={{onChange: setSelectedWidgetConfig}}
          />
        ) : (
          <StyledEmptyInput>
            <FormattedMessage id="client.widget-showcase.selectWidget" />
          </StyledEmptyInput>
        )}
      </StatedValue>

      {selectedWidgetConfig && (
        <>
          <StyledSection>
            <Typography.H2>
              <FormattedMessage id="client.widget-showcase.settings" />
            </Typography.H2>
            {isWidgetManager && (
              <Button
                type="button"
                onClick={editWidgetConfig}
                ink="primary"
                look="raised"
                icon="gear"
                label={msg('client.widget-showcase.widgetConfiguration.edit')}
              />
            )}

            {isWpThemeApplied && (
              <Button
                type="button"
                onClick={() => resetTheme()}
                look="raised"
                label={msg('client.widget-showcase.theme.resetWpTheme')}
              />
            )}
            {!isWpThemeApplied && (
              <Button
                type="button"
                onClick={handleApplyWordPressTheme}
                look="raised"
                label={msg('client.widget-showcase.theme.applyWpTheme')}
              />
            )}

            {isAdvanced(mode) && (
              <Button
                type="button"
                onClick={editTheme}
                look="raised"
                icon="pen"
                label={msg('client.widget-showcase.theme.edit')}
              />
            )}
            {isAdvanced(mode) && (
              <Button
                type="button"
                icon="info"
                look="raised"
                onClick={() => viewWidgetInput(input)}
                label={msg('client.widget-showcase.widgetInput.view')}
              />
            )}
          </StyledSection>
          <StyledSection>
            <Typography.H2>
              <FormattedMessage id="client.widget-showcase.widget" />: {selectedWidgetConfig.display}
            </Typography.H2>
            <StyledToolboxContainer>
              <Button
                type="button"
                onClick={() => setShowFullscreen(true)}
                look="raised"
                icon="arrows-maximize"
                label={msg('client.widget-showcase.fullscreen')}
              />
              {widgetToolbox}
            </StyledToolboxContainer>
            <Fullscreen fullscreen={showFullscreen} onExit={() => setShowFullscreen(false)} toolbox={widgetToolbox}>
              <StyledWidgetContainer id={ID_WIDGET_CONTAINER}></StyledWidgetContainer>
            </Fullscreen>
          </StyledSection>

          {isAdvanced(mode) && (
            <StyledSection>
              <Typography.H2>
                <FormattedMessage id="client.widget-showcase.events" />
              </Typography.H2>
              {eventLog.length > 0 ? (
                <pre>
                  {eventLog.map(({key, name, args}) => (
                    <Fragment key={key}>
                      {name} - {JSON.stringify(args)}
                      <br />
                    </Fragment>
                  ))}
                </pre>
              ) : (
                '-'
              )}
            </StyledSection>
          )}

          {isAdvanced(mode) && (
            <StyledSection>
              <Typography.H2>
                <FormattedMessage id="client.widget-showcase.methods" />
              </Typography.H2>
              {methods ? (
                <>
                  {Object.keys(methods).map(method => (
                    <MethodButton label={method} onClick={methods[method]} key={method} intl={intl} />
                  ))}
                </>
              ) : (
                '-'
              )}
            </StyledSection>
          )}
        </>
      )}
    </Container>
  )
}

Showcase.propTypes = {
  selectedWidget: PropTypes.object,
  setSelectedWidget: PropTypes.func,
  selectedWidgetConfig: PropTypes.object,
  setSelectedWidgetConfig: PropTypes.func,
  loadWidgets: PropTypes.func,
  setWidgetConfig: PropTypes.func,
  editWidgetConfig: PropTypes.func,
  editTheme: PropTypes.func,
  setTheme: PropTypes.func,
  resetTheme: PropTypes.func,
  viewWidgetInput: PropTypes.func,
  widgets: PropTypes.array,
  widgetConfigs: PropTypes.array,
  widgetConfig: PropTypes.object,
  theme: PropTypes.object,
  isWidgetManager: PropTypes.bool,
  intl: PropTypes.object.isRequired,
  mode: PropTypes.string.isRequired
}

export default Showcase
