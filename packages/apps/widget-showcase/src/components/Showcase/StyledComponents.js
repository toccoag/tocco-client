import styled from 'styled-components'
import {declareFont, scale, themeSelector} from 'tocco-ui'

export const Container = styled.div`
  max-width: 1600px;
  margin: 0 auto;
`
export const StyledToolboxContainer = styled.div`
  display: flex;
  margin-bottom: 20px;
`

export const StyledFullscreenWidgetContainer = styled.div`
  background-color: ${themeSelector.color('paper')};
  z-index: 1;
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  padding: 0 ${scale.space(0.5)} ${scale.space(0.5)};
  position: absolute;
  box-sizing: border-box;
  overflow: auto;
`

export const StyledFullscreenToolboxContainer = styled.div`
  padding: ${scale.space(0.5)} 0 ${scale.space(1)};
  display: flex;
`

export const StyledWidgetContainer = styled.div`
  & * {
    ${declareFont({color: 'inherit'})}
  }
`

export const StyledEmptyInput = styled.span`
  ${declareFont()}
  min-height: 2.6rem;
  display: flex;
  align-items: center;
`

export const StyledSection = styled.div`
  margin: 20px 0;
  border-top: 1px solid ${themeSelector.color('borderLight')};
  padding: 40px 0 20px;
`

export const StyledVisibilityStatus = styled.span`
  ${declareFont()}
  border: 1px solid #e6e6e6;
  background-color: #e6e6e6;
  border-radius: ${themeSelector.radii('button')};
  padding: ${scale.space(-2.1)} ${scale.space(0)};
  margin-right: ${scale.space(-1.38)};
`
