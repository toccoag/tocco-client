import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {
  loadWidgets,
  setWidgetConfig,
  setSelectedWidget,
  setSelectedWidgetConfig,
  setTheme,
  resetTheme
} from '../../modules/showcase/actions'
import {editWidgetConfig, editTheme, viewWidgetInput} from '../../modules/showcaseActions/actions'

import Showcase from './Showcase'

const mapActionCreators = {
  setSelectedWidget,
  setSelectedWidgetConfig,
  loadWidgets,
  setWidgetConfig,
  editWidgetConfig,
  editTheme,
  setTheme,
  resetTheme,
  viewWidgetInput
}

const mapStateToProps = state => ({
  selectedWidget: state.showcase.selectedWidget,
  selectedWidgetConfig: state.showcase.selectedWidgetConfig,
  widgets: state.showcase.widgets,
  widgetConfigs: state.showcase.widgetConfigs,
  widgetConfig: state.showcase.widgetConfig,
  theme: state.showcase.theme,
  isWidgetManager: state.showcase.access.isWidgetManager,
  mode: state.showcase.mode
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Showcase))
