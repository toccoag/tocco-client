import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {IntlStub, testingLibrary} from 'tocco-test-util'
import {WidgetTheme} from 'tocco-theme'

import {modes} from '../../utils/mode'

import Showcase from './Showcase'

const defaultProps = {
  selectedWidget: null,
  setSelectedWidget: sinon.spy(),
  selectedWidgetConfig: null,
  setSelectedWidgetConfig: sinon.spy(),
  loadWidgets: sinon.spy(),
  widgets: [],
  widgetConfigs: [],
  widgetConfig: null,
  editWidgetConfig: sinon.spy(),
  theme: WidgetTheme,
  editTheme: sinon.spy(),
  setTheme: sinon.spy(),
  resetTheme: sinon.spy(),
  viewWidgetInput: sinon.spy(),
  isWidgetManager: false,
  mode: modes.simple,
  intl: IntlStub
}

const widgetSelectionProps = {
  widgets: [
    {
      key: 'w1',
      label: 'Widget 1'
    }
  ],
  selectedWidget: {key: 'w1', display: 'Widget 1'},
  widgetConfigs: [
    {
      key: 'wc2',
      uniqueId: 'widget-config-2',
      label: 'Widget Config 2'
    }
  ],
  selectedWidgetConfig: {
    key: 'wc2',
    uniqueId: 'widget-config-2',
    display: 'widget-config-2 - Widget Config 2'
  }
}

const renderShowcase = async props => {
  const renderResult = testingLibrary.renderWithIntl(<Showcase {...props} />)
  await screen.findAllByTestId('icon')
  return renderResult
}

describe('widget-showcase', () => {
  describe('components', () => {
    describe('Showcase', () => {
      beforeEach(() => {
        Object.keys(defaultProps).forEach(k => {
          if (typeof defaultProps[k]?.resetHistory === 'function') {
            defaultProps[k].resetHistory()
          }
        })
      })

      test('should render initial view', async () => {
        const props = {
          ...defaultProps
        }
        await renderShowcase(props)

        expect(screen.getByText('client.widget-showcase.selectWidget')).to.exist
        expect(props.loadWidgets).to.have.been.calledOnce
      })

      test('should be able to select a widget', async () => {
        const props = {
          ...defaultProps,
          widgets: [
            {
              key: 'w1',
              label: 'Widget 1'
            },
            {
              key: 'w2',
              label: 'Widget 2'
            },
            {
              key: 'w3',
              label: 'Widget 3'
            }
          ]
        }
        await renderShowcase(props)

        const user = userEvent.setup()

        const widgetSelet = screen.getByRole('combobox')
        await user.type(widgetSelet, 'Widget 3')
        await user.type(widgetSelet, '[Enter]')

        expect(props.setSelectedWidget).to.have.been.calledWith({key: 'w3', display: 'Widget 3'})
      })

      test('should be able to select a widget-config', async () => {
        const props = {
          ...defaultProps,
          widgets: [
            {
              key: 'w1',
              label: 'Widget 1'
            }
          ],
          selectedWidget: {key: 'w1', display: 'Widget 1'},
          widgetConfigs: [
            {
              key: 'wc1',
              uniqueId: 'widget-config-1',
              label: 'Widget Config 1'
            },
            {
              key: 'wc2',
              uniqueId: 'widget-config-2',
              label: 'Widget Config 2'
            }
          ]
        }
        await renderShowcase(props)

        const user = userEvent.setup()

        const widgetSelet = screen.getByLabelText('client.widget-showcase.widgetConfiguration')
        await user.type(widgetSelet, 'Widget Config 2')
        await user.type(widgetSelet, '[Enter]')

        expect(props.setSelectedWidgetConfig).to.have.been.calledWith({
          key: 'wc2',
          uniqueId: 'widget-config-2',
          display: 'widget-config-2 - Widget Config 2'
        })
      })

      test('show simple view', async () => {
        const props = {
          ...defaultProps,
          ...widgetSelectionProps
        }
        await renderShowcase(props)

        expect(screen.getByText('client.widget-showcase.settings')).to.exist
        expect(screen.getByText('client.widget-showcase.theme.applyWpTheme')).to.exist
        expect(screen.queryByText('client.widget-showcase.theme.edit')).to.not.exist
        expect(screen.queryByText('client.widget-showcase.widgetInput.view')).to.not.exist
        expect(screen.queryByText('client.widget-showcase.events')).to.not.exist
        expect(screen.queryByText('client.widget-showcase.methods')).to.not.exist
      })

      test('show advanced view', async () => {
        const props = {
          ...defaultProps,
          ...widgetSelectionProps,
          mode: modes.advanced
        }
        await renderShowcase(props)

        expect(screen.getByText('client.widget-showcase.settings')).to.exist
        expect(screen.getByText('client.widget-showcase.theme.applyWpTheme')).to.exist
        expect(screen.getByText('client.widget-showcase.theme.edit')).to.exist
        expect(screen.getByText('client.widget-showcase.widgetInput.view')).to.exist
        expect(screen.getByText('client.widget-showcase.events')).to.exist
        expect(screen.getByText('client.widget-showcase.methods')).to.exist
      })

      describe('WordPress-Theme', () => {
        test('should be able to apply the WordPress theme', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps
          }

          await renderShowcase(props)

          const user = userEvent.setup()
          await user.click(screen.getByText('client.widget-showcase.theme.applyWpTheme'))

          expect(props.setTheme).to.have.been.calledOnce
        })

        test('should be able to reset theme when WordPress theme is applied', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps,
            theme: {...defaultProps.theme, name: 'wk-widget'}
          }

          await renderShowcase(props)

          expect(screen.queryByText('client.widget-showcase.theme.applyWpTheme')).to.not.exist
          expect(screen.getByText('client.widget-showcase.theme.resetWpTheme')).to.exist

          const user = userEvent.setup()
          await user.click(screen.getByText('client.widget-showcase.theme.resetWpTheme'))

          expect(props.resetTheme).to.have.been.calledOnce
        })
      })

      describe('Widget Manager', () => {
        test('hide widget config edit for non widget managers', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps,
            mode: modes.advanced
          }
          await renderShowcase(props)

          expect(screen.queryByText('client.widget-showcase.widgetConfiguration.edit')).to.not.exist
        })

        test('able to edit widget config for widget manager', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps,
            isWidgetManager: true
          }
          await renderShowcase(props)

          const user = userEvent.setup()
          await user.click(screen.getByText('client.widget-showcase.widgetConfiguration.edit'))

          expect(props.editWidgetConfig).to.have.been.calledOnce
        })
      })

      describe('Advanced Mode', () => {
        test('able to edit theme', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps,
            mode: modes.advanced
          }
          await renderShowcase(props)

          const user = userEvent.setup()
          await user.click(screen.getByText('client.widget-showcase.theme.edit'))

          expect(props.editTheme).to.have.been.calledOnce
        })

        test('able to view widget input', async () => {
          const props = {
            ...defaultProps,
            ...widgetSelectionProps,
            mode: modes.advanced
          }
          await renderShowcase(props)

          const user = userEvent.setup()
          await user.click(screen.getByText('client.widget-showcase.widgetInput.view'))

          expect(props.viewWidgetInput).to.have.been.calledOnce
        })
      })
    })
  })
})
