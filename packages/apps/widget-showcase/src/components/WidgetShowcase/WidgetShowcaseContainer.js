import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {init, setMode} from '../../modules/showcase/actions'

import WidgetShowcase from './WidgetShowcase'

const mapActionCreators = {
  init,
  setMode
}

const mapStateToProps = state => ({
  isDevelopmentRunEnv: state.showcase.access.isDevelopmentRunEnv,
  isWidgetManager: state.showcase.access.isWidgetManager,
  mode: state.showcase.mode
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(WidgetShowcase))
