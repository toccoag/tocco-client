import {createGlobalStyle} from 'styled-components'
import {declareFont} from 'tocco-ui'

export const GlobalStyles = createGlobalStyle`
  html,
  body {
    ${declareFont()}
    font-size: 14px !important;
  }
`
