import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage, injectIntl} from 'react-intl'
import {notification} from 'tocco-app-extensions'
import {Button, ButtonGroup, LoadMask, Typography} from 'tocco-ui'

import ToccoLogo from '../../assets/tocco-circle.svg'
import {isAdvanced, isSimple, modes} from '../../utils/mode'
import Showcase from '../Showcase'

import {GlobalStyles} from './GlobalStyles'
import {
  StyledWidgetShowcaseWrapper,
  StyledNavigationWrapper,
  StyledMain,
  StyledLogo,
  StyledBrand,
  StyledLightText,
  StyledBoldText,
  StyledHeader,
  StyledModeWrapper
} from './StyledComponents'

const WidgetShowcase = ({init, intl, isDevelopmentRunEnv, isWidgetManager, mode, setMode}) => {
  useEffect(() => {
    init()
  }, [init])

  return (
    <LoadMask required={[typeof isWidgetManager !== 'undefined' && isDevelopmentRunEnv !== 'undefined']}>
      <GlobalStyles />
      {isWidgetManager || isDevelopmentRunEnv || __DEV__ ? (
        <StyledWidgetShowcaseWrapper>
          <StyledHeader>
            <StyledNavigationWrapper>
              <StyledBrand>
                <StyledLogo src={ToccoLogo} alt="tocco-logo" />
                <Typography.H1>
                  <StyledBoldText>Tocco</StyledBoldText> <StyledLightText>Widget Showcase</StyledLightText>
                </Typography.H1>
              </StyledBrand>
            </StyledNavigationWrapper>
          </StyledHeader>
          <notification.Notifications />

          <StyledMain>
            <StyledModeWrapper>
              <ButtonGroup>
                <Button
                  look="raised"
                  onClick={() => setMode(modes.simple)}
                  label={intl.formatMessage({id: 'client.widget-showcase.mode.simple'})}
                  {...(isSimple(mode) ? {ink: 'secondary'} : {})}
                />
                <Button
                  look="raised"
                  onClick={() => setMode(modes.advanced)}
                  label={intl.formatMessage({id: 'client.widget-showcase.mode.advanced'})}
                  {...(isAdvanced(mode) ? {ink: 'secondary'} : {})}
                />
              </ButtonGroup>
            </StyledModeWrapper>
            <Showcase />
          </StyledMain>
        </StyledWidgetShowcaseWrapper>
      ) : (
        <Typography.P>
          <FormattedMessage id="client.widget-showcase.noAccess" />
        </Typography.P>
      )}
    </LoadMask>
  )
}

WidgetShowcase.propTypes = {
  intl: PropTypes.object.isRequired,
  init: PropTypes.func.isRequired,
  isDevelopmentRunEnv: PropTypes.bool,
  isWidgetManager: PropTypes.bool,
  setMode: PropTypes.func.isRequired,
  mode: PropTypes.string.isRequired
}

export default injectIntl(WidgetShowcase)
