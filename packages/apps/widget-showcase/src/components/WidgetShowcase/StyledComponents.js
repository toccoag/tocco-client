import styled from 'styled-components'
import {themeSelector, declareFont, scale, StyledH1} from 'tocco-ui'

export const StyledWidgetShowcaseWrapper = styled.div`
  background-color: ${themeSelector.color('paper')};
  display: flex;
  flex-direction: column;
  height: 100%;
  ${declareFont()}
`

export const StyledLogo = styled.img`
  width: 40px;
  height: auto;
`

export const StyledBrand = styled.div`
  display: flex;
  gap: ${scale.space(0)};
  align-items: center;

  ${StyledH1} {
    margin-top: 0;
    ${declareFont({
      fontSize: scale.font(9),
      fontWeight: themeSelector.fontWeight('bold')
    })}
  }
`

export const StyledModeWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  max-width: 1600px;
  margin: 0 auto ${scale.space(1)};
`

export const StyledBoldText = styled.span`
  font-weight: 500;
`
export const StyledLightText = styled.span`
  ${declareFont({
    color: themeSelector.color('textLight'),
    fontSize: scale.font(9)
  })}
`

export const StyledHeader = styled.header`
  position: sticky;
  top: 0;
  z-index: 1;
  background-color: ${themeSelector.color('paper')};
  box-shadow: 0 0 10px rgba(0 0 0 / 30%);
  border-bottom: 1px solid ${themeSelector.color('borderLight')};
`

export const StyledNavigationWrapper = styled.nav`
  min-height: 50px;
  max-width: 1600px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${scale.space(-0.5)} ${scale.space(0.5)} ${scale.space(-0.5)} 0;
  box-sizing: border-box;
  margin: 0 auto;
`

export const StyledNavigationList = styled.ul`
  list-style: none;
  display: flex;
`

export const StyledMain = styled.main`
  flex: 1;
  padding: ${scale.space(1)} ${scale.space(0.5)};
  overflow: auto;
`
