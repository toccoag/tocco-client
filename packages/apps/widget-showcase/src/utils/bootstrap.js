import {env} from 'tocco-util'
import {utils} from 'tocco-util/bundle'

import {ID_WIDGET_APP, ID_WIDGET_CONTAINER} from './constants'

export const getWidgetContainerElement = () => document.getElementById(ID_WIDGET_CONTAINER)

export const renderWidget = async (container, widgetConfigUniqueId, widgetConfig, customTheme, eventHandlers) => {
  const {packageName, appName, locale, config} = widgetConfig

  const backendUrl = env.getBackendUrl()
  await utils.loadScriptAsync(`${backendUrl}${utils.getEntryFilePath(packageName, appName)}`)

  const input = {
    backendUrl,
    locale,
    ...(customTheme ? {customTheme} : {}),
    ...config,
    appContext: {
      embedType: 'widget',
      widgetConfigKey: widgetConfigUniqueId
    },
    ...eventHandlers
  }
  const srcPath = `${backendUrl}/js/tocco-${packageName}/dist/`

  const methods = window.reactRegistry.render(appName, container, ID_WIDGET_APP, input, {}, srcPath)
  return {methods, input}
}

export const unmountWidget = () => {
  return new Promise(resolve => {
    const container = getWidgetContainerElement()
    if (!container || container.children.length === 0) {
      resolve()
      return
    }

    setTimeout(() => {
      window.reactRegistry.unmount(ID_WIDGET_APP)
      resolve()
    }, 0)
  })
}
