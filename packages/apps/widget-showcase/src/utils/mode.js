import Cookies from 'js-cookie'

const MODE_COOKIE = 'tocco-widget-showcase-mode'

export const modes = {
  advanced: 'advanced',
  simple: 'simple'
}

export const isAdvanced = mode => mode === modes.advanced
export const isSimple = mode => mode === modes.simple

export const persistMode = mode => {
  Cookies.set(MODE_COOKIE, mode, {expires: 365})
}

export const getPersistedMode = () => {
  const mode = Cookies.get(MODE_COOKIE)

  if (mode && !Object.keys(modes).includes(mode)) {
    Cookies.remove(MODE_COOKIE)
    return undefined
  }

  return mode
}
