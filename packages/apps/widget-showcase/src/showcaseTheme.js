export default {
  name: 'widget-showcase',
  fontFamily: {
    monospace: 'Ubuntu Mono, monospace',
    regular: 'Ubuntu, sans-serif',
    url: 'https://fonts.googleapis.com/css?family=Ubuntu+Mono:300,500|Ubuntu:300,500&display=swap'
  },
  fontSize: {
    base: 1,
    factor: 1.1
  },
  fontWeights: {
    regular: 300,
    bold: 300
  },
  radii: {
    button: '20px'
  },
  space: {
    base: 1.0,
    factor: 2
  }
}
