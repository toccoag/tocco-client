import PropTypes from 'prop-types'
import React, {Suspense} from 'react'
import {actionEmitter, appFactory, selection, notification} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext} from 'tocco-util'

import reducers, {sagas} from './modules/reducers'
import showcaseTheme from './showcaseTheme'

const packageName = 'widget-showcase'

const LazyWidgetShowcase = React.lazy(() => import('./components/WidgetShowcase'))
const WidgetShowcase = () => (
  <div>
    <Suspense fallback="">
      <LazyWidgetShowcase />
    </Suspense>
  </div>
)

const initApp = (id, input, events, publicPath) => {
  const content = <WidgetShowcase />

  const actualInput = {...input, customTheme: showcaseTheme}
  const store = appFactory.createStore(reducers, sagas, actualInput, packageName)
  actionEmitter.addToStore(store)
  notification.addToStore(store, true)

  return appFactory.createApp(packageName, content, store, {
    input: actualInput,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const WidgetShowcaseApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

WidgetShowcaseApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  selection: selection.propType.isRequired,
  /**
   * Route where the showcase is loaded (part after top level domain)
   */
  baseRoute: PropTypes.string
}

export default WidgetShowcaseApp
