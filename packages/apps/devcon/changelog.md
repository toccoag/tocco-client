3.13.41
- make widget date picker more compact

3.13.40
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- align ranges horizontally for widgets

3.13.39
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.38
- add isTransparent prop to Ball to enable transparent backgrounds

3.13.37
- fix document component

3.13.36
- fix modal in routerless widgets

3.13.35
- show icon on tiles
- add icons

3.13.34
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- add week numbers to date picker

3.13.33
- add labelVisibility prop to Button to enable omitting the prop in specific cases

3.13.32
- fix sidepanel button overflow
- able to set justify alingment on tile column
- handle use label properly

3.13.31
- harmonize react select spacing

3.13.30
- fix focus styling

3.13.29
- close all modals on route change
- adapt component list text resources

3.13.28
- fix adress / location browser autofill
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.27
- force rerender of ckeditor with auto-complete
- export idSelector
- fix ckeditor and initial auto complete

3.13.26
- improve label / form field component accessibility
- improve tile responsiveness on mobile devices

3.13.25
- fix stylelint error by removing obsolete styling

3.13.24
- remove useSelectorsByDefault property

3.13.23
- allow setting title for button
- pass inWrongBusinessUnit

3.13.22
- add data-cy to table refresh button

3.13.21
- add background color for table tiles
- improve tile styling

3.13.20
- no error message on column resize

3.13.19
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- show tile on narrow phones
- hide empty cells on tile

3.13.18
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.17
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.16
- fix stated value labels spacing
- fix color of label-neutral

3.13.15
- harmonize google maps icon positioning in LocationEdit

3.13.14
- add timezone header

3.13.13
- show tooltips on multi-select values
- add tocco home breadcrumb item

3.13.12
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.11
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.10
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.9
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.8
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.7
- cleanup marking code
- add native date(time)picker for touch devices

3.13.6
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.5
- add _widget_key query param to all requests

3.13.4
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.3
- fix growing textarea
- handle StyledLayoutContainer styling globally
- able to set link state on breadcrumbs

3.13.2
- fix cut off text in singleValue Selects
- add fetchModules helper

3.13.1
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.24
- revert focus fix

3.12.23
- do not auto focus anything on touch devices

3.12.22
- improve UX for simple selects on touch devices

3.12.21
- fix double scrollbars in some modals

3.12.20
- fix missing focus styles in some elements

3.12.19
- prevent infinite resize
- show left column drop indicator
- consider vertical drop position for column sorting

3.12.18
- add tooltip texts for buttons of upload component

3.12.17
- fix table edit styling

3.12.16
- set checkbox field to touched on safari/ipad/iphone

3.12.15
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes

3.12.14
- hide table refresh button when nothing happens

3.12.13
- able to click on the label in the column picker
- fix hover on toaster close buttons

3.12.12
- add context to auto-complete

3.12.11
- show correct select field in export action
- show correct select field

3.12.10
- use error message when action fires error event

3.12.9
- support report actions in adjustAllActions

3.12.8
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- remove unused / broken styling

3.12.7
- be able to accept prompt with enter

3.12.6
- extend font sizes with missing sizes

3.12.5
- trim searchInput value

3.12.4
- add declareColoredLabel util
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.30
- do not apply btn hover style on disable buttons

3.11.29
- add copy icon
- fix outputjob in synchron simple action

3.11.28
- support search fields in useAutofocus

3.11.27
- add legacy font sizes to HtmlEditor
- cleanup transient props
- adjust toaster for action validation

3.11.26
- TOCDEV-9023

3.11.25
- add mailbox icon

3.11.24
- fix dev con fragments being displayed in single row

3.11.23
- use correct shouldForwardProp implementation

3.11.22
- add new property to theme
- add navigateToPath to navigationStrategy
- add icon
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18

3.11.21
- update react-router v6
- remove unused history

3.11.20
- use transient props for styled components
- align stated value boxes content vertically

3.11.19
- add new icons

3.11.18
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.17
- add truck-medical icon

3.11.16
- add id to formatted values

3.11.15
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes

3.11.14
- standardize sticky button wrapper styling

3.11.13
- support post flush validation
- replace default props in tocco-ui with javascript default parameters

3.11.12
- ignore undefined optionDisplay

3.11.11
- implement validation for simple actions

3.11.10
- update to yarn 4

3.11.9
- handle rule provider in widgets
- handle optionDisplay in remote and select fields

3.11.8
- do not overwrite toasters from sync simple actions
- add react-router compatibility package
- migrate devcon to react router v6

3.11.7
- fix tooltips freeze by replacing popper library with floating-ui library

3.11.6
- fix range mapping after upgrade to date-fns 3.x

3.11.5
- fix formatted value colors in dark mode labels

3.11.4
- allow auto focus to be disabled

3.11.3
- fix button group not being displayed correctly after styled components update

3.11.2
- fix react-select menu after upgrade to styled component

3.11.1
- merge ModalDisplay and BlockingDisplay into OverlayDisplay
- show error id for actions
- fix clear number field via auto complete

3.11.0
- initial release for version 3.11

3.10.33
- map postcode fields to string tql
- add option to reevalute action condition

3.10.32
- enhance toasters with large content by adding vertical scrollbar

3.10.31
- fix popover text and stated value text not wrapping

3.10.30
- render html in toaster body
- return horizontal drop position in useDnD
- allow dropping and display drop indicator on both sides in ColumnPicker

3.10.29
- add row numbering to ui table

3.10.28
- rename property description to title
- move rest endpoint to client "route"
- fix side panel vertical scrolling in create view

3.10.27
- fix hostname for custom actions in cms
- add method to extract env as input props
- pass env to dynamic action

3.10.26
- Fixed filtering by elapsed time in SQL log

3.10.25
- fix datepicker styling in widgets by providing the tocco-app classname which prevents webkinder styles being applied
- let requestSaga handle blocking info in simpleAction

3.10.24
- disable auto-complete when selecting template
- map phone fields to string tql

3.10.23
- place date picker directly in body to prevent z-index problems in widgets

3.10.22
- set max height for info boxes
- disable cache for create forms

3.10.21
- do not show same tooltip twice

3.10.20
- delay the display of popovers to prevent distracting flashing effect in large lists on hover
- style link inside label duplicate
- do not show same tooltip twice

3.10.19
- use breaks instead of separate paragraphs for readonly text
- add isDefined helper
- accept falsy values as settings for reports

3.10.18
- fix draggable file/image

3.10.17
- change link color back to secondary'
- harmonize icon list spacing
- style duplicate warning
- optimise last opened color for dark theme
- fix selected day color in date picker
- fix draggable in preview

3.10.16
- enhance breadcrumbs und popover colors for dark mode
- decrease spacing of menu tree items
- enhance dark dark theme colors

3.10.15
- fix save button coloring in dark mode

3.10.14
- fix clear of remote field
- improve dark theme color harmony
- enhance table aesthetic and readability

3.10.13
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation
- fix disabled buttons disappearing in widgets

3.10.12
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation

3.10.11
- pass navigationStrategy to simple action form
- add report to output group instead of new group

3.10.10
- adjust isReadOnly for address and location field
- fix rounding issue for sticky columns
- allow not whitelisted style properties for svg element

3.10.9
- remove obsolete bottom padding in form
- extend widget theme with missing text colors
- add file circle plus icon

3.10.8
- style payment summary view

3.10.7
- style fixed columns in table with border at the right edge
- add coloration for number of attempts

3.10.6
- add title to description element
- add acknowledge notification and entities message
- remove description title again
- handle custom title in action pre checks

3.10.5
- disable CKEditor version check
- fix dark theme icon issues within buttons

3.10.4
- respect itemCount as limit for remote fields
- default disable remote create in widgets

3.10.3
- enable whole area of menu entries to be clicked
- fix dark theme color issues
- apply corresponding theme background color to login screen

3.10.2
- only use AutoSizer with new windowed table

3.10.1
- fix colors in signal list items for dark mode

3.10.0
- initial release for version 3.10

3.9.31
- style focused menu elements the same as on hover

3.9.30
- set referrer policy to `no-referrer-when-downgrade`
- add sticky column to ui table
- apply theme themeswitching to ace code editor as well

3.9.29
- fix blocking display disappearing in widgets on long pages

3.9.28
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin

3.9.27
- fix menu items wobble on hover in firefox browser
- fix dark theme color issues

3.9.26
- change last opened color to increase readability

3.9.25
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- use Debouncer in CodeEdit instead of custom debounce

3.9.24
- enable hover on parent items of menu tree

3.9.23
- map shrinkToContent in getColumnDefinition
- fix jumping buttons in tocco header when opening help menu

3.9.22
- improve menu ux by adding hover background and expanding clickable area
- add dark mode
- fix widget theme

3.9.21
- fix intermediate values in time edit

3.9.20
- fix intermediate values in time edit

3.9.19
- add no button option for simple action form

3.9.18
- map email fields to string tql

3.9.17
- add non breaking space to ensure correct spacing in multiple fields separator

3.9.16
- fix column order resetting when moving multiple columns after another

3.9.15
- support custom report settings in widgets
- harmonize exam edit spacing

3.9.14
- enhance last openend color for better visibility

3.9.13
- limit watchForHover to activate in window.onload

3.9.12
- Add `watchForHover` styling utility

3.9.11
- fix enabling button after db refactoring finished
- shrink size of datepicker

3.9.10
- fix width of exam edit table ipunt fields in firefox
- add last opened to ui table
- align stated value content verically with label

3.9.9
- use path for setting placeholder terms value
- fix multi-select-field

3.9.8
- hide readonly terms

3.9.7
- harmonize date picker styling and implement time picker
- fix selection in docs tree search

3.9.6
- fix multivalue separator span alignment

3.9.5
- add updateDocumentTitle prop to Breadcrumbs

3.9.4
- register gear icon

3.9.3
- split styling of blocking display for admin and widget
- fix multivalue label text overflow

3.9.2
- add readonly terms config and use pristine value change for placeholders
- handle immutable prop in TermsEdit

3.9.1
- split toaster into widget and admin components
- adjust height of evaluation tree table to scale depending on available viewport height

3.9.0
- initial release for version 3.9

3.8.21
- move logic from reports saga to backend

3.8.20
- align widget labels at flex start position
- register list-tree icon

3.8.19
- harmonize status label border radii
- add border around exam edit datepicker inputs

3.8.18
- fix resizing of ui table

3.8.17
- add download icon to DocumentFormatter

3.8.16
- fix terms edit link color
- navigate to next field without selecting with tab

3.8.15
- escape front-slash in fulltext tql query

3.8.14
- vertically center input edit table cells
- force underscore styling of terms edit links

3.8.13
- hide overflow of column headers
- register gear icon
- disable sideEffects optimization in webpack
- fix css styles not being passed down to multi value label span
- fix sticky buttons within modal cutting off content
- fix terms edit link styling and refactor component

3.8.12
- register calculator icon

3.8.11
- fix themes by adding missing status label radius
- extract nested ternary operation
- improve exam edit action styling
- fix vertical alignment of html formatter

3.8.10
- add data-cy attributes

3.8.9
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add BasicSearchForm
- add qualification dataRequest
- make select work without formData
- add useTitle to FormattedValue
- add additional units

3.8.8
- refactor panel group to remove duplications

3.8.7
- fix long lines in code edit being cut off by showing scrollbar
- add id to ReactDatePicker
- add default link target and protocol to html editor

3.8.6
- fix disabled button and menu item styles
- fix broken prop types
- do not use array index in keys
- fix legacy styles being applied to status labels

3.8.5
- change fields separator
- change fields separator
- make editableValueFactory work without formField

3.8.4
- remove obsolete padding around styled entity browser
- fix disabled textarea on safari

3.8.3
- fix menu menu links not opening in new tab when icon is clicked

3.8.2
- fix menu menu links not opening in new tab when icon is clicked

3.8.1
- use widget prefix for report location
- lighten status label colors and harmonize spacing

3.8.0
- initial release for version 3.8

1.0.210
- harmonize menu layout by moving technical name below label
- fix scrollbars on disabled textareas with content

1.0.209
- reset disabled min-height on textareas

1.0.208
- harmonize panel spacing and font weights
- make actionConditions work in the children of action groups

1.0.207
- build up modifiers from smaller independent functions

1.0.206
- continue handling form elements after a custom component was encountered
- do not crash if no formValues exists when checking terms data

1.0.205
- enable co-existence with legacy ckeditor
- improve readability of status labels
- adjust disabled textarea height to match vertical label position

1.0.204
- do not render terms if no conditions were configured

1.0.203
- fix getTextOfChildren

1.0.202
- fix buggy scroll to top for modals
- handle phone validation for relations
- handle validation response properly
- fix broken prop types

1.0.201
- dependency update
- dependency update
- dependency update
- add data-cy attributes

1.0.200
- handle de-DE locale for datepicker
- align terms condition with checkbox properly

1.0.199
- increase left padding of merge table

1.0.198
- add min width to toaster box

1.0.197
- get all address suggestions within country
- render sidepanel initially with correct collapse state
- add terms component
- add terms component
- add pickAllProperties to getFlattenEntity

1.0.196
- export logged in action type

1.0.195
- add reports after new button
- place label in widgets on left side
- move question code to tocco-util

1.0.194
- increase toaster width to prevent early line breaks within titles
- fix acl browser styling

1.0.193
- add icon for action group
- fix immutable label positioned inside
- fix log input height
- fix immutable height of integer edit by passing immutable prop

1.0.192
- clear cached view stores

1.0.191
- style edit text within html formatter
- reset value properly

1.0.190
- fix docs browser label alignment
- set correct label position
- fix build for customer bundles

1.0.189
- adjust useSyncValidation
-  add data-cy attr to confirm buttons
- fix hostname field in log view

1.0.188
- fix disabled select spacing
- adjust widget stated value label color
- add action form modifier fn
- add simple form helper functions

1.0.187
- fix choice edit label alignment
- fix light cellrenderer width

1.0.186
- add core sidepanel horizontal button comp

1.0.185

1.0.184
- adjust google maps address link
- allow all html content
- align editor config with legacy editor
- add advanced color picker
- add paste as plaintext
- add upload to editableMapping

1.0.183
- fix null values in percent field

1.0.182
- add font awesome eye icon
- use eye icon for documents
- export StyledTableRow

1.0.181
- fix action button styling

1.0.180
- style promotion status badge
- adjust font factor of widget theme

1.0.179
- extend embed type
- add widget panel
- handle label position via prop
- handle label color for widget and admin differently
- handle different form components for widgets
- treat admin embedType logic as well for legacy-admin
- load formComponents on runtime

1.0.178
- add action button styling

1.0.177
- support hide and read-only for location/address

1.0.176
- copy only changesets function
- position toasters at bottom right to prevent interference with interaction elements
- remove x-backend-url header
- align labels in center of input box
- harmonize positioning of sticky buttons within modal

1.0.175
- editable sql statements
- copy changelog to clipboard
- add cypress data attribute to breadcrumb

1.0.174
- handle remote create on simpel actions
- show advancedSearch properly

1.0.173
- fix country cache
- expand checkbox and label options for ChoiceEdit

1.0.172
- move country cache to object cache

1.0.171
- harmonize menutree spacing and implement default icon

1.0.170
- Make sure that address field is visible also without value

1.0.169
- show multiline lables with correct line breaks
- Added new component to search for full address

1.0.168
- fix client questions with cancel
- do not save form on open dropdown

1.0.167
- show entity-docs in email attachements

1.0.166
- remove top and left padding within immutable stated value box
- fix alignment of input fields within input edit table
- show back action always before reports
- fix fulltext search

1.0.165
- also implement border radius for indicator container wrapper to prevent cover of input field edges
- add cancel button to confirm modals
- show correct pending changes modal on widgets
- fix border radius calculation of panel headers

1.0.164
- fix baseline of code edit

1.0.163
- fix input width of login form

1.0.162
- position input label at baseline of input

1.0.161
- replace ckeditor5 with ckeditor4
- fix PropTypes - date to string
- fix PropTypes - function to conditions list

1.0.160
- add additional data-cy attributes

1.0.159
- fix height rendering of load mask and spacing of layout boxes within modal

1.0.158
- fix duplicated key on list

1.0.157
- remove minimum height for modal body which is not needed
- remove [object Object] button title
- ensure pending changes prompt is fired first

1.0.156
- fix missaligned dropdown indicators within select
- add fetchAllEntities which handles paging by itself
- replace fetchEntities in selection
- replace custom paging in notifications
- replace fetchEntities in formData

1.0.155
- improve readability of disabled multi value label text
- be able to tab through mulit-remote-selects
- navigate to next field with tab on select

1.0.154
- add global classname for all wrapping elements

1.0.153
- use global scrollbar style instead of specific imports
- support column action in adjustActions

1.0.152
- fix miscellaneous ui bugs in widgets

1.0.151
- adjust input field z-index and harmonize padding within input edit
- harmonize modal spacing and prevent scrollbar with minimum height introduction

1.0.150
- add action conditions
- harmonize label spacing in choice edit
- add outputgroup when multiple reports

1.0.149
-

1.0.148
- fix report generation with empty model

1.0.147
- only update input when app is mounted
- restructure cache for widget use cases
- remove revision from object cache

1.0.146
- handle expanded range field via default value
- remove unused expanded property

1.0.145
- add X-Backend-Url to all requests
- adjust X-Backend-Url to all requests

1.0.144
- add synchronous validation for pseudoSelect
- fix mandatory validation for pseudoSelect

1.0.143
- fix border radius of search boxes
- fix select fields exceeding the new maximum limit and crashing

1.0.142
- fix phone edit
- fix datepicker input styling

1.0.141
- do not submit form on select item
- do not propagate and use default for enter key

1.0.140
- refactor errors in pseudo form fields

1.0.139
- removed filename

1.0.138
- add pseudo select ui component
- add pseudo select field

1.0.137
- upgrade to react v18

1.0.136
- allow customization of validation messages
- add checkbox pseudo field helper

1.0.135
- show correct border-radius on panel header
- enforce css selectors for textarea

1.0.134
- fix light cellrenderer font sizing to prevent icon overflow within cells

1.0.133
- add geosearch field

1.0.132
- trigger visibility status change only for widget embed type

1.0.131
- add onSuccess to synchronous simple actions

1.0.130
- fix cancelling client questions

1.0.129
- when creating a new entity, trigger auto-complete for fields with default values

1.0.128
- fix header and content cell aligment of table
- harmonize nav and search box styling

1.0.127
- make yes button in YesNo client question work again
- re-init cache instead of clear cache
- fix caching and intl race condition

1.0.126
- remove unnecessary usage of darken and lighten functions and use theme colors instead

1.0.125
- add option to disable onclick on breadcrumb

1.0.124
- remove light cellrenderer flex so that it does not cut off content in table
- improve sql dialog styling inside devcon

1.0.123
- remove usage of shade color function and extend theme instead
- introduce radii

1.0.122
- tqlBuilder now builds multiple conditions connected by 'or' when passed localized paths
- accept enter two-year date without punctation

1.0.121
- clean up shade color function use and switch to theme colors instead
- add on enter feature to advanced search

1.0.120
- extend theme with more specific colors to better target elements
- adapt new specific theme colors to table, select and column picker

1.0.119
- handle url search fields as strings
- be able to have uppercase urls

1.0.118
- standardize yes no question modal buttons

1.0.117
- fix theme selectors

1.0.116
- render fields reached through an empty but writable path
- improve global styles

1.0.115
- use correct fonts for choices

1.0.114
- harmonize spacing of breadcrumbs

1.0.113
- add min/max selection option for multiple-choice
- support min and max selection in validation

1.0.112
- fix error message alignment
- add choice ui component
- add choice field
- add form pseudo field logic
- add additional condition to getUsedPaths

1.0.111
- standardize page overlay and holder components for modal and blocking info

1.0.110
- datepicker focus handling on ffox and date only inputs
- lower height threshold of button context

1.0.109
- fix react attribute error

1.0.108
- fix default confirm button
- fix sidepanel height
- extend theme
- add sticky action bar
- simplify dynamic action configuration

1.0.107
- add icons for user menu and business unit menu inside header

1.0.106
- display more custom icons in Breadcrumbs as defined by backend, specifically domains and folders in dms

1.0.105
- add autoFocusOnMount prop to ace editor to set focus manually since the useAutoFocus hook is not working in this case
- replace range type icons with generic icon and harmonize alignment to improve UX
- fix modal position in widget context and use vh units

1.0.104
- fix table randomly jumping scroll positions when switching pages

1.0.103
- fix autofocus by wrapping selector in timeout

1.0.102
- add possibility to show reload prompt
- add prompt component

1.0.101
- add back button

1.0.100
- enhance external events

1.0.99
- fix object cache

1.0.98
- add object cache
- use global object cache

1.0.97
- allow padding of Button to be removed like when used as icon
- allow Action to be rendered as text without button visual
- export action types to use same names everywhere
- confirm modals now use whichever default action the backend declares, which means switching the styling and keyboard shortcut

1.0.96
- order range values on top instead of side by side to improve ux
- create ENTITY_DOCS selection for legacy actions
- move submit button to parent
- add sync validation to template form

1.0.95
- support identifier type in tql builder
- enforce css selectors

1.0.94
- link for max 100 created entities
- add docs-tree search
- add conditional selection for tables
- only select entities for docs tree search

1.0.93
- allow target attribute in html

1.0.92
- do not use default display for tooltips again

1.0.91
- send selection to adjusted POST endpoint when loading report settings
- use a new custom display when displaying entities in select boxes

1.0.90
- harmonize spacing of ranged dates by increasing search panel width

1.0.89
- reintroduce min-width for Ball component, since normal width caused rendering issues in the main menu
- replace useWindowWidth hook with universal useWindowSize hook

1.0.88
- do not collapse sidepanel on top

1.0.87
- show time in 24h format for all locales

1.0.86
- move toasters on mobile to bottom for better ux as most of the interaction elements (e.g actions) are on top and should not be covered

1.0.85
- do not reload fullscreen action on single action

1.0.84
- add collapsible sidepanel component
- introduce legacy-admin embed type

1.0.83
- use Icon component in LoadingSpinner

1.0.82
- move component specific event handlers

1.0.81
- fix null values in field name transformation
- support nested paths in location field
- provide entityField for create field config
- use long as datetime value

1.0.80
- allow componentConfigs to adjust value used
- use correct decimal digits and % suffix for percentage fields
- use FormattedNumber in percent mode in PercentFormatter
- increase top padding of input label to prevent cut off

1.0.79
- disable removing outputjob toasters in widgets
- fix performance of fulltext search

1.0.78
- prevent body scroll when modal is opened

1.0.77
- improve responsiveness of table component
- datepicker keyboard handling
- do not lazy load datepicker
- autofocus datepickers

1.0.76
- add helpers to remove fields from forms

1.0.75
- fix escape handling in tql fulltext search

1.0.74
- time input component
- fix scroll on select

1.0.73
- add widget config key to env
- add custom create endpoint to validation
- remove removeCreate form modifier
- add addCreate form modifier

1.0.72
- add pen-field icon
- increase menu item padding on touch devices for better usability

1.0.71
- ignore any text and textarea fields of a datepicker when autofocusing

1.0.70
- fix date picker icon

1.0.69
- fix datepicker year dropdown closing on scroll
- fix whitespace in notification

1.0.68
- close datepicker on scroll
- add calendar icon
- add icon to date picker

1.0.67
- add model name to notification

1.0.66
- reset select dropdown to older state and implement close on scroll feature
- fix select dropdown dimension calculations

1.0.65
- adjust sizing of datepicker on mobile screens

1.0.64
- generate reports without dialog in widgets

1.0.63
- collapse buttons when wrapper is too small
- use responsiveness buttons on action bar

1.0.62
- add ballot-check icon

1.0.61
- quick fix that onChange is not infinite often triggered

1.0.60
- HTML editor: fix saving the form in source editing mode

1.0.59
- enable fullscreen modals for mobile/touch devices

1.0.58
- show collapse/expand icon on touch devices in panel header/footer
- harmonize modal spacing of modal message

1.0.57
- remove date picker arrow as the positioning seems to be off when fields are too long

1.0.56
- fix position of reports in form modifier
- fix datetime range with single default value
- do not clear date input on blur

1.0.55
- improve datepicker styling and fix bugs in widgets
- force hover styles of upload button when hovering upload field
- remove advanced search text in modal title
- fix input label being cut in widgets

1.0.54
- change signal box background colors for more consistency

1.0.53
- fix change date on range date picker
- fix datetime search field with default value
- keep ckeditor as similar as legacy editor

1.0.52
- add new list action icons
- use ckeditor
- only cache displays as long as tab is open

1.0.51
- accept fulltext search term for advanced search

1.0.50
- use constrictions from form in remotefield as well
- set current time as default time value in datepicker

1.0.49
- fix breakwords for email and phone formatter

1.0.48
- remotefields now sort the same way in select box and advanced search (form sorting first, then last updated)

1.0.47
- add list-check and percent icons
- allow date inputs without punctuation
- allow two digits year formats
- use save aborted key as textresource instead of message directly

1.0.46
- style new date picker like the old one
- fulltext search fields are now prioritized to be autofocused

1.0.45
- fix datetime in searchforms after using date-fns
- enhance breadcrumbs
- column labels in ColumnPicker no longer display escaped html elements

1.0.44
- add envelope icon
- add icon to email field to mail client
- add link to email and phone formatter

1.0.43
- today button in datepicker now behaves as expected

1.0.42
- reports added through formModifier are now placed directly into the action bar, instead of the output group
- report definitions now contain an icon as defined by the backend

1.0.41
- remote fields now honor the constrictions defined on forms with remotefield scope
- advanced search now uses form with remotefield scope if it exists
- remotefields now use sorting of their forms or entity models instead of only their timestamp

1.0.40
- optimize toggle behaviour of column picker to only select currently visible items
- do not import react components in widget-utils

1.0.39
- fix remote error handler
- remove momentjs in favor of date-fns
- create removeBoxes to help adjust forms
- fix error logging app extension

1.0.38
- convert phoneEdit to functional component
- convert layout to functional component
- convert statedvalue.stories to functional component
- convert typography to functional component

1.0.37
- show today button in datepicker

1.0.36
- fix datepicker max-, mind-date value for ranges

1.0.35
- replace flatpickr with react-datepicker

1.0.34
- add condition to remote and select field
- simple actions add conditions

1.0.33
- add css classes for display expressions styling

1.0.32
- reverse title order to first display entity name and then default display

1.0.31
- enhance column picker ux by adding numbering, mass selection and sorting features

1.0.30
- Length and size validators no longer trigger if the max or min is not defined

1.0.29
- show from and to placeholders on number ranges
- update of the values and field widths in duration edit fixed

1.0.28
- add form properties to prepare request

1.0.27
- render html escape characters in breadcrumbs

1.0.26
- fix state handling for report settings
- fix form mapping for search componentConfig
- use boolean search component for marking

1.0.25
- fix searchfilters url with query params
- add bundle packages
- fix integer input when min value is set

1.0.24
- add legacy-widget embedType
- show input on top only for remote multi selects
- fix broken disable styles on empty selects

1.0.23
- support browser plugins for phone numbers
- fix too many field callbacks being called

1.0.22
- fixate menu on multiremote select fields at bottom to improve ux

1.0.21
- support selectors in forms
- show empty fields that are children of selectors

1.0.20
- remove floating labels in favour of fixed labels

1.0.19
- change rank icon position and refactor table components
- convert HtmlEdit class component to functional component

1.0.18
- debouncer accepts value changes from outside

1.0.17
- allow duration values to be negative
- keep all values on blur

1.0.16
- add form modifier helper methods
- add report helper actions

1.0.15
- harmonize multi value select
- exclude selected values in suggestion

1.0.14
- possible to overwrite readonly form
- use distinguishable icons for ranges

1.0.13
- invalidate cache on page refresh properly
- correct dimension calculation of select menu since in some situations it does not seem to calculate the correct width/height

1.0.12
- updated fontawesome and added merge icon

1.0.11
- filter out null values on nested paths
- handle nested 'to many' relations on list forms

1.0.10
- make notification message at the end of notification center italic
- fix double scrollbar on textarea

1.0.9
- fix popper arrow positioning

1.0.8
- do not reuse same tab for different reports
- attach tether to target to prevent coverage of input fields

1.0.7
- update react-tether dependency and change Menu.js to new method
- update react-dropzone dependancy
- harmonize error list spacing within forms
- fix ignored first input click
- update range values properly
- constrain tether to parent element to prevent z-index problems inside modal
- use bolt-lightning as bolt icon
- tql autocompletion now displays localized labels in suggestions
- add hasFixedWidth prop to harmonize spacing if necessary
- show from and to placeholders on ranges
- add calendar plus and minus icons
- add specific range icons for datetime
- fix date picker modal position

1.0.6
- ace editor works again in create forms

1.0.5
- boolean search fields are now displayed as a single select box with fixed values to allow for ternary state and searching for explicit false values
- headers of right aligned table columns are right aligned now
- onBlur of date component is called with value from onChange again
- legacy actions ignore some exceptions
- fix jumping layout on firefox

1.0.4
- fix cache clearance to keep notifications session
- Navigating to detail pages through links in multi-select fields is now possible.
- Tiny millisecond values (1 - 3) are now displayed correctly.
- fix virtual form field handling
- implement theme type logic to enable loading of the widget theme
- Fixed searching in select boxes. Removed default searchOptions from Select.
- Fixed searching in select boxes. Removed default searchOptions from Select.
- onChange is now debounced for CodeEdit

1.0.3
- do not execute injected JS in html field
- no styles allowed in rendered HTML
- fix flicker in Firefox on panel hovers
- validation errors fixed for location fields
- do not load display of null entity
- use throttle for select instead of debounce to prevent flickering of dropdown
- null pointer fixed in document field formatter (resp. merge action)
- add tql mapping for type text
- improve searching for text based types
- harmonize DurationEdit spacing and refactor class based component to functional component
- show seconds, milliseconds for small read-only duration
- allow whitelisted inline css for nice tooltips
- show seconds/milliseconds only for small duration
- Display overflowing hours in durations
- fix onError of customAction
- change toaster type of aborted action handler
- change toaster to fixed positioning to guarantee visibility on scroll
- Use app locale automatically for all (REST) requests
- render modal action component with right locale
- Using the advanced search in multi-remote fields no longer ignores the current selection.
- Code editors used to write TQL can now be configured in the backend to assume some entity model as the given base, allowing only writing the 'where'- and 'order by'-parts of a query.
- increase table style specificities to prevent them from being overwritten inside widgets

1.0.2
- register more icons
- open downloadable file in new tab to avoid errors
- remove framer-motion dependency for burger button and create pure css version instead
- fix sticky popover on table
- fix UploadInput.js button titles and refactor components
- add notification support for anonymous users

1.0.1
- harmonize popover background color and spacing
- register icons
- register more icons
- change notification title and refactor NotificationCenter
- create env utils
- add generic nice2 fetch wrapper
- add useDidUpdate helper hook
- add useApp helper hook

1.0.0
- fix websocket reconnection
- refactor initial notification
- fix confirm handler
- calculate drop position before or after element
- provide sanitize function for html
- render sanitized html only
- don't resize column after mouse release
- Remote fields and select boxes now only load active relation values.
- add keyField to model transformer
- Reset legacy overwrite of link color inside toasters
- Fix popper menu disappearing behind bm-menu elements
- Lighten scrollbar color
- Fix popper menu closing immediately when clicking an action
- Add minimum box width property to Layout Container to better control column width
- add open in new tab to document compact
- only load huge global styles once per app
- Added a preview image on hovering over download icons.
- prevent position from switching between after/before
- generic resize util
- refactor notification
- performance improvements
- fix drag and drop
- create link button
- create link button
- Add responsivity to all buttons so that the label disappears on smaller screens and only the icon is displayed
- Fix datepickr disappearing behind modal
- show textarea bigger for better distinction
- table can be refreshed
- register more icons
- increase z-index of popper menu to prevent it slipping behind modal
- show button hover title and pointer cursor
- user agent detection
- improve performance on Safari
- fix missing popover in single values and overflowing in firefox
- add tooltip for remote fields
- Add Ace editor to code fields.
- add usePrevious helper hook
- cleanup flatpickr components
- fix jumping datepicker on Safari
- debounce autosize on textarea
- harmonize fontawesome icon spacing within table data cell
- register more icons
- improve dropdown performance for selects on Safari
- fix performance bottleneck on linked sleect values
- adjust z-index of modal to prevent legacy actions falling behind it
- add auto completion for TQL in code fields.
- prevent default behaviour on shortcuts
- adjust min width of location edit postcode and refactor component
- refactor largeSelectionHandler
- enable select dropdown indicators to be aligned at bottom of multiselect fields
- tql builder support login type
- add title to custom action response
- throttle autosize to fix performance issues
- change toaster behavior to stay on hover and refactor ToasterDisplay
- remove X-Enable-Notifications header

0.4.0
- Added language upgrade form (in DB refactoring view)

0.3.1
- Fixed generating SQL and changesets in prod env

0.3.0
- Added simple log view
- Added simple model validation view
- Added simple SQL log view
- Prevent complete reload when changing page

0.2.0
- New input param `baseRoute`

0.1.0
- Initial release of Developer Console (DevCon) with simple DB refactoring
