import {addCheckEvent, setCurrent, setResult, setSelected, setTotal, startValidation} from './actions'
import reducer from './reducer'

describe('devcon', () => {
  describe('modules', () => {
    describe('modelvalidation', () => {
      describe('reducer', () => {
        describe('startValidation', () => {
          test('should set running true and take initial state', () => {
            const initialState = {
              state: {
                running: false,
                result: null
              },
              changelog: null
            }

            const expectedState = {
              checkEvents: {},
              state: {
                running: true,
                currentIndex: null,
                currentName: null,
                total: null,
                result: null
              },
              selection: new Set(),
              sql: null,
              changelog: null
            }
            const state = reducer(initialState, startValidation())
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('setTotal', () => {
          test('should set total', () => {
            const initialState = {
              state: {
                running: false,
                total: null
              },
              changelog: null
            }

            const expectedState = {
              state: {
                running: false,
                total: 3
              },
              changelog: null
            }
            const state = reducer(initialState, setTotal(3))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('setCurrent', () => {
          test('should set running, total, currentIndex and currentName', () => {
            const initialState = {
              state: {
                running: false,
                total: null,
                currentIndex: null,
                currentName: null
              },
              changelog: null
            }

            const expectedState = {
              state: {
                running: true,
                total: 3,
                currentIndex: 0,
                currentName: 'Entity_template'
              },
              changelog: null
            }
            const state = reducer(initialState, setCurrent(3, 0, 'Entity_template'))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('setResult', () => {
          test('should unset running and set result', () => {
            const initialState = {
              state: {
                running: true,
                total: 3,
                currentIndex: 0,
                currentName: 'Entity_template',
                result: null
              },
              changelog: null
            }

            const expectedState = {
              state: {
                running: false,
                total: 3,
                currentIndex: 0,
                currentName: 'Entity_template',
                result: 'SUCCESS'
              },
              changelog: null
            }
            const state = reducer(initialState, setResult('SUCCESS'))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('addCheckEvent', () => {
          test('should add check event for new type', () => {
            const initialState = {
              checkEvents: {}
            }

            const expectedState = {
              checkEvents: {
                a: [{id: '1', type: 'a', label: 'label'}]
              }
            }
            const state = reducer(initialState, addCheckEvent('1', 'a', 'label'))
            expect(state).to.deep.eql(expectedState)
          })

          test('should add check event for existing type', () => {
            const initialState = {
              checkEvents: {
                a: [{id: '1', type: 'a', label: 'label'}],
                b: [{id: '3', type: 'b', label: 'label'}]
              }
            }

            const expectedState = {
              checkEvents: {
                a: [
                  {id: '1', type: 'a', label: 'label'},
                  {id: '2', type: 'a', label: 'label-c'}
                ],
                b: [{id: '3', type: 'b', label: 'label'}]
              }
            }
            const state = reducer(initialState, addCheckEvent('2', 'a', 'label-c'))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('setSelected', () => {
          test('should add selected ids to empty selection', () => {
            const initialState = {
              selection: new Set()
            }

            const expectedState = {
              selection: new Set(['1', '2'])
            }
            const state = reducer(initialState, setSelected(['1', '2'], true))
            expect(state).to.deep.eql(expectedState)
          })

          test('should add selected ids to existing selection', () => {
            const initialState = {
              selection: new Set(['1'])
            }

            const expectedState = {
              selection: new Set(['1', '2'])
            }
            const state = reducer(initialState, setSelected(['1', '2'], true))
            expect(state).to.deep.eql(expectedState)
          })

          test('should remove selected ids to existing selection', () => {
            const initialState = {
              selection: new Set(['1', '2'])
            }

            const expectedState = {
              selection: new Set(['2'])
            }
            const state = reducer(initialState, setSelected(['1'], false))
            expect(state).to.deep.eql(expectedState)
          })

          test('should remove all ids from existing selection', () => {
            const initialState = {
              selection: new Set(['1', '2'])
            }

            const expectedState = {
              selection: new Set()
            }
            const state = reducer(initialState, setSelected(['1', '2'], false))
            expect(state).to.deep.eql(expectedState)
          })
        })
      })
    })
  })
})
