import {executeDbRefactoring, executeLanguageUpgrade, setFragments, setFragmentSelected, unsetRunning} from './actions'
import reducer from './reducer'

describe('devcon', () => {
  describe('modules', () => {
    describe('dbrefactoring', () => {
      describe('reducer', () => {
        describe('setFragments', () => {
          test('should set fragments and selected fragments', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [],
                selectedFragments: [],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }
            const fragments = [
              {id: '1', selected: true},
              {id: '2', selected: false}
            ]

            const expectedState = {
              dbRefactoring: {
                fragments,
                selectedFragments: ['1'],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }
            const state = reducer(initialState, setFragments(fragments))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('setFragmentSelected', () => {
          test('should select fragment', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [
                  {id: '1', selected: true},
                  {id: '2', selected: false}
                ],
                selectedFragments: ['1'],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [
                  {id: '1', selected: true},
                  {id: '2', selected: false}
                ],
                selectedFragments: ['1', '2'],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }
            const state = reducer(initialState, setFragmentSelected('2', true))
            expect(state).to.deep.eql(expectedState)
          })

          test('should deselect fragment', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [
                  {id: '1', selected: true},
                  {id: '2', selected: false}
                ],
                selectedFragments: ['1', '2'],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [
                  {id: '1', selected: true},
                  {id: '2', selected: false}
                ],
                selectedFragments: ['1'],
                ignoreErrors: false
              },
              languageUpgrade: {}
            }
            const state = reducer(initialState, setFragmentSelected('2', false))
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('executeDbRefactoring', () => {
          test('should set running', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [],
                running: false
              },
              languageUpgrade: {
                running: false
              }
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [],
                running: true
              },
              languageUpgrade: {
                running: false
              }
            }
            const state = reducer(initialState, executeDbRefactoring())
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('executeLanguageUpgrade', () => {
          test('should set running', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [],
                running: false
              },
              languageUpgrade: {
                running: false
              }
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [],
                running: false
              },
              languageUpgrade: {
                running: true
              }
            }
            const state = reducer(initialState, executeLanguageUpgrade())
            expect(state).to.deep.eql(expectedState)
          })
        })

        describe('unsetRunning', () => {
          test('should unset running for dbRefactoring', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [],
                running: true
              },
              languageUpgrade: {
                running: true
              }
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [],
                running: false
              },
              languageUpgrade: {
                running: true
              }
            }
            const state = reducer(initialState, unsetRunning('dbRefactoring'))
            expect(state).to.deep.eql(expectedState)
          })

          test('should unsset running for languageUpgrade', () => {
            const initialState = {
              dbRefactoring: {
                fragments: [],
                running: true
              },
              languageUpgrade: {
                running: true
              }
            }

            const expectedState = {
              dbRefactoring: {
                fragments: [],
                running: true
              },
              languageUpgrade: {
                running: false
              }
            }
            const state = reducer(initialState, unsetRunning('languageUpgrade'))
            expect(state).to.deep.eql(expectedState)
          })
        })
      })
    })
  })
})
