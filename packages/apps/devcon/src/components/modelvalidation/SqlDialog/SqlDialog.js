import PropTypes from 'prop-types'
import {Button, EditableValue, StatedValue} from 'tocco-ui'

import {StyledButtons} from './StyledComponents'

const SqlDialog = ({sql, setSql, executeSql, close}) => {
  const handleExecute = () => {
    executeSql(close)
  }

  return (
    <div>
      <StatedValue label="SQL Statement">
        <EditableValue type="text" value={sql} events={{onChange: setSql}} />
      </StatedValue>
      <StyledButtons>
        <Button look="raised" onClick={handleExecute} label="Run Statements" />
      </StyledButtons>
    </div>
  )
}

SqlDialog.propTypes = {
  sql: PropTypes.string,
  setSql: PropTypes.func.isRequired,
  executeSql: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
}

export default SqlDialog
