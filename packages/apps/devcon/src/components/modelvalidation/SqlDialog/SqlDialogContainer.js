import {connect} from 'react-redux'

import {executeSql, setSql} from '../../../modules/modelvalidation/actions'

import SqlDialog from './SqlDialog'

const mapStateToProps = state => ({
  sql: state.modelValidation.sql
})

const mapActionCreators = {
  executeSql,
  setSql
}

export default connect(mapStateToProps, mapActionCreators)(SqlDialog)
