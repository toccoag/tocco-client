import PropTypes from 'prop-types'
import {Button} from 'tocco-ui'
import {js} from 'tocco-util'

import {StyledButtons, StyledChangelogCode} from './StyledComponents'

// remove xml declaration and `databaseChangeLog` element
const getChangesets = changelog => changelog.split('\n').slice(2, -1).join('\n')

const ChangelogDialog = ({changelog}) => {
  const copyChangelog = () => {
    js.copyToClipboard(changelog)
  }

  const copyChangesets = () => {
    js.copyToClipboard(getChangesets(changelog))
  }

  return (
    <>
      <StyledChangelogCode>{changelog}</StyledChangelogCode>
      <StyledButtons>
        <Button look="raised" onClick={copyChangesets} label="Copy only Changesets to Clipboard" />
        <Button look="raised" ink="primary" onClick={copyChangelog} label="Copy Changelog to Clipboard" />
      </StyledButtons>
    </>
  )
}

ChangelogDialog.propTypes = {
  changelog: PropTypes.string
}

export default ChangelogDialog
