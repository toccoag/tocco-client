import styled from 'styled-components'
import {scale, StyledButton, StyledCode} from 'tocco-ui'

export const StyledButtons = styled.div`
  bottom: 0;
  padding: ${scale.space(0)} 0;
  display: flex;
  justify-content: flex-end;

  ${/* sc-selector */ StyledButton}:last-child {
    margin-right: 0;
  }
`

export const StyledChangelogCode = styled(StyledCode)`
  && {
    max-width: calc(100% - 2 * ${scale.space(0.3)}); /* subtract side paddings to prevent cut off inside modal */
    overflow: auto;
    padding: ${scale.space(0.3)};
    margin-bottom: ${scale.space(0.3)};
    text-overflow: unset;
  }
`
