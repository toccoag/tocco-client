import {createHref, goBack} from './routing'

describe('admin', () => {
  describe('utils', () => {
    describe('routing', () => {
      describe('createHref', () => {
        test('should create href without basename', () => {
          const href = createHref({pathname: 'e/User/list'}, {})
          expect(href).to.eql('/e/User/list')
        })

        test('should create href with basename', () => {
          const href = createHref({pathname: 'e/User/list'}, {basename: '/tocco'})
          expect(href).to.eql('/tocco/e/User/list')
        })
      })

      describe('goBack', () => {
        test('should remove the right amouts of paths', () => {
          const url = '/e/User/620/detail'
          const result = goBack(url, 2)

          const expected = '/e/User'

          expect(result).to.eql(expected)
        })

        test('should ignore slashes at the end of the url', () => {
          const url = '/e/User/620/detail/'
          const result = goBack(url, 2)

          const expected = '/e/User'

          expect(result).to.eql(expected)
        })
      })
    })
  })
})
