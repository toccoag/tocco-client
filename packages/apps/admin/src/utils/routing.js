import {createPath} from 'react-router-dom'

const joinPaths = paths => paths.join('/').replace(/\/\/+/g, '/')

// copied from here: https://github.com/remix-run/react-router/blob/5d66dbdbc8edf1d9c3a4d9c9d84073d046b5785b/packages/react-router/lib/hooks.tsx#L57
export const createHref = ({pathname, search, hash}, {basename = ''}) => {
  let joinedPathname = pathname
  if (basename !== '/') {
    joinedPathname = pathname === '/' ? basename : joinPaths([basename, pathname])
  }

  return createPath({pathname: joinedPathname, search, hash})
}

export const goBack = (url, amount = 1) => {
  let normalizedUrl = url.replace(/\/$/, '')

  for (let i = 0; i < amount; i++) {
    normalizedUrl = normalizedUrl.substring(0, normalizedUrl.lastIndexOf('/'))
  }

  return normalizedUrl
}
