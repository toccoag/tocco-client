import Cookies from 'js-cookie'
import {all, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {login, notification, rest, appFactory} from 'tocco-app-extensions'
import {AUTO_LOGIN_COOKIE, sagasUtils} from 'tocco-sso-login/src/main'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('admin', () => {
  describe('session', () => {
    describe('sagas', () => {
      describe('root saga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([
              takeLatest(actions.LOGIN_SUCCESSFUL, sagas.loginSuccessful),
              takeLatest(actions.DO_LOGOUT, sagas.logout),
              takeLatest(actions.LOAD_PRINCIPAL, sagas.loadPrincipal),
              takeLatest(actions.LOAD_BUSINESS_UNITS, sagas.loadBusinessUnits),
              takeLatest(actions.CHANGE_BUSINESS_UNIT, sagas.changeBusinessUnitId),
              takeLatest(actions.CHECK_SSO_AVAILABLE, sagas.checkSsoAvailable),
              takeLatest(actions.GET_ROUTING_USER_CONFIRMATION, sagas.getRoutingUserConfirmation),
              takeLatest(actions.LOAD_HAS_OUTLOOK_ADD_IN_MODULE, sagas.loadHasOutlookAddInModule)
            ])
          )
          expect(generator.next().done).to.equal(true)
        })
      })

      describe('loginSuccessful', () => {
        test('should log in successfully', () => {
          const sessionTimeoutInMinutes = 30
          return expectSaga(sagas.loginSuccessful, actions.loginSuccessful(sessionTimeoutInMinutes))
            .provide([[matchers.call.fn(sagas.setUserLocale)], [matchers.call.fn(sagas.sessionHeartbeat)]])
            .call(sagas.setUserLocale)
            .put(login.setAdminAllowed(undefined))
            .put(login.setLoggedIn(true))
            .put(notification.connectSocket())
            .call(sagas.sessionHeartbeat, sessionTimeoutInMinutes)
            .run()
        })
      })

      describe('sessionHeartbeat', () => {
        test('should set flags and call itself', () => {
          const sessionTimeoutInMinutes = 30
          const sessionResponse = {
            success: true,
            adminAllowed: true
          }

          // after the half time of the session timeout
          const expectedHeartbeatDelayInMilliseconds = 15 * 60 * 1000

          return expectSaga(sagas.sessionHeartbeat, sessionTimeoutInMinutes)
            .provide([
              [matchers.call.fn(login.doSessionRequest), sessionResponse],
              [matchers.call.fn(sagas.sessionHeartbeat)],
              [matchers.call.fn(sagas.delayByTimeout)]
            ])
            .put(login.setLoggedIn(true))
            .put(login.setAdminAllowed(true))
            .call(sagas.sessionHeartbeat, sessionTimeoutInMinutes)
            .call(sagas.delayByTimeout, expectedHeartbeatDelayInMilliseconds)
            .run()
        })
      })

      describe('logout', () => {
        test('should show pending changes modal before logout', () => {
          const sessionState = {
            pendingChanges: true
          }
          return expectSaga(sagas.logout)
            .provide([
              [matchers.select(sagas.sessionSelector), sessionState],
              [matchers.call.fn(sagas.promptConfirm), 'yes'],
              [matchers.call.fn(sagas.doLogoutRequest)],
              [matchers.call.fn(sagas.redirectToBaseRoute)]
            ])
            .call(sagas.promptConfirm, 'client.admin.confirmTouchedFormLogout')
            .call(Cookies.remove, AUTO_LOGIN_COOKIE)
            .call(sagas.doLogoutRequest)
            .put(login.setLoggedIn(false))
            .put(notification.closeSocket())
            .call(sagas.redirectToBaseRoute)
            .run()
        })

        test('should not logout when user stays on dirty page', () => {
          const sessionState = {
            pendingChanges: true
          }
          return expectSaga(sagas.logout)
            .provide([
              [matchers.select(sagas.sessionSelector), sessionState],
              [matchers.call.fn(sagas.promptConfirm), 'no']
            ])
            .call(sagas.promptConfirm, 'client.admin.confirmTouchedFormLogout')
            .not.call(Cookies.remove, AUTO_LOGIN_COOKIE)
            .not.call(sagas.doLogoutRequest)
            .not.put(login.setLoggedIn(false))
            .not.put(notification.closeSocket())
            .not.call(sagas.redirectToBaseRoute)
            .run()
        })

        test('should not show pending changes modal when page is not dirty', () => {
          const sessionState = {
            pendingChanges: false
          }
          return expectSaga(sagas.logout)
            .provide([
              [matchers.select(sagas.sessionSelector), sessionState],
              [matchers.call.fn(sagas.doLogoutRequest)],
              [matchers.call.fn(sagas.redirectToBaseRoute)]
            ])
            .not.call(sagas.promptConfirm, 'client.admin.confirmTouchedFormLogout')
            .call(Cookies.remove, AUTO_LOGIN_COOKIE)
            .call(sagas.doLogoutRequest)
            .put(login.setLoggedIn(false))
            .put(notification.closeSocket())
            .call(sagas.redirectToBaseRoute)
            .run()
        })
      })

      describe('loadPrincipal', () => {
        test('should fetch and save principal', () => {
          const response = {username: 'tester', currentBusinessUnit: 'test', inWrongBusinessUnit: false}
          return expectSaga(sagas.loadPrincipal)
            .provide([[matchers.call.fn(rest.fetchPrincipal), response]])
            .put(actions.setUsername('tester'))
            .put(actions.setCurrentBusinessUnit('test'))
            .put(actions.setInWrongBusinessUnit(false))
            .run()
        })
      })

      describe('loadBusinessUnits', () => {
        test('should fetch and save business units', () => {
          return expectSaga(sagas.loadBusinessUnits)
            .provide([
              [matchers.call(rest.requestSaga, 'principals/businessunits'), {body: {data: ['test1', 'test2']}}]
            ])
            .put(actions.setBusinessUnits(['test1', 'test2']))
            .run()
        })
      })

      describe('changeBusinessUnitId', () => {
        test('should save given business unit', () => {
          const sessionState = {
            username: 'user'
          }
          return expectSaga(sagas.changeBusinessUnitId, actions.changeBusinessUnit('test1'))
            .provide([
              [matchers.select(sagas.sessionSelector), sessionState],
              [
                matchers.call(rest.requestSaga, 'principals/user/businessunit', {
                  method: 'PUT',
                  body: {businessUnit: 'test1'}
                })
              ]
            ])
            .call(appFactory.reinitialiseCache)
            .run()
        })
      })

      describe('checkSsoAvailable', () => {
        test('check if sso is available', () => {
          return expectSaga(sagas.checkSsoAvailable)
            .provide([[matchers.call(sagasUtils.checkSsoAvailable), 'google']])
            .put(actions.setSsoAvailable('google'))
            .run()
        })
      })

      describe('getRoutingUserConfirmation', () => {
        test('should call callback with true on user confirmation', () => {
          const message = 'sure?'
          const callback = sinon.spy()
          return expectSaga(sagas.getRoutingUserConfirmation, actions.getRoutingUserConfirmation(message, callback))
            .provide([[matchers.call(sagas.promptConfirm, message), 'yes']])
            .call(callback, true)
            .run()
        })

        test('should call callback with false on cancelles user confirmation', () => {
          const message = 'sure?'
          const callback = sinon.spy()
          return expectSaga(sagas.getRoutingUserConfirmation, actions.getRoutingUserConfirmation(message, callback))
            .provide([[matchers.call(sagas.promptConfirm, message), 'no']])
            .call(callback, false)
            .run()
        })
      })

      describe('loadHasOutlookAddInModule', () => {
        test('should return true if officeintegration is installed', () => {
          const modules = ['nice.optional.address', 'nice.optional.officeintegration']
          return expectSaga(sagas.loadHasOutlookAddInModule)
            .provide([[matchers.call(rest.fetchModules), modules]])
            .put(actions.setHasOutlookAddInModule(true))
            .run()
        })

        test('should return false if officeintegration is not installed', () => {
          const modules = ['nice.optional.address']
          return expectSaga(sagas.loadHasOutlookAddInModule)
            .provide([[matchers.call(rest.fetchModules), modules]])
            .put(actions.setHasOutlookAddInModule(false))
            .run()
        })
      })
    })
  })
})
