import Cookies from 'js-cookie'
import {channel} from 'redux-saga'
import {all, call, delay, put, select, take, takeLatest} from 'redux-saga/effects'
import {login, notification, rest, appFactory} from 'tocco-app-extensions'
import {AUTO_LOGIN_COOKIE, sagasUtils} from 'tocco-sso-login/src/main'
import {PendingChangesModal} from 'tocco-ui'
import {intl} from 'tocco-util'

import * as actions from './actions'

export const sessionSelector = state => state.session
export const inputSelector = state => state.input
export const appSelector = state => state.app

export function* sessionHeartbeat(sessionTimeoutInMinutes) {
  const sessionHeartbeatTimeoutInMs = (sessionTimeoutInMinutes / 2) * 60 * 1000
  const {success, adminAllowed} = yield call(login.doSessionRequest)
  yield put(login.setLoggedIn(success))
  yield put(login.setAdminAllowed(adminAllowed))
  yield call(delayByTimeout, sessionHeartbeatTimeoutInMs)
  yield call(sessionHeartbeat, sessionTimeoutInMinutes)
}

/**
 * this is only here because 'yield delay(timeout)' is a pain to be called when testing
 * see https://github.com/jfairbank/redux-saga-test-plan/issues/257
 * @param timeout the time to wait
 */
export function* delayByTimeout(timeout) {
  yield delay(timeout)
}

export function* doLogoutRequest() {
  return yield call(login.doRequest, 'logout', {method: 'POST'})
}

export function* redirectToBaseRoute() {
  const {baseRoute} = yield select(inputSelector)
  window.location.href = baseRoute || '/'
}

export function* setUserLocale() {
  const {textResourceModules} = yield select(appSelector)
  const locale = yield call(intl.getUserLocale)
  const changeLocaleAction = yield call(intl.getChangeLocaleAction, textResourceModules, locale)
  yield put(changeLocaleAction)
}

export function* loginSuccessful({payload}) {
  const {sessionTimeout} = payload

  yield call(setUserLocale)

  /**
   * `adminAllowed` will be set explicitly to true/false inside the sessionHeartbeat.
   * Nevertheless it has to be reset toghether with `loggedIn=true`.
   * With `adminAllowed=undefined` an empty page is shown instead
   * "no roles" error message while fetching the session.
   */
  yield put(login.setAdminAllowed(undefined))
  yield put(login.setLoggedIn(true))
  yield put(notification.connectSocket())
  yield call(sessionHeartbeat, sessionTimeout)
}

export function* promptConfirm(message) {
  const answerChannel = yield call(channel)

  yield put(
    notification.modal(
      'navigation-pending-changes',
      'client.admin.confirmTouchedFormTitle',
      message,
      ({close}) => {
        const onYes = () => {
          close()
          answerChannel.put('yes')
        }
        const onNo = () => {
          close()
          answerChannel.put('no')
        }
        return <PendingChangesModal onYes={onYes} onNo={onNo} />
      },
      true,
      () => answerChannel.put('no')
    )
  )
  return yield take(answerChannel)
}

export function* getRoutingUserConfirmation({payload}) {
  const {message, confirmCallback} = payload
  const response = yield call(promptConfirm, message)
  if (response === 'yes') {
    yield call(confirmCallback, true)
  } else {
    yield call(confirmCallback, false)
  }
}

export function* logout() {
  const {pendingChanges} = yield select(sessionSelector)
  if (pendingChanges) {
    const response = yield call(promptConfirm, 'client.admin.confirmTouchedFormLogout')
    if (response !== 'yes') {
      return
    }
  }

  yield call(Cookies.remove, AUTO_LOGIN_COOKIE)
  yield call(doLogoutRequest)
  yield put(login.setLoggedIn(false))
  yield put(notification.closeSocket())
  yield call(redirectToBaseRoute)
}

export function* loadPrincipal() {
  const {username, currentBusinessUnit, inWrongBusinessUnit} = yield call(rest.fetchPrincipal)
  yield put(actions.setUsername(username))
  yield put(actions.setCurrentBusinessUnit(currentBusinessUnit))
  yield put(actions.setInWrongBusinessUnit(inWrongBusinessUnit))
}

export function* loadBusinessUnits() {
  const bUs = yield call(rest.requestSaga, 'principals/businessunits')
  yield put(actions.setBusinessUnits(bUs.body.data))
}

export function* changeBusinessUnitId({payload: {businessUnitId}}) {
  const {username} = yield select(sessionSelector)
  const resource = `principals/${username}/businessunit`
  yield call(rest.requestSaga, resource, {method: 'PUT', body: {businessUnit: businessUnitId}})
  yield call(appFactory.reinitialiseCache)
  location.reload()
}

export function* checkSsoAvailable() {
  const ssoAvailable = yield call(sagasUtils.checkSsoAvailable)
  yield put(actions.setSsoAvailable(ssoAvailable))
}

export function* loadHasOutlookAddInModule() {
  const modules = yield call(rest.fetchModules)
  const hasOutlookAddInModule = modules.includes('nice.optional.officeintegration')
  yield put(actions.setHasOutlookAddInModule(hasOutlookAddInModule))
}

export default function* mainSagas() {
  yield all([
    takeLatest(actions.LOGIN_SUCCESSFUL, loginSuccessful),
    takeLatest(actions.DO_LOGOUT, logout),
    takeLatest(actions.LOAD_PRINCIPAL, loadPrincipal),
    takeLatest(actions.LOAD_BUSINESS_UNITS, loadBusinessUnits),
    takeLatest(actions.CHANGE_BUSINESS_UNIT, changeBusinessUnitId),
    takeLatest(actions.CHECK_SSO_AVAILABLE, checkSsoAvailable),
    takeLatest(actions.GET_ROUTING_USER_CONFIRMATION, getRoutingUserConfirmation),
    takeLatest(actions.LOAD_HAS_OUTLOOK_ADD_IN_MODULE, loadHasOutlookAddInModule)
  ])
}
