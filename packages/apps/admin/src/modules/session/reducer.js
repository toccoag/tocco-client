import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_USERNAME]: reducerUtil.singleTransferReducer('username'),
  [actions.SET_CURRENT_BUSINESS_UNIT]: reducerUtil.singleTransferReducer('currentBusinessUnit'),
  [actions.SET_BUSINESS_UNITS]: reducerUtil.singleTransferReducer('businessUnits'),
  [actions.SET_SSO_AVAILABLE]: reducerUtil.singleTransferReducer('ssoAvailable'),
  [actions.SET_PENDING_CHANGES]: reducerUtil.singleTransferReducer('pendingChanges'),
  [actions.SET_HAS_OUTLOOK_ADD_IN_MODULE]: reducerUtil.singleTransferReducer('hasOutlookAddInModule'),
  [actions.SET_IN_WRONG_BUSINESS_UNIT]: reducerUtil.singleTransferReducer('inWrongBusinessUnit')
}

const initialState = {
  username: '',
  currentBusinessUnit: {
    label: '',
    id: ''
  },
  businessUnits: [],
  ssoAvailable: false,
  pendingChanges: false,
  hasOutlookAddInModule: false,
  inWrongBusinessUnit: undefined
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
