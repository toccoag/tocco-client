import {FormattedMessage} from 'react-intl'
import {channel} from 'redux-saga'
import {takeLatest, call, all, put, take} from 'redux-saga/effects'
import {appFactory, rest, notification} from 'tocco-app-extensions'
import {cache} from 'tocco-util'

import * as actions from './actions'
import {transformValues} from './preferences'

const themePreferencesKey = 'admin.theme'

export function* loadSettingsAndPreferences() {
  const settings = yield call(rest.fetchServerSettings)
  yield put(actions.setServerSettings(settings))

  const adminPreferences = yield call(rest.fetchUserPreferences, 'admin.*')
  const transformedAdminPreferences = yield call(transformValues, adminPreferences)

  const adminTreePreferences = yield call(rest.fetchUserPreferences, 'admintree.*')
  const transformedAdminTreePreferences = yield call(transformValues, adminTreePreferences)

  yield put(actions.setUserPreferences({...transformedAdminTreePreferences, ...transformedAdminPreferences}))
}

export function* saveUserPreferences({payload: {preferences}}) {
  yield call(rest.savePreferences, preferences)
}

export function* setThemeFromPreferences({payload: {userPreferences}}) {
  const themePreference = userPreferences[themePreferencesKey]
  if (themePreference) {
    yield call(cache.addLongTerm, 'admin', 'theme', themePreference)
    yield put(appFactory.setThemeType(themePreference))
  } else {
    yield call(cache.removeLongTerm, 'admin', 'theme')
    yield put(appFactory.initThemeType())
  }
}

export function* resetUserPreferences() {
  const answerChannel = yield call(channel)
  const onYes = () => answerChannel.put(true)
  const onCancel = () => answerChannel.put(false)

  yield put(
    notification.confirm(
      <FormattedMessage id="client.admin.menu.resetPreferences.title" />,
      <FormattedMessage id="client.admin.menu.resetPreferences.question" />,
      <FormattedMessage id="client.common.ok" />,
      <FormattedMessage id="client.common.cancel" />,
      onYes,
      onCancel
    )
  )
  const confirmed = yield take(answerChannel)
  if (confirmed) {
    yield call(rest.resetAllPreferences)
    yield call(cache.removeLongTerm, 'admin', 'theme')
    yield put(appFactory.initThemeType())
    yield put(
      notification.toaster({
        type: 'success',
        title: 'client.admin.menu.resetPreferences.success'
      })
    )
  }
}

export default function* mainSagas() {
  yield all([
    takeLatest(actions.LOAD_SETTINGS_AND_PREFERENCES, loadSettingsAndPreferences),
    takeLatest(actions.SAVE_USER_PREFERENCES, saveUserPreferences),
    takeLatest(actions.SET_USER_PREFERENCES, setThemeFromPreferences),
    takeLatest(actions.RESET_USER_PREFERENCES, resetUserPreferences)
  ])
}
