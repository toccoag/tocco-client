import {channel} from 'redux-saga'
import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {appFactory, rest, notification} from 'tocco-app-extensions'
import {cache} from 'tocco-util'

import * as actions from './actions'
import {transformValues} from './preferences'
import mainSaga, * as sagas from './sagas'

describe('admin', () => {
  describe('modules', () => {
    describe('preferences', () => {
      describe('sagas', () => {
        describe('main saga', () => {
          test('should fork child sagas', () => {
            const generator = mainSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_SETTINGS_AND_PREFERENCES, sagas.loadSettingsAndPreferences),
                takeLatest(actions.SAVE_USER_PREFERENCES, sagas.saveUserPreferences),
                takeLatest(actions.SET_USER_PREFERENCES, sagas.setThemeFromPreferences),
                takeLatest(actions.RESET_USER_PREFERENCES, sagas.resetUserPreferences)
              ])
            )
            expect(generator.next().done).to.equal(true)
          })
        })

        describe('loadSettingsAndPreferences', () => {
          test('should load settings and preferences', () => {
            const serverSettings = {
              captchaKey: 'xxx',
              niceRevision: 'abcd',
              niceVersion: '3.0'
            }
            const adminPref = {
              'admin.activeMenu': 'main#settings',
              'admin.detail.relationViewCollapsed': 'false'
            }
            const admintreePref = {
              'admintree.address.collapsed': 'false',
              'admintree.groupware.collapsed': 'true',
              'admintree.settings.system.business_unit.collapsed': 'true'
            }

            const result = {
              'admin.activeMenu': 'main#settings',
              'admin.detail.relationViewCollapsed': false,
              'admintree.address.collapsed': false,
              'admintree.groupware.collapsed': true,
              'admintree.settings.system.business_unit.collapsed': true
            }

            return expectSaga(sagas.loadSettingsAndPreferences, actions.loadSettingsAndPreferences)
              .provide([
                [matchers.call(rest.fetchServerSettings), serverSettings],
                [matchers.call(rest.fetchUserPreferences, 'admin.*'), adminPref],
                [matchers.call(rest.fetchUserPreferences, 'admintree.*'), admintreePref]
              ])
              .put(actions.setServerSettings(serverSettings))
              .call(transformValues, adminPref)
              .call(transformValues, admintreePref)
              .put(actions.setUserPreferences(result))
              .run()
          })
        })

        describe('saveUserPreferences', () => {
          test('should save key-value preferences', () => {
            const key = 'admintree.address.collapsed'
            const value = false
            return expectSaga(sagas.saveUserPreferences, actions.saveUserPreferences({[key]: value}))
              .provide([[matchers.call(rest.savePreferences, {'admintree.address.collapsed': false})]])
              .run()
          })
        })

        describe('setThemeFromPreferences', () => {
          test('should use theme type from preferences', () => {
            const userPreferenceAction = actions.setUserPreferences({'admin.theme': 'dark'})

            return expectSaga(sagas.setThemeFromPreferences, userPreferenceAction)
              .call(cache.addLongTerm, 'admin', 'theme', 'dark')
              .put(appFactory.setThemeType('dark'))
              .run()
          })

          test('should reset theme type when preferences are not set', () => {
            const userPreferenceAction = actions.setUserPreferences({})
            return expectSaga(sagas.setThemeFromPreferences, userPreferenceAction)
              .not.call.fn(cache.addLongTerm)
              .not.put(appFactory.setThemeType('dark'))
              .call(cache.removeLongTerm, 'admin', 'theme')
              .put(appFactory.initThemeType())
              .run()
          })
        })

        describe('resetUserPreferences', () => {
          test('should reset user preferences when confirmed', () => {
            return expectSaga(sagas.resetUserPreferences)
              .provide([
                [channel, {}],
                {
                  take() {
                    return true
                  }
                },
                [matchers.call(rest.resetAllPreferences), {}]
              ])
              .put.like({
                action: {
                  type: 'notification/CONFIRM'
                }
              })
              .call(rest.resetAllPreferences)
              .put(
                notification.toaster({
                  type: 'success',
                  title: 'client.admin.menu.resetPreferences.success'
                })
              )
              .run()
          })
          test('should do nothing when not confirmed', () => {
            return expectSaga(sagas.resetUserPreferences)
              .provide([
                [channel, {}],
                {
                  take() {
                    return false
                  }
                },
                [matchers.call(rest.resetAllPreferences), {}]
              ])
              .put.like({
                action: {
                  type: 'notification/CONFIRM'
                }
              })
              .not.call(rest.resetAllPreferences)
              .not.put(
                notification.toaster({
                  type: 'success',
                  title: 'client.admin.menu.resetPreferences.success'
                })
              )
              .run()
          })
        })
      })
    })
  })
})
