import PropTypes from 'prop-types'
import {useState, useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import ToccoLogin from 'tocco-login/src/main'
import SsoLogin from 'tocco-sso-login/src/main'

import ToccoSlogan from '../../assets/tocco_white.svg'

import {
  StyledSsoMsg,
  StyledSpanLogin,
  StyledLogin,
  StyledMobileSloganImg,
  StyledSloganImg,
  StyledLoginWrapper,
  StyledHeadingLogin,
  GlobalBodyStyle
} from './StyledComponents'

const Login = ({ssoAvailable, loginSuccessful, checkSsoAvailable}) => {
  const [showRegistrationText, setShowRegistrationText] = useState(false)

  useEffect(() => {
    checkSsoAvailable()
  }, [checkSsoAvailable])

  const loginSuccess = ({timeout}) => {
    loginSuccessful(timeout)
  }

  const ssoLoginCompleted = ({successful, registration}) => {
    if (!successful && registration) {
      setShowRegistrationText(true)
    }
  }

  const SsoLoginPart = () => (
    <>
      <SsoLogin ssoLoginEndpoint="/sso" loginCompleted={ssoLoginCompleted} autoLogin />
      {showRegistrationText && (
        <StyledSsoMsg breakWords={true}>
          <FormattedMessage id="client.sso-login.registration" />
        </StyledSsoMsg>
      )}
      <StyledSpanLogin>
        <FormattedMessage id="client.admin.loginChoice" />
      </StyledSpanLogin>
    </>
  )

  return (
    <>
      <GlobalBodyStyle />
      <StyledLogin>
        <StyledMobileSloganImg src={ToccoSlogan} alt="Tocco Slogan" height="42.3" width="460" />
        <StyledSloganImg src={ToccoSlogan} alt="Tocco Slogan" height="42.3" width="460" />
        <StyledLoginWrapper>
          <StyledHeadingLogin>
            <FormattedMessage id="client.admin.welcomeTitle" />
          </StyledHeadingLogin>
          {ssoAvailable && <SsoLoginPart />}
          <ToccoLogin loginSuccess={loginSuccess} showTitle={false} />
        </StyledLoginWrapper>
      </StyledLogin>
    </>
  )
}

Login.propTypes = {
  ssoAvailable: PropTypes.bool,
  loginSuccessful: PropTypes.func.isRequired,
  checkSsoAvailable: PropTypes.func.isRequired
}

export default Login
