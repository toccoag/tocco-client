import {testingLibrary} from 'tocco-test-util'

import Admin from './Admin'

jest.mock('./AdminRoute', () => () => <div data-testid="admin" />)

describe('admin', () => {
  describe('components', () => {
    describe('Admin', () => {
      describe('Admin', () => {
        const defaultProps = {
          initializeNavigation: sinon.spy(),
          baseRoute: '',
          getRoutingUserConfirmation: sinon.spy(),
          loadPrincipal: sinon.spy(),
          loadSettingsAndPreferences: sinon.spy(),
          resetPathInfos: sinon.spy(),
          loadHasOutlookAddInModule: sinon.spy()
        }

        test('should init', async () => {
          testingLibrary.renderWithIntl(<Admin {...defaultProps} />)

          expect(defaultProps.resetPathInfos).to.have.been.calledOnce
          expect(defaultProps.initializeNavigation).to.have.been.calledOnce
          expect(defaultProps.loadPrincipal).to.have.been.calledOnce
          expect(defaultProps.loadSettingsAndPreferences).to.have.been.calledOnce
          expect(defaultProps.loadHasOutlookAddInModule).to.have.been.calledOnce
        })
      })
    })
  })
})
