import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {RouterProvider, createBrowserRouter, createMemoryRouter} from 'react-router-dom'
import {useAutoLoginSetup} from 'tocco-sso-login/src/main'
import {viewPersistor} from 'tocco-util'

import AdminRoute from './AdminRouteContainer'

const Admin = ({
  initializeNavigation,
  baseRoute,
  loadPrincipal,
  loadSettingsAndPreferences,
  resetPathInfos,
  loadHasOutlookAddInModule,
  memoryHistory
}) => {
  // only on mount
  useEffect(() => {
    viewPersistor.clearPersistedViews()
    resetPathInfos()
    initializeNavigation()
    loadPrincipal()
    loadSettingsAndPreferences()
    loadHasOutlookAddInModule()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useAutoLoginSetup()

  const routerFactory = memoryHistory ? createMemoryRouter : createBrowserRouter
  const router = routerFactory(
    [
      {
        path: '*',
        element: <AdminRoute />
      }
    ],
    {basename: baseRoute}
  )

  return <RouterProvider router={router} />
}

Admin.propTypes = {
  baseRoute: PropTypes.string,
  loggedIn: PropTypes.bool,
  loadPrincipal: PropTypes.func.isRequired,
  initializeNavigation: PropTypes.func.isRequired,
  loadSettingsAndPreferences: PropTypes.func.isRequired,
  resetPathInfos: PropTypes.func.isRequired,
  loadHasOutlookAddInModule: PropTypes.func.isRequired,
  memoryHistory: PropTypes.bool
}

export default Admin
