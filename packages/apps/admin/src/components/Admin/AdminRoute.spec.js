import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import AdminRoute from './AdminRoute'

/* eslint-disable react/prop-types */
jest.mock('../../../../../core/app-extensions/src/notification/index', () => ({
  Notifications: () => <div data-testid="notifications" />
}))
jest.mock('../../../../../core/app-extensions/src/errorLogging/index', () => ({
  ErrorBoundary: ({children}) => <div data-testid="error-boundary">{children}</div>
}))
jest.mock('../Navigation', () => () => <div data-testid="navigation" />)
jest.mock('../Header', () => () => <div data-testid="header" />)
jest.mock('../../routes/dashboard', () => () => <div data-testid="dashboard" />)
jest.mock('../../routes/docs', () => () => <div data-testid="docs" />)
jest.mock('../../routes/entities', () => () => <div data-testid="entities" />)
jest.mock('../../routes/settings', () => () => <div data-testid="settings" />)
/* eslint-enable react/prop-types */

describe('admin', () => {
  describe('components', () => {
    describe('Admin', () => {
      describe('AdminRoute', () => {
        const defaultProps = {
          setMenuOpen: sinon.spy(),
          menuOpen: false,
          adminAllowed: false
        }

        test('should init', async () => {
          testingLibrary.renderWithIntl(
            <MemoryRouter>
              <AdminRoute {...defaultProps} />
            </MemoryRouter>
          )
          await screen.findAllByTestId('icon')
        })

        test('should show admin forbidden content', async () => {
          testingLibrary.renderWithIntl(
            <MemoryRouter>
              <AdminRoute {...defaultProps} />
            </MemoryRouter>
          )

          expect(await screen.findAllByTestId('icon-times')).to.exist
          expect(screen.getByText('client.admin.error.no_roles.title')).to.exist
          expect(screen.getByText('client.admin.error.no_roles.message')).to.exist
        })

        test('should show admin allowed content', () => {
          testingLibrary.renderWithIntl(
            <MemoryRouter>
              <AdminRoute {...defaultProps} adminAllowed={true} />
            </MemoryRouter>
          )

          expect(screen.queryByText('client.admin.error.no_roles.title')).to.not.exist
          expect(screen.queryByText('client.admin.error.no_roles.message')).to.not.exist
          expect(screen.getByTestId('dashboard')).to.exist
        })
      })
    })
  })
})
