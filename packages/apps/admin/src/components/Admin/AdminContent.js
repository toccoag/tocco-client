import PropTypes from 'prop-types'
import {useRef, useEffect} from 'react'
import {Route, Routes, Navigate, useLocation} from 'react-router-dom'
import {withTheme} from 'styled-components'
import {errorLogging} from 'tocco-app-extensions'
import {viewPersistor} from 'tocco-util'

import DashboardRoute from '../../routes/dashboard'
import DocsRoute from '../../routes/docs'
import EntitiesRoute from '../../routes/entities'
import EntityModelRoute from '../../routes/entityModel'
import HistoryRoute from '../../routes/history'
import Settings from '../../routes/settings'
import Navigation from '../Navigation'

import {StyledContent, StyledMenu, StyledOverlay} from './StyledComponents'

const Admin = ({setMenuOpen, menuOpen}) => {
  const menuRef = useRef(null)
  const location = useLocation()

  const handleClick = e => {
    // whenever a link is not opened in the current tab, do not clear the current view
    // the actual behaviour is browser-dependant, but generally they all do something else when pressing a modifier
    if (!(e.ctrlKey || e.shiftKey || e.metaKey || e.altKey)) {
      setMenuOpen(false)
      viewPersistor.clearPersistedViews()
    }
  }

  useEffect(() => {
    const handleKeyInput = e => {
      if (e.key === 'Escape') {
        setMenuOpen(false)
      }
    }

    document.addEventListener('keydown', handleKeyInput)

    return () => {
      document.removeEventListener('keydown', handleKeyInput)
    }
  }, [menuRef, setMenuOpen])

  useEffect(() => {
    const handleClickOutside = event => {
      const burgerButton = document.getElementById('burger-button')

      if (menuRef.current && !menuRef.current.contains(event.target) && !burgerButton.contains(event.target)) {
        setMenuOpen(false)
      }
    }

    document.addEventListener('mousedown', handleClickOutside)

    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [menuRef, setMenuOpen])

  return (
    <>
      <StyledMenu isOpen={menuOpen} ref={menuRef}>
        <errorLogging.ErrorBoundary>
          <Navigation onClick={handleClick} />
        </errorLogging.ErrorBoundary>
      </StyledMenu>
      <StyledContent>
        <errorLogging.ErrorBoundary>
          <Routes>
            <Route exact path="/" element={<Navigate to="/dashboard" replace />} />
            <Route exact path="dashboard/reload" element={<Navigate to="/dashboard" replace />} />
            <Route exact path="dashboard" element={<DashboardRoute />} />
            <Route path="e/*" element={<EntitiesRoute location={location} />} />
            <Route path="s" element={<Settings />} />
            <Route path="docs/*" element={<DocsRoute />} />
            <Route path="em/*" element={<EntityModelRoute />} />
            <Route path="history/*" element={<HistoryRoute />} />
            <Route path="*" element={<Navigate to="/dashboard" replace />} />
          </Routes>
        </errorLogging.ErrorBoundary>
        <StyledOverlay isOpen={menuOpen} />
      </StyledContent>
    </>
  )
}

Admin.propTypes = {
  menuOpen: PropTypes.bool,
  setMenuOpen: PropTypes.func.isRequired,
  theme: PropTypes.object.isRequired
}

export default withTheme(Admin)
