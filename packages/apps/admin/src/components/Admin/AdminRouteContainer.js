import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setMenuOpen} from '../../modules/navigation/actions'

import AdminRoute from './AdminRoute'

const mapActionCreators = {
  setMenuOpen
}

const mapStateToProps = state => ({
  menuOpen: state.navigation.menuOpen,
  adminAllowed: state.login.adminAllowed
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(AdminRoute))
