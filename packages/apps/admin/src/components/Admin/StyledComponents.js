import styled from 'styled-components'
import {themeSelector} from 'tocco-ui'

export const StyledAdminWrapper = styled.div`
  display: grid;
  grid-template:
    'header header' auto
    'menu content' 1fr / auto 1fr;
  background-color: ${themeSelector.color('paper')};
  height: 100dvh;

  .bm-menu-wrap {
    width: 16dvw !important;
  }

  @media only screen and (max-width: 400px) {
    .bm-menu-wrap {
      width: 100% !important;
    }
  }

  .bm-burger-button button:focus {
    outline: 0;
  }
`

export const StyledContent = styled.div`
  grid-area: content;
  background-color: ${themeSelector.color('backgroundBody')};
  overflow: hidden;
  display: flex;
`

export const StyledMenu = styled.div`
  grid-area: menu;
  height: calc(100% - 39px); /* subtract height of StyledHeader */
  width: 350px;
  position: absolute;
  top: 39px; /* height of StyledHeader */
  z-index: 1001; /* higher than StyledOverlay */
  background: ${themeSelector.color('paper')};
  transition: all 0.5s ease 0s;
  transform: translate3d(${({isOpen}) => (isOpen ? '0' : '-100%')}, 0, 0);
`

export const StyledOverlay = styled.div`
  position: fixed;
  z-index: 1000;
  width: 100%;
  height: 100%;
  background: rgba(0 0 0 / 50%);
  opacity: ${({isOpen}) => (isOpen ? '1' : '0')};
  transition: opacity 0.3s ease 0s;
  transform: ${({isOpen}) => !isOpen && 'translate3d(100%, 0, 0)'};
`
