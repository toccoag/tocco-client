import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {notification, errorLogging} from 'tocco-app-extensions'

import ErrorView from '../ErrorView'
import Header from '../Header'

import navigationStrategy from './../../routes/entities/utils/navigationStrategy'
import AdminContent from './AdminContent'
import {StyledContent, StyledAdminWrapper} from './StyledComponents'

const AdminRoute = ({setMenuOpen, menuOpen, adminAllowed}) => {
  const adminAllowedContent = adminAllowed && <AdminContent menuOpen={menuOpen} setMenuOpen={setMenuOpen} />

  const adminForbiddenContent = adminAllowed === false && (
    <StyledContent>
      <ErrorView
        title={<FormattedMessage id={'client.admin.error.no_roles.title'} />}
        message={<FormattedMessage id={'client.admin.error.no_roles.message'} />}
      />
    </StyledContent>
  )

  return (
    <>
      <errorLogging.ErrorBoundary>
        <notification.Notifications navigationStrategy={navigationStrategy()} />
      </errorLogging.ErrorBoundary>
      <StyledAdminWrapper>
        <Header menuOpen={menuOpen} />
        {adminAllowedContent || adminForbiddenContent}
      </StyledAdminWrapper>
    </>
  )
}

AdminRoute.propTypes = {
  menuOpen: PropTypes.bool,
  setMenuOpen: PropTypes.func.isRequired,
  adminAllowed: PropTypes.bool.isRequired
}

export default AdminRoute
