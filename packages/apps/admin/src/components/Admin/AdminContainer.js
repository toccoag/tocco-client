import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initializeNavigation} from '../../modules/navigation/actions'
import {loadSettingsAndPreferences} from '../../modules/preferences/actions'
import {loadPrincipal, loadHasOutlookAddInModule} from '../../modules/session/actions'
import {resetPathInfos} from '../../routes/entities/modules/path/actions'

import Admin from './Admin'

const mapActionCreators = {
  loadPrincipal,
  initializeNavigation,
  loadSettingsAndPreferences,
  resetPathInfos,
  loadHasOutlookAddInModule
}

const mapStateToProps = (state, props) => ({
  baseRoute: state.input.baseRoute,
  memoryHistory: state.input.memoryHistory,
  hasOutlookAddInModule: state.session.hasOutlookAddInModule
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Admin))
