import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {Helmet} from 'react-helmet'
import {LoadMask} from 'tocco-ui'

import Login from '../../components/Login'
import Admin from '../Admin'

const LoginGuard = ({doSessionCheck, loggedIn}) => {
  useEffect(() => {
    doSessionCheck()
  }, [doSessionCheck])

  return (
    <>
      <Helmet defer={false}>
        <title>Tocco</title>
      </Helmet>
      <LoadMask required={[loggedIn !== undefined]} loadingText="Logging in...">
        {!loggedIn ? <Login /> : <Admin />}
      </LoadMask>
    </>
  )
}

LoginGuard.propTypes = {
  loggedIn: PropTypes.bool,
  doSessionCheck: PropTypes.func.isRequired
}

export default LoginGuard
