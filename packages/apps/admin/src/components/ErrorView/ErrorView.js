import PropTypes from 'prop-types'
import styled from 'styled-components'
import {Icon, scale, themeSelector, Typography} from 'tocco-ui'

const StyledErrorView = styled.div`
  width: 50dvw;
  max-width: 650px;
  padding: ${scale.space(1)};
  text-align: center;
  margin: 20dvh auto auto;

  h1 {
    margin: 0 auto;
  }
`

const StyledIconWrapper = styled.span`
  color: ${themeSelector.color('signal.danger')};
  font-size: ${scale.font(13.4)};
`

const ErrorView = ({title, message}) => {
  return (
    <StyledErrorView>
      <StyledIconWrapper>
        <Icon icon="times" />
      </StyledIconWrapper>
      <Typography.H1>{title}</Typography.H1>
      <Typography.Span>{message}</Typography.Span>
    </StyledErrorView>
  )
}

ErrorView.propTypes = {
  title: PropTypes.element.isRequired,
  message: PropTypes.element.isRequired
}

export default ErrorView
