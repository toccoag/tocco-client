import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadBusinessUnits, changeBusinessUnit} from '../../modules/session/actions'

import BusinessUnitButtonMenu from './BusinessUnitButtonMenu'

const mapActionCreators = {
  loadBusinessUnits,
  changeBusinessUnit
}

const mapStateToProps = (state, props) => ({
  currentBusinessUnit: state.session.currentBusinessUnit,
  businessUnits: state.session.businessUnits,
  inWrongBusinessUnit: state.session.inWrongBusinessUnit
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(BusinessUnitButtonMenu))
