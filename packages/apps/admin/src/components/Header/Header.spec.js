import {fireEvent, screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import Header from './Header'

jest.mock('./NotificationCenterButton', () => () => <div data-testid="notification-center-button" />)
jest.mock('./BusinessUnitButtonMenuContainer', () => () => <div data-testid="business-unit-button-menu" />)

const renderHeader = async props => {
  testingLibrary.renderWithIntl(
    <MemoryRouter>
      <Header {...props} />
    </MemoryRouter>
  )

  await screen.findAllByTestId('icon')
}

describe('admin', () => {
  describe('components', () => {
    describe('Header', () => {
      describe('Header', () => {
        const defaultProps = {
          username: 'user',
          doLogout: sinon.spy(),
          resetUserPreferences: sinon.spy(),
          runEnv: 'TEST',
          niceVersion: '3.7',
          openModalComponent: sinon.spy(),
          removeModalComponent: sinon.spy(),
          info: sinon.spy(),
          intl: IntlStub,
          setThemeType: sinon.spy(),
          saveUserPreferences: sinon.spy(),
          setMenuOpen: sinon.spy()
        }

        beforeEach(() => {
          window.ResizeObserver = ResizeObserver
        })

        test('should render Header', async () => {
          await renderHeader(defaultProps)

          jestExpect(screen.getByRole('link', {name: 'client.admin.dashboard'})).toHaveAttribute(
            'href',
            '/dashboard/reload'
          )
          expect(screen.getByTestId('business-unit-button-menu')).to.exist
          expect(screen.getByRole('button', {name: 'user'})).to.exist
          expect(screen.getByTestId('notification-center-button')).to.exist
        })

        test('should be able to user settings', async () => {
          await renderHeader(defaultProps)

          const menuButton = screen.getByRole('button', {name: 'user'})
          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.passwordUpdate')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.passwordUpdate'))
          expect(defaultProps.openModalComponent).to.have.been.calledWith(
            'passwordUpdateModal',
            'client.login.passwordUpdate.title',
            null,
            sinon.match.any,
            true
          )
          defaultProps.openModalComponent.resetHistory()

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.twoFactorConnector')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.twoFactorConnector'))
          expect(defaultProps.openModalComponent).to.have.been.calledWith(
            'twoFactorConnector',
            'client.actions.two-factor-connector.title',
            null,
            sinon.match.any,
            true
          )

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.resetPreferences')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.resetPreferences'))
          expect(defaultProps.resetUserPreferences).to.have.been.calledOnce

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.logout')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.logout'))
          expect(defaultProps.doLogout).to.have.been.calledOnce
        })

        test('should be able get information about tocco', async () => {
          window.open = sinon.spy()

          await renderHeader(defaultProps)

          expect(await screen.findByTestId('icon-question-circle')).to.exist
          expect(screen.getByRole('button', {name: 'client.admin.header.help'})).to.exist

          const menuButton = screen.getByRole('button', {name: 'client.admin.header.help'})
          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.doc')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.doc'))
          expect(window.open).to.have.been.calledWith('https://307.docs.tocco.ch/', '_blank')

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.sysdoc')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.sysdoc'))
          expect(window.open).to.have.been.calledWith('https://sysdoc.tocco.ch', '_blank')

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.restDoc')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.restDoc'))
          expect(window.open).to.have.been.calledWith('/nice2/swagger', '_blank')

          fireEvent.click(menuButton)

          expect(screen.getByText('client.admin.menu.aboutToccoTitle')).to.exist
          fireEvent.click(screen.getByText('client.admin.menu.aboutToccoTitle'))
          expect(defaultProps.openModalComponent).to.have.been.calledWith('about', null, null, sinon.match.any, true)
        })
      })
    })
  })
})
