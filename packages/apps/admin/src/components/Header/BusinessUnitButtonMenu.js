import PropTypes from 'prop-types'
import {withTheme} from 'styled-components'
import {ButtonMenu, MenuItem} from 'tocco-ui'

const BusinessUnitButtonMenu = ({
  currentBusinessUnit,
  businessUnits,
  loadBusinessUnits,
  changeBusinessUnit,
  inWrongBusinessUnit,
  intl,
  theme
}) => {
  const msg = id => intl.formatMessage({id})

  const handleBusinessUnitOpen = () => {
    if (businessUnits.length === 0) {
      loadBusinessUnits()
    }
  }

  const BusinessUnitMenuItems = businessUnits.map(bU => (
    <MenuItem
      key={`buMenu-${bU.id}`}
      disabled={bU.id === currentBusinessUnit.id}
      onClick={() => changeBusinessUnit(bU.id)}
    >
      {bU.label}
    </MenuItem>
  ))

  const buttonProps = inWrongBusinessUnit
    ? {title: msg('client.admin.menu.principalInWrongBusinessUnit'), style: {color: theme.colors.signal.warning}}
    : {}
  return (
    <ButtonMenu
      label={currentBusinessUnit.label}
      onOpen={handleBusinessUnitOpen}
      icon={inWrongBusinessUnit ? 'exclamation-circle' : 'suitcase'}
      buttonProps={buttonProps}
    >
      {BusinessUnitMenuItems}
    </ButtonMenu>
  )
}

const bUPropType = PropTypes.shape({
  id: PropTypes.string,
  label: PropTypes.string
})

BusinessUnitButtonMenu.propTypes = {
  currentBusinessUnit: bUPropType,
  businessUnits: PropTypes.arrayOf(bUPropType),
  changeBusinessUnit: PropTypes.func.isRequired,
  loadBusinessUnits: PropTypes.func.isRequired,
  inWrongBusinessUnit: PropTypes.bool,
  intl: PropTypes.object.isRequired,
  theme: PropTypes.shape({
    colors: PropTypes.shape({
      signal: PropTypes.shape({
        warning: PropTypes.string.isRequired
      }).isRequired
    }).isRequired
  }).isRequired
}

export default withTheme(BusinessUnitButtonMenu)
