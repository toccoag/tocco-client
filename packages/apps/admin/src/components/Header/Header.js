import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {PasswordUpdateApp} from 'tocco-login/src/main'
import OutlookAddInConnectorApp from 'tocco-outlook-add-in-connector/src/main'
import TwoFactorConnectorApp from 'tocco-two-factor-connector/src/main'
import {Ball, BallMenu, ButtonContextProvider, ButtonMenu, MenuItem, RouterLinkButton, BurgerButton} from 'tocco-ui'
import {cache} from 'tocco-util'

import {getDocsUrl} from '../../utils/docsUtils'
import AboutTocco from '../AboutTocco'

import BusinessUnitButtonMenu from './BusinessUnitButtonMenuContainer'
import NotificationCenterButton from './NotificationCenterButton'
import {
  StyledBackgroundCover,
  StyledBackgroundLogo,
  StyledBallMenuWrapper,
  StyledConfig,
  StyledHeader
} from './StyledComponents'

const Header = ({
  username,
  doLogout,
  resetUserPreferences,
  runEnv,
  niceVersion,
  openModalComponent,
  removeModalComponent,
  info,
  intl,
  setMenuOpen,
  menuOpen,
  themeType,
  setThemeType,
  saveUserPreferences,
  hasOutlookAddInModule
}) => {
  const handleChange = () => {
    const updatedThemeType = themeType === 'dark' ? 'light' : 'dark'
    setThemeType(updatedThemeType)
    cache.addLongTerm('admin', 'theme', updatedThemeType)
    saveUserPreferences({'admin.theme': updatedThemeType})
  }

  const openPasswordUpdate = () => {
    const handleSuccess = () => {
      removeModalComponent('passwordUpdateModal')
      info('success', 'client.login.passwordUpdate.success')
    }

    openModalComponent(
      'passwordUpdateModal',
      'client.login.passwordUpdate.title',
      null,
      () => <PasswordUpdateApp username={username} success={handleSuccess} />,
      true
    )
  }

  const openOutlookAddInConnector = () => {
    const handleOnSuccess = () => {
      removeModalComponent('outlookAddInConnector')
    }

    openModalComponent(
      'outlookAddInConnector',
      'client.actions.outlook-add-in-connector.title',
      null,
      () => <OutlookAddInConnectorApp onSuccess={handleOnSuccess} />,
      true
    )
  }

  const openTwoFactorConnector = () => {
    const handleOnSuccess = () => {
      removeModalComponent('twoFactorConnector')
    }

    openModalComponent(
      'twoFactorConnector',
      'client.actions.two-factor-connector.title',
      null,
      () => <TwoFactorConnectorApp onSuccess={handleOnSuccess} />,
      true
    )
  }

  const closeMenu = () => setMenuOpen(false)
  const msg = id => intl.formatMessage({id})
  const toggleMenu = () => {
    setMenuOpen(!menuOpen)
  }
  const MenuItemDocumentation = niceVersion && (
    <MenuItem
      onClick={() => {
        window.open(getDocsUrl(niceVersion), '_blank')
      }}
      data-cy="menuitem-documentation"
    >
      <FormattedMessage id="client.admin.menu.doc" />
    </MenuItem>
  )
  const MenuItemSysdoc = (
    <MenuItem
      onClick={() => {
        window.open('https://sysdoc.tocco.ch', '_blank')
      }}
      data-cy="menuitem-sysdoc"
    >
      <FormattedMessage id="client.admin.menu.sysdoc" />
    </MenuItem>
  )
  const MenuItemRestDocumentation = (
    <MenuItem
      onClick={() => {
        window.open('/nice2/swagger', '_blank')
      }}
      data-cy="menuitem-rest-documentation"
    >
      <FormattedMessage id="client.admin.menu.restDoc" />
    </MenuItem>
  )
  const MenuItemAbout = (
    <MenuItem
      onClick={() => {
        openModalComponent('about', null, null, () => <AboutTocco />, true)
      }}
      data-cy="menuitem-about"
    >
      <FormattedMessage id="client.admin.menu.aboutToccoTitle" />
    </MenuItem>
  )

  return (
    <>
      <BurgerButton isOpen={menuOpen} toggleMenu={toggleMenu} />
      <StyledBackgroundLogo runEnv={runEnv} />
      <StyledBackgroundCover />
      <StyledHeader>
        <ButtonContextProvider>
          {ref => (
            <StyledConfig ref={ref}>
              <RouterLinkButton
                to="/dashboard/reload"
                onClick={closeMenu}
                icon="house-blank"
                label={msg('client.admin.dashboard')}
              />
              <BusinessUnitButtonMenu />
              <ButtonMenu label={username} icon="user" buttonProps={{'data-cy': 'btn-user'}}>
                <MenuItem disabled>{username}</MenuItem>
                <MenuItem onClick={openPasswordUpdate} data-cy="menuitem-password-update">
                  <FormattedMessage id="client.admin.menu.passwordUpdate" />
                </MenuItem>
                {hasOutlookAddInModule && (
                  <MenuItem onClick={openOutlookAddInConnector} data-cy="menuitem-outlook-add-in-connector">
                    <FormattedMessage id="client.admin.menu.outlookAddInConnector" />
                  </MenuItem>
                )}
                <MenuItem onClick={openTwoFactorConnector} data-cy="menuitem-two-factor-connector">
                  <FormattedMessage id="client.admin.menu.twoFactorConnector" />
                </MenuItem>
                <MenuItem onClick={resetUserPreferences} data-cy="menuitem-reset-preferences">
                  <FormattedMessage id="client.admin.menu.resetPreferences" />
                </MenuItem>
                <MenuItem onClick={doLogout} data-cy="menuitem-logout">
                  <FormattedMessage id="client.admin.menu.logout" />
                </MenuItem>
              </ButtonMenu>
              <StyledBallMenuWrapper>
                <Ball
                  icon={themeType === 'dark' ? 'sun-bright' : 'moon'}
                  onClick={handleChange}
                  title={
                    themeType === 'dark' ? msg('client.admin.header.lightmode') : msg('client.admin.header.darkmode')
                  }
                />
                <BallMenu
                  buttonProps={{
                    icon: 'question-circle',
                    title: msg('client.admin.header.help'),
                    'data-cy': 'btn-helpcenter'
                  }}
                >
                  {MenuItemDocumentation}
                  {MenuItemSysdoc}
                  {MenuItemRestDocumentation}
                  {MenuItemAbout}
                </BallMenu>
                <NotificationCenterButton />
              </StyledBallMenuWrapper>
            </StyledConfig>
          )}
        </ButtonContextProvider>
      </StyledHeader>
    </>
  )
}

Header.propTypes = {
  username: PropTypes.string,
  doLogout: PropTypes.func.isRequired,
  resetUserPreferences: PropTypes.func.isRequired,
  runEnv: PropTypes.string,
  niceVersion: PropTypes.string,
  openModalComponent: PropTypes.func.isRequired,
  removeModalComponent: PropTypes.func.isRequired,
  info: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  menuOpen: PropTypes.bool,
  setMenuOpen: PropTypes.func.isRequired,
  themeType: PropTypes.string,
  setThemeType: PropTypes.func.isRequired,
  saveUserPreferences: PropTypes.func.isRequired,
  hasOutlookAddInModule: PropTypes.bool
}

export default Header
