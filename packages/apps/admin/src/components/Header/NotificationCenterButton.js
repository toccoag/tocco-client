import {
  useFloating,
  useDismiss,
  useClick,
  useInteractions,
  FloatingPortal,
  shift,
  offset,
  autoUpdate
} from '@floating-ui/react'
import PropTypes from 'prop-types'
import {useState, useRef} from 'react'
import {injectIntl} from 'react-intl'
import {notification} from 'tocco-app-extensions'
import {GlobalAppClassName, Ball} from 'tocco-ui'

import navigationStrategy from './../../routes/entities/utils/navigationStrategy'
import NotificationCenterRedDotContainer from './NotificationCenterRedDotContainer'
import {StyledNotificationBellWrapper, StyledFloatingUi} from './StyledComponents'

const NotificationCenterButton = ({intl}) => {
  const redDotElement = useRef(null)
  const [isOpen, setIsOpen] = useState(false)

  const {refs, floatingStyles, context} = useFloating({
    open: isOpen,
    onOpenChange: setIsOpen,
    middleware: [shift({padding: 35}), offset(3)],
    whileElementsMounted: autoUpdate
  })

  const click = useClick(context)
  const dismiss = useDismiss(context)

  const {getReferenceProps, getFloatingProps} = useInteractions([click, dismiss])

  const msg = id => intl.formatMessage({id})

  return (
    <>
      <StyledNotificationBellWrapper title={msg('client.admin.notification.title')}>
        <Ball icon="bell" ref={refs.setReference} {...getReferenceProps()} data-cy="btn-notification" />
        <span ref={redDotElement}>
          <NotificationCenterRedDotContainer />
        </span>
      </StyledNotificationBellWrapper>
      {isOpen && (
        <FloatingPortal>
          <StyledFloatingUi
            ref={refs.setFloating}
            style={floatingStyles}
            {...getFloatingProps()}
            className={GlobalAppClassName}
          >
            <notification.NotificationCenter navigationStrategy={navigationStrategy()} />
          </StyledFloatingUi>
        </FloatingPortal>
      )}
    </>
  )
}

NotificationCenterButton.propTypes = {
  intl: PropTypes.object.isRequired
}

export default injectIntl(NotificationCenterButton)
