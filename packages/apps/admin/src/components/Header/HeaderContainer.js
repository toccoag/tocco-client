import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {notification, appFactory} from 'tocco-app-extensions'

import {setMenuOpen} from '../../modules/navigation/actions'
import {saveUserPreferences, resetUserPreferences} from '../../modules/preferences/actions'
import {doLogout} from '../../modules/session/actions'

import Header from './Header'

const mapActionCreators = {
  doLogout,
  resetUserPreferences,
  openModalComponent: notification.modal,
  removeModalComponent: notification.removeModal,
  info: notification.toaster,
  setMenuOpen,
  setThemeType: appFactory.setThemeType,
  saveUserPreferences
}

const mapStateToProps = (state, props) => ({
  username: state.session.username,
  runEnv: state.preferences.serverSettings.runEnv,
  niceVersion: state.preferences.serverSettings.niceVersion,
  themeType: state.theme.themeType,
  hasOutlookAddInModule: state.session.hasOutlookAddInModule
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Header))
