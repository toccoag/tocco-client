import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import BusinessUnitButtonMenu from './BusinessUnitButtonMenu'

describe('admin', () => {
  describe('components', () => {
    describe('Header', () => {
      describe('BusinessUnitButtonMenu', () => {
        const defaultProps = {
          currentBusinessUnit: {id: 'test1', label: 'Test 1'},
          businessUnits: [
            {id: 'test1', label: 'Test 1'},
            {id: 'test2', label: 'Test 2'}
          ],
          loadBusinessUnits: sinon.spy(),
          changeBusinessUnit: sinon.spy()
        }

        test('should be able to change business unit', async () => {
          testingLibrary.renderWithIntl(<BusinessUnitButtonMenu {...defaultProps} />)

          const menuButton = screen.getByRole('button', {name: 'Test 1'})
          fireEvent.click(menuButton)

          expect(screen.getByText('Test 2')).to.exist
          fireEvent.click(screen.getByText('Test 2'))

          expect(defaultProps.changeBusinessUnit).to.have.been.calledWith('test2')
        })
      })
    })
  })
})
