import PropTypes from 'prop-types'

import {StyledRedDot} from './StyledComponents'

const NotificationCenterRedDot = ({unreadNotificationKeys, innerRef}) =>
  unreadNotificationKeys.length > 0 && <StyledRedDot ref={innerRef} />

NotificationCenterRedDot.propTypes = {
  unreadNotificationKeys: PropTypes.arrayOf(PropTypes.string),
  innerRef: PropTypes.shape({current: PropTypes.any})
}

export default NotificationCenterRedDot
