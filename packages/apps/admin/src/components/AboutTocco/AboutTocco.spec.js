import {IntlStub, testingLibrary, screenUtils} from 'tocco-test-util'

import AboutTocco from './AboutTocco'

jest.mock('../../../package.json', () => ({name: 'tocco-package', version: '3.8.15'}))

describe('admin', () => {
  describe('components', () => {
    describe('AboutTocco', () => {
      describe('AboutTocco', () => {
        const props = {niceVersion: '3.7', niceRevision: 'abc', buildTimestamp: '2023-09-29T08:15:00.000Z'}
        test('should show nice information', () => {
          testingLibrary.renderWithIntl(<AboutTocco {...props} intl={IntlStub} />)

          expect(screenUtils.getByTextIncludes('29.09.2023 10:15')).to.exist
          expect(screenUtils.getByTextIncludes('3.7')).to.exist
          expect(screenUtils.getByTextIncludes('abc')).to.exist
        })

        test('should show current package version', () => {
          testingLibrary.renderWithIntl(<AboutTocco {...props} intl={IntlStub} />)

          expect(screenUtils.getByTextIncludes('3.8.15')).to.exist
        })
      })
    })
  })
})
