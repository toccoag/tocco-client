import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import EntityExplorerEntry from './EntityExplorerEntry'

describe('admin', () => {
  describe('components', () => {
    describe('Navigation', () => {
      describe('menuType', () => {
        describe('EntityExplorerEntry', () => {
          test('should render label', async () => {
            const item = {
              entity: 'Entity',
              label: 'Label'
            }

            testingLibrary.renderWithIntl(
              <MemoryRouter>
                <EntityExplorerEntry item={item} />
              </MemoryRouter>
            )
            await screen.findAllByTestId('icon')

            expect(screen.queryByText('Label')).to.exist
          })

          test('should render label with entity when matching', async () => {
            const item = {
              entity: 'Entity',
              label: 'Label',
              matchingAttribute: 'entity'
            }

            testingLibrary.renderWithIntl(
              <MemoryRouter>
                <EntityExplorerEntry item={item} />
              </MemoryRouter>
            )
            await screen.findAllByTestId('icon')

            expect(screen.queryByText('Label')).to.exist
            expect(screen.queryByText('(Entity)')).to.exist
          })

          test('should render label with entity when expanded label requested', async () => {
            const item = {
              entity: 'Entity',
              label: 'Label'
            }

            testingLibrary.renderWithIntl(
              <MemoryRouter>
                <EntityExplorerEntry item={item} alwaysShowExpandedLabel={true} />
              </MemoryRouter>
            )
            await screen.findAllByTestId('icon')

            expect(screen.queryByText('Label')).to.exist
            expect(screen.queryByText('(Entity)')).to.exist
          })
        })
      })
    })
  })
})
