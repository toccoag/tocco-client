import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actions} from 'tocco-app-extensions'

import ActionEntry from './ActionEntry'

const mapActionCreators = {
  onActionClick: actions.actions.actionInvoke
}

export default connect(null, mapActionCreators)(injectIntl(ActionEntry))
