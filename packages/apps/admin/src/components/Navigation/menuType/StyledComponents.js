import {Link} from 'react-router-dom'
import styled, {css} from 'styled-components'
import {scale, themeSelector} from 'tocco-ui'
import {device} from 'tocco-util'

const secondaryLight = themeSelector.color('secondaryLight')

export const StyledMenuLink = styled(Link)`
  display: block;
  width: 100%;
  color: ${themeSelector.color('text')};
  text-decoration: none;

  &:hover {
    text-decoration: none; /* nice2 reset */
  }

  &:focus {
    outline: none;
    text-decoration: none; /* nice2 reset */
    color: ${secondaryLight};
  }

  &.active {
    text-decoration: underline;
  }

  &:link,
  &:visited {
    color: ${themeSelector.color('text')}; /* nice2 reset */
  }
`

export const StyledIconLink = styled(Link)`
  opacity: 0;
  color: ${themeSelector.color('text')};
  text-decoration: none;
  place-self: center baseline;

  &:link,
  &:visited {
    color: ${themeSelector.color('text')}; /* nice2 reset */
  }

  &:hover {
    color: ${themeSelector.color('secondaryLight')};
  }

  &:focus {
    opacity: 1;
  }

  /* disable open new tab feature on touch devices */
  ${device.isPrimaryTouchDevice() &&
  css`
    display: none;
  `}
`

export const StyledMenuLinkWrapper = styled.div`
  display: grid;
  border-radius: ${themeSelector.radii('menuEntries')};
  padding-left: ${scale.space(-1)};
  padding-top: ${scale.space(-1.5)};
  padding-bottom: ${scale.space(-1.5)};
  grid-template-columns: 91% 1fr;

  &:hover {
    cursor: pointer;
    background-color: ${themeSelector.color('backgroundItemHover')};
  }

  &:focus-within {
    &:not(:has(${StyledIconLink}:focus)) {
      outline: 2px solid ${themeSelector.color('signal.info')};
    }
  }

  button {
    width: 100%;
  }

  &:hover ${StyledIconLink} {
    opacity: 1;
  }

  /* since StyledIconLink is disabled on mobile, there is only one column to display  */
  ${device.isPrimaryTouchDevice() &&
  css`
    grid-template-columns: auto;
  `}
`

export const StyledEntityNameDisplay = styled.span`
  display: inline-block;
  color: ${themeSelector.color('textLight')};
  overflow-wrap: anywhere;
  font-weight: ${themeSelector.fontWeight('regular')};
`

export const StyledTogglerButton = styled.button`
  opacity: 0;
  background: transparent;
  border: transparent;
  outline: none;
  padding: 0;
  cursor: pointer;
  margin-left: ${scale.space(-1)};
  color: ${themeSelector.color('text')};

  &:hover {
    color: ${themeSelector.color('secondaryLight')};
  }

  /* always show expand/collapse icon on touch devices */
  ${device.isPrimaryTouchDevice() &&
  css`
    opacity: 1;
  `}
`

export const StyledMenuEntry = styled.div`
  display: flex;
  justify-content: space-between;
  color: ${themeSelector.color('text')};
  font-weight: ${themeSelector.fontWeight('bold')};
  border-radius: ${themeSelector.radii('menuEntries')};
  padding: ${scale.space(-1)} ${scale.space(-1.2)};

  &:hover {
    background-color: ${themeSelector.color('backgroundItemHover')};

    ${StyledTogglerButton} {
      opacity: 1;
    }
    ${({onClick, theme}) =>
      onClick
        ? css`
            cursor: pointer;
            ${themeSelector.color('backgroundItemHover')({theme})}
          `
        : css`
            cursor: auto;
          `}
  }
`

export const StyledMenuIconWrapper = styled.span`
  margin-right: ${scale.space(-1.2)};
`

export const StyledMenuChildrenWrapper = styled.div`
  display: ${({isOpen}) => (isOpen ? 'block' : 'none')};
`
