import PropTypes from 'prop-types'
import {actions} from 'tocco-app-extensions'
import {Icon} from 'tocco-ui'

import {StyledIconLink, StyledMenuLink, StyledMenuLinkWrapper, StyledEntityNameDisplay} from './StyledComponents'

const ActionEntry = ({onClick, item, alwaysShowExpandedLabel, onActionClick}) => {
  if (!isCustomAction(item)) {
    return <></>
  }

  const label = (
    <>
      {item.label}
      {(alwaysShowExpandedLabel || item.matchingAttribute === 'name') && (
        <>
          <br />
          <StyledEntityNameDisplay>({item.name})</StyledEntityNameDisplay>
        </>
      )}
    </>
  )

  const openActionModal = e => {
    e.preventDefault()

    const definition = {
      id: item.name,
      label: item.label,
      endpoint: item.endpoint,
      runInBackgroundTask: item.runInBackgroundTask,
      actionType: item.endpoint ? actions.actionTypes.SIMPLE : actions.actionTypes.CUSTOM,
      buttonType: 'TEXT',
      appId: item.name
    }
    const selection = {}
    onActionClick(definition, selection)
  }

  return (
    <StyledMenuLinkWrapper>
      {item.fullscreen && (
        <>
          <StyledMenuLink data-quick-navigation={true} onClick={onClick} to={`/e/action/${item.name}`}>
            {label}
          </StyledMenuLink>
          <StyledIconLink to={`/e/action/${item.name}`} target="_blank" rel="noreferrer">
            <Icon icon="external-link" />
          </StyledIconLink>
        </>
      )}
      {!item.fullscreen && (
        <StyledMenuLink data-quick-navigation={true} onClick={openActionModal}>
          {label}
        </StyledMenuLink>
      )}
    </StyledMenuLinkWrapper>
  )
}

const isCustomAction = item => item.name && !item.path

ActionEntry.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    fullscreen: PropTypes.bool,
    runInBackgroundTask: PropTypes.bool,
    endpoint: PropTypes.string,
    path: PropTypes.string,
    matchingAttribute: PropTypes.string
  }).isRequired,
  onClick: PropTypes.func,
  alwaysShowExpandedLabel: PropTypes.bool,
  onActionClick: PropTypes.func.isRequired
}

export default ActionEntry
