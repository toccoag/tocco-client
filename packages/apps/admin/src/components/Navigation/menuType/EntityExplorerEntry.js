import PropTypes from 'prop-types'
import {Icon} from 'tocco-ui'

import {StyledIconLink, StyledMenuLink, StyledMenuLinkWrapper, StyledEntityNameDisplay} from './StyledComponents'

const EntityExplorerEntry = ({onClick, item, alwaysShowExpandedLabel}) => {
  const entityNameDisplay =
    alwaysShowExpandedLabel || item.matchingAttribute === 'entity' ? (
      <StyledEntityNameDisplay>({item.entity})</StyledEntityNameDisplay>
    ) : (
      ''
    )

  return (
    <StyledMenuLinkWrapper>
      <StyledMenuLink data-quick-navigation={true} onClick={onClick} to={`/e/${item.entity}`}>
        {item.label}
        {entityNameDisplay !== '' && (
          <>
            <br />
            {entityNameDisplay}
          </>
        )}
      </StyledMenuLink>
      <StyledIconLink to={`/e/${item.entity}`} target="_blank" rel="noreferrer">
        <Icon icon="external-link" />
      </StyledIconLink>
    </StyledMenuLinkWrapper>
  )
}

EntityExplorerEntry.propTypes = {
  item: PropTypes.shape({
    entity: PropTypes.string,
    label: PropTypes.string,
    matchingAttribute: PropTypes.string
  }),
  onClick: PropTypes.func,
  alwaysShowExpandedLabel: PropTypes.bool
}

export default EntityExplorerEntry
