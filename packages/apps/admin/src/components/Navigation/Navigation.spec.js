import {configureStore} from '@reduxjs/toolkit'
import {act, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary, IntlStub} from 'tocco-test-util'

import {menuTabs} from '../../utils/navigationUtils'

import Navigation from './Navigation'

describe('admin', () => {
  describe('components', () => {
    describe('Navigation', () => {
      describe('Navigation', () => {
        beforeEach(() => {
          jest.useFakeTimers()
        })
        afterEach(() => {
          jest.useRealTimers()
        })

        const defaultProps = {
          modulesMenuTree: [
            {
              name: 'menu1',
              menuType: 'menu',
              label: 'Menu1',
              icon: 'cog',
              children: [
                {
                  name: 'submenu1',
                  menuType: 'entity-explorer',
                  label: 'Submenu1',
                  entity: 'User'
                },
                {
                  name: 'submenu2',
                  menuType: 'action',
                  label: 'Submenu2',
                  fullscreen: true
                }
              ]
            }
          ],
          settingsMenuTree: [],
          systemMenuTree: [],
          completeMenuTree: [],
          menuOpen: true,
          onClick: sinon.spy(),
          activeMenuTab: menuTabs.MODULES,
          setActiveMenuTab: sinon.spy(),
          intl: IntlStub,
          saveUserPreferences: sinon.spy()
        }

        test('should render menu', async () => {
          const store = configureStore({
            reducer: () => ({preferences: {userPreferences: {'admintree.menu1.collapsed': false}}})
          })
          testingLibrary.renderWithStore(
            <MemoryRouter>
              <Navigation {...defaultProps} />
            </MemoryRouter>,
            {store}
          )
          await screen.findAllByTestId('icon')

          expect(screen.getByText('client.admin.navigation.modules')).to.exist
          expect(screen.getByText('Menu1')).to.exist
          expect(screen.getByRole('link', {name: 'Submenu1'})).to.exist
          expect(screen.getByRole('link', {name: 'Submenu2'})).to.exist
        })

        test('should be able to search by menu items', async () => {
          const store = configureStore({
            reducer: () => ({preferences: {userPreferences: {'admintree.menu1.collapsed': false}}})
          })
          testingLibrary.renderWithStore(
            <MemoryRouter>
              <Navigation {...defaultProps} />
            </MemoryRouter>,
            {store}
          )
          await screen.findAllByTestId('icon')

          const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})
          await user.type(screen.getByRole('searchbox'), 'menu2')

          // searchbox has a debouncer - wait for search reset button is visible
          expect(await screen.findByTestId('icon-times')).to.exist
          act(() => {
            jest.runAllTimers()
          })
          expect(screen.queryByRole('link', {name: 'Submenu1'})).to.not.exist
          expect(screen.getByRole('link', {name: 'Submenu2'})).to.exist

          // clear search again
          await user.click(screen.getByTestId('icon-times'))

          expect(await screen.findByRole('link', {name: 'Submenu1'})).to.exist
          expect(screen.getByRole('link', {name: 'Submenu2'})).to.exist
        })

        test('should expand menu', async () => {
          const store = configureStore({
            reducer: () => ({preferences: {userPreferences: {}}})
          })
          testingLibrary.renderWithStore(
            <MemoryRouter>
              <Navigation {...defaultProps} />
            </MemoryRouter>,
            {store}
          )
          await screen.findAllByTestId('icon')
          const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})

          expect(screen.getByText('client.admin.navigation.modules')).to.exist
          expect(screen.getByText('Menu1')).to.exist
          expect(screen.queryByRole('link', {name: 'Submenu1'})).to.not.exist
          expect(screen.queryByRole('link', {name: 'Submenu2'})).to.not.exist

          await user.click(screen.getByRole('button', {name: 'client.admin.navigation.expandAll'}))

          expect(defaultProps.saveUserPreferences).to.have.been.calledWith({'admintree.menu1.collapsed': false})
        })

        test('should collapse menu', async () => {
          const store = configureStore({
            reducer: () => ({preferences: {userPreferences: {'admintree.menu1.collapsed': false}}})
          })
          testingLibrary.renderWithStore(
            <MemoryRouter>
              <Navigation {...defaultProps} />
            </MemoryRouter>,
            {store}
          )
          await screen.findAllByTestId('icon')
          const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})

          expect(screen.getByText('client.admin.navigation.modules')).to.exist
          expect(screen.getByText('Menu1')).to.exist
          expect(screen.queryByRole('link', {name: 'Submenu1'})).to.exist
          expect(screen.queryByRole('link', {name: 'Submenu2'})).to.exist

          await user.click(screen.getByRole('button', {name: 'client.admin.navigation.collapseAll'}))

          expect(defaultProps.saveUserPreferences).to.have.been.calledWith({'admintree.menu1.collapsed': true})
        })
      })
    })
  })
})
