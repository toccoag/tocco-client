import PropTypes from 'prop-types'
import React, {Suspense} from 'react'
import ReactDOM from 'react-dom'
import {
  actionEmitter,
  appFactory,
  actions,
  errorLogging,
  externalEvents,
  keyDown,
  login,
  notification
} from 'tocco-app-extensions'
import {chooseDocument} from 'tocco-docs-browser/src/main'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {reducer as reducerUtil} from 'tocco-util'

import reducers, {sagas} from './modules/reducers'
import Action from './routes/entities/components/Action'
import shortcuts from './shortcuts'

const packageName = 'admin'

const LazyLoginGuard = React.lazy(() => import('./components/LoginGuard'))
const LazyAdmin = () => (
  <>
    <Suspense fallback="">
      <LazyLoginGuard />
    </Suspense>
  </>
)

const initApp = (id, input, events, publicPath) => {
  if (window.reactRegistry && window.reactRegistry.setReact) {
    window.reactRegistry.setReact(React, ReactDOM)
  }

  const content = <LazyAdmin />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, () => ({}))
  actionEmitter.addToStore(store)
  actions.addToStore(store, () => ({
    appComponent: Action,
    formApp: SimpleFormApp
  }))
  actions.dynamicActionsAddToStore(store)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  notification.addToStore(store, true, {withNotificationCenter: true})
  login.addToStore(store)
  keyDown.addToStore(store, shortcuts)
  chooseDocument.addToStore(store)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: [
      'component',
      'common',
      'actions',
      'login',
      'sso-login',
      'entity-browser',
      'entity-list',
      'entity-detail',
      'dashboard',
      'docs-browser',
      'entities',
      packageName
    ]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const AdminApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

AdminApp.propTypes = {
  /**
   * Route where admin is loaded (part after top level domain)
   */
  baseRoute: PropTypes.string,
  memoryHistory: PropTypes.bool
}

export default AdminApp
