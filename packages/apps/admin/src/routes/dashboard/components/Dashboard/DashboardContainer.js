import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actionEmitter} from 'tocco-app-extensions'

import Dashboard from './Dashboard'

const mapActionCreators = {
  emitAction: action => actionEmitter.dispatchEmittedAction(action)
}

const mapStateToProps = state => ({
  baseRoute: state.input.baseRoute
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Dashboard))
