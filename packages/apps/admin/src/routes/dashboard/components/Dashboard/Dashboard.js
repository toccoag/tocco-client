import PropTypes from 'prop-types'
import React from 'react'
import {useNavigate} from 'react-router-dom'
import {errorLogging} from 'tocco-app-extensions'
import DashboardApp from 'tocco-dashboard/src/main'
import {Breadcrumbs} from 'tocco-ui'

import navigationStrategy from '../../../entities/utils/navigationStrategy'

import {StyledDashboardWrapper, StyledWrapper, StyledBreadcrumbs} from './StyledComponents'

const Dashboard = ({emitAction, intl, baseRoute}) => {
  const navigate = useNavigate()
  const msg = id => intl.formatMessage({id})

  return (
    <StyledWrapper>
      <StyledBreadcrumbs>
        <errorLogging.ErrorBoundary>
          <Breadcrumbs
            currentView={{display: msg('client.admin.dashboard'), title: 'Tocco', type: 'home'}}
            pathPrefix="/dashboard"
          />
        </errorLogging.ErrorBoundary>
      </StyledBreadcrumbs>
      <StyledDashboardWrapper>
        <DashboardApp emitAction={emitAction} navigationStrategy={navigationStrategy(navigate, baseRoute)} />
      </StyledDashboardWrapper>
    </StyledWrapper>
  )
}

Dashboard.propTypes = {
  intl: PropTypes.object.isRequired,
  emitAction: PropTypes.func.isRequired,
  baseRoute: PropTypes.string
}

export default React.memo(Dashboard, () => true)
