import History from 'tocco-history/src/main'

const HistoryApp = props => {
  return <History {...props} />
}

HistoryApp.propTypes = {}

export default HistoryApp
