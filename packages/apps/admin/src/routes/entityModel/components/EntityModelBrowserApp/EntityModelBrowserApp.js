import EntityModelBrowser from 'tocco-entity-model-browser/src/main'

const EntityModelBrowserApp = props => {
  return <EntityModelBrowser {...props} pathPrefix="em" />
}

EntityModelBrowserApp.propTypes = {}

export default EntityModelBrowserApp
