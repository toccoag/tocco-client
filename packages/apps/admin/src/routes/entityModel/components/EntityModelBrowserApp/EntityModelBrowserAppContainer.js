import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import EntityModelBrowserApp from './EntityModelBrowserApp'

const mapActionCreators = {}

const mapStateToProps = (state, props) => ({})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityModelBrowserApp))
