import {FormattedMessage} from 'react-intl'
import {channel} from 'redux-saga'
import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'
import {consoleLogger} from 'tocco-util'

import Action from '../../components/Action'
import {getTemplateKeyFromUrl, transformEntityTemplateValuesToDefaultValues} from '../../utils/entityTemplate'

import * as actions from './actions'

export const entityTemplatesSelector = state => state.entities.entityTemplate.entityTemplates
export const textResourceSelector = state => state.intl.messages

export default function* mainSagas() {
  yield all([
    takeLatest(actions.FETCH_ENTITY_TEMPLATES, fetchEntityTemplates),
    takeLatest(actions.SET_ACTIVE_ENTITY_TEMPLATE, changeEntityTemplate),
    takeLatest(actions.FETCH_TEMPLATE_VALUES, fetchTemplateValues),
    takeLatest(actions.UNLOAD_VIEW, resetActiveTemplate),
    takeLatest(actions.CREATE_ENTITY_TEMPLATE, createEntityTemplate),
    takeLatest(actions.DELETE_ENTITY_TEMPLATE, deleteEntityTemplate),
    takeLatest(actions.UPDATE_ENTITY_TEMPLATE, updateEntityTemplate)
  ])
}

export function* fetchEntityTemplates({payload}) {
  const {entityName} = payload
  yield put(actions.setEntityTemplates(entityName, undefined))
  const entityTemplates = yield call(
    rest.fetchAllEntities,
    'Entity_template',
    {
      where: `base_model=="${entityName}" and (relUser.pk==:currentUser or not exists(relUser))`,
      sorting: [
        {field: 'sorting', order: 'asc'},
        {field: 'label', order: 'asc'}
      ],
      paths: ['label', 'description', 'base_model', 'unique_id']
    },
    {method: 'GET'}
  )
  yield put(actions.setEntityTemplates(entityName, entityTemplates))
}

export function* changeEntityTemplate({payload: {activeEntityTemplate}}) {
  yield put(actions.setTemplateDefaultValues(null))
  if (activeEntityTemplate) {
    const entityName = activeEntityTemplate.paths.base_model.value
    const templateId = activeEntityTemplate.paths.unique_id.value
    yield put(actions.fetchTemplateValues(entityName, templateId))
  }
}

export function* fetchTemplateValues({payload}) {
  const {entityName, templateId} = payload
  try {
    const response = yield call(
      rest.requestSaga,
      `entities/2.0/templates/${entityName}/${encodeURIComponent(templateId)}`,
      {
        method: 'GET'
      }
    )

    const templateValues = response.body
    const defaultValues = transformEntityTemplateValuesToDefaultValues(templateValues)
    yield put(actions.setTemplateDefaultValues(defaultValues))
  } catch (error) {
    consoleLogger.logError('Failed to fetch template values', error)
    yield put(
      notification.toaster({
        type: 'error',
        title: 'client.admin.entities.action.chooseEntityTemplate.label',
        body: 'client.admin.entities.action.chooseEntityTemplate.error'
      })
    )
    yield call(resetActiveTemplate)
  }
}

export function* resetActiveTemplate() {
  yield put(actions.setActiveEntityTemplate(null))
  yield put(actions.setEntity(null))
}

export function* createEntityTemplate({payload}) {
  const {entityName, label, entity, onSuccess, onError} = payload
  const textResources = yield select(textResourceSelector)

  try {
    const options = {
      method: 'POST',
      body: {
        label,
        entity
      }
    }
    const response = yield call(rest.requestSaga, `entities/2.0/templates/${entityName}`, options)
    const createdTemplateUrl = response.headers.get('location')
    if (!createEntityTemplate) {
      yield call(onError, {
        title: textResources['client.admin.entities.action.createEntityTemplate.label'],
        message: textResources['client.admin.entities.action.createEntityTemplate.error']
      })
      return
    }

    yield put(actions.fetchEntityTemplates(entityName))
    yield take(actions.SET_ENTITY_TEMPLATES)
    yield take(actions.SET_ENTITY_TEMPLATES)

    const allEntityTemplates = yield select(entityTemplatesSelector)
    const entityTemplates = allEntityTemplates[entityName]

    const templateKey = getTemplateKeyFromUrl(createdTemplateUrl)

    const createdEntityTemplate = entityTemplates.find(template => template.key === templateKey)
    yield put(actions.setActiveEntityTemplate(createdEntityTemplate))

    yield call(onSuccess, {
      title: textResources['client.admin.entities.action.createEntityTemplate.label'],
      message: textResources['client.admin.entities.action.createEntityTemplate.success']
    })
  } catch (error) {
    yield call(onError, {
      title: textResources['client.admin.entities.action.createEntityTemplate.label'],
      message: textResources['client.admin.entities.action.createEntityTemplate.error']
    })
  }
}

export function* deleteEntityTemplate({payload}) {
  const {entityName, templateKey, navigationStrategy} = payload

  const answerChannel = yield call(channel)
  yield put(
    notification.modal(
      'entity-template-delete',
      'client.admin.entities.action.deleteEntityTemplate.title',
      null,
      ({close}) => {
        const onSuccess = () => {
          answerChannel.put(true)
          close()
        }

        const onCancel = () => {
          answerChannel.put(false)
          close()
        }

        return (
          <Action
            appId="delete"
            selection={{
              entityName: 'Entity_template',
              ids: [templateKey],
              type: 'ID'
            }}
            navigationStrategy={navigationStrategy}
            onSuccess={onSuccess}
            onCancel={onCancel}
          />
        )
      },
      true
    )
  )

  const isEntityTemplateDeleted = yield take(answerChannel)
  if (isEntityTemplateDeleted) {
    yield put(actions.fetchEntityTemplates(entityName))
    yield call(resetActiveTemplate)
  }
}

export function* promptUpdateEntityTemplateConfirm(label) {
  const answerChannel = yield call(channel)
  const onYes = () => answerChannel.put(true)
  const onCancel = () => answerChannel.put(false)

  yield put(
    notification.confirm(
      <FormattedMessage id="client.admin.entities.action.updateEntityTemplate.confirmTitle" />,
      <FormattedMessage id="client.admin.entities.action.updateEntityTemplate.question" values={{label}} />,
      <FormattedMessage id="client.common.ok" />,
      <FormattedMessage id="client.common.cancel" />,
      onYes,
      onCancel
    )
  )
  return yield take(answerChannel)
}

export function* updateEntityTemplate({payload}) {
  const {activeEntityTemplate, entity, onSuccess, onError} = payload
  const textResources = yield select(textResourceSelector)

  if (activeEntityTemplate) {
    const entityName = activeEntityTemplate.paths.base_model.value
    const templateId = activeEntityTemplate.paths.unique_id.value
    const label = activeEntityTemplate.paths.label.value

    const answer = yield call(promptUpdateEntityTemplateConfirm, label)

    if (answer) {
      try {
        const options = {
          method: 'PATCH',
          body: {
            ...entity
          }
        }
        const response = yield call(rest.requestSaga, `entities/2.0/templates/${entityName}/${templateId}`, options)
        if (response.status === 200) {
          yield call(onSuccess, {
            title: textResources['client.admin.entities.action.updateEntityTemplate.label'],
            message: textResources['client.admin.entities.action.updateEntityTemplate.success']
          })
        } else {
          yield call(onError, {
            title: textResources['client.admin.entities.action.updateEntityTemplate.label'],
            message: textResources['client.admin.entities.action.updateEntityTemplate.error']
          })
        }
      } catch (err) {
        yield call(onError, {
          title: textResources['client.admin.entities.action.updateEntityTemplate.label'],
          message: textResources['client.admin.entities.action.updateEntityTemplate.error']
        })
      }
    }
  } else {
    yield call(onError, {
      title: textResources['client.admin.entities.action.updateEntityTemplate.label'],
      message: textResources['client.admin.entities.action.updateEntityTemplate.noActiveTemplateError']
    })
  }
}
