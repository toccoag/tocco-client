import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const setEntityTemplates = (state, {payload: {entityName, entityTemplates}}) => ({
  ...state,
  entityTemplates: {
    ...state.entityTemplates,
    [entityName]: entityTemplates
  }
})

const ACTION_HANDLERS = {
  [actions.SET_ENTITY_TEMPLATES]: setEntityTemplates,
  [actions.SET_ACTIVE_ENTITY_TEMPLATE]: reducerUtil.singleTransferReducer('activeEntityTemplate'),
  [actions.SET_ACTIVE_TEMPLATE_DEFAULT_VALUES]: reducerUtil.singleTransferReducer('activeTemplateDefaultValues'),
  [actions.SET_ENTITY]: reducerUtil.singleTransferReducer('entity')
}

const initialState = {
  entityTemplates: {},
  activeEntityTemplate: null,
  activeTemplateDefaultValues: null,
  entity: null
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
