import {channel} from 'redux-saga'
import {all, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest, notification} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('modules', () => {
        describe('entityTemplate', () => {
          describe('sagas', () => {
            describe('rootSaga', () => {
              test('should fork child sagas', () => {
                const generator = rootSaga()
                expect(generator.next().value).to.deep.equal(
                  all([
                    takeLatest(actions.FETCH_ENTITY_TEMPLATES, sagas.fetchEntityTemplates),
                    takeLatest(actions.SET_ACTIVE_ENTITY_TEMPLATE, sagas.changeEntityTemplate),
                    takeLatest(actions.FETCH_TEMPLATE_VALUES, sagas.fetchTemplateValues),
                    takeLatest(actions.UNLOAD_VIEW, sagas.resetActiveTemplate),
                    takeLatest(actions.CREATE_ENTITY_TEMPLATE, sagas.createEntityTemplate),
                    takeLatest(actions.DELETE_ENTITY_TEMPLATE, sagas.deleteEntityTemplate),
                    takeLatest(actions.UPDATE_ENTITY_TEMPLATE, sagas.updateEntityTemplate)
                  ])
                )
                expect(generator.next().done).to.be.true
              })
            })

            describe('fetchEntityTemplates', () => {
              test('should set fetched entityTemplates', () => {
                const entityTemplatesResponse = [
                  {
                    key: '1',
                    model: 'Entity_template',
                    version: 0,
                    paths: {
                      base_model: {
                        type: 'string',
                        writable: null,
                        value: 'User'
                      },
                      unique_id: {
                        type: 'identifier',
                        writable: null,
                        value: 'CA562562-DD10-0001-3179-51EA13A0164D'
                      },
                      description: {
                        type: 'text',
                        writable: null,
                        value: 'Das ist ein Test'
                      },
                      label: {
                        type: 'string',
                        writable: null,
                        value: 'Test'
                      }
                    }
                  }
                ]

                return expectSaga(sagas.fetchEntityTemplates, {payload: {entityName: 'User'}})
                  .provide([[matchers.call.fn(rest.fetchAllEntities), entityTemplatesResponse]])
                  .put(actions.setEntityTemplates('User', undefined))
                  .put(actions.setEntityTemplates('User', entityTemplatesResponse))
                  .run()
              })
            })

            describe('changeEntityTemplate', () => {
              test('should reset template default values', () => {
                return expectSaga(sagas.changeEntityTemplate, {payload: {activeEntityTemplate: null}})
                  .put(actions.setTemplateDefaultValues(null))
                  .run()
              })
              test('should fetch template values', () => {
                const activeEntityTemplate = {
                  paths: {
                    base_model: {value: 'User'},
                    unique_id: {value: 'abc'}
                  }
                }
                return expectSaga(sagas.changeEntityTemplate, {payload: {activeEntityTemplate}})
                  .put(actions.setTemplateDefaultValues(null))
                  .put(actions.fetchTemplateValues('User', 'abc'))
                  .run()
              })
            })

            describe('fetchTemplateValues', () => {
              test('should fetch default values from template values', () => {
                const entityTemplateValuesRepsonse = {
                  body: {
                    key: 'null',
                    model: 'User',
                    version: 0,
                    paths: {
                      firstname: {
                        type: 'string',
                        writable: null,
                        value: 'Vorname'
                      },
                      lastname: {
                        type: 'string',
                        writable: null,
                        value: 'Nachname'
                      }
                    }
                  }
                }
                const expectedTemplateDefaultValues = [
                  {id: 'firstname', value: 'Vorname'},
                  {id: 'lastname', value: 'Nachname'}
                ]
                return expectSaga(sagas.fetchTemplateValues, {payload: {entityName: 'User', templateId: 'abc'}})
                  .provide([[matchers.call.fn(rest.requestSaga), entityTemplateValuesRepsonse]])
                  .put(actions.setTemplateDefaultValues(expectedTemplateDefaultValues))
                  .run()
              })

              test('should handle exception on fetching template values', () => {
                return expectSaga(sagas.fetchTemplateValues, {payload: {entityName: 'User', templateId: 'abc'}})
                  .provide([[matchers.call.fn(rest.requestSaga), throwError(new Error('Failed'))]])
                  .put.like({action: notification.toaster({type: 'error'})})
                  .call(sagas.resetActiveTemplate)
                  .run()
              })
            })

            describe('resetActiveTemplate', () => {
              test('reset active entity template', () => {
                return expectSaga(sagas.resetActiveTemplate)
                  .put(actions.setActiveEntityTemplate(null))
                  .put(actions.setEntity(null))
                  .run()
              })
            })

            describe('createEntityTemplate', () => {
              test('create new entity template', () => {
                const onSuccess = sinon.spy()
                const onError = sinon.spy()
                const entity = {
                  paths: {
                    firstname: 'Vorname',
                    lastname: 'Nachname'
                  }
                }

                const expectedEntity = {
                  paths: {
                    firstname: 'Vorname',
                    lastname: 'Nachname'
                  }
                }

                const entityTemplateResponse = {
                  headers: new Headers({location: 'http://localhost:8080/nice2/rest/entities/2.0/Entity_template/1743'})
                }

                return expectSaga(sagas.createEntityTemplate, {
                  payload: {entityName: 'User', label: 'Template', entity, onSuccess, onError}
                })
                  .provide([
                    [
                      matchers.call(rest.requestSaga, 'entities/2.0/templates/User', {
                        method: 'POST',
                        body: {label: 'Template', entity: expectedEntity}
                      }),
                      entityTemplateResponse
                    ],
                    {
                      take() {
                        return ''
                      }
                    },
                    [matchers.select(sagas.entityTemplatesSelector), {User: [{key: '1'}, {key: '1743'}]}],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.createEntityTemplate.success': 'Erfolgreich',
                        'client.admin.entities.action.createEntityTemplate.label': 'Vorlage erstellen'
                      }
                    ]
                  ])
                  .put(actions.fetchEntityTemplates('User'))
                  .take(actions.SET_ENTITY_TEMPLATES)
                  .take(actions.SET_ENTITY_TEMPLATES)
                  .put(actions.setActiveEntityTemplate({key: '1743'}))
                  .call(onSuccess, {title: 'Vorlage erstellen', message: 'Erfolgreich'})
                  .run()
              })

              test('handle create new entity template response exception', () => {
                const onSuccess = sinon.spy()
                const onError = sinon.spy()
                const entity = {
                  paths: {
                    firstname: 'Vorname',
                    lastname: 'Nachname'
                  }
                }

                const expectedEntity = {
                  paths: {
                    firstname: 'Vorname',
                    lastname: 'Nachname'
                  }
                }

                return expectSaga(sagas.createEntityTemplate, {
                  payload: {entityName: 'User', label: 'Template', entity, onSuccess, onError}
                })
                  .provide([
                    [
                      matchers.call(rest.requestSaga, 'entities/2.0/templates/User', {
                        method: 'POST',
                        body: {label: 'Template', entity: expectedEntity}
                      }),
                      throwError(new Error('Failed'))
                    ],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.createEntityTemplate.error': 'Fehler',
                        'client.admin.entities.action.createEntityTemplate.label': 'Vorlage erstellen'
                      }
                    ]
                  ])
                  .call(onError, {title: 'Vorlage erstellen', message: 'Fehler'})
                  .run()
              })
            })

            describe('deleteEntityTemplate', () => {
              test('delete entity template', () => {
                const channelMock = channel()

                return expectSaga(sagas.deleteEntityTemplate, {
                  payload: {entityName: 'User', templateKey: '1', navigationStrategy: {}}
                })
                  .provide([
                    {
                      call(effect, next) {
                        return effect.fn === channel ? channelMock : next()
                      }
                    }
                  ])
                  .put.like({
                    action: {
                      type: 'notification/MODAL',
                      payload: {
                        id: 'entity-template-delete',
                        title: 'client.admin.entities.action.deleteEntityTemplate.title',
                        message: null,
                        cancelable: true
                      }
                    }
                  })
                  .dispatch(channelMock.put(true))
                  .take(channelMock)
                  .put(actions.fetchEntityTemplates('User'))
                  .call(sagas.resetActiveTemplate)
                  .run()
              })

              test('handle delete entity template cancelled', () => {
                const channelMock = channel()

                return expectSaga(sagas.deleteEntityTemplate, {
                  payload: {entityName: 'User', templateKey: '1', navigationStrategy: {}}
                })
                  .provide([
                    {
                      call(effect, next) {
                        return effect.fn === channel ? channelMock : next()
                      }
                    }
                  ])
                  .put.like({
                    action: {
                      type: 'notification/MODAL',
                      payload: {
                        id: 'entity-template-delete',
                        title: 'client.admin.entities.action.deleteEntityTemplate.title',
                        message: null,
                        cancelable: true
                      }
                    }
                  })
                  .dispatch(channelMock.put(false))
                  .take(channelMock)
                  .not.put(actions.fetchEntityTemplates('User'))
                  .run()
              })
            })

            describe('updateEntityTemplate', () => {
              const onError = sinon.spy()
              const onSuccess = sinon.spy()
              const entity = {
                paths: {
                  firstname: 'Vorname',
                  lastname: 'Nachname'
                }
              }
              const activeEntityTemplate = {
                paths: {
                  base_model: {value: 'User'},
                  unique_id: {value: 'abc'},
                  label: {value: 'Vorlage'}
                }
              }

              const expectedEntity = {
                paths: {
                  firstname: 'Vorname',
                  lastname: 'Nachname'
                }
              }

              test('update entity template', () => {
                return expectSaga(sagas.updateEntityTemplate, {
                  payload: {activeEntityTemplate, entity, onSuccess, onError}
                })
                  .provide([
                    [matchers.call(sagas.promptUpdateEntityTemplateConfirm, 'Vorlage'), true],
                    [
                      matchers.call(rest.requestSaga, 'entities/2.0/templates/User/abc', {
                        method: 'PATCH',
                        body: expectedEntity
                      }),
                      {status: 200}
                    ],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.updateEntityTemplate.success': 'Erfolgreich',
                        'client.admin.entities.action.updateEntityTemplate.label': 'Vorlage aktualisieren'
                      }
                    ]
                  ])
                  .call(onSuccess, {title: 'Vorlage aktualisieren', message: 'Erfolgreich'})
                  .run()
              })

              test('handle update entity template cancelled', () => {
                return expectSaga(sagas.updateEntityTemplate, {
                  payload: {activeEntityTemplate, entity, onSuccess, onError}
                })
                  .provide([
                    [matchers.call(sagas.promptUpdateEntityTemplateConfirm, 'Vorlage'), false],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.updateEntityTemplate.success': 'Erfolgreich',
                        'client.admin.entities.action.updateEntityTemplate.label': 'Vorlage aktualisieren'
                      }
                    ]
                  ])
                  .not.call(onSuccess, {title: 'Vorlage aktualisieren', message: 'Erfolgreich'})
                  .run()
              })

              test('handle update entity template response error', () => {
                return expectSaga(sagas.updateEntityTemplate, {
                  payload: {activeEntityTemplate, entity, onSuccess, onError}
                })
                  .provide([
                    [matchers.call(sagas.promptUpdateEntityTemplateConfirm, 'Vorlage'), true],
                    [
                      matchers.call(rest.requestSaga, 'entities/2.0/templates/User/abc', {
                        method: 'PATCH',
                        body: expectedEntity
                      }),
                      {status: 400}
                    ],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.updateEntityTemplate.error': 'Fehler',
                        'client.admin.entities.action.updateEntityTemplate.label': 'Vorlage aktualisieren'
                      }
                    ]
                  ])
                  .call(onError, {title: 'Vorlage aktualisieren', message: 'Fehler'})
                  .run()
              })

              test('handle update entity template response exception', () => {
                return expectSaga(sagas.updateEntityTemplate, {
                  payload: {activeEntityTemplate, entity, onSuccess, onError}
                })
                  .provide([
                    [matchers.call(sagas.promptUpdateEntityTemplateConfirm, 'Vorlage'), true],
                    [
                      matchers.call(rest.requestSaga, 'entities/2.0/templates/User/abc', {
                        method: 'PATCH',
                        body: expectedEntity
                      }),
                      throwError(new Error('Failed'))
                    ],
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.updateEntityTemplate.error': 'Fehler',
                        'client.admin.entities.action.updateEntityTemplate.label': 'Vorlage aktualisieren'
                      }
                    ]
                  ])
                  .call(onError, {title: 'Vorlage aktualisieren', message: 'Fehler'})
                  .run()
              })

              test('handle update entity template with no active template', () => {
                return expectSaga(sagas.updateEntityTemplate, {
                  payload: {activeEntityTemplate: undefined, entity, onSuccess, onError}
                })
                  .provide([
                    [
                      matchers.select(sagas.textResourceSelector),
                      {
                        'client.admin.entities.action.updateEntityTemplate.noActiveTemplateError':
                          'Keine Vorlage ausgewählt',
                        'client.admin.entities.action.updateEntityTemplate.label': 'Vorlage aktualisieren'
                      }
                    ]
                  ])
                  .call(onError, {title: 'Vorlage aktualisieren', message: 'Keine Vorlage ausgewählt'})
                  .run()
              })
            })
          })
        })
      })
    })
  })
})
