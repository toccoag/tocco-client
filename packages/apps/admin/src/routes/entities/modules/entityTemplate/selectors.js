export const isLoadingEntityTemplates = (state, entityName) =>
  typeof state.entities.entityTemplate.entityTemplates[entityName] === 'undefined'

export const isLoadingTemplateValues = state =>
  Boolean(state.entities.entityTemplate.activeEntityTemplate) &&
  !state.entities.entityTemplate.activeTemplateDefaultValues
