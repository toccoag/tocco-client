export const UNLOAD_VIEW = 'entityTemplate/UNLOAD_VIEW'
export const FETCH_ENTITY_TEMPLATES = 'entityTemplate/FETCH_ENTITY_TEMPLATES'
export const SET_ENTITY_TEMPLATES = 'entityTemplate/SET_ENTITY_TEMPLATES'
export const SET_ACTIVE_ENTITY_TEMPLATE = 'entityTemplate/SET_ACTIVE_ENTITY_TEMPLATE'
export const FETCH_TEMPLATE_VALUES = 'entityTemplate/FETCH_TEMPLATE_VALUES'
export const SET_ACTIVE_TEMPLATE_DEFAULT_VALUES = 'entityTemplate/SET_ACTIVE_TEMPLATE_DEFAULT_VALUES'
export const SET_ENTITY = 'entityTemplate/SET_ENTITY'
export const CREATE_ENTITY_TEMPLATE = 'entityTemplate/CREATE_ENTITY_TEMPLATE'
export const DELETE_ENTITY_TEMPLATE = 'entityTemplate/DELETE_ENTITY_TEMPLATE'
export const UPDATE_ENTITY_TEMPLATE = 'entityTemplate/UPDATE_ENTITY_TEMPLATE'

export const unloadView = () => ({
  type: UNLOAD_VIEW
})

export const fetchEntityTemplates = entityName => ({
  type: FETCH_ENTITY_TEMPLATES,
  payload: {
    entityName
  }
})

export const setEntityTemplates = (entityName, entityTemplates) => ({
  type: SET_ENTITY_TEMPLATES,
  payload: {
    entityName,
    entityTemplates
  }
})

export const setActiveEntityTemplate = activeEntityTemplate => ({
  type: SET_ACTIVE_ENTITY_TEMPLATE,
  payload: {
    activeEntityTemplate
  }
})

export const fetchTemplateValues = (entityName, templateId) => ({
  type: FETCH_TEMPLATE_VALUES,
  payload: {
    entityName,
    templateId
  }
})

export const setTemplateDefaultValues = activeTemplateDefaultValues => ({
  type: SET_ACTIVE_TEMPLATE_DEFAULT_VALUES,
  payload: {
    activeTemplateDefaultValues
  }
})

export const setEntity = entity => ({
  type: SET_ENTITY,
  payload: {
    entity
  }
})

export const createEntityTemplate = (entityName, label, entity, onSuccess, onError) => ({
  type: CREATE_ENTITY_TEMPLATE,
  payload: {
    entityName,
    label,
    entity,
    onSuccess,
    onError
  }
})

export const deleteEntityTemplate = (templateKey, entityName, navigationStrategy) => ({
  type: DELETE_ENTITY_TEMPLATE,
  payload: {
    templateKey,
    entityName,
    navigationStrategy
  }
})

export const updateEntityTemplate = (activeEntityTemplate, entity, onSuccess, onError) => ({
  type: UPDATE_ENTITY_TEMPLATE,
  payload: {
    activeEntityTemplate,
    entity,
    onSuccess,
    onError
  }
})
