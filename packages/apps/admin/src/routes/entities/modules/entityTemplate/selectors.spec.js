import {isLoadingEntityTemplates, isLoadingTemplateValues} from './selectors'

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('modules', () => {
        describe('entityTemplate', () => {
          describe('selectors', () => {
            describe('isLoadingEntityTemplates', () => {
              const entityName = 'User'
              test('should not be loading if an empty array is set as value', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      entityTemplates: {
                        User: []
                      }
                    }
                  }
                }
                expect(isLoadingEntityTemplates(state, entityName)).to.be.false
              })

              test('should not be loading if an array is set as value', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      entityTemplates: {
                        User: [{key: '1'}, {key: '2'}]
                      }
                    }
                  }
                }
                expect(isLoadingEntityTemplates(state, entityName)).to.be.false
              })

              test('should be loading if entity name is not a valid key', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      entityTemplates: {
                        Address: [{key: '1'}, {key: '2'}]
                      }
                    }
                  }
                }
                expect(isLoadingEntityTemplates(state, entityName)).to.be.true
              })

              test('should be loading if undefined is set', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      entityTemplates: {
                        User: undefined
                      }
                    }
                  }
                }
                expect(isLoadingEntityTemplates(state, entityName)).to.be.true
              })
            })

            describe('isLoadingTemplateValues', () => {
              test('should not be loading when not entity template is active', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      activeEntityTemplate: null,
                      activeTemplateDefaultValues: []
                    }
                  }
                }

                expect(isLoadingTemplateValues(state)).to.be.false
              })

              test('should not be loading when default values are already set', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      activeEntityTemplate: {key: 1},
                      activeTemplateDefaultValues: []
                    }
                  }
                }

                expect(isLoadingTemplateValues(state)).to.be.false
              })

              test('should be loading when entity template is active but no default values are set', () => {
                const state = {
                  entities: {
                    entityTemplate: {
                      activeEntityTemplate: {key: 1},
                      activeTemplateDefaultValues: null
                    }
                  }
                }

                expect(isLoadingTemplateValues(state)).to.be.true
              })
            })
          })
        })
      })
    })
  })
})
