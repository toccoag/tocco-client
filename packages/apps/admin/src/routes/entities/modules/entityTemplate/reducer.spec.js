import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  entityTemplates: {},
  activeEntityTemplate: null,
  activeTemplateDefaultValues: null,
  entity: null
}

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('modules', () => {
        describe('entityTemplate', () => {
          describe('reducer', () => {
            test('should create a valid initial state', () => {
              expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
            })

            describe('SET_ENTITY_TEMPLATES', () => {
              test('should set first entity template properly', () => {
                const stateBefore = {
                  entityTemplates: {}
                }

                const expectedStateAfter = {
                  entityTemplates: {User: [{key: 1}]}
                }

                expect(reducer(stateBefore, actions.setEntityTemplates('User', [{key: 1}]))).to.deep.equal(
                  expectedStateAfter
                )
              })
              test('should keep existing entity templates', () => {
                const stateBefore = {
                  entityTemplates: {Address: [{key: 1}]}
                }

                const expectedStateAfter = {
                  entityTemplates: {Address: [{key: 1}], User: [{key: 2}]}
                }

                expect(reducer(stateBefore, actions.setEntityTemplates('User', [{key: 2}]))).to.deep.equal(
                  expectedStateAfter
                )
              })
              test('should overwrite entity templates if already set', () => {
                const stateBefore = {
                  entityTemplates: {User: [{key: 1}]}
                }

                const expectedStateAfter = {
                  entityTemplates: {User: [{key: 2}]}
                }

                expect(reducer(stateBefore, actions.setEntityTemplates('User', [{key: 2}]))).to.deep.equal(
                  expectedStateAfter
                )
              })
            })
          })
        })
      })
    })
  })
})
