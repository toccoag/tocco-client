import {combineReducers} from 'redux'

import EntitiesRoute from './components/EntitiesRoute'
import entityTemplate from './modules/entityTemplate'
import entityTemplateSagas from './modules/entityTemplate/sagas'
import path from './modules/path'
import sagas from './modules/path/sagas'

export default {
  container: EntitiesRoute,
  reducers: {
    entities: combineReducers({path, entityTemplate})
  },
  sagas: [sagas, entityTemplateSagas]
}
