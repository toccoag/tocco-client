export const addDefaultValues = (defaultValues, defaultValuesToAdd) =>
  defaultValuesToAdd.reduce((acc, value) => (acc.some(v => v.id === value.id) ? acc : [...acc, value]), defaultValues)
