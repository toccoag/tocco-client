import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'
import {ignoreDefaultNavigationStrategy} from 'tocco-docs-browser/src/main'
import {AdminLink as StyledLink, Typography} from 'tocco-ui'
import {queryString as queryStringUtil} from 'tocco-util'

import {createHref} from '../../../utils/routing'

const DetailLinkRelativeWithoutIntl = ({entityKey, entityModel, children, relation, intl, target}) => {
  const msg = id => intl.formatMessage({id})

  return (
    <StyledLink
      aria-label={msg('client.component.navigationStrategy.detailLinkRelative')}
      to={`../${relation ? relation + '/' : ''}${entityKey}`}
      target={target}
    >
      {children}
    </StyledLink>
  )
}

const DetailLinkRelative = injectIntl(DetailLinkRelativeWithoutIntl)

export const RelationLink = ({entityName, entityKey, relationName, children, ...props}) => (
  <StyledLink to={`/e/${entityName}/${entityKey}/${relationName}`} target="_blank" {...props}>
    {children}
  </StyledLink>
)

export const DetailLink = ({entityName, entityKey, children, ...props}) => (
  <StyledLink to={`/e/${entityName}/${entityKey}`} target="_blank" {...props}>
    {children}
  </StyledLink>
)

export const ListLink = ({entityName, entityKeys, children}) => {
  if (ignoreDefaultNavigationStrategy(entityName)) {
    return <Typography.Span>{children}</Typography.Span>
  }

  const queryString = entityKeys?.length && 'tql=KEYS(' + entityKeys.join(',') + ')'

  return (
    <StyledLink
      to={{
        pathname: `/e/${entityName}/list`,
        search: `?${queryString}`
      }}
      target="_blank"
    >
      {children}
    </StyledLink>
  )
}

ListLink.propTypes = {
  entityName: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  entityKeys: PropTypes.arrayOf(PropTypes.string)
}

export const ListOrDetailLink = ({entityName, entityKeys, children}) => {
  if (entityKeys?.length === 1) {
    return (
      <DetailLink entityName={entityName} entityKey={entityKeys[0]}>
        {children}
      </DetailLink>
    )
  }
  return (
    <ListLink entityName={entityName} entityKeys={entityKeys}>
      {children}
    </ListLink>
  )
}

ListOrDetailLink.propTypes = {
  entityName: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  entityKeys: PropTypes.arrayOf(PropTypes.string)
}

const navigationStrategy = (navigate, basename) => {
  const navigateToCreateRelative = (relationName, state) => {
    if (relationName) {
      navigate(`${relationName}/create`, {
        state
      })
    } else {
      navigate('../create', {
        state
      })
    }
  }

  const navigateToActionRelative = (definition, selection) => {
    const search = queryStringUtil.toQueryString({
      selection,
      actionProperties: definition.properties
    })
    navigate(
      {
        pathname: '../action/' + definition.appId,
        search
      },
      {
        state: {
          definition,
          selection
        }
      }
    )
  }

  const navigateToPath = ({pathname, search, hash, inNewTab = false}) => {
    if (inNewTab) {
      const url = createHref({pathname, search, hash}, {basename})
      window.open(url, '_blank')
    } else {
      navigate({pathname})
    }
  }

  const openDetail = (entityName, key, inNewTab = true) => {
    const pathname = `/e/${entityName}/${key}`
    navigateToPath({pathname, inNewTab})
  }

  return {
    RelationLink,
    DetailLink,
    ListLink,
    ListOrDetailLink,
    DetailLinkRelative,
    navigateToCreateRelative,
    navigateToActionRelative,
    navigateToPath,
    openDetail
  }
}

export default navigationStrategy

DetailLinkRelativeWithoutIntl.propTypes = {
  entityKey: PropTypes.string.isRequired,
  entityModel: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  relation: PropTypes.string,
  intl: PropTypes.object.isRequired,
  target: PropTypes.string
}

DetailLink.propTypes = {
  entityName: PropTypes.string.isRequired,
  entityKey: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired
}

RelationLink.propTypes = {
  entityName: PropTypes.string.isRequired,
  entityKey: PropTypes.string.isRequired,
  relationName: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired
}
