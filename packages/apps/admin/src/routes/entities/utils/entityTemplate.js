const getValue = ({type, value}) => {
  if (type === 'entity') {
    return value.key
  }

  if (type === 'entity-list') {
    return Array.isArray(value) ? value.map(v => v.key) : value
  }

  return value
}

export const transformEntityTemplateValuesToDefaultValues = templateValues => {
  const defaultValues = Object.keys(templateValues.paths).map(key => {
    const path = templateValues.paths[key]
    const value = getValue(path)
    return {
      id: key,
      value
    }
  })

  return defaultValues
}

export const getTemplateKeyFromUrl = url => url.split('/').slice(-1)[0]
