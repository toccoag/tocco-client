import {addDefaultValues} from './defaultValues'

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('utils', () => {
        describe('defaultValues', () => {
          describe('addDefaultValues', () => {
            test('should not overwrite values from first values', () => {
              const valuesA = [{id: 'field1', value: 'a'}]
              const valuesB = [{id: 'field1', value: 'b'}]

              const expectedValues = [{id: 'field1', value: 'a'}]

              expect(addDefaultValues(valuesA, valuesB)).to.deep.eql(expectedValues)
            })

            test('should keep values from first values', () => {
              const valuesA = [
                {id: 'field1', value: 'a'},
                {id: 'field2', value: 'c'}
              ]
              const valuesB = []

              const expectedValues = [
                {id: 'field1', value: 'a'},
                {id: 'field2', value: 'c'}
              ]

              expect(addDefaultValues(valuesA, valuesB)).to.deep.eql(expectedValues)
            })

            test('should append new values from second values', () => {
              const valuesA = [
                {id: 'field1', value: 'a'},
                {id: 'field2', value: 'c'}
              ]
              const valuesB = [{id: 'field3', value: 'd'}]

              const expectedValues = [
                {id: 'field1', value: 'a'},
                {id: 'field2', value: 'c'},
                {id: 'field3', value: 'd'}
              ]

              expect(addDefaultValues(valuesA, valuesB)).to.deep.eql(expectedValues)
            })
          })
        })
      })
    })
  })
})
