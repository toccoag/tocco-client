import {getTemplateKeyFromUrl, transformEntityTemplateValuesToDefaultValues} from './entityTemplate'

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('utils', () => {
        describe('entityTemplate', () => {
          describe('transformEntityTemplateValuesToDefaultValues', () => {
            test('should transform simple template values for default values', () => {
              const templateValues = {
                paths: {
                  firstname: {
                    type: 'string',
                    writable: null,
                    value: 'Vorname'
                  },
                  lastname: {
                    type: 'string',
                    writable: null,
                    value: 'Nachname'
                  }
                }
              }
              const expectedDefaultValues = [
                {id: 'firstname', value: 'Vorname'},
                {id: 'lastname', value: 'Nachname'}
              ]
              expect(transformEntityTemplateValuesToDefaultValues(templateValues)).to.deep.equal(expectedDefaultValues)
            })

            test('should handle document values', () => {
              const templateValues = {
                paths: {
                  document: {
                    type: 'document',
                    writable: null,
                    value: {mimeType: 'image/png'}
                  },
                  binary: {
                    type: 'binary',
                    writable: null,
                    value: {mimeType: 'image/png'}
                  },
                  image: {
                    type: 'image',
                    writable: null,
                    value: {mimeType: 'image/png'}
                  },
                  'named-upload': {
                    type: 'named-upload',
                    writable: null,
                    value: {mimeType: 'image/png'}
                  },
                  upload: {
                    type: 'upload',
                    writable: null,
                    value: {mimeType: 'image/png'}
                  }
                }
              }
              const expectedDefaultValues = [
                {
                  id: 'document',
                  value: {mimeType: 'image/png'}
                },
                {
                  id: 'binary',
                  value: {mimeType: 'image/png'}
                },
                {
                  id: 'image',
                  value: {mimeType: 'image/png'}
                },
                {
                  id: 'named-upload',
                  value: {mimeType: 'image/png'}
                },
                {
                  id: 'upload',
                  value: {mimeType: 'image/png'}
                }
              ]
              expect(transformEntityTemplateValuesToDefaultValues(templateValues)).to.deep.equal(expectedDefaultValues)
            })
          })

          test('should transform complicated template values for default values', () => {
            const templateValues = {
              paths: {
                relUser_code1: {
                  type: 'entity-list',
                  writable: null,
                  value: [
                    {
                      key: '301',
                      model: 'User_code1',
                      version: 0,
                      paths: {}
                    },
                    {
                      key: '1',
                      model: 'User_code1',
                      version: 1,
                      paths: {}
                    }
                  ]
                },
                birthdate: {
                  type: 'birthdate',
                  writable: null,
                  value: '2023-06-29'
                },
                publish: {
                  type: 'boolean',
                  writable: null,
                  value: true
                },
                relNationality: {
                  type: 'entity',
                  writable: null,
                  value: {
                    key: '107',
                    model: 'Country',
                    version: 1,
                    paths: {}
                  }
                }
              }
            }
            const expectedDefaultValues = [
              {id: 'relUser_code1', value: ['301', '1']},
              {id: 'birthdate', value: '2023-06-29'},
              {id: 'publish', value: true},
              {id: 'relNationality', value: '107'}
            ]
            expect(transformEntityTemplateValuesToDefaultValues(templateValues)).to.deep.equal(expectedDefaultValues)
          })
        })

        describe('getTemplateKeyFromUrl', () => {
          test('should extract key from last part of URL', () => {
            expect(
              getTemplateKeyFromUrl('http://localhost:8080/nice2/rest/entities/2.0/Entity_template/1743')
            ).to.equal('1743')
          })
        })
      })
    })
  })
})
