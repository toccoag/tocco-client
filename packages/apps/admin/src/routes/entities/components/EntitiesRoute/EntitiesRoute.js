import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {Navigate, Route, Routes, useLocation} from 'react-router-dom'
import {errorLogging} from 'tocco-app-extensions'

import Action from '../../subroutes/action'
import Entity from '../../subroutes/entity'
import {currentViewPropType} from '../../utils/propTypes'
import Breadcrumbs from '../Breadcrumbs'
import ErrorView from '../ErrorView'

import {StyledWrapper, StyledBreadcrumbs, StyledContent} from './StyledComponents'

const EntitiesRoute = ({loadCurrentRoute, currentViewInfo}) => {
  const location = useLocation()
  const {pathname} = location

  useEffect(() => {
    loadCurrentRoute(pathname)
  }, [pathname, loadCurrentRoute])

  const content = currentViewInfo?.error ? (
    <ErrorView location={location} />
  ) : (
    <Routes>
      <Route path="action/:actionId" element={<Action location={location} />} />
      <Route path=":entity/*" element={<Entity />} />
      <Route exact path="/" element={<Navigate to="/dashboard" replace />} />
    </Routes>
  )

  return (
    <StyledWrapper>
      <errorLogging.ErrorBoundary>
        <StyledBreadcrumbs>
          <Breadcrumbs updateDocumentTitle showToccoHome />
        </StyledBreadcrumbs>
      </errorLogging.ErrorBoundary>
      <StyledContent>{content}</StyledContent>
    </StyledWrapper>
  )
}

EntitiesRoute.propTypes = {
  loadCurrentRoute: PropTypes.func.isRequired,
  currentViewInfo: currentViewPropType
}

export default EntitiesRoute
