import styled from 'styled-components'
import {StyledButton, scale} from 'tocco-ui'

export const StyledButtonsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: ${scale.space(0.3)} 0 0;

  ${StyledButton} {
    margin-right: 0;
  }
`
