import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {
  deleteEntityTemplate,
  fetchEntityTemplates,
  setActiveEntityTemplate
} from '../../../modules/entityTemplate/actions'
import {isLoadingEntityTemplates} from '../../../modules/entityTemplate/selectors'

import EntityTemplateSidepanel from './EntityTemplateSidepanel'

const mapActionCreators = {
  fetchEntityTemplates,
  setActiveEntityTemplate,
  deleteEntityTemplate
}

const mapStateToProps = (state, props) => ({
  isLoadingEntityTemplates: isLoadingEntityTemplates(state, props.entityName),
  entityTemplates: state.entities.entityTemplate.entityTemplates[props.entityName] || [],
  activeEntityTemplate: state.entities.entityTemplate.activeEntityTemplate
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityTemplateSidepanel))
