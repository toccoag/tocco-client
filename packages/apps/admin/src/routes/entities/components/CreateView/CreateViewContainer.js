import _get from 'lodash/get'
import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actionEmitter} from 'tocco-app-extensions'
import {chooseDocument} from 'tocco-docs-browser/src/main'

import {saveUserPreferences} from '../../../../modules/preferences/actions'
import {getRoutingUserConfirmation, setPendingChanges} from '../../../../modules/session/actions'
import {unloadView, setEntity, updateEntityTemplate} from '../../modules/entityTemplate/actions'
import {isLoadingTemplateValues} from '../../modules/entityTemplate/selectors'

import CreateView from './CreateView'

const mapActionCreators = {
  chooseDocument: chooseDocument.actions.chooseDocument,
  dispatchEmittedAction: actionEmitter.dispatchEmittedAction,
  setPendingChanges,
  setEntity,
  unloadView,
  setEntityTemplateSidepanelCollapsed: collapsed =>
    saveUserPreferences({'admin.create.entityTemplateSidepanelCollapsed': collapsed}),
  updateEntityTemplate,
  showPendingChangesModal: getRoutingUserConfirmation
}

const mapStateToProps = (state, props) => ({
  currentViewInfo: state.entities.path.currentViewInfos[props.location.pathname],
  activeTemplateDefaultValues: state.entities.entityTemplate.activeTemplateDefaultValues,
  isLoadingTemplateValues: isLoadingTemplateValues(state),
  entityTemplateSidepanelCollapsed: _get(
    state.preferences.userPreferences,
    'admin.create.entityTemplateSidepanelCollapsed',
    false
  ),
  activeEntityTemplate: state.entities.entityTemplate.activeEntityTemplate,
  entity: state.entities.entityTemplate.entity,
  baseRoute: state.input.baseRoute
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(CreateView))
