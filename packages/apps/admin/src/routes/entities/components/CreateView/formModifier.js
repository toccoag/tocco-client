import {form} from 'tocco-app-extensions'

export function modifyFormDefinition(formDefinition, intl, activeEntityTemplate) {
  const createEntityTemplate = {
    id: 'entity-template-create',
    appId: 'entity-template-create',
    label: intl.formatMessage({id: 'client.admin.entities.action.createEntityTemplate.label'}),
    componentType: 'action',
    children: [],
    actionType: 'custom',
    useLabel: 'YES',
    buttonType: 'REGULAR',
    canExecuteOnPendingChanges: true
  }
  const formDefinitionWithCreate = form.addAction(formDefinition, createEntityTemplate, ['save'])

  const updateEntityTemplate = {
    id: 'entity-template-update',
    label: intl.formatMessage({id: 'client.admin.entities.action.updateEntityTemplate.label'}),
    componentType: 'action',
    children: [],
    actionType: 'custom',
    useLabel: 'YES',
    buttonType: 'REGULAR',
    canExecuteOnPendingChanges: true
  }

  return activeEntityTemplate
    ? form.addAction(formDefinitionWithCreate, updateEntityTemplate, ['save', 'entity-template-create'])
    : formDefinitionWithCreate
}
