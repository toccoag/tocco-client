import styled from 'styled-components'
import {scale, themeSelector} from 'tocco-ui'

export const StyledSidepanelWrapper = styled.div`
  background-color: ${themeSelector.color('paper')};
  padding: ${scale.space(-1.1)} ${scale.space(-0.375)} 0 ${scale.space(0)};
  position: relative;
  height: calc(100% - 50px); /* subtract height of StyledSidepanelHeader */
  overflow-y: auto;
`
