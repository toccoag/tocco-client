import _get from 'lodash/get'
import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {flushSync} from 'react-dom'
import {useLocation, useNavigate} from 'react-router-dom'
import styled from 'styled-components'
import DocsBrowserApp from 'tocco-docs-browser/src/main'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {
  theme,
  scale,
  SidepanelContainer,
  Sidepanel,
  SidepanelHeader,
  SidepanelMainContent,
  LoadMask,
  usePrompt
} from 'tocco-ui'

import {addDefaultValues} from '../../utils/defaultValues'
import navigationStrategy from '../../utils/navigationStrategy'
import {currentViewPropType} from '../../utils/propTypes'

import Action from './Action'
import EntityTemplateSidepanel from './EntityTemplateSidepanel'
import {modifyFormDefinition} from './formModifier'
import {StyledSidepanelWrapper} from './StyledComponents'

const StyledEntityDetailAppWrapper = styled.div`
  margin: 0;
  background-color: ${theme.color('paper')};
  padding: 0 0 0 ${scale.space(0)};
  height: calc(100dvh - 75px); /* ~75px the height of header and breadcrumbs */
  width: 100%;
`

const CreateView = props => {
  const {
    currentViewInfo,
    intl,
    chooseDocument,
    dispatchEmittedAction,
    setPendingChanges,
    setEntity,
    unloadView,
    isLoadingTemplateValues,
    activeTemplateDefaultValues,
    setEntityTemplateSidepanelCollapsed,
    entityTemplateSidepanelCollapsed,
    updateEntityTemplate,
    activeEntityTemplate,
    entity,
    showPendingChangesModal,
    baseRoute
  } = props
  const msg = id => intl.formatMessage({id})
  const location = useLocation()
  const navigate = useNavigate()

  const stateDefaultValues = _get(location, 'state.defaultValues', [])
  const [touched, setTouched] = useState(false)

  const mode = 'create'

  useEffect(() => {
    return () => {
      setPendingChanges(false)
      unloadView()
    }
  }, [setPendingChanges, unloadView])

  usePrompt({
    when: touched,
    message: msg('client.admin.entities.confirmTouchedFormLeave'),
    showPendingChangesModal
  })

  if (!currentViewInfo) {
    return null
  }

  const {model, reverseRelation, parentKey} = currentViewInfo
  const entityName = model.name

  const isMultiReverseRelation = _get(model, `paths.${reverseRelation}.multi`, false)
  const relationDefaultValue =
    reverseRelation && parentKey ? [{id: reverseRelation, value: isMultiReverseRelation ? [parentKey] : parentKey}] : []

  /**
   * Default values hierarchy
   *  - reverse relation value
   *  - active template values
   *  - default values (aka copied values)
   */
  const defaultValues = addDefaultValues(
    addDefaultValues(relationDefaultValue, activeTemplateDefaultValues || []),
    stateDefaultValues
  )

  const handleEntityCreated = ({id, followUp}) => {
    /**
     * `touched` on the Prompt component has to be already set to false before the
     * navigate happens.
     * Otherwise the prompt will show up evertime after the user has created the new entity.
     */
    flushSync(() => {
      setTouched(false)
    })
    navigate({
      pathname: `../${id}/detail`,
      search: followUp?.action ? `?action=${followUp?.action}` : ''
    })
  }

  const handleToucheChanged = ({touched: changedTouched}) => {
    setTouched(changedTouched)
    setPendingChanges(changedTouched)
  }
  const handleFormValuesChanged = ({entity: updatedEntity}) => {
    setEntity(updatedEntity)
  }
  const handleEntityTemplateUpdate = (_definition, _selection, _parent, _params, _config, onSuccess, onError) => {
    updateEntityTemplate(activeEntityTemplate, entity, onSuccess, onError)
  }

  return (
    <SidepanelContainer
      sidepanelPosition="left"
      scrollBehaviour="inline"
      setSidepanelCollapsed={setEntityTemplateSidepanelCollapsed}
      sidepanelCollapsed={entityTemplateSidepanelCollapsed}
    >
      <Sidepanel>
        <SidepanelHeader />
        <StyledSidepanelWrapper>
          <EntityTemplateSidepanel
            entityName={entityName}
            navigationStrategy={navigationStrategy(navigate, baseRoute)}
          />
        </StyledSidepanelWrapper>
      </Sidepanel>
      <SidepanelMainContent>
        <StyledEntityDetailAppWrapper>
          <LoadMask
            required={[!isLoadingTemplateValues]}
            loadingText={msg('client.admin.entities.create.entityTemplateLoading')}
          >
            <EntityDetailApp
              entityName={entityName}
              formName={entityName}
              mode={mode}
              defaultValues={defaultValues}
              emitAction={action => {
                dispatchEmittedAction(action)
              }}
              navigationStrategy={navigationStrategy(navigate, baseRoute)}
              chooseDocument={chooseDocument}
              onEntityCreated={handleEntityCreated}
              onTouchedChange={handleToucheChanged}
              onFormValuesChange={handleFormValuesChanged}
              listApp={EntityListApp}
              docsApp={DocsBrowserApp}
              actionAppComponent={Action}
              customActions={{
                'entity-template-update': handleEntityTemplateUpdate
              }}
              modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, intl, activeEntityTemplate)}
            />
          </LoadMask>
        </StyledEntityDetailAppWrapper>
      </SidepanelMainContent>
    </SidepanelContainer>
  )
}

CreateView.propTypes = {
  intl: PropTypes.object,
  currentViewInfo: currentViewPropType,
  chooseDocument: PropTypes.func.isRequired,
  dispatchEmittedAction: PropTypes.func.isRequired,
  setPendingChanges: PropTypes.func.isRequired,
  unloadView: PropTypes.func.isRequired,
  setEntity: PropTypes.func.isRequired,
  setEntityTemplateSidepanelCollapsed: PropTypes.func.isRequired,
  entityTemplateSidepanelCollapsed: PropTypes.bool,
  isLoadingTemplateValues: PropTypes.bool,
  activeTemplateDefaultValues: PropTypes.array,
  updateEntityTemplate: PropTypes.func.isRequired,
  activeEntityTemplate: PropTypes.object,
  entity: PropTypes.object,
  showPendingChangesModal: PropTypes.func.isRequired,
  baseRoute: PropTypes.string
}

export default CreateView
