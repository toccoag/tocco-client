import PropTypes from 'prop-types'
import {useState} from 'react'
import {selection as selectionUtil} from 'tocco-app-extensions'
import SimpleFormApp from 'tocco-simple-form/src/main'

import {createSimpleFormDefinition} from './formDefinition'

const CreateEntityTemplate = ({intl, createEntityTemplate, entity, selection, onSuccess, onError, closeModal}) => {
  const [label, setLabel] = useState('')

  const entityName = selection.entityName

  const saveEntityTemplate = () => {
    createEntityTemplate(entityName, label, entity, onSuccess, onError)
    closeModal('action-entity-template-create')
  }

  const handleChange = ({values}) => {
    setLabel(values.label)
  }

  return (
    <div>
      <SimpleFormApp
        form={createSimpleFormDefinition(intl)}
        labelPosition="inside"
        mode="create"
        onChange={handleChange}
        onSubmit={saveEntityTemplate}
      />
    </div>
  )
}

CreateEntityTemplate.propTypes = {
  intl: PropTypes.object.isRequired,
  selection: selectionUtil.propType,
  createEntityTemplate: PropTypes.func.isRequired,
  entity: PropTypes.object.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired
}

export default CreateEntityTemplate
