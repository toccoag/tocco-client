import {lazy} from 'react'
import {actions} from 'tocco-app-extensions'

const actionMap = {
  'entity-template-create': lazy(() => import(/* webpackChunkName: "actions" */ './actions/CreateEntityTemplate/index'))
}

const LazyAction = actions.actionFactory(actionMap)

export default LazyAction
