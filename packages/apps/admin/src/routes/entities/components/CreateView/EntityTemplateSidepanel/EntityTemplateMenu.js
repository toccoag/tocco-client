import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {BallMenu, MenuItem} from 'tocco-ui'

const EntityTemplateMenu = ({navigationStrategy, deleteEntityTemplate, templateKey}) => (
  <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-entity-template-ballmenu'}}>
    <MenuItem
      onClick={() => navigationStrategy.openDetail('Entity_template', templateKey)}
      data-cy="menuitem-entity-template-edit"
    >
      <FormattedMessage id="client.admin.entities.create.entityTemplate.settings.edit" />
    </MenuItem>
    <MenuItem onClick={deleteEntityTemplate} data-cy="menuitem-entity-template-delete">
      <FormattedMessage id="client.admin.entities.create.entityTemplate.settings.delete" />
    </MenuItem>
  </BallMenu>
)

EntityTemplateMenu.propTypes = {
  templateKey: PropTypes.string,
  deleteEntityTemplate: PropTypes.func,
  navigationStrategy: PropTypes.objectOf(PropTypes.func)
}

export default EntityTemplateMenu
