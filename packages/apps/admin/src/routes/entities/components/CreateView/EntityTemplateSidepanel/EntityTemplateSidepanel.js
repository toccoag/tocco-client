import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import {LoadingSpinner, SidepanelHorizontalButton, Typography} from 'tocco-ui'

import EntityTemplateMenu from './EntityTemplateMenu'
import {StyledButtonWrapper, StyledLoaderWrapper, StyledMessageWrapper} from './StyledComponents'

const EntityTemplateSidepanel = ({
  entityName,
  isLoadingEntityTemplates,
  activeEntityTemplate,
  setActiveEntityTemplate,
  fetchEntityTemplates,
  deleteEntityTemplate,
  entityTemplates,
  navigationStrategy
}) => {
  useEffect(() => {
    fetchEntityTemplates(entityName)
  }, [fetchEntityTemplates, entityName])

  if (isLoadingEntityTemplates) {
    return (
      <StyledLoaderWrapper data-cy="entity-template-loading">
        <LoadingSpinner size="1.8rem" />
        <Typography.Span>
          <FormattedMessage id="client.admin.entities.create.entityTemplatesLoading" />
        </Typography.Span>
      </StyledLoaderWrapper>
    )
  }

  if (entityTemplates.length === 0) {
    return (
      <StyledMessageWrapper data-cy="entity-template-empty">
        <Typography.I>
          <FormattedMessage id="client.admin.entities.create.noEntityTemplates" />
        </Typography.I>
      </StyledMessageWrapper>
    )
  }

  return (
    <StyledButtonWrapper data-cy="entity-template-list">
      {entityTemplates.map(template => (
        <SidepanelHorizontalButton
          key={template.key}
          primaryKey={template.key}
          label={template.paths.label.value}
          title={
            !template.paths.description.value
              ? template.paths.label.value
              : `${template.paths.label.value}: ${template.paths.description.value}`
          }
          active={activeEntityTemplate?.key === template.key}
          setActive={active => setActiveEntityTemplate(active ? template : null)}
          showAddRemoveBtn={false}
          menuElement={
            <EntityTemplateMenu
              templateKey={template.key}
              navigationStrategy={navigationStrategy}
              deleteEntityTemplate={() => deleteEntityTemplate(template.key, entityName, navigationStrategy)}
            />
          }
        />
      ))}
    </StyledButtonWrapper>
  )
}

EntityTemplateSidepanel.propTypes = {
  entityName: PropTypes.string.isRequired,
  isLoadingEntityTemplates: PropTypes.bool,
  activeEntityTemplate: PropTypes.object,
  setActiveEntityTemplate: PropTypes.func.isRequired,
  fetchEntityTemplates: PropTypes.func.isRequired,
  deleteEntityTemplate: PropTypes.func.isRequired,
  navigationStrategy: PropTypes.object.isRequired,
  entityTemplates: PropTypes.array
}

export default EntityTemplateSidepanel
