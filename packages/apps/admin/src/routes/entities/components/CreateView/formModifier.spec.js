import _get from 'lodash/get'
import {IntlStub} from 'tocco-test-util'

import {modifyFormDefinition} from './formModifier'

describe('admin', () => {
  describe('routes', () => {
    describe('entities', () => {
      describe('components', () => {
        describe('CreateView', () => {
          describe('formModifier', () => {
            const formDefinition = {
              componentType: 'form',
              children: [
                {
                  id: 'main-action-bar',
                  componentType: 'action-bar',
                  children: [
                    {
                      id: 'save',
                      componentType: 'action'
                    }
                  ]
                }
              ]
            }
            const intl = IntlStub

            describe('entity-template-create', () => {
              test('should add create entity template action to main action bar after save', () => {
                const modifiedFormDefinition = modifyFormDefinition(formDefinition, intl, null)
                const createEntityTemplateAction = _get(modifiedFormDefinition, [
                  'children',
                  '0', // main action bar
                  'children',
                  '1' // create entity template action
                ])
                expect(createEntityTemplateAction.id).to.eq('entity-template-create')
                expect(createEntityTemplateAction.appId).to.eq('entity-template-create')
              })
            })

            describe('entity-template-update', () => {
              test('should not add update entity template action when no active template is set', () => {
                const modifiedFormDefinition = modifyFormDefinition(formDefinition, intl, null)
                const actions = _get(modifiedFormDefinition, [
                  'children',
                  '0', // main action bar
                  'children'
                ])
                expect(actions.length).to.eq(2)
                expect(actions[0].id).to.eq('save')
                expect(actions[1].id).to.eq('entity-template-create')
              })

              test('should add update entity template action when active template is set', () => {
                const modifiedFormDefinition = modifyFormDefinition(formDefinition, intl, {})
                const actions = _get(modifiedFormDefinition, [
                  'children',
                  '0', // main action bar
                  'children'
                ])
                expect(actions.length).to.eq(3)
                expect(actions[0].id).to.eq('save')
                expect(actions[1].id).to.eq('entity-template-create')
                expect(actions[2].id).to.eq('entity-template-update')
                expect(actions[2].appId).to.be.undefined
              })
            })
          })
        })
      })
    })
  })
})
