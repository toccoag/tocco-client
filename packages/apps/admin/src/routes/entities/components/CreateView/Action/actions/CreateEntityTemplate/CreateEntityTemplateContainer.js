import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {notification} from 'tocco-app-extensions'

import {createEntityTemplate} from '../../../../../modules/entityTemplate/actions'

import CreateEntityTemplate from './CreateEntityTemplate'

const mapActionCreators = {
  closeModal: notification.removeModal,
  createEntityTemplate
}

const mapStateToProps = state => ({
  entity: state.entities.entityTemplate.entity
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(CreateEntityTemplate))
