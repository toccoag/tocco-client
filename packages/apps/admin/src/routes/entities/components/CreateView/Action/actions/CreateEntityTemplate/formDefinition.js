import {form} from 'tocco-app-extensions'

export const createSimpleFormDefinition = intl => {
  const msg = id => intl.formatMessage({id})
  const formDefinition = form.createSimpleForm({
    children: [
      form.createHorizontalBox({
        children: [
          form.createVerticalBox({
            children: [
              form.createVerticalBox({
                label: msg('client.admin.entities.action.createEntityTemplate.title'),
                children: [
                  form.createFieldSet({
                    label: msg('client.admin.entities.action.createEntityTemplate.entityTemplateLabel'),
                    path: 'label',
                    validation: form.createValidation(form.createMandatoryValidation()),
                    dataType: 'string'
                  })
                ]
              })
            ]
          })
        ]
      })
    ]
  })

  return formDefinition
}
