import styled from 'styled-components'

export const StyledButtonWrapper = styled.div`
  margin-top: 0.4rem;
  margin-bottom: 0.4rem;
`

export const StyledMessageWrapper = styled.div`
  padding-left: 8px;
`

export const StyledLoaderWrapper = styled.div`
  padding-left: 8px;
  text-align: center;
  display: flex;
  flex-direction: column;
`
