import PropTypes from 'prop-types'
import {selection as selectionPropType} from 'tocco-app-extensions'
import ResourceSchedulerApp from 'tocco-resource-scheduler/src/main'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

const ResourceScheduler = ({selection, actionProperties, navigationStrategy}) => {
  return (
    <ResourceSchedulerApp
      onEventClick={({model, key}) => {
        navigationStrategy.openDetail(model, key)
      }}
      selection={selection}
      actionProperties={actionProperties}
    />
  )
}

ResourceScheduler.propTypes = {
  selection: selectionPropType.propType,
  actionProperties: PropTypes.shape({
    calendarType: PropTypes.string
  }),
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export default ResourceScheduler
