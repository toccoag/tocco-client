import {Navigate} from 'react-router-dom'

const Documents = () => <Navigate to={{pathname: '/docs'}} replace />

export default Documents
