import {lazy} from 'react'
import {actions} from 'tocco-app-extensions'

const actionMap = {
  'acl-browser': lazy(() => import(/* webpackChunkName: "actions" */ './actions/AclBrowser')),
  'address-check': lazy(() => import(/* webpackChunkName: "actions" */ './actions/AddressCheck')),
  'display-accountings': lazy(() => import(/* webpackChunkName: "actions" */ './actions/DisplayAccountings')),
  log: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Log')),
  'login-role': lazy(() => import(/* webpackChunkName: "actions" */ './actions/LoginRole')),
  'entity-model-browser': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EntityModelBrowser')),
  'event-logic-copy': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EventLogicCopy')),
  'widget-code-copy': lazy(() => import(/* webpackChunkName: "actions" */ './actions/WidgetCodeCopy')),
  'widget-visibility-status-code-copy': lazy(
    () => import(/* webpackChunkName: "actions" */ './actions/WidgetVisibilityStatusCodeCopy')
  ),
  'user-qr-action': lazy(() => import(/* webpackChunkName: "actions" */ './actions/UserQrAction')),
  'initialvalue-generator': lazy(() => import(/* webpackChunkName: "actions" */ './actions/InitialvalueGenerator')),
  'input-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/InputEdit')),
  'evaluation-view': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EvaluationView')),
  resourcescheduler: lazy(() => import(/* webpackChunkName: "actions" */ './actions/ResourceScheduler')),
  'show-output-jobs-action': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ShowOutputJobsAction')),
  delete: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Delete')),
  'data-protection-report': lazy(() => import(/* webpackChunkName: "actions" */ './actions/DataProtectionReport')),
  merge: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Merge')),
  copy: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Copy')),
  'entity-report': lazy(() => import(/* webpackChunkName: "actions" */ './actions/EntityReport')),
  documents: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Documents')),
  'password-update': lazy(() => import(/* webpackChunkName: "actions" */ './actions/PasswordUpdate')),
  'widget-config-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/WidgetConfigEdit')),
  'connect-principal': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ConnectPrincipal')),
  export: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Export')),
  'reload-configuration': lazy(() => import(/* webpackChunkName: "actions" */ './actions/Configuration')),
  'exam-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ExamEdit')),
  'restore-entity': lazy(() => import(/* webpackChunkName: "actions" */ './actions/RestoreEntity')),
  mail: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Mail')),
  'mass-mutation': lazy(() => import(/* webpackChunkName: "actions" */ './actions/MassMutation')),
  'create-api-key': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ApiKey')),
  'accept-conflict': lazy(() => import(/* webpackChunkName: "actions" */ './actions/AcceptConflict')),
  'configure-automation': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ConfigureAutomation')),
  'configure-after-automation-logic': lazy(
    () => import(/* webpackChunkName: "actions" */ './actions/ConfigureAfterAutomationLogic')
  )
}

const LazyAction = actions.actionFactory(actionMap)

export default LazyAction
