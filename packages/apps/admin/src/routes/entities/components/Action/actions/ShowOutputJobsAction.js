import PropTypes from 'prop-types'
import {Navigate} from 'react-router-dom'

const ShowOutputJobsAction = props => {
  const tql = encodeURIComponent(`entity=="${props.selection.entityName}"`)

  return (
    <Navigate
      to={{
        pathname: '/e/Output_job/list',
        search: `tql=${tql}`
      }}
      replace
    />
  )
}

ShowOutputJobsAction.propTypes = {
  selection: PropTypes.shape({
    entityName: PropTypes.string.isRequired
  }).isRequired
}

export default ShowOutputJobsAction
