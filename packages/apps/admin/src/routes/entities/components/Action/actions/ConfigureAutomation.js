import ConfigureAutomationApp from 'tocco-configure-automation/src/main'
import EntityListApp from 'tocco-entity-list/src/main'

const ConfigureAutomation = props => <ConfigureAutomationApp {...props} listApp={EntityListApp} />

export default ConfigureAutomation
