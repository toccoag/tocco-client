import ConfigureAfterAutomationLogicApp from 'tocco-configure-after-automation-logic/src/main'
import EntityListApp from 'tocco-entity-list/src/main'

const ConfigureAfterAutomationLogic = props => <ConfigureAfterAutomationLogicApp {...props} listApp={EntityListApp} />

export default ConfigureAfterAutomationLogic
