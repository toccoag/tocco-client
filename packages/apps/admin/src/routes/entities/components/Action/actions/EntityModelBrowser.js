import {Navigate} from 'react-router-dom'

const EntityModelBrowser = () => <Navigate to={{pathname: '/em'}} replace />

export default EntityModelBrowser
