import PropTypes from 'prop-types'
import {navigationStrategy as navigationStrategyUtil} from 'tocco-util'

import {currentViewPropType} from '../../utils/propTypes'
import DocsView from '../DocsView'

const DocsViewAdapter = ({currentViewInfo, initialLocation, navigationStrategy}) => (
  <DocsView
    noLeftPadding={false}
    entityName={currentViewInfo.parentModel.name}
    entityKey={currentViewInfo.parentKey}
    showActions={true}
    initialLocation={initialLocation}
    navigationStrategy={navigationStrategy}
  />
)

DocsViewAdapter.propTypes = {
  currentViewInfo: currentViewPropType,
  initialLocation: PropTypes.string,
  navigationStrategy: navigationStrategyUtil.propTypes
}

export default DocsViewAdapter
