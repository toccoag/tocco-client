import PropTypes from 'prop-types'
import {useLocation, useNavigate, useSearchParams} from 'react-router-dom'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {getModelOverviewUrl} from 'tocco-history/src/main'
import {viewPersistor} from 'tocco-util'

import baseNavigationStrategy from '../../utils/navigationStrategy'
import {currentViewPropType} from '../../utils/propTypes'
import Action from '../Action'

import DocsViewAdapter from './DocsViewAdapter'

const ListView = ({currentViewInfo, emitAction, searchFormCollapsed, saveUserPreferences, baseRoute}) => {
  const [searchParams] = useSearchParams()
  const location = useLocation()
  const navigate = useNavigate()

  if (!currentViewInfo) {
    return null
  }

  const handleRowClick = ({id}) => {
    navigate(`../${id}`)
  }

  const initialLocation = location.hash ? location.hash.substring(1) : null

  const navigationStrategy = baseNavigationStrategy(navigate, baseRoute)

  if (currentViewInfo.model.name === 'Resource') {
    return (
      <DocsViewAdapter
        currentViewInfo={currentViewInfo}
        initialLocation={initialLocation}
        navigationStrategy={navigationStrategy}
      />
    )
  }

  const history = (_definition, selection, _parent, _params, _config, _onSuccess, _onError) => {
    navigationStrategy.navigateToPath({pathname: getModelOverviewUrl(selection), inNewTab: true})
  }

  return (
    <EntityListApp
      emitAction={emitAction}
      limit={25}
      id={`${currentViewInfo.model.name}_list`}
      entityName={currentViewInfo.model.name}
      formName={searchParams.get('formName') || currentViewInfo.model.name}
      onRowClick={handleRowClick}
      {...(currentViewInfo.reverseRelation && {
        parent: {
          key: currentViewInfo.parentKey,
          reverseRelationName: currentViewInfo.reverseRelation,
          model: currentViewInfo.parentModel.name
        }
      })}
      showLink={true}
      showNumbering={true}
      navigationStrategy={navigationStrategy}
      searchFormPosition="left"
      searchFormType="admin"
      scrollBehaviour="inline"
      store={viewPersistor.viewInfoSelector(location.pathname).store}
      onStoreCreate={store => {
        viewPersistor.persistViewInfo(location.pathname, {store}, currentViewInfo.level)
      }}
      actionAppComponent={Action}
      tql={searchParams.get('tql')}
      searchFormCollapsed={searchFormCollapsed}
      onSearchFormCollapsedChange={({collapsed}) => {
        saveUserPreferences({'admin.list.searchFormCollapsed': collapsed})
      }}
      detailApp={EntityDetailApp}
      customActions={{
        history
      }}
    />
  )
}

ListView.propTypes = {
  emitAction: PropTypes.func.isRequired,
  currentViewInfo: currentViewPropType,
  persistedViewInfo: PropTypes.shape({
    store: PropTypes.object
  }),
  searchFormCollapsed: PropTypes.bool,
  saveUserPreferences: PropTypes.func,
  baseRoute: PropTypes.string
}

export default ListView
