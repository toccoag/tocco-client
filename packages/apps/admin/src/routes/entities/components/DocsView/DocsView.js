import PropTypes from 'prop-types'
import {useEffect, useState, useCallback} from 'react'
import {rest} from 'tocco-app-extensions'
import DocsBrowserApp, {ignoreDefaultNavigationStrategy} from 'tocco-docs-browser/src/main'
import {Typography} from 'tocco-ui'
import {navigationStrategy as navigationStrategyUtil} from 'tocco-util'

const DocsView = ({
  entityName,
  entityKey,
  showActions,
  noLeftPadding,
  openResource,
  initialLocation,
  sortable,
  navigationStrategy
}) => {
  const [folderKey, setFolderKey] = useState(null)

  const fetchFolder = useCallback(async () => {
    rest.simpleRequest('entities/2.0/' + entityName + '/' + entityKey + '/entitydocs/folder').then(res => {
      setFolderKey(res.body.key)
    })
  }, [entityName, entityKey])

  useEffect(() => {
    fetchFolder()
  }, [fetchFolder])

  // use default admin navigationStrategy for ListLink expect for the resource/folder entity
  // as there the link is not working. explicitly show no link as else in the delete dialog an invalid link is rendered.
  const ListLink = ({entityName: entityNameListLink, entityKeys, children}) => {
    if (ignoreDefaultNavigationStrategy(entityNameListLink)) {
      return <Typography.Span>{children}</Typography.Span>
    }

    return (
      <navigationStrategy.ListLink entityName={entityNameListLink} entityKeys={entityKeys}>
        {children}
      </navigationStrategy.ListLink>
    )
  }

  ListLink.propTypes = {
    entityName: PropTypes.string.isRequired,
    children: PropTypes.any.isRequired,
    entityKeys: PropTypes.arrayOf(PropTypes.string)
  }

  return (
    folderKey && (
      <DocsBrowserApp
        routerType="routerless"
        initialLocation={initialLocation}
        noLeftPadding={noLeftPadding}
        sortable={sortable}
        searchFormType="none"
        rootNodes={[
          {
            key: folderKey,
            entityName: 'Folder'
          }
        ]}
        embedded={true}
        showActions={showActions}
        {...(showActions === false ? {selectionStyle: 'none'} : {})}
        {...(openResource ? {openResource} : {})}
        scrollBehaviour="inline"
        navigationStrategy={{
          ...navigationStrategy,
          ListLink
        }}
      />
    )
  )
}

DocsView.propTypes = {
  entityName: PropTypes.string.isRequired,
  entityKey: PropTypes.string.isRequired,
  showActions: PropTypes.bool.isRequired,
  noLeftPadding: PropTypes.bool,
  openResource: PropTypes.func,
  initialLocation: PropTypes.string,
  sortable: PropTypes.bool,
  navigationStrategy: navigationStrategyUtil.propTypes
}

export default DocsView
