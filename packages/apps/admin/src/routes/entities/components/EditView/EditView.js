import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {useLocation, useNavigate, useSearchParams} from 'react-router-dom'
import styled from 'styled-components'
import DocsBrowserApp from 'tocco-docs-browser/src/main'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {getEntityOverviewUrl} from 'tocco-history/src/main'
import {scale, theme, usePrompt} from 'tocco-ui'

import baseNavigationStrategy from '../../utils/navigationStrategy'
import {currentViewPropType} from '../../utils/propTypes'
import Action from '../Action'

export const StyledEntityDetailAppWrapper = styled.div`
  margin: 0;
  background-color: ${theme.color('paper')};
  padding: 0 0 0 ${scale.space(0)};
  overflow: hidden;
  height: 100%;
`

const EditView = props => {
  const {
    currentViewInfo,
    intl,
    chooseDocument,
    emitAction,
    propagateRefresh,
    invalidateLastBreadcrumb,
    setPendingChanges,
    showPendingChangesModal,
    baseRoute
  } = props
  const msg = id => intl.formatMessage({id})
  const location = useLocation()
  const navigate = useNavigate()
  const [searchParams] = useSearchParams()

  const initialAction = searchParams.get('action')
  const queryFormName = searchParams.get('formName')

  const [touched, setTouched] = useState(false)
  const mode = 'update'

  useEffect(() => {
    return () => setPendingChanges(false)
  }, [setPendingChanges])

  usePrompt({
    when: touched,
    message: msg('client.admin.entities.confirmTouchedFormLeave'),
    showPendingChangesModal
  })

  if (!currentViewInfo || currentViewInfo.pathname !== location.pathname) {
    return null
  }

  const handleToucheChanged = ({touched: changedTouched}) => {
    setTouched(changedTouched)
    setPendingChanges(changedTouched)
  }

  const navigateToCreateRelative = (relationName, state) => {
    if (relationName) {
      navigate(`../${relationName}/create`, {
        state
      })
    } else {
      navigate('../../create', {
        state
      })
    }
  }

  const handleEntityDeleted = () => {
    navigate('../../')
  }

  const handleEntityUpdated = () => {
    invalidateLastBreadcrumb(location)
  }

  const handleRefresh = () => {
    propagateRefresh(location)
  }

  const handleSubGridRowClick = ({id, relationName}) => {
    navigate(`../${relationName}/${id}`)
  }

  const entityName = currentViewInfo.model.name

  const navigationStrategy = {...baseNavigationStrategy(navigate, baseRoute), navigateToCreateRelative}

  const history = (_definition, selection, _parent, _params, _config, _onSuccess, _onError) => {
    navigationStrategy.navigateToPath({pathname: getEntityOverviewUrl(selection), inNewTab: true})
  }

  return (
    <StyledEntityDetailAppWrapper>
      <EntityDetailApp
        entityName={entityName}
        entityId={currentViewInfo.key}
        formName={queryFormName || entityName}
        mode={mode}
        emitAction={emitAction}
        onTouchedChange={handleToucheChanged}
        navigationStrategy={navigationStrategy}
        chooseDocument={chooseDocument}
        onEntityDeleted={handleEntityDeleted}
        onEntityUpdated={handleEntityUpdated}
        onRefresh={handleRefresh}
        actionAppComponent={Action}
        onSubGridRowClick={handleSubGridRowClick}
        listApp={EntityListApp}
        docsApp={DocsBrowserApp}
        initialAction={initialAction}
        customActions={{
          history
        }}
      />
    </StyledEntityDetailAppWrapper>
  )
}

EditView.propTypes = {
  intl: PropTypes.object,
  currentViewInfo: currentViewPropType,
  chooseDocument: PropTypes.func.isRequired,
  emitAction: PropTypes.func.isRequired,
  propagateRefresh: PropTypes.func.isRequired,
  invalidateLastBreadcrumb: PropTypes.func.isRequired,
  setPendingChanges: PropTypes.func.isRequired,
  showPendingChangesModal: PropTypes.func.isRequired,
  baseRoute: PropTypes.string
}

export default EditView
