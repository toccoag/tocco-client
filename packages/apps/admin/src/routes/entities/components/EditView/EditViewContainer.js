import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actionEmitter} from 'tocco-app-extensions'
import {chooseDocument} from 'tocco-docs-browser/src/main'

import {getRoutingUserConfirmation, setPendingChanges} from '../../../../modules/session/actions'
import {propagateRefresh, invalidateLastBreadcrumb} from '../../modules/path/actions'

import EditView from './EditView'

const mapActionCreators = {
  chooseDocument: chooseDocument.actions.chooseDocument,
  emitAction: action => actionEmitter.dispatchEmittedAction(action),
  propagateRefresh,
  invalidateLastBreadcrumb,
  setPendingChanges,
  showPendingChangesModal: getRoutingUserConfirmation
}

const mapStateToProps = (state, props) => ({
  currentViewInfo: state.entities.path.currentViewInfos[props.location.pathname],
  baseRoute: state.input.baseRoute
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EditView))
