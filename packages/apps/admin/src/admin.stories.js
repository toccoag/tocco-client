import noBorder from '../../../../storybook/addons/no-border'

import AdminApp from './main'

export default {
  title: 'Admin/Admin',
  decorators: [noBorder]
}

export const Story = () => {
  return <AdminApp memoryHistory />
}
