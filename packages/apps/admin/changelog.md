3.13.56
- implement action
- add configure after automation logic action

3.13.55
- fix icon alignment of menu entry icon
- implement action
- add configure automation action

3.13.54
- make widget date picker more compact

3.13.53
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- adjust widgets search
- align ranges horizontally for widgets

3.13.52
- handle layout-scope
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.51
- add attachments
- add isTransparent prop to Ball to enable transparent backgrounds
- add transparent background
- remove bottom border in login matrix for a cleaner appearance

3.13.50
- use formSuffix for navigation cell
- prevent "jumping" menu whenn collapse buttons are hidden

3.13.49
- fix document component

3.13.48
- refactor full calendar navigation and add week number
- fix modal in routerless widgets

3.13.47
- show icon on tiles
- add icons
- fix mail action without incamail module

3.13.46
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- add week numbers to date picker

3.13.45
- implement incamail in mail action

3.13.44
- fix documnet upload in dms
- display input fields in basic search form as grid layout
- improve styling for search form on widgets
- enhance responsiveness of search form in entity list
- improve responsiveness of entity-list search form
- add labelVisibility prop to Button to enable omitting the prop in specific cases
- improve label for extended search button
- harmonize search form button spacing and positioning

3.13.43
- add email senders

3.13.42
- fix sidepanel button overflow
- fix focus styling when tabbing through navigation
- able to set justify alingment on tile column
- handle use label properly
- harmonize navigation spacing of toggle buttons

3.13.41
- harmonize react select spacing

3.13.40
- remove icon in VisibilityStatusCodeCopy component

3.13.39
- fix focus styling

3.13.38
- close all modals on route change
- add title to actions pending changes modal
- add pending changes modal on dms
- fix navigation cell header in dms
- adapt component list text resources

3.13.37
- fix adress / location browser autofill
- fix accept conflict action
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.36
- force rerender of ckeditor with auto-complete
- export idSelector
- adjust mail action
- implement initial default template
- fix ckeditor and initial auto complete

3.13.35
- pass rating ids to keyboard navigation

3.13.34
- improve label / form field component accessibility
- improve ux and styling of login matrix
- adjust conflict action
- improve tile responsiveness on mobile devices

3.13.33
- fix stylelint error by removing obsolete styling

3.13.32
- remove useSelectorsByDefault property

3.13.31
- allow setting title for button
- extract business unit into seperate file
- pass inWrongBusinessUnit
- show invalid business unit hint

3.13.30
- add SimpleFormApp

3.13.29
- add data-cy to table refresh button

3.13.28
- add background color for table tiles
- improve tile styling

3.13.27
- fix overlap of collapse buttons in navigation

3.13.26
- no error message on column resize

3.13.25
- handle null minChars in formDefinition

3.13.24
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- add new mail action
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.23
- adjust min char search field validation
- clean up searchFormValiation

3.13.22
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.21
- use correct touch detection
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.20
- fix stated value labels spacing
- fix color of label-neutral

3.13.19
- harmonize google maps icon positioning in LocationEdit

3.13.18
- add timezone header
- added data-cy to event-logic-copy buttons

3.13.17
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to entity model browser
- add tocco home breadcrumb to history
- add tocco home breadcrumb for entity routes

3.13.16
- use table layout as defined in list form
- introduce enter handling in modal
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.15
- create accept-conflict custom action
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.14
- cleanup save button code

3.13.13
- show full content of immutable textarea
- set auto focus if action is opened
- hide terms component if all fields are readonly
- harmonize cell spacing of event logic copy
- harmonize cell spacing of table form

3.13.12
- ignore line breaks in label texts for the sidepanel since they cover the input content
- fix relations box alignment when there are only 2 relations shown

3.13.11
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute
- create api key custom action

3.13.10
- cleanup marking code
- add native date(time)picker for touch devices
- create login-role action
- add login-role action
- remove custom _widget_key param

3.13.9
- fix relations box alignment when there are only 2 relations shown
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.8
- add new action data-protection-report
- add _widget_key query param to all requests

3.13.7
- pass escapeHtml to display formatter

3.13.6
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.5
- fix growing textarea
- remove padding styling for StyledLayoutContainer as it is now handled globally in the modal styling
- add left cell alignment for table
- add enter handling to widget code copy
- add enter handling to widget-visibility-status-code-copy
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- fix tets
- able to set link state on breadcrumbs
- show relation name in breadcrumbs

3.13.4
- add new action display-accountings
- add display accoutings action

3.13.3
- show buttons on configure modal on bottom

3.13.2
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper
- create app
- add outlook-add-in app

3.13.1
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.33
- revert focus fix

3.12.32
- do not auto focus anything on touch devices

3.12.31
- improve UX for simple selects on touch devices

3.12.30
- fix double scrollbars in some modals
- fix height of exam edit

3.12.29
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.28
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.27
- add tooltip texts for buttons of upload component
- fix cell alignment inside event logic copy table

3.12.26
- fix text select input edit and show available options
- fix table edit styling

3.12.25
- set checkbox field to touched on safari/ipad/iphone

3.12.24
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.23
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.22
- update list for mutated entities
- show massmutation modal button bottom right
- fix navigation strategy list link for folder and resource

3.12.21
- hide table refresh button when nothing happens

3.12.20
- apply entity template on copied entity

3.12.19
- color icon grey when no instance available
- adjust to fit backend api

3.12.18
- remove selection controller in dms

3.12.17
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on menu tab buttons
- fix hover on toaster close buttons

3.12.16
- use fix width for menu

3.12.15
- transform initial value paths
- only use initial configuration to setup form

3.12.14
- add context to auto-complete
- remove templateSelected property
- use ListView helpers

3.12.13
- disable button and change icon
- add sorting, pagination and column widths
- use SimpleTableFormApp
- show correct select field in export action
- show max age hint

3.12.12
- use error message when action fires error event

3.12.11
- action menu entry
- show action name in menu

3.12.10
- support report actions in adjustAllActions
- implement add type for mass mutation
- implement remove type for mass mutation

3.12.9
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- add simple table form app
- use core components
- remove unused / broken styling
- disable virtual table
- disable virtual table for entity model

3.12.8
- be able to accept prompt with enter

3.12.7
- show correct select field

3.12.6
- add fallback for formValues
- add ErrorBoundary around notifications
- extend font sizes with missing sizes
- do not show duplicated scrollbars

3.12.5
- trim searchInput value
- improve line numbers color in dark mode
- save column width in preferences

3.12.4
- add declareColoredLabel util
- improve evaluation view tree styling
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix sorting of columns with missing preferences
- fix sorting of columns with missing preferences
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.42
- do not apply btn hover style on disable buttons

3.11.41
- handle query changes properly

3.11.40
- add copy icon
- improve history action ui
- pass origin event for onChange
- improve history action data loading
- improve acl-browser data loading
- preselect row
- fix outputjob in synchron simple action
- fix navigatino to document edit page
- improve diff viewer theme

3.11.39
- add export action
- support search fields in useAutofocus
- focus search field

3.11.38
- add legacy font sizes to HtmlEditor
- fix selection handling
- cleanup transient props
- adjust toaster for action validation

3.11.37
- add massmutation-replace action
- TOCDEV-9023

3.11.36
- open entity list in entity model browser in separat tab
- adjust diff view
- implement breadcrumb and adjust navigation
- add url helpers to diff view
- pass navigation strategy to entity docs dms
- pass full navigation strategy to dms
- adjust opening history action
- support history action
- clear selection if new table is loaded
- duplicated blocker instances on entity detail

3.11.35
- improve selection in history table

3.11.34
- add mailbox icon

3.11.33
- only navigate back two steps when selection deleted

3.11.32
- entity model ux impovements

3.11.31
- navigate to parent folder after deletion
- pass navigationStrategy to sub-actions
- add transfer and date

3.11.30
- use correct shouldForwardProp implementation

3.11.29
- export combineSelection
- add overview page
- add history action
- support selection
- add history component and modules
- add entity overview page
- add new property to theme
- add entity detail
- add navigateToPath to navigationStrategy
- add navigateToPath to navigationStrategy
- implement navigation between views
- add icon
- improve entity model
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18
- improve routing and add breadcrumbs
- define new route for entity model browser
- do not reload fullscreen action on menu open

3.11.28
- update react-router v6
- update react-router v6
- update react-router v6
- remove unused history
- able to run with memory router

3.11.27
- prevent additional scrollbars being rendered in the searchpanel

3.11.26
- use transient props for styled components
- use transient props for styled components
- use transient props for styled components
- align stated value boxes content vertically

3.11.25
- add new icons
- add entity-model-browser action
- add entity-model-browser action

3.11.24
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.23
- migrate to react router v6

3.11.22
- add truck-medical icon

3.11.21
- add id to formatted values
- replace keyHandler with navigateTable util
- replace keyHandler with navigateTable util

3.11.20
- add active condition for loading page limit options
- add active condition for loading page limit options

3.11.19
- keep view when opening menu in different tab or window
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes

3.11.18
- standardize sticky button wrapper styling
- pass state property on navigate
- remove obsolete right padding in evaluation view tree
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic

3.11.17
- support post flush validation
- replace default props in tocco-ui with javascript default parameters
- replace default props in entity-detail with javascript default parameters
- replace default props in scheduler with javascript default parameters

3.11.16
- run action with single entity in list
- run action with single entity in list
- run action with single entity in list
- load relations on detail properly

3.11.15
- ignore undefined optionDisplay

3.11.14
- fix fullscreen action on detail

3.11.13
- implement validation for simple actions

3.11.12
- fix release after yarn v4 upgrade

3.11.11
- update to yarn 4

3.11.10
- migrate admin to react router v6

3.11.9
- handle rule provider in widgets
- handle optionDisplay in remote and select fields

3.11.8
- do not overwrite toasters from sync simple actions
- reset theme type properly
- add react-router compatibility package

3.11.7
- fix tooltips freeze by replacing popper library with floating-ui library
- do not create entity template multiple times

3.11.6
- fix range mapping after upgrade to date-fns 3.x

3.11.5
- replace old action in front and back-end
- fix formatted value colors in dark mode labels

3.11.4
- allow auto focus to be disabled
- disable search field focusing in sub grids in widgets
- set isSubGrid property

3.11.3
- fix button group not being displayed correctly after styled components update

3.11.2
- fix react-select menu after upgrade to styled component

3.11.1
- merge ModalDisplay and BlockingDisplay into OverlayDisplay
- show error id for actions
- fix clear number field via auto complete

3.11.0
- initial release for version 3.11

3.10.34
- map postcode fields to string tql
- reset to default when deselecting template
- add option to reevalute action condition
- reevaluate action conditions if detail reloaded

3.10.33
- enhance toasters with large content by adding vertical scrollbar

3.10.32
- fix popover text and stated value text not wrapping

3.10.31
- render html in toaster body
- return horizontal drop position in useDnD
- reference vertical drop position now that horizontal drop position also exists
- allow dropping and display drop indicator on both sides in ColumnPicker

3.10.30
- add row numbering to ui table
- add row numbering
- enable numbering for main entity-list

3.10.29
- add secondary sorting for entity-templates
- rename property description to title
- show tooltip with label and description for entity template
- show label and description in search filter tooltip
- move rest endpoint to client "route"
- fix side panel vertical scrolling in create view

3.10.28
- fix hostname for custom actions in cms
- add method to extract env as input props
- pass env to dynamic action

3.10.27
- fix datepicker styling in widgets by providing the tocco-app classname which prevents webkinder styles being applied
- let requestSaga handle blocking info in simpleAction

3.10.26
- always require captcha for anonymous
- disable auto-complete when selecting template
- disable auto-complete when selecting template
- map phone fields to string tql

3.10.25
- place date picker directly in body to prevent z-index problems in widgets
- identify exams by key instead of nr

3.10.24
- set title for notification center

3.10.23
- set max height for info boxes
- disable cache for create forms

3.10.22
- do not show same tooltip twice

3.10.21
- delay the display of popovers to prevent distracting flashing effect in large lists on hover
- style link inside label duplicate
- do not show same tooltip twice

3.10.20
- use breaks instead of separate paragraphs for readonly text
- add isDefined helper
- accept falsy values as settings for reports
- prioritize exact matches in menu search
- sort menu entires alphabetically by label when filtering

3.10.19
- fix draggable file/image

3.10.18
- change link color back to secondary'
- harmonize icon list spacing
- style duplicate warning
- optimise last opened color for dark theme
- fix selected day color in date picker
- fix draggable in preview

3.10.17
- enhance breadcrumbs und popover colors for dark mode
- decrease spacing of menu tree items
- enhance dark dark theme colors

3.10.16
- New menu item for new sysdoc in help menu

3.10.15
- fix save button coloring in dark mode

3.10.14
- fix clear of remote field
- improve dark theme color harmony
- enhance table aesthetic and readability

3.10.13
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation
- fix disabled buttons disappearing in widgets
- fix sso login in old cms

3.10.12
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation

3.10.11
- add navigationStrategy as input prop
- pass navigationStrategy to simple action form
- add report to output group instead of new group

3.10.10
- adjust isReadOnly for address and location field
- fix rounding issue for sticky columns
- allow not whitelisted style properties for svg element

3.10.9
- remove obsolete bottom padding in form
- extend widget theme with missing text colors
- add file circle plus icon

3.10.8
- style payment summary view

3.10.7
- do not try to access captcha when hidden
- style fixed columns in table with border at the right edge
- add coloration for number of attempts

3.10.6
- only use captcha when updating password if the target is the current user or the current user is no usermanager
- add title to description element
- load entities text resources
- add acknowledge notification and entities message
- remove description title again
- handle custom title in action pre checks
- remove ignoreFormDefaultValues property
- remove ignoreFormDefaultValues from entity-detail

3.10.5
- disable CKEditor version check
- fix dark theme icon issues within buttons

3.10.4
- respect itemCount as limit for remote fields
- default disable remote create in widgets

3.10.3
- enable whole area of menu entries to be clicked
- fix dark theme color issues
- apply corresponding theme background color to login screen

3.10.2
- only use AutoSizer with new windowed table

3.10.1
- fix colors in signal list items for dark mode

3.10.0
- initial release for version 3.10

3.9.36
- store theme in local storage
- style focused menu elements the same as on hover

3.9.35
- set referrer policy to `no-referrer-when-downgrade`
- add sticky column to ui table
- use other endpoint for delete button
- apply theme themeswitching to ace code editor as well

3.9.34
- fix blocking display disappearing in widgets on long pages

3.9.33
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin

3.9.32
- add the full size initialvalue generator action
- add new initialvalue generator action

3.9.31
- check rights to remove delete button
- fix selection in remotefield
- fix selection in remotefield

3.9.30
- change last opened color to increase readability
- fix menu items wobble on hover in firefox browser
- fix dark theme color issues
- dynamic determine selectable in table

3.9.29
- change last opened color to increase readability

3.9.28
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- add listApp
- use Debouncer in CodeEdit instead of custom debounce

3.9.27
- use correct search form name as fallback
- enable hover on parent items of menu tree

3.9.26
- map shrinkToContent in getColumnDefinition
- fix jumping buttons in tocco header when opening help menu
- add feature

3.9.25
- improve menu ux by adding hover background and expanding clickable area
- add dark mode
- fix widget theme

3.9.24
- fix intermediate values in time edit

3.9.23
- fix intermediate values in time edit

3.9.22
- add no button option for simple action form

3.9.21
- map email fields to string tql

3.9.20
- add non breaking space to ensure correct spacing in multiple fields separator

3.9.19
- fix column order resetting when moving multiple columns after another

3.9.18
- add drag'n'drop upload

3.9.17
- support custom report settings in widgets
- harmonize exam edit spacing

3.9.16
- enhance last openend color for better visibility

3.9.15
- limit watchForHover to activate in window.onload

3.9.14
- Add `watchForHover` styling utility
- Fix icons to open module in new tab on mobile

3.9.13
- adjust styling of log action
- shrink size of datepicker

3.9.12
- handle 403 error in delete

3.9.11
- fix width of exam edit table ipunt fields in firefox
- Fix visibility of icon link to open module in new window
- add last opened to ui table
- add last opened to entity-list
- align stated value content verically with label

3.9.10
- use path for setting placeholder terms value
- fix multi-select-field

3.9.9
- hide readonly terms

3.9.8
- harmonize date picker styling and implement time picker
- fix several selection issues in the dms
- fix selection in docs tree search

3.9.7
- load row number options from Page_limit entities
- fix multivalue separator span alignment

3.9.6
- add updateDocumentTitle prop to Breadcrumbs
- add updateDocumentTitle prop to Breadcrumbs
- add updateDocumentTitle prop to Breadcrumbs

3.9.5
- register gear icon

3.9.4
- split styling of blocking display for admin and widget
- fix multivalue label text overflow

3.9.3
- add readonly terms config and use pristine value change for placeholders
- handle immutable prop in TermsEdit
- check for writable paths in form instead of actions
- only set form builder readonly if mode is not create

3.9.2
- split toaster into widget and admin components
- adjust height of evaluation tree table to scale depending on available viewport height

3.9.1
- TOCDEV-7717, TOCDEV-7983

3.9.0
- initial release for version 3.9

3.8.25
- move logic from reports saga to backend
- Show error message if too many entities are selected for deletion

3.8.24
- align widget labels at flex start position
- add hideFooter property to entity-detail
- register list-tree icon
- add evaluation-view-tree action
- support action in table

3.8.23
- harmonize status label border radii
- add border around exam edit datepicker inputs

3.8.22
- load row number options from Page_limit entities
- fix resizing of ui table

3.8.21
- add download icon to DocumentFormatter
- allow multiple paths and disallow navigation for custom table on SubGrid
- disable navigation if parent defines it
- do not add search value for parent relation if no reverse relation was defined

3.8.20
- fix terms edit link color
- navigate to next field without selecting with tab

3.8.19
- only use visible search fields for building tql
- escape front-slash in fulltext tql query

3.8.18
- vertically center input edit table cells
- force underscore styling of terms edit links

3.8.17
- hide overflow of column headers
- register gear icon
- disable sideEffects optimization in webpack
- fix css styles not being passed down to multi value label span
- fix sticky buttons within modal cutting off content
- fix terms edit link styling and refactor component

3.8.16
- register calculator icon

3.8.15
- fix themes by adding missing status label radius
- extract nested ternary operation
- improve exam edit action styling
- fix vertical alignment of html formatter

3.8.14
- add data-cy attributes

3.8.13
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add BasicSearchForm
- add qualification dataRequest
- make select work without formData
- refactor input-edit to use more shared code
- refactor evaluation-view to use more shared code
- use column position hook
- add useTitle to FormattedValue
- add promotion reason as title
- fix checking if form contains only readonly
- add additional units

3.8.12
- refactor panel group to remove duplications

3.8.11
- fix long lines in code edit being cut off by showing scrollbar
- add id to ReactDatePicker
- add keyboard navigation, editable value focus and postPointDigits
- decouple docs-browser from entity-list
- add default link target and protocol to html editor

3.8.10
- fix disabled button and menu item styles
- fix broken prop types
- do not use array index in keys
- fix legacy styles being applied to status labels

3.8.9
- change fields separator
- change fields separator
- fix test for new separator
- make editableValueFactory work without formField
- create exam-edit
- add exam-edit action
- add exam-edit action

3.8.8
- remove obsolete padding around styled entity browser
- fix disabled textarea on safari

3.8.7
- fix menu menu links not opening in new tab when icon is clicked

3.8.6
- fix menu menu links not opening in new tab when icon is clicked

3.8.5
- add option to run menu simple action in background

3.8.4
- ignore default search filter if tql is passed

3.8.3
- if a form contains only readonly field the save button should be removed

3.8.2
- add icon to merge button

3.8.1
- use widget prefix for report location
- display simple actions in menu
- lighten status label colors and harmonize spacing

3.8.0
- initial release for version 3.8

1.0.283
- harmonize menu layout by moving technical name below label
- fix scrollbars on disabled textareas with content
- load tql helper synchron

1.0.282
- reset disabled min-height on textareas

1.0.281
- growing subtables
- show full username
- load email template properly

1.0.280
- harmonize panel spacing and font weights
- make actionConditions work in the children of action groups
- handle follow up actions

1.0.279
- build up modifiers from smaller independent functions

1.0.278
- use new formSuffix to build formBase of SubGrids
- continue handling form elements after a custom component was encountered
- do not crash if no formValues exists when checking terms data
- pass formValues to formBuilder
- pass formValues to formBuilder

1.0.277
- enable co-existence with legacy ckeditor
- improve readability of status labels
- adjust disabled textarea height to match vertical label position

1.0.276
- do not render terms if no conditions were configured
- create function-mutation
- add function-mutation
- check for empty string that element is only added if necessary

1.0.275
- fix getTextOfChildren
- replace deprecated selectUnit
- replace deprecated selectUnit
- remove formatjs dependency

1.0.274
- fix buggy scroll to top for modals
- remove delete action from all forms and add searchFormType
- handle phone validation for relations
- handle validation response properly
- fix broken prop types

1.0.273
- remove redundant prettier default options

1.0.272
- dependency update
- dependency update
- fullcalendar dependency update
- dependency update
- align data-cy naming
- add data-cy attributes

1.0.271
- handle de-DE locale for datepicker
- align terms condition with checkbox properly
- add data-cy attribute

1.0.270
- increase left padding of merge table

1.0.269
- add min width to toaster box

1.0.268
- handle attempt and max attempts

1.0.267
- get all address suggestions within country
- render sidepanel initially with correct collapse state
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- add terms component
- add terms component
- add pickAllProperties to getFlattenEntity
- use new terms

1.0.266
- handle sso with redirects
- handle auto login properly
- connect principal with sso login properly
- handle sso login properly

1.0.265
- export logged in action type
- handle showAutentication flag

1.0.264
- add reports after new button
- place label in widgets on left side
- move question code to tocco-util
- add membership questions to membership-registration

1.0.263
- flag selection as deleted on duplicate detail
- navigate to list after fullscreen action deleted selection

1.0.262
- refactor partial order cancellation fix import typo
- add relation link to navigation strategy
- show correct relation link for related entitites
- show only success toaster once
- show only success toaster once

1.0.261
- accept limit as string
- add partial order cancellation custom action
- add partial order cancellation to the bundle

1.0.260
- handle search form type properly
- render and unmount widget automatically
- use key as app id
- move configs to modal actions
- move all state to redux
- use uniqueId for appContext
- clear states on unmount
- show acutal widget input and visibility status
- improve styling for widgets
- show no config message

1.0.259
- increase toaster width to prevent early line breaks within titles
- fix acl browser styling
- add membership-registration widget
- add membership-registration to bundle

1.0.258
- add icon for action group
- fix immutable label positioned inside
- fix log input height
- fix immutable height of integer edit by passing immutable prop

1.0.257
- remove list numbering
- clear cached view stores
- re-load view on locale change

1.0.256
- style edit text within html formatter
- render resource-rows in correct heights
- add formApp to actions
- improve error handling for entity template creation
- improve error handling for choosing entity template
- reset value properly
- handle complex field for entity templates

1.0.255
- fix fullcalendar license

1.0.254
- allow documents in entity templates

1.0.253
- fix docs browser label alignment
- fix generator-func sonar bug
- set correct label position
- fix build for customer bundles

1.0.252
- fix widget config if unique id is not key
- adjust useSyncValidation
- fix action order
- add selection to structure search
- add entity template update action
-  add data-cy attr to confirm buttons
- use comon text resources
- use text resources for extended search
- use text resources for extended search
- fix hostname field in log view
- load actual question instead of label of the question

1.0.251
- add data-cy attr to delete buttons
- add data-cy attr to submit button
- add data-cy attr to entity template buttons

1.0.250
- show no entity templates available message
- save entity template sidepanel in preferences
- edit and delete entity template
- cleanup entity template menu

1.0.249
- fix disabled select spacing
- adjust widget stated value label color
- add widget-showcase
- add action form modifier fn
- add simple form helper functions
- add onFormValuesChange event
- action can ignore pending changes prompt
- entity template create action
- reset current page when searching

1.0.248
- fix choice edit label alignment
- fix light cellrenderer width

1.0.247
- handle new formBase attribute
- add core sidepanel horizontal button comp
- use core button for search filters
- handle input default values exclusivly
- add entity templates to create view
- add save action with follow up that can start an action after save
- pass action from follow up to detail after create

1.0.246

1.0.245
- add education-portfolio widget
- add education-portfolio widget
- adjust google maps address link
- allow all html content
- align editor config with legacy editor
- add advanced color picker
- add paste as plaintext
- correct display of numbering
- create new acl browser
- add acl-browser action
- do not crash if action has no flags object in response
- add upload to editableMapping

1.0.244
- fix null values in percent field

1.0.243
- use new navigation cell to navigate in cypress test
- handle new flags object in action response
- allow hard reload after action and ignore invisible meta fields on soft reload

1.0.242
- allow passing target to DetailLinkRelative
- add font awesome eye icon
- use eye icon for documents
- export StyledTableRow
- add row numbering and hover link that opens entity in new tab

1.0.241
- fix action button styling

1.0.240
- style promotion status badge
- adjust font factor of widget theme
- add tql request

1.0.239
- extend embed type
- add widget panel
- use env.embed helpers
- handle label position via prop
- handle label color for widget and admin differently
- handle different form components for widgets
- treat admin embedType logic as well for legacy-admin
- load formComponents on runtime
- add selectionDeleted for simple action

1.0.238
- add action button styling

1.0.237
- add missing read-only type for location/address
- support hide and read-only for location/address

1.0.236
- copy only changesets function
- position toasters at bottom right to prevent interference with interaction elements
- remove x-backend-url header
- align labels in center of input box
- harmonize positioning of sticky buttons within modal

1.0.235
- hide rule checking for difference to old password if old password field is not shown
- editable sql statements
- copy changelog to clipboard
- add cypress data attribute to breadcrumb

1.0.234
- handle remote create on simpel actions
- handle entity model corretly to support remote create
- handle entity model corretly on form builder
- provide detail app to actions
- provide detail app to entity-list
- show advancedSearch properly

1.0.233
- fix country cache
- expand checkbox and label options for ChoiceEdit
- expand pseudo field choice options
- use new loadModules endpoint

1.0.232
- handle new points types with averages
- move field definition cache to object cache

1.0.231
- move country cache to object cache

1.0.230
- harmonize menutree spacing and implement default icon

1.0.229
- enable enter handling again

1.0.228
- Make sure that address field is visible also without value

1.0.227
- show multiline lables with correct line breaks
- run query automatically
- keep sorting preferences up to date
- Added new component to search for full address

1.0.226
- fix client questions with cancel
- do not save form on open dropdown

1.0.225
- show entity-docs in email attachements

1.0.224
- remove top and left padding within immutable stated value box
- fix alignment of input fields within input edit table
- show back action always before reports
- add title to confirm modal for touched form
- fix fulltext search

1.0.223
- harmonize sso login spacing and color
- also implement border radius for indicator container wrapper to prevent cover of input field edges
- fix subfolder search
- add cancel button to confirm modals
- show correct pending changes modal on widgets
- fix border radius calculation of panel headers

1.0.222
- fix baseline of code edit

1.0.221
- fix input width of login form

1.0.220
- different backends for storybooks
- copy documents and binary

1.0.219
- position input label at baseline of input

1.0.218
- replace ckeditor5 with ckeditor4
- don't show prompt after entity creation
- fix PropTypes - date to string
- fix PropTypes - function to conditions list
- don't show prompt after entity creation
- rename wrapper
- do not infinite reload fullscreen action on reload

1.0.217
- introduce text alignment for input edit column

1.0.216
- add additional data-cy attributes

1.0.215
- fix height rendering of load mask and spacing of layout boxes within modal

1.0.214
- fix duplicated key on list

1.0.213
- remove duplicated enter handling

1.0.212
- remove minimum height for modal body which is not needed
- add error message if login failed
- remove sso error message
- stop action on failed sso login
- remove [object Object] button title
- ensure pending changes prompt is fired first

1.0.211
- fix missaligned dropdown indicators within select
- fix border radii of input edit select
- add fetchAllEntities which handles paging by itself
- replace fetchEntities with fetchAllEntities
- replace fetchEntities with fetchEntitiesPage
- replace fetchEntities
- replace fetchEntities in selection
- replace custom paging in notifications
- replace fetchEntities in formData

1.0.210
- improve readability of disabled multi value label text
- be able to tab through mulit-remote-selects
- navigate to next field with tab on select

1.0.209
- add modifyEntityPaths input parameter
- add modifyEntityPaths input parameter
- add initialAction
- remove history in action and use initialAction of entity-browser
- use initialAction of entity-browser

1.0.208
- fix jumping menu items in firefox
- fix radii not being applied to some input elements

1.0.207
- add global classname for all wrapping elements

1.0.206
- fix address-update after adding action conditions
- use global scrollbar style instead of specific imports
- support column action in adjustActions
- pass reportDisplayId property to action
- support (dynamic) reports in evaluation-execution-participation-action

1.0.205
- fix miscellaneous ui bugs in widgets

1.0.204
- adjust input field z-index and harmonize padding within input edit
- harmonize modal spacing and prevent scrollbar with minimum height introduction

1.0.203
- fix wide column content overflowing within td

1.0.202
- add action conditions
- harmonize label spacing in choice edit
- refactor simple form so that submit via enter key works and prevent nested forms
- add outputgroup when multiple reports

1.0.201
- show reset sorting again
-
- do not pass meta fields to onSubmit

1.0.200
- fix report generation with empty model

1.0.199
- invoke action only once
- only update input when app is mounted
- restructure cache for widget use cases
- remove revision from object cache
- fix text resource in evaluation-execution-participation-action
- create evaluation-view-info
- create evaluation-view action
- ignore meta fields on values
- add form to store and add form model name

1.0.198
- handle expanded range field via default value
- remove unused expanded property

1.0.197
- add X-Backend-Url to all requests
- adjust X-Backend-Url to all requests
- add share ansers functionality
- add finish evaluation
- add save on change
- add visbility status for evaluation-execution-participation
- add visbility status for evaluation-execution-participation-action

1.0.196
- do not show pending changes prompt twice

1.0.195
- show error message when deletion is not possible

1.0.194
- add error handling in evaluation-execution-participation-action
- add synchronous validation for pseudoSelect
- add error handling during loading
- add pseudo submitting which just blur form
- fix mandatory validation for pseudoSelect
- add back button
- add short link

1.0.193
- fix offline text in docs browser being cut off
- fix border radius of search boxes
- fix autofocus on search form
- fix select fields exceeding the new maximum limit and crashing

1.0.192
- fix phone edit
- fix widget code copy action
- fix datepicker input styling

1.0.191
- do not submit form on select item
- do not propagate and use default for enter key

1.0.190
- refactor errors in pseudo form fields
- refactor errors in event-registration-action
- fix range expand button vertical alignment
- allow actions to be hidden by backend

1.0.189
- Button as well as code are only shown when visibility-status selected
- removed filename

1.0.188
- add evaluation-execution-participation widget
- add evaluation-execution-participation widget to widget-bundle
- add pseudo select ui component
- add pseudo select field
- add select to pseudo form field
- add evaluation-execution-participation-action action
- add evaluation-execution-participation-action to entity-browser

1.0.187
- create job-offer-apply action
- create job-application widget

1.0.186
- upgrade to react v18

1.0.185
- allow customization of validation messages
- add checkbox pseudo field helper
- use checkbox pseudo field for terms to use custom validation message

1.0.184
- show correct border-radius on panel header
- enforce css selectors for textarea

1.0.183
- improve navigation on login workflow

1.0.182
- add events for authentication and register page
- add visibility status for authentication and register
- fix light cellrenderer font sizing to prevent icon overflow within cells

1.0.181
- add geosearch field

1.0.180
- trigger visibility status change only for widget embed type
- add visibility status to password update
- add visibility status to user-vailability
- add visibility status to mailing-list

1.0.179
- add onSuccess to synchronous simple actions
- enable custom action event handlers in entity-detail and entity-list
- pass customActionEventHandlers to store instead of actionFactory
- add visibility status

1.0.178
- fix init redux form
- style label based on publish status as in old client

1.0.177
- fix cancelling client questions

1.0.176
- when creating a new entity, trigger auto-complete for fields with default values

1.0.175
- fix header and content cell aligment of table
- show toaster with validation errors
- harmonize nav and search box styling

1.0.174
- show correct search icon

1.0.173
- fix bundling apps

1.0.172
- load React only once
- make yes button in YesNo client question work again
- re-init cache instead of clear cache
- fix caching and intl race condition
- fix object cache for not existing forms

1.0.171
- navigate back to list when state is lost
- remove unnecessary usage of darken and lighten functions and use theme colors instead
- fix move action for multiple root folders
- limit login container width

1.0.170
- add option to disable onclick on breadcrumb
- disable clicking on search result in breadcrumb

1.0.169
- map function to props correctly
- remove light cellrenderer flex so that it does not cut off content in table
- enforce border of input fields within input edit table
- improve sql dialog styling inside devcon

1.0.168
- remove usage of shade color function and extend theme instead
- introduce radii

1.0.167
- tqlBuilder now builds multiple conditions connected by 'or' when passed localized paths
- search fields for a localized field now search in every available locale
- show validation errors on deletion
- accept enter two-year date without punctation

1.0.166
- link entities on remote selects in action

1.0.165
- use correct locale for two factor connector
- add a new structure for the Cypress tests
- clean up shade color function use and switch to theme colors instead
- add on enter feature to advanced search
- add on enter feature to two factor connector action

1.0.164
- extend theme with more specific colors to better target elements
- adapt new specific theme colors to table, select and column picker
- handle sub module questions
- pass all query params to action
- simplify action url

1.0.163
- hide empty submodules table
- handle url search fields as strings
- be able to have uppercase urls

1.0.162
- standardize yes no question modal buttons

1.0.161
- fix theme selectors
- add agb and privacy protection fields

1.0.160
- add detail for event the user is registering too
- render fields reached through an empty but writable path
- improve global styles

1.0.159
- use correct fonts for choices
- add modules
- use correct locale for login comp
- harmonize scheduler navigation on smaller screen
- create custom event registration widget
- add event-registration widget
- add visibility status and hide after register

1.0.158
- harmonize spacing of breadcrumbs

1.0.157
- pass widget key as query param so backend can read it in a standardized way
- add min/max selection option for multiple-choice
- support min and max selection in validation
- add login component

1.0.156
- add username form to login
- restructre login page input props
- fix paddings for user-menu button
- fix error message alignment
- add the possibility to close some modals with enter key if button is primary
- add choice ui component
- add choice field
- add form pseudo field logic
- add setBoxChildren form modifier
- add additional condition to getUsedPaths
- add synchronous validation for choice type
- support pseudo fields in asynchronous validation
- add event questions to event registration

1.0.155
- create event-registration-action
- add event-registration-action

1.0.154
- standardize page overlay and holder components for modal and blocking info

1.0.153
- datepicker focus handling on ffox and date only inputs
- lower height threshold of button context
- add button context to header buttons so they can collapse properly on smaller screens

1.0.152
- show proper error msg for 413
- fix react attribute error
- handle formid insteaf of scope

1.0.151
- show pending changes modal on logout
- fix default confirm button
- fix sidepanel height
- enable scrollBehaviour on detail
- extend theme
- add sticky action bar
- simplify dynamic action configuration

1.0.150
- add icons for user menu and business unit menu inside header

1.0.149
- always display entity name in complete menu instead of just when matching search
- display more custom icons in Breadcrumbs as defined by backend, specifically domains and folders in dms

1.0.148
- use dynamic viewports units to better handle height rendering on mobile devices
- display percent fields correctly

1.0.147
- fix dnd on dashboard
- fix login page height not filling full page
- be able to embed bootstrap script relative

1.0.146
- add autoFocusOnMount prop to ace editor to set focus manually since the useAutoFocus hook is not working in this case
- replace range type icons with generic icon and harmonize alignment to improve UX
- remove data-id attribute
- fix relations list on detail
- fix modal position in widget context and use vh units
- add feature to save number of rows as user preference in entity list

1.0.145
- add visibility status code copy action
- rename onStateChange to onVisibilityStatusChange
- rename onStateChange to onVisibilityStatusChange
- rename onStateChange to onVisibilityStatusChange
- set correct user locale after login

1.0.144
- pass navigation strategy to actions
- fix table randomly jumping scroll positions when switching pages

1.0.143
- fix autofocus by wrapping selector in timeout

1.0.142
- add possibility to show reload prompt
- add prompt component
- show user confirmation on browser-routing
- show user confirmation without blur
- pass selection from url to fullscreen action
- pass selection from url to fullscreen action

1.0.141
- user now gets new widget forms for personal-dms

1.0.140
- add back button
- use core back button
- use core back button for cancel
- user now gets redirected to homepage after logout

1.0.139
- add widget states
- enhance external events
- add states (list, detail, fullscreen-action)
- add onStateChange event

1.0.138
- fix object cache

1.0.137
- re-load data on login
- add object cache
- use global object cache

1.0.136
- allow padding of Button to be removed like when used as icon
- allow Action to be rendered as text without button visual
- export action types to use same names everywhere
- custom actions that should open in a modal are now displayed in menu
- confirm modals now use whichever default action the backend declares, which means switching the styling and keyboard shortcut

1.0.135
- prevent action buttons within table of being missaligned in widgets

1.0.134
- show fullscreen modal based on screen width instead of generally switching to fullscreen on touch devices

1.0.133
- order range values on top instead of side by side to improve ux
- remove the split pane feature as it doesn not improve UX and wrap scheduler in generic sidepanel component
- create ENTITY_DOCS selection for legacy actions
- support disabling remote create button
- move submit button to parent
- add sync validation to template form
- disable submit button for invalid form
- add reload action

1.0.132
- fixed width for user-menu
- add system menu shortcut
- add the full size log view action
- add the full size log view action to the admin
- support identifier type in tql builder
- enforce css selectors
- enforce css selectors
- fix null as end date

1.0.131
- fix save button being clipped and change hover on error
- fix react-select dropdown being clipped inside input-edit table
- link for max 100 created entities
- add docs-tree search
- add conditional selection for tables
- only select entities for docs tree search

1.0.130
- allow target attribute in html
- harmonize spacing of input edit action wrapper

1.0.129
- do not use default display for tooltips again

1.0.128
- send selection to adjusted POST endpoint when loading report settings
- use a new custom display when displaying entities in select boxes

1.0.127
- harmonize spacing of ranged dates by increasing search panel width
- simplify widget code

1.0.126
- reintroduce min-width for Ball component, since normal width caused rendering issues in the main menu
- correct height calculation method of admin to always ensure that the full viewport height is filled
- replace useWindowWidth hook with universal useWindowSize hook
- possible to define custom render for actions

1.0.125
- do not collapse sidepanel on top

1.0.124
- show time in 24h format for all locales
- show time in 24h format for all locales

1.0.123
- move toasters on mobile to bottom for better ux as most of the interaction elements (e.g actions) are on top and should not be covered

1.0.122
- input edit info action for showing meta info
- add input-edit-info action
- do not reload fullscreen action on single action
- be able to click on row in action cell

1.0.121
- add collapsible sidepanel component
- possible to pass beforeRenderField func
- introduce legacy-admin embed type
- same sidepanel behaviour as in admin
- use reusable sidepanel component
- use simple_advanced search form
- autofocus form fields
- prepare sso-login widget for widget embedType

1.0.120
- remove tocco subdomain

1.0.119
- use Icon component in LoadingSpinner
- replace values of readonly input fields with loading spinner while the update request is processing

1.0.118
- move component specific event handlers
- pass redirectUri for sso logins in widgets
- add sso-login and login package to widget bundle

1.0.117
- fix null values in field name transformation
- support nested paths in location field
- provide entityField for create field config
- use long as datetime value
- allow login to redirect to another page from an url parameter

1.0.116
- allow componentConfigs to adjust value used
- use correct decimal digits and % suffix for percentage fields
- use FormattedNumber in percent mode in PercentFormatter
- disable autozoom of input fields on ios devices
- always show search filter button icons on touch devices because of missing hover function
- increase top padding of input label to prevent cut off

1.0.115
- disable removing outputjob toasters in widgets
- fix performance of fulltext search

1.0.114
- add export action to entity-browser
- prevent body scroll when modal is opened

1.0.113
- improve responsiveness of table component
- datepicker keyboard handling
- do not lazy load datepicker
- autofocus datepickers

1.0.112
- add helpers to remove fields from forms

1.0.111
- fix escape handling in tql fulltext search

1.0.110
- improve save button look on error

1.0.109
- time input component
- fix scroll on select

1.0.108
- fix reload button shifting when interacting with fullcalendar navigation
- add mobile collapse feature on list view
- change HTTP method of `rest.fetchEntity` to POST
- add widget config key to env
- add custom create endpoint to validation
- add custom create endpoint
- add copy action to entity-browser
- remove removeCreate form modifier
- add addCreate form modifier
- load defalut values on action forms

1.0.107
- do not refresh list multiple times
- show correct search form in subgrid

1.0.106
- add pen-field icon
- increase menu item padding on touch devices for better usability

1.0.105
- transform QUERY to ID selection for actions (if custom endpoint)

1.0.104
- fix mobile pane bug in detail view on small screens when right pane is open

1.0.103
- let entity list handle changed props properly
- ignore any text and textarea fields of a datepicker when autofocusing

1.0.102
- fix date picker icon

1.0.101
- fix null pointer when starting in standalone mode
- added action callbacks to navigate back
- remove locale from documentation url

1.0.100
- enable fullscreen menu on mobile devices
- fix datepicker year dropdown closing on scroll
- add mobile collapse feature to detail view
- fix null pointer in login box
- fix whitespace in notification

1.0.99
- close datepicker on scroll
- add calendar icon
- add icon to date picker

1.0.98
- add model name to notification

1.0.97
- add home icon to header and breadcrumbs
- fix navigating back handler for fullscreen actions
- add 'cancel' custom action to go back

1.0.96
- implement sticky buttons for simple-form modal
- reset select dropdown to older state and implement close on scroll feature
- fix select dropdown dimension calculations

1.0.95
- adjust sizing of datepicker on mobile screens
- make constriction, search filters, tql and keys passed through input props work again

1.0.94
- add redirect that happens after successful login, destination defined by widget config

1.0.93
- generate reports without dialog in widgets

1.0.92
- collapse buttons when wrapper is too small
- use responsiveness buttons on action bar
- use responsiveness buttons for actions

1.0.91
- add ballot-check icon
- use more generic error message when no Calendar_export_conf could be found

1.0.90
- quick fix that onChange is not infinite often triggered
- make username input prop optional else use current user

1.0.89
- HTML editor: fix saving the form in source editing mode

1.0.88
- enable fullscreen modals for mobile/touch devices

1.0.87
- show collapse/expand icon on touch devices in panel header/footer
- harmonize modal spacing of modal message

1.0.86
- remove date picker arrow as the positioning seems to be off when fields are too long
- fix overflowing text in fullcalender entries
- prevent relation box count label from being cut on overflow

1.0.85
- fix position of reports in form modifier
- fix datetime range with single default value
- do not clear date input on blur

1.0.84
- improve datepicker styling and fix bugs in widgets
- force hover styles of upload button when hovering upload field
- remove advanced search text in modal title
- fix input label being cut in widgets

1.0.83
- remount entity-detail app after navigation
- change signal box background colors for more consistency

1.0.82
- fix change date on range date picker
- fix datetime search field with default value
- load correct store for relations view
- keep ckeditor as similar as legacy editor

1.0.81
- check form definition if form is markable

1.0.80
- add new list action icons
- use actions as fallback content for empty columns and align column content at same height
- use ckeditor
- only cache displays as long as tab is open

1.0.79
- accept fulltext search term for advanced search

1.0.78
- use constrictions from form in remotefield as well
- set current time as default time value in datepicker

1.0.77
- fix navigation to subfolder inside search results

1.0.76
- save prompt w/o explicit cancel button

1.0.75
- fix breakwords for email and phone formatter

1.0.74
- remotefields now sort the same way in select box and advanced search (form sorting first, then last updated)
- fix view persistor in sub grid

1.0.73
- entity docs can now be sorted
- add list-check and percent icons
- allow date inputs without punctuation
- allow two digits year formats
- use save aborted key as textresource instead of message directly

1.0.72
- style new date picker like the old one
- fulltext search fields are now prioritized to be autofocused

1.0.71
- fix datetime in searchforms after using date-fns
- enhance breadcrumbs
- export action now handles additional paths from templates again
- column labels in ColumnPicker no longer display escaped html elements

1.0.70
- add envelope icon
- add icon to email field to mail client
- add link to email and phone formatter
- fix loading form definition for dms

1.0.69
- today button in datepicker now behaves as expected

1.0.68
- fix header text-overflow in firefox
- reports added through formModifier are now placed directly into the action bar, instead of the output group
- report definitions now contain an icon as defined by the backend
- action to subscribe to calendar now displays all personal calendar exports

1.0.67
- remote fields now honor the constrictions defined on forms with remotefield scope
- entity list now uses a form definition passed as input without reloading it
- advanced search now uses form with remotefield scope if it exists
- remotefields now use sorting of their forms or entity models instead of only their timestamp

1.0.66
- optimize toggle behaviour of column picker to only select currently visible items
- do not import react components in widget-utils

1.0.65
- show prompt before action for unsaved changes

1.0.64
- fix remote error handler
- remove momentjs in favor of date-fns
- support simple search flag for fulltext search field
- create removeBoxes to help adjust forms
- allow modification of form in subgrid
- listen to input param changes
- fix error logging app extension
- add error boundary
- add error boundary
- add error boundary
- add error boundary
- add error boundary

1.0.63
- convert phoneEdit to functional component
- convert layout to functional component
- convert statedvalue.stories to functional component
- convert typography to functional component
- convert loginbox to functional component
- prepare for customer actions
- prepare for customer actions

1.0.62
- show today button in datepicker

1.0.61
- fix datepicker max-, mind-date value for ranges

1.0.60
- replace flatpickr with react-datepicker

1.0.59
- add condition to remote and select field
- simple actions add conditions

1.0.58
- add css classes for display expressions styling

1.0.57
- reverse title order to first display entity name and then default display
- make menu buttons inside header responsive, so that oveflowing text gets ellipsis on smaller screens

1.0.56
- enhance column picker ux by adding numbering, mass selection and sorting features

1.0.55
- Length and size validators no longer trigger if the max or min is not defined

1.0.54
- show from and to placeholders on number ranges
- pass sorting objects to entity list app
- Add sorting field to entity-list sagas
- update of the values and field widths in duration edit fixed

1.0.53
- remove static overview page and redirect to dashboard instead

1.0.52
- add form properties to prepare request

1.0.51
- render html escape characters in breadcrumbs

1.0.50
- emit actions to parent
- fix state handling for report settings
- fix form mapping for search componentConfig
- use boolean search component for marking

1.0.49
- fix searchfilters url with query params
- add bundle packages
- create multipel entry files
- fix integer input when min value is set

1.0.48
- don't break table on collapsed sidepanel
- add legacy-widget embedType
- show input on top only for remote multi selects
- fix broken disable styles on empty selects

1.0.47
- support browser plugins for phone numbers
- fix too many field callbacks being called

1.0.46
- fixate menu on multiremote select fields at bottom to improve ux

1.0.45
- support selectors in forms
- show empty fields that are children of selectors

1.0.44
- dependency updates

1.0.43
- remove floating labels in favour of fixed labels

1.0.42
- change rank icon position and refactor table components
- convert HtmlEdit class component to functional component

1.0.41
- debouncer accepts value changes from outside

1.0.40
- allow duration values to be negative
- keep all values on blur

1.0.39
- add modifyFormDefinition input property
- add modifyFormDefinition input property
- add form modifier helper methods
- add report helper actions

1.0.38
- use preformatted error message for tql
- harmonize multi value select
- exclude selected values in suggestion

1.0.37
- query selections now get resolved in the resource-scheduler action
- remove vertical center alignment for marking column

1.0.36
- open panel according to calendar type
- possible to overwrite readonly form
- set focus on input on save filter
- use distinguishable icons for ranges

1.0.35
- invalidate cache on page refresh properly
- correct dimension calculation of select menu since in some situations it does not seem to calculate the correct width/height

1.0.34
- fixed navigating to entity document

1.0.33
- new prop `searchListFormName` to use separate list form for search mode
- define different list form for search mode
- updated fontawesome and added merge icon
- merge action now handles resolved entities instead of selection directly

1.0.32
- filter out null values on nested paths
- handle nested 'to many' relations on list forms

1.0.31
- make notification message at the end of notification center italic
- fix double scrollbar on textarea

1.0.30
- fix popper arrow positioning

1.0.29
- hide meta data on widgets

1.0.28
- use correct files name for upload
- do not reuse same tab for different reports
- update fullcalendar sizes on sidepanel toggle
- attach tether to target to prevent coverage of input fields

1.0.27
- style entity-report action dialog
- harmonize extended search result styling

1.0.26
- add hasFixedWidth prop to harmonize spacing if necessary
- show from and to placeholders on ranges
- add collapse feature to resource-scheduler
- add collapse feature to tql view in entity-list
- add calendar plus and minus icons
- add specific range icons for datetime
- fix date picker modal position

1.0.25
- fix ignored first input click
- update range values properly
- constrain tether to parent element to prevent z-index problems inside modal
- replace old legacy export with new action
- use bolt-lightning as bolt icon
- tql autocompletion now displays localized labels in suggestions

1.0.24
- update react-tether dependency and change Menu.js to new method
- remove react-split-pane dependency as we already have a library called react-split with almost the same features and us it instead
- convert entity-detail class compomeents to functional components
- refactor dispatch actions
- update react-burger-menu and fix input focus
- fix entity-list re-init handling on store updates
- fix docs browser back navigation for search view
- improve data handling on search query changes
- update react-dropzone dependancy
- harmonize error list spacing within forms
- add trigger action event
- add entity-report action

1.0.23
- ace editor works again in create forms
- fix table flicker on firefox

1.0.22
- fields in simple search are shown according to model flag

1.0.21
- menu now closes when navigating to the dashboard
- improve menu search result handling
- onBlur of date component is called with value from onChange again
- legacy actions ignore some exceptions
- fix jumping layout on firefox

1.0.20
- boolean search fields are now displayed as a single select box with fixed values to allow for ternary state and searching for explicit false values
- headers of right aligned table columns are right aligned now
- cleaned up search form types
- introduce input prop `searchFormType`
- routerless docs-browser

1.0.19
- fix activating two factor without session

1.0.18
- searchfilter show description as mouseover

1.0.17
- introduce input prop `constriction`
- Fixed searching in select boxes. Removed default searchOptions from Select.
- Fixed searching in select boxes. Removed default searchOptions from Select.
- onChange is now debounced for CodeEdit
- added new button to clear the current query to QueryView

1.0.16
- The model field in the QueryView is now a display.
- Navigating to detail pages through links in multi-select fields is now possible.
- ResourceScheduler now always open the root entities route when clicking on events.
- Tiny millisecond values (1 - 3) are now displayed correctly.
- fix virtual form field handling
- fix copy Address
- implement theme type logic to enable loading of the widget theme

1.0.15
- show loading mask while fetching info-boxes
- fix cache clearance to keep notifications session
- align choose document button to the right

1.0.14
- Use app locale automatically for all (REST) requests
- render modal action component with right locale
- render subgrids with correct locale
- Using the advanced search in multi-remote fields no longer ignores the current selection.
- Code editors used to write TQL can now be configured in the backend to assume some entity model as the given base, allowing only writing the 'where'- and 'order by'-parts of a query.
- The TQL editor in the query view now uses an implicit base model, which allows auto-complete to work.
- increase table style specificities to prevent them from being overwritten inside widgets
- support `locale` input

1.0.13
- display overflowing hours in durations
- auto focus basic search form
- fix onError of customAction
- change toaster type of aborted action handler
- add connect-principal action
- Add second tab to search form that allows executing custom queries. Add additional actions to open search as query and save query as a new filter.
- change toaster to fixed positioning to guarantee visibility on scroll

1.0.12


1.0.11
- add widget config edit action

1.0.10
- add widget code copy action

1.0.9
- show seconds/milliseconds only for small duration

1.0.8
- use throttle for select instead of debounce to prevent flickering of dropdown
- null pointer fixed in document field formatter (resp. merge action)
- add tql mapping for type text
- improve searching for text based types
- initialize widget with widget config
- harmonize DurationEdit spacing and refactor class based component to functional component
- show seconds, milliseconds for small read-only duration
- allow whitelisted inline css for nice tooltips

1.0.7


1.0.6
- adjust viewport height calculation to correctly render when entering fullscreen mode on android chrome
- clickability fixed of expand/collapse all in module menu
- add admin package version info to about modal

1.0.5
- choose between different scroll behaviours
- show 5 rows per default

1.0.4
- register more icons
- add scrollbar to dashboard panel and refactor dashboard components
- open downloadable file in new tab to avoid errors
- fix docs-browser height inside modal
- remove framer-motion dependency for burger button and create pure css version instead
- fix sticky popover on table
- correct link to user manual
- fix UploadInput.js button titles and refactor components
- add dynamic backend-url as input parameter
- Add notification support for anonymous users
- remove empty menus

1.0.3
- dependency updrade

1.0.2
- add useDidUpdate helper hook
- reuse existing usePrevious hook
- add useApp helper hook

1.0.1
- harmonize popover background color and spacing
- register icons
- replace settings menu icon
- add panel collapse feature to input edit
- register more icons
- change notification title and refactor NotificationCenter
- create env utils
- add generic nice2 fetch wrapper
- use generic request for non-rest requests
- add dynamic backend-url as input parameter

1.0.0
- change toaster behavior to stay on hover and refactor ToasterDisplay
- the first search field in admin search forms now gets automatically focused on opening
- remove X-Enable-Notifications header

0.5.39
- harmonize menu entry padding for cleaner left alignment
- harmonize navigation tab spacing and sizing
- adjust min width of location edit postcode and refactor component
- refactor largeSelectionHandler
- handle-non-existent relation entity gracefully
- enable select dropdown indicators to be aligned at bottom of multiselect fields
- fix html in toaster message
- tql builder support login type
- add no permission message for uploading document
- add title to custom action response
- show no permission error if entity is created
- add no permission message for moving document
- fix menu navigation with arrow keys when some groups were collapsed
- throttle autosize to fix performance issues
- reload breadcrumbs after entity update

0.5.38
- adjust z-index of modal to prevent legacy actions falling behind it
- fix duplicated search entries
- add auto completion for TQL in code fields.
- prevent default behaviour on shortcuts
- set focus to search input on menu change

0.5.37
- fix jumping datepicker on Safari
- show create form for remote fields again in the folder creation form
- debounce autosize on textarea
- fix chevron icon position in relationsview
- move RelationBox to a separate file for cleaner RelationsView
- position collapse buttons in absolute fashion to prevent jumps when scrollbar appears
- harmonize fontawesome icon spacing within table data cell
- register more icons
- restructure menu
- improve dropdown performance for selects on Safari
- fix performance bottleneck on linked sleect values
- save active menu tab on shortcut navigation

0.5.36
- add tooltip for remote fields
- relation counts and current visible relation entities refreshes together with its detail form
- Add Ace editor to code fields.
- add usePrevious helper hook
- cleanup flatpickr components
- propagate onSelectChange
- add option to choose document from dms
- fix dnd in input edit
- fix dashboard searchfilter

0.5.35
- show button hover title and pointer cursor
- use correct button component for icon button
- harmonize detail footer spacing
- fix placing of chevron icon in placeholder panel of admin search form
- keep focus in search input on switching menu tabs
- reset pagination on reload first page
- user agent detection
- improve performance on Safari
- fix missing popover in single values and overflowing in firefox

0.5.34
- show textarea bigger for better distinction
- table can be refreshed
- make menu collapsible
- collapse all and expand all menu entries
- only reload on explicit store updates
- register more icons
- show menu item icons
- increase z-index of popper menu to prevent it slipping behind modal

0.5.33
- Harmonize main menu spacing and add new tab icon
- add breadcrumbs to dashboard
- fix drag and drop
- create link button
- use link button for dashboard link
- Add responsivity to all buttons so that the label disappears on smaller screens and only the icon is displayed
- small dashboard adjustments
- show minimum 5 rows
- cleanup menu shortcuts
- use 3-key-shortcuts everywhere
- Fix datepickr disappearing behind modal

0.5.32
- performance improvements

0.5.31
- add open in new tab to document compact
- only load huge global styles once per app
- Added actions to DMS lists
- Added a preview image on hovering over download icons.
- create dashboard package
- prevent position from switching between after/before
- generic resize util
- use scope for preferences
- add dashboard
- add ListOrDetailLink to navigation strategy
- refactor notification

0.5.30
- add scope as input
- Style detail footer for better UX
- Add minimum box width property to Layout Container to better control column width
- add optional path to save preferences

0.5.29
- Reset legacy overwrite of link color inside toasters
- Fix popper menu disappearing behind bm-menu elements
- Harmonize paragraph spacing whithin fields
- Lighten scrollbar color
- Fix column position change
- Fix popper menu closing immediately when clicking an action

0.5.28
- fix confirm handler
- calculate drop position before or after element
- provide sanitize function for html
- render sanitized html only
- don't resize column after mouse release
- Remote fields and select boxes now only load active relation values.
- add keyField to model transformer
- use dynamic key field

0.5.27
- add footer with meta info
- Don't show 'or with Tocco login' text if no SSO provider
- refactor initial notification

0.5.26
- Improve text input UX within input edit table
- Reduce notification center title size
- fix state propagation in resize hook
- persist columns widths
- Dms edit and create messages clean up
- clean up documentation and fix dispatched actions
- fix websocket reconnection

0.5.25
- fix entity docs url
- Improve table hover colors for better readability
- Change popover text color of paragraphs to white

0.5.24
- fix password update app
- rename username to usernameOrPk for passwordUpdate

0.5.23
- Change icon size within docs-browser
- Harmonize link styling to be consistent
- Increase min-height of filter panel in search form
- fix about tocco
- change dms icons

0.5.22
- Change table hover colors to a more generic grey tone

0.5.21
- Fix opening task execution of notification
- Add button to remote fields that opens a popup containing a create form
- Split session saga
- Fix socket connection handling
- Fix suqare icon display expressions
- Swap out donwload and upload icons to harmonize look
- Change date picker month button color for better visibility
- Harmonize notification center styling

0.5.20
- Update select
- Dms edit and create messages clean up
- Fix calendar entity-list text resources
- Add "open output job" to notification
- Fix disabled delete button if nothing to delete
- Add background color to unread notifications

0.5.19
- Support disablePreferencesMenu in table
- Money post point digits fallback

0.5.18
- Handle 404 of path parts better

0.5.17
- Harmonize collapsible panel styling for better contrast
- Prevent early line breaks in popover menu
- Remove initial underline in notification detail link

0.5.16
- Better breadcrumb error handling

0.5.15
- Open Url in list in new window
- Fix simple-search extend button in remote search
- Improve browser tab title

0.5.14
- Refresh list on navigation back to list
- Harmonize html formatter spacing inside stated value
- Harmonize icon spacing and hover behaviour in notifications
- Add the feature to close modals via escape key
- Add underline to links

0.5.13
- Prevent preview image collapsing to 1px if preview cannot be generated

0.5.12
- Better entity error handling
- Harmonize marked icon inside table
- Harmonize marked icon inside detail view
- Increase z-index of modal holder to properly display as widget
- Show outdated error in detail
- Increase toaster z-index to properly display as widget

0.5.11


0.5.10
- Add collapse user preferences support

0.5.9
- Style location edit dropdown to match other dropdowns
- Remove download icon inside button of report settings
- Adjust notification dot position
- Fix calender search bug
- Fix search-form remote bug

0.5.8
- Added support for "markable" feature
- Added support for "markable" feature
- Prevent dropdown menu being clipped in viewport
- fix notifications
- Fix business unit change in edit action
- Support empty html edit field
- Has value for checkboxes change
- Improve pagination UX by moving it to the left and changing button behavior
- Fix search filter menu hover
- Add preview for documents

0.5.7
- Fix merge error summary
- Add hover effect to column picker list

0.5.6


0.5.5
- Adjust height and width of input edit table
- Fix notifications for already deleted outputjobs

0.5.4
- Adjust height and width of input edit table
- Fix datetime search
- Fix Menu

0.5.3
- Add hover effect to nav switch button
- Align modal globally at same position
- Fix modal overflow
- Style merge errors
- Harmonize toaster styling
- Fix blocking info being covered by header

0.5.2
- Refactor column picker
- Move useDnD
- Add search form customization
- Make select dropdown menu style more uniform for a more consistent UI
- Harmonize select box size und colors
- Change collpase icon for detail and list view

0.5.1
- Fix popper menu on safari browsers
- Fix notification socket url

0.5.0
- New notifications with notification center
- Fix date-time search
- Add panel collapse feature in detail view
- Align nav switch button horizontally
- Fix overlay of modal in widget mode

0.4.53
- Improve input field UX by better highlighting fields and labels
- Adapt input font-sizes for better readability
- Fix selection controller button font color
- Add collapse feature to search panel in list view
- Harmonize menu tree spacing and switch icon size
- Fix legacy actions popup in DMS (was hidden before)
- Disable sorting in "move" action in DMS

0.4.52
- disable client questions for merge action

0.4.50
- Forward input props to move action
- Style nav switch button to better fit into UI
- Disable client questions for merge action
- Fix various dms problems (navigation and search)

0.4.48
- Fix merge close function with query selection
- Add confirm keyboard support

0.4.47
- New menus for system options and all possible options
- Last opened menu is now saved in user preferences
-  Add action selection count confirm

0.4.46
- Adjust spacing and alignment of button inside modal

0.4.45
- Fix report settings

0.4.44

0.4.43
- Edit action only one message
- Entity-Docs open resource in full view

0.4.41
- Change cursor to pointer on panel header/footer hover
- Added options to create a search filter from the current search, edit or delete an existing filter
- Enable body scrolling on login screen for smaller screens
- Use explicit selection style
- datetime search fields now use a date field until the full range is expanded
- Fix hover delay and hover color of search filter buttons

0.4.40
- DMS fixes

0.4.39
- Introduce new prop to determine if layout containers should occupy remaining height on screen
- Adjust left padding of Breadcrumbs in list view
- Location field focus problem fix
- Displayexpression call adjustment

0.4.38
- EntityDocs not sortable
- Dms fixes

0.4.37
- refactor extracting displays
- Fix select menu disappearing behind modal
- Fix fulltext search form

0.4.36
- Fix doc-browsers client questions
- New input prop `businessUnit` to filter by business unit
- Prevent range input overflow
- Adapt table colors to better differentiate from background
- Remove obsolete left padding in table
- Ignore field if path is null in copy action

0.4.35
- Fix doc-browsers client questions

0.4.34
- Prevent stacking of modal overlays
- render description field in form
- Fix popper menu positioning
- Prevent layout container overlap in detail view
- Show not deletable entities with 0
- Fix html edit initial change
- Fix icon and document view in doc browser
- Fix dms reload
- Refactoring list initialization
- Fix tether dropdown z-index to prevent overlay on scroll
- Set background color of breadcrumbs inside doc browser to white

0.4.33
- Clear button in date fields only show up when data has been entered, tab and enter can now be used to navigate in the calendar popup and between date fields
- fix resetting search mode
- Embedd dms in entity view
- Text as html

0.4.32
- Handle client question cancellation
- Keep scroll position on multi select selection
- Fix menu in action
- Move validation helper method to tocco-util
- Add move action

0.4.31
- Introduce onResize external event
- Reset label spacing inside column picker for old client
- Harmonize popover placement on description formatter
- Add aria-labels to improve accessibility
- No list refresh after selection clear
- New input param `domainTypes` to filter domains
- hide list navigation arrow if row is not clickable
- fix long term caching
- Adjust label margin in input fields
- support sorting by search filters and preferences
- reload sources after deployment
- clear selection if parent is set
- dispatch actions if store already exists
- support selection of type query
- Fix fulltext search form
- Restrict max width of popover to 400px
- Reference text ressources for aria-labels for improved localization
- Increase contrast of theme colors to ensure WCAG 2.0 standards
- Change spacing/hover colors inside table and adjust scrollbar width
- Change notifier style to a solid variant to increase contrast/visibility
- Change popover style to solid for increased contrast
- Adjust text shade colors to a lighter variant
- New input parameter `keys`
- New input param `rootNodes` to define root items
- clear selection fire onSelectChange
- Fix fulltext search
- fix load data if parent is changed
- support createuser & updateuser in tql builder

0.4.30
- Introduce onResize external event
- Reset label spacing inside column picker for old client
- Harmonize popover placement on description formatter
- Add aria-labels to improve accessibility
- No list refresh after selection clear
- New input param `domainTypes` to filter domains
- Hide list navigation arrow if row is not clickable
- Fix long term caching
- Adjust label margin in input fields
- Support sorting by search filters and preferences
- Reload sources after deployment
- Clear selection if parent is set
- Dispatch actions if store already exists
- Support selection of type query
- Fix fulltext search form
- Restrict max width of popover to 400px
- Reference text ressources for aria-labels for improved localization
- Increase contrast of theme colors to ensure WCAG 2.0 standards
- Change spacing/hover colors inside table and adjust scrollbar width
- Change notifier style to a solid variant to increase contrast/visibility
- Change popover style to solid for increased contrast
- Adjust text shade colors to a lighter variant

0.4.29
- Introduce onResize external event
- Reset label spacing inside column picker for old client
- Harmonize popover placement on description formatter
- Add aria-labels to improve accessibility
- No list refresh after selection clear
- New input param `domainTypes` to filter domains
- Hide list navigation arrow if row is not clickable
- Fix long term caching

0.4.28
- Introduce onResize external event
- Reset label spacing inside column picker for old client
- Harmonize popover placement on description formatter
- Add aria-labels to improve accessibility
- No list refresh after selection clear

0.4.28


0.4.27
- Dms upload directory (TOCDEV-3042)
- Restrict max height of basic search form only in modal
- Prevent action buttons disappearing on scroll

0.4.26
-

0.4.25
- Change header color
- DMS delete

0.4.24
- Style upload and switch to light icons

0.4.23
- Fix icon and qr code spacing inside two factor connector
- Merge: Remove workaround for opening an entity list in the old client

0.4.22
- Fix spacing of notifiers without title
- Add dms edit action
- Add Domain create action

0.4.21
- Move buttons inside modals to the right to improve UX
- Fix hidden extender and force border rendering of ranges in chrome
- Fix empty report settings (TOCDEV-3218)

0.4.20
- Fix caching of business unit
- Vertically center spinner icon in delete progress
- Style merge table header contents for better UX
- Style notifier popups to better harmonize with the existing styling
- Make panel header clickable so it can also expand the panel
- Fix logo not displaying in login screen on iOS
- Fix height and action button styling of detail form in old client

0.4.19
- Fix empty create forms because of missing field information

0.4.17
- Fix merge table and harmonize spacing/text
- Harmonize text spacing in two factor connector
- Fix checkbox and menu background hover inside table heading
- support fullscreen action callbacks
- Set origin id for each session (TOCDEV-2980)
- Writable mutlipath fix (TOCDEV-3012)
- Writable mutlipath fix (TOCDEV-3012)
- Load two-factor-connector in login when two-factor activation is forced
- Fix autofocus if first field is a textarea

0.4.16
- Support deletion of documents in docs route
- Fix relation selection bug (TOCDEV-3037)
- Fix default value serach filter bug

0.4.15
- Harmonize icon size and text spacing
- Fix blank screen after navigating away from the calendar
- Style datepicker so that it fits better into the rest of admin
- Add a popover variant that is placed on the right of element
- Prevent flickering effect for tooltips
- New input prop `contextParams` to extend context
- New action in the docs route to create a folder
- Support shrinkToContent column attribute
- Fix search form endless loading with empty filter field
- Fix path dirty bug
- Fix missing default values bug

0.4.13
- Support uploading documents in DMS
- Merge V2!

0.4.12
- Text autosize new component (Fixes Safari performance problems)

0.4.11
- Adjust left pane grid on for android tablets to prevent overflow
- Two Factor Connector process optimizations
- Upload component: Show file names only in upload process
- Style two-factor-connector app
- Login: fix leading zeros bug in two-factor code

0.4.10
- Async validation on blur

0.4.9
- Prevent content ghosting in safari when opening/closing collapsibles
- Add password-update and two-factor-connector to menu
- Update dependencies
- Update FullCalendar

0.4.8
- Introduce navigationStrategy
- Remove favicons inside react project, as they will loaded via the static index.html
- Remove obsolete right margin inside menu item
- Display integer and counter data unformatted
- Add copy action

0.4.7
- Refactor Layout components to use CSS Grid instead of JS for nested elements
- Fix positioning and z-index of ActionsWrapper
- Improve render of multi column layouts
- Change the text format if there are only fieldErrors
- Persist selected relation tab on detail
- Focus on first input field

0.4.6
- Use same width for all Fontawesome icons
- Introduce custom cell renderers
- New type icons in DMS list
- Disable pointer event on Desktop as text cant be copied otherwise in Firefox

0.4.5
- Fix top padding of modal
- Fix padding of immutable stated values

0.4.4
- Fix column picker styling
- Use column label as titles when hovering over headers
- Prevent mobile keyboard from pushing the modal up
- Fix bug on list and detail

0.4.3
- Fix table reset

0.4.2
- Fix the label of richtext fields to the top
- Reset load mask height to 100%
- Modal refactoring
- Remove hover background color on active filter buttons
- Add height to label to prevent vertical clip
- Hide overflow of single value element
- Default search filter is disabled when opening the relations view of an entity as its own list
- Prevent click on disabled date input field

0.4.1
- Prevent null pointer when opening DMS (no action bar in form model)

0.4.0
- Add initial version of DMS
- Hide burger menu button behind legacy modal window mask

0.3.36
- Fix reports without custom settings
- Add minimal padding to search filter to avoid scrollbar on fewer entries
- Style button inside table
- Add title tooltip to button
- Make whole row clickable in list

0.3.35
- Fix advanced search menu bug
- Change typography link color to secondary (blue)
- Do not display relation to parent entity in column picker if opened in a relation tab
- Display id instead of label if the label is empty in column

0.3.34
- Fix modal being push out of view when keyboard pops up on iOS
- Prevent label overflow inside input field
- Prevent popper menu disappearing behind main menu overlay

0.3.33
- Adjust tile width of modal to match modal width
- Adjust top margin of modal content
- Set height and width attribute to allow the browser to calculate/reserve sufficient space and minimize layout shifts
- Remove hover of relation boxes on mobile devices
- Add caption to preview
- Improve caching (long-term cache)

0.3.32
- Style popover to better align with overall design and have better contrast
- Add margin to error items inside popper
- Adjust entity list height inside detail view to scale on smaller screens
- Change link colors to secondary color

0.3.31
- Adjust relations view height for smaller screens
- Increase min-width of table column for more usability on smaller screens
- Disable touch on mobile and adjust margin
- Autocomplete support
- Fix select for mobile
- Fix display bug

0.3.30
- Adjust filter pane height when expanded
- Change chevron icon in multi select input when dropdown is opened
- Change form field colors. Dirty: blue, Mandatory: organge
- Fix create reverse relation
- Open remote fields on second click
- Improve form error notification
- Set max width in layout box to prevent overflowing elements
- Hide horizontal scrollbar to prevent it appearing on while loading

0.3.29
- Remove stated value error animation, as it caused a wobble effect
- Extend actiom handling (precheck and initial forms with data)
- Optimize input edit initialization
- Fix relation columns edit bug


0.3.28
- Change sort icon order and adjust table header styles to show marker on long columns when dragging
- Reset date/time indicators on input fields
- Fix search filter sorting

0.3.27
- List performance optimizations
- Update style components

0.3.26
- Reset css overwrites of Upload and refactor Preview
- Restrict urls in table to one line
- Fix error field focus bug
- Fix breadcrumbs action bug
- Add create breadcrumbs
- Adjust search filter height
- Set min and max width for modal

0.3.25
- Initialize container size for correct display in modal
- Add min width to modal
- Center QR Code

0.3.24
- Searchform bug fix

0.3.23
- Narrow left panel width of input edit
- Improve UX of list and search view within modal
- Build fields without model (refactoring)
- Hide readonly fields without value
- Style column picker dialog inside modal
- Fix overflow of column picker in case of long texts and change ok button look
- Remove all usages of old display endpoints (refactoring)

0.3.22
- Change div to flex item for proper display in old client
- Change flex properties for proper display in old client
- Adjust search filter area height to have minimal padding at bottom when expanded
- Set box sizing on td to prevent overwrite in external context
- Change error view background and icon size
- Admin loadCurrentViewInfo refactoring

0.3.21
- Remove about tocco modal title
- Form builder refactoring
- Fix action query selection bug

0.3.20
- Display search filter button only on hover - else hide
- Change help menu icon
- Hide action in advanced search
- Fix remote field

0.3.19
- Handle strings in search with "like" and add boolean handler
- Expand search filter list button area for better UX

0.3.18
- Use recaptcha v2
- Add tabindex for password update dialog
- Entity list: Add support for `clickable` attribute
- Menu entry to edit columns and reset

0.3.17
- Adjust gutter height in admin search form
- Style error view

0.3.16
- Change header ball menu icon to info symbol
- Style draggable gutter for more usability and design consistency
- Revert resource-scheduler tooltip border

0.3.15
- Add admin help menu
- Fix width of table cells so that there is no overlap at the last column on smaller screens
- Align bm button menu with content
- Add bottom padding to search box to prevent see through of underlying text
- Style input edit table to ensure consistend UI. Remove obsolete split pane feature
- Invert header color for TEST/Prod

0.3.14
- Style header logo depending on runEnv
- Use KEYS instead of IN in tql
- Style draggable table headers and kabob menu
- Show relation create button depending of permissions
- Handle notifications depending on parent
- Fix iOS relations boxes on iOS Tablets and restrict width of left pane in detail view to 40%
- Remove obsolete default searchfield styling and height in nested layout containers
- Change Stated and Editablevalue colors for more contrast/readability
- Fix admin resource scheduler bug

0.3.13
- Allow calendars to be preselected by passing them to the preselectedCalendars input
- Table component
- Added button to reset all preferences
- Implement responsive height fixes for tablets
- Make all children of readonly layouts readonly
- Style input fields according to material design

0.3.12


0.3.11
- Improve handling of empty values in number fields

0.3.10
- Center search filter icon vertically
- Finish styling of input edit table
- Center input field of select
- Fix sizing of ball

0.3.9
- Place button groups on same level
- Prevent action menu appearing underneath fixed menu bar
- Enable wrapping of buttons in detail view with narrower screen width
- Harmonize button spacing and sizing in list view
- Harmonize button sizing and spacing of button groups
- Adapt navigation bar width to match the width of list view sidebars
- Style entity view screens
- NumberFormatter can now handle options for setting the minimum fraction digits
- InputEditTable now displays all readonly fields a FormattedValues
- Allow multiple selected Inputs to be handled in input-edit, if they are compatible.
- Add vertical scrolling to input edit table
- Adjust minimum height of table content to 300px
- Style kabob menu inside table header

0.3.8
- Style InputEdit Table
- Harmonize relations view spacing
- Enable 100% height of layout container in old client
- Menu makeover
- Style new dropdown menu
- Harmonize menu icon spacing
- Reposition notifier close icon
- sort list by preferences and save new preferences when changing the sorting
- Add button to reset sorting to entity-list
- Fix icon positioning within buttons
- Fix relation count

0.3.7
- Table column positioning
- Style Two-Step Login
- Fix Range delete value bug

0.3.6
- Fix delete bug

0.3.5
- Support time fields in search form
- Fix split action button
- Display notifications correctly in InputEdit

0.3.4
- Fix shy in table header
- Delete sso cookie on logout
- Adjust HTML Edit
- Optimize admin view loading
- Add actions to input-edit
- Enable notifications in input-edit
- Add information box to input-edit
- Fix readonly fields shown as editable
- Improve date time rendering
- Adjust table cell width of last column

0.3.3
- Mitigate unwanted legacy action loading effects
- Harmonize notification spacing
- Enable full height scaling of detail view
- Add legacy icons
- Adjust list limits
- Adjust advanced-search list limit
- Enable table height in resource calender to full height
- Enable hover effect on first two table header elements
- Delete Action
- Harmonize question icon size/spacing in detail view
- Adjust fulltext search to work the same way as in old client

0.3.2
- Support description fields

0.3.1
- Adjust table padding and hover table heading hover
- Slim down left panel width in entity list view
- Remove additional clear icon in safari
- Fix fuzzy rendering of bold fonts in firefox

0.3.0
- Table v2
- Fix display expressions
- Add custom app action handler

0.2.9
- Support new two-factor authentication
- Adjust two factor handling

0.2.8
- Fix two-factor authentication bug
- Hide actions in preview list
- Use new search filter endpoint
- Harmonize icon spacing in preview box
- Harmonize modal box spacing
- Disable caching for DEV
- Make welcome screen responsive
- Support multi-path select and remote fields

0.2.7
- Fix admin width problem

0.2.6
- Style range component
- Small date component improvements
- Fix fallback sorting bug
- Fix label null bug
- Add captcha to login
- Fix bug where old pw was displayed as invalid

0.2.5
- Component type fix
- Fix burger button
- Adapt success color for better readability

0.2.4
- Panel animation refactoring
- Animate burger icon
- Fix &shy; in label
- Improve modal ux

0.2.3
- Backend: Send searchform values with tql
- Minimize login form dimensions
- Menu arrow navigation
- Harmonize stated value box padding
- SSO integration

0.2.2
- Button hover styling
- Style display-expressions
- Adjust search-filter size automatically
- Crop search filter text
- Make login responsive
- Button adjustment for mobile
- Style button dropdown list
- Constriction bug fix

0.2.1
- Adjust responsive behavior of layout box in detail view
- Fix reports
- Animate validate error list

0.2.0
- Fix display-expression not shown in list view
- Basic display-expression bootstrap class support
- Fix display-expression sorting in list view
- Create multi relation create fix
- Optimize async validation in create
- Persist search-filters
- Fix prompt when changing relations preview
- Use relation-count client endpoint

0.1.26
- Fix principal service usage

0.1.25
- Fallback sorting (Last_updated) in lists
- Support of code and ipaddress datatypes fields

0.1.24
- Increase list limit
- Style fixing

0.1.23
- Fix list scrollbar

0.1.22
- Change fontSize and spacing
- Style List View
- Remove scrollbar of table
- Button styling
- Fix small icon bug

0.1.21
- Style filter buttons
- Welcome screen
- Detail view styling

0.1.20
- Style list view
- Style breadcrumbs
- Fetch form with scope (update/create)
- Rearrange search form buttons
- Load language from browser
- Translate breadcrumbs entity name
- Add User and Businessunit menu
- Disable word-break in table

0.1.19
- Fix upload

0.1.18
- Save button in action bar
- Style relations view

0.1.17
- Fix images

0.1.16
- New Icon set
- Styled search-box
- Styled top bar and menu
- Styled login text
- Styled breadcrumbs
- New selection controller

0.1.15
- Fix location field
- Fix sso-login

0.1.14
- Further style login view
- Use rest 2.0

0.1.13
- Fix bug where images were not loaded correctly

0.1.12
- Add relation preview open link on detail view
- Style login and sso-login
- Fix parent Search field bug
- Fix url view bug

0.1.11
- Fix navigation bug where the same url loaded different views
- View caching

0.1.10
- Restrict global icon dom watch
- Add search-filters to admin search form

0.1.9
- Menu endpoint url fix
- Size and padding

0.1.8
- Search Form Prototype

0.1.7
- Settings Menu
- Menu Shortcut fix
- Local caching
- Show current beta version on Dashboard

0.1.6
- General performance optimizations

0.1.5
- Detail performance improvement
- Relation view in detail

0.1.3
- menu and navigation improvements
- detail dashboard

0.1.2
- improved navigation and detail view

0.1.1
- Basic routing and prototype
- Login prototype

