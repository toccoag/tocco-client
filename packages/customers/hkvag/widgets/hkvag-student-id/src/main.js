import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {appContext, consoleLogger, env, reducer as reducerUtil} from 'tocco-util'

import StudentId from './components/StudentId'
import reducers, {sagas} from './modules/reducers'

const packageName = 'hkvag-student-id'

const initApp = (id, input, events, publicPath) => {
  const content = <StudentId />

  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const HkvagStudentIdApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

HkvagStudentIdApp.propTypes = {
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  limit: PropTypes.number,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired
}

export default HkvagStudentIdApp
export const app = appFactory.createBundleableApp(packageName, initApp, HkvagStudentIdApp)
