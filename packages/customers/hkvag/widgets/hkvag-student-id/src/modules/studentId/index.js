import {fetchUser} from './actions'
import reducer from './reducer'
import sagas from './sagas'

export {fetchUser, sagas}
export default reducer
