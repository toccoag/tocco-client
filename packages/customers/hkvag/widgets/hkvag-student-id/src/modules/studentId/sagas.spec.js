import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import * as sagas from './sagas'

describe('student-id', () => {
  describe('modules', () => {
    describe('studentId', () => {
      describe('sagas', () => {
        describe('fetchUser', () => {
          test('should read user data from rest-API', () => {
            const resultData = '1'
            const response = [{key: '1'}]

            return expectSaga(sagas.fetchUser)
              .provide([[matchers.call.fn(rest.fetchEntitiesPage), response]])
              .put(actions.setUser(resultData))
              .run()
          })

          test('should handle error', () => {
            return expectSaga(sagas.fetchUser)
              .provide([[matchers.call.fn(rest.fetchEntitiesPage), throwError(new Error('Failed to fetch data'))]])
              .run()
          })
        })
      })
    })
  })
})
