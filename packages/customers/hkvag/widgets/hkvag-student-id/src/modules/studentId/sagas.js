import {all, call, put, takeEvery} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {consoleLogger} from 'tocco-util'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeEvery(actions.FETCH_USER, fetchUser)])
}

export function* fetchUser() {
  try {
    const response = yield call(rest.fetchEntitiesPage, 'User', {where: 'pk == :currentUser'}, {method: 'GET'})
    yield put(actions.setUser(response[0].key))
  } catch (err) {
    consoleLogger.logError('Failed to get selected User', err)
  }
}
