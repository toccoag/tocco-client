export const FETCH_USER = 'studentId/FETCH_USER'
export const SET_USER = 'studentId/SET_USER'

export const fetchUser = () => ({
  type: FETCH_USER
})

export const setUser = user => ({
  type: SET_USER,
  payload: {
    user
  }
})
