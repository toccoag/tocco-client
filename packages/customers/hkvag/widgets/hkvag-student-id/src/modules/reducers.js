import studentIdReducer, {sagas as studentIdSagas} from './studentId'

export default {
  studentId: studentIdReducer
}

export const sagas = [studentIdSagas]
