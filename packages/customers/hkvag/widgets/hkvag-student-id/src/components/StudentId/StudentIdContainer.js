import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {fetchUser} from '../../modules/studentId'

import StudentId from './StudentId'

const mapActionCreators = {fetchUser}

const mapStateToProps = state => ({
  formBase: state.input.formBase,
  entityName: state.input.entityName,
  reportIds: state.input.reportIds,
  user: state.studentId.user,
  backendUrl: state.input.backendUrl,
  businessUnit: state.input.businessUnit,
  appContext: state.input.appContext
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(StudentId))
