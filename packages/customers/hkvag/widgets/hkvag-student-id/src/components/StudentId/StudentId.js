import PropTypes from 'prop-types'
import {useEffect} from 'react'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {LoadMask} from 'tocco-ui'
import {appContext as appContextPropType} from 'tocco-util'

const StudentId = ({fetchUser, formBase, entityName, reportIds, user, backendUrl, businessUnit, appContext}) => {
  useEffect(() => {
    fetchUser()
  }, [fetchUser])

  return (
    <>
      <LoadMask required={[user]}>
        <EntityBrowserApp
          entityName={entityName}
          formBase={formBase}
          initialKey={user}
          reportIds={reportIds}
          backendUrl={backendUrl}
          businessUnit={businessUnit}
          appContext={appContext}
        />
      </LoadMask>
    </>
  )
}

StudentId.propTypes = {
  fetchUser: PropTypes.func.isRequired,
  formBase: PropTypes.string,
  entityName: PropTypes.string,
  reportIds: PropTypes.arrayOf(PropTypes.string),
  user: PropTypes.string,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired
}

export default StudentId
