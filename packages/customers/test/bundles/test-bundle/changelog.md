3.13.23
- make widget date picker more compact

3.13.22
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- align ranges horizontally for widgets

3.13.21
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.20
- add isTransparent prop to Ball to enable transparent backgrounds

3.13.19
- fix document component

3.13.18
- fix modal in routerless widgets

3.13.17
- show icon on tiles
- add icons

3.13.16
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- add week numbers to date picker

3.13.15
- add labelVisibility prop to Button to enable omitting the prop in specific cases

3.13.14
- fix sidepanel button overflow
- able to set justify alingment on tile column
- handle use label properly

3.13.13
- harmonize react select spacing

3.13.12
- fix focus styling

3.13.11
- close all modals on route change
- adapt component list text resources

3.13.10
- fix adress / location browser autofill
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.9
- force rerender of ckeditor with auto-complete
- export idSelector
- fix ckeditor and initial auto complete

3.13.8
- improve label / form field component accessibility
- improve tile responsiveness on mobile devices

3.13.7
- fix stylelint error by removing obsolete styling

3.13.6
- remove useSelectorsByDefault property

3.13.5
- allow setting title for button
- pass inWrongBusinessUnit

3.13.4
- add data-cy to table refresh button

3.13.3
- add background color for table tiles
- improve tile styling

3.13.2
- no error message on column resize

3.13.1
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- show tile on narrow phones
- hide empty cells on tile

3.13.0
- initial release for version 3.13

