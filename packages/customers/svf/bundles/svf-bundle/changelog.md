3.13.25
- preserve splat path after creating entity

3.13.24
- added data-cy to mail-action button
- handle null minChars in formDefinition

3.13.23
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.22
- adjust min char search field validation
- clean up searchFormValiation

3.13.21
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.20
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.19
- fix stated value labels spacing
- fix color of label-neutral

3.13.18
- harmonize google maps icon positioning in LocationEdit

3.13.17
- add timezone header

3.13.16
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to history

3.13.15
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.14
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.13
- cleanup save button code

3.13.12
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.11
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.10
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.9
- cleanup marking code
- add native date(time)picker for touch devices
- remove custom _widget_key param

3.13.8
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.7
- add _widget_key query param to all requests

3.13.6
- pass escapeHtml to display formatter

3.13.5
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.4
- fix growing textarea
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.3
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper

3.13.2
- disable register button when required modules are forbidden

3.13.1
- fix sorting of answer options
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.19
- revert focus fix

3.12.18
- do not auto focus anything on touch devices

3.12.17
- improve UX for simple selects on touch devices

3.12.16
- fix double scrollbars in some modals
- fix height of exam edit

3.12.15
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.14
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.13
- add tooltip texts for buttons of upload component

3.12.12
- Make submit button hideable

3.12.11
- fix text select input edit and show available options
- fix table edit styling

3.12.10
- set checkbox field to touched on safari/ipad/iphone

3.12.9
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.8
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.7
- add copy function to entity-browser

3.12.6
- show correct link for subtables

3.12.5
- hide table refresh button when nothing happens

3.12.4
- able to navigate to create view on detail

3.12.3
- Make buttons in evaluation-execution-participation-action customizable

3.12.2
- remove selection controller in dms

3.12.1
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on toaster close buttons

3.12.0
- initial release for version 3.12

