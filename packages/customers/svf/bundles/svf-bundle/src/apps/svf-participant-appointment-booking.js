import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-svf-participant-appointment-booking/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
