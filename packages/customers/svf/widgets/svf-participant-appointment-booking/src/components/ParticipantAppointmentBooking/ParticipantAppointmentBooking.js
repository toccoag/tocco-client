import PropTypes from 'prop-types'
import {useMemo, useState} from 'react'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {VisibilityStatus} from '../../visibilityStatus'

const ParticipantAppointmentBooking = props => {
  const {
    entityName,
    formBase,
    searchFilters,
    searchFormType,
    limit,
    backendUrl,
    businessUnit,
    appContext,
    reportIds,
    locale,
    fireVisibilityStatusChangeEvent,
    onVisibilityStatusChange
  } = props

  const [hideEntityBrowser, setHideEntityBrowser] = useState(false)

  const customActionEventHandlers = useMemo(
    () => ({
      participantAppointmentBooking: {
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        }
      }
    }),
    [fireVisibilityStatusChangeEvent]
  )

  return (
    <EntityBrowserApp
      entityName={entityName}
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      searchFormType={searchFormType}
      reportIds={reportIds}
      locale={locale}
      onVisibilityStatusChange={onVisibilityStatusChange}
      customActionEventHandlers={customActionEventHandlers}
      isHidden={hideEntityBrowser}
      onRouteChange={() => setHideEntityBrowser(false)}
    />
  )
}

ParticipantAppointmentBooking.propTypes = {
  entityName: PropTypes.string.isRequired,
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  intl: PropTypes.object.isRequired,
  locale: PropTypes.string,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  onVisibilityStatusChange: PropTypes.func.isRequired
}

export default ParticipantAppointmentBooking
