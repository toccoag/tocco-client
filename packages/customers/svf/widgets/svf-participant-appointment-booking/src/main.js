import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext, consoleLogger, env} from 'tocco-util'

import ParticipantAppointmentBooking from './components/ParticipantAppointmentBooking'

const packageName = 'svf-participant-appointment-booking'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <ParticipantAppointmentBooking />

  const store = appFactory.createStore({}, null, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {component} = initApp(packageName, input)

      appFactory.renderApp(component)
    }
  }
})()

const ParticipantAppointmentBookingApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

ParticipantAppointmentBookingApp.propTypes = {
  entityName: PropTypes.string.isRequired,
  formBase: PropTypes.string.isRequired,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  businessUnit: PropTypes.string,
  searchFormType: searchFormTypePropTypes,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  backendUrl: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default ParticipantAppointmentBookingApp
export const app = appFactory.createBundleableApp(packageName, initApp, ParticipantAppointmentBookingApp)
