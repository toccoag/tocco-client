import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {notification, form} from 'tocco-app-extensions'
import {LoadMask} from 'tocco-ui'

import ProjectApplicationForm from '../ProjectApplicationForm'

const ProjectApplicationExaminee = ({
  formDefinition,
  formInitialValues,
  fieldDefinitions,
  mode,
  loadView,
  unloadView
}) => {
  useEffect(() => {
    loadView()
    return () => {
      unloadView()
    }
  }, [loadView, unloadView])

  const handleAsyncValidate = form.hooks.useAsyncValidation({formInitialValues, fieldDefinitions, formDefinition, mode})
  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  const fieldDefinitionPaths = fieldDefinitions.map(fD => fD.path)

  return (
    <>
      <notification.Notifications />
      <LoadMask required={[formInitialValues]}>
        <ProjectApplicationForm
          validate={handleSyncValidate}
          asyncValidate={handleAsyncValidate}
          asyncBlurFields={fieldDefinitionPaths.map(form.transformFieldName)}
        />
      </LoadMask>
    </>
  )
}

ProjectApplicationExaminee.propTypes = {
  mode: PropTypes.string.isRequired,
  formDefinition: PropTypes.object.isRequired,
  fieldDefinitions: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string,
      id: PropTypes.string,
      validation: PropTypes.object
    })
  ).isRequired,
  unloadView: PropTypes.func.isRequired,
  loadView: PropTypes.func.isRequired,
  formInitialValues: PropTypes.shape({
    initial: PropTypes.objectOf(PropTypes.string)
  })
}

export default ProjectApplicationExaminee
