import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'

import {unloadView, loadView} from '../../modules/projectApplicationExaminee/actions'
import {REDUX_FORM_NAME} from '../ProjectApplicationForm'

import ProjectApplicationExaminee from './ProjectApplicationExaminee'

const mapActionCreators = {
  unloadView,
  loadView
}

const mapStateToProps = state => ({
  mode: state.projectApplicationExaminee.mode,
  formDefinition: state.projectApplicationExaminee.formDefinition,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state),
  fieldDefinitions: state.projectApplicationExaminee.fieldDefinitions
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ProjectApplicationExaminee))
