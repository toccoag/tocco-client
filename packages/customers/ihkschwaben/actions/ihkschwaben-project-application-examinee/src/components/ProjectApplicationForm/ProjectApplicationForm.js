import PropTypes from 'prop-types'
import {useEffect, useRef} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {SaveButton} from 'tocco-entity-detail/src/main'
import {BackButton, Button} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

export const REDUX_FORM_NAME = 'project-application-examinee-action'

const ProjectApplicationForm = ({
  entity,
  formDefinition,
  fieldDefinitions,
  formValues,
  submitForm,
  submitFormSetStatus,
  intl,
  submitting,
  valid,
  anyTouched,
  fireTouched,
  dirty,
  formErrors,
  mode,
  fireExternalEvent
}) => {
  const handleBack = () => fireExternalEvent('onCancel')

  const formEl = useRef(null)

  customHooks.useAutofocus(formEl)
  const formEventProps = form.hooks.useFormEvents({submitForm})

  useEffect(() => {
    fireTouched(dirty && anyTouched)
  }, [dirty, anyTouched, fireTouched])

  const customRenderedActions = {
    cancel: actionDefinition => (
      <BackButton onClick={handleBack} icon={actionDefinition.icon} label={actionDefinition.label} />
    ),
    save: actionDefinition => (
      <SaveButton
        submitting={submitting}
        mode={mode}
        hasErrors={!valid && anyTouched}
        formErrors={formErrors}
        entity={entity}
        fieldDefinitions={fieldDefinitions}
        {...actionDefinition}
      />
    ),
    save_and_set_status: actionDefinition => (
      <Button
        intl={intl}
        disabled={submitting || !valid}
        mode={mode}
        hasErrors={!valid && anyTouched}
        formErrors={formErrors}
        entity={entity}
        fieldDefinitions={fieldDefinitions}
        onClick={submitFormSetStatus}
        {...actionDefinition}
      />
    )
  }

  return (
    <form {...formEventProps} ref={formEl}>
      <form.FormBuilder
        entity={entity}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        mode={mode}
        customRenderedActions={customRenderedActions}
      />
    </form>
  )
}

ProjectApplicationForm.propTypes = {
  intl: PropTypes.object.isRequired,
  formDefinition: PropTypes.object.isRequired,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object).isRequired,
  entity: PropTypes.object.isRequired,
  mode: PropTypes.string.isRequired,
  form: PropTypes.string.isRequired,
  submitForm: PropTypes.func.isRequired,
  submitFormSetStatus: PropTypes.func.isRequired,
  fireTouched: PropTypes.func.isRequired,
  formValues: PropTypes.object,
  submitting: PropTypes.bool,
  formErrors: PropTypes.object,
  valid: PropTypes.bool,
  dirty: PropTypes.bool,
  anyTouched: PropTypes.bool,
  fireExternalEvent: PropTypes.func.isRequired
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(ProjectApplicationForm)
