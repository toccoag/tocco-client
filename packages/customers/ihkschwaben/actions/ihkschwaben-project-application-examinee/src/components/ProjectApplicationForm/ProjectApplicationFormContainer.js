import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form, externalEvents} from 'tocco-app-extensions'

import {submitForm, fireTouched, submitFormSetStatus} from '../../modules/projectApplicationExaminee/actions'

import ProjectApplicationForm, {REDUX_FORM_NAME} from './ProjectApplicationForm'

const mapActionCreators = {
  submitForm,
  fireTouched,
  submitFormSetStatus,
  fireExternalEvent: externalEvents.fireExternalEvent
}

const mapStateToProps = state => ({
  mode: state.projectApplicationExaminee.mode,
  formDefinition: state.projectApplicationExaminee.formDefinition,
  fieldDefinitions: state.projectApplicationExaminee.fieldDefinitions,
  entity: state.projectApplicationExaminee.entity,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ProjectApplicationForm))
