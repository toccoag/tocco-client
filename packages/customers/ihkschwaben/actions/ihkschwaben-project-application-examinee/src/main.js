import PropTypes from 'prop-types'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  notification,
  formData,
  form,
  selection
} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, appContext, consoleLogger, env} from 'tocco-util'

import ProjectApplicationExaminee from './components/ProjectApplicationExaminee'
import reducers, {sagas, formSagaConfig} from './modules/reducers'

const packageName = 'ihkschwaben-project-application-examinee'
const EXTERNAL_EVENTS = ['onCancel']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <ProjectApplicationExaminee />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store)
  notification.addToStore(store, true)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  form.addToStore(store, formSagaConfig)
  formData.addToStore(store, () => ({listApp: EntityListApp}))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'docs-browser', 'entity-detail', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const ProjectApplicationExamineeApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

ProjectApplicationExamineeApp.propTypes = {
  /**
   * Selection of a single `Project_application` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired,
  actionProperties: PropTypes.shape({
    /**
     * Default: Project_application_examinee
     */
    projectApplicationExamineeForm: PropTypes.string.isRequired
  }),
  formName: PropTypes.string.isRequired,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  backendUrl: PropTypes.string
}

export default ProjectApplicationExamineeApp
export const app = appFactory.createBundleableApp(packageName, initApp, ProjectApplicationExamineeApp)
