import {SubmissionError} from 'redux-form/es/SubmissionError'
import {call} from 'redux-saga/effects'
import {form, rest} from 'tocco-app-extensions'

const SUCCESSFUL_SAVED_STATUS = 201

export function* updateProjectApplication(entity, fieldDefinitions, setStatus, formName) {
  const options = {
    method: 'PATCH',
    body: {
      entity,
      setStatus,
      formName
    },
    acceptedErrorCodes: ['VALIDATION_FAILED'],
    acceptedStatusCodes: [403, 409]
  }

  const resp = yield call(rest.requestSaga, 'ihkschwaben/action/projectApplicationExaminee', options)

  if (resp.status !== SUCCESSFUL_SAVED_STATUS) {
    if (resp.body && resp.body.errorCode === 'VALIDATION_FAILED') {
      throw new SubmissionError(form.validationErrorToFormError(entity, fieldDefinitions, resp.body.errors))
    }

    if (resp.status === 403) {
      throw new rest.ForbiddenException()
    }

    if (resp.status === 409 && resp.body.information) {
      throw new rest.InformationError(resp.body.information)
    }

    throw new Error('unexpected error during save', resp)
  }
}
