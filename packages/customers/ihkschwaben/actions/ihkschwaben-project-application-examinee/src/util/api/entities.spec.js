import {SubmissionError} from 'redux-form/es/SubmissionError'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import {updateProjectApplication} from './entities'

describe('ihkschwaben-project-application-examinee', () => {
  describe('util', () => {
    describe('api', () => {
      describe('entites', () => {
        describe('updateProjectApplication', () => {
          test('should patch ProjectApplication', () => {
            const entity = {}
            const setStatus = false
            const formName = 'Project_application_examinee_submission'
            const fieldDefinitions = {}

            const expectedOptions = {
              method: 'PATCH',
              body: {
                entity,
                setStatus,
                formName
              },
              acceptedErrorCodes: ['VALIDATION_FAILED'],
              acceptedStatusCodes: [403, 409]
            }

            const response = {body: {test: 'hi'}, status: 201}

            return expectSaga(updateProjectApplication, entity, fieldDefinitions, setStatus, formName)
              .provide([[matchers.call.fn(rest.requestSaga), response]])
              .call(rest.requestSaga, 'ihkschwaben/action/projectApplicationExaminee', expectedOptions)
              .run()
          })

          test('should handle information error', () => {
            const entity = {}
            const setStatus = false
            const formName = 'Project_application_examinee_submission'
            const fieldDefinitions = {}

            const expectedOptions = {
              method: 'PATCH',
              body: {
                entity,
                setStatus,
                formName
              },
              acceptedErrorCodes: ['VALIDATION_FAILED'],
              acceptedStatusCodes: [403, 409]
            }

            const response = {status: 409, body: {information: 'not good'}}

            return expectSaga(updateProjectApplication, entity, fieldDefinitions, setStatus, formName)
              .provide([[matchers.call.fn(rest.requestSaga), response]])
              .call(rest.requestSaga, 'ihkschwaben/action/projectApplicationExaminee', expectedOptions)
              .throws(rest.InformationError)
              .run()
          })

          test('should handle validation error', () => {
            const entity = {model: 'Project_application', key: '25'}
            const setStatus = false
            const formName = 'Project_application_examinee_submission'
            const fieldDefinitions = [{id: 'zip', componentType: 'field', dataType: 'text'}]

            const expectedOptions = {
              method: 'PATCH',
              body: {
                entity,
                setStatus,
                formName
              },
              acceptedErrorCodes: ['VALIDATION_FAILED'],
              acceptedStatusCodes: [403, 409]
            }

            const response = {
              body: {
                errorCode: 'VALIDATION_FAILED',
                errors: [{model: 'Project_application', key: '25', paths: {zip: {mandatory: 'mandatory'}}}]
              },
              status: 403
            }

            const expectedErrors = {
              _error: {relatedEntityErrors: []},
              zip: {mandatory: 'mandatory'}
            }

            return expectSaga(updateProjectApplication, entity, fieldDefinitions, setStatus, formName)
              .provide([[matchers.call.fn(rest.requestSaga), response]])
              .call(rest.requestSaga, 'ihkschwaben/action/projectApplicationExaminee', expectedOptions)
              .throws(new SubmissionError(expectedErrors))
              .run()
          })
        })
      })
    })
  })
})
