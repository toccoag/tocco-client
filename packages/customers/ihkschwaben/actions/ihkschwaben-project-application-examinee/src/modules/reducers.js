import {reducer as form} from 'redux-form'

import projectApplicationExamineeReducer, {
  sagas as projectApplicationExamineeReducerSagas,
  formSagaConfig
} from './projectApplicationExaminee'

export default {
  projectApplicationExaminee: projectApplicationExamineeReducer,
  form
}

export const sagas = [projectApplicationExamineeReducerSagas]
export {formSagaConfig}
