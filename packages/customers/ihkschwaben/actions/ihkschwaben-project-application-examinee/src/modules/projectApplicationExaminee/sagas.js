import {actions as formActions, isValid as isValidSelector} from 'redux-form'
import {all, call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {display, form, externalEvents, rest, selection as selectionUtil} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ProjectApplicationForm'
import {updateProjectApplication} from '../../util/api/entities'

import * as actions from './actions'

export const projectApplicationExamineeSelector = state => state.projectApplicationExaminee

export const inputSelector = state => state.input

export const entityName = 'Project_application'

export const formSagaConfig = {
  formId: REDUX_FORM_NAME,
  stateSelector: projectApplicationExamineeSelector
}

export default function* sagas() {
  yield all([
    takeLatest(actions.LOAD_VIEW, loadView),
    takeLatest(actions.UNLOAD_VIEW, unloadView),
    takeLatest(actions.TOUCH_ALL_FIELDS, form.sagasUtils.touchAllFields, formSagaConfig),
    takeEvery(actions.SUBMIT_FORM, submitForm),
    takeEvery(actions.SUBMIT_FORM_SET_STATUS, submitFormSetStatus),
    takeEvery(actions.FIRE_TOUCHED, fireTouched)
  ])
}

export function* loadView() {
  const {actionProperties} = yield select(inputSelector)

  const formName =
    actionProperties && actionProperties.projectApplicationExamineeForm
      ? actionProperties.projectApplicationExamineeForm
      : 'Project_application_examinee'
  yield put(actions.setFormName(formName))

  yield call(loadFormDefinition, formName, 'update')
  yield call(loadData)
}

export function* loadFormDefinition(formName, mode) {
  const formDefinition = yield call(rest.fetchForm, formName, mode)
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)

  yield put(actions.setFormDefinition(formDefinition))
  yield put(actions.setFieldDefinitions(fieldDefinitions))

  return {formDefinition, fieldDefinitions}
}

export function* loadData() {
  const {fieldDefinitions, formName, mode} = yield select(projectApplicationExamineeSelector)

  const paths = yield call(form.getUsedPaths, fieldDefinitions)
  const entity = yield call(loadEntity, paths)
  const flattenEntity = yield call(api.getFlattenEntity, entity)

  yield call(display.enhanceEntityWithDisplayExpressions, flattenEntity, formName, fieldDefinitions, mode)
  yield call(display.enhanceEntityWithDisplays, flattenEntity, fieldDefinitions)

  const formValues = yield call(form.entityToFormValues, flattenEntity, fieldDefinitions)

  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* loadEntity(paths) {
  const {selection} = yield select(inputSelector)

  const key = selectionUtil.getSingleKey(selection, entityName)
  const entity = yield call(rest.fetchEntity, entityName, key, {paths})

  yield put(actions.setEntity(entity))

  return entity
}

export function* unloadView() {
  yield put(actions.setEntity(null))
  yield put(formActions.destroy(REDUX_FORM_NAME))
}

export function* submitForm() {
  yield call(submitData, false)
}

export function* submitFormSetStatus() {
  yield call(submitData, true)
}

export function* submitData(setStatus) {
  try {
    const isValid = yield select(isValidSelector(REDUX_FORM_NAME))
    if (!isValid) {
      yield call(form.sagasUtils.handleInvalidForm, formSagaConfig)
    } else {
      yield put(formActions.startSubmit(REDUX_FORM_NAME))
      const {fieldDefinitions, formName} = yield select(projectApplicationExamineeSelector)
      yield call(submitValidate)
      const entity = yield call(form.sagasUtils.getEntityForSubmit, formSagaConfig)
      yield call(executeSubmit, entity, fieldDefinitions, setStatus, formName)
    }
  } catch (error) {
    yield call(form.sagasUtils.handleSubmitError, formSagaConfig, error)
  }
}

export function* submitValidate() {
  const {formValues, initialValues, mode, fieldDefinitions, formDefinition} = yield call(
    form.sagasUtils.getCurrentEntityState,
    formSagaConfig
  )
  yield call(form.submitValidation, formValues, initialValues, fieldDefinitions, formDefinition, mode)
}

export function* executeSubmit(entity, fieldDefinitions, setStatus, formName) {
  yield call(updateProjectApplication, entity, fieldDefinitions, setStatus, formName)
  yield put(externalEvents.fireExternalEvent('onCancel'))
  yield put(formActions.stopSubmit(REDUX_FORM_NAME))
}

export function* fireTouched({payload}) {
  const {touched: actionTouched} = payload
  const {touched: stateTouched} = yield select(projectApplicationExamineeSelector)

  if (actionTouched !== stateTouched) {
    yield put(actions.setTouched(actionTouched))
  }
}
