import {actions as formActions, isValid as isValidSelector} from 'redux-form'
import {all, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {form, rest, externalEvents} from 'tocco-app-extensions'
import {env} from 'tocco-util'

import {updateProjectApplication} from '../../util/api/entities'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

const FORM_ID = 'project-application-examinee-action'

describe('ihkschwaben-project-application-examinee', () => {
  describe('modules', () => {
    describe('projectApplicationExaminee', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_VIEW, sagas.loadView),
                takeLatest(actions.UNLOAD_VIEW, sagas.unloadView),
                takeLatest(actions.TOUCH_ALL_FIELDS, form.sagasUtils.touchAllFields, sagas.formSagaConfig),
                takeEvery(actions.SUBMIT_FORM, sagas.submitForm),
                takeEvery(actions.SUBMIT_FORM_SET_STATUS, sagas.submitFormSetStatus),
                takeEvery(actions.FIRE_TOUCHED, sagas.fireTouched)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadView saga', () => {
          test('should fetch entity and set it in store', () => {
            const expectedDataForm = 'Project_application_examinee_submission'

            return expectSaga(sagas.loadView)
              .provide([
                [
                  select(sagas.inputSelector),
                  {
                    selection: [25],
                    actionProperties: {
                      projectApplicationExamineeForm: 'Project_application_examinee_submission'
                    }
                  }
                ],
                [matchers.call.fn(sagas.loadFormDefinition)],
                [matchers.call.fn(sagas.loadData)]
              ])
              .call(sagas.loadFormDefinition, expectedDataForm, 'update')
              .call(sagas.loadData)
              .run()
          })
        })

        describe('submitData saga', () => {
          const entity = {paths: {}}
          test('should call submitData', () => {
            const fieldDefinitions = []
            const setStatus = false
            const formName = 'Project_application_examinee_submission'

            return expectSaga(sagas.submitData, setStatus)
              .provide([
                [select(isValidSelector(FORM_ID)), true],
                [select(sagas.projectApplicationExamineeSelector), {mode: 'update', fieldDefinitions, formName}],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getEntityForSubmit), entity]
              ])
              .call(sagas.executeSubmit, entity, fieldDefinitions, setStatus, formName)
              .run()
          })

          test('should handle thrown errors', () => {
            const fieldDefinitions = []
            const setStatus = false
            const formName = 'Project_application_examinee_submission'
            const error = new Error('error')
            return expectSaga(sagas.submitData, setStatus)
              .provide([
                [select(isValidSelector(FORM_ID)), true],
                [select(sagas.projectApplicationExamineeSelector), {mode: 'update', fieldDefinitions, formName}],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getEntityForSubmit), entity],
                [matchers.call.fn(sagas.executeSubmit), throwError(error)]
              ])
              .call(form.sagasUtils.handleSubmitError, sagas.formSagaConfig, error)
              .run()
          })

          test('should call handleInvalidForm on invalid form', () =>
            expectSaga(sagas.submitData)
              .provide({
                select() {
                  return false
                }
              })
              .call(form.sagasUtils.handleInvalidForm, sagas.formSagaConfig)
              .not.call(sagas.executeSubmit, entity)
              .run())
        })

        describe('updateFormSubmit saga', () => {
          const entity = {key: '25', model: 'Project_application', paths: {lastname: 'test'}}
          const setStatus = false
          const formName = 'Project_application_examinee_submission'
          env.setWidgetConfigKey('1')
          test('should call updateProjectApplication, load data, show notification', () => {
            const updateResponse = {status: 201}
            return expectSaga(sagas.executeSubmit, entity, setStatus, formName)
              .provide([[matchers.call.fn(updateProjectApplication), updateResponse]])
              .call.like({fn: updateProjectApplication})
              .put(externalEvents.fireExternalEvent('onCancel'))
              .put.like({action: {type: formActions.stopSubmit().type}})
              .run()
          })
        })

        describe('loadFormDefinition saga', () => {
          test('should load form and field definitions', () => {
            const formName = 'Project_application_examinee_submission'
            const mode = 'update'
            const formDefinition = {children: []}
            const fieldDefinitions = [{id: 'field_1'}, {id: 'field_2'}]

            return expectSaga(sagas.loadFormDefinition, formName, mode)
              .provide([
                [matchers.call(rest.fetchForm, formName, mode), formDefinition],
                [matchers.call(form.getFieldDefinitions, formDefinition), fieldDefinitions]
              ])
              .put(actions.setFormDefinition(formDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .run()
          })
        })

        describe('submitValidate saga', () => {
          test('should call submitValidation', () => {
            const formValues = {firstname: 'test'}
            const initialValues = {firstname: 'test1'}
            const mode = 'update'
            const fieldDefinitions = []
            const formDefinition = {}

            return expectSaga(sagas.submitValidate)
              .provide([
                [
                  matchers.call.fn(form.sagasUtils.getCurrentEntityState),
                  {formValues, initialValues, mode, fieldDefinitions, formDefinition}
                ],
                [matchers.call.fn(form.submitValidation)]
              ])
              .call(form.submitValidation, formValues, initialValues, fieldDefinitions, formDefinition, mode)
              .run()
          })
        })

        describe('loadData saga', () => {
          test('should fetch the entity and call form initialize', () => {
            return expectSaga(sagas.loadData)
              .provide([
                [select(sagas.projectApplicationExamineeSelector), {fieldDefinitions: []}],
                [matchers.call.fn(sagas.loadEntity), {paths: {}}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplays), {}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplayExpressions), {}]
              ])
              .call.like({fn: sagas.loadEntity})
              .call.like({fn: form.entityToFormValues})
              .put.like({action: {type: formActions.initialize().type}})
              .run()
          })
        })
      })
    })
  })
})
