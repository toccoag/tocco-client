import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  formName: '',
  formDefinition: {},
  entity: {},
  touched: false,
  fieldDefinitions: [],
  mode: 'update'
}

describe('ihkschwaben-project-application-examinee', () => {
  describe('modules', () => {
    describe('projectApplicationExaminee', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        test('should handle an action', () => {
          const stateBefore = {
            entity: ''
          }

          const expectedStateAfter = {
            entity: 'Project_application'
          }

          expect(reducer(stateBefore, actions.setEntity('Project_application'))).to.deep.equal(expectedStateAfter)
        })
      })
    })
  })
})
