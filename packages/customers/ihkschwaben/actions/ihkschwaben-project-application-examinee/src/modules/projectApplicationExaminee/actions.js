export const SET_FORM_DEFINITION = 'projectApplicationExaminee/SET_FORM_DEFINITION'
export const LOAD_VIEW = 'projectApplicationExaminee/LOAD_VIEW'
export const SET_ENTITY = 'projectApplicationExaminee/SET_ENTITY'
export const SUBMIT_FORM = 'projectApplicationExaminee/SUBMIT_FORM'
export const SUBMIT_FORM_SET_STATUS = 'projectApplicationExaminee/SUBMIT_FORM_SET_STATUS'
export const UNLOAD_VIEW = 'projectApplicationExaminee/UNLOAD_VIEW'
export const FIRE_TOUCHED = 'projectApplicationExaminee/FIRE_TOUCHED'
export const SET_TOUCHED = 'projectApplicationExaminee/SET_TOUCHED'
export const SET_FORM_NAME = 'projectApplicationExaminee/SET_FORM_NAME'
export const TOUCH_ALL_FIELDS = 'projectApplicationExaminee/TOCH_ALL_FIELDS'
export const SET_FIELD_DEFINITIONS = 'projectApplicationExaminee/SET_FIELD_DEFINITIONS'

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {
    formDefinition
  }
})

export const loadView = () => ({
  type: LOAD_VIEW
})

export const setEntity = entity => ({
  type: SET_ENTITY,
  payload: {
    entity
  }
})

export const submitForm = () => ({
  type: SUBMIT_FORM
})

export const submitFormSetStatus = () => ({
  type: SUBMIT_FORM_SET_STATUS
})

export const unloadView = () => ({
  type: UNLOAD_VIEW
})

export const fireTouched = touched => ({
  type: FIRE_TOUCHED,
  payload: {
    touched
  }
})

export const setTouched = touched => ({
  type: SET_TOUCHED,
  payload: {
    touched
  }
})

export const setFormName = formName => ({
  type: SET_FORM_NAME,
  payload: {
    formName
  }
})

export const touchAllFields = () => ({
  type: TOUCH_ALL_FIELDS
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {
    fieldDefinitions
  }
})
