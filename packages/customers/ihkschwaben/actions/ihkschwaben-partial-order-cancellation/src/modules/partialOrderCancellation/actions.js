export const SUBMIT = 'PartialOrderCancellation/SUBMIT'
export const PRECHECK_ACTION = 'PartialOrderCancellation/PRECHECK_ACTION'

export const precheckAction = key => ({
  type: PRECHECK_ACTION,
  payload: {
    key
  }
})

export const submit = (comment, selectedPositions) => ({
  type: SUBMIT,
  payload: {
    comment,
    selectedPositions
  }
})
