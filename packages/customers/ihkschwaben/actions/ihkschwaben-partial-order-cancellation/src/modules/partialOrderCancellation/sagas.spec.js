import {select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {rest, externalEvents} from 'tocco-app-extensions'

import * as sagas from './sagas'

describe('customers', () => {
  describe('ihkschwaben', () => {
    describe('actions', () => {
      describe('ihkschwaben-partial-order-cancellation', () => {
        describe('modules', () => {
          describe('partialOrderCancellation', () => {
            describe('sagas', () => {
              describe('precheckAction', () => {
                const key = '157'
                test('should check the order-status with status booked', () => {
                  const response = {
                    _links: null,
                    key: '157',
                    model: 'Order',
                    version: 3,
                    paths: {
                      relOrder_debitor_status: {
                        type: 'entity',
                        writable: false,
                        value: {
                          _links: null,
                          key: '3',
                          model: 'Order_debitor_status',
                          version: 1,
                          paths: {
                            unique_id: {
                              type: 'identifier',
                              writable: true,
                              value: 'booked'
                            }
                          }
                        }
                      }
                    }
                  }
                  return expectSaga(sagas.precheckAction, {payload: key})
                    .provide([[matchers.call.fn(rest.fetchEntity), response]])
                    .not.put.like(
                      externalEvents.fireExternalEvent('onError', {
                        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.already.canceled.body'
                      })
                    )
                    .run()
                })

                test('should check the order-status with status cancelled', () => {
                  const response = {
                    _links: null,
                    key: '157',
                    model: 'Order',
                    version: 3,
                    paths: {
                      relOrder_debitor_status: {
                        type: 'entity',
                        writable: false,
                        value: {
                          _links: null,
                          key: '3',
                          model: 'Order_debitor_status',
                          version: 1,
                          paths: {
                            unique_id: {
                              type: 'identifier',
                              writable: true,
                              value: 'cancelled'
                            }
                          }
                        }
                      }
                    }
                  }

                  return expectSaga(sagas.precheckAction, {payload: key})
                    .provide([[matchers.call.fn(rest.fetchEntity), response]])
                    .put(
                      externalEvents.fireExternalEvent('onError', {
                        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.already.canceled.body'
                      })
                    )
                    .run()
                })

                test('should handle error', () => {
                  return expectSaga(sagas.precheckAction, {payload: key})
                    .provide([[matchers.call.fn(rest.fetchEntity), throwError(new Error('Failed to fetch data'))]])
                    .put(
                      externalEvents.fireExternalEvent('onError', {
                        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.body'
                      })
                    )
                    .run()
                })
              })
              describe('submitAction', () => {
                const input = {
                  selection: {
                    type: 'ID',
                    entityName: 'Order',
                    ids: ['1']
                  }
                }
                const resource = '/ihkschwaben/actions/partialOrderCancellation/submit/1'
                const comment = 'comment'
                const selectedPositions = ['1']
                const options = {
                  method: 'POST',
                  body: {
                    comment,
                    selectedPositions
                  }
                }

                test('successful submit', () => {
                  const response = {
                    success: true,
                    message: 'success-text-message'
                  }
                  return expectSaga(sagas.submit, {payload: {comment, selectedPositions}})
                    .provide([
                      [select(sagas.inputSelector), input],
                      [matchers.call(rest.requestSaga, resource, options), {body: response}]
                    ])
                    .put(
                      externalEvents.fireExternalEvent('onSuccess', {
                        message: 'success-text-message'
                      })
                    )
                    .run()
                })

                test('error during connecting', () => {
                  const response = {
                    success: false,
                    message: 'error-text-resource'
                  }
                  return expectSaga(sagas.submit, {payload: {comment, selectedPositions}})
                    .provide([
                      [select(sagas.inputSelector), input],
                      [matchers.call(rest.requestSaga, resource, options), {body: response}]
                    ])
                    .put(
                      externalEvents.fireExternalEvent('onError', {
                        message: 'error-text-resource'
                      })
                    )
                    .run()
                })

                test('error submit', () => {
                  return expectSaga(sagas.submit, {payload: {comment, selectedPositions}})
                    .provide([[select(sagas.inputSelector), throwError(new Error('Failed to fetch data'))]])
                    .put(
                      externalEvents.fireExternalEvent('onError', {
                        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.body'
                      })
                    )
                    .run()
                })
              })
            })
          })
        })
      })
    })
  })
})
