import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {externalEvents, rest, selection as selectionUtil} from 'tocco-app-extensions'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeLatest(actions.SUBMIT, submit), takeLatest(actions.PRECHECK_ACTION, precheckAction)])
}

export function* submit({payload: {comment, selectedPositions}}) {
  try {
    const {selection} = yield select(inputSelector)
    const key = selectionUtil.getSingleKey(selection, 'Order')
    const response = yield call(rest.requestSaga, `/ihkschwaben/actions/partialOrderCancellation/submit/${key}`, {
      method: 'POST',
      body: {
        comment,
        selectedPositions
      }
    })

    const type = response.body.success ? 'onSuccess' : 'onError'
    yield put(
      externalEvents.fireExternalEvent(type, {
        message: response.body.message
      })
    )
  } catch (e) {
    yield put(
      externalEvents.fireExternalEvent('onError', {
        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.body'
      })
    )
  }
}

export function* precheckAction({payload: {key}}) {
  try {
    const response = yield call(rest.fetchEntity, 'Order', key, {paths: ['relOrder_debitor_status.unique_id']})
    if (response) {
      const resultData = response ? response.paths.relOrder_debitor_status.value.paths.unique_id.value : null
      if (resultData !== 'booked') {
        yield put(
          externalEvents.fireExternalEvent('onError', {
            message: 'client.actions.ihkschwaben-partial-order-cancellation.error.already.canceled.body'
          })
        )
      }
    }
  } catch (e) {
    yield put(
      externalEvents.fireExternalEvent('onError', {
        message: 'client.actions.ihkschwaben-partial-order-cancellation.error.body'
      })
    )
  }
}
