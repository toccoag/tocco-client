import PropTypes from 'prop-types'
import {useState, useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import {selection as selectionUtil} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {Button, Panel, Typography} from 'tocco-ui'

import {createSimpleFormDefinition} from './formDefinition'
import {StyledButtonWrapper} from './StyledComponents'

const PartialOrderCancellation = ({selection, submit, precheckAction, intl}) => {
  const msg = id => intl.formatMessage({id})
  const key = selectionUtil.getSingleKey(selection, 'Order')

  const [comment, setComment] = useState('')
  const [selectedPositions, setSelectedPositions] = useState([])

  useEffect(() => {
    precheckAction(key)
  }, [precheckAction, key])

  const commentLength = comment?.length || 0

  return (
    <>
      <Panel.Wrapper>
        <Panel.Header>
          <Typography.H4>
            <FormattedMessage id="client.ihkschwaben-partial-order-cancellation.header.title" />
          </Typography.H4>
        </Panel.Header>
        <Panel.Body>
          <SimpleFormApp
            form={createSimpleFormDefinition(intl)}
            labelPosition="inside"
            mode="create"
            onChange={({values}) => setComment(values?.comment || '')}
            noButtons
          />
          <EntityListApp
            entityName="Order_position"
            formName="PartialOrderCancellationOrderPosition"
            limit={15}
            scrollBehaviour="none"
            searchFormPosition="top"
            searchFormType="none"
            tql={`relOrder.pk == ${key} and cancelled == false`}
            onSelectChange={setSelectedPositions}
          />
        </Panel.Body>
      </Panel.Wrapper>
      <StyledButtonWrapper>
        <Button
          label={msg('client.ihkschwaben-partial-order-cancellation.submit.label')}
          ink="primary"
          look="raised"
          onClick={() => {
            submit(comment, selectedPositions)
          }}
          disabled={commentLength === 0 || selectedPositions.length <= 0}
        />
      </StyledButtonWrapper>
    </>
  )
}

PartialOrderCancellation.propTypes = {
  submit: PropTypes.func.isRequired,
  precheckAction: PropTypes.func.isRequired,
  intl: PropTypes.any,
  selection: selectionUtil.propType
}

export default PartialOrderCancellation
