import styled from 'styled-components'
import {scale} from 'tocco-ui'

export const StyledButtonWrapper = styled.div`
  margin-left: ${scale.space(0.2)};
  margin-top: ${scale.space(0.6)};
  display: flex;
  justify-content: flex-end;
`
