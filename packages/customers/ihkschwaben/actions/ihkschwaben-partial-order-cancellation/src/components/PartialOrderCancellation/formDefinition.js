export const createSimpleFormDefinition = intl => {
  const msg = id => intl.formatMessage({id})

  const formDefinition = {
    componentType: 'form',
    children: [
      {
        id: 'box',
        componentType: 'layout',
        layoutType: 'horizontal-box',
        children: [
          {
            id: 'comment',
            label: msg('client.ihkschwaben-partial-order-cancellation.comment.label'),
            componentType: 'field-set',
            children: [
              {
                id: 'comment',
                label: null,
                componentType: 'field',
                path: 'comment',
                dataType: 'text',
                validation: {
                  length: {
                    toIncluding: 104857600
                  },
                  mandatory: true
                },
                defaultValue: null,
                autoCompleteEndpoint: null,
                dmsEntityModel: null,
                simpleSearch: false
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'yes',
            scopes: [],
            ignoreCopy: false
          }
        ]
      }
    ]
  }

  return formDefinition
}
