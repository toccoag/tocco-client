import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {submit, precheckAction} from '../../modules/partialOrderCancellation/actions'

import PartialOrderCancellation from './PartialOrderCancellation'

const mapActionCreators = {
  submit,
  precheckAction
}

const mapStateToProps = state => ({
  selection: state.input.selection
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(PartialOrderCancellation))
