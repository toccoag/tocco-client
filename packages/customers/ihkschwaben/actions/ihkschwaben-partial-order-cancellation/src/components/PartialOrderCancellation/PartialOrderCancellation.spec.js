/* eslint-disable react/prop-types */
import {screen, fireEvent, waitFor} from '@testing-library/react'
import {testingLibrary, IntlStub, TestThemeProvider} from 'tocco-test-util'

import PartialOrderCancellation from './PartialOrderCancellation'

const comment = {
  values: {
    comment: 'abc'
  }
}

jest.mock('tocco-simple-form/src/main', () => props => (
  <div data-testid="simple-form">
    <button data-testid="changeValue" onClick={() => props.onChange(comment)}></button>
  </div>
))

jest.mock('tocco-entity-list/src/main', () => props => (
  <div data-testid="entity-list">
    <button data-testid="setSelection" onClick={() => props.onSelectChange(['1'])}></button>
  </div>
))

describe('ihkschwaben', () => {
  describe('actions', () => {
    describe('ihkschwaben-partial-order-cancellation', () => {
      describe('components', () => {
        describe('PartialOrderCancellation', () => {})

        const selection = {
          entityName: 'Order',
          type: 'ID',
          ids: ['126']
        }

        test('should show all elements', async () => {
          const precheckAction = sinon.spy()
          const submit = sinon.spy()

          testingLibrary.renderWithIntl(
            <TestThemeProvider>
              <PartialOrderCancellation
                selection={selection}
                submit={submit}
                precheckAction={precheckAction}
                intl={IntlStub}
              />
            </TestThemeProvider>
          )
          await screen.findAllByTestId('icon')

          expect(screen.getByTestId('simple-form')).to.exist

          expect(screen.getByText('client.ihkschwaben-partial-order-cancellation.header.title')).exist
          expect(screen.getByRole('button', {name: 'client.ihkschwaben-partial-order-cancellation.submit.label'})).to
            .exist
        })

        test('should be able to trigger the submit function after comment and selection', async () => {
          const precheckAction = sinon.spy()
          const submit = sinon.spy()

          testingLibrary.renderWithIntl(
            <TestThemeProvider>
              <PartialOrderCancellation
                selection={selection}
                submit={submit}
                precheckAction={precheckAction}
                intl={IntlStub}
              />
            </TestThemeProvider>
          )

          fireEvent.click(
            screen.getByRole('button', {name: 'client.ihkschwaben-partial-order-cancellation.submit.label'})
          )
          expect(submit).not.to.have.been.calledOnce

          fireEvent.click(screen.getByTestId('changeValue'))
          fireEvent.click(screen.getByTestId('setSelection'))

          await waitFor(() => {
            jestExpect(
              screen.getByRole('button', {name: 'client.ihkschwaben-partial-order-cancellation.submit.label'})
            ).not.toBeDisabled()
          })

          fireEvent.click(
            screen.getByRole('button', {name: 'client.ihkschwaben-partial-order-cancellation.submit.label'})
          )
          expect(submit).to.have.been.calledOnce
        })
        test('should trigger the precheckAction', () => {
          const precheckAction = sinon.spy()
          const submit = sinon.spy()

          testingLibrary.renderWithIntl(
            <TestThemeProvider>
              <PartialOrderCancellation
                selection={selection}
                submit={submit}
                precheckAction={precheckAction}
                intl={IntlStub}
              />
            </TestThemeProvider>
          )
          expect(precheckAction).to.have.been.calledOnce
          expect(screen.getByTestId('simple-form')).to.exist
        })
      })
    })
  })
})
