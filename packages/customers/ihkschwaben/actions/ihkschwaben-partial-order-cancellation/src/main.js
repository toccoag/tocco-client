import {appFactory, selection, externalEvents, notification, actionEmitter} from 'tocco-app-extensions'
import {reducer as reducerUtil} from 'tocco-util'

import PartialOrderCancellation from './components/PartialOrderCancellation'
import reducers, {sagas} from './modules/reducers'

const packageName = 'ihkschwaben-partial-order-cancellation'

const EXTERNAL_EVENTS = ['emitAction', 'onSuccess', 'onCancel', 'onError']

const initApp = (id, input, events, publicPath) => {
  const content = <PartialOrderCancellation />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  actionEmitter.addToStore(store, state => state.input.emitAction)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const IhkschwabenPartialOrderCancellationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

IhkschwabenPartialOrderCancellationApp.propTypes = {
  /**
   * Selection of a single `Order` (only type `ID` is supported and exacte one key is required)
   */
  selection: selection.propType.isRequired
}

export default IhkschwabenPartialOrderCancellationApp
export const app = appFactory.createBundleableApp(packageName, initApp, IhkschwabenPartialOrderCancellationApp)
