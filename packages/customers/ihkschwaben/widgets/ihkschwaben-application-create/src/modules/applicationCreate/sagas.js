import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, intl, remoteLogger, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ApplicationCreate'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'

export const inputSelector = state => state.input
export const applicationCreateSelector = state => state.applicationCreate

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_FORM_DEFINITION, loadFormDefinition), takeLatest(actions.SUBMIT_FORM, submitForm)])
}

export function* loadFormDefinition() {
  const {formBase} = yield select(inputSelector)
  // add createEndpoint for async validation
  const formDefinition = {
    ...(yield call(rest.fetchForm, formBase, 'create')),
    createEndpoint: 'ihkschwaben/widgets/applicationCreate/submit'
  }
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)

  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const entityValues = yield call(api.getFlattenEntity, {model: 'Application'})
  const formValues = yield call(form.entityToFormValues, {...entityValues, ...defaultValues}, fieldDefinitions)
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))

  yield all([put(actions.setFormDefinition(formDefinition)), put(actions.setFieldDefinitions(fieldDefinitions))])
}

export function* submitForm() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {fieldDefinitions} = yield select(applicationCreateSelector)
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const applicationEntity = yield call(api.toEntity, flatEntity)

  const locale = yield select(intl.localeSelector)

  const response = yield call(rest.requestSaga, 'ihkschwaben/widgets/applicationCreate/submit', {
    method: 'POST',
    queryParams: {
      locale
    },
    body: applicationEntity,
    acceptedErrorCodes: ['VALIDATION_FAILED']
  })

  if (response.status === 201) {
    yield put(actions.hideForm())
    yield put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    if (response.body.errorCode === 'VALIDATION_FAILED') {
      yield put(
        notification.toaster({
          type: 'error',
          title: 'client.component.actions.validationError',
          body: validation.getErrorCompact(response.body.errors)
        })
      )
    } else {
      remoteLogger.logError('unexpected error during submit', response)
    }
  }
}
