import {actions as formActions} from 'redux-form'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, env, intl, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/ApplicationCreate'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('ihkschwaben-application-create', () => {
  describe('modules', () => {
    describe('applicationCreate', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_FORM_DEFINITION, sagas.loadFormDefinition),
                takeLatest(actions.SUBMIT_FORM, sagas.submitForm)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadFormDefinition', () => {
          test('should load form and field definitions and initialize form', () => {
            const formDefinition = {children: []}
            const expectedFormDefinition = {
              createEndpoint: 'ihkschwaben/widgets/applicationCreate/submit',
              children: []
            }
            const fieldDefinitions = [{id: 'field_1'}, {id: 'field_2'}]
            const defaultValues = {field_1: 'value 1'}
            const entityValues = {field_2: 'value 2'}
            const formValues = {
              __key: undefined,
              __model: 'Application',
              __version: undefined,
              field_1: 'value 1'
            }
            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [select(sagas.inputSelector), {formBase: 'Examiner_application'}],
                [matchers.call(rest.fetchForm, 'Examiner_application', 'create'), formDefinition],
                [matchers.call(form.getFieldDefinitions, expectedFormDefinition), fieldDefinitions],
                [matchers.call(form.getDefaultValues, fieldDefinitions), defaultValues],
                [matchers.call(api.getFlattenEntity, {model: 'Application'}), entityValues],
                [
                  matchers.call(form.entityToFormValues, {...entityValues, ...defaultValues}, fieldDefinitions),
                  formValues
                ]
              ])
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .put(actions.setFormDefinition(expectedFormDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .run()
          })
        })

        describe('submitForm', () => {
          const fieldDefinitions = [{id: 'field'}]
          const flatEntity = {field: 'value'}
          const application = {
            pk: null,
            field: 'value'
          }
          const widgetConfigKey = '456'
          env.setWidgetConfigKey(widgetConfigKey)

          test('should submit', () => {
            const response = {status: 201}
            return expectSaga(sagas.submitForm)
              .provide([
                [select(sagas.applicationCreateSelector), {fieldDefinitions}],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'ihkschwaben/widgets/applicationCreate/submit', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(actions.hideForm())
              .put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
              .run()
          })

          test('should handle validation', () => {
            const response = {status: 400, body: {errorCode: 'VALIDATION_FAILED', errors: []}}
            return expectSaga(sagas.submitForm)
              .provide([
                [select(sagas.applicationCreateSelector), {fieldDefinitions}],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'ihkschwaben/widgets/applicationCreate/submit', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: validation.getErrorCompact(response.body.errors)
                })
              )
              .run()
          })
        })
      })
    })
  })
})
