export const LOAD_FORM_DEFINITION = 'applicationCreate/LOAD_FORM_DEFINITION'
export const SET_FORM_DEFINITION = 'applicationCreate/SET_FORM_DEFINITION'
export const SET_FIELD_DEFINITIONS = 'applicationCreate/SET_FIELD_DEFINITIONS'
export const SUBMIT_FORM = 'applicationCreate/SUBMIT_FORM'
export const SET_HIDE_FORM = 'applicationCreate/SET_HIDE_FORM'

export const loadFormDefinition = () => ({
  type: LOAD_FORM_DEFINITION
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const submitForm = () => ({
  type: SUBMIT_FORM
})

export const hideForm = () => ({
  type: SET_HIDE_FORM,
  payload: {
    hideForm: true
  }
})
