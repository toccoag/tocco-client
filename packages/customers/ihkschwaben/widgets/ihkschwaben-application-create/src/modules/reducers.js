import {reducer as form} from 'redux-form'

import applicationCreateReducer from './applicationCreate/reducer'
import applicationCreateSagas from './applicationCreate/sagas'

export default {
  applicationCreate: applicationCreateReducer,
  form
}

export const sagas = [applicationCreateSagas]
