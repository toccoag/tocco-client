import PropTypes from 'prop-types'
import {actions, appFactory, errorLogging, externalEvents, notification, formData} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {env, reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import ApplicationCreate from './components/ApplicationCreate'
import reducers, {sagas} from './modules/reducers'

const packageName = 'ihkschwaben-application-create'
const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = (
    <>
      <notification.Notifications />
      <ApplicationCreate />
    </>
  )

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  notification.addToStore(store, true)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  formData.addToStore(store, () => ({
    listApp: EntityListApp
  }))
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [notification.connectSocket()],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const ApplicationCreateApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

ApplicationCreateApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default ApplicationCreateApp
export const app = appFactory.createBundleableApp(packageName, initApp, ApplicationCreateApp)
