import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'
import {externalEvents} from 'tocco-app-extensions'

import {loadFormDefinition} from '../../modules/applicationCreate/actions'

import ApplicationCreate from './ApplicationCreate'
import {REDUX_FORM_NAME} from './Form'

const mapActionCreators = {
  loadFormDefinition,
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  hideForm: state.applicationCreate.hideForm,
  formDefinition: state.applicationCreate.formDefinition,
  fieldDefinitions: state.applicationCreate.fieldDefinitions,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ApplicationCreate))
