import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {form} from 'tocco-app-extensions'
import {LoadMask} from 'tocco-ui'

import {VisibilityStatus} from '../../visibilityStatus'

import Form from './FormContainer'

const ApplicationCreate = ({
  hideForm,
  formDefinition,
  fieldDefinitions,
  formInitialValues,
  loadFormDefinition,
  fireVisibilityStatusChangeEvent
}) => {
  useEffect(() => {
    fireVisibilityStatusChangeEvent([VisibilityStatus.createForm])
  }, [fireVisibilityStatusChangeEvent])
  useEffect(() => {
    loadFormDefinition()
  }, [loadFormDefinition])

  const handleAsyncValidate = form.hooks.useAsyncValidation({
    formInitialValues,
    fieldDefinitions,
    formDefinition,
    mode: 'create'
  })
  const handleSyncValidate = form.hooks.useSyncValidation({fieldDefinitions, formDefinition})

  return (
    !hideForm && (
      <LoadMask required={[formDefinition, fieldDefinitions]}>
        <Form
          validate={handleSyncValidate}
          asyncValidate={handleAsyncValidate}
          asyncBlurFields={fieldDefinitions ? form.getUsedPaths(fieldDefinitions).map(form.transformFieldName) : null}
        />
      </LoadMask>
    )
  )
}

ApplicationCreate.propTypes = {
  hideForm: PropTypes.bool,
  formDefinition: PropTypes.object,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object),
  formInitialValues: PropTypes.object,
  loadFormDefinition: PropTypes.func.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired
}

export default ApplicationCreate
