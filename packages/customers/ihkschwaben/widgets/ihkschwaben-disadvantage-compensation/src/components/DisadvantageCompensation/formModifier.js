import {form} from 'tocco-app-extensions'

const removeActionsFromForm = (formDefinition, allowCreate, intl) => {
  formDefinition = form.removeActions(formDefinition, ['delete'])
  if (allowCreate) {
    formDefinition = form.addCreate(formDefinition, intl)
  } else {
    formDefinition = form.removeActions(formDefinition, ['new', 'copy'])
  }
  return formDefinition
}

export const modifyFormDefinition = (formDefinition, allowCreate, intl) => {
  return removeActionsFromForm(formDefinition, allowCreate, intl)
}
