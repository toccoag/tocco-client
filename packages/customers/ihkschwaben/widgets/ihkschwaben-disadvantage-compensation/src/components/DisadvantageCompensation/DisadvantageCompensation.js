import PropTypes from 'prop-types'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {modifyFormDefinition} from './formModifier'

const DisadvantageCompensation = ({
  entityName,
  formBase,
  reportIds,
  allowCreate,
  searchFilters,
  searchFormType,
  limit,
  backendUrl,
  appContext,
  intl,
  onVisibilityStatusChange
}) => {
  return (
    <EntityBrowserApp
      entityName={entityName}
      formBase={formBase}
      searchFormType={searchFormType}
      searchFilters={searchFilters}
      limit={limit}
      modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, allowCreate, intl)}
      backendUrl={backendUrl}
      appContext={appContext}
      reportIds={reportIds}
      onVisibilityStatusChange={onVisibilityStatusChange}
    />
  )
}

DisadvantageCompensation.propTypes = {
  entityName: PropTypes.string,
  formBase: PropTypes.string,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  searchFormType: searchFormTypePropTypes,
  limit: PropTypes.number,
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  formDefinition: PropTypes.object,
  allowCreate: PropTypes.bool,
  onVisibilityStatusChange: PropTypes.func
}

export default DisadvantageCompensation
