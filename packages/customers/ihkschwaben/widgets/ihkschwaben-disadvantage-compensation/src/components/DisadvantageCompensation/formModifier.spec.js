import {IntlStub} from 'tocco-test-util'

import {modifyFormDefinition} from './formModifier'

describe('customers', () => {
  describe('ihkschwaben', () => {
    describe('Widgets', () => {
      describe('ihkschwaben-disadvantage-compensation', () => {
        describe('components', () => {
          describe('DisadvantageCompensation', () => {
            describe('formModifier', () => {
              describe('modifyFormDefinition', () => {
                const formDefinitionDetailView = {
                  id: 'Disadvantage_compensation_view_detail',
                  componentType: 'form',
                  children: [
                    {
                      id: 'main-action-bar',
                      componentType: 'action-bar',
                      children: [
                        {
                          id: 'createcopy',
                          componentType: 'action-group',
                          children: [
                            {
                              id: 'new',
                              componentType: 'action'
                            },
                            {
                              id: 'copy',
                              componentType: 'action'
                            }
                          ],
                          actions: [],
                          defaultAction: {
                            id: 'new',
                            componentType: 'action'
                          }
                        },
                        {
                          id: 'delete',
                          componentType: 'action'
                        },
                        {
                          id: 'save',
                          componentType: 'action'
                        }
                      ]
                    }
                  ]
                }

                test('should remove new, create and delete action', () => {
                  const modifiedFormDefinition = modifyFormDefinition(formDefinitionDetailView, false, IntlStub)
                  expect(modifiedFormDefinition.children.length).to.eql(1)
                })

                test('should have save action only', () => {
                  const modifiedFormDefinition = modifyFormDefinition(formDefinitionDetailView, false, IntlStub)
                  expect(modifiedFormDefinition.children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children[0].id).to.eql('save')
                })

                test('should have new and save action', () => {
                  const modifiedFormDefinition = modifyFormDefinition(formDefinitionDetailView, true, IntlStub)
                  expect(modifiedFormDefinition.children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children.length).to.eql(2)
                  expect(modifiedFormDefinition.children[0].children[0].id).to.eql('createcopy')
                  expect(modifiedFormDefinition.children[0].children[1].id).to.eql('save')
                })
              })
            })
          })
        })
      })
    })
  })
})
