import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {env, appContext} from 'tocco-util'

import DisadvantageCompensation from './components/DisadvantageCompensation'

const packageName = 'ihkschwaben-disadvantage-compensation'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <DisadvantageCompensation />

  const store = appFactory.createStore({}, null, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      appFactory.renderApp(app.component)
    }
  }
})()

const DisadvantageCompensationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

DisadvantageCompensationApp.propTypes = {
  /**
   * Show create button if user has the permission
   */
  allowCreate: PropTypes.bool,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  searchFormType: searchFormTypePropTypes,
  limit: PropTypes.number,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  entityName: PropTypes.string,
  formBase: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default DisadvantageCompensationApp
export const app = appFactory.createBundleableApp(packageName, initApp, DisadvantageCompensationApp)
