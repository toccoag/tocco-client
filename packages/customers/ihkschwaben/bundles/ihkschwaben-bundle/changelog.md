3.13.25
- preserve splat path after creating entity

3.13.24
- added data-cy to mail-action button
- handle null minChars in formDefinition

3.13.23
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.22
- adjust min char search field validation
- clean up searchFormValiation

3.13.21
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.20
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.19
- fix stated value labels spacing
- fix color of label-neutral

3.13.18
- harmonize google maps icon positioning in LocationEdit

3.13.17
- add timezone header

3.13.16
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to history

3.13.15
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.14
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.13
- cleanup save button code

3.13.12
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.11
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.10
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.9
- cleanup marking code
- add native date(time)picker for touch devices
- remove custom _widget_key param

3.13.8
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.7
- add _widget_key query param to all requests

3.13.6
- pass escapeHtml to display formatter

3.13.5
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.4
- fix growing textarea
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.3
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper

3.13.2
- disable register button when required modules are forbidden

3.13.1
- fix sorting of answer options
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.32
- revert focus fix

3.12.31
- do not auto focus anything on touch devices

3.12.30
- improve UX for simple selects on touch devices

3.12.29
- fix double scrollbars in some modals
- fix height of exam edit

3.12.28
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.27
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.26
- add tooltip texts for buttons of upload component

3.12.25
- Make submit button hideable

3.12.24
- fix text select input edit and show available options
- fix table edit styling

3.12.23
- set checkbox field to touched on safari/ipad/iphone

3.12.22
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.21
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.20
- add copy function to entity-browser

3.12.19
- show correct link for subtables

3.12.18
- hide table refresh button when nothing happens

3.12.17
- able to navigate to create view on detail

3.12.16
- Make buttons in evaluation-execution-participation-action customizable

3.12.15
- remove selection controller in dms

3.12.14
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on toaster close buttons

3.12.13
- transform initial value paths

3.12.12
- add context to auto-complete
- use ListView helpers

3.12.11
- add sorting, pagination and column widths
- use SimpleTableFormApp
- show correct select field in export action
- show max age hint

3.12.10
- use error message when action fires error event

3.12.9
- support report actions in adjustAllActions

3.12.8
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- add simple table form app
- use core components
- remove unused / broken styling
- disable virtual table

3.12.7
- be able to accept prompt with enter

3.12.6
- extend font sizes with missing sizes

3.12.5
- trim searchInput value
- improve line numbers color in dark mode
- save column width in preferences

3.12.4
- add declareColoredLabel util
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix sorting of columns with missing preferences
- fix sorting of columns with missing preferences
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.36
- do not apply btn hover style on disable buttons

3.11.35
- handle query changes properly

3.11.34
- se correct color for promotion status
- add copy icon
- improve history action ui
- pass origin event for onChange
- improve history action data loading
- preselect row
- fix outputjob in synchron simple action
- fix navigatino to document edit page
- fix initial navigation to detail view or action
- improve diff viewer theme

3.11.33
- support search fields in useAutofocus

3.11.32
- add legacy font sizes to HtmlEditor
- fix selection handling
- cleanup transient props
- adjust toaster for action validation

3.11.31
- TOCDEV-9023

3.11.30
- improve selection in history table
- adjust diff view
- implement breadcrumb and adjust navigation
- add url helpers to diff view
- support history action
- clear selection if new table is loaded

3.11.29
- add mailbox icon

3.11.28
- navigate to parent folder after deletion

3.11.27
- use correct shouldForwardProp implementation

3.11.26
- export combineSelection
- add new property to theme
- add navigateToPath to navigationStrategy
- add icon
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18

3.11.25
- update react-router v6
- update react-router v6
- remove unused history

3.11.24
- use transient props for styled components
- align stated value boxes content vertically

3.11.23
- add new icons

3.11.22
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.21
- migrate to react router v6

3.11.20
- add truck-medical icon

3.11.19
- add id to formatted values
- replace keyHandler with navigateTable util
- replace keyHandler with navigateTable util

3.11.18
- add active condition for loading page limit options
- add active condition for loading page limit options

3.11.17
- add cy attributes
- add cy attributes
- add cy attributes
- add cy attributes

3.11.16
- standardize sticky button wrapper styling
- remove unused onSearchChange event
- remove unused searchListFormName feature
- remove unused queryChanged action
- use own formDefinition selector
- remove unused update store logic

3.11.15
- migrate routes to v5 compat package

3.11.14
- add widget application-create for customer ihkschwaben
- add widget application-create for customer ihkschwaben

3.11.13
- support post flush validation
- replace default props in tocco-ui with javascript default parameters
- replace default props in entity-detail with javascript default parameters

3.11.12
- ignore undefined optionDisplay

3.11.11
- implement validation for simple actions

3.11.10
- update to yarn 4

3.11.9
- handle rule provider in widgets
- handle optionDisplay in remote and select fields

3.11.8
- do not overwrite toasters from sync simple actions
- add react-router compatibility package

3.11.7
- fix tooltips freeze by replacing popper library with floating-ui library

3.11.6
- fix range mapping after upgrade to date-fns 3.x

3.11.5
- fix formatted value colors in dark mode labels

3.11.4
- allow auto focus to be disabled
- disable search field focusing in sub grids in widgets
- set isSubGrid property

3.11.3
- fix button group not being displayed correctly after styled components update

3.11.2
- fix react-select menu after upgrade to styled component

3.11.1
- merge ModalDisplay and BlockingDisplay into OverlayDisplay
- show error id for actions
- fix clear number field via auto complete

3.11.0
- initial release for version 3.11

3.10.33
- map postcode fields to string tql
- reset to default when deselecting template
- add option to reevalute action condition
- reevaluate action conditions if detail reloaded

3.10.32
- enhance toasters with large content by adding vertical scrollbar

3.10.31
- fix popover text and stated value text not wrapping

3.10.30
- render html in toaster body
- return horizontal drop position in useDnD
- allow dropping and display drop indicator on both sides in ColumnPicker

3.10.29
- add row numbering to ui table
- add row numbering

3.10.28
- rename property description to title
- show label and description in search filter tooltip
- move rest endpoint to client "route"
- fix side panel vertical scrolling in create view

3.10.27
- fix hostname for custom actions in cms
- add method to extract env as input props
- pass env to dynamic action

3.10.26
- fix datepicker styling in widgets by providing the tocco-app classname which prevents webkinder styles being applied
- let requestSaga handle blocking info in simpleAction

3.10.25
- always require captcha for anonymous
- disable auto-complete when selecting template
- map phone fields to string tql

3.10.24
- place date picker directly in body to prevent z-index problems in widgets
- identify exams by key instead of nr

3.10.23
- set max height for info boxes
- disable cache for create forms

3.10.22
- do not show same tooltip twice

3.10.21
- delay the display of popovers to prevent distracting flashing effect in large lists on hover
- style link inside label duplicate
- do not show same tooltip twice

3.10.20
- add payment-provider-action
- add queryParams to navigateToAction
- remove payment logic
- use breaks instead of separate paragraphs for readonly text
- add isDefined helper
- accept falsy values as settings for reports

3.10.19
- fix draggable file/image

3.10.18
- change link color back to secondary'
- harmonize icon list spacing
- style duplicate warning
- optimise last opened color for dark theme
- fix selected day color in date picker
- fix draggable in preview

3.10.17
- enhance breadcrumbs und popover colors for dark mode
- decrease spacing of menu tree items
- enhance dark dark theme colors

3.10.16
- fix save button coloring in dark mode

3.10.15
- fix clear of remote field
- improve dark theme color harmony
- enhance table aesthetic and readability

3.10.14
- fix disabled buttons disappearing in widgets
- fix sso login in old cms

3.10.13
- enable selectors by default
- remove obsolete bottom padding within advanced search
- use default display after selecting a value in remote fields
- handle float imprecision in duration calculation

3.10.12
- add navigationStrategy as input prop
- pass navigationStrategy to simple action form
- add report to output group instead of new group

3.10.11
- adjust isReadOnly for address and location field
- fix rounding issue for sticky columns
- allow not whitelisted style properties for svg element

3.10.10
- remove obsolete bottom padding in form
- extend widget theme with missing text colors
- add file circle plus icon

3.10.9
- style payment summary view

3.10.8
- add ihkschwaben-project-application-examinee custom action

3.10.7
- do not try to access captcha when hidden
- style fixed columns in table with border at the right edge
- add coloration for number of attempts

3.10.6
- only use captcha when updating password if the target is the current user or the current user is no usermanager
- add title to description element
- add acknowledge notification and entities message
- remove description title again
- handle custom title in action pre checks
- remove ignoreFormDefaultValues property

3.10.5
- disable CKEditor version check
- fix dark theme icon issues within buttons

3.10.4
- set min-height on lower div
- respect itemCount as limit for remote fields
- default disable remote create in widgets

3.10.3
- enable whole area of menu entries to be clicked
- fix dark theme color issues
- apply corresponding theme background color to login screen

3.10.2
- only use AutoSizer with new windowed table

3.10.1
- fix colors in signal list items for dark mode

3.10.0
- initial release for version 3.10

3.9.37
- add summary page

3.9.36
- style focused menu elements the same as on hover

3.9.35
- set referrer policy to `no-referrer-when-downgrade`
- add sticky column to ui table
- use other endpoint for delete button
- apply theme themeswitching to ace code editor as well

3.9.34
- fix blocking display disappearing in widgets on long pages

3.9.33
- fix dark mode color issues
- remove obsolete padding on display & immutable labels in admin

3.9.32
- check rights to remove delete button
- fix selection in remotefield
- fix selection in remotefield

3.9.31
- fix menu items wobble on hover in firefox browser
- fix dark theme color issues
- dynamic determine selectable in table

3.9.30
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- add listApp
- add listApp
- add listApp
- add listApp
- add listApp
- add externalEvents to appFactory
- use Debouncer in CodeEdit instead of custom debounce
- change last opened color to increase readability

3.9.29
- harmonize choice edit layout with labels
- initially collapse layout boxes when configured
- allow boxes in widgets to be initially collapsed and opened by user
- fix styles being overwritten in dark mode after a legacy action is executed
- add listApp
- add listApp
- add listApp
- add listApp
- add listApp
- add externalEvents to appFactory
- use Debouncer in CodeEdit instead of custom debounce

3.9.28
- use correct search form name as fallback
- enable hover on parent items of menu tree

3.9.27
- map shrinkToContent in getColumnDefinition
- fix jumping buttons in tocco header when opening help menu
- add feature

3.9.26
- improve menu ux by adding hover background and expanding clickable area
- add dark mode
- fix widget theme

3.9.25
- fix intermediate values in time edit

3.9.24
- add no button option for simple action form

3.9.23
- map email fields to string tql

3.9.22
- add payment methods and payment provider handling

3.9.21
- add non breaking space to ensure correct spacing in multiple fields separator

3.9.20
- fix column order resetting when moving multiple columns after another

3.9.19
- add drag'n'drop upload

3.9.18
- support custom report settings in widgets
- harmonize exam edit spacing

3.9.17
- enhance last openend color for better visibility

3.9.16
- limit watchForHover to activate in window.onload

3.9.15
- Add `watchForHover` styling utility

3.9.14
- shrink size of datepicker

3.9.13
- handle 403 error in delete

3.9.12
- add formBase property

3.9.11
- fix width of exam edit table ipunt fields in firefox
- add last opened to ui table
- add last opened to entity-list
- align stated value content verically with label

3.9.10
- use path for setting placeholder terms value
- fix multi-select-field

3.9.9
- hide readonly terms
- fix locale for anonymous
- fix locale for anonymous

3.9.8
- harmonize date picker styling and implement time picker
- fix several selection issues in the dms
- fix selection in docs tree search

3.9.7
- load row number options from Page_limit entities
- fix multivalue separator span alignment

3.9.6
- add updateDocumentTitle prop to Breadcrumbs
- add updateDocumentTitle prop to Breadcrumbs

3.9.5
- register gear icon

3.9.4
- split styling of blocking display for admin and widget
- fix multivalue label text overflow

3.9.3
- add readonly terms config and use pristine value change for placeholders
- handle immutable prop in TermsEdit
- check for writable paths in form instead of actions
- only set form builder readonly if mode is not create

3.9.2
- split toaster into widget and admin components
- adjust height of evaluation tree table to scale depending on available viewport height

3.9.1
- TOCDEV-7717, TOCDEV-7983

3.9.0
- initial release for version 3.9

3.8.22
- move logic from reports saga to backend
- Show error message if too many entities are selected for deletion

3.8.21
- align widget labels at flex start position
- add hideFooter property to entity-detail
- register list-tree icon

3.8.20
- harmonize status label border radii
- add border around exam edit datepicker inputs

3.8.19
- load row number options from Page_limit entities
- fix resizing of ui table

3.8.18
- hide attempts on hidden checkbox
- add download icon to DocumentFormatter
- allow multiple paths and disallow navigation for custom table on SubGrid
- disable navigation if parent defines it
- do not add search value for parent relation if no reverse relation was defined
- handle multiple relation paths by resolving all of them and handle custom relation paths by load the table form

3.8.17
- fix terms edit link color
- migrate terms element to the disadvantage compensation widget
- navigate to next field without selecting with tab

3.8.16
- only use visible search fields for building tql
- escape front-slash in fulltext tql query

3.8.15
- vertically center input edit table cells
- force underscore styling of terms edit links

3.8.14
- hide overflow of column headers
- register gear icon
- disable sideEffects optimization in webpack
- fix css styles not being passed down to multi value label span
- fix sticky buttons within modal cutting off content
- fix terms edit link styling and refactor component

3.8.13
- register calculator icon

3.8.12
- fix themes by adding missing status label radius
- extract nested ternary operation
- improve exam edit action styling
- fix vertical alignment of html formatter

3.8.11
- add data-cy attributes

3.8.10
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add BasicSearchForm
- add qualification dataRequest
- make select work without formData
- refactor input-edit to use more shared code
- use column position hook
- add useTitle to FormattedValue
- count hidden required checkbox as optional and ignore it in validation
- fix checking if form contains only readonly
- add additional units

3.8.9
- refactor panel group to remove duplications

3.8.8
- fix long lines in code edit being cut off by showing scrollbar
- add id to ReactDatePicker
- add keyboard navigation, editable value focus and postPointDigits
- decouple docs-browser from entity-list
- add default link target and protocol to html editor

3.8.7
- fix disabled button and menu item styles
- fix broken prop types
- do not use array index in keys
- fix legacy styles being applied to status labels

3.8.6
- change fields separator
- change fields separator
- fix test for new separator
- make editableValueFactory work without formField
- create exam-edit
- add exam-edit action
- add exam-edit action

3.8.5
- load email template for non-admins as well
- remove obsolete padding around styled entity browser
- fix disabled textarea on safari

3.8.4
- fix menu menu links not opening in new tab when icon is clicked

3.8.3
- ignore default search filter if tql is passed

3.8.2
- if a form contains only readonly field the save button should be removed

3.8.1
- use widget prefix for report location
- lighten status label colors and harmonize spacing

3.8.0
- initial release for version 3.8

0.1.22
- harmonize menu layout by moving technical name below label
- fix scrollbars on disabled textareas with content

0.1.21
- reset disabled min-height on textareas

0.1.20
- growing subtables
- load email template properly

0.1.19
- harmonize panel spacing and font weights
- make actionConditions work in the children of action groups
- handle follow up actions

0.1.18
- build up modifiers from smaller independent functions

0.1.17
- use new formSuffix to build formBase of SubGrids
- continue handling form elements after a custom component was encountered
- do not crash if no formValues exists when checking terms data
- pass formValues to formBuilder

0.1.16
- enable co-existence with legacy ckeditor
- improve readability of status labels
- adjust disabled textarea height to match vertical label position

0.1.15
- do not render terms if no conditions were configured
- check for empty string that element is only added if necessary

0.1.14
- fix getTextOfChildren
- replace deprecated selectUnit

0.1.13
- fix buggy scroll to top for modals
- remove delete action from all forms and add searchFormType
- handle phone validation for relations
- handle validation response properly
- fix broken prop types

0.1.12
- dependency update
- dependency update
- dependency update
- add data-cy attributes

0.1.11
- handle de-DE locale for datepicker
- align terms condition with checkbox properly

0.1.10
- increase left padding of merge table

0.1.9
- add min width to toaster box

0.1.8
- handle attempt and max attempts

0.1.7
- get all address suggestions within country
- render sidepanel initially with correct collapse state
- enable async validation for relations
- enable async validation for relations
- enable async validation for relations
- add terms component
- add terms component
- add pickAllProperties to getFlattenEntity
- use new terms

0.1.6
- handle sso with redirects
- handle auto login properly
- connect principal with sso login properly
- handle sso login properly

0.1.5
- export logged in action type
- handle showAutentication flag

0.1.4
- add reports after new button
- place label in widgets on left side
- move question code to tocco-util

0.1.3
- flag selection as deleted on duplicate detail
- navigate to list after fullscreen action deleted selection

0.1.2
- refactor partial order cancellation fix import typo

0.1.1
- accept limit as string
- add partial order cancellation custom action
- add partial order cancellation to the bundle

0.1.0
- initial release
