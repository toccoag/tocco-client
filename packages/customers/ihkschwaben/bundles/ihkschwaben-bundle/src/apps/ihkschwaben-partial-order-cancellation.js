import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-ihkschwaben-partial-order-cancellation/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
