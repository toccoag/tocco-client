import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-ihkschwaben-project-application-examinee/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
