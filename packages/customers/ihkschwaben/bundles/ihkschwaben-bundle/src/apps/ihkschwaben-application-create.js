import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-ihkschwaben-application-create/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
