3.13.23
- handle null minChars in formDefinition

3.13.22
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.21
- adjust min char search field validation
- clean up searchFormValiation

3.13.20
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.19
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.18
- fix stated value labels spacing
- fix color of label-neutral

3.13.17
- harmonize google maps icon positioning in LocationEdit

3.13.16
- add timezone header

3.13.15
- show tooltips on multi-select values
- add tocco home breadcrumb item

3.13.14
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.13
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.12
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.11
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.10
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.9
- cleanup marking code
- add native date(time)picker for touch devices

3.13.8
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.7
- add _widget_key query param to all requests

3.13.6
- pass escapeHtml to display formatter

3.13.5
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.4
- fix growing textarea
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.3
- fix cut off text in singleValue Selects
- add fetchModules helper

3.13.2
- add missing dependency for releasing

3.13.1
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.11
- revert focus fix

3.12.10
- do not auto focus anything on touch devices

3.12.9
- improve UX for simple selects on touch devices

3.12.8
- fix double scrollbars in some modals

3.12.7
- fix missing focus styles in some elements

3.12.6
- prevent infinite resize
- show left column drop indicator
- consider vertical drop position for column sorting

3.12.5
- add tooltip texts for buttons of upload component

3.12.4
- fix table edit styling

3.12.3
- set checkbox field to touched on safari/ipad/iphone

3.12.2
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes

3.12.1
- hide table refresh button when nothing happens

3.12.0
- initial release for version 3.12

0.1.0
- initial release
