import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-eitswiss-evaluation-execution-participation-exam-manager-action/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
