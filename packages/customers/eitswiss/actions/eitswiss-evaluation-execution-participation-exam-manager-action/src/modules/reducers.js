import actionReducer, {sagas as actionSagas} from './evaluationExecutionParticipationExamManagerAction'

export default {
  evaluationExecutionParticipationExamManagerAction: actionReducer
}

export const sagas = [actionSagas]
