export const SET_TO_CHECK = 'evaluationExecutionParticipationExamManagerAction/SET_TO_CHECK'
export const SET_TO_APPROVED = 'evaluationExecutionParticipationExamManagerAction/SET_TO_APPROVED'
export const SET_STATUS_COMPLETED = 'evaluationExecutionParticipationExamManagerAction/SET_STATUS_COMPLETED'

export const setToCheck = () => ({
  type: SET_TO_CHECK
})

export const setToApproved = () => ({
  type: SET_TO_APPROVED
})

export const setStatusCompleted = () => ({
  type: SET_STATUS_COMPLETED
})
