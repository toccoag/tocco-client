import {setToApproved, setToCheck} from './actions'
import reducer from './reducer'
import sagas from './sagas'

export {setToCheck, setToApproved}
export {sagas}
export default reducer
