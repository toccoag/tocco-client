import * as actions from './actions'

const initialState = {
  settingToApproved: false,
  settingToCheck: false
}

const handleSetToApproved = state => ({...state, settingToApproved: true})

const handleSetToCheck = state => ({...state, settingToCheck: true})

const handleSetStatusCompleted = () => ({...initialState})

const ACTION_HANDLERS = {
  [actions.SET_TO_CHECK]: handleSetToCheck,
  [actions.SET_TO_APPROVED]: handleSetToApproved,
  [actions.SET_STATUS_COMPLETED]: handleSetStatusCompleted
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
