import {all, call, put, select, takeEvery} from 'redux-saga/effects'
import {externalEvents, rest} from 'tocco-app-extensions'
import {evaluationExecutionParticipationActionSelector} from 'tocco-evaluation-execution-participation-action/src/main'

import * as actions from './actions'

export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeEvery(actions.SET_TO_CHECK, setToCheck), takeEvery(actions.SET_TO_APPROVED, setToApproved)])
}

export function* setToCheck() {
  yield call(setStatus, 'check')
}

export function* setToApproved() {
  yield call(setStatus, 'approved')
}

export function* setStatus(status) {
  const {uuid} = yield select(evaluationExecutionParticipationActionSelector)
  const resource = `widgets/eitswissEvaluationExecutionParticipationExamManager/${uuid}/status`
  const options = {
    method: 'PUT',
    body: {
      status
    }
  }

  const response = yield call(rest.requestSaga, resource, options)

  yield put(actions.setStatusCompleted())

  if (response.status === 204) {
    const {selection} = yield select(inputSelector)
    // if fullscreen action was opened from list navigate back to list
    // if fullscreen action was opened via uuid show empty page and fire visibility status
    if (selection) {
      yield put(externalEvents.fireExternalEvent('onCancel'))
    } else {
      yield put(externalEvents.fireExternalEvent('onSuccess'))
    }
  }
}
