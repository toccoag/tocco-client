import PropTypes from 'prop-types'
import {
  actionEmitter,
  actions,
  appFactory,
  externalEvents,
  formData,
  notification,
  selection
} from 'tocco-app-extensions'
import BaseAction from 'tocco-evaluation-execution-participation-action/src/main'
import {appContext, env, reducer as reducerUtil} from 'tocco-util'

import FormButtons from './components/FormButtons'
import reducers, {sagas} from './modules/reducers'

const packageName = 'eitswiss-evaluation-execution-participation-exam-manager-action'
const EXTERNAL_EVENTS = ['onSuccess', 'onCancel', 'emitAction']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  formData.addToStore(store, () => ({}))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  const handleNotifications = !events?.emitAction
  notification.addToStore(store, handleNotifications)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  const content = <BaseAction {...input} FormButtonsComponent={FormButtons} store={store} />

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    publicPath,
    textResourceModules: ['actiongroup', 'component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const EitswissEvaluationExecutionParticipationExamManagerAction = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

EitswissEvaluationExecutionParticipationExamManagerAction.propTypes = {
  appContext: appContext.propTypes.isRequired,
  selection: selection.propType,
  uuid: PropTypes.string,
  actionProperties: PropTypes.shape({
    reportDisplayId: PropTypes.string
  })
}

export default EitswissEvaluationExecutionParticipationExamManagerAction
export const app = appFactory.createBundleableApp(
  packageName,
  initApp,
  EitswissEvaluationExecutionParticipationExamManagerAction
)
