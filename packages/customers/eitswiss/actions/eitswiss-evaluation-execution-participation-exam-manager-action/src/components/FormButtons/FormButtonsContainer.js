import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues, isSubmitting, isValid} from 'redux-form'
import {externalEvents, form} from 'tocco-app-extensions'
import {REDUX_FORM_NAME} from 'tocco-evaluation-execution-participation-action/src/main'

import {setToApproved, setToCheck} from '../../modules/evaluationExecutionParticipationExamManagerAction'

import FormButtons from './FormButtons'

const mapActionCreators = {
  fireExternalEvent: externalEvents.fireExternalEvent,
  setToCheck,
  setToApproved
}

const mapStateToProps = state => {
  return {
    formDefinition: state.evaluationExecutionParticipationAction.formDefinition,
    formValues: getFormValues(REDUX_FORM_NAME)(state),
    evaluation: state.evaluationExecutionParticipationAction.evaluation,
    formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state),
    selection: state.input.selection,
    valid: isValid(REDUX_FORM_NAME)(state),
    settingToApproved: state.evaluationExecutionParticipationExamManagerAction.settingToApproved,
    settingToCheck: state.evaluationExecutionParticipationExamManagerAction.settingToCheck,
    submitting: isSubmitting(REDUX_FORM_NAME)(state)
  }
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(FormButtons))
