import PropTypes from 'prop-types'
import {selection as selectionUtil} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

const FormButtons = ({
  selection,
  evaluation,
  submitting,
  settingToApproved,
  settingToCheck,
  intl,
  fireExternalEvent,
  setToCheck,
  setToApproved
}) => {
  const handleBack = () => fireExternalEvent('onCancel')
  return (
    <>
      {selection && (
        <Button
          disabled={submitting || settingToApproved || settingToCheck}
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.back'})}
          look="raised"
          onClick={handleBack}
        />
      )}
      {!evaluation.readOnly && (
        <Button
          disabled={submitting || settingToApproved || settingToCheck}
          pending={submitting}
          label={intl.formatMessage({id: 'client.actions.evaluation-execution-participation-action.submit'})}
          type="submit"
          look="raised"
          ink="primary"
        />
      )}
      {evaluation.status === 'executed' && (
        <Button
          disabled={submitting || settingToApproved || settingToCheck}
          pending={settingToCheck}
          label={intl.formatMessage({
            id: 'client.actions.eitswiss-evaluation-execution-participation-exam-manager-action.check'
          })}
          look="raised"
          onClick={setToCheck}
        />
      )}
      {evaluation.status === 'executed' && (
        <Button
          disabled={submitting || settingToApproved || settingToCheck}
          pending={settingToApproved}
          label={intl.formatMessage({
            id: 'client.actions.eitswiss-evaluation-execution-participation-exam-manager-action.approve'
          })}
          look="raised"
          ink="primary"
          onClick={setToApproved}
        />
      )}
    </>
  )
}

FormButtons.propTypes = {
  evaluation: PropTypes.object,
  selection: selectionUtil.propType,
  intl: PropTypes.object.isRequired,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  settingToApproved: PropTypes.bool,
  settingToCheck: PropTypes.bool,
  setToCheck: PropTypes.func.isRequired,
  setToApproved: PropTypes.func.isRequired,
  fireExternalEvent: PropTypes.func.isRequired
}

export default FormButtons
