import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-physioswiss-online-registration-non-members/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
