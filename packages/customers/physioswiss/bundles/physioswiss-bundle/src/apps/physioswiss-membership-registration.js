import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-physioswiss-membership-registration/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
