3.13.25
- preserve splat path after creating entity

3.13.24
- added data-cy to mail-action button
- handle null minChars in formDefinition

3.13.23
- able to reveal password on login form
- able to reveal password on password update
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- add table layout preferences
- hide navigation arrow on tiles
- show tile on narrow phones
- hide empty cells on tile

3.13.22
- adjust min char search field validation
- clean up searchFormValiation

3.13.21
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.20
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.19
- fix stated value labels spacing
- fix color of label-neutral

3.13.18
- harmonize google maps icon positioning in LocationEdit

3.13.17
- add timezone header

3.13.16
- show tooltips on multi-select values
- add tocco home breadcrumb item
- add tocco home breadcrumb to dms
- add tocco home breadcrumb to history

3.13.15
- use table layout as defined in list form
- use SimpleFormApp for SearchFilterNameForm
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.14
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- enable tile layout for entity-lists via cookies
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.13
- cleanup save button code

3.13.12
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.11
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.10
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.9
- cleanup marking code
- add native date(time)picker for touch devices
- remove custom _widget_key param

3.13.8
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.7
- add _widget_key query param to all requests

3.13.6
- pass escapeHtml to display formatter

3.13.5
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.4
- fix growing textarea
- handle StyledLayoutContainer styling globally
- add enter handling to SelectRowNums and SearchFilterNameForm
- able to set link state on breadcrumbs

3.13.3
- fix cut off text in singleValue Selects
- add fetchModules helper
- reuse fetchModules helper

3.13.2
- disable register button when required modules are forbidden

3.13.1
- fix sorting of answer options
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.32
- revert focus fix

3.12.31
- do not auto focus anything on touch devices

3.12.30
- improve UX for simple selects on touch devices

3.12.29
- fix double scrollbars in some modals
- fix height of exam edit

3.12.28
- fix missing focus styles in some elements
- handle multi step relations in position preferences
- pass forname to preference util
- add data-cy to input edit table

3.12.27
- prevent infinite resize
- show left column drop indicator
- able to drop column on left side
- consider vertical drop position for column sorting

3.12.26
- add tooltip texts for buttons of upload component

3.12.25
- Make submit button hideable

3.12.24
- fix text select input edit and show available options
- fix table edit styling

3.12.23
- set checkbox field to touched on safari/ipad/iphone

3.12.22
- add string table navigation handling
- enable string arrow navigation for string edits
- add datepicker table navigation handling
- enable datepicker arrow navigation
- add text table navigation handling
- enable text arrow navigation
- add cy attributes
- enable to pass a custom-label to SaveButton

3.12.21
- extend simple table form with scroll behaviour
- show input edit table on widgets

3.12.20
- add copy function to entity-browser

3.12.19
- show correct link for subtables

3.12.18
- hide table refresh button when nothing happens

3.12.17
- able to navigate to create view on detail

3.12.16
- Make buttons in evaluation-execution-participation-action customizable

3.12.15
- remove selection controller in dms

3.12.14
- wait for initialized before loading data
- able to click on the label in the column picker
- keep correct sort order in column picker
- add data-cy attributes
- fix hover on toaster close buttons

3.12.13
- transform initial value paths

3.12.12
- add context to auto-complete
- use ListView helpers

3.12.11
- add sorting, pagination and column widths
- use SimpleTableFormApp
- show correct select field in export action
- show max age hint

3.12.10
- use error message when action fires error event

3.12.9
- support report actions in adjustAllActions

3.12.8
- add StatedCellValue for form fields in tables
- add table form components
- extend form modifiers
- allow virtual table to be disabled
- add simple table form app
- use core components
- remove unused / broken styling
- disable virtual table

3.12.7
- be able to accept prompt with enter

3.12.6
- extend font sizes with missing sizes

3.12.5
- trim searchInput value
- improve line numbers color in dark mode
- save column width in preferences

3.12.4
- add declareColoredLabel util
- do not pass boolean to component

3.12.3
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.2
- ignore datepicker for arrow keyboard handling

3.12.1
- fix dropping column on handle
- fix sorting of columns with missing preferences
- fix sorting of columns with missing preferences
- fix keyboard handling for datepicker

3.12.0
- initial release for version 3.12

3.11.17
- do not apply btn hover style on disable buttons

3.11.16
- handle query changes properly

3.11.15
- se correct color for promotion status
- add copy icon
- improve history action ui
- pass origin event for onChange
- improve history action data loading
- preselect row
- fix outputjob in synchron simple action
- fix navigatino to document edit page
- fix initial navigation to detail view or action
- improve diff viewer theme

3.11.14
- support search fields in useAutofocus

3.11.13
- add legacy font sizes to HtmlEditor
- fix selection handling
- cleanup transient props
- adjust toaster for action validation

3.11.12
- TOCDEV-9023

3.11.11
- improve selection in history table
- adjust diff view
- implement breadcrumb and adjust navigation
- add url helpers to diff view
- support history action
- clear selection if new table is loaded

3.11.10
- add mailbox icon

3.11.9
- set input envs

3.11.8
- add default values as input
- create physioswiss-membership-registration

3.11.7
- navigate to parent folder after deletion

3.11.6
- use correct shouldForwardProp implementation

3.11.5
- export combineSelection
- add new property to theme
- add navigateToPath to navigationStrategy
- add icon
- replace react-burger-button package with custom solution since the package still used default props and was not ready for react v18

3.11.4
- update react-router v6
- update react-router v6
- remove unused history

3.11.3
- use transient props for styled components
- align stated value boxes content vertically

3.11.2
- add new icons

3.11.1
- replace not-allowed cursor in immutable inputs with text cursor to improve UX

3.11.0
- initial release for version 3.11

0.1.0
- initial release