import PropTypes from 'prop-types'
import {useEffect} from 'react'
import MembershipRegistrationApp from 'tocco-membership-registration/src/main'
import {LoadMask} from 'tocco-ui'

const PhysioswissMembershipRegistration = ({input, defaultValues, loadDefaultValues}) => {
  useEffect(() => {
    loadDefaultValues()
  }, [loadDefaultValues])
  return (
    <LoadMask required={[defaultValues]}>
      <MembershipRegistrationApp defaultValues={defaultValues} {...input} />
    </LoadMask>
  )
}

PhysioswissMembershipRegistration.propTypes = {
  input: PropTypes.object,
  defaultValues: PropTypes.object,
  loadDefaultValues: PropTypes.func
}

export default PhysioswissMembershipRegistration
