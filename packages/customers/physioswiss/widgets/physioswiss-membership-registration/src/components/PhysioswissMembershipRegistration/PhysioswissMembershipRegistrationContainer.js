import {connect} from 'react-redux'

import {loadDefaultValues} from '../../modules/physioswissMembershipRegistration/actions'

import PhysioswissMembershipRegistration from './PhysioswissMembershipRegistration'

const mapActionCreators = {
  loadDefaultValues
}

const mapStateToProps = state => ({
  input: state.input,
  defaultValues: state.physioswissMembershipRegistration.defaultValues
})

export default connect(mapStateToProps, mapActionCreators)(PhysioswissMembershipRegistration)
