import physioswissMembershipRegistrationReducer from './physioswissMembershipRegistration/reducer'
import physioswissMembershipRegistrationSagas from './physioswissMembershipRegistration/sagas'

export default {
  physioswissMembershipRegistration: physioswissMembershipRegistrationReducer
}

export const sagas = [physioswissMembershipRegistrationSagas]
