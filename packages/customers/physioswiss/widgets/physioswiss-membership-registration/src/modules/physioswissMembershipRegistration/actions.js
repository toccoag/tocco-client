export const LOAD_DEFAULT_VALUES = 'physioswissMembershipRegistration/LOAD_DEFAULT_VALUES'
export const SET_DEFAULT_VALUES = 'physioswissMembershipRegistration/SET_DEFAULT_VALUES'

export const loadDefaultValues = () => ({
  type: LOAD_DEFAULT_VALUES
})

export const setDefaultValues = defaultValues => ({
  type: SET_DEFAULT_VALUES,
  payload: {defaultValues}
})
