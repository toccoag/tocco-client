import {all, call, put, takeLatest} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {queryString as queryStringUtil} from 'tocco-util'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_DEFAULT_VALUES, loadDefaultValues)])
}

export function* loadDefaultValues() {
  const {membershipType, address} = queryStringUtil.fromQueryString(location.search)
  const defaultValues = {
    ...(yield call(getDefaultValue, membershipType, 'Membership_type', 'relMembership_type')),
    ...(yield call(getDefaultValue, address, 'Address', 'relAddress'))
  }
  yield put(actions.setDefaultValues(defaultValues))
}

function* getDefaultValue(key, model, field) {
  if (key) {
    const display = yield call(rest.fetchDisplay, model, key)
    return {
      [field]: {
        key,
        display
      }
    }
  } else {
    return {}
  }
}
