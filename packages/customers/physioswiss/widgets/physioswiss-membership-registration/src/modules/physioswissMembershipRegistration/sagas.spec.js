import {all, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('physioswiss-membership-registration', () => {
  describe('sagas', () => {
    describe('rootSaga', () => {
      test('should fork child sagas', () => {
        const generator = rootSaga()
        expect(generator.next().value).to.deep.equal(
          all([takeLatest(actions.LOAD_DEFAULT_VALUES, sagas.loadDefaultValues)])
        )
        expect(generator.next().done).to.be.true
      })
    })

    describe('loadDefaultValues', () => {
      test('should load displays', () => {
        delete window.location
        window.location = {
          search: '?membershipType=1&address=2'
        }
        return expectSaga(sagas.loadDefaultValues)
          .provide([
            [matchers.call(rest.fetchDisplay, 'Membership_type', '1'), 'type display'],
            [matchers.call(rest.fetchDisplay, 'Address', '2'), 'address display']
          ])
          .put(
            actions.setDefaultValues({
              relAddress: {key: '2', display: 'address display'},
              relMembership_type: {key: '1', display: 'type display'}
            })
          )
          .run()
      })
      test('should ignore undefined values', () => {
        delete window.location
        window.location = {
          search: '?other=2'
        }
        return expectSaga(sagas.loadDefaultValues).put(actions.setDefaultValues({})).run()
      })
    })
  })
})
