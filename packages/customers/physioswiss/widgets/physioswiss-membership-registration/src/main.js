import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {env, reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import PhysioswissMembershipRegistration from './components/PhysioswissMembershipRegistration'
import reducers, {sagas} from './modules/reducers'

const packageName = 'physioswiss-membership-registration'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <PhysioswissMembershipRegistration />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const PhysioswissMembershipRegistrationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

/**
 * This extends `tocco-membership-registration`, check main.js there for further information.
 * Form default values can be amended by passing keys in the urls. Use `membershipType` or `address` query params.
 */
PhysioswissMembershipRegistrationApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  defaultValues: PropTypes.object,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default PhysioswissMembershipRegistrationApp
export const app = appFactory.createBundleableApp(packageName, initApp, PhysioswissMembershipRegistrationApp)
