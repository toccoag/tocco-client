import PropTypes from 'prop-types'
import {actions, appFactory, errorLogging, externalEvents, notification, formData, login} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {env, reducer as reducerUtil, appContext, consoleLogger} from 'tocco-util'

import OnlineRegistration from './components/OnlineRegistration'
import reducers, {sagas} from './modules/reducers'

const packageName = 'physioswiss-online-registration-non-members'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = (
    <>
      <notification.Notifications />
      <OnlineRegistration />
    </>
  )

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  notification.addToStore(store, true)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  formData.addToStore(store, () => ({
    listApp: EntityListApp
  }))
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  login.addToStore(store)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [notification.connectSocket()],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const OnlineRegistrationNonMembersApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

OnlineRegistrationNonMembersApp.propTypes = {
  /**
   * A URL that the user gets redirected to after they're logged in
   */
  redirectUrl: PropTypes.string,
  formBase: PropTypes.string.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default OnlineRegistrationNonMembersApp
export const app = appFactory.createBundleableApp(packageName, initApp, OnlineRegistrationNonMembersApp)
