import {reducer as form} from 'redux-form'

import onlineRegistrationReducer from './onlineRegistration/reducer'
import onlineRegistrationSagas from './onlineRegistration/sagas'
import onlineRegistrationCreateReducer from './onlineRegistrationCreate/reducer'
import onlineRegistrationCreateSagas from './onlineRegistrationCreate/sagas'

export default {
  onlineRegistration: onlineRegistrationReducer,
  onlineRegistrationCreate: onlineRegistrationCreateReducer,
  form
}

export const sagas = [onlineRegistrationSagas, onlineRegistrationCreateSagas]
