import {actions as formActions} from 'redux-form'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, env, intl, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/OnlineRegistrationCreateForm'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('physioswiss-online-registration-non-members', () => {
  describe('modules', () => {
    describe('onlineRegistrationCreate', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_FORM_DEFINITION, sagas.loadFormDefinition),
                takeLatest(actions.SUBMIT_FORM, sagas.submitForm)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadFormDefinition', () => {
          test('should load form and field definitions and initialize form', () => {
            const formDefinition = {children: []}
            const expectedFormDefinition = {
              createEndpoint: 'physioswiss/widgets/onlineRegistrationNonMembers/register',
              children: []
            }
            const fieldDefinitions = [{id: 'email'}, {id: 'field_2'}]
            const defaultValues = {email: ''}
            const entityValues = {field_2: 'value 2'}
            const formValues = {
              __key: undefined,
              __model: 'User',
              __version: undefined,
              email: 'Test@tocco.ch'
            }
            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [select(sagas.inputSelector), {formBase: 'Online_registration_non_members'}],
                [select(sagas.onlineRegistrationSelector), {newUserEmail: 'Test@tocco.ch'}],
                [matchers.call(rest.fetchForm, 'Online_registration_non_members', 'create'), formDefinition],
                [matchers.call(form.getFieldDefinitions, expectedFormDefinition), fieldDefinitions],
                [matchers.call(form.getDefaultValues, fieldDefinitions), defaultValues],
                [matchers.call(api.getFlattenEntity, {model: 'User'}), entityValues],
                [
                  matchers.call(form.entityToFormValues, {...entityValues, ...defaultValues}, fieldDefinitions),
                  formValues
                ]
              ])
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .put(actions.setFormDefinition(expectedFormDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .run()
          })
        })

        describe('setupFormForNewUser', () => {
          test('should initialize redux form', () => {
            const fieldDefinitions = [{id: 'field-id'}, {id: 'second', defaultValue: 'value'}]
            const pseudoEntityPaths = {
              pseudoField_question_1: {
                value: 'value'
              }
            }
            const expectedDefaultValues = {
              second: 'value'
            }
            const expectedEntityValues = {
              __key: undefined,
              __model: 'User',
              __version: undefined,
              pseudoField_question_1: 'value'
            }
            const expectedFormValues = {
              ...expectedEntityValues,
              ...expectedDefaultValues,
              email: 'test@tocco.ch'
            }
            return expectSaga(sagas.setupFormForNewUser, fieldDefinitions)
              .provide([[select(sagas.onlineRegistrationSelector), {pseudoEntityPaths, newUserEmail: 'test@tocco.ch'}]])
              .call(form.getDefaultValues, fieldDefinitions)
              .call(api.getFlattenEntity, {
                model: 'User',
                paths: {...pseudoEntityPaths}
              })
              .call(
                form.entityToFormValues,
                {...expectedEntityValues, ...expectedDefaultValues, email: 'test@tocco.ch'},
                fieldDefinitions
              )
              .put(formActions.initialize(REDUX_FORM_NAME, expectedFormValues))
              .run()
          })

          test('should not set email for no existing new user', () => {
            const fieldDefinitions = []
            const expectedEntityValues = {
              __key: undefined,
              __model: 'User',
              __version: undefined
            }
            const expectedFormValues = {
              ...expectedEntityValues
            }
            return expectSaga(sagas.setupFormForNewUser, fieldDefinitions)
              .provide([[select(sagas.onlineRegistrationSelector), {pseudoEntityPaths: {}, newUserEmail: undefined}]])
              .call(form.getDefaultValues, fieldDefinitions)
              .call(api.getFlattenEntity, {model: 'User', paths: {}})
              .call(form.entityToFormValues, {...expectedEntityValues}, fieldDefinitions)
              .put(formActions.initialize(REDUX_FORM_NAME, expectedFormValues))
              .run()
          })
        })

        describe('submitForm', () => {
          const fieldDefinitions = [{id: 'field'}]
          const flatEntity = {field: 'value'}
          const application = {
            pk: null,
            field: 'value'
          }
          const widgetConfigKey = '456'
          env.setWidgetConfigKey(widgetConfigKey)

          test('should submit', () => {
            const response = {status: 201}
            return expectSaga(sagas.submitForm)
              .provide([
                [select(sagas.onlineRegistrationCreateSelector), {fieldDefinitions}],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'physioswiss/widgets/onlineRegistrationNonMembers/register', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(actions.hideForm())
              .put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
              .run()
          })

          test('should handle validation', () => {
            const response = {status: 400, body: {errorCode: 'VALIDATION_FAILED', errors: []}}
            return expectSaga(sagas.submitForm)
              .provide([
                [select(sagas.onlineRegistrationCreateSelector), {fieldDefinitions}],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatEntity), application],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'physioswiss/widgets/onlineRegistrationNonMembers/register', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: application,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: validation.getErrorCompact(response.body.errors)
                })
              )
              .run()
          })
        })
      })
    })
  })
})
