import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, intl, remoteLogger, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/OnlineRegistrationCreateForm'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'

export const inputSelector = state => state.input
export const onlineRegistrationCreateSelector = state => state.onlineRegistrationCreate
export const onlineRegistrationSelector = state => state.onlineRegistration

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_FORM_DEFINITION, loadFormDefinition), takeLatest(actions.SUBMIT_FORM, submitForm)])
}

export function* loadFormDefinition() {
  const {formBase} = yield select(inputSelector)
  // add createEndpoint for async validation
  const formDefinition = {
    ...(yield call(rest.fetchForm, formBase, 'create')),
    createEndpoint: 'physioswiss/widgets/onlineRegistrationNonMembers/register'
  }
  const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)

  yield call(setupFormForNewUser, fieldDefinitions)
  yield all([put(actions.setFormDefinition(formDefinition)), put(actions.setFieldDefinitions(fieldDefinitions))])
}

export function* setupFormForNewUser(fieldDefinitions) {
  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const {pseudoEntityPaths, newUserEmail} = yield select(onlineRegistrationSelector)
  const entityValues = yield call(api.getFlattenEntity, {model: 'User', paths: {...pseudoEntityPaths}})
  const formValues = yield call(
    form.entityToFormValues,
    {...entityValues, ...defaultValues, ...(newUserEmail ? {email: newUserEmail} : {})},
    fieldDefinitions
  )
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))
}

export function* submitForm() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const formValues = yield select(getFormValues(REDUX_FORM_NAME))
  const {fieldDefinitions} = yield select(onlineRegistrationCreateSelector)
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const onlineRegistrationEntity = yield call(api.toEntity, flatEntity)

  const locale = yield select(intl.localeSelector)

  const response = yield call(rest.requestSaga, 'physioswiss/widgets/onlineRegistrationNonMembers/register', {
    method: 'POST',
    queryParams: {
      locale
    },
    body: onlineRegistrationEntity,
    acceptedErrorCodes: ['VALIDATION_FAILED']
  })

  if (response.status === 201) {
    yield put(actions.hideForm())
    yield put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    if (response.body.errorCode === 'VALIDATION_FAILED') {
      yield put(
        notification.toaster({
          type: 'error',
          title: 'client.component.actions.validationError',
          body: validation.getErrorCompact(response.body.errors)
        })
      )
    } else {
      remoteLogger.logError('unexpected error during submit', response)
    }
  }
}
