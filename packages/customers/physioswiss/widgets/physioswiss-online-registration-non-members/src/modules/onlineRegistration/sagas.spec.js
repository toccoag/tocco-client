import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {login} from 'tocco-app-extensions'

import {Pages} from '../../utils/constants'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('physioswiss-online-registration-non-members', () => {
  describe('modules', () => {
    describe('onlineRegistrationCreate', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeLatest(actions.INIT, sagas.init), takeLatest(actions.HANDLE_REDIRECT, sagas.handleRedirect)])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('init', () => {
          test('should display the login page if the user is not logged in', () => {
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: false}],
                {
                  take() {}
                }
              ])
              .put(login.doSessionCheck())
              .take(login.SET_LOGGED_IN)
              .put(actions.setPage(Pages.Login))
              .run()
          })

          test('should redirect if the user is already logged in', () => {
            delete window.location
            window.location = {}
            return expectSaga(sagas.init)
              .provide([
                [select(sagas.loginSelector), {loggedIn: true}],
                [select(sagas.inputSelector), {redirectUrl: 'https://tocco.ch/tocco'}],
                {
                  take() {}
                }
              ])
              .run()
              .then(() => {
                expect(window.location.href).to.eql('https://tocco.ch/tocco')
              })
          })
        })
      })
      describe('functions', () => {
        describe('handleRedirect', () => {
          test('redirect url is defined', () => {
            delete window.location
            window.location = {}

            return expectSaga(sagas.handleRedirect)
              .provide([
                [matchers.call(sagas.handleRedirect)],
                [select(sagas.inputSelector), {redirectUrl: 'https://tocco.ch'}]
              ])
              .run()
              .then(() => {
                expect(window.location.href).to.eql('https://tocco.ch')
              })
          })
        })
      })
    })
  })
})
