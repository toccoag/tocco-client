import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {login} from 'tocco-app-extensions'
import {queryString} from 'tocco-util'

import {Pages} from '../../utils/constants'

import * as actions from './actions'

export const loginSelector = state => state.login
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([takeLatest(actions.INIT, init), takeLatest(actions.HANDLE_REDIRECT, handleRedirect)])
}

export function* init() {
  yield put(login.doSessionCheck())
  yield take(login.SET_LOGGED_IN)

  const {loggedIn} = yield select(loginSelector)

  if (loggedIn === true) {
    yield call(handleRedirect)
  } else {
    yield put(actions.setPage(Pages.Login))
  }
}

export function* handleRedirect() {
  const redirectUrl = yield loadRedirectUrl()
  if (redirectUrl) {
    window.location.href = redirectUrl
  }
}

function* loadRedirectUrl() {
  const {_redirect_url: queryUrl} = queryString.fromQueryString(window.location.search)
  if (queryUrl) {
    return queryUrl
  } else {
    const {redirectUrl: inputUrl} = yield select(inputSelector)
    return inputUrl
  }
}
