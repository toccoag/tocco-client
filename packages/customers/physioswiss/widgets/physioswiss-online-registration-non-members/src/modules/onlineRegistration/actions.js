export const INIT = 'onlineRegistration/INIT'
export const HANDLE_REDIRECT = 'onlineRegistration/HANDLE_REDIRECT'
export const SET_NEW_USER_EMAIL = 'onlineRegistration/SET_NEW_USER_EMAIL'
export const SET_PAGE = 'onlineRegistration/SET_PAGE'

export const init = () => ({
  type: INIT
})

export const handleRedirect = () => ({
  type: HANDLE_REDIRECT
})

export const setNewUserEmail = newUserEmail => ({
  type: SET_NEW_USER_EMAIL,
  payload: {
    newUserEmail
  }
})

export const setPage = page => ({
  type: SET_PAGE,
  payload: {
    page
  }
})
