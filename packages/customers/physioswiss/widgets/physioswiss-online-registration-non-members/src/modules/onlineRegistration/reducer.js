import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  page: null,
  newUserEmail: undefined
}

const ACTION_HANDLERS = {
  [actions.SET_NEW_USER_EMAIL]: reducerUtil.singleTransferReducer('newUserEmail'),
  [actions.SET_PAGE]: reducerUtil.singleTransferReducer('page')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
