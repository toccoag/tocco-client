import {connect} from 'react-redux'

import {setNewUserEmail, init, setPage, handleRedirect} from '../../modules/onlineRegistration/actions'

import OnlineRegistration from './OnlineRegistration'

const mapActionCreators = {
  setNewUserEmail,
  init,
  setPage,
  handleRedirect
}

const mapStateToProps = state => ({
  page: state.onlineRegistration.page
})

export default connect(mapStateToProps, mapActionCreators)(OnlineRegistration)
