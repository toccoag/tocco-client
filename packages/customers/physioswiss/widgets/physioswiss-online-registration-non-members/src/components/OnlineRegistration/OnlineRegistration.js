import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'

import {Pages} from '../../utils/constants'
import Login from '../Login'
import OnlineRegistrationCreateForm from '../OnlineRegistrationCreateForm'

const OnlineRegistration = ({init, page, setPage, setNewUserEmail, handleRedirect}) => {
  useEffect(() => {
    init()
  }, [init])
  return (
    <LoadMask required={[page !== null]}>
      {(() => {
        switch (page) {
          case Pages.RegisterView:
            return <OnlineRegistrationCreateForm />
          case Pages.Login:
            return (
              <Login
                onLoggedIn={handleRedirect}
                onRegister={({username}) => {
                  setNewUserEmail(username)
                  setPage(Pages.RegisterView)
                }}
              />
            )
          default:
            return null
        }
      })()}
    </LoadMask>
  )
}

OnlineRegistration.propTypes = {
  setNewUserEmail: PropTypes.func.isRequired,
  setPage: PropTypes.func.isRequired,
  init: PropTypes.func.isRequired,
  handleRedirect: PropTypes.func.isRequired,
  page: PropTypes.string
}

export default OnlineRegistration
