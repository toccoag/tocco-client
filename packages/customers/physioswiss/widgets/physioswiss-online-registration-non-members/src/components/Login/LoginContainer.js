import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import Login from './Login'

const mapActionCreators = {
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Login))
