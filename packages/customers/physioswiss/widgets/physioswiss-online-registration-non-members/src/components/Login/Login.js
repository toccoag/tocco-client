import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import LoginApp from 'tocco-login/src/main'
import {Typography} from 'tocco-ui'

import {VisibilityStatus} from '../../visibilityStatus'

import {StyledContainer} from './StyledComponents'

const Login = ({locale, onLoggedIn, onRegister, fireVisibilityStatusChangeEvent}) => {
  fireVisibilityStatusChangeEvent([VisibilityStatus.authentication])

  return (
    <StyledContainer>
      <Typography.H2>
        <FormattedMessage id="client.physioswiss-online-registration-non-members.loginTitle" />
      </Typography.H2>
      <LoginApp
        showTitle={false}
        register={onRegister}
        loginSuccess={onLoggedIn}
        locale={locale}
        entryPage="username"
      />
    </StyledContainer>
  )
}

Login.propTypes = {
  onLoggedIn: PropTypes.func.isRequired,
  onRegister: PropTypes.func.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default Login
