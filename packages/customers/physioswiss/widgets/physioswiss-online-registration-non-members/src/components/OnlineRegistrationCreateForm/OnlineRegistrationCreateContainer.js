import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'
import {externalEvents} from 'tocco-app-extensions'

import {loadFormDefinition} from '../../modules/onlineRegistrationCreate/actions'

import {REDUX_FORM_NAME} from './Form'
import OnlineRegistrationCreate from './OnlineRegistrationCreate'

const mapActionCreators = {
  loadFormDefinition,
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  hideForm: state.onlineRegistrationCreate.hideForm,
  formDefinition: state.onlineRegistrationCreate.formDefinition,
  fieldDefinitions: state.onlineRegistrationCreate.fieldDefinitions,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(OnlineRegistrationCreate)
