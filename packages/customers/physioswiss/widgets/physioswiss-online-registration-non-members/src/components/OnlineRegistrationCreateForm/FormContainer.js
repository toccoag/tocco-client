import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form} from 'tocco-app-extensions'

import {submitForm} from '../../modules/onlineRegistrationCreate/actions'

import Form, {REDUX_FORM_NAME} from './Form'

const mapActionCreators = {
  submitForm
}

const mapStateToProps = state => ({
  formDefinition: state.onlineRegistrationCreate.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Form))
