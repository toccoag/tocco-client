import PropTypes from 'prop-types'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

export const REDUX_FORM_NAME = 'physioswiss-online-registration-non-members-create'

const Form = ({formDefinition, formValues, submitting, valid, submitForm, intl}) => {
  const formEventProps = form.hooks.useFormEvents({submitForm})

  return (
    <form {...formEventProps}>
      <form.FormBuilder
        entity={{model: 'User'}}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        mode={'create'}
      />
      <Button
        disabled={submitting || !valid}
        label={intl.formatMessage({id: 'client.physioswiss-online-registration-non-members.submit'})}
        type="submit"
        look="raised"
        ink="primary"
      />
    </form>
  )
}

Form.propTypes = {
  formDefinition: PropTypes.object,
  formValues: PropTypes.object,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  submitForm: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(Form)
