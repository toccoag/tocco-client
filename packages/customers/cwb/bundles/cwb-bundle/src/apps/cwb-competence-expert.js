import {appFactory} from 'tocco-app-extensions'
import {app} from 'tocco-cwb-competence-expert/src/main'

appFactory.registerAppInRegistry(app.name, app.init)
export {app}
