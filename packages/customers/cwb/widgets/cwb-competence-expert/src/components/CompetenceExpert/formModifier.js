import {form} from 'tocco-app-extensions'

const removeActionsFromForm = (formDefinition, allowCreate, intl) => {
  formDefinition = form.removeActions(formDefinition, ['new', 'copy', 'delete'])
  if (allowCreate) {
    return form.addCreate(formDefinition, intl)
  } else {
    return formDefinition
  }
}

export const modifyFormDefinition = (formDefinition, allowCreate, intl) => {
  return removeActionsFromForm(formDefinition, allowCreate, intl)
}
