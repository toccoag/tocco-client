import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import CompetenceExpert from './CompetenceExpert'

const mapActionCreators = {}

const mapStateToProps = state => ({
  entityName: state.input.entityName,
  formBase: state.input.formBase,
  reportIds: state.input.reportIds,
  allowCreate: state.input.allowCreate,
  searchFilters: state.input.searchFilters,
  searchFormType: state.input.searchFormType,
  limit: state.input.limit,
  backendUrl: state.input.backendUrl,
  appContext: state.input.appContext,
  onVisibilityStatusChange: state.input.onVisibilityStatusChange
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(CompetenceExpert))
