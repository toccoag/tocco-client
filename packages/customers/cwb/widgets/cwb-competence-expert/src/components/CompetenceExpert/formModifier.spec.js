import {IntlStub} from 'tocco-test-util'

import {modifyFormDefinition} from './formModifier'

describe('customers', () => {
  describe('cwb', () => {
    describe('widgets', () => {
      describe('cwb-competence-expert', () => {
        describe('components', () => {
          describe('CompetenceExpert', () => {
            describe('formModifier', () => {
              describe('modifyFormDefinition', () => {
                const formDefinitionDetailView = {
                  id: 'Competence_expert_edit_detail',
                  componentType: 'form',
                  children: [
                    {
                      id: 'main-action-bar',
                      componentType: 'action-bar',
                      children: [
                        {
                          id: 'createcopy',
                          componentType: 'action-group',
                          children: [
                            {
                              id: 'new',
                              componentType: 'action'
                            },
                            {
                              id: 'copy',
                              componentType: 'action'
                            }
                          ],
                          actions: [],
                          defaultAction: {
                            id: 'new',
                            componentType: 'action'
                          }
                        },
                        {
                          id: 'delete',
                          componentType: 'action'
                        },
                        {
                          id: 'save',
                          componentType: 'action'
                        }
                      ]
                    }
                  ]
                }

                test('should remove createcopy and delete action', () => {
                  const modifiedFormDefinition = modifyFormDefinition(formDefinitionDetailView, false, IntlStub)
                  expect(modifiedFormDefinition.children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children[0].id).to.eql('save')
                })

                test('should add create, when enabled', () => {
                  const modifiedFormDefinition = modifyFormDefinition(formDefinitionDetailView, true, IntlStub)
                  expect(modifiedFormDefinition.children.length).to.eql(1)
                  expect(modifiedFormDefinition.children[0].children.length).to.eql(2)
                  expect(modifiedFormDefinition.children[0].children[0].id).to.eql('new')
                  expect(modifiedFormDefinition.children[0].children[1].id).to.eql('save')
                })
              })
            })
          })
        })
      })
    })
  })
})
