import isPropValidEmotion from '@emotion/is-prop-valid'

const isPropValid = propName => isPropValidEmotion(propName)

export default isPropValid
