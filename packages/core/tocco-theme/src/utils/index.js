import isPropValid from './isPropValid'
import shouldForwardProp from './shouldForwardProp'

export default {shouldForwardProp, isPropValid}
