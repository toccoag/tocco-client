export default {
  name: 'default',
  colors: {
    paper: '#fff',
    primary: '#b22a31',
    primaryLight: '#d13e45',
    secondary: '#263e5b',
    secondaryLight: '#347da9',
    secondaryLighter: '#4fade5',
    text: '#000',
    textLight: '#595959',
    textLighter: '#8c8c8c',
    textDisabled: '#bcbcbc',
    textLabel: '#121212',
    textToaster: '#fcfcfc',
    textPopover: '#fcfcfc',
    backgroundBody: '#dee6ed',
    backgroundBreadcrumbs: '#dee6ed',
    backgroundPopover: '#424242',
    backgroundItemHover: '#e6e6e6',
    backgroundItemSelected: '#ccc',
    backgroundItemLastOpened: '#B9B9B9',
    backgroundTableEvenRow: '#f2f2f2',
    backgroundTableTiles: '#f2f2f2',
    backgroundScrollbar: '#999',
    backgroundScrollbarHover: '#808080',
    backgroundPanelHeaderFooter: '#f2f2f2',
    backgroundTypographyCode: '#e6e6e6',
    backgroundToccoCircleProduction: '#b22a31',
    backgroundToccoCircleTest: '#263e5b',
    backgroundBurgerMenuBars: '#fff',
    backgroundMultiValue: '#e6e6e6',
    hoverInteractiveIcon: '#263e5b',
    loadingSpinner: '#000',
    breadcrumbsActive: '#b22a31',
    border: '#ccc',
    borderTable: '#ccc',
    borderLight: '#ededed',
    borderSelect: '#027cc1',
    signal: {
      danger: '#961b1b',
      dangerLight: '#e77c7c',
      info: '#027cc1',
      infoLight: '#78cdfe',
      success: '#337f37',
      successLight: '#91d395',
      warning: '#ec8a00',
      warningLight: '#ffd7a0',
      neutral: '#CECECE'
    }
  },
  fontFamily: {
    monospace: 'Ubuntu Mono, monospace',
    regular: 'Ubuntu, sans-serif',
    url: 'https://fonts.googleapis.com/css?family=Ubuntu+Mono:300,500|Ubuntu:300,500&display=swap'
  },
  fontSize: {
    base: 1.3,
    factor: 1.1
  },
  fontWeights: {
    regular: 300,
    bold: 500
  },
  lineHeights: {
    dense: 1,
    light: 1.2,
    regular: 1.4
  },
  radii: {
    statusLabel: '8px',
    button: '13px',
    buttonLarge: '25px',
    form: 0,
    dashboard: 0,
    modal: 0,
    toaster: '8px',
    menu: 0,
    menuEntries: '4px'
  },
  space: {
    base: 1.3,
    factor: 2
  },
  specificProperties: {
    actionBarStickyTopPosition: 0
  },
  codeEditorTheme: 'textmate',
  diffViewerDarkTheme: false,
  diffViewer: {
    codeFoldGutterBackground: '#e6e6e6',
    codeFoldBackground: '#f2f2f2',
    gutterBackground: '#f2f2f2',
    diffViewerTitleBackground: '#fff',
    diffViewerTitleBorderColor: '#fff',
    emptyLineBackground: '#fcfcfc',
    addedBackground: '#cbffce',
    addedColor: '#24292e',
    removedBackground: '#ffbfbf',
    removedColor: '#24292e',
    wordAddedBackground: '#91d395',
    wordRemovedBackground: '#e77c7c',
    addedGutterBackground: '#91d395',
    removedGutterBackground: '#e77c7c'
  }
}
