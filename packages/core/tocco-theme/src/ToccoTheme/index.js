import darkTheme from './darkTheme'
import defaultTheme from './defaultTheme'

export default {defaultTheme, darkTheme}
