export default {
  name: 'dark',
  colors: {
    paper: '#212330',
    primary: '#b22a31',
    primaryLight: '#d13e45',
    secondary: '#6eafff',
    secondaryLight: '#82baff',
    secondaryLighter: '#9ECAFF',
    text: '#dfdfdf',
    textLight: '#bdbdbd',
    textLighter: '#818181',
    textDisabled: '#7a7a7a',
    textLabel: '#121212',
    textToaster: '#000',
    textPopover: '#fff',
    backgroundBody: '#2C2F41',
    backgroundBreadcrumbs: '#2C2F41',
    backgroundPopover: '#757575',
    backgroundItemHover: '#444',
    backgroundItemSelected: '#565656',
    backgroundItemLastOpened: '#636363',
    backgroundTableEvenRow: '#2f2f2f',
    backgroundTableTiles: '#2f2f2f',
    backgroundScrollbar: '#444',
    backgroundScrollbarHover: '#666',
    backgroundPanelHeaderFooter: '#444',
    backgroundTypographyCode: '#444',
    backgroundToccoCircleProduction: '#b22a31',
    backgroundToccoCircleTest: '#263e5b',
    backgroundBurgerMenuBars: '#fff',
    backgroundMultiValue: '#6e6e6e',
    hoverInteractiveIcon: '#fff',
    loadingSpinner: '#cccccc',
    breadcrumbsActive: '#82baff',
    border: '#666',
    borderTable: '#666',
    borderLight: '#444',
    borderSelect: '#4fade5',
    signal: {
      danger: '#d74444',
      dangerLight: '#f18888',
      info: '#4fade5',
      infoLight: '#9ce6ff',
      success: '#4DAC54',
      successLight: '#72c172',
      warning: '#ec8a00',
      warningLight: '#ffcc84',
      neutral: '#CECECE'
    }
  },
  fontFamily: {
    monospace: 'Ubuntu Mono, monospace',
    regular: 'Ubuntu, sans-serif',
    url: 'https://fonts.googleapis.com/css?family=Ubuntu+Mono:300,500|Ubuntu:300,500&display=swap'
  },
  fontSize: {
    base: 1.3,
    factor: 1.1
  },
  fontWeights: {
    regular: 300,
    bold: 500
  },
  lineHeights: {
    dense: 1,
    light: 1.2,
    regular: 1.4
  },
  radii: {
    statusLabel: '8px',
    button: '13px',
    buttonLarge: '25px',
    form: 0,
    dashboard: 0,
    modal: 0,
    toaster: '8px',
    menu: 0,
    menuEntries: '4px'
  },
  space: {
    base: 1.3,
    factor: 2
  },
  specificProperties: {
    actionBarStickyTopPosition: 0
  },
  codeEditorTheme: 'dracula',
  diffViewerDarkTheme: true,
  diffViewer: {
    diffViewerBackground: '#212330',
    codeFoldGutterBackground: '#444',
    codeFoldBackground: '#2f2f2f',
    gutterBackground: '#2f2f2f',
    diffViewerTitleBackground: '#212330',
    diffViewerTitleBorderColor: '#212330',
    emptyLineBackground: '#2f2f2f',
    diffViewerColor: '#dfdfdf',
    addedBackground: '#217a28',
    addedColor: '#dfdfdf',
    removedBackground: '#b92e2e',
    removedColor: '#dfdfdf',
    wordAddedBackground: '#35913c',
    wordRemovedBackground: '#d74444',
    addedGutterBackground: '#35913c',
    removedGutterBackground: '#d74444'
  }
}
