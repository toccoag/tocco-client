import _get from 'lodash/get'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export default {
  title: 'Tocco-Theme/Theme'
}

const Box = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 140px);
  grid-gap: 20px 10px;
  justify-content: center;
`

const Container = styled.div`
  text-align: center;
  display: block;
  cursor: pointer;
`

const Circle = styled.span`
  height: 25px;
  width: 25px;
  background-color: ${({color, theme}) => _get(theme.colors, color)};
  text-align: center;
  border-radius: 50%;
  display: flex;
  margin: auto;
  font-size: 20px;
  padding: 0 7px;
  box-sizing: border-box;
`

const Preview = ({color}) => (
  <Container>
    <Circle color={color}></Circle>
    <span>{color.join('.')}</span>
  </Container>
)
Preview.propTypes = {
  color: PropTypes.array
}

export const Colors = () => (
  <div>
    <Box>
      <Preview color={['paper']} />
      <Preview color={['primary']} />
      <Preview color={['secondary']} />
      <Preview color={['secondaryLight']} />
      <Preview color={['text']} />
      <Preview color={['backgroundBody']} />
      <Preview color={['backgroundBreadcrumbs']} />
      <Preview color={['backgroundPopover']} />
      <Preview color={['signal', 'info']} />
      <Preview color={['signal', 'success']} />
      <Preview color={['signal', 'warning']} />
      <Preview color={['signal', 'danger']} />
    </Box>
  </div>
)
