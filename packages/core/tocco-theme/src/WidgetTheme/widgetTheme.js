export default {
  name: 'widget',
  colors: {
    paper: '#FFFFFF',
    primary: '#000000',
    primaryLight: '#424242',
    secondary: '#333',
    secondaryLight: '#7c7c7c',
    secondaryLighter: '#bababa',
    text: '#000',
    textLight: '#595959',
    textLighter: '#8c8c8c',
    textDisabled: '#bcbcbc',
    textLabel: '#121212',
    textToaster: '#fcfcfc',
    textPopover: '#fcfcfc',
    backgroundBody: '#EDF1F5',
    backgroundBreadcrumbs: '#DEE6ED',
    backgroundPopover: '#424242',
    backgroundItemHover: '#E6E6E6',
    backgroundItemSelected: '#CCC',
    backgroundItemLastOpened: '#B9B9B9',
    backgroundTableEvenRow: '#f2f2f2',
    backgroundTableTiles: '#f2f2f2',
    backgroundScrollbar: '#999',
    backgroundScrollbarHover: '#808080',
    backgroundPanelHeaderFooter: '#f2f2f2',
    backgroundTypographyCode: '#e6e6e6',
    backgroundMultiValue: '#e6e6e6',
    loadingSpinner: '#000',
    border: '#ccc',
    borderTable: '#ccc',
    borderLight: '#ededed',
    borderSelect: '#027cc1',
    signal: {
      danger: '#961b1b',
      dangerLight: '#e77c7c',
      info: '#027cc1',
      infoLight: '#78cdfe',
      success: '#337f37',
      successLight: '#91d395',
      warning: '#ec8a00',
      warningLight: '#ffd7a0',
      neutral: '#CECECE'
    }
  },
  fontFamily: {
    monospace: 'inherit',
    regular: 'inherit'
  },
  fontSize: {
    base: 1.0,
    factor: 1.1
  },
  fontWeights: {
    regular: 300,
    bold: 500
  },
  lineHeights: {
    dense: 1,
    light: 1.2,
    regular: 1.4
  },
  radii: {
    statusLabel: '8px',
    button: '2px',
    buttonLarge: '4px',
    form: 0,
    dashboard: 0,
    modal: 0,
    toaster: '8px',
    menu: 0
  },
  space: {
    base: 1.0,
    factor: 2
  },
  specificProperties: {
    actionBarStickyTopPosition: 0
  },
  codeEditorTheme: 'textmate',
  diffViewerDarkTheme: false,
  diffViewer: {}
}
