# Tocco Theme

The properties defined in theme files enable global styling across the application.

Hereby an explanation of the purpose of some keys:

## Colors
The main color scheme used in the application

- paper: background color of text
- primary : main CI/CD color (all terminologically successive entries also mark as CI/CD colors)
- secondaryLight: light derivative of secondary color
- secondaryLighter: lighter derivative of secondary color
- text: main color of text
- textLight: light derivative of text color
- textLighter: lighter derivative of text color
- textDisabled: color of disabled text
- textLabel: color of formatted value labels
- textToaster: textcolor whithin toaster elements
- textPopover: textcolor whithin popover elements
- backgroundBody: background color of the body
- backgroundPopover: color of all the Popovers
- signal: colors used to notify the user
- backgroundItemHover: used for hover in Select and Table
- backgroundItemSelected: used for selected items in Table and Select
- backgroundItemLastOpened: background color of the Table item which was last opened
- backgroundTableEvenRow: used for zebra stripe effect in Table
- backgroundScrollbar: background color of scrollbar thumb
- backgroundScrollbarHover: hover background color of scrollbar thumb
- backgroundPanelHeaderFooter: background color of Panel header and footer
- backgroundTypographyCode: background color of Code and Pre
- backgroundToccoCircleProduction: color of the tocco circle in production environments
- backgroundToccoCircleTest: color of the tocco circle in test environments
- backgroundBurgerMenuBars: color of the burger menu bars
- backgroundMultiValue: background of multivalue select labels
- hoverInteractiveIcon: hover color of interactable icons
- border: general border color (used for inputs and table)
- borderTable: border color of Table component
- borderSelect: Used for the border around Select dropdown

## Radii
The main border-radii used in the application

- button: Used for all buttons
- buttonLarge: Used for large buttons (e.g. SSO Provider Buttons)
- form: Used for form elements such as Fields, Selects, Panels
- dashboard: Used for dashboard info box
- modal: Used for any modal
- menu: Used for menus such as buttons menu (not for Select Sropdowns, there the `form` radii is used)
- statusLabel: used for any status label
- menuEntries: used for the menu entries of the main navigation tree

## SpecificProperties
Specific properties used for a precise spot only

- actionBarStickyTopPosition: top distance of sticky action bar

