import styled from 'styled-components'

import {design, scale, themeSelector} from '../utilStyles'

const ALLOWED_CONDITIONS = [
  design.condition.BASE,
  design.condition.INFO,
  design.condition.DANGER,
  design.condition.SUCCESS,
  design.condition.WARNING
]

const COLORS = {
  [design.condition.BASE]: themeSelector.color('signal.neutral'),
  [design.condition.DANGER]: themeSelector.color('signal.dangerLight'),
  [design.condition.SUCCESS]: themeSelector.color('signal.successLight'),
  [design.condition.WARNING]: themeSelector.color('signal.warningLight'),
  [design.condition.INFO]: themeSelector.color('signal.infoLight')
}

const StyledSignalBox = styled.div`
  && {
    background-color: ${({condition}) =>
      COLORS[ALLOWED_CONDITIONS.includes(condition) ? condition : design.condition.INFO]};
    padding: ${scale.space(-0.25)};
    border-radius: ${themeSelector.radii('statusLabel')};

    &:not(:last-child) {
      margin-bottom: ${scale.space(-1)};
    }

    * {
      color: ${themeSelector.color('text')};
    }
  }
`

StyledSignalBox.propTypes = {
  condition: design.oneOfPropTypeAndCompletelyMapped(ALLOWED_CONDITIONS, COLORS)
}

export {ALLOWED_CONDITIONS, StyledSignalBox as default}
