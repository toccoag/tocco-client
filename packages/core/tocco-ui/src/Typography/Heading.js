import PropTypes from 'prop-types'

import {getTextOfChildren} from '../utilStyles'

import {StyledH1, StyledH2, StyledH3, StyledH4, StyledH5, StyledH6} from './StyledHeading'

/**
 * Use <H1>, <H2>, <H3>, <H4>, <H5> and <H6> according there semantic hierarchy. Since only one <H1> should exist on
 * a single webpage and React components are usually embeded, use <H2> or lower. Utilize prop styledLike to tweek
 * size and space.
 */

const H1 = ({breakWords = true, styledLike = 'H1', children}) => (
  <StyledH1
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH1>
)

const H2 = ({breakWords = true, styledLike = 'H2', children}) => (
  <StyledH2
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH2>
)

const H3 = ({breakWords = true, styledLike = 'H3', children}) => (
  <StyledH3
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH3>
)

const H4 = ({breakWords = true, styledLike = 'H4', children}) => (
  <StyledH4
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH4>
)

const H5 = ({breakWords = true, styledLike = 'H5', children}) => (
  <StyledH5
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH5>
)

const H6 = ({breakWords = true, styledLike = 'H6', children}) => (
  <StyledH6
    breakWords={breakWords}
    styledLike={styledLike}
    title={breakWords ? undefined : getTextOfChildren(children)}
  >
    {children}
  </StyledH6>
)

H1.propTypes =
  H2.propTypes =
  H3.propTypes =
  H4.propTypes =
  H5.propTypes =
  H6.propTypes =
    {
      /**
       * If true words break with hyphens. If false text is forced into a single truncated line.
       */
      breakWords: PropTypes.bool,
      children: PropTypes.node,
      /**
       * Control size and space independently from semantic meaning.
       */
      styledLike: PropTypes.oneOf(['H1', 'H2', 'H3', 'H4', 'H5', 'H6'])
    }

export {H1, H2, H3, H4, H5, H6}
