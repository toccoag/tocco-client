import PropTypes from 'prop-types'

import {getTextOfChildren} from '../utilStyles'

import {StyledDd, StyledDl, StyledDt, StyledLi, StyledOl, StyledUl} from './StyledList'

const Dd = ({breakWords = true, children}) => (
  <StyledDd breakWords={breakWords} title={breakWords ? undefined : getTextOfChildren(children)}>
    {children}
  </StyledDd>
)

const Dl = props => <StyledDl>{props.children}</StyledDl>

const Dt = ({breakWords = true, children}) => (
  <StyledDt breakWords={breakWords} title={breakWords ? undefined : getTextOfChildren(children)}>
    {children}
  </StyledDt>
)

const Li = ({children}) => <StyledLi>{children}</StyledLi>

const Ol = ({children}) => <StyledOl>{children}</StyledOl>

const Ul = ({children}) => <StyledUl as="ul">{children}</StyledUl>

Dd.propTypes = Dt.propTypes = {
  /**
   * If true words break with hyphens. If false text is forced into a single truncated line.
   */
  breakWords: PropTypes.bool,
  children: PropTypes.node
}

Dl.propTypes =
  Li.propTypes =
  Ol.propTypes =
  Ul.propTypes =
    {
      children: PropTypes.node
    }

export {Dd, Dl, Dt, Li, Ol, Ul}
