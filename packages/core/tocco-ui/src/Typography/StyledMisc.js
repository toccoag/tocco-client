import styled from 'styled-components'

import {declareFont, declareNoneWrappingText, declareWrappingText, scale, themeSelector} from '../utilStyles'

const StyledB = styled.b`
  && {
    ${declareFont({
      fontWeight: themeSelector.fontWeight('bold')
    })}
  }
  ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
`

const StyledCode = styled.code`
  && {
    ${declareFont({
      fontFamily: themeSelector.fontFamily('monospace'),
      fontSize: scale.font(-1)
    })}
    background-color: ${themeSelector.color('backgroundTypographyCode')};
    border-radius: ${themeSelector.radii('form')};
    padding: ${scale.space(-4)} ${scale.space(-2)};
    white-space: pre;
    overflow-y: auto;
  }
`

const StyledDel = styled.del`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    text-decoration: line-through;
  }
`

const StyledEm = styled.em`
  && {
    ${declareFont({
      fontStyle: 'italic'
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledFigcaption = styled.figcaption`
  && {
    ${declareFont({
      fontSize: scale.font(-1)
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    margin: ${scale.space(-2)} 0 ${scale.space(-1)} 0;

    &:last-child {
      margin-bottom: 0;
    }
  }
`
const StyledI = styled.i`
  && {
    ${declareFont({
      fontStyle: 'italic'
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledIns = styled.ins`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    text-decoration: underline;
  }
`

const StyledLabel = styled.label`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    margin: 0 0 ${scale.space(-1)};

    &:last-child {
      margin-bottom: 0;
    }
  }
`

const StyledMark = styled.mark`
  && {
    ${declareFont({
      fontFamily: themeSelector.fontFamily('monospace'),
      color: themeSelector.color('paper')
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    background-color: ${themeSelector.color('signal.info')};
    border-radius: ${themeSelector.radii('form')};
    padding: ${scale.space(-4)} ${scale.space(-2)};
  }
`

const StyledP = styled.p`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    margin: 0 0 ${scale.space(-1)};

    &:last-child {
      margin-bottom: 0;
    }
  }
`

const StyledPre = styled.pre`
  && {
    ${declareFont({
      fontFamily: themeSelector.fontFamily('monospace'),
      fontSize: scale.font(-1)
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    background-color: ${themeSelector.color('backgroundTypographyCode')};
    border-radius: ${themeSelector.radii('form')};
    border: 1px solid ${themeSelector.color('border')};
    display: block;
    margin: 0 0 ${scale.space(-1)};
    padding: ${scale.space(-3)} ${scale.space(-2)};

    &:last-child {
      margin-bottom: 0;
    }
  }
`

const StyledS = styled.s`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    text-decoration: line-through;
  }
`

const StyledSmall = styled.small`
  && {
    ${declareFont()}
    font-size: calc(1em / ${themeSelector.fontSize('factor')});
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledSpan = styled.span`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledSub = styled.sub`
  && {
    ${declareFont({
      fontSize: scale.font(-1)
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    line-height: 0;
    position: relative;
    bottom: -0.25em;
    vertical-align: baseline;
  }
`

const StyledSup = styled.sup`
  && {
    ${declareFont({
      fontSize: scale.font(-1)
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    line-height: 0;
    position: relative;
    top: -0.5em;
    vertical-align: baseline;
  }
`

const StyledStrong = styled.strong`
  && {
    ${declareFont({
      fontWeight: themeSelector.fontWeight('bold')
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledTime = styled.time`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
  }
`

const StyledU = styled.u`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    text-decoration: underline;
  }
`

const StyledVar = styled.var`
  && {
    ${declareFont({
      fontFamily: themeSelector.fontFamily('monospace'),
      fontSize: scale.font(-1)
    })}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    &:after {
      content: ']';
    }

    &:before {
      content: '[';
    }
  }
`

const StyledQ = styled.q`
  && {
    ${declareFont()}
    ${({breakWords}) => (breakWords ? declareWrappingText() : declareNoneWrappingText())}
    &:after {
      content: close-quote;
    }

    &:before {
      content: open-quote;
    }
  }
`

export {
  StyledB,
  StyledCode,
  StyledDel,
  StyledEm,
  StyledFigcaption,
  StyledI,
  StyledIns,
  StyledLabel,
  StyledMark,
  StyledP,
  StyledPre,
  StyledQ,
  StyledS,
  StyledSmall,
  StyledSpan,
  StyledStrong,
  StyledSub,
  StyledSup,
  StyledTime,
  StyledU,
  StyledVar
}
