import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {
  B,
  Code,
  Del,
  Em,
  Figcaption,
  I,
  Ins,
  Mark,
  P,
  Pre,
  Q,
  S,
  Small,
  Span,
  Strong,
  Sub,
  Sup,
  Time,
  U,
  Var
} from './Misc'

const expectTextAndTitle = Comp => {
  testingLibrary.renderWithTheme(<Comp breakWords={false}>Lorem Ipsum</Comp>)
  expect(screen.queryByText('Lorem Ipsum')).to.exist
  expect(screen.queryByTitle('Lorem Ipsum')).to.exist
}

describe('tocco-ui', () => {
  describe('Typography', () => {
    describe('Misc', () => {
      describe('B', () => {
        test('should render', () => {
          expectTextAndTitle(B)
        })
      })

      describe('Code', () => {
        test('should render', () => {
          expectTextAndTitle(Code)
        })
      })

      describe('Del', () => {
        test('should render', () => {
          expectTextAndTitle(Del)
        })
      })

      describe('Em', () => {
        test('should render', () => {
          expectTextAndTitle(Em)
        })
      })

      describe('Figcaption', () => {
        test('should render', () => {
          expectTextAndTitle(Figcaption)
        })
      })

      describe('I', () => {
        test('should render', () => {
          expectTextAndTitle(I)
        })
      })

      describe('Ins', () => {
        test('should render', () => {
          expectTextAndTitle(Ins)
        })
      })

      describe('Mark', () => {
        test('should render', () => {
          expectTextAndTitle(Mark)
        })
      })

      describe('P', () => {
        test('should render', () => {
          expectTextAndTitle(P)
        })
      })

      describe('Pre', () => {
        test('should render', () => {
          expectTextAndTitle(Pre)
        })
      })

      describe('Q', () => {
        test('should render', () => {
          expectTextAndTitle(Q)
        })
      })

      describe('S', () => {
        test('should render', () => {
          expectTextAndTitle(S)
        })
      })

      describe('Small', () => {
        test('should render', () => {
          expectTextAndTitle(Small)
        })
      })

      describe('Span', () => {
        test('should render', () => {
          expectTextAndTitle(Span)
        })
      })

      describe('Strong', () => {
        test('should render', () => {
          expectTextAndTitle(Strong)
        })
      })

      describe('Sub', () => {
        test('should render', () => {
          expectTextAndTitle(Sub)
        })
      })

      describe('Sup', () => {
        test('should render', () => {
          expectTextAndTitle(Sup)
        })
      })

      describe('Time', () => {
        test('should render', () => {
          testingLibrary.renderWithTheme(
            <Time title="23:59:59" dateTime="23:59:59">
              23:59:59
            </Time>
          )
          expect(screen.queryByText('23:59:59')).to.exist
          expect(screen.queryByTitle('23:59:59')).to.exist
        })
      })

      describe('U', () => {
        test('should render', () => {
          expectTextAndTitle(U)
        })
      })

      describe('Var', () => {
        test('should render', () => {
          expectTextAndTitle(Var)
        })
      })
    })
  })
})
