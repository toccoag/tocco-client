import PropTypes from 'prop-types'

import {getTextOfChildren} from '../utilStyles'

import {
  StyledB,
  StyledCode,
  StyledDel,
  StyledEm,
  StyledFigcaption,
  StyledI,
  StyledIns,
  StyledLabel,
  StyledMark,
  StyledP,
  StyledPre,
  StyledQ,
  StyledS,
  StyledSmall,
  StyledSpan,
  StyledStrong,
  StyledSub,
  StyledSup,
  StyledTime,
  StyledU,
  StyledVar
} from './StyledMisc'

export const B = ({breakWords = true, useTitle = true, children}) => (
  <StyledB breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledB>
)

export const Code = ({breakWords = true, useTitle = true, children}) => (
  <StyledCode title={getTitle(breakWords, useTitle, children)}>{children}</StyledCode>
)

export const Del = ({breakWords = true, useTitle = true, children}) => (
  <StyledDel breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledDel>
)

export const Em = ({breakWords = true, useTitle = true, children}) => (
  <StyledEm breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledEm>
)

export const Figcaption = ({breakWords = true, useTitle = true, children}) => (
  <StyledFigcaption breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledFigcaption>
)

export const I = ({breakWords = true, useTitle = true, children}) => (
  <StyledI breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledI>
)

export const Ins = ({breakWords = true, useTitle = true, children}) => (
  <StyledIns breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledIns>
)

export const Label = ({breakWords = true, useTitle = true, children, htmlFor}) => (
  <StyledLabel breakWords={breakWords} title={getTitle(breakWords, useTitle, children)} htmlFor={htmlFor}>
    {children}
  </StyledLabel>
)

export const Mark = ({breakWords = true, useTitle = true, children}) => (
  <StyledMark breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledMark>
)

export const P = ({breakWords = true, useTitle = true, children, id}) => (
  <StyledP breakWords={breakWords} title={getTitle(breakWords, useTitle, children)} id={id}>
    {children}
  </StyledP>
)

export const Pre = ({breakWords = true, useTitle = true, children}) => (
  <StyledPre breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledPre>
)

export const S = ({breakWords = true, useTitle = true, children}) => (
  <StyledS breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledS>
)

export const Small = ({breakWords = true, useTitle = true, children}) => (
  <StyledSmall breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledSmall>
)

export const Span = ({breakWords = true, useTitle = true, children, id}) => (
  <StyledSpan breakWords={breakWords} title={getTitle(breakWords, useTitle, children)} id={id}>
    {children}
  </StyledSpan>
)

export const Sub = ({breakWords = true, useTitle = true, children}) => (
  <StyledSub breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledSub>
)

export const Sup = ({breakWords = true, useTitle = true, children}) => (
  <StyledSup breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledSup>
)

export const Strong = ({breakWords = true, useTitle = true, children}) => (
  <StyledStrong breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledStrong>
)

export const Time = ({breakWords = false, title, dateTime, children}) => (
  <StyledTime breakWords={breakWords} title={title} dateTime={dateTime}>
    {children}
  </StyledTime>
)

export const U = ({breakWords = true, useTitle = true, children}) => (
  <StyledU breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledU>
)

export const Var = ({breakWords = true, useTitle = true, children}) => (
  <StyledVar breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledVar>
)

export const Q = ({breakWords = true, useTitle = true, children}) => (
  <StyledQ breakWords={breakWords} title={getTitle(breakWords, useTitle, children)}>
    {children}
  </StyledQ>
)

const getTitle = (breakWords, useTitle, children) => (!useTitle || breakWords ? undefined : getTextOfChildren(children))

const propTypes = {
  /**
   * If true words break with hyphens.
   * If false text is forced into a single truncated line.
   */
  breakWords: PropTypes.bool,
  /**
   * True if the default title should be used, false disables the title
   */
  useTitle: PropTypes.bool,
  id: PropTypes.string,
  children: PropTypes.node
}

B.propTypes =
  Code.propTypes =
  Del.propTypes =
  Em.propTypes =
  Figcaption.propTypes =
  I.propTypes =
  Ins.propTypes =
  Label.propTypes =
  Mark.propTypes =
  P.propTypes =
  Pre.propTypes =
  Q.propTypes =
  S.propTypes =
  Small.propTypes =
  Span.propTypes =
  Strong.propTypes =
  Sub.propTypes =
  Sup.propTypes =
  U.propTypes =
  Var.propTypes =
    propTypes

Time.propTypes = {
  /**
   * If true words break with hyphens.
   * If false text is forced into a single truncated line.
   */
  breakWords: PropTypes.bool,
  children: PropTypes.node,
  dateTime: PropTypes.string.isRequired,
  title: PropTypes.string
}
