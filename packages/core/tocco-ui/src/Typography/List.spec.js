import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {Dd, Dt, Dl, Li, Ol, Ul} from './List'

const expectTextAndTitle = Comp => {
  testingLibrary.renderWithTheme(<Comp breakWords={false}>Lorem Ipsum</Comp>)
  expect(screen.queryByText('Lorem Ipsum')).to.exist
  expect(screen.queryByTitle('Lorem Ipsum')).to.exist
}

describe('tocco-ui', () => {
  describe('Typography', () => {
    describe('List', () => {
      describe('Dd', () => {
        test('should render', () => {
          expectTextAndTitle(Dd)
        })
      })

      describe('Dl', () => {
        test('should display text', () => {
          testingLibrary.renderWithTheme(<Dl>Lorem ipsum</Dl>)
          expect(screen.queryByText('Lorem ipsum')).to.exist
        })
      })

      describe('Dt', () => {
        test('should render', () => {
          expectTextAndTitle(Dt)
        })
      })

      describe('Li', () => {
        test('should display text', () => {
          testingLibrary.renderWithTheme(<Li>Lorem ipsum</Li>)
          expect(screen.queryByText('Lorem ipsum')).to.exist
        })
      })

      describe('Ol', () => {
        test('should display text', () => {
          testingLibrary.renderWithTheme(<Ol>Lorem ipsum</Ol>)
          expect(screen.queryByText('Lorem ipsum')).to.exist
        })
      })

      describe('Ul', () => {
        test('should display text', () => {
          testingLibrary.renderWithTheme(<Ul as="ul">Lorem ipsum</Ul>)
          expect(screen.queryByText('Lorem ipsum')).to.exist
        })
      })
    })
  })
})
