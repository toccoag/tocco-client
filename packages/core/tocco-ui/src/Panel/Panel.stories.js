import Typography from '../Typography'

import Panel from './'

export default {
  title: 'Tocco-UI/Layout/Panel',
  component: Panel,
  argTypes: {
    isOpenInitial: {type: 'boolean', defaultValue: true},
    isFramed: {type: 'boolean', defaultValue: true},
    isToggleable: {type: 'boolean', defaultValue: true}
  }
}

export const PanelGroup = args => (
  <Panel.Group initialOpenPanelIndex={2}>
    {['Group 1', 'Group 2', 'Group 3'].map(item => (
      <Panel.Wrapper key={item} {...args}>
        <Panel.Header>
          <Typography.H4>{item}</Typography.H4>
        </Panel.Header>
        <Panel.Body>
          <Typography.Span>Body {item}</Typography.Span>
        </Panel.Body>
      </Panel.Wrapper>
    ))}
  </Panel.Group>
)

export const PanelWithHeader = args => (
  <Panel.Wrapper {...args}>
    <Panel.Header>
      <Typography.H4>Header</Typography.H4>
    </Panel.Header>
    <Panel.Body>
      <Typography.Span>Body</Typography.Span>
    </Panel.Body>
  </Panel.Wrapper>
)
