import {env} from 'tocco-util'

import {mapping} from './panels/mapping'

export default {
  Body: env.embedStrategyFactory({mapping: mapping.Body}),
  Group: env.embedStrategyFactory({mapping: mapping.Group}),
  Header: env.embedStrategyFactory({mapping: mapping.Header}),
  Wrapper: env.embedStrategyFactory({mapping: mapping.Wrapper})
}
