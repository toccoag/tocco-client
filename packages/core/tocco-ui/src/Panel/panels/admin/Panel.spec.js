import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Typography from '../../../Typography'

import Panel from './'

describe('tocco-ui', () => {
  describe('Panel', () => {
    describe('Panel', () => {
      test('should render a open panel by default', async () => {
        testingLibrary.renderWithTheme(
          <Panel.Wrapper>
            <Panel.Header>
              <Typography.H4>Title</Typography.H4>
            </Panel.Header>
            <Panel.Body>
              <Typography.Span>Body</Typography.Span>
            </Panel.Body>
          </Panel.Wrapper>
        )
        await screen.findAllByTestId('icon')

        jestExpect(screen.getByText('Body')).toBeVisible()
      })

      test('should regard isOpenInitial prop for state', async () => {
        testingLibrary.renderWithTheme(
          <Panel.Wrapper isOpenInitial={false}>
            <Panel.Header>
              <Typography.H4>Title</Typography.H4>
            </Panel.Header>
            <Panel.Body>
              <Typography.Span>Body</Typography.Span>
            </Panel.Body>
          </Panel.Wrapper>
        )
        await screen.findAllByTestId('icon')

        jestExpect(screen.getByText('Body')).not.toBeVisible()
      })

      test('should open and close on click', async () => {
        testingLibrary.renderWithTheme(
          <Panel.Wrapper>
            <Panel.Header>
              <Typography.H4>Title</Typography.H4>
            </Panel.Header>
            <Panel.Body>
              <Typography.Span>Body</Typography.Span>
            </Panel.Body>
          </Panel.Wrapper>
        )
        await screen.findAllByTestId('icon')

        fireEvent.click(screen.getByRole('heading'))
        jestExpect(screen.getByText('Body')).not.toBeVisible()
        fireEvent.click(screen.getByRole('heading'))
        jestExpect(screen.getByText('Body')).toBeVisible()
      })

      test('should open and close on click', () => {
        const onToggleSpy = sinon.spy()

        testingLibrary.renderWithTheme(
          <Panel.Wrapper controlledIsOpen={false} onToggle={onToggleSpy}>
            <Panel.Header>
              <Typography.H4>Title</Typography.H4>
            </Panel.Header>
            <Panel.Body>
              <Typography.Span>Body</Typography.Span>
            </Panel.Body>
          </Panel.Wrapper>
        )

        jestExpect(screen.getByText('Body')).not.toBeVisible()
        fireEvent.click(screen.getByRole('heading'))
        expect(onToggleSpy).to.have.been.calledWith(true)
      })
    })
  })
})
