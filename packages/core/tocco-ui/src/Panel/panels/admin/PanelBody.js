import PropTypes from 'prop-types'

import StyledPanelBody from './StyledPanelBody'

/**
 * Only <Panel.Body/> is affected by the visibility state.
 */

const PanelBody = ({children, isFramed, isOpen, ...props}) => (
  <StyledPanelBody isFramed={isFramed} isOpen={isOpen} data-cy={props['data-cy']}>
    <div>{children}</div>
  </StyledPanelBody>
)

PanelBody.propTypes = {
  children: PropTypes.node,
  isFramed: PropTypes.bool,
  /**
   * Boolean to control if <Panel.Body/> is initially opened. Value is always overridden by parent element.
   */
  isOpen: PropTypes.bool,
  'data-cy': PropTypes.string
}

export default PanelBody
