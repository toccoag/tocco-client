import styled from 'styled-components'

import {scale} from '../../../utilStyles'

const StyledPanelBody = styled.div`
  && {
    padding: ${({isFramed}) => (isFramed ? scale.space(-0.375) : 0)};
    display: ${({isOpen}) => (isOpen ? 'block' : 'none')};
  }
`
export default StyledPanelBody
