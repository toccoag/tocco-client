import Wrapper from './Panel'
import Body from './PanelBody'
import Group from './PanelGroup'
import Header from './PanelHeader'
import StyledPanel from './StyledPanel'
import StyledPanelBody from './StyledPanelBody'
import StyledPanelHeader from './StyledPanelHeader'

export {StyledPanel, StyledPanelBody, StyledPanelHeader}

export default {
  Body,
  Group,
  Header,
  Wrapper
}
