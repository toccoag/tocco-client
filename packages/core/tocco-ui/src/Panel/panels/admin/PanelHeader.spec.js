import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import PanelHeader from './PanelHeader'

describe('tocco-ui', () => {
  describe('Panel', () => {
    describe('PanelHeader', () => {
      test('should render parent and children', async () => {
        testingLibrary.renderWithIntl(
          <PanelHeader isToggleable={true} showToggler={true}>
            <span data-testid="child1">child-1</span>
            <span data-testid="child2">child-2</span>
          </PanelHeader>
        )

        expect(await screen.findByTestId('icon-chevron-down')).to.exist
        expect(screen.getByTestId('child1').textContent).eq('child-1')
        expect(screen.getByTestId('child2').textContent).eq('child-2')
      })

      test('should hide or display Icon according precondition', async () => {
        const {rerender} = testingLibrary.renderWithIntl(<PanelHeader isToggleable={true} showToggler={true} />)

        expect(await screen.findByTestId('icon-chevron-down')).to.exist

        rerender(<PanelHeader isToggleable={false} showToggler={true} />)
        expect(screen.queryByTestId('icon-chevron-down')).to.not.exist

        rerender(<PanelHeader isToggleable={true} showToggler={false} />)
        expect(screen.queryByTestId('icon-chevron-down')).to.not.exist

        rerender(<PanelHeader isToggleable={false} showToggler={false} />)
        expect(screen.queryByTestId('icon-chevron-down')).to.not.exist
      })

      test('should display Ball correctly', async () => {
        const {rerender} = testingLibrary.renderWithIntl(
          <PanelHeader isOpen={false} isToggleable={true} showToggler={true} />
        )
        expect(await screen.findByTestId('icon-chevron-down')).to.exist
        expect(screen.getByTitle('Show more information')).to.exist

        rerender(<PanelHeader isOpen={true} isToggleable={true} showToggler={true} />)

        expect(await screen.findByTestId('icon-chevron-up')).to.exist
        expect(screen.getByTitle('Hide information')).to.exist
      })
    })
  })
})
