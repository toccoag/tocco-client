import styled, {css} from 'styled-components'
import {device} from 'tocco-util'

import {scale, themeSelector} from '../../../utilStyles'

const BORDER_WIDTH = '1px'

const declareDivider = ({theme, isOpen, isFramed}) => {
  const cssShared = `
    &:first-child {
      border-bottom-color: ${theme.colors.borderLight};
      border-bottom-style: ${isFramed ? 'solid' : 'none'};
    }

    &:last-child,
    &:nth-child(3) {
      border-top-color: ${theme.colors.borderLight};
      border-top-style: ${isFramed ? 'solid' : 'none'};
    }
  `

  if (isOpen) {
    return `
      ${cssShared}
      &:first-child {
        border-bottom-width: ${BORDER_WIDTH};
      }

      &:last-child,
      &:nth-child(3) {
        border-top-width: ${BORDER_WIDTH};
      }
    `
  } else {
    return `
      ${cssShared}
      &:first-child {
        border-bottom-width: 0;
      }

      &:last-child {
        border-top-width: 0;
      }

      &:nth-child(3) {
        border-top-width: ${BORDER_WIDTH};
      }
    `
  }
}

export const StyledIconWrapper = styled.span`
  font-size: ${scale.font(1)};
  color: ${themeSelector.color('text')};

  /* only hide arrow on non-touch devices */
  ${!device.isPrimaryTouchDevice() &&
  css`
    opacity: 0;

    &:hover {
      color: ${themeSelector.color('secondaryLight')};
    }
  `}
`

const StyledPanelHeader = styled.div`
  && {
    background-color: ${themeSelector.color('backgroundPanelHeaderFooter')};
    display: flex;
    /* stylelint-disable declaration-block-no-redundant-longhand-properties */
    border-top-left-radius: calc(${themeSelector.radii('form')} - ${BORDER_WIDTH});
    border-top-right-radius: calc(${themeSelector.radii('form')} - ${BORDER_WIDTH});
    border-bottom-right-radius: calc(${themeSelector.radii('form')} - ${BORDER_WIDTH});
    border-bottom-left-radius: calc(${themeSelector.radii('form')} - ${BORDER_WIDTH});
    ${({isOpen}) =>
      isOpen &&
      css`
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      `}
    /* stylelint-enable declaration-block-no-redundant-longhand-properties */
    padding: ${({isFramed}) =>
      isFramed
        ? css`
            ${scale.space(-1.7)} ${scale.space(-0.375)}
          `
        : 0};
    align-items: center;

    /* only apply arrow styles on non-touch devices */
    ${!device.isPrimaryTouchDevice() &&
    css`
      &:hover {
        cursor: pointer;

        ${StyledIconWrapper} {
          opacity: 1;
        }
      }
    `}
    /* stylelint-disable-line rule-empty-line-before */
    > div {
      flex: 1 1 auto;
    }
    ${props => declareDivider(props)}
  }
`

export default StyledPanelHeader
