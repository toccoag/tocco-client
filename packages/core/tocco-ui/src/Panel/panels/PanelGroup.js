import PropTypes from 'prop-types'
import React, {useState} from 'react'

const PanelGroup = ({initialOpenPanelIndex, children}) => {
  const [openPanelIndex, setOpenPanelIndex] = useState(initialOpenPanelIndex)

  const onToggle = (index, open) => {
    setOpenPanelIndex(open === true ? index : undefined)
  }

  return (
    <>
      {React.Children.map(children, (child, i) =>
        React.cloneElement(child, {
          controlledIsOpen: openPanelIndex === i,
          onToggle: open => onToggle(i, open)
        })
      )}
    </>
  )
}

PanelGroup.propTypes = {
  children: PropTypes.node,
  /**
   * Define a panel which is initially opened (zero-based index).
   */
  initialOpenPanelIndex: PropTypes.number
}

export default PanelGroup
