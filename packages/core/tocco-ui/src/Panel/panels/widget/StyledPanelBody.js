import styled, {css} from 'styled-components'

import {scale} from '../../../utilStyles'

const StyledPanelBody = styled.div`
  && {
    padding: ${({isFramed}) =>
      isFramed
        ? css`
            ${scale.space(-1)} ${scale.space(0.2)} ${scale.space(0.2)} ${scale.space(0.2)}
          `
        : 0};
    display: ${({isOpen}) => (isOpen ? 'block' : 'none')};
  }
`
export default StyledPanelBody
