import PropTypes from 'prop-types'
import React, {useState} from 'react'

import StyledPanel from './StyledPanel'

/**
 * <Panel/> is used to conceal and display related content alternating by interaction and to
 * emphasize close relationship of content.
 */
const Panel = ({children, isFramed = true, isOpenInitial = true}) => {
  const [isOpen, setIsOpen] = useState(isOpenInitial)

  return (
    <StyledPanel isFramed={isFramed}>
      {React.Children.map(children, child =>
        React.cloneElement(child, {
          isOpen,
          isToggleable: !isOpenInitial,
          isFramed,
          toggleOpenState: () => setIsOpen(!isOpen)
        })
      )}
    </StyledPanel>
  )
}

Panel.propTypes = {
  children: PropTypes.node,
  /**
   * Boolean to control if <Panel.Header/>, <Panel.Body/> is bordered.
   * Default value is 'true'.
   */
  isFramed: PropTypes.bool,
  /**
   * Boolean to control if <Panel.Body/> is initially opened
   */
  isOpenInitial: PropTypes.bool
}

export default Panel
