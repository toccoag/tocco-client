import styled, {css} from 'styled-components'
import {device} from 'tocco-util'

import {scale, themeSelector} from '../../../utilStyles'

const BORDER_WIDTH = '1px'

const declareDivider = ({theme, isFramed}) => {
  const cssShared = `
    &:first-child {
      border-bottom-color: ${theme.colors.borderLight};
      border-bottom-style: ${isFramed ? 'solid' : 'none'};
    }

    &:last-child,
    &:nth-child(3) {
      border-top-color: ${theme.colors.borderLight};
      border-top-style: ${isFramed ? 'solid' : 'none'};
    }
  `

  return `
      ${cssShared}
      &:first-child {
        border-bottom-width: 0;
      }

      &:last-child {
        border-top-width: 0;
      }

      &:nth-child(3) {
        border-top-width: ${BORDER_WIDTH};
      }
    `
}

const StyledPanelHeader = styled.div`
  && {
    background-color: ${themeSelector.color('paper')};
    display: flex;
    border-radius: calc(${themeSelector.radii('form')} - ${BORDER_WIDTH})
      calc(${themeSelector.radii('form')} - ${BORDER_WIDTH}) 0 0;
    padding: ${({isFramed}) => (isFramed ? scale.space(0.2) : 0)};
    align-items: center;

    > div {
      flex: 1 1 auto;
    }

    * {
      font-weight: ${themeSelector.fontWeight('regular')};
    }
    ${props => declareDivider(props)}
  }
`

export const StyledIconWrapper = styled.span`
  font-size: ${scale.font(1)};
  color: ${themeSelector.color('text')};

  /* only hide arrow on non-touch devices */
  ${!device.isPrimaryTouchDevice() &&
  css`
    opacity: 0;

    &:hover {
      color: ${themeSelector.color('secondaryLight')};
    }
  `}
`

export default StyledPanelHeader
