import styled from 'styled-components'

import {themeSelector} from '../../../utilStyles'

const StyledPanel = styled.div`
  && {
    background-color: ${themeSelector.color('paper')};
    border: 1.5px ${({isFramed}) => (isFramed ? 'solid' : 'none')} ${themeSelector.color('borderLight')};
    border-radius: ${themeSelector.radii('form')};
  }
`
export default StyledPanel
