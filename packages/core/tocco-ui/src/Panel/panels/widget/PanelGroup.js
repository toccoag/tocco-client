import CorePanelGroup from '../PanelGroup'

import StyledPanelGroup from './StyledPanelGroup'

const PanelGroup = props => (
  <StyledPanelGroup>
    <CorePanelGroup {...props} />
  </StyledPanelGroup>
)

PanelGroup.propTypes = CorePanelGroup.propTypes

export default PanelGroup
