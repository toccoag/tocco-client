import PropTypes from 'prop-types'
import React from 'react'

import Icon from '../../../Icon'

import StyledPanelHeader, {StyledIconWrapper} from './StyledPanelHeader'

/**
 * <Panel.Header/> contain by default a button to toggle the visibility state of <Panel.Body>.
 * Header can contain any content.
 */
const PanelHeader = ({
  children,
  isFramed,
  isOpen,
  isToggleable,
  showToggler = true,
  toggleOpenState,
  options = {
    collapseButtonText: 'Hide information',
    unfoldButtonText: 'Show more information'
  }
}) => (
  <StyledPanelHeader isFramed={isFramed} isOpen={isOpen} onClick={isToggleable ? toggleOpenState : () => {}}>
    <div>{React.Children.map(children, child => React.cloneElement(child))}</div>
    {isToggleable && showToggler && (
      <StyledIconWrapper>
        <Icon
          icon={isOpen ? 'chevron-up' : 'chevron-down'}
          onClick={toggleOpenState}
          title={isOpen ? options.collapseButtonText : options.unfoldButtonText}
        />
      </StyledIconWrapper>
    )}
  </StyledPanelHeader>
)

PanelHeader.propTypes = {
  children: PropTypes.node,
  isFramed: PropTypes.bool,
  /**
   * Boolean to control if <Panel.Body/> is initially opened. Value is always overridden by parent element.
   */
  isOpen: PropTypes.bool,
  /**
   * Boolean to control if body can be opened or closed. Value is always overridden by parent element.
   */
  isToggleable: PropTypes.bool,
  /**
   * Show or hide button if needed. Default value is 'true'.
   */
  showToggler: PropTypes.bool,
  /**
   * Function executed on button click to control accordion. Is always passed from parent.
   */
  toggleOpenState: PropTypes.func,
  /* Define display text for buttons as option object (keys: collapseButtonText, unfoldButtonText). */
  options: PropTypes.shape({
    collapseButtonText: PropTypes.string,
    unfoldButtonText: PropTypes.string
  })
}

export default PanelHeader
