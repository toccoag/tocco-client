import {env} from 'tocco-util'

import Panel from './admin'
import WidgetPanel from './widget'

export const mapping = {
  Body: {
    [env.EmbedType.admin]: Panel.Body,
    [env.EmbedType.widget]: WidgetPanel.Body
  },
  Group: {
    [env.EmbedType.admin]: Panel.Group,
    [env.EmbedType.widget]: WidgetPanel.Group
  },
  Header: {
    [env.EmbedType.admin]: Panel.Header,
    [env.EmbedType.widget]: WidgetPanel.Header
  },
  Wrapper: {
    [env.EmbedType.admin]: Panel.Wrapper,
    [env.EmbedType.widget]: WidgetPanel.Wrapper
  }
}
