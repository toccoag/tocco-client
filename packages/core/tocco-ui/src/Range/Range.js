import _omit from 'lodash/omit'
import PropTypes from 'prop-types'
import {useMemo, useRef} from 'react'
import {env} from 'tocco-util'

import EditableValue from '../EditableValue'
import Icon from '../Icon'

import rangeTypeMappings from './rangeTypeMappings'
import {StyledBall, StyledIconWrapper, StyledInput, StyledRange} from './StyledRange'

/**
 * Allows to render EditableValues as a range. The value can be switched between a range or single value.
 */
const Range = props => {
  const {value, events, readOnly, type, options: optionsProp, fromText, toText} = props

  const typeMapping = rangeTypeMappings[type]

  const hasRangeValue = value?.isRangeValue || false

  /**
   * Workaround:
   * https://toccoag.atlassian.net/browse/TOCDEV-5176
   * Problem:
   *   When range is enabled and
   *   to or from values changes
   *   the other value will be reset to a previous value
   *
   * Somehow the `value` in the onChanges handlers was outdated and still had the previous value.
   * Not sure why this wasn't working properly although all memoized hooks have been removed.
   * Therefore the current value is now attached via a `ref` which is not bound 1:1 to the state.
   */
  const valueRef = useRef(value)
  valueRef.current = value

  const exactEvents = useMemo(
    () => ({
      ...events,
      onChange: events.onChange
    }),
    [events]
  )

  const toEvents = {
    onChange: toValue => {
      events.onChange({
        ...valueRef.current,
        to: toValue
      })
    }
  }

  const fromEvents = {
    onChange: fromValue => {
      events.onChange({
        ...valueRef.current,
        from: fromValue
      })
    }
  }

  const getToOptions = (options, fromValue) =>
    typeMapping?.getToOptions ? typeMapping.getToOptions(options, fromValue) : options

  const getFromOptions = (options, toValue) =>
    typeMapping?.getFromOptions ? typeMapping.getFromOptions(options, toValue) : options

  const getFromOrTo = val => (typeMapping?.fromRange ? typeMapping.fromRange(val) : val?.from || val?.to || null)

  const getRangeValue = val =>
    typeMapping?.toRange ? typeMapping.toRange(val) : {from: val, to: val, isRangeValue: true}

  const baseType = typeMapping?.type || type

  const handleExtend = () => {
    const val = hasRangeValue ? getFromOrTo(value) : getRangeValue(value)
    events.onChange(val)
  }

  const isAdmin = env.isInAdminEmbedded()
  const isWidget = env.isInWidgetEmbedded()

  const SingleValueContent = <EditableValue type={baseType} {..._omit(props, ['type'])} events={exactEvents} />
  const RangeValuesContent = (
    <StyledInput direction={isWidget ? 'row' : 'column'}>
      <EditableValue
        {...props}
        options={getFromOptions(optionsProp, value?.to)}
        value={value?.from || null}
        events={fromEvents}
        placeholder={fromText}
      />
      <StyledIconWrapper>
        <Icon icon="horizontal-rule" />
      </StyledIconWrapper>
      <EditableValue
        {...props}
        options={getToOptions(optionsProp, value?.from)}
        value={value?.to || null}
        events={toEvents}
        placeholder={toText}
      />
    </StyledInput>
  )

  return (
    <StyledRange>
      {hasRangeValue ? RangeValuesContent : SingleValueContent}
      <StyledBall
        isAdmin={isAdmin}
        disabled={readOnly}
        icon={hasRangeValue ? 'square-minus' : 'square-plus'}
        onClick={handleExtend}
      />
    </StyledRange>
  )
}

Range.propTypes = {
  /**
   * EditableValue type
   */
  type: PropTypes.string,
  /**
   * The Range Component can either handle a single value or an object with the following three
   * attributes: isRangeValue (true), from, to. The latter two are single values.
   * example:
   *  {
   *    isRangeValue: true,
   *    from: 1,
   *    to: 2
   *  }
   */
  value: PropTypes.any,
  /**
   * See EditableValue
   */
  events: PropTypes.shape({
    onChange: PropTypes.func
  }).isRequired,
  /**
   * See EditableValue
   */
  readOnly: PropTypes.bool,
  /**
   * See EditableValue
   */
  options: PropTypes.object,
  /**
   * Shown above "from" input
   */
  fromText: PropTypes.string,
  /**
   * Shown above "to" input
   */
  toText: PropTypes.string
}

export default Range
