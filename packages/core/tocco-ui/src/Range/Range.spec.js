import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Range from './'

describe('tocco-ui', () => {
  describe('Range', () => {
    describe('<Range>', () => {
      test('should render', async () => {
        testingLibrary.renderWithIntl(<Range type="number" options={{}} value={1} events={{onChange: () => {}}} />)
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('textbox')).to.have.length(1)
      })

      test('should render two inputs for range values', async () => {
        testingLibrary.renderWithIntl(
          <Range type="number" options={{}} value={{isRangeValue: true, from: 1}} events={{onChange: () => {}}} />
        )
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('textbox')).to.have.length(2)
      })

      test('should change to range value on expander click', async () => {
        const onChangeSpy = sinon.spy()
        testingLibrary.renderWithIntl(<Range type="number" options={{}} value={1} events={{onChange: onChangeSpy}} />)
        await screen.findAllByTestId('icon')

        fireEvent.click(screen.getByRole('button'))
        expect(onChangeSpy).to.have.been.calledWith({isRangeValue: true, from: 1, to: 1})
      })

      test('should change to single value on expander click', async () => {
        const onChangeSpy = sinon.spy()
        testingLibrary.renderWithIntl(
          <Range
            type="number"
            options={{}}
            value={{isRangeValue: true, from: null, to: 1}}
            events={{onChange: onChangeSpy}}
          />
        )
        await screen.findAllByTestId('icon')

        fireEvent.click(screen.getByRole('button'))
        expect(onChangeSpy).to.have.been.calledWith(1)
      })
    })
  })
})
