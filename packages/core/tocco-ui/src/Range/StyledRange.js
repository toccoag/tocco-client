import styled, {css} from 'styled-components'

import {Ball} from '../'
import {scale, themeSelector} from '../utilStyles'

export const StyledIconWrapper = styled.div`
  font-size: ${scale.font(-2.7)};
  color: ${themeSelector.color('text')};
`

export const StyledRange = styled.div`
  display: flex;
  align-items: baseline;
  width: 100%;
`

export const StyledInput = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;

  ${({direction}) =>
    direction === 'row' &&
    css`
      flex-direction: row;
      gap: ${scale.space(-0.5)};
      align-items: center;

      * {
        font-size: ${scale.font(-0.52)};
      }
    `}
`
export const StyledBall = styled(Ball)`
  align-self: start;
  position: relative;
  top: ${({isAdmin}) => (isAdmin ? '2.5' : '8')}px;
`
