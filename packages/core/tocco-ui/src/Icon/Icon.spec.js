import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Icon from './Icon'

describe('tocco-ui', () => {
  describe('Icon', () => {
    test('should render an icon', () => {
      testingLibrary.renderWithIntl(<Icon icon="cog" />)

      expect(screen.queryByTestId('icon-cog')).to.exist
    })

    test('should render tocco icon', () => {
      testingLibrary.renderWithIntl(<Icon icon="tocco" />)

      expect(screen.queryByTestId('icon-logo')).to.exist
    })

    test('should render office icon', () => {
      testingLibrary.renderWithIntl(<Icon icon="office" />)

      expect(screen.queryByTestId('icon-office')).to.exist
    })
  })
})
