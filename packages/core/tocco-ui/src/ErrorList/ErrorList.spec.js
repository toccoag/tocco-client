import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ErrorList from './ErrorList'

describe('tocco-ui', () => {
  describe('ErrorList', () => {
    describe('ErrorList', () => {
      test('should render errors', async () => {
        const error = {
          error1: ['error1-1'],
          error2: ['error2-1', 'error2-2']
        }
        testingLibrary.renderWithIntl(<ErrorList error={error} />)
        await screen.findAllByTestId('icon')

        const errorList = screen.getByRole('list')
        expect(errorList.childNodes).to.have.length(3)
        expect(errorList.childNodes[0].textContent).to.eq('error1-1')
        expect(errorList.childNodes[1].textContent).to.eq('error2-1')
        expect(errorList.childNodes[2].textContent).to.eq('error2-2')
      })
    })
  })
})
