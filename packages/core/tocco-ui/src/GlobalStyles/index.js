import {GlobalAppClassName} from './constants'
import GlobalStyles from './GlobalStyles'

export {GlobalStyles as default, GlobalAppClassName}
