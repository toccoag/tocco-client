import PropTypes from 'prop-types'
import styled from 'styled-components'

import Icon from '../Icon'
import Typography from '../Typography'

import {GlobalClassNameStyles} from './GlobalClassNameStyles'

export default {
  title: 'Tocco-UI/GlobalClassNameStyles',
  component: GlobalClassNameStyles
}

const Box = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, 140px);
  grid-gap: 20px 10px;
  justify-content: center;
`

const IconContainer = styled.div`
  text-align: center;
  display: block;
  cursor: pointer;

  span {
    display: block;
  }
`

const Preview = ({className}) => (
  <IconContainer className={className}>
    <span>
      <Icon icon="circle-full" />
      <Icon icon="check-circle-full" />
    </span>
    <Typography.Span>{className}</Typography.Span>
  </IconContainer>
)
Preview.propTypes = {
  className: PropTypes.string
}

export const Showcase = () => (
  <div>
    <GlobalClassNameStyles />
    <Typography.H2>Global color class names</Typography.H2>
    <Box>
      <Preview className="text-success" />
      <Preview className="text-info" />
      <Preview className="text-danger" />
      <Preview className="text-warning" />
      <Preview className="text-muted" />
    </Box>
  </div>
)
