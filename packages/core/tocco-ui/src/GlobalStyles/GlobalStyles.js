import {GlobalDatePickerStyles} from '../DatePicker'

import {GlobalClassNameStyles} from './GlobalClassNameStyles'
import {GlobalScrollbar} from './GlobalScrollbar'

const GlobalStyles = () => {
  return (
    <>
      <GlobalClassNameStyles />
      <GlobalDatePickerStyles />
      <GlobalScrollbar />
    </>
  )
}

export default GlobalStyles
