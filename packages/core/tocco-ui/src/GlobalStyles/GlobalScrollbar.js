import {createGlobalStyle} from 'styled-components'

import {themeSelector} from '../utilStyles'

export const GlobalScrollbar = createGlobalStyle`
  /* Chrome, Edge, and Safari */
  ::-webkit-scrollbar {
    width: 6px;
    height: 6px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: ${themeSelector.color('backgroundScrollbar')};

    &:hover {
      background-color: ${themeSelector.color('backgroundScrollbarHover')};
    }
  }

  /* Firefox */
  * {
    scrollbar-color: ${themeSelector.color('backgroundScrollbar')} transparent; /* Firefox workaround */
    scrollbar-width: thin;
  }
`
