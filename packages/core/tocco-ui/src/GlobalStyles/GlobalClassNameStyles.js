import {createGlobalStyle} from 'styled-components'

import {themeSelector} from '../utilStyles'

export const GlobalClassNameStyles = createGlobalStyle`
  /****** Global text colors *****/
  
  /* usages: display expressions, freemarker, html content, icon styling, template snippets, ... */
  .text-success {
    color: ${themeSelector.color('signal.success')};
  }

  .text-info {
    color: ${themeSelector.color('signal.info')};
  }

  .text-warning {
    color: ${themeSelector.color('signal.warning')};
  }

  .text-danger {
    color: ${themeSelector.color('signal.danger')};
  }

  .text-muted {
    color: ${themeSelector.color('textDisabled')};
  }

  /***** Global text colors *****/

  /***** Template Snippets ******/

  /* light_cellrenderer */
  .light_cellrenderer {
    font-size: 1.6rem;
    display: inline-block;
    min-width: 1.7rem;
  }

  /***** Template Snippets *****/
`
