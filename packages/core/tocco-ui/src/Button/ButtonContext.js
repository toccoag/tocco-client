import PropTypes from 'prop-types'
import React, {useContext, useMemo, useEffect, useRef} from 'react'
import {react} from 'tocco-util'

export const LabelVisibility = {
  visible: 'visible',
  hidden: 'hidden',
  responsive: 'responsive'
}

const defaultValue = {labelVisibility: LabelVisibility.responsive}
const ButtonContext = React.createContext(defaultValue)

const HeightThreshold = 45

const ButtonContextProvider = ({children}) => {
  const widthThreshold = useRef(null)

  const [wrapperRef, contentRect] = react.useResizeObserver()

  /**
   * Keep the width of the threshold and only show full buttons again, when wrapper has reached threshold again.
   * This prevents from infinite toggling between icon only buttons and full buttons when height threshold has reached.
   */
  const isLabelHidden = !widthThreshold.current
    ? contentRect?.height > HeightThreshold
    : contentRect?.width < widthThreshold.current

  /**
   * Only change widthThreshold when `isLabelHidden` has changed.
   * `contentRect.width` should be ignored in the `deps`:
   *  - `isLabelHidden` will only change when `contentRect.width` has changed too
   *  - useEffect code should not run when only `contentReact.width` has changed without `isLabelHidden`
   */
  useEffect(() => {
    if (isLabelHidden) {
      widthThreshold.current = contentRect.width + 1
    } else {
      widthThreshold.current = null
    }
  }, [isLabelHidden]) // eslint-disable-line react-hooks/exhaustive-deps

  const value = useMemo(
    () => ({labelVisibility: isLabelHidden ? LabelVisibility.hidden : LabelVisibility.visible}),
    [isLabelHidden]
  )
  return <ButtonContext.Provider value={value}>{children(wrapperRef)}</ButtonContext.Provider>
}

ButtonContextProvider.propTypes = {
  children: PropTypes.func.isRequired
}

export const useButtonContext = () => useContext(ButtonContext)

export default ButtonContextProvider
