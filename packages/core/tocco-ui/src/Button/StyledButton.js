import styled from 'styled-components'

import {scale, themeSelector} from '../utilStyles'

import {buttonStyle} from './buttonStyle'

export const StyledLabelWrapper = styled.span``

const StyledButton = styled.button`
  ${buttonStyle}
`

export const StyledStickyButtonWrapper = styled.div`
  position: sticky;
  bottom: 0;
  padding-top: ${scale.space(0)};
  background-color: ${themeSelector.color('paper')};
  display: flex;
  justify-content: ${({position}) => (position === 'LEFT' ? 'flex-start' : 'flex-end')};
  z-index: 1;

  ${/* sc-selector */ StyledButton}:last-child {
    margin-right: 0;
  }
`

export default StyledButton
