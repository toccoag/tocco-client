import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {utils} from 'tocco-theme'

import {themeSelector} from '../utilStyles'

import {buttonStyle} from './buttonStyle'

const StyledRouterLinkButton = styled(Link).withConfig({shouldForwardProp: utils.isPropValid})`
  text-decoration: none;

  &:visited {
    color: ${themeSelector.color('text')}; /* nice2 reset */
  }

  &:hover,
  &:focus {
    text-decoration: none; /* nice2 reset */
  }
  ${buttonStyle}
`

export default StyledRouterLinkButton
