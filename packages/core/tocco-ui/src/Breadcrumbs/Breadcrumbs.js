import PropTypes from 'prop-types'
import {Helmet} from 'react-helmet'
import {js} from 'tocco-util'

import BreadcrumbItem from './BreadcrumbItem'
import BreadcrumbSeparator from './BreadcrumbSeparator'
import {StyledBreadcrumbs} from './StyledBreadcrumbs'

const getElementTitle = breadcrumb => breadcrumb.title || breadcrumb.display

export const getTitle = (breadcrumbsInfo, createTitle) => {
  if (!breadcrumbsInfo || breadcrumbsInfo.length === 0) {
    return 'Tocco'
  }

  const lastElement = breadcrumbsInfo.at(-1)
  let title = ''

  switch (lastElement.type) {
    case 'create':
      title = `${getElementTitle(lastElement)} - ${createTitle}`
      break
    case 'detail':
      title = breadcrumbsInfo
        .slice(breadcrumbsInfo.length - 2)
        .map(getElementTitle)
        .join(' - ')
      break
    default:
      title = getElementTitle(lastElement)
  }

  return js.adjustedHTMLString(title)
}

const homeBreadcrumb = {type: 'home', title: 'Tocco', display: 'Home', path: ''}
const Breadcrumbs = ({
  intl,
  pathPrefix = '',
  breadcrumbsInfo = [],
  currentView,
  backgroundColor,
  onClick,
  linkComp,
  updateDocumentTitle,
  showToccoHome
}) => {
  const breadcrumbs = [...breadcrumbsInfo, ...(currentView ? [currentView] : [])]

  if (breadcrumbs.length === 0) {
    return null
  }

  const msg = id => intl.formatMessage({id})

  const BreadcrumbList = breadcrumbs
    .map((breadcrumb, idx) => (
      <BreadcrumbItem
        key={`breadcrumbItem-${idx}`}
        pathPrefix={pathPrefix}
        breadcrumb={breadcrumb}
        isLast={idx === breadcrumbs.length - 1}
        linkComp={linkComp}
        onClick={onClick}
      />
    ))
    .reduce((acc, curr, idx) => [...acc, curr, <BreadcrumbSeparator key={`icon${idx}`} />], [])
    .slice(0, -1)

  return (
    <StyledBreadcrumbs backgroundColor={backgroundColor}>
      {updateDocumentTitle ? (
        <Helmet defer={false}>
          <title>{getTitle(breadcrumbs, msg('client.component.breadcrumbs.createTitle'))}</title>
        </Helmet>
      ) : null}
      <>
        {showToccoHome && (
          <>
            <BreadcrumbItem
              pathPrefix=""
              breadcrumb={homeBreadcrumb}
              isLast={breadcrumbs.length === 0}
              linkComp={linkComp}
              onClick={onClick}
            />
            <BreadcrumbSeparator />
          </>
        )}{' '}
        {BreadcrumbList}
      </>
    </StyledBreadcrumbs>
  )
}

Breadcrumbs.propTypes = {
  intl: PropTypes.object.isRequired,
  pathPrefix: PropTypes.string,
  match: PropTypes.object,
  updateDocumentTitle: PropTypes.bool,
  showToccoHome: PropTypes.bool,
  breadcrumbsInfo: PropTypes.arrayOf(
    PropTypes.shape({
      display: PropTypes.string,
      title: PropTypes.string,
      path: PropTypes.string,
      type: PropTypes.string,
      clickable: PropTypes.bool
    })
  ),
  currentView: PropTypes.shape({
    display: PropTypes.string,
    title: PropTypes.string
  }),
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  linkComp: PropTypes.any
}

export default Breadcrumbs
