import PropTypes from 'prop-types'

import Icon from '../Icon'

const typeIconMappings = {
  error: 'exclamation-circle',
  home: 'house-blank'
}

const getIconId = type => typeIconMappings[type] || type

const BreadcrumbIcon = ({type}) => {
  if (!type || type === 'record') {
    return null
  }

  return <Icon icon={getIconId(type)} />
}

BreadcrumbIcon.propTypes = {
  type: PropTypes.string
}

export default BreadcrumbIcon
