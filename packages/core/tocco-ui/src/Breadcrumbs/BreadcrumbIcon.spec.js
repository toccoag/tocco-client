import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import BreadcrumbIcon from './BreadcrumbIcon'

describe('tocco-ui', () => {
  describe('BreadcrumbIcon', () => {
    test('should render home icon', async () => {
      testingLibrary.renderWithIntl(<BreadcrumbIcon type="home" />)
      const homeIcon = await screen.findByTestId('icon-house-blank')
      expect(homeIcon).to.exist
    })
    test('should render error icon', async () => {
      testingLibrary.renderWithIntl(<BreadcrumbIcon type="error" />)
      const errorIcon = await screen.findByTestId('icon-exclamation-circle')
      expect(errorIcon).to.exist
    })
    test('should render type icon', async () => {
      const type = 'folder'
      testingLibrary.renderWithIntl(<BreadcrumbIcon type={type} />)
      const folderIcon = await screen.findByTestId(`icon-${type}`)
      expect(folderIcon).to.exist
    })
    test('should not render record type', () => {
      const {container} = testingLibrary.renderWithIntl(<BreadcrumbIcon type="record" />)
      expect(container.children).to.be.empty
    })
    test('should not render empty type', () => {
      const {container} = testingLibrary.renderWithIntl(<BreadcrumbIcon />)
      expect(container.children).to.be.empty
    })
  })
})
