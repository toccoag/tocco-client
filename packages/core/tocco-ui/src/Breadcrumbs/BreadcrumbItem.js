import PropTypes from 'prop-types'
import {html} from 'tocco-util'

import {AdminLink as StyledLink} from '../AdminLink'
import Typography from '../Typography'

import BreadcrumbIcon from './BreadcrumbIcon'
import {StyledBreadcrumbsLink, StyledBreadcrumbsTitle, StyledBreadcrumbWrapper} from './StyledBreadcrumbs'

const BreadcrumbItem = ({pathPrefix = '', breadcrumb, isLast, onClick, linkComp: LinkComp}) => {
  const handleClick = breadcrumbsItem => () => {
    if (onClick && breadcrumbsItem.clickable !== false) {
      onClick(breadcrumbsItem)
    }
  }

  const display = breadcrumb.display || ''
  const Breadcrumb = isLast ? StyledBreadcrumbsTitle : StyledBreadcrumbsLink
  const activeProp = isLast && {active: 'true'}
  const pathProp =
    typeof breadcrumb.path !== 'undefined' ? {to: `${pathPrefix}/${breadcrumb.path}`, state: breadcrumb.state} : {}
  const componentType = LinkComp ? <LinkComp /> : <StyledLink />

  return (
    <Typography.Span>
      <StyledBreadcrumbWrapper>
        <Breadcrumb neutral {...activeProp} {...pathProp} onClick={handleClick(breadcrumb)} component={componentType}>
          <BreadcrumbIcon type={breadcrumb.type} />
          <span data-cy="breadcrumb-item" dangerouslySetInnerHTML={{__html: html.sanitizeHtml(display)}} />
        </Breadcrumb>
      </StyledBreadcrumbWrapper>
    </Typography.Span>
  )
}

BreadcrumbItem.propTypes = {
  pathPrefix: PropTypes.string,
  isLast: PropTypes.bool,
  breadcrumb: PropTypes.shape({
    display: PropTypes.string,
    title: PropTypes.string,
    path: PropTypes.string,
    type: PropTypes.string,
    state: PropTypes.object,
    clickable: PropTypes.bool
  }),
  onClick: PropTypes.func,
  linkComp: PropTypes.any
}

export default BreadcrumbItem
