/* stylelint-disable no-descending-specificity */
import React from 'react'
import styled from 'styled-components'

import {StyledFontAwesomeAdapterWrapper} from '../Icon'
import {StyledSpan} from '../Typography'
import {theme, scale} from '../utilStyles'

export const StyledBreadcrumbs = styled.div`
  background-color: ${props => props.backgroundColor || theme.color('backgroundBreadcrumbs')(props)};
  padding: ${scale.space(-0.7)} ${scale.space(0.39)};
  position: relative;
  z-index: 2; /* higher than StyledTether to prevent cover on scroll */
`

export const StyledBreadcrumbsLink = styled(({component, ...props}) => React.cloneElement(component, props))`
  font-weight: ${theme.fontWeight('bold')};
  text-decoration: none;
  color: ${({active}) => active && theme.color('breadcrumbsActive')};
  cursor: pointer;

  & * {
    font-weight: ${theme.fontWeight('bold')};
    text-decoration: none;
    color: ${({active}) => active && theme.color('breadcrumbsActive')};
  }

  &:hover,
  &:hover * {
    color: ${theme.color('secondaryLight')};
  }

  &:focus,
  &:active,
  &:focus *,
  &:active * {
    color: ${theme.color('breadcrumbsActive')};
  }
`

// noinspection Stylelint
export const StyledBreadcrumbsTitle = styled.span`
  font-weight: ${theme.fontWeight('bold')};
  text-decoration: none;
  color: ${({active}) => active && theme.color('breadcrumbsActive')};

  & * {
    font-weight: ${theme.fontWeight('bold')};
    text-decoration: none;
    color: ${({active}) => active && theme.color('breadcrumbsActive')};
  }

  &:active * {
    color: ${theme.color('breadcrumbsActive')};
  }
`

export const StyledBreadcrumbSeparator = styled(StyledSpan)`
  margin-left: ${scale.space(-2)};
  margin-right: ${scale.space(-2)};
`

export const StyledBreadcrumbWrapper = styled.span`
  ${StyledFontAwesomeAdapterWrapper} {
    margin-right: ${scale.space(-1)};
  }
`
