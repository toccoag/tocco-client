import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SearchBox from './SearchBox'

describe('tocco-ui', () => {
  describe('SearchBox', () => {
    const TIMEOUT = 400
    const SEARCH_STRING = 'My Search String'

    test('should render', async () => {
      testingLibrary.renderWithIntl(<SearchBox onSearch={() => {}} />)
      await screen.findAllByTestId('icon')

      expect(screen.queryAllByRole('searchbox')).to.have.length(1)
    })

    test('should not call search function on keyDown events with live search but to short input', async () => {
      const searchFunc = sinon.spy()
      testingLibrary.renderWithIntl(<SearchBox onSearch={searchFunc} />)
      await screen.findAllByTestId('icon')

      const input = screen.getByRole('searchbox')
      fireEvent.change(input, {target: {value: 'a'}})

      setTimeout(() => {
        expect(searchFunc).to.not.have.been.called
      }, TIMEOUT)
    })

    test('should await debounce time onSearch', async () => {
      const searchFunc = sinon.spy()
      testingLibrary.renderWithIntl(<SearchBox onSearch={searchFunc} />)
      await screen.findAllByTestId('icon')

      const input = screen.getByRole('searchbox')
      fireEvent.change(input, {target: {value: SEARCH_STRING}})

      setTimeout(() => {
        expect(searchFunc).to.not.have.been.called
      }, 10)

      setTimeout(() => {
        expect(searchFunc).to.have.been.called
      }, TIMEOUT)
    })

    test('should accept an input value', async () => {
      const inputValue = 'TEST'
      testingLibrary.renderWithIntl(<SearchBox onSearch={() => {}} value={inputValue} />)
      await screen.findAllByTestId('icon')

      expect(screen.getByRole('searchbox').value).to.eql(inputValue)
    })
  })
})
