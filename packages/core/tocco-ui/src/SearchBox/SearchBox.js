import PropTypes from 'prop-types'
import React, {useState} from 'react'
import {react} from 'tocco-util'

import Ball from '../Ball'
import {StyledEditableWrapper} from '../EditableValue/StyledEditableValue'
import Icon from '../Icon'
import StatedValue from '../StatedValue'

import {StyledSearchBox, StyledSearchBoxInput, StyledSearchIconWrapper} from './StyledSearchBox'

const SearchBox = React.forwardRef(({value, minInputLength = 3, onSearch, placeholder, debounce = 200}, ref) => {
  const [inputValue, setInputValue] = useState(value || '')

  const onChange = e => {
    const newValue = e.target.value
    setInputValue(newValue)

    if (newValue.length === 0 || newValue.length >= minInputLength) {
      onSearch(newValue.trim())
    }
  }

  const resetSearch = () => {
    setInputValue('')
    onSearch('')
  }

  return (
    <StyledSearchBox>
      <StatedValue hasValue={Boolean(inputValue)} labelPosition="inside">
        <StyledEditableWrapper>
          <StyledSearchIconWrapper>
            <Icon icon="search" />
          </StyledSearchIconWrapper>
          <StyledSearchBoxInput
            data-cy="input-search-box"
            ref={ref}
            onChange={onChange}
            value={inputValue}
            aria-label={placeholder}
            placeholder={placeholder}
          />
          {inputValue?.length > 0 && <Ball data-cy="btn-reset" icon="times" onClick={resetSearch} />}
        </StyledEditableWrapper>
      </StatedValue>
    </StyledSearchBox>
  )
})

SearchBox.propTypes = {
  /**
   * Optional default value
   */
  value: PropTypes.string,
  /**
   * Function that will be triggered. The input value will be passed as argument.
   */
  onSearch: PropTypes.func.isRequired,
  /**
   * Placeholder of the input field.
   */
  placeholder: PropTypes.string,
  /**
   * onSearch is only called when the input has at least the length of minInputLength or 0
   */
  minInputLength: PropTypes.number,
  /**
   * debounce delay set in ms which controls how frequently onSearch is called
   */
  debounce: PropTypes.number
}

export default react.Debouncer(SearchBox, 100, 'onSearch')
