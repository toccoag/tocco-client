import styled from 'styled-components'

import {StyledInputCss} from '../EditableValue/StyledEditableValue'
import {scale, themeSelector} from '../utilStyles'

export const StyledSearchIconWrapper = styled.span`
  margin-right: ${scale.space(-0.8)};
  color: ${themeSelector.color('text')};
`

export const StyledSearchBox = styled.div`
  && {
    /* clears the ‘X’ from Chrome */
    input[type='search']::-webkit-search-decoration,
    input[type='search']::-webkit-search-cancel-button,
    input[type='search']::-webkit-search-results-button,
    input[type='search']::-webkit-search-results-decoration {
      display: none;
    }
  }
`

export const StyledSearchBoxInput = styled.input.attrs(props => ({
  type: 'search',
  placeholder: props.placeholder
}))`
  &&&& {
    ${StyledInputCss}
  }
`
