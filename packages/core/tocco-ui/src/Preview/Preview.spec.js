import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Preview from './Preview'

describe('tocco-ui', () => {
  describe('Preview', () => {
    test('render all elements and attributes', () => {
      testingLibrary.renderWithIntl(
        <Preview
          alt="alternative text"
          caption="caption text"
          srcUrl="/link-to-source"
          thumbnailUrl="/link-to-thumbnail"
        />
      )

      const link = screen.getByRole('link')
      expect(link.getAttribute('href')).to.be.eq('/link-to-source')
      expect(link.getAttribute('rel')).to.be.eq('noopener noreferrer')
      expect(link.getAttribute('target')).to.be.eq('_blank')

      const img = screen.getByRole('img')
      expect(img.getAttribute('alt')).to.be.eq('alternative text')
      expect(img.getAttribute('title')).to.be.eq('alternative text')
      expect(img.getAttribute('src')).to.be.eq('/link-to-thumbnail')
    })

    test('display image or icon depending on thumbnailUrl', async () => {
      const {rerender} = testingLibrary.renderWithIntl(
        <Preview alt="alt text" srcUrl="/link-to-source" thumbnailUrl="" />
      )

      expect(screen.queryAllByRole('img')).to.have.length(0)
      expect(await screen.findByTestId('icon-file-alt')).to.exist

      rerender(<Preview alt="alt text" srcUrl="/link-to-source" thumbnailUrl="/link-to-thumbnail" />)

      expect(screen.queryAllByRole('img')).to.have.length(1)
      expect(screen.queryByTestId('icon-file-alt')).to.not.exist
    })
  })
})
