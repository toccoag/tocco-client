import {statedValuePropTypes} from '../propTypes'
import StatedValue from '../StatedValue'

import {labelStyles, valueStyles} from './utils'

const AdminStatedValue = ({labelPosition, ...props}) => (
  <StatedValue
    styles={{
      labelStyles,
      valueStyles
    }}
    labelPosition={labelPosition || 'outside-responsive'}
    {...props}
  />
)

AdminStatedValue.propTypes = statedValuePropTypes

export default AdminStatedValue
