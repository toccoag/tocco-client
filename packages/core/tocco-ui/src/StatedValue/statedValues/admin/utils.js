import {css} from 'styled-components'

import {colorizeText, themeSelector} from '../../../utilStyles'

export const getLabelColor = ({isDisplay, immutable, signal, hasValue}) => {
  if (signal) {
    return 'signal'
  }
  if (hasValue) {
    return 'hasValue'
  }
  if (isDisplay) {
    return 'shade1'
  }
  if (immutable) {
    return 'base'
  }

  return 'shade1'
}

export const labelStyles = props => css`
  color: ${colorizeText[getLabelColor(props)](props)};
  font-weight: ${({dirty}) => (dirty ? themeSelector.fontWeight('bold') : themeSelector.fontWeight('regular'))};
`

export const valueStyles = () => css`
  padding-left: ${({immutable}) => immutable && 0};
`
