import {getLabelColor} from './utils'

describe('tocco-ui', () => {
  describe('StatedValue', () => {
    describe('statedValues', () => {
      describe('admin', () => {
        describe('utils', () => {
          const mapTextColorNames = [
            {
              input: {
                immutable: false,
                isDisplay: false,
                signal: 'success'
              },
              output: 'signal'
            },
            {
              input: {
                immutable: false,
                isDisplay: false,
                signal: undefined
              },
              output: 'shade1'
            },
            {
              input: {
                immutable: true,
                isDisplay: false,
                signal: 'success'
              },
              output: 'signal'
            },
            {
              input: {
                immutable: true,
                isDisplay: false,
                signal: undefined
              },
              output: 'base'
            },
            {
              input: {
                immutable: true,
                isDisplay: false,
                signal: 'success'
              },
              output: 'signal'
            },
            {
              input: {
                immutable: false,
                isDisplay: true,
                signal: 'success'
              },
              output: 'signal'
            },
            {
              input: {
                immutable: false,
                isDisplay: true,
                signal: undefined
              },
              output: 'shade1'
            },
            {
              input: {
                immutable: true,
                isDisplay: true,
                signal: 'success'
              },
              output: 'signal'
            },
            {
              input: {
                immutable: true,
                isDisplay: true,
                signal: undefined
              },
              output: 'shade1'
            }
          ]

          test('should get correct name of color for label text', () => {
            mapTextColorNames.forEach(item => {
              expect(getLabelColor(item.input)).to.be.equal(item.output)
            })
          })
        })
      })
    })
  })
})
