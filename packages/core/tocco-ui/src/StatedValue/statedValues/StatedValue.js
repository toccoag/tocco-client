import PropTypes from 'prop-types'
import FocusWithin from 'react-simple-focus-within'
import {js, html} from 'tocco-util'

import ErrorList from '../../ErrorList'
import {design} from '../../utilStyles'

import {statedValuePropTypes} from './propTypes'
import {
  StyledInputArea,
  StyledStatedValueBox,
  StyledStatedValueDescription,
  StyledStatedValueError,
  StyledStatedValueLabel,
  StyledStatedValueOuterWrapper,
  StyledStatedValueInnerWrapper
} from './StyledStatedValue'

const detectSignal = (dirty, hasError, mandatory, hasValue) => {
  if (hasError) {
    return design.condition.DANGER
  } else if (dirty) {
    return design.condition.INFO
  } else if (mandatory && !hasValue) {
    return design.condition.WARNING
  }
}

/**
 * Wrap <EditableValue> and <FormattedValUe> in <StatedValue> to describe it
 * and signal conditions.
 */
const StatedValue = ({
  children,
  description,
  dirty,
  isDisplay,
  error,
  hasValue,
  id,
  label,
  mandatory,
  mandatoryTitle,
  immutable,
  touched,
  signal: signalProp,
  labelPosition,
  styles,
  formField
}) => {
  const showError = !immutable && touched && error && Object.keys(error).length > 0
  const optionalMandatoryTitle = mandatory && mandatoryTitle ? `, ${mandatoryTitle}` : ''
  const labelAlt = `${js.adjustedHTMLString(label)}${optionalMandatoryTitle}`
  const signal = signalProp || detectSignal(dirty, showError, mandatory, hasValue)
  const statedValueLabelText = {__html: `${html.sanitizeHtml(label)}${mandatory ? ' *' : ''}`}
  const forAttr = !isDisplay && !immutable && {htmlFor: id}

  const isUploadComponent = ['binary', 'document', 'image', 'upload', 'named-upload', 'pseudo-multi-document'].includes(
    formField?.dataType
  )

  return (
    <FocusWithin>
      {({getRef}) => (
        <StyledStatedValueOuterWrapper labelPosition={labelPosition}>
          <StyledStatedValueInnerWrapper
            labelPosition={labelPosition}
            ref={getRef}
            label={label}
            styles={styles?.innerWrapperStyles}
            as={isUploadComponent ? 'span' : undefined}
          >
            {label && (
              <StyledStatedValueLabel
                {...forAttr}
                hasValue={hasValue}
                dirty={dirty}
                title={labelAlt}
                immutable={immutable}
                isDisplay={isDisplay}
                signal={signal}
                labelPosition={labelPosition}
                styles={styles?.labelStyles}
              >
                <span dangerouslySetInnerHTML={statedValueLabelText} />
              </StyledStatedValueLabel>
            )}
            <StyledInputArea labelPosition={labelPosition}>
              <StyledStatedValueBox
                hasValue={hasValue}
                immutable={immutable}
                isDisplay={isDisplay}
                signal={signal}
                labelPosition={labelPosition}
                styles={styles?.valueStyles}
              >
                {children}
              </StyledStatedValueBox>
              {showError && (
                <StyledStatedValueError data-cy={`errorlist-${id}`}>
                  <ErrorList error={error} />
                </StyledStatedValueError>
              )}
              {description && <StyledStatedValueDescription>{description}</StyledStatedValueDescription>}
            </StyledInputArea>
          </StyledStatedValueInnerWrapper>
        </StyledStatedValueOuterWrapper>
      )}
    </FocusWithin>
  )
}

StatedValue.propTypes = {
  ...statedValuePropTypes,
  labelPosition: statedValuePropTypes.labelPosition.isRequired,
  styles: PropTypes.shape({
    innerWrapperStyles: PropTypes.func,
    labelStyles: PropTypes.func,
    valueStyles: PropTypes.func
  })
}

export default StatedValue
