import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import StatedValue from './StatedValue'
import {getBorderColor} from './StyledStatedValue'

/* eslint-disable react/prop-types */
jest.mock('./StyledStatedValue', () => {
  const original = jest.requireActual('./StyledStatedValue')

  return {
    ...original,
    StyledStatedValueLabel: props => (
      <div data-testid="styled-stated-value-label">
        <div>{props.signal}</div>
        <div>{props.title}</div>
        <div>{props.children}</div>
        <div>{props.htmlFor}</div>
      </div>
    )
  }
})
/* eslint-enable react/prop-types */

describe('tocco-ui', () => {
  describe('StatedValue', () => {
    test('should show content', () => {
      testingLibrary.renderWithIntl(
        <StatedValue labelPosition="outside">
          <input key="StatedValueContent" label="Label" />
        </StatedValue>
      )
      expect(screen.queryAllByRole('textbox')).to.have.length(1)
    })

    test('should not have content', () => {
      testingLibrary.renderWithIntl(<StatedValue label="Label" labelPosition="outside" />)
      expect(screen.queryAllByRole('textbox')).to.have.length(0)
    })

    test('should show description', () => {
      testingLibrary.renderWithIntl(<StatedValue description="description" label="Label" labelPosition="outside" />)
      expect(screen.queryAllByText('description')).to.have.length(1)
    })

    test('should not have description', () => {
      testingLibrary.renderWithIntl(<StatedValue labelPosition="outside" />)
      expect(screen.queryAllByText('description')).to.have.length(0)
    })

    test('should detect condition dirty', () => {
      testingLibrary.renderWithIntl(<StatedValue dirty={true} label="Label" labelPosition="outside" />)
      expect(screen.getByTestId('styled-stated-value-label').childNodes[0].textContent).to.eq('info')
    })

    test('should not detect condition error if not touched', () => {
      testingLibrary.renderWithIntl(<StatedValue error={{error: ['error']}} label="Label" labelPosition="outside" />)
      expect(screen.getByTestId('styled-stated-value-label').childNodes[0].textContent).to.eq('')
    })

    test('should detect condition error if touched', async () => {
      testingLibrary.renderWithIntl(
        <StatedValue error={{error: ['error']}} touched={true} label="Label" labelPosition="outside" />
      )
      expect(await screen.findByTestId('icon-times')).to.exist
      expect(screen.getByTestId('styled-stated-value-label').childNodes[0].textContent).to.eq('danger')
    })

    test('condition error should overrule condition dirty', async () => {
      testingLibrary.renderWithIntl(
        <StatedValue dirty={true} error={{error: ['error']}} touched={true} label="Label" labelPosition="outside" />
      )
      expect(await screen.findByTestId('icon-times')).to.exist
      expect(screen.getByTestId('styled-stated-value-label').childNodes[0].textContent).to.eq('danger')
    })

    test('should list errors', async () => {
      testingLibrary.renderWithIntl(
        <StatedValue
          dirty={true}
          error={{error1: ['error 1-1', 'error 1-2'], error2: ['error 2']}}
          touched={true}
          labelPosition="outside"
        />
      )
      expect(await screen.findAllByTestId('icon-times')).to.have.length(3)
      const list = screen.getByRole('list')
      expect(list.childNodes).to.have.length(3)
      expect(list.childNodes[0].textContent).to.eq('error 1-1')
      expect(list.childNodes[1].textContent).to.eq('error 1-2')
      expect(list.childNodes[2].textContent).to.eq('error 2')
    })

    test('should show label', () => {
      testingLibrary.renderWithIntl(<StatedValue label="label a" labelPosition="outside" />)
      expect(screen.getByTestId('styled-stated-value-label').childNodes[2].textContent).to.eq('label a')
    })

    test('should add alt attribute to label', () => {
      const {rerender} = testingLibrary.renderWithIntl(
        <StatedValue label="firstname" mandatoryTitle="input is required" labelPosition="outside" />
      )
      expect(screen.getByTestId('styled-stated-value-label').childNodes[1].textContent).to.eq('firstname')

      rerender(
        <StatedValue label="lastname" mandatory={true} mandatoryTitle="input is required" labelPosition="outside" />
      )
      expect(screen.getByTestId('styled-stated-value-label').childNodes[1].textContent).to.eq(
        'lastname, input is required'
      )
    })

    test('should extend label if mandatory', () => {
      testingLibrary.renderWithIntl(<StatedValue label="label b" mandatory={true} labelPosition="outside" />)
      expect(screen.getByTestId('styled-stated-value-label').childNodes[2].textContent).to.eq('label b *')
    })

    test('should use prop id for htmlFor on label', () => {
      testingLibrary.renderWithIntl(<StatedValue id="target-element" label="Label" labelPosition="outside" />)
      expect(screen.getByTestId('styled-stated-value-label').childNodes[3].textContent).to.eq('target-element')
    })

    const mapBorderColorNames = [
      {
        input: {
          immutable: false,
          isDisplay: false,
          signal: 'success'
        },
        output: 'signal'
      },
      {
        input: {
          immutable: false,
          isDisplay: false,
          signal: undefined
        },
        output: 'base'
      },
      {
        input: {
          immutable: false,
          isDisplay: false,
          signal: 'success'
        },
        output: 'signal'
      },
      {
        input: {
          immutable: false,
          isDisplay: false,
          signal: undefined
        },
        output: 'base'
      },
      {
        input: {
          immutable: true,
          isDisplay: false,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: false,
          signal: undefined
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: false,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: false,
          signal: undefined
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: false,
          isDisplay: true,
          signal: undefined
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: false,
          isDisplay: true,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: false,
          isDisplay: true,
          signal: undefined
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: false,
          isDisplay: true,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: true,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: true,
          signal: undefined
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: true,
          signal: 'success'
        },
        output: 'transparent'
      },
      {
        input: {
          immutable: true,
          isDisplay: true,
          signal: undefined
        },
        output: 'transparent'
      }
    ]

    test('should get correct name of color for borders', () => {
      mapBorderColorNames.forEach(item => {
        expect(getBorderColor(item.input)).to.be.equal(item.output)
      })
    })
  })
})
