import styled, {css} from 'styled-components'

import {StyledHtmlFormatter} from '../../FormattedValue/formatters/StyledComponents'
import {colorizeBorder, declareFocus, declareFont, scale, themeSelector} from '../../utilStyles'

const borderWidth = '1.1px' // deliberately uneven to force correct rendering in chrome

export const getBorderColor = ({immutable, isDisplay, signal, isDirty}) => {
  if (isDirty) {
    return 'isDirty'
  }
  if (isDisplay || immutable) {
    return 'transparent'
  }
  if (signal) {
    return 'signal'
  }

  return 'base'
}

const declareCursor = ({isDisplay, immutable}) => `cursor: ${!isDisplay && immutable ? 'text' : 'auto'};`

export const StyledStatedValueLabel = styled.span`
  &&& {
    display: flex;
    width: 40%;
    flex-grow: 1;
    flex-basis: 125px;
    ${declareFont({
      lineHeight: themeSelector.lineHeight('dense')
    })}
    background-color: ${themeSelector.color('paper')};
    pointer-events: auto;
    ${props => declareCursor(props)}
    overflow-wrap: anywhere;
    white-space: pre-wrap;
    ${({labelPosition}) =>
      labelPosition === 'outside' &&
      css`
        margin-bottom: ${scale.space(-1.5)};
        display: block;
        width: auto;
      `}
    ${({labelPosition}) =>
      labelPosition === 'inside' &&
      css`
        flex-basis: unset;
        position: absolute;
        top: 0;
        z-index: 1;
        display: inline-block;

        /* if immutable remove left padding */
        padding: 0 ${scale.space(-2)} ${scale.space(-3.5)} ${({immutable}) => immutable && '0'};
        width: fit-content;
        left: ${({immutable}) => (immutable ? '0' : scale.space(-1.8))};
        white-space: nowrap; /* prevent multiline labels since they cover the input content */
        overflow: hidden;
        text-overflow: ellipsis;
        max-width: 100%;
      `}
    ${({styles}) => styles}
  }
`

export const StyledInputArea = styled.div`
  width: 60%;
  flex-grow: 1;
  ${({labelPosition}) =>
    labelPosition === 'outside' &&
    css`
      width: auto;
    `}
`

export const StyledStatedValueBox = styled.div`
  &&& {
    position: relative;
    height: fit-content; /* prevent height getting bigger due to tall labels */
    border: ${borderWidth} solid ${props => colorizeBorder[getBorderColor(props)](props)};
    border-radius: ${themeSelector.radii('form')};
    padding: ${scale.space(-2.5)} ${scale.space(-1.5)};
    display: flex;
    ${props => !props.immutable && declareFocus(props)}
    ${props => declareCursor(props)}
    ${declareFont()}

    > ${StyledHtmlFormatter} {
      display: inline-block;
      max-width: 100%;

      p:first-of-type {
        margin-top: 0;
      }
    }
    ${({labelPosition}) =>
      labelPosition === 'inside' &&
      css`
        padding: ${scale.space(-1)} ${scale.space(-1.7)} ${scale.space(-2)} ${scale.space(-1)};
      `}
    ${({styles}) => styles}
  }
`

export const StyledStatedValueDescription = styled.p`
  &&& {
    ${declareFont({
      fontSize: scale.font(-1)
    })}
  }
`

export const StyledStatedValueError = styled.div``

export const StyledStatedValueOuterWrapper = styled.div`
  &:not(:last-child) {
    margin-bottom: ${scale.space(-0.7)};
  }
  ${({labelPosition}) =>
    labelPosition === 'inside' &&
    css`
      padding-top: ${scale.space(-1.5)};
      margin-bottom: 0.65rem;
      position: relative;
    `}
`

export const StyledStatedValueInnerWrapper = styled.label`
  &&& {
    display: flex;
    flex-wrap: wrap;
    gap: ${scale.space(-1.5)};
    align-items: center;

    ${/* sc-selector */ StyledStatedValueBox},
    ${/* sc-selector */ StyledStatedValueDescription},
    ${/* sc-selector */ StyledStatedValueError} {
      margin-bottom: ${scale.space(-2)};

      &:last-child {
        margin-bottom: 0;
      }
    }

    ${/* sc-selector */ StyledStatedValueDescription} {
      margin-left: calc(${scale.space(-1)} + ${borderWidth});
    }
    ${({labelPosition}) =>
      labelPosition === 'outside' &&
      css`
        display: block;
      `}
    ${({labelPosition}) =>
      labelPosition === 'inside' &&
      css`
        padding-top: 0;
      `}
    ${({styles}) => styles}
  }
`
