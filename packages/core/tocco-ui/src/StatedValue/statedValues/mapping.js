import {env} from 'tocco-util'

import StatedValue from './admin'
import WidgetStatedValue from './widget'

export const mapping = {
  [env.EmbedType.admin]: StatedValue,
  [env.EmbedType.widget]: WidgetStatedValue
}
