import PropTypes from 'prop-types'

export const statedValuePropTypes = {
  /**
   * A component to enter or display data.
   */
  children: PropTypes.node,
  /**
   * Visualize as display and not as field.
   */
  isDisplay: PropTypes.bool,
  /**
   * A helper text to instruct users.
   */
  description: PropTypes.string,
  /**
   * If true field is marked as changed.
   */
  dirty: PropTypes.bool,
  /**
   * Error object.
   */
  error: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.node, PropTypes.string]))),
  /**
   * Force label on secondaryPosition.
   */
  fixLabel: PropTypes.bool,
  /**
   * If true label is moved to uncover value.
   */
  hasValue: PropTypes.bool,
  /**
   * Focus target id by clicking on label.
   */
  id: PropTypes.string,
  /**
   * Describe editable value briefly.
   */
  label: PropTypes.string,
  /**
   * Defines whether the form field labels should be inside or outside (with or without responsiveness) of the field.
   * The posisiton 'outside-responsive' will show the label left on wide screen and on top on small screens.
   */
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  /**
   * If true an asterisk is appended to the label.
   */
  mandatory: PropTypes.bool,
  /**
   * Append to label's alternative text.
   */
  mandatoryTitle: PropTypes.string,
  /**
   * Determines if value is editable
   */
  immutable: PropTypes.bool,
  /**
   * Pass a valid signal condition to omit auto detection
   */
  signal: PropTypes.string,
  /**
   * If true field was in focus.
   */
  touched: PropTypes.bool,
  formField: PropTypes.shape({
    dataType: PropTypes.string
  })
}
