import {statedValuePropTypes} from '../propTypes'
import StatedValue from '../StatedValue'

import {innerWrapperStyles, labelStyles, valueStyles} from './utils'

const WidgetStatedValue = ({labelPosition, ...props}) => (
  <StatedValue
    styles={{
      innerWrapperStyles,
      labelStyles,
      valueStyles
    }}
    labelPosition={labelPosition || 'outside-responsive'}
    {...props}
  />
)

WidgetStatedValue.propTypes = statedValuePropTypes

export default WidgetStatedValue
