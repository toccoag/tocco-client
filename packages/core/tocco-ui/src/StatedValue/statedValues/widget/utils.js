import {css} from 'styled-components'

import {colorizeText, scale, themeSelector} from '../../../utilStyles'

const getLabelColor = ({signal}) => {
  if (signal) {
    return 'signal'
  }

  return 'shade2'
}

export const innerWrapperStyles = () => css`
  align-items: flex-start;
`

export const labelColor = props => colorizeText[getLabelColor(props)](props)

export const labelFontWeight = () => themeSelector.fontWeight('bold')

export const labelStyles = props => css`
  padding-top: ${({immutable, labelPosition}) => {
    if (labelPosition === 'inside') {
      return 0
    }

    if (immutable) {
      return css`
        ${scale.space(-1.8)}
      `
    }

    return css`
      ${scale.space(0.1)}
    `
  }};
  color: ${colorizeText[getLabelColor(props)](props)};
`

export const valueStyles = () => css`
  padding-top: ${({isDisplay, immutable, labelPosition}) => {
    if (immutable && labelPosition !== 'inside') {
      return 0
    }
    if (isDisplay && immutable) {
      return css`
        ${scale.space(-1)}
      `
    }
  }};
  padding-left: ${({immutable}) => immutable && 0};
`
