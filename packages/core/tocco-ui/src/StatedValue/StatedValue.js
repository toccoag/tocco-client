import {env} from 'tocco-util'

import {mapping} from './statedValues/mapping'

const StatedValue = env.embedStrategyFactory({mapping})

export default StatedValue
