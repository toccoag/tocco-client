/* eslint-disable react/prop-types */

import {storybookHtmlMarkup} from '../util/storybookHtmlMarkup'

import FormattedValue from './'
import {map} from './FormatterProvider'
export default {
  title: 'Tocco-UI/FormattedValue',
  component: FormattedValue,
  argTypes: {
    type: {
      control: 'select',
      options: Object.keys(map)
    }
  }
}

export const BooleanValue = args => <FormattedValue {...args} />
BooleanValue.args = {value: true, type: 'boolean'}
BooleanValue.argTypes = {value: {control: 'boolean'}}
BooleanValue.storyName = 'Boolean'

export const DateTimeValue = args => <FormattedValue {...args} />
DateTimeValue.args = {value: Date.now(), type: 'datetime'}
DateTimeValue.argTypes = {value: {control: 'date'}}
DateTimeValue.storyName = 'Date Time'

export const DescriptionValue = ({mode, title, ...args}) => <FormattedValue {...args} options={{mode, title}} />
DescriptionValue.args = {value: 'This is a <b>strong</b> text', type: 'description', mode: 'tooltip', title: 'Title'}
DescriptionValue.argTypes = {
  mode: {control: 'radio', options: ['tooltip', 'text']},
  title: {control: 'text'},
  value: {control: 'text'}
}
DescriptionValue.storyName = 'Description'

export const DocumentValue = args => <FormattedValue {...args} />
DocumentValue.args = {
  value: {
    alt: 'a classic car parked in nature',
    binaryLink: 'https://picsum.photos/1000/1000?image=1070',
    caption: 'car parked nature',
    fileName: 'nature_car.jpg',
    thumbnailLink: 'https://picsum.photos/400/400?image=1070'
  },
  type: 'document',
  options: {}
}
DocumentValue.argTypes = {value: {control: 'object'}}
DocumentValue.storyName = 'Document'

export const DocumentCompactValue = args => <FormattedValue {...args} />
DocumentCompactValue.args = {
  value: {
    alt: 'a classic car parked in nature',
    binaryLink: 'https://picsum.photos/1000/1000?image=1070',
    caption: 'car parked nature',
    fileName: 'nature_car.jpg',
    thumbnailLink: 'https://picsum.photos/400/400?image=1070'
  },
  type: 'document-compact',
  options: {}
}
DocumentCompactValue.argTypes = {value: {control: 'object'}}
DocumentCompactValue.storyName = 'Document Compact'

export const DurationValue = args => <FormattedValue {...args} />
DurationValue.args = {value: 83456678, type: 'duration'}
DurationValue.argTypes = {
  value: {control: 'number', description: 'Duration in milliseconds'}
}
DurationValue.storyName = 'Duration'

export const HtmlValue = args => <FormattedValue {...args} />
HtmlValue.args = {value: storybookHtmlMarkup, type: 'html'}
HtmlValue.storyName = 'Html'

export const IntegerValue = args => <FormattedValue {...args} />
IntegerValue.args = {value: 87660000, type: 'integer'}
IntegerValue.argTypes = {
  value: {control: 'number'}
}
IntegerValue.storyName = 'Integer'

export const RemoteValue = args => (
  <FormattedValue
    {...args}
    options={{
      DetailLink: ({entityKey, children}) => (
        <a href={`/${entityKey}`} target="_blank" rel="noopener noreferrer">
          {children}
        </a>
      )
    }}
  />
)
RemoteValue.args = {
  value: {key: '1', display: 'apple'},
  type: 'remote'
}
RemoteValue.argTypes = {
  value: {control: 'object'}
}
RemoteValue.storyName = 'Remote'

export const MultiRemoteValue = args => (
  <FormattedValue
    {...args}
    options={{
      DetailLink: ({entityKey, children}) => (
        <a href={`/${entityKey}`} target="_blank" rel="noopener noreferrer">
          {children}
        </a>
      )
    }}
  />
)
MultiRemoteValue.args = {
  value: [
    {key: '1', display: 'apple'},
    {key: '2', display: 'khaki'}
  ],
  type: 'multi-remote'
}
MultiRemoteValue.argTypes = {
  value: {control: 'object'}
}
MultiRemoteValue.storyName = 'Multi Remote'

export const MultiSelectValue = args => <FormattedValue {...args} />
MultiSelectValue.args = {
  value: [
    {key: '3', display: 'Matterhorn'},
    {key: '4', display: 'Jungfraujoch'},
    {key: '5', display: 'Rigi'}
  ],
  type: 'multi-select'
}
MultiSelectValue.argTypes = {
  value: {
    control: 'object'
  }
}
MultiSelectValue.storyName = 'Multi Select'

export const NumberValue = args => <FormattedValue {...args} />
NumberValue.args = {value: 876600.01, type: 'number'}
NumberValue.argTypes = {value: {control: 'number'}}
NumberValue.storyName = 'Number'

export const PercentValue = args => <FormattedValue {...args} />
PercentValue.args = {value: 0.6589, type: 'percent'}
PercentValue.argTypes = {value: {control: 'number'}}
PercentValue.storyName = 'Percent'

export const PhoneValue = args => <FormattedValue {...args} />
PhoneValue.args = {value: '+41761234567', type: 'phone'}
PhoneValue.argTypes = {value: {control: 'text'}}
PhoneValue.storyName = 'Phone'

export const StringValue = args => <FormattedValue {...args} />
StringValue.args = {value: 'Test String', type: 'string'}
StringValue.argTypes = {value: {control: 'text'}}
StringValue.storyName = 'String'

export const TextValue = args => <FormattedValue {...args} />
TextValue.args = {value: 'Line1 \nLine2', type: 'text'}
TextValue.argTypes = {value: {control: 'text'}}
TextValue.storyName = 'Text'

export const TimeValue = args => <FormattedValue {...args} />
TimeValue.args = {value: '03:21:23.123Z', type: 'time'}
TimeValue.argTypes = {value: {control: 'text'}}
TimeValue.storyName = 'Time'

export const UrlValue = args => <FormattedValue {...args} />
UrlValue.args = {value: 'https://www.tocco.ch', type: 'url'}
UrlValue.argTypes = {value: {control: 'text'}}
UrlValue.storyName = 'Url'
