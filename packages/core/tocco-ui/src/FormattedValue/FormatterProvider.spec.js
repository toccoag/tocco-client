import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import FormatterProvider from './FormatterProvider'

// eslint-disable-next-line react/prop-types
jest.mock('./formatters/StringFormatter', () => props => <div data-testid="string">{props.value.toString()}</div>)
jest.mock('./formatters/DocumentFormatter', () => () => <div data-testid="document"></div>)

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('FormatterProvider', () => {
      test('should render a string input and set props', () => {
        testingLibrary.renderWithIntl(<FormatterProvider componentType="string" value="test" />)
        expect(screen.getByTestId('string').textContent).to.eql('test')
      })

      test('should return empty div in case of unknown type', () => {
        const {container} = testingLibrary.renderWithIntl(<FormatterProvider componentType="unknown" value="test" />)
        expect(container.innerHTML).to.eql('<div></div>')
      })
    })
  })
})
