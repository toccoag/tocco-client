import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import FormattedValue from './FormattedValue'

/* eslint-disable react/prop-types */
jest.mock('./formatters/StringFormatter', () => props => <div data-testid="string">{props.value.toString()}</div>)
jest.mock('./formatters/DateFormatter', () => props => <div data-testid="date">{props.value.toString()}</div>)
jest.mock('./formatters/BooleanFormatter', () => props => <div data-testid="boolean">{props.value.toString()}</div>)
/* eslint-enable react/prop-types */

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    test('should render a string value', () => {
      testingLibrary.renderWithIntl(<FormattedValue type="string" value="test" />)
      expect(screen.getByTestId('string').textContent).to.eql('test')
    })

    test('should render a date value', () => {
      testingLibrary.renderWithIntl(<FormattedValue type="date" value="1976-03-16" />)
      expect(screen.getByTestId('date').textContent).to.eql('1976-03-16')
    })

    test('should return an empty span on a undefined input', () => {
      const {container} = testingLibrary.renderWithIntl(<FormattedValue type="string" value={undefined} />)
      expect(container.innerHTML).to.eql('<span></span>')
    })

    test('should return an empty span on empty input', () => {
      const {container} = testingLibrary.renderWithIntl(<FormattedValue type="date" value="" />)
      expect(container.innerHTML).to.eql('<span></span>')
    })

    test('should return an empty span on input "null"', () => {
      const {container} = testingLibrary.renderWithIntl(<FormattedValue type="string" value={null} />)
      expect(container.innerHTML).to.eql('<span></span>')
    })

    test('should not return an empty span on input false', () => {
      testingLibrary.renderWithIntl(<FormattedValue type="boolean" value={false} />)
      expect(screen.getByTestId('boolean').textContent).to.eql('false')
    })
  })
})
