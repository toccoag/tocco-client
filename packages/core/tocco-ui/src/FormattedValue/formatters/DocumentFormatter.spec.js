import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DocumentFormatter from './DocumentFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DocumentFormatter ', () => {
        test('should render link and image', async () => {
          const {container} = testingLibrary.renderWithTheme(
            <DocumentFormatter
              value={{
                alt: 'alt text',
                binaryLink: 'binary url',
                caption: 'caption text',
                fileName: 'file name',
                thumbnailLink: 'thumbnail url'
              }}
              options={{
                downloadText: 'download'
              }}
            />
          )
          await screen.findAllByTestId('icon')

          expect(container.querySelectorAll('img').length).to.equal(1)
          expect(screen.queryByRole('button')).to.exist
        })
      })
    })
  })
})
