import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import BooleanFormatter from './BooleanFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('BooleanFormatter ', () => {
        test('should format true value', async () => {
          testingLibrary.renderWithTheme(<BooleanFormatter value={true} />)

          expect(await screen.findByTestId('icon-check')).to.exist
        })

        test('should format falsy value', async () => {
          testingLibrary.renderWithTheme(<BooleanFormatter value={false} />)

          expect(await screen.findByTestId('icon-times')).to.exist
        })
      })
    })
  })
})
