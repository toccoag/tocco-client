import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import TimeFormatter from './TimeFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('TimeFormatter ', () => {
        const timeOutputIso = '23:15:00.000'
        const timeOutputEn = '23:15'
        const timeOutputDe = '23:15'

        const timeValue = '23:15'

        test('should format value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <TimeFormatter value={timeValue} />
            </IntlProvider>
          )
          expect(screen.queryByText(timeOutputEn)).to.exist
          expect(screen.queryByTitle(timeOutputEn)).to.exist
          jestExpect(container.querySelector(`time[dateTime="${timeOutputIso}"]`)).toBeInTheDocument()
        })

        test('should format value according to locale', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <TimeFormatter value={timeValue} />
            </IntlProvider>
          )
          expect(screen.queryByText(timeOutputDe)).to.exist
          expect(screen.queryByTitle(timeOutputDe)).to.exist
          jestExpect(container.querySelector(`time[dateTime="${timeOutputIso}"]`)).toBeInTheDocument()
        })
      })
    })
  })
})
