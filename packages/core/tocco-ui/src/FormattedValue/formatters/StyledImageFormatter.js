import styled from 'styled-components'

import {themeSelector} from '../../utilStyles'

export const StyledImage = styled.img`
  max-width: 100%;
  max-height: 100%;
  width: 200px;
  border: 1px solid ${themeSelector.color('border')};
  border-radius: ${themeSelector.radii('form')};
`
