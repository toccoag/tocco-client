import PropTypes from 'prop-types'
import {FormattedDate, useIntl} from 'react-intl'

import Typography from '../../Typography'
import {matchesIsoDate} from '../util/DateUtils'

export const DateFormatter = props => {
  const intl = useIntl()
  return (
    <Typography.Time dateTime={props.value} title={intl.formatDate(props.value)}>
      <FormattedDate value={props.value} year="numeric" month="2-digit" day="2-digit" timeZone="UTC" id={props.id} />
    </Typography.Time>
  )
}

DateFormatter.propTypes = {
  value: (props, propName, componentName) => {
    if (!matchesIsoDate(props[propName])) {
      return new Error(`Invalid prop '${propName}' supplied to ${componentName}.`)
    }
  },
  id: PropTypes.string
}

export default DateFormatter
