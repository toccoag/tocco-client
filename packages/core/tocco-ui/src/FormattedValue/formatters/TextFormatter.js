import PropTypes from 'prop-types'

import Typography from '../../Typography'

const interweaveNewLines = value =>
  value.split('\n').flatMap((v, index, array) => (index + 1 < array.length ? [v, <br key={index} />] : v))

const TextFormatter = props => {
  if (!props.breakWords) {
    return (
      <Typography.Span breakWords={false} useTitle={props.useTitle} id={props.id}>
        {props.value.replace('/\n/g', ' ')}
      </Typography.Span>
    )
  }

  return (
    <Typography.P breakWords={true} useTitle={props.useTitle} id={props.id}>
      {interweaveNewLines(props.value)}
    </Typography.P>
  )
}

TextFormatter.propTypes = {
  value: PropTypes.string,
  breakWords: PropTypes.bool,
  useTitle: PropTypes.bool,
  id: PropTypes.string
}

export default TextFormatter
