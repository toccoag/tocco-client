import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import DateTimeFormatter from './DateTimeFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DateTimeFormatter ', () => {
        test('should handle null value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <DateTimeFormatter value={null} />
            </IntlProvider>
          )

          expect(container.querySelectorAll('time').length).to.equal(0)
        })

        test('should handle invalid value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <DateTimeFormatter value="some invalid text" />
            </IntlProvider>
          )

          expect(container.querySelectorAll('time').length).to.equal(0)
        })

        test('should format value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <DateTimeFormatter value="1976-03-16T12:00:00.000Z" />
            </IntlProvider>
          )

          expect(screen.queryByText('03/16/1976, 13:00')).to.exist
          expect(screen.queryByTitle('3/16/1976, 13:00')).to.exist
          jestExpect(container.querySelector('time[dateTime="1976-03-16T12:00:00.000Z"]')).toBeInTheDocument()
        })

        test('should format value according to locale', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <DateTimeFormatter value="1976-03-16T12:00:00.000Z" />
            </IntlProvider>
          )

          expect(screen.queryByText('16.03.1976, 13:00')).to.exist
          expect(screen.queryByTitle('16.3.1976, 13:00')).to.exist
          jestExpect(container.querySelector('time[dateTime="1976-03-16T12:00:00.000Z"]')).toBeInTheDocument()
        })

        test('should recognize alternative datetime formats', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <DateTimeFormatter value={1594562400000} />
            </IntlProvider>
          )

          expect(screen.queryByText('12.07.2020, 16:00')).to.exist
          expect(screen.queryByTitle('12.7.2020, 16:00')).to.exist
          jestExpect(container.querySelector('time[dateTime="2020-07-12T14:00:00.000Z"]')).toBeInTheDocument()
        })
      })
    })
  })
})
