import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import DateFormatter from './DateFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DateFormatter ', () => {
        const dateInput = '1976-11-16'
        const dateOutputIso = '1976-11-16'
        const dateOutputEn = '11/16/1976'
        const dateOutputDe = '16.11.1976'

        test('should format value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <DateFormatter value={dateInput} />
            </IntlProvider>
          )
          expect(screen.queryByText(dateOutputEn)).to.exist
          expect(screen.queryByTitle(dateOutputEn)).to.exist
          jestExpect(container.querySelector(`time[dateTime="${dateOutputIso}"]`)).toBeInTheDocument()
        })

        test('should format value according to locale', () => {
          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <DateFormatter value={dateInput} />
            </IntlProvider>
          )
          expect(screen.queryByText(dateOutputDe)).to.exist
          expect(screen.queryByTitle(dateOutputDe)).to.exist
          jestExpect(container.querySelector(`time[dateTime="${dateOutputIso}"]`)).toBeInTheDocument()
        })
      })
    })
  })
})
