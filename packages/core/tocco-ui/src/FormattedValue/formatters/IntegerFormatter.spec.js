import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import IntegerFormatter from './IntegerFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('IntegerFormatter ', () => {
        test('should show value unformatted', () => {
          testingLibrary.renderWithIntl(<IntegerFormatter value={99999} />)
          expect(screen.getByText('99999')).exist
        })
      })
    })
  })
})
