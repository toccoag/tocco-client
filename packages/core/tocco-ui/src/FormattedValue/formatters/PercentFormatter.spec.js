import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import PercentFormatter from './PercentFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('PercentFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <PercentFormatter value={0.0241} options={{postPointDigits: 2}} />
            </IntlProvider>
          )
          expect(screen.getByText('2.41%')).exist
        })

        test('should respect postDecimalDigits', () => {
          testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <PercentFormatter value={0.0241} options={{postPointDigits: 5}} />
            </IntlProvider>
          )
          expect(screen.getByText('2.41000%')).exist
        })

        test('should format value according to locale', () => {
          testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <PercentFormatter value={0.999} options={{postPointDigits: 2}} />
            </IntlProvider>
          )
          expect(screen.getByText('99,90 %')).exist
        })
      })
    })
  })
})
