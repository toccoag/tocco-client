/* eslint-disable react/prop-types */
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SingleSelectFormatter from './SingleSelectFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('SingleSelectFormatter ', () => {
        test('should format value', () => {
          const value = {key: '3', display: 'Selected'}
          testingLibrary.renderWithTheme(<SingleSelectFormatter value={value} />)
          expect(screen.getByText('Selected')).exist
        })

        test('should render Link if factory provided', () => {
          const value = {key: '3', display: 'Selected'}
          const {container} = testingLibrary.renderWithTheme(
            <SingleSelectFormatter
              value={value}
              options={{navigationStrategy: {DetailLink: ({key, children}) => <a>{children}</a>}}}
            />
          )
          expect(container.querySelectorAll('a').length).to.equal(1)
        })
      })
    })
  })
})
