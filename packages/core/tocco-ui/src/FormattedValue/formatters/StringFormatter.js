import PropTypes from 'prop-types'

import Typography from '../../Typography'

const StringFormatter = props => (
  <Typography.Span breakWords={props.breakWords} useTitle={props.useTitle} id={props.id}>
    {props.value.toString()}
  </Typography.Span>
)

StringFormatter.propTypes = {
  value: PropTypes.any,
  breakWords: PropTypes.bool,
  useTitle: PropTypes.bool,
  id: PropTypes.string
}

export default StringFormatter
