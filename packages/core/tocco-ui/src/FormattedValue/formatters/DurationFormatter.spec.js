import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import DurationFormatter from './DurationFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DurationFormatter ', () => {
        test('should format value with seconds', () => {
          const ms = 83000

          const duration = '00:01:23.000'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="de-CH">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })
        test('should format value with seconds & milliseconds', () => {
          const ms = 63123

          const duration = '00:01:03.123'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="de-CH">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })

        test('should format value w/o seconds', () => {
          const ms = 120000

          const duration = '00:02'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="de-CH">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })

        test('should format value in en', () => {
          const ms = 120000

          const duration = '00:02'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })

        test('should format value in fr-CH', () => {
          const ms = 83123

          const duration = '00:01:23.123'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="fr-CH">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })

        test('should format value in it-CH', () => {
          const ms = 83123

          const duration = '00:01:23.123'

          testingLibrary.renderWithTheme(
            <IntlProvider locale="it-CH">
              <DurationFormatter value={ms} />
            </IntlProvider>
          )

          expect(screen.getByText(duration)).exist
        })
      })
    })
  })
})
