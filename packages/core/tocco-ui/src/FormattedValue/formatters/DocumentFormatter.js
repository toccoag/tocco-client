import PropTypes from 'prop-types'
import {Upload} from 'tocco-ui'

/*
 * Reuse upload component and set immutable to true that it has the same look as the EditableValue Document
 * if immutable is true
 */
const DocumentFormatter = props => (
  <Upload
    id={props.id}
    onUpload={() => {}}
    immutable={true}
    textResources={{
      download: props.options.downloadText
    }}
    value={props.value ? props.value : null}
  />
)

DocumentFormatter.propTypes = {
  id: PropTypes.string,
  value: PropTypes.shape({
    alt: PropTypes.string,
    binaryLink: PropTypes.string.isRequired,
    caption: PropTypes.string,
    fileName: PropTypes.string.isRequired,
    thumbnailLink: PropTypes.string.isRequired
  }).isRequired,
  options: PropTypes.shape({
    downloadText: PropTypes.string
  })
}

export default DocumentFormatter
