import {screen} from '@testing-library/react'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import NumberFormatter from './NumberFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('NumberFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(
            <IntlProvider locale="en">
              <NumberFormatter value={1.3} />
            </IntlProvider>
          )
          expect(screen.getByText('1.30')).exist
        })

        test('should format value accorind to locale', () => {
          testingLibrary.renderWithTheme(
            <IntlProvider locale="de">
              <NumberFormatter value={1.3} />
            </IntlProvider>
          )
          expect(screen.getByText('1,30')).exist
        })
      })
    })
  })
})
