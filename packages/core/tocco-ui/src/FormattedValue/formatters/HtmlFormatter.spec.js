import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import HtmlFormatter from './HtmlFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('HtmlFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(<HtmlFormatter value="<p>TEST TEST</p>" options={{escapeHtml: false}} />)
          expect(screen.getByText('TEST TEST')).exist
        })

        test('should escape value', () => {
          testingLibrary.renderWithTheme(<HtmlFormatter value="<p>TEST TEST</p>" options={{escapeHtml: true}} />)
          expect(screen.getByText('<p>TEST TEST</p>')).exist
        })
      })
    })
  })
})
