import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import EmailFormatter from './EmailFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('typeFormatters', () => {
      describe('EmailFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(<EmailFormatter value="support@tocco.ch" />)
          expect(screen.getByText('support@tocco.ch')).exist
        })
      })
    })
  })
})
