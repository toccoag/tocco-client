/* eslint-disable react/prop-types */
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import MultiSelectFormatter from './MultiSelectFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('MultiSelectFormatter ', () => {
        test('should format value', () => {
          const value = [
            {key: '3', display: 'Selected'},
            {key: '4', display: 'Selected2'}
          ]
          testingLibrary.renderWithTheme(<MultiSelectFormatter value={value} breakWords={true} />)
          expect(screen.getByText('Selected; Selected2')).exist
        })

        test('should render Links if factory provided', () => {
          const value = [
            {key: '3', display: 'Selected'},
            {key: '4', display: 'Selected2'}
          ]
          const {container} = testingLibrary.renderWithTheme(
            <MultiSelectFormatter
              value={value}
              breakWords={true}
              options={{navigationStrategy: {DetailLink: ({key, children}) => <a>{children}</a>}}}
            />
          )
          expect(container.querySelectorAll('a').length).to.equal(2)
        })
      })
    })
  })
})
