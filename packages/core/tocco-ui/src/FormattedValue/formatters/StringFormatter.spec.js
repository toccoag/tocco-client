import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import StringFormatter from './StringFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('StringFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(<StringFormatter value="TEST TEST" />)
          expect(screen.getByText('TEST TEST')).exist
        })

        test('should format number 0', () => {
          testingLibrary.renderWithTheme(<StringFormatter value={0} />)
          expect(screen.getByText('0')).exist
        })
      })
    })
  })
})
