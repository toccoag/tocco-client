import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import UrlFormatter from './UrlFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('UrlFormatter ', () => {
        test('should format value', () => {
          testingLibrary.renderWithTheme(<UrlFormatter value="https://www.tocco.ch" />)
          const link = screen.getByRole('link')
          jestExpect(link).toHaveAttribute('href', 'https://www.tocco.ch')
        })
      })
    })
  })
})
