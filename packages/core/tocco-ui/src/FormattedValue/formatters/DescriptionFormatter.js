import PropTypes from 'prop-types'

import Icon from '../../Icon'
import Popover from '../../Popover'
import Typography from '../../Typography'

import HtmlFormatter from './HtmlFormatter'
import {StyledIconWrapper} from './StyledComponents'

const DescriptionFormatter = props => {
  const options = props.options || {}
  const content = (
    <>
      {options.title && <Typography.H5>{options.title}</Typography.H5>}
      <HtmlFormatter {...props} />
    </>
  )

  if (options.mode === 'text') {
    return content
  }

  return (
    <Popover content={content} isPlainHtml={true}>
      <StyledIconWrapper>
        <Icon icon="question-circle" />
      </StyledIconWrapper>
    </Popover>
  )
}

DescriptionFormatter.propTypes = {
  value: PropTypes.string,
  breakWords: PropTypes.bool,
  options: PropTypes.shape({
    mode: PropTypes.oneOf(['text', 'tooltip']),
    title: PropTypes.string
  })
}

export default DescriptionFormatter
