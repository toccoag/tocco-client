import _get from 'lodash/get'
import PropTypes from 'prop-types'
import {Fragment} from 'react'
import {html} from 'tocco-util'

import Popover from '../../Popover'
import Typography from '../../Typography'

export const MultiSeparator = () => '; '

const MultiSelectFormatter = ({value: values, options, breakWords, useTitle}) => {
  const {navigationStrategy, linkProps, tooltips, loadTooltip} = options || {}

  const mapValueToDisplay = value => {
    if (navigationStrategy?.DetailLink) {
      const tooltip = _get(tooltips, value.key, null)
      return (
        <span onClick={e => e.stopPropagation()} key={value.key}>
          <navigationStrategy.DetailLink {...linkProps} entityKey={value.key}>
            <Popover content={tooltip ? <div dangerouslySetInnerHTML={{__html: html.sanitizeHtml(tooltip)}} /> : null}>
              <span onMouseOver={() => loadTooltip && !tooltip && loadTooltip(value.key)}>{value.display}</span>
            </Popover>
          </navigationStrategy.DetailLink>
        </span>
      )
    }

    return <Fragment key={value.key}>{value.display}</Fragment>
  }

  const displays =
    values?.length > 0
      ? values.map(mapValueToDisplay).reduce((prev, curr, idx) => [prev, <MultiSeparator key={`sep${idx}`} />, curr])
      : null

  return (
    <Typography.Span breakWords={breakWords} useTitle={useTitle}>
      {displays}
    </Typography.Span>
  )
}

MultiSelectFormatter.propTypes = {
  value: PropTypes.array,
  options: PropTypes.shape({
    navigationStrategy: PropTypes.object,
    linkProps: PropTypes.object,
    tooltips: PropTypes.objectOf(PropTypes.string),
    loadTooltip: PropTypes.func
  }),
  breakWords: PropTypes.bool,
  useTitle: PropTypes.bool
}

export default MultiSelectFormatter
