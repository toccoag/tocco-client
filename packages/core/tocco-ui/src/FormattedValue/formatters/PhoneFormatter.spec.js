import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import LibPhoneFormatter from './LibPhoneFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('PhoneFormatter ', () => {
        test('should format valid phone numbers', () => {
          testingLibrary.renderWithTheme(<LibPhoneFormatter value="+41443886000" />)
          expect(screen.getByText('+41 44 388 60 00')).exist
        })

        test('should show original string if its not a valid phone number', () => {
          const invalidPhoneNumber = '+41 4438860011111110'
          testingLibrary.renderWithTheme(<LibPhoneFormatter value={invalidPhoneNumber} />)
          expect(screen.getByText(invalidPhoneNumber)).exist
        })
      })
    })
  })
})
