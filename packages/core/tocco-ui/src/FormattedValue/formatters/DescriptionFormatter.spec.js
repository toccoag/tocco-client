import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DescriptionFormatter from './DescriptionFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DescriptionFormatter ', () => {
        test('should show question mark in tooltip mode', async () => {
          testingLibrary.renderWithIntl(
            <DescriptionFormatter
              value="<p>TEST TEST</p>"
              options={{
                mode: 'tooltip',
                title: 'Test'
              }}
            />
          )

          expect(await screen.findByTestId('icon-question-circle')).to.exist
        })

        test('should format value', async () => {
          testingLibrary.renderWithIntl(
            <DescriptionFormatter
              value="<p>TEST TEST</p>"
              options={{
                mode: 'text',
                title: 'Title!'
              }}
            />
          )

          expect(screen.getByText('Title!')).exist
          expect(screen.getByText('TEST TEST')).exist
        })
      })
    })
  })
})
