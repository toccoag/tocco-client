import PropTypes from 'prop-types'
import {html} from 'tocco-util'

import {StyledHtmlFormatter} from './StyledComponents'
import TextFormatter from './TextFormatter'

const HtmlFormatter = props => {
  const {id, value, breakWords, className, options = {}} = props
  if (options.escapeHtml) {
    return <TextFormatter {...props} />
  }

  return (
    <StyledHtmlFormatter
      id={id}
      dangerouslySetInnerHTML={{__html: html.sanitizeHtml(value)}}
      breakWords={breakWords}
      className={className}
    />
  )
}

HtmlFormatter.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string,
  breakWords: PropTypes.bool,
  className: PropTypes.string,
  options: PropTypes.shape({
    escapeHtml: PropTypes.bool
  })
}

export {HtmlFormatter as default}
