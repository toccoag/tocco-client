import PropTypes from 'prop-types'

import Link from '../../Link'

const UrlFormatter = ({value, breakWords, useTitle}) => (
  <Link
    onClick={e => {
      e.stopPropagation()
    }}
    href={value}
    target="_blank"
    rel="noopener noreferrer"
    label={value}
    breakWords={breakWords}
    useTitle={useTitle}
    neutral={false}
  />
)

UrlFormatter.propTypes = {
  value: PropTypes.string,
  breakWords: PropTypes.bool,
  useTitle: PropTypes.bool
}

export default UrlFormatter
