import PropTypes from 'prop-types'

import {StyledImage} from './StyledImageFormatter'

const ImageFormatter = ({value}) => {
  return (
    <StyledImage
      src={value.binaryLink}
      srcSet={`${value.thumbnailLink} 250w, ${value.binaryLink} 400w`}
      sizes={'(max-width: 250px) 125px, 200px'}
      alt={value.fileName}
      title={value.fileName}
    />
  )
}

ImageFormatter.propTypes = {
  value: PropTypes.shape({
    fileName: PropTypes.string.isRequired,
    binaryLink: PropTypes.string.isRequired,
    thumbnailLink: PropTypes.string
  }).isRequired
}

export default ImageFormatter
