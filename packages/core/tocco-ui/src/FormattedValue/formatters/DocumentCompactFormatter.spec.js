import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DocumentCompactFormatter from './DocumentCompactFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('DocumentCompactFormatter ', () => {
        test('should render link but no image', async () => {
          const {container} = testingLibrary.renderWithTheme(
            <DocumentCompactFormatter
              value={{
                alt: 'alt text',
                binaryLink: 'binary url',
                fileName: 'file name',
                thumbnailLink: 'link'
              }}
            />
          )
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('link')).to.have.length(2)
          const links = screen.getAllByRole('link')
          jestExpect(links[0]).toHaveAttribute('alt', 'alt text')
          jestExpect(links[0]).toHaveAttribute('href', 'binary url')
          jestExpect(links[1]).toHaveAttribute('alt', 'alt text')
          jestExpect(links[1]).toHaveAttribute('href', 'binary url?download=true')

          expect(container.querySelectorAll('img').length).to.equal(0)
        })
      })
    })
  })
})
