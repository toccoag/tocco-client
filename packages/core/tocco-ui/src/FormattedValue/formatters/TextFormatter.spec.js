import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import TextFormatter from './TextFormatter'

describe('tocco-ui', () => {
  describe('FormattedValue', () => {
    describe('formatters', () => {
      describe('TextFormatter ', () => {
        test('should format value', () => {
          const {container} = testingLibrary.renderWithTheme(
            <TextFormatter breakWords={true} value={'Lorem\n\nipsum\n'} />
          )
          expect(container.querySelectorAll('p').length).to.equal(1)
          expect(container.querySelectorAll('br').length).to.equal(3)
          expect(screen.getByText('Lorem', {exact: false})).exist
          expect(screen.getByText('ipsum', {exact: false})).exist
        })

        test('should format value in single paragraph', () => {
          const {container} = testingLibrary.renderWithTheme(
            <TextFormatter breakWords={false} value={'Lorem\nipsum'} />
          )
          expect(container.querySelectorAll('span').length).to.equal(1)
          expect(screen.getByText('Lorem ipsum')).exist
        })
      })
    })
  })
})
