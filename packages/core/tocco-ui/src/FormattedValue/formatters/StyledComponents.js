import styled, {css} from 'styled-components'

import {declareTypograhpy, StyledSpan} from '../../Typography'
import {scale, themeSelector} from '../../utilStyles'

export const StyledIconWrapper = styled.span`
  color: ${themeSelector.color('text')};
  font-size: ${scale.font(1)};
  margin-left: -${scale.space(-2)};
`

const baseLabelStyles = css`
  display: inline-block;
  color: ${themeSelector.color('text')}; /* nice2 reset */
  line-height: ${themeSelector.lineHeight('regular')}; /* nice2 reset */
  text-align: left; /* nice2 reset */
  vertical-align: unset; /* nice2 reset */
  font-size: 100% !important; /* nice2 reset */
  font-weight: ${themeSelector.fontWeight('regular')} !important; /* nice2 reset */
  padding: ${scale.space(-3)} ${scale.space(-1.5)} !important; /* nice2 reset */
  white-space: normal !important; /* nice2 reset */
  border-radius: ${themeSelector.radii('statusLabel')};
`

export const StyledHtmlFormatter = styled(StyledSpan)`
  &&& {
    ${props => declareTypograhpy(props, 'html')}
    margin: 0 0 ${scale.space(-1)};

    svg {
      margin-right: ${scale.space(-1.5)};
    }

    &:last-child {
      margin-bottom: 0;
    }

    .label {
      ${baseLabelStyles}
    }

    .label-danger {
      background-color: ${themeSelector.color('signal.dangerLight')} !important; /* nice2 reset */
      color: ${themeSelector.color('textLabel')};
    }

    .label-info {
      background-color: ${themeSelector.color('signal.infoLight')} !important; /* nice2 reset */
      color: ${themeSelector.color('textLabel')};
    }

    .label-warning {
      background-color: ${themeSelector.color('signal.warningLight')} !important; /* nice2 reset */
      color: ${themeSelector.color('textLabel')};
    }

    .label-success {
      background-color: ${themeSelector.color('signal.successLight')} !important; /* nice2 reset */
      color: ${themeSelector.color('textLabel')};
    }

    .label-neutral {
      ${baseLabelStyles}
      background-color: ${themeSelector.color('signal.neutral')} !important; /* nice2 reset */
      color: ${themeSelector.color('textLabel')};
    }

    .label-status {
      border-radius: ${themeSelector.radii('statusLabel')};
      padding: ${scale.space(-2.1)} ${scale.space(0)};

      &.filled {
        color: ${themeSelector.color('paper')};
        background-color: ${themeSelector.color('signal.info')};
      }
    }

    .label-attention {
      background-color: ${themeSelector.color('signal.warningLight')} !important; /* nice2 reset */
      padding: ${scale.space(-1.5)} ${scale.space(0.5)} !important;

      a {
        text-decoration: underline;
        color: #000; /* same color in all themes */
      }
    }
  }
`
