import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SignalList from './'

describe('tocco-ui', () => {
  describe('SignalList', () => {
    test('should render children', () => {
      testingLibrary.renderWithTheme(
        <SignalList.List>
          <span>SpanItem</span>
          <span>SpanItem</span>
        </SignalList.List>
      )
      expect(screen.queryAllByText('SpanItem')).to.have.length(2)
    })
  })
})
