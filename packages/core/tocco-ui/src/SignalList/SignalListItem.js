import PropTypes from 'prop-types'
import React from 'react'

import Icon from '../Icon'
import {design} from '../utilStyles'

import {StyledSignalListIcon, StyledSignalListItem} from './StyledComponents'

const ICONS = {
  [design.condition.BASE]: false,
  [design.condition.DANGER]: 'times',
  [design.condition.SUCCESS]: 'check',
  [design.condition.WARNING]: 'exclamation-triangle',
  [design.condition.INFO]: 'info'
}

const getIcon = condition => {
  const icon = ICONS[condition]

  if (icon) {
    return <Icon icon={icon} position={design.position.SOLE} hasFixedWidth={false} />
  }

  return <i>{'\u2022'}</i>
}

/**
 * Signalize single condition by icon and color. It must be wrapped by <SignalList/>
 */
const SignalListItem = ({condition = design.condition.BASE, label, children, ...props}) => (
  <StyledSignalListItem condition={condition} data-cy={props['data-cy']}>
    <StyledSignalListIcon>{getIcon(condition)}</StyledSignalListIcon>
    {label}
    {React.Children.map(children, child => React.cloneElement(child))}
  </StyledSignalListItem>
)

SignalListItem.propTypes = {
  /**
   * Visible text. Default is an empty string.
   */
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node,
  /**
   * Color and icon is set according condition. Default value is 'base'.
   * Possible values: base|danger|primary|success|warning
   */
  condition: PropTypes.oneOf(Object.values(design.condition)),
  'data-cy': PropTypes.string
}

export default SignalListItem
