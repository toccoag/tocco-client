import styled from 'styled-components'

import {StyledLi, StyledUl} from '../Typography'
import {design, themeSelector, scale} from '../utilStyles'

const COLORS = {
  [design.condition.BASE]: themeSelector.color('text'),
  [design.condition.DANGER]: themeSelector.color('signal.danger'),
  [design.condition.SUCCESS]: themeSelector.color('signal.success'),
  [design.condition.WARNING]: themeSelector.color('signal.warning'),
  [design.condition.INFO]: themeSelector.color('signal.info')
}

const getColor = ({condition}) => COLORS[condition]

export const StyledSignalList = styled(StyledUl)`
  && {
    list-style-type: none;
    margin-left: 0;
  }
`

export const StyledSignalListIcon = styled.span`
  margin-left: -1px;
  margin-right: ${scale.space(-2)};
`

export const StyledSignalListItem = styled(StyledLi)`
  && {
    color: ${props => getColor(props)};
    position: relative;
    padding: 0;
  }
`
