import {Link} from 'react-router-dom'
import styled from 'styled-components'
import {utils} from 'tocco-theme'

import {themeSelector} from '../utilStyles'

import {linkStyle} from './linkStyle'

export default styled(Link).withConfig({
  shouldForwardProp: propName => utils.isPropValid(propName) || propName === 'state'
})`
  &:link,
  &:visited {
    color: ${themeSelector.color('text')}; /* nice2 reset */
  }
  ${linkStyle}
`
