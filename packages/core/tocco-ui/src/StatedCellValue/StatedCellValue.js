import PropTypes from 'prop-types'

import ErrorList from '../ErrorList'

import {StyledCell, StyledStatedValueBox} from './StyledComponents'

const StatedCellValue = ({children, id, error, borderless}) => {
  const showError = error && Object.keys(error).length > 0

  return (
    <StyledCell>
      <StyledStatedValueBox borderless={borderless} hasError={showError}>
        {children}
      </StyledStatedValueBox>

      {showError && (
        <div data-cy={`errorlist-${id}`}>
          <ErrorList error={error} />
        </div>
      )}
    </StyledCell>
  )
}

StatedCellValue.propTypes = {
  children: PropTypes.node.isRequired,
  id: PropTypes.string,
  error: PropTypes.object,
  borderless: PropTypes.bool
}

export default StatedCellValue
