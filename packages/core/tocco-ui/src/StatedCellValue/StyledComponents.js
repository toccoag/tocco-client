import _get from 'lodash/get'
import styled, {css} from 'styled-components'

import {StyledReactSelectOuterWrapper} from '../Select'
import {declareFocus, scale, themeSelector} from '../utilStyles'

const borderColor = ({theme}) => _get(theme, 'colors.border')
const borderErrorColor = ({theme}) => _get(theme, 'colors.signal.danger')

export const StyledCell = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

export const StyledStatedValueBox = styled.div`
  width: 100%;
  border: 1px solid ${({hasError}) => (hasError ? borderErrorColor : borderColor)};
  border-radius: ${themeSelector.radii('form')};
  ${({borderless}) => css`
    ${!borderless && declareFocus}
    ${borderless &&
    css`
      border: none;
    `}
  `}
  ${StyledReactSelectOuterWrapper} {
    padding: ${scale.space(-2)} ${scale.space(-1.7)} ${scale.space(-2)} ${scale.space(-1)};
    border-radius: ${themeSelector.radii('form')};
    background-color: ${themeSelector.color('paper')};
  }

  button {
    background-color: transparent;
  }

  &&&&& {
    input,
    textarea {
      padding-left: ${scale.space(-1)};
      padding-right: ${scale.space(-1)};

      &:not([disabled]):hover {
        cursor: pointer;
      }
    }

    .react-datepicker-wrapper {
      margin-right: ${scale.space(-0.5)};
    }

    input.react-datepicker__input {
      padding-left: ${scale.space(-1)};
      padding-right: 0;
    }
  }
`
