import {act, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import LoadMask from './LoadMask'

describe('tocco-ui', () => {
  describe('LoadMask', () => {
    test('shows spinner if an object is falsy', async () => {
      testingLibrary.renderWithIntl(<LoadMask required={[undefined, undefined, true]} />)

      expect(await screen.findByTestId('icon-circle-notch')).to.exist
    })

    test('shows children if loaded', () => {
      testingLibrary.renderWithIntl(
        <LoadMask required={[{}]}>
          <div data-testid="test123"></div>
        </LoadMask>
      )
      expect(screen.getByTestId('test123')).to.exist
    })

    test('shows spinner until promis is resolved', async () => {
      const promise = Promise.resolve({})

      const content = (
        <LoadMask promises={[promise]}>
          <div data-testid="content" />
        </LoadMask>
      )

      const {rerender} = testingLibrary.renderWithIntl(content)

      expect(screen.queryAllByTestId('content')).to.have.length(0)

      await act(async () => {
        await promise
      })
      rerender(content)

      expect(screen.queryAllByTestId('content')).to.have.length(1)
    })

    test('shows loading-text if set', () => {
      const {rerender} = testingLibrary.renderWithIntl(<LoadMask required={[undefined]} loadingText="Lorem ipsum" />)
      expect(screen.queryAllByText('Lorem ipsum')).to.have.length(1)

      rerender(<LoadMask required={[undefined]} />)
      expect(screen.queryAllByText('Lorem ipsum')).to.have.length(0)
    })

    test('should be able to switch between loading and not loading', () => {
      const content = <div data-testid="content" />
      const {rerender} = testingLibrary.renderWithIntl(<LoadMask required={[true]}>{content}</LoadMask>)
      expect(screen.queryAllByTestId('content')).to.have.length(1)

      rerender(<LoadMask required={[false]}>{content}</LoadMask>)
      expect(screen.queryAllByTestId('content')).to.have.length(0)
    })
  })
})
