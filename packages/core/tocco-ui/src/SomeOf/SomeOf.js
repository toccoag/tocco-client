import PropTypes from 'prop-types'

import Typography from '../Typography'
/**
 * Use <SomeOf> to display an amount in relation to a total.
 */
const SomeOf = ({some = 0, of}) => (
  <Typography.Span>
    {some} / {of}
  </Typography.Span>
)

SomeOf.propTypes = {
  /**
   * Any number from 0 to a total. Default value is 0.
   */
  some: PropTypes.number,
  /**
   * A number representing a total.
   */
  of: PropTypes.number.isRequired
}

export default SomeOf
