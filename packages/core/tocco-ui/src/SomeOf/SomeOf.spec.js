import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SomeOf from './SomeOf'

describe('tocco-ui', () => {
  describe('SomeOf', () => {
    test('should display values', () => {
      testingLibrary.renderWithIntl(<SomeOf some={12} of={345} />)
      expect(screen.queryAllByText('12 / 345')).to.have.length(1)
    })

    test('should have a default value for "some"', () => {
      testingLibrary.renderWithIntl(<SomeOf of={345} />)
      expect(screen.queryAllByText('0 / 345')).to.have.length(1)
    })
  })
})
