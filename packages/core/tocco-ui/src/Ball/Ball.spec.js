import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Ball from './Ball'

describe('tocco-ui', () => {
  describe('Button', () => {
    test('should handle click events', async () => {
      const onButtonClick = sinon.spy()
      testingLibrary.renderWithTheme(<Ball icon="chevron-right" onClick={onButtonClick} />)
      await screen.findAllByTestId('icon')
      fireEvent.click(screen.getByRole('button'))
      expect(onButtonClick).to.have.been.calledOnce
    })
  })
})
