import styled, {css} from 'styled-components'

import {declareFont, themeSelector} from '../utilStyles'

const StyledEditableWrapperCss = css`
  align-items: center;
  cursor: ${({immutable}) => immutable && 'text'};
  display: flex;
  width: 100%;
`

const StyledEditableWrapper = styled.label`
  &&& {
    margin: 0; /* reset Bootstrap and Ext JS */
    padding: 0; /* reset Ext JS */
    ${StyledEditableWrapperCss}
  }
`

const StyledInputCss = css`
  background-color: transparent;
  border: 0;
  cursor: ${({immutable}) => immutable && 'text'};
  flex-grow: 1;
  min-height: ${({immutable}) => (immutable ? '0' : '2.6rem')};
  min-width: 0;
  outline: 0;
  padding: 0;
  ${() =>
    declareFont({
      color: themeSelector.color('text')
    })}

  &::-ms-clear {
    display: none;
  }

  &::-webkit-clear-button {
    display: none;
  }

  &::-webkit-inner-spin-button {
    display: none;
  }

  &[type='number'] {
    appearance: textfield;
  }

  &[type='search'] {
    appearance: none;
  }

  &:disabled {
    -webkit-text-fill-color: ${themeSelector.color('text')}; /* Safari fix */
    opacity: 1; /* iOS fix */
  }

  /* remove indicators */
  &[type='date'],
  &[type='datetime-local'],
  &[type='month'],
  &[type='time'],
  &[type='week'] {
    appearance: none;
    display: flex;

    /* reset margin on smaller screens */
    ::-webkit-date-and-time-value {
      ${({immutable}) => immutable && 'margin: unset;'}
    }
  }

  /* allow pointer event only on touch devices */
  @media (pointer: coarse) {
    pointer-events: ${({immutable}) => (immutable ? 'none' : 'auto')};
  }
`

const StyledEditableControlCss = css`
  display: flex;
  align-items: center;
`

const StyledEditableControl = styled.div`
  &&& {
    ${StyledEditableControlCss}
  }
`

export const StyledEditableValue = styled.span`
  width: 100%;

  && {
    [type='url'] + .input-group-addon > a {
      color: inherit; /* reset bootstrap default */
    }

    [type='tel'] + .input-group-addon > a {
      color: inherit; /* reset bootstrap default */
    }

    .date-edit {
      .input-group-addon {
        cursor: pointer;
      }

      input {
        z-index: 0;
      }

      &.disabled {
        input {
          cursor: text;
        }
      }

      .right-addon {
        position: relative;
      }

      .right-addon input {
        padding-right: 30px;
      }

      .reset {
        position: absolute;
        padding-right: 10px;
        padding-top: 5px;
        right: 0;
        color: #999;
        font-size: 18px;
        font-style: normal;
        cursor: pointer;
      }

      .reset:hover {
        color: ${themeSelector.color('signal.danger')};
      }
    }
  }
`

export {
  StyledEditableControl,
  StyledEditableControlCss,
  StyledEditableValue as default,
  StyledEditableWrapper,
  StyledEditableWrapperCss,
  StyledInputCss
}
