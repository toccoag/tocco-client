import {act, fireEvent, screen} from '@testing-library/react'
import {expect} from 'chai'
import {testingLibrary} from 'tocco-test-util'

import IntegerEdit from './IntegerEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('IntegerEdit ', () => {
        let clock

        beforeEach(() => {
          clock = sinon.useFakeTimers()
        })

        afterEach(() => {
          clock.restore()
        })

        test('should render IntegerEdit', () => {
          testingLibrary.renderWithIntl(<IntegerEdit value={12345} options={{}} onChange={EMPTY_FUNC} />)
          expect(screen.getByRole('textbox')).to.exist
        })

        test('should return number string in en', () => {
          const result = '123456'
          testingLibrary.renderWithIntl(<IntegerEdit value={123456} onChange={EMPTY_FUNC} />)
          jestExpect(screen.getByRole('textbox')).toHaveValue(result)
        })

        test('should call onChange handler', () => {
          const changeHandler = sinon.spy()

          testingLibrary.renderWithIntl(<IntegerEdit value={1} onChange={changeHandler} options={{maxValue: 100}} />)
          fireEvent.change(screen.getByRole('textbox'), {target: {value: '100'}})
          act(() => {
            clock.tick(800)
          })
          expect(changeHandler).to.have.been.calledWith(100)
        })

        test('should call onChange handler with value null', () => {
          const changeHandler = sinon.spy()
          testingLibrary.renderWithIntl(<IntegerEdit value={1} onChange={changeHandler} options={{maxValue: 100}} />)
          fireEvent.change(screen.getByRole('textbox'), {target: {value: null}})
          act(() => {
            clock.tick(800)
          })
          expect(changeHandler).to.have.been.calledWith(null)
        })

        test('should call onChange handler with value 0', () => {
          const changeHandler = sinon.spy()
          testingLibrary.renderWithIntl(<IntegerEdit value={1} onChange={changeHandler} options={{maxValue: 100}} />)
          fireEvent.change(screen.getByRole('textbox'), {target: {value: 0}})
          act(() => {
            clock.tick(800)
          })
          expect(changeHandler).to.have.been.calledWith(0)
        })

        test('should not call onChange it number is too high', () => {
          const spy = sinon.spy()
          testingLibrary.renderWithIntl(<IntegerEdit value={2} options={{maxValue: 10}} onChange={spy} />)

          fireEvent.change(screen.getByRole('textbox'), {target: {value: 15}})
          act(() => {
            clock.tick(800)
          })
          expect(spy).to.not.have.been.called
          jestExpect(screen.getByRole('textbox')).toHaveValue('2')
        })

        test('should not call onChange it number is too low', () => {
          const spy = sinon.spy()
          testingLibrary.renderWithIntl(<IntegerEdit value={10} options={{minValue: 10}} onChange={spy} />)
          fireEvent.change(screen.getByRole('textbox'), {target: {value: 9}})
          act(() => {
            clock.tick(800)
          })
          expect(spy).to.not.have.been.called
          // value still needs to be accepted into the field, otherwise min values > 10 block any single key input
          jestExpect(screen.getByRole('textbox')).toHaveValue('9')
        })
      })
    })
  })
})
