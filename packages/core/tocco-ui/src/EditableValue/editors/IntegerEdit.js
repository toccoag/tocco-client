import PropTypes from 'prop-types'
import {react} from 'tocco-util'

import {StyledEditableWrapper} from '../StyledEditableValue'

import {checkValueRange, isAllowedValue} from './NumberEdit'
import StyledNumberEdit from './StyledNumberEdit'

const IntegerEdit = ({value, onChange, options, immutable, name, id, placeholder}) => {
  const {minValue, maxValue, format, allowLeadingZeros, suffix} = options || {}

  const handleChange = ({value: changedValue, floatValue}) => {
    let newValue = allowLeadingZeros ? changedValue : floatValue
    if (newValue === undefined) {
      newValue = null
    }
    if (checkValueRange(minValue, maxValue, floatValue)) {
      onChange(newValue)
    }
  }

  const actualValue = typeof value === 'undefined' || value === null ? '' : value

  return (
    <StyledEditableWrapper immutable={immutable}>
      <StyledNumberEdit
        isAllowed={isAllowedValue(minValue, maxValue)}
        allowNegative={true}
        decimalScale={0}
        disabled={immutable}
        id={id}
        name={name}
        onValueChange={handleChange}
        value={actualValue}
        allowLeadingZeros={allowLeadingZeros}
        suffix={suffix}
        format={format}
        placeholder={placeholder}
        immutable={immutable}
      />
    </StyledEditableWrapper>
  )
}

IntegerEdit.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  id: PropTypes.string,
  placeholder: PropTypes.string,
  intl: PropTypes.object,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    minValue: PropTypes.number,
    maxValue: PropTypes.number,
    format: PropTypes.string,
    suffix: PropTypes.string,
    allowLeadingZeros: PropTypes.bool
  })
}

export default react.Debouncer(IntegerEdit, 800)
