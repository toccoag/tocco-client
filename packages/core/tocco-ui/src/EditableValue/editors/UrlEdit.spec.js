import {screen, act} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import UrlEdit from './UrlEdit'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('UrlEdit ', () => {
        beforeAll(() => {
          jest.useFakeTimers()
        })

        afterEach(() => {
          jest.clearAllTimers()
        })

        afterAll(() => {
          jest.useRealTimers()
        })

        test('should show the value and a link', async () => {
          const value = 'http://www.tocco.ch/'
          testingLibrary.renderWithTheme(<UrlEdit value={value} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('link')).to.have.length(1)
          expect(screen.getByRole('link').href).to.eq(value)
        })

        test('should now show a link with no value', () => {
          const value = ''
          testingLibrary.renderWithTheme(<UrlEdit value={value} />)
          expect(screen.queryAllByRole('link')).to.have.length(0)
        })

        test('should normalize new input', async () => {
          const input = 'www.google.com?a=ABc23'
          const expectedResult = 'https://www.google.com?a=ABc23'
          const onChangeSpy = sinon.spy()
          const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})
          testingLibrary.renderWithTheme(<UrlEdit value="" onChange={onChangeSpy} />)

          await user.type(screen.getByRole('textbox'), input)

          act(() => {
            jest.advanceTimersByTime(200)
          })

          expect(onChangeSpy).to.have.been.calledWith(expectedResult)
        })
      })
    })
  })
})
