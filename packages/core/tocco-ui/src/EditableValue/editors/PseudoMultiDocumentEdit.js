import PropTypes from 'prop-types'

import Upload from '../../Upload'

const PseudoMultiDocumentEdit = ({id, onChange, options = {}, immutable, value}) => {
  const values = value || []
  const onChangeAppend = v => {
    onChange([...values, v])
  }
  const onChangeRemove = index => {
    onChange(values.toSpliced(index, 1))
  }

  const selectedUploads = values
    .filter(v => v)
    .map((v, index) => {
      return (
        <Upload
          key={`${id}-${index}`}
          id={`${id}-${index}`}
          // looks strange, but onUpload in an Upload that has a value is only called when clearing the field
          onUpload={() => onChangeRemove(index)}
          immutable={immutable}
          textResources={{
            upload: options.uploadText,
            uploading: options.uploadingText,
            delete: options.deleteText,
            download: options.downloadText
          }}
          allowedFileTypes={options.allowedFileTypes}
          value={v}
        />
      )
    })

  return [
    ...selectedUploads,
    <Upload
      key={`${id}-${selectedUploads.length + 1}`}
      id={`${id}-${selectedUploads.length + 1}`}
      onUpload={file => options.upload(file, onChangeAppend)}
      onChoose={() => options.choose(onChangeAppend)}
      immutable={immutable}
      textResources={{
        upload: options.uploadText,
        uploading: options.uploadingText,
        delete: options.deleteText,
        download: options.downloadText
      }}
      allowedFileTypes={options.allowedFileTypes}
      value={null}
    />
  ]
}

PseudoMultiDocumentEdit.propTypes = {
  id: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    upload: PropTypes.func.isRequired,
    choose: PropTypes.func,
    uploadText: PropTypes.string,
    uploadingText: PropTypes.string,
    deleteText: PropTypes.string,
    downloadText: PropTypes.string,
    allowedFileTypes: PropTypes.arrayOf(PropTypes.string)
  }),
  onChange: PropTypes.func,
  value: PropTypes.arrayOf(
    PropTypes.shape({
      fileName: PropTypes.string.isRequired,
      binaryLink: PropTypes.string.isRequired,
      thumbnailLink: PropTypes.string
    })
  )
}

export default PseudoMultiDocumentEdit
