import {getHours, getMinutes, getSeconds, isMatch, setHours, setMinutes, setSeconds, startOfDay} from 'date-fns'
import {useEffect, useRef, useState} from 'react'
import {date as dateUtil, consoleLogger} from 'tocco-util'

import {parseISOValue} from '../../DatePicker/utils'

const ReactDatepickerTimeInputClassName = 'react-datepicker-time__input'
const ReactDatepickerDayClassName = 'react-datepicker__day'
const ReactDatepickerInputClassName = 'react-datepicker__input'

const isDay = element => element.classList.contains(ReactDatepickerDayClassName)
const isTimeInput = element => element.classList.contains(ReactDatepickerTimeInputClassName)
const isInput = element => element.classList.contains(ReactDatepickerInputClassName)

const setTime = (date, {hour = 0, minute = 0, second = 0}) =>
  setHours(setMinutes(setSeconds(date, second), minute), hour)

const focusTimeInput = wrapper => {
  setTimeout(() => {
    if (wrapper) {
      const inputElement = wrapper.querySelector(`input.${ReactDatepickerTimeInputClassName}`)
      if (inputElement) {
        inputElement.focus({preventScroll: true})
      }
    }
  }, 0)
}

const focusDay = wrapper => {
  setTimeout(() => {
    if (wrapper) {
      // only the day of the preSelection has tabindex set to 0
      const selectedDay = wrapper.querySelector(`.${ReactDatepickerDayClassName}[tabindex="0"]`)
      if (selectedDay) {
        selectedDay.focus({preventScroll: true})
      }
    }
  }, 0)
}

const focusPrevInput = inputElement => {
  const form = inputElement?.form
  if (form) {
    const index = [...form].indexOf(inputElement)
    if (index >= 1) {
      focusInput(form[index - 1])
    }
  } else {
    consoleLogger.logWarning(
      `Datepicker '${inputElement?.id}' is not withing a form and focus to the previous input cannot be set.`
    )
  }
}

const focusNextInput = (inputElement, changedInitially) => {
  const form = inputElement?.form
  if (form) {
    const index = [...form].indexOf(inputElement)
    if (index >= 0) {
      const skip = changedInitially ? 2 : 3 // skip datepicker button and clear button (when datepicker has value)
      focusInput(form[index + skip])
    }
  } else {
    consoleLogger.logWarning(
      `Datepicker '${inputElement?.id}' is not withing a form and focus to the next input cannot be set.`
    )
  }
}

const focusInput = inputElement => {
  setTimeout(() => {
    if (inputElement) {
      inputElement.focus({preventScroll: true})
    }
  }, 0)
}

const isoStringToDate = parseISOValue
const dateToIsoString = date => (date ? date.toISOString() : null)

/**
 * Keyboard Handling Requirements:
 * https://toccoag.atlassian.net/browse/TOCDEV-6134
 * https://toccoag.atlassian.net/browse/TOCRT-89
 *
 * In order to fulfill the requirements react-datepicker internals had to be touched.
 *  - preSelection:
 *      A date which represents the currenty selected day (date) on the calendar.
 *      It can be different than the selected date when choosing another day by the arrow keys.
 *
 *   - open:
 *      The react-datepicker is tracking an internal open state
 *      although it's possible to set the open state from outside.
 *      Unfortunately at many places it only checks the internal state. Therefore we tried to keep those in sync.
 *
 * - getPreselection():
 *      https://github.com/Hacker0x01/react-datepicker/blob/master/src/index.jsx#L342
 *      Returns the initial preSelection state.
 */

/**
 * Tab key handling issue
 *  - when input is focused and Date(time)picker is opened the next field handling via tab key press is broken
 *  - on Firefox it is generally an issue
 *  - on any Browser it's an issue since we use a Portal to show Date(time)picker
 *    the Date(time)picker is not within document flow anymore
 *
 *  Reproduction:
 *    - set focus on input field by clicking inside input
 *    - ensure the Datepicker modal is open
 *    - press tab
 *    - expecting to tab to the next form field
 */

const useDatePicker = (
  value,
  onChange,
  {dateFormats = [], minDate, maxDate, hasTime, valueToDate = isoStringToDate, dateToValue = dateToIsoString}
) => {
  const datePickerRef = useRef(null)
  const [isOpen, setIsOpen] = useState(false)

  const selectedDate = valueToDate(value)
  const minDateVal = minDate ? valueToDate(minDate) : undefined
  const maxDateVal = maxDate ? valueToDate(maxDate) : undefined

  useEffect(() => {
    /**
     * Keep internal state of DatePicker in sync.
     */
    if (datePickerRef.current && typeof datePickerRef.current.setOpen === 'function') {
      datePickerRef.current.setOpen(isOpen)
    }
  }, [isOpen])

  const toggleModal = () => setIsOpen(open => !open)
  const closeModal = () => setIsOpen(false)
  const openModal = () => setIsOpen(true)

  const getCurrentPreSelection = (fallbackToInitial = false) => {
    if (datePickerRef.current && typeof datePickerRef.current.getPreSelection === 'function') {
      const initialDate = fallbackToInitial ? datePickerRef.current.getPreSelection() : null
      return datePickerRef.current.state?.preSelection || initialDate
    }
    return null
  }

  const setFocusedDate = date => {
    /**
     * In order to focus a day on the calendar we only have to set the preSelection to the given date.
     * Vice versa to be able to remove the focus from a day an set it to another input/element
     * the preSelection has to be cleared first.
     */
    if (datePickerRef.current && typeof datePickerRef.current.setPreSelection === 'function') {
      datePickerRef.current.setPreSelection(date)
    }
  }

  const clearValue = () => {
    onChange(null)
    /**
     * Workaround:
     * When the date is cleared the preSelection has to be cleared as well.
     * Otherwise the value will be set right after it has been cleared when tabbing/entering out.
     */
    setFocusedDate(null)
  }

  const handleChangeToCurrentPreSelection = (event, onlyDate = false) => {
    const date = getCurrentPreSelection(false)
    if (date) {
      const dateWithTime = selectedDate || new Date()
      const changedDate = onlyDate
        ? setTime(date, {
            hour: getHours(dateWithTime),
            minute: getMinutes(dateWithTime),
            second: getSeconds(dateWithTime)
          })
        : date
      handleOnChange(changedDate, event)
    }
  }

  const handleOnChange = (date, event) => {
    if (date) {
      const changedInitially = !value && Boolean(date)

      const inputValue = event?.target?.value
      const inputValueWithoutTime = inputValue ? dateFormats.some(frmt => isMatch(inputValue, frmt)) : false

      const startOfSelectedDay = startOfDay(date)
      const hasTimeChanged =
        getHours(date) !== getHours(startOfSelectedDay) || getMinutes(date) !== getMinutes(startOfSelectedDay)

      // set time to now when time has not set explicitly yet
      if (((changedInitially && !hasTimeChanged) || inputValueWithoutTime) && hasTime) {
        date = dateUtil.setCurrentTime(date)
      }
      onChange(dateToValue(date))
    } else {
      clearValue()
    }

    if (!hasTime && event?.type === 'click') {
      // close Datepicker on select date by click (only without time)
      closeModal()
      focusInput(datePickerRef.current?.input)
    }
  }

  const handleConfirmKey = e => {
    const changedInitially = !value

    if (e.key === 'Tab') {
      if (!e.shiftKey) {
        if (isInput(e.target) && isOpen) {
          /**
           * ℹ️ Focus on input field and Datepicker is open
           *  - close Datepicker
           *  - set value => use preSelection as new value
           *  - tab to next input
           */
          handleChangeToCurrentPreSelection(e)
          closeModal()
          focusNextInput(datePickerRef.current?.input, changedInitially)
        } else if (isTimeInput(e.target)) {
          /**
           * ℹ️ Focus on time input field inside Datetimepicker
           *  - close Datepicker
           *  - set time value => no workaround needed
           *  - tab to next input
           */
          closeModal()
          focusNextInput(datePickerRef.current?.input, changedInitially)
        } else if (isDay(e.target)) {
          if (hasTime) {
            /**
             * ℹ️ Focus on day inside Datetimepicker
             *  - keep Datetimepicker open
             *  - set day value => use preSelection as new value
             *  - tab to time input
             */
            handleChangeToCurrentPreSelection(e, true)
            /**
             * Workaround:
             * Clear preSelection so that it's possible to loose focus from days.
             */
            setFocusedDate(null)
            focusTimeInput(datePickerRef?.current?.calendar?.containerRef?.current)
          } else {
            /**
             * ℹ️ Focus on day inside Datepicker
             *  - keep Datepicker open
             *  - set day value => use preSelection as new value
             *  - tab to next input
             */
            handleChangeToCurrentPreSelection(e)
            closeModal()
            focusNextInput(datePickerRef.current?.input, changedInitially)
          }
        }
      } else {
        /** SHIFT + TAB */
        if (isInput(e.target)) {
          /**
           * ℹ️ Focus on input field
           *  - close Datepicker
           *  - tab to prev input
           */
          closeModal()
        } else if (isTimeInput(e.target)) {
          /**
           * ℹ️ Focus on time input field inside Datetimepicker
           *  - keep Datepicker open
           *  - tab back to days
           *
           * Workaround:
           * To focus a day the preSelection has to be set.
           * We jump back to the selected value or to the initial preSelection.
           */
          setFocusedDate(selectedDate || getCurrentPreSelection(true))
          focusDay(datePickerRef?.current?.calendar?.containerRef?.current)
        } else if (isDay(e.target)) {
          /**
           * ℹ️ Focus on day inside Datepicker
           *  - close Datepicker
           *  - tab to prev input
           */
          closeModal()
          focusPrevInput(datePickerRef.current?.input)
        }
      }
    }

    if (e.key === 'Enter') {
      /**
       * ℹ️ Focus no any element
       *  - close Datepicker
       *  - do not submit form
       *  - keep focus on input (to be able to open Datepicker with keyboard again)
       *
       * Workaround:
       * To focus the input when form is untouched the focus event has to be deferred.
       * Otherwise focus is removed again.
       */
      closeModal()
      setTimeout(() => {
        focusInput(datePickerRef.current?.input)
      }, 10)
      e.preventDefault()
      e.stopPropagation()
    }

    if (['ArrowDown', 'ArrowUp', 'Enter'].includes(e.key)) {
      if (!isOpen) {
        openModal()
        /**
         * React DatePicker expects to be open on click inside input and therefore to be open on key input.
         * We want to have it closed and open it with Arrow keys.
         * To be able to workaround the DatePickers expectations wie do not want let it handle the default behaviour
         * in this case.
         * The only option is to overwrite the key event so that it ignores the event completely.
         * https://github.com/Hacker0x01/react-datepicker/blob/master/src/index.jsx#L686
         * https://github.com/Hacker0x01/react-datepicker/issues/3458
         */
        e.key = `Handled${e.key}`
      } else if (isOpen && getCurrentPreSelection(false) === null) {
        /**
         * Workaround:
         * Since we do not want to open the Datepicker with a preSelection we need to set the preSelection
         * to be able to jump into the calendar by the keyboard.
         */
        setFocusedDate(selectedDate || getCurrentPreSelection(true))
        focusDay(datePickerRef?.current?.calendar?.containerRef?.current)
      }
    }
  }

  const handleMouseDownOnClear = e => {
    e.preventDefault() // to not loose focus from the origin element
    clearValue()
  }

  const handleInputCalendarClick = () => {
    toggleModal()
    /**
     * Workaround:
     * Since we do not want to open the Datepicker with a preSelection we need to set it to null explicitly.
     * Otherwise it will be set to any date always.
     */
    setFocusedDate(null)
    focusInput(datePickerRef.current?.input) // do not remove focus from input when open modal
  }

  const handleCalendarOpen = () => {
    /**
     * Workaround:
     * Since we do not want to open the Datepicker with a preSelection we need to set it to null explicitly.
     * Otherwise it will be set to any date always.
     */
    setFocusedDate(null)
  }

  const handleBlur = () => {
    /**
     * Workaround:
     * When the date/datetime has been set via typing an input value (with closed Datepicker)
     * the value does not get formatted correctly after moving to the next input.
     * Setting the inputValue explicitly to `null` again will reformat the displayed input value.
     */
    if (datePickerRef.current) {
      datePickerRef.current.setState({inputValue: null})
    }
  }

  const reactDatePickerProps = {
    onClickOutside: closeModal,
    onInputClick: handleInputCalendarClick,
    onKeyDown: handleConfirmKey,
    onChange: handleOnChange,
    onCalendarOpen: handleCalendarOpen,
    onBlur: handleBlur,
    preventOpenOnFocus: true,
    enableTabLoop: false,
    /**
     * We do not want that react-datepicker it's changing it's internal state.
     * We would like to be under control the open state otherwise it can happen that
     * the user needs to click multiple times to close/open the modal because the states got out of sync.
     */
    shouldCloseOnSelect: false,
    open: isOpen,
    selected: selectedDate,
    showTimeInput: hasTime,
    minDate: minDateVal,
    maxDate: maxDateVal,
    ref: datePickerRef,
    className: ReactDatepickerInputClassName
  }

  const timeInputProps = {
    onKeyDown: handleConfirmKey,
    className: ReactDatepickerTimeInputClassName
  }

  const clearButtonProps = {
    onMouseDown: handleMouseDownOnClear
  }

  const calendarButtonProps = {
    // onMouseDown is not working here since it would interf with the closeOutside handler
    onClick: handleInputCalendarClick,
    /**
     * Prevent having multiple DatePicker open by clicking on Calendar icons.
     * Use ignore onClickOutside classname from React DatePicker https://github.com/Hacker0x01/react-datepicker/blob/master/src/index.jsx#L50
     * https://github.com/Pomax/react-onclickoutside#marking-elements-as-skip-over-this-one-during-the-event-loop
     */
    className: isOpen ? 'react-datepicker-ignore-onclickoutside' : ''
  }

  return {
    reactDatePickerProps,
    timeInputProps,
    clearButtonProps,
    calendarButtonProps,
    closeModal
  }
}

export default useDatePicker
