import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import TimeEdit from './TimeEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('TimeEdit ', () => {
        test('should render input', async () => {
          testingLibrary.renderWithTheme(<TimeEdit value="16:30" onChange={EMPTY_FUNC} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('textbox')).to.have.length(1)
          expect(screen.getByRole('textbox').value).to.eq('16:30')
        })
      })
    })
  })
})
