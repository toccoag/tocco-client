import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import PseudoMultiDocumentEdit from './PseudoMultiDocumentEdit'

const EMPTY_FUNC = () => {}

const createValue = id => ({
  fileName: `${id}.txt`,
  thumbnailLink: '',
  binaryLink: `blob:${id}`
})

const mockOptions = {
  uploadText: 'upload',
  uploadingText: 'uploading...',
  upload: EMPTY_FUNC
}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('PseudoMultiDocumentEdit', () => {
        test('should render upload without value', async () => {
          testingLibrary.renderWithIntl(<PseudoMultiDocumentEdit options={mockOptions} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByTestId('icon-trash')).to.be.empty
          expect(screen.queryAllByTestId('icon-arrow-to-bottom')).to.be.empty
          expect(await screen.findAllByTestId('icon-folder')).to.have.length(1)
          expect(await screen.findAllByTestId('icon-arrow-to-top')).to.have.length(1)
        })

        test('should render upload with value', async () => {
          testingLibrary.renderWithIntl(<PseudoMultiDocumentEdit value={[createValue('mock')]} options={mockOptions} />)
          await screen.findAllByTestId('icon')

          expect(await screen.findByText('mock.txt')).to.exist
          expect(await screen.findAllByTestId('icon-trash')).to.have.length(1)
          expect(await screen.findAllByTestId('icon-arrow-to-bottom')).to.have.length(1)
          expect(await screen.findAllByTestId('icon-folder')).to.have.length(1)
          expect(await screen.findAllByTestId('icon-arrow-to-top')).to.have.length(1)
        })

        test('should render multiple values', async () => {
          testingLibrary.renderWithIntl(
            <PseudoMultiDocumentEdit value={[createValue('mock1'), createValue('mock2')]} options={mockOptions} />
          )
          await screen.findAllByTestId('icon')

          expect(await screen.findByText('mock1.txt')).to.exist
          expect(await screen.findByText('mock2.txt')).to.exist
          expect(await screen.findAllByTestId('icon-trash')).to.have.length(2)
          expect(await screen.findAllByTestId('icon-arrow-to-bottom')).to.have.length(2)
          expect(await screen.findAllByTestId('icon-folder')).to.have.length(1)
          expect(await screen.findAllByTestId('icon-arrow-to-top')).to.have.length(1)
        })
      })
    })
  })
})
