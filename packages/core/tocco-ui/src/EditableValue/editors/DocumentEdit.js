import PropTypes from 'prop-types'

import Upload from '../../Upload'

const DocumentEdit = ({id, onChange, options = {}, immutable, value}) => {
  const onUpload = file => {
    if (file === null) {
      if (onChange) {
        onChange(null)
      }
    } else {
      if (options.upload) {
        options.upload(file)
      }
    }
  }

  return (
    <Upload
      id={id}
      onUpload={onUpload}
      onChoose={options.choose}
      immutable={immutable}
      textResources={{
        upload: options.uploadText,
        uploading: options.uploadingText,
        delete: options.deleteText,
        download: options.downloadText
      }}
      allowedFileTypes={options.allowedFileTypes}
      value={value || null}
    />
  )
}

DocumentEdit.propTypes = {
  id: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    upload: PropTypes.func.isRequired,
    choose: PropTypes.func,
    uploadText: PropTypes.string,
    uploadingText: PropTypes.string,
    deleteText: PropTypes.string,
    downloadText: PropTypes.string,
    allowedFileTypes: PropTypes.arrayOf(PropTypes.string)
  }),
  onChange: PropTypes.func,
  value: PropTypes.object
}

export default DocumentEdit
