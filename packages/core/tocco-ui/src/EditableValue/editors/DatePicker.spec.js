import {fireEvent, screen} from '@testing-library/react'
import {
  getHours,
  getMinutes,
  getSeconds,
  setHours,
  setMilliseconds,
  setMinutes,
  setSeconds,
  startOfDay,
  startOfMonth
} from 'date-fns'
import {IntlProvider} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'
import {date as dateUtil} from 'tocco-util'

import {loadLocales} from '../../DatePicker/utils'

import DatePicker from './DatePicker'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('DatePicker ', () => {
        beforeEach(() => {
          loadLocales()
          jest.useFakeTimers()
        })

        afterEach(() => {
          jest.useRealTimers()
        })

        test('should be able to select a day', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime={false} dateFormats={['P']} />,
            {container: document.body}
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.click(input)
          testingLibrary.handleFloatingUiAsyncComputation()

          const day = container.querySelector('.react-datepicker__day--001:not(.react-datepicker__day--outside-month)')
          fireEvent.click(day)

          const firstOfMonth = startOfMonth(new Date()).toISOString()
          expect(onChangeSpy).to.have.been.calledWith(firstOfMonth)
        })

        test('should be able to enter a date', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime={false} dateFormats={['P']} />
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.focus(input)

          fireEvent.change(input, {target: {value: '06/14/2022'}})

          const date = startOfDay(new Date('06/14/2022')).toISOString()
          expect(onChangeSpy).to.have.been.calledWith(date)
        })

        test('should be able to enter a date time', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime dateTimeFormats={['P HH:mm']} />
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.focus(input)

          fireEvent.change(input, {target: {value: '06/14/2022 14:00'}})

          const date = new Date('06/14/2022 14:00').toISOString()
          expect(onChangeSpy).to.have.been.calledWith(date)
        })

        test('should be able to enter a date time in localized format', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de-CH" defaultLocale="de-CH" onError={() => {}}>
              <DatePicker onChange={onChangeSpy} hasTime dateTimeFormats={['P HH:mm']} />
            </IntlProvider>
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.focus(input)

          fireEvent.change(input, {target: {value: '14.06.2022 14:00'}})

          const date = new Date('06/14/2022 14:00').toISOString()
          expect(onChangeSpy).to.have.been.calledOnce
          expect(onChangeSpy).to.have.been.calledWith(date)
        })

        test('should be able to enter a time in popover', async () => {
          const onChangeSpy = sinon.spy()
          const date = new Date('06/14/2022 14:00').toISOString()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker value={date} onChange={onChangeSpy} dateTimeFormats={['P HH:mm']} hasTime />,
            {container: document.body}
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.click(input)

          const timeInput = container.querySelector('.react-datepicker-time__input input')
          fireEvent.change(timeInput, {target: {value: '10:00'}})

          const expectedDate = new Date('06/14/2022 10:00').toISOString()
          expect(onChangeSpy).to.have.been.calledWith(expectedDate)
        })

        test('should be able to enter a date in different format', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithTheme(
            <IntlProvider locale="de-CH" defaultLocale="de-CH" onError={() => {}}>
              <DatePicker
                onChange={onChangeSpy}
                hasTime
                dateFormats={['P', dateUtil.getLocalizedDateFormatWithoutPunctuation('de-CH')]}
              />
            </IntlProvider>
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.focus(input)

          fireEvent.change(input, {target: {value: '14062022'}})

          const date = dateUtil.setCurrentTime(new Date('06/14/2022')).toISOString()
          expect(onChangeSpy).to.have.been.calledOnce
          expect(onChangeSpy).to.have.been.calledWith(date)
        })

        test('should set time automatically', async () => {
          const onChangeSpy = sinon.spy()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime={true} dateFormats={['P']} />,
            {container: document.body}
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.click(input)
          testingLibrary.handleFloatingUiAsyncComputation()

          const day = container.querySelector('.react-datepicker__day--001:not(.react-datepicker__day--outside-month)')
          fireEvent.click(day)

          const firstOfMonth = startOfMonth(new Date())
          const expectedDate = dateUtil.setCurrentTime(firstOfMonth)

          expect(onChangeSpy).to.have.been.calledWith(expectedDate.toISOString())
        })

        test('should not overwrite time when already set', async () => {
          const onChangeSpy = sinon.spy()
          const value = new Date()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime={true} dateFormats={['P']} value={value.toISOString()} />,
            {container: document.body}
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.click(input)
          testingLibrary.handleFloatingUiAsyncComputation()

          const day = container.querySelector('.react-datepicker__day--001:not(.react-datepicker__day--outside-month)')
          fireEvent.click(day)

          const firstOfMonth = startOfMonth(new Date())
          let expectedDate = setHours(firstOfMonth, getHours(value))
          expectedDate = setMinutes(expectedDate, getMinutes(value))
          expectedDate = setSeconds(expectedDate, getSeconds(value))
          expectedDate = setMilliseconds(expectedDate, 0) // is reset by react-datepicker

          expect(onChangeSpy).to.have.been.calledWith(expectedDate.toISOString())
        })

        test('should set time automatically when entered date only', async () => {
          const onChangeSpy = sinon.spy()
          const value = new Date()

          const {container} = testingLibrary.renderWithIntl(
            <DatePicker onChange={onChangeSpy} hasTime={true} dateFormats={['P']} value={value.toISOString()} />
          )
          await screen.findAllByTestId('icon')

          const input = container.querySelector('.react-datepicker__input-container input')
          fireEvent.click(input)
          testingLibrary.handleFloatingUiAsyncComputation()

          fireEvent.change(input, {target: {value: '06/14/2022'}})

          const enteredDate = new Date(2022, 5, 14)
          const expectedDate = dateUtil.setCurrentTime(enteredDate)

          expect(onChangeSpy).to.have.been.calledWith(expectedDate.toISOString())
        })
      })
    })
  })
})
