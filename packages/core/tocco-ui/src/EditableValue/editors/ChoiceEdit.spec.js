import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ChoiceEdit from './ChoiceEdit'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('ChoiceEdit ', () => {
        test('should render multiple choice', () => {
          const value = {
            options: [
              {id: 'first', label: 'first', checked: true, immutable: true},
              {id: 'second', label: 'second', checked: true},
              {id: 'third', label: 'third', checked: false, immutable: true}
            ]
          }

          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'multiple'}} />
          )

          const checkboxes = screen.getAllByRole('checkbox')
          expect(checkboxes).to.have.length(3)
          expect(checkboxes[0].checked).to.be.true
          expect(checkboxes[0].id).to.eql('first')
          expect(checkboxes[0].disabled).to.be.true
          expect(checkboxes[1].checked).to.be.true
          expect(checkboxes[1].id).to.eql('second')
          expect(checkboxes[1].disabled).to.be.false
          expect(checkboxes[2].checked).to.be.false
          expect(checkboxes[2].id).to.eql('third')
          expect(checkboxes[2].disabled).to.be.true
        })

        test('should render multiple choice', () => {
          const value = {
            options: [
              {id: 'first', label: 'first', hideCheckbox: true},
              {id: 'second', label: 'second', hideCheckbox: true},
              {id: 'third', label: 'third', hideCheckbox: true}
            ]
          }

          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'multiple'}} />
          )

          const checkboxes = screen.queryAllByRole('checkbox')
          expect(checkboxes).to.be.empty
        })

        test('should render single choice', () => {
          const value = {
            options: [
              {id: 'first', label: 'first', checked: true},
              {id: 'second', label: 'second', checked: false},
              {id: 'third', label: 'third', checked: false}
            ]
          }

          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'single'}} />
          )

          const checkboxes = screen.getAllByRole('radio')
          expect(checkboxes).to.have.length(3)
          expect(checkboxes[0].checked).to.be.true
          expect(checkboxes[0].id).to.eql('first')
          expect(checkboxes[1].checked).to.be.false
          expect(checkboxes[1].id).to.eql('second')
          expect(checkboxes[2].checked).to.be.false
          expect(checkboxes[2].id).to.eql('third')
        })

        test('should call onChange for multiple choice', async () => {
          const value = {
            options: [
              {id: 'first', label: 'first', checked: false},
              {id: 'second', label: 'second', checked: true},
              {id: 'third', label: 'third', checked: false}
            ]
          }
          const valueChanged = {
            options: [
              {id: 'first', label: 'first', checked: true},
              {id: 'second', label: 'second', checked: true},
              {id: 'third', label: 'third', checked: false}
            ]
          }
          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'multiple'}} />
          )

          fireEvent.click(screen.getAllByRole('checkbox')[0])
          expect(spy).to.have.been.calledWith(valueChanged)
        })

        test('should call onChange for single choice', async () => {
          const value = {
            options: [
              {id: 'first', label: 'first', checked: false},
              {id: 'second', label: 'second', checked: true},
              {id: 'third', label: 'third', checked: false}
            ]
          }
          const valueChanged = {
            options: [
              {id: 'first', label: 'first', checked: true},
              {id: 'second', label: 'second', checked: false},
              {id: 'third', label: 'third', checked: false}
            ]
          }
          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'single'}} />
          )

          fireEvent.click(screen.getAllByRole('radio')[0])
          expect(spy).to.have.been.calledWith(valueChanged)
        })

        test('should render custom label', () => {
          const value = {
            options: [{id: 'first', labelComponent: <span>custom component value</span>}]
          }

          const spy = sinon.spy()
          testingLibrary.renderWithTheme(
            <ChoiceEdit id="name" onChange={spy} value={value} options={{selectionType: 'multiple'}} />
          )

          expect(screen.queryByText('custom component value')).to.exist
        })
      })
    })
  })
})
