import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import LocationEdit from './LocationEdit'

const options = {
  suggestions: [],
  fetchSuggestions: sinon.spy(),
  isLoading: false,
  country: ['CH', 'AT'],
  locationValues: {
    city: 'Zurich',
    postcode: '2306',
    canton: 'ZH',
    district: 'Zurich',
    country: {display: 'CH', key: '1'}
  }
}

const EMPTY_FUNC = () => {}

const value = {
  city: 'Zurich',
  postcode: '2306'
}

const locationString = 'https://www.google.com/maps/search/?api=1&query=2306+Zurich+CH'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('LocationEdit ', () => {
        test('should render LocationEdit', async () => {
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={EMPTY_FUNC} value={value} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('textbox')).to.have.length(2)
        })

        test('should render input value', async () => {
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={EMPTY_FUNC} value={value} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('textbox')[0].value).to.eql(value.postcode)
          expect(screen.queryAllByRole('textbox')[1].value).to.eql(value.city)
        })

        test('should update value postcode', async () => {
          const spy = sinon.spy()
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={spy} value={{}} />)
          await screen.findAllByTestId('icon')

          const postCodeField = screen.queryAllByRole('textbox')[0]
          fireEvent.change(postCodeField, {target: {value: '23'}})
          expect(spy).to.have.been.calledWith({postcode: '23'})
        })

        test('should call fetchSuggestions with new value', async () => {
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={EMPTY_FUNC} value={value} />)
          await screen.findAllByTestId('icon')

          const postCodeField = screen.queryAllByRole('textbox')[0]
          fireEvent.change(postCodeField, {target: {value: '23'}})

          expect(options.fetchSuggestions).to.have.been.calledWith({postcode: '23'}, {display: 'CH', key: '1'})
        })

        test('should call onChange with new value', async () => {
          const spy = sinon.spy()
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={spy} value={value} />)
          await screen.findAllByTestId('icon')

          const postCodeField = screen.queryAllByRole('textbox')[0]
          fireEvent.change(postCodeField, {target: {value: '2345'}})

          expect(spy).to.have.been.calledWith({postcode: '2345', city: 'Zurich'})
        })

        test('should render Link', async () => {
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={EMPTY_FUNC} value={value} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryAllByRole('link')).to.have.length(1)
        })

        test('should set Link href', async () => {
          testingLibrary.renderWithIntl(<LocationEdit options={options} onChange={EMPTY_FUNC} value={value} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryByRole('link').getAttribute('href')).to.eql(locationString)
        })
      })
    })
  })
})
