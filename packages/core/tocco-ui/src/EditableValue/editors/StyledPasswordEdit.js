import styled, {css} from 'styled-components'

import {scale} from '../../utilStyles'
import {StyledInputCss} from '../StyledEditableValue'

export const StyledPasswordEdit = styled.input`
  /* stylelint-disable-next-line block-no-empty */
  @keyframes onAutoFillStart {
    from {
    }
  }

  &&&& {
    ${StyledInputCss}
    transition: background-color 50000s, color 50000s, filter 50000s;

    &:-webkit-autofill {
      animation-duration: 50000s;
      animation-name: onAutoFillStart;
    }

    ${({extraHigh}) =>
      extraHigh &&
      css`
        padding-top: ${scale.space(-0.9)};
        padding-bottom: ${scale.space(-0.9)};
      `}
  }
`
