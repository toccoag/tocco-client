import PropTypes from 'prop-types'
import Autosuggest from 'react-autosuggest'
import FocusWithin from 'react-simple-focus-within'

import Link from '../../Link'
import LoadingSpinner from '../../LoadingSpinner'
import Typography from '../../Typography'
import {StyledEditableControl} from '../StyledEditableValue'
import {getGoogleMapsAddressLink} from '../utils'

import {StyledLocationEdit} from './StyledLocationEdit'

const LocationEdit = ({onChange: onChangeProp, options, value: valueProp, id, immutable, name}) => {
  const returnGetSuggestion = attr => suggestion => suggestion[attr]

  const renderSuggestion = suggestion => {
    const {state, country, postcode, city} = suggestion
    const cantonString = state ? `- ${state}` : ''
    const countryString = country ? `/ ${country.display}` : ''

    return (
      <Typography.Span>
        {postcode} {city} {cantonString} {countryString}
      </Typography.Span>
    )
  }

  const onChange =
    field =>
    (event, {newValue}) => {
      onChangeProp({...valueProp, [field]: newValue})
    }

  const onSuggestionSelected = (event, {suggestion}) => {
    if (suggestion.validationRequired === true) {
      options.validateLocation(suggestion, onChangeProp)
    } else {
      onChangeProp(suggestion)
    }
  }

  const returnOnSuggestionFetchRequested =
    field =>
    ({value}) => {
      options.fetchSuggestions({[field]: value}, options.locationValues.country)
    }

  const showGoogleMaps = value => Boolean(value.city || value.postcode) && !value.street

  const inputPropsZip = {
    id,
    value: valueProp.postcode || '',
    onChange: onChange('postcode'),
    disabled: immutable,
    name: 'postal',
    autocomplete: 'postal-code'
  }

  const inputPropsCity = {
    value: valueProp.city || '',
    onChange: onChange('city'),
    disabled: immutable,
    name: 'city',
    autocomplete: 'address-level2'
  }

  return (
    <FocusWithin>
      {({focused, getRef}) => (
        <StyledLocationEdit immutable={immutable} name={name} ref={getRef}>
          <Autosuggest
            suggestions={options.suggestions || []}
            onSuggestionsFetchRequested={returnOnSuggestionFetchRequested('postcode')}
            getSuggestionValue={returnGetSuggestion('postcode')}
            renderSuggestion={renderSuggestion}
            onSuggestionsClearRequested={() => {}}
            inputProps={inputPropsZip}
            onSuggestionSelected={onSuggestionSelected}
            focusInputOnSuggestionClick={false}
            shouldRenderSuggestions={v => v && !immutable}
          />
          {(focused || valueProp.city || valueProp.postcode) && <Typography.Span>/</Typography.Span>}
          <Autosuggest
            suggestions={options.suggestions || []}
            onSuggestionsFetchRequested={returnOnSuggestionFetchRequested('city')}
            getSuggestionValue={returnGetSuggestion('city')}
            renderSuggestion={renderSuggestion}
            onSuggestionsClearRequested={() => {}}
            inputProps={inputPropsCity}
            onSuggestionSelected={onSuggestionSelected}
            focusInputOnSuggestionClick={false}
            shouldRenderSuggestions={v => v && !immutable}
          />
          <StyledEditableControl>
            {options.isLoading && <LoadingSpinner size="1.8rem" />}
            {showGoogleMaps(options.locationValues) && (
              <Link
                href={getGoogleMapsAddressLink(options.locationValues)}
                icon="map-marked"
                tabIndex={-1}
                target="_blank"
                dense={false}
                title={options.mapButtonTitle || 'Show on map'}
                neutral
              />
            )}
          </StyledEditableControl>
        </StyledLocationEdit>
      )}
    </FocusWithin>
  )
}

const locationObjectPropType = PropTypes.shape({
  city: PropTypes.string,
  postcode: PropTypes.string,
  address: PropTypes.string,
  country: PropTypes.shape({
    display: PropTypes.string,
    key: PropTypes.string
  }),
  canton: PropTypes.string,
  district: PropTypes.string
})

LocationEdit.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.shape({
    city: PropTypes.string,
    postcode: PropTypes.string
  }),
  options: PropTypes.shape({
    suggestions: PropTypes.arrayOf(locationObjectPropType),
    fetchSuggestions: PropTypes.func,
    validateLocation: PropTypes.func,
    isLoading: PropTypes.bool,
    mapButtonTitle: PropTypes.string,
    locationValues: locationObjectPropType
  }),
  name: PropTypes.string,
  id: PropTypes.string,
  immutable: PropTypes.bool
}

export default LocationEdit
