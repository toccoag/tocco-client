import {waitFor} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DateTimeEdit from './DateTimeEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('DateTimeEdit ', () => {
        test('should render an instance of DatePicker', async () => {
          const {container} = testingLibrary.renderWithIntl(<DateTimeEdit onChange={EMPTY_FUNC} />)

          await waitFor(() => {
            const datePicker = container.querySelector('.react-datepicker-wrapper')
            expect(datePicker).to.have.exist
          })
        })
      })
    })
  })
})
