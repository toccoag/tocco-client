import styled from 'styled-components'

import {declareFont, scale} from '../../utilStyles'

export const StyledOption = styled.div`
  margin: ${scale.space(-1)} 0;
  display: flex;
  align-items: center;
  gap: ${scale.space(-1.5)};

  input {
    margin: 0;
  }
`

export const StyledLabel = styled.span`
  ${declareFont()}
  display: flex;
  gap: ${scale.space(-1.5)};
  align-items: center;
`
