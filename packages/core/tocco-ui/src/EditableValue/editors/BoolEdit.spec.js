import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import BoolEdit from './BoolEdit'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('BoolEdit ', () => {
        test('should show checked checkbox on true', () => {
          testingLibrary.renderWithIntl(<BoolEdit value />)
          jestExpect(screen.queryByRole('checkbox')).toBeChecked()
        })

        test('should show unchecked checkbox on falsy values', () => {
          const {rerender} = testingLibrary.renderWithIntl(<BoolEdit value={false} />)
          jestExpect(screen.queryByRole('checkbox')).not.toBeChecked()

          rerender(<BoolEdit value={null} />)
          jestExpect(screen.queryByRole('checkbox')).not.toBeChecked()

          rerender(<BoolEdit />)
          jestExpect(screen.queryByRole('checkbox')).not.toBeChecked()
        })

        test('call on change', async () => {
          const spy = sinon.spy()
          const user = userEvent.setup()
          testingLibrary.renderWithIntl(<BoolEdit value={false} onChange={spy} />)
          const checkbox = screen.queryByRole('checkbox')
          await user.click(checkbox)

          expect(spy).to.have.been.calledWith(true)
        })
      })
    })
  })
})
