import _omit from 'lodash/omit'
import _pick from 'lodash/pick'

const locationValueProps = ['key', 'display', 'postcode', 'city', 'country', 'latitude', 'longitude']

export const extractPostcodeCity = value => {
  if (!value) {
    return null
  }

  const matches = [...value.matchAll(/(?<postcode>[\d]*)(?<city>[^\d\s]*)/g)]
  const matchedGroups = matches.map(m => m.groups)
  const postcodeGroup = matchedGroups.find(m => m.postcode)
  const cityGroup = matchedGroups.find(m => m.city)
  return {...(cityGroup ? {city: cityGroup.city} : {}), ...(postcodeGroup ? {postcode: postcodeGroup.postcode} : {})}
}

export const mapValueToLocationValue = value =>
  value?.latitude && value?.longitude ? _pick(value, locationValueProps) : undefined

export const mapLocationValueToValue = (value, locationValue) => {
  if (locationValue) {
    return {
      ...value,
      ..._pick(locationValue, locationValueProps)
    }
  }

  return _omit(value, locationValueProps)
}

const convertDegToRad = deg => (deg * Math.PI) / 180.0
const mapSuggestionToOption = suggestion => {
  const {state, country, postcode, city, latitude, longitude} = suggestion
  const cantonString = state ? `- ${state}` : ''
  const countryString = country ? `/ ${country.display}` : ''
  return {
    key: postcode,
    display: `${postcode} ${city} ${cantonString} ${countryString}`,
    postcode,
    city,
    country,
    latitude: convertDegToRad(latitude),
    longitude: convertDegToRad(longitude)
  }
}

export const mapLocationSuggestionsToOptions = suggestions =>
  suggestions ? suggestions.map(mapSuggestionToOption) : []
