import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import PseudoSelect from './PseudoSelect'

/* eslint-disable react/prop-types */
jest.mock('../../Select', () => props => {
  const optionsJsx = props.options.map(option => (
    <option key={option.key} value={option.key}>
      {option.display}
    </option>
  ))
  return <select data-testid="select">{optionsJsx}</select>
})
/* eslint-enable react/prop-types */

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('PseudoSelect ', () => {
        test('should render a Select component', () => {
          const options = {isMulti: false}
          const value = {
            options: [
              {key: '1', display: 'label1', selected: true},
              {key: '2', display: 'label2', selected: false}
            ]
          }

          testingLibrary.renderWithIntl(<PseudoSelect options={options} value={value} onChange={() => {}} />)
          expect(screen.queryAllByTestId('select')).to.have.length(1)
          const select = screen.queryByTestId('select')
          expect(select.childNodes).to.have.length(value.options.length)
          const allOptionsAreRendered = value.options.every((o, i) => {
            const optionNode = select.childNodes[i]
            return optionNode.getAttribute('value') === `${o.key}` && optionNode.textContent === o.display
          })
          expect(allOptionsAreRendered).to.be.true
        })
      })
    })
  })
})
