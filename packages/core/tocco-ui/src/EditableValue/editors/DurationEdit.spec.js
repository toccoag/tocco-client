import {fireEvent, screen} from '@testing-library/react'
import {expect} from 'chai'
import {testingLibrary} from 'tocco-test-util'

import DurationEdit from './DurationEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('DurationEdit ', () => {
        const implyTargetObject = value => ({target: {value, select: EMPTY_FUNC}})

        test('should render two inputs', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={3720000} onChange={EMPTY_FUNC} />)
          expect(screen.getAllByRole('spinbutton')).to.have.length(2)
        })

        test('should display value in hours and minutes', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={3720000} onChange={EMPTY_FUNC} />)
          jestExpect(screen.getAllByRole('spinbutton').at(0)).toHaveValue(1)
          jestExpect(screen.getAllByRole('spinbutton').at(1)).toHaveValue(2)
        })

        test('should call onChange with milliseconds', () => {
          const onInputSpy = sinon.spy()
          testingLibrary.renderWithIntl(<DurationEdit value={60000} onChange={onInputSpy} />)
          const timeTestInput = '1'
          fireEvent.input(screen.getAllByRole('spinbutton').at(0), implyTargetObject(timeTestInput))
          expect(onInputSpy).to.be.calledWith(3660000)
        })

        test('should call onChange with null on invalid input', () => {
          const onInputSpy = sinon.spy()
          testingLibrary.renderWithIntl(<DurationEdit value={0} onChange={onInputSpy} />)
          const invalidInput = '..'
          const expectedCallValue = null
          fireEvent.input(screen.getAllByRole('spinbutton').at(0), implyTargetObject(invalidInput))
          expect(onInputSpy).to.be.calledWith(expectedCallValue)
        })

        test('should set immutable prop to true', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={null} onChange={EMPTY_FUNC} immutable />)
          jestExpect(screen.getAllByRole('spinbutton').at(0)).toBeDisabled()
          jestExpect(screen.getAllByRole('spinbutton').at(1)).toBeDisabled()
        })

        test('should always show units', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={0} onChange={EMPTY_FUNC} />)
          expect(screen.queryByText('hrs')).to.exist
          expect(screen.queryByText('min')).to.exist
        })

        test('should show and hide units', () => {
          testingLibrary.renderWithIntl(<DurationEdit onChange={EMPTY_FUNC} />)
          expect(screen.queryByText('hrs')).to.not.exist
          expect(screen.queryByText('min')).to.not.exist
          fireEvent.focus(screen.getAllByRole('spinbutton').at(0))
          expect(screen.queryByText('hrs')).to.exist
          expect(screen.queryByText('min')).to.exist
          fireEvent.blur(screen.getAllByRole('spinbutton').at(0))
          expect(screen.queryByText('hrs')).to.not.exist
          expect(screen.queryByText('min')).to.not.exist
        })

        test('should show seconds on small duration when read only', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={329} immutable onChange={EMPTY_FUNC} />)
          expect(screen.queryAllByText('hrs')).to.have.length(1)
          expect(screen.queryAllByText('min')).to.have.length(1)
          expect(screen.queryAllByText('s')).to.have.length(1)
          jestExpect(screen.getAllByRole('spinbutton').at(0)).toHaveValue(0)
          jestExpect(screen.getAllByRole('spinbutton').at(1)).toHaveValue(0)
          jestExpect(screen.getAllByRole('spinbutton').at(2)).toHaveValue(0.329)
        })

        test('should not show seconds on small duration when editable', () => {
          testingLibrary.renderWithIntl(<DurationEdit value={329} onChange={EMPTY_FUNC} />)
          expect(screen.queryByText('hrs')).to.exist
          expect(screen.queryByText('min')).to.exist
          expect(screen.queryByText('s')).to.not.exist
          jestExpect(screen.getAllByRole('spinbutton').at(0)).toHaveValue(null)
          jestExpect(screen.getAllByRole('spinbutton').at(1)).toHaveValue(0)
        })
      })
    })
  })
})
