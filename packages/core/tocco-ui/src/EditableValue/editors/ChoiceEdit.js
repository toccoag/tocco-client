import PropTypes from 'prop-types'

import HtmlFormatter from '../../FormattedValue/formatters/HtmlFormatter'

import {StyledOption, StyledLabel} from './StyledChoicEdit'

const INPUT_TYPE = {
  single: 'radio',
  multiple: 'checkbox'
}

const NOT_CHECKED_OPTION = {
  single: {checked: false},
  multiple: {}
}

const getLabel = (label, renderHtmlInOptions) => (renderHtmlInOptions ? <HtmlFormatter value={label} /> : label)

const AnswerOption = ({
  groupName,
  id,
  label,
  labelComponent,
  checked,
  immutable,
  hideCheckbox,
  selectionType,
  renderHtmlInOptions,
  handleChange
}) => (
  <StyledOption>
    {!hideCheckbox && (
      <input
        name={groupName}
        type={INPUT_TYPE[selectionType]}
        checked={checked || false}
        onChange={handleChange}
        id={id}
        disabled={immutable}
        data-cy={`choice-answer-option-${id}`}
      />
    )}
    <StyledLabel>{labelComponent || getLabel(label, renderHtmlInOptions)}</StyledLabel>
  </StyledOption>
)

AnswerOption.propTypes = {
  groupName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  labelComponent: PropTypes.node,
  checked: PropTypes.bool.isRequired,
  immutable: PropTypes.bool,
  hideCheckbox: PropTypes.bool,
  selectionType: PropTypes.oneOf(['single', 'multiple']),
  renderHtmlInOptions: PropTypes.bool,
  handleChange: PropTypes.func
}

const ChoiceEdit = ({onChange, value, options, id, immutable}) => {
  const handleChange = e => {
    if (onChange) {
      const newSelectedOptions = value.options.map(o => ({
        ...o,
        ...(o.id === e.target.id ? {checked: e.target.checked} : NOT_CHECKED_OPTION[options.selectionType])
      }))
      onChange({options: newSelectedOptions})
    }
  }

  return (
    <>
      {value.options.map(o => (
        <AnswerOption
          groupName={id}
          key={o.id}
          id={o.id}
          label={o.label}
          labelComponent={o.labelComponent}
          checked={o.checked}
          immutable={immutable || o.immutable}
          hideCheckbox={o.hideCheckbox}
          selectionType={options.selectionType}
          renderHtmlInOptions={options.renderHtmlInOptions}
          handleChange={handleChange}
        />
      ))}
    </>
  )
}

ChoiceEdit.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.shape({
    options: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        label: PropTypes.string,
        labelComponent: PropTypes.node,
        checked: PropTypes.bool.isRequired,
        immutable: PropTypes.bool,
        hideCheckbox: PropTypes.bool
      })
    )
  }),
  options: PropTypes.shape({
    selectionType: PropTypes.oneOf(['single', 'multiple']),
    renderHtmlInOptions: PropTypes.bool
  }),
  id: PropTypes.string,
  immutable: PropTypes.bool
}

export default ChoiceEdit
