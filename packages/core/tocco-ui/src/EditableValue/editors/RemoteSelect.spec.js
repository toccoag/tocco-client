import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import RemoteSelect from './RemoteSelect'

// eslint-disable-next-line react/prop-types
jest.mock('../../Select', () => props => <div data-testid="select">{props.noResultsText}</div>)

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('RemoteSelect ', () => {
        test('should render a Select component', () => {
          const options = {
            noResultsText: 'NO_RESULTS_TEXT'
          }

          testingLibrary.renderWithIntl(
            <RemoteSelect options={options} value={{key: 2, display: 'Two'}} onChange={() => {}} />
          )

          expect(screen.queryAllByTestId('select')).to.have.length(1)
          expect(screen.queryByTestId('select').textContent).to.be.eql('NO_RESULTS_TEXT')
        })
      })
    })
  })
})
