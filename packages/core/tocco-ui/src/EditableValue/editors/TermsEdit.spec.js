import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import TermsEdit from './TermsEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('TermsEdit ', () => {
        test('should render checkbox', () => {
          const loadTermsSpy = sinon.spy()
          const options = {
            loadTerms: loadTermsSpy,
            terms: {
              label: 'AGB',
              link: null,
              checkbox: true
            }
          }
          testingLibrary.renderWithTheme(<TermsEdit options={options} onChange={EMPTY_FUNC} />)
          expect(screen.queryAllByRole('checkbox')).to.have.length(1)
          expect(screen.queryAllByRole('link')).to.have.length(0)
          expect(screen.getAllByText('AGB')).to.have.length(1)
          expect(loadTermsSpy).to.have.been.calledOnce
        })

        test('should render link', () => {
          const loadTermsSpy = sinon.spy()
          const options = {
            loadTerms: loadTermsSpy,
            terms: {
              label: 'AGB',
              link: 'https://tocco.ch/agb',
              checkbox: false
            }
          }
          testingLibrary.renderWithTheme(<TermsEdit options={options} onChange={EMPTY_FUNC} />)
          expect(screen.queryAllByRole('checkbox')).to.have.length(0)
          expect(screen.queryAllByRole('link')).to.have.length(1)
          expect(screen.getAllByText('AGB')).to.have.length(1)
          expect(loadTermsSpy).to.have.been.calledOnce
        })

        test('should render label without link and checkbox', () => {
          const loadTermsSpy = sinon.spy()
          const options = {
            loadTerms: loadTermsSpy,
            terms: {
              label: 'AGB',
              link: null,
              checkbox: false
            }
          }
          testingLibrary.renderWithTheme(<TermsEdit options={options} onChange={EMPTY_FUNC} />)
          expect(screen.queryAllByRole('checkbox')).to.have.length(0)
          expect(screen.queryAllByRole('link')).to.have.length(0)
          expect(screen.getAllByText('AGB')).to.have.length(1)
          expect(loadTermsSpy).to.have.been.calledOnce
        })
      })
    })
  })
})
