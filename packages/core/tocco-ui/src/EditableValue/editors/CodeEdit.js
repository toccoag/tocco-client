import PropTypes from 'prop-types'
import {withTheme} from 'styled-components'
import {react} from 'tocco-util'

import CodeEditor from '../../CodeEditor'

import {StyledEditorWrapper} from './StyledCodeEdit'

const CodeEdit = ({value, onChange, immutable, options, id, theme}) => (
  <StyledEditorWrapper>
    <CodeEditor
      value={value}
      id={id}
      onChange={onChange}
      autoFocusOnMount={options.autoFocusOnMount}
      mode={options.mode}
      theme={theme.codeEditorTheme}
      showGutter={true}
      editorOptions={{
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: true,
        minLines: 5,
        maxLines: 50,
        readOnly: immutable,
        wrap: true,
        indentedSoftWrap: false
      }}
      implicitModel={options.implicitModel}
    />
  </StyledEditorWrapper>
)

CodeEdit.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  id: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    mode: PropTypes.string.isRequired,
    autoFocusOnMount: PropTypes.bool,
    implicitModel: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.shape({
        entityModel: PropTypes.string,
        field: PropTypes.string,
        key: PropTypes.string
      })
    ])
  }),
  theme: PropTypes.shape({
    codeEditorTheme: PropTypes.string
  }).isRequired
}

export default withTheme(react.Debouncer(CodeEdit))
