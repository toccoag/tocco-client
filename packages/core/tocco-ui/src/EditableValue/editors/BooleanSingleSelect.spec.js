import {fireEvent, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import BooleanSingleSelect from './BooleanSingleSelect'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('BooleanSingleSelect ', () => {
        test('should render a Select component', async () => {
          testingLibrary.renderWithIntl(
            <BooleanSingleSelect
              onChange={() => {}}
              options={{
                trueLabel: 'true',
                falseLabel: 'false'
              }}
            />
          )
          const user = userEvent.setup()
          fireEvent.focus(screen.getByRole('combobox'))

          // open menu
          await user.keyboard('[ArrowDown]')

          expect(screen.queryByText('true')).to.exist
          expect(screen.queryByText('false')).to.exist
        })
      })
    })
  })
})
