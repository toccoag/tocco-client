import {waitFor} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DateEdit from './DateEdit'

const EMPTY_FUNC = () => {}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('DateEdit ', () => {
        test('should render an instance of DatePicker', async () => {
          const {container} = testingLibrary.renderWithIntl(<DateEdit onChange={EMPTY_FUNC} />)

          await waitFor(() => {
            const datePicker = container.querySelector('.react-datepicker-wrapper')
            expect(datePicker).to.have.exist
          })
        })
      })
    })
  })
})
