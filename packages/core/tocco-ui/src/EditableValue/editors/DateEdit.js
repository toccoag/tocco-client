import PropTypes from 'prop-types'
import {useIntl} from 'react-intl'
import {date as dateUtils, device} from 'tocco-util'

import DatePicker from './DatePicker'
import DatePickerNative from './DatePickerNative'

export const DateEdit = ({onChange, options, id, value, immutable, events, placeholder}) => {
  const intl = useIntl()

  const datePickerOptions = options?.datePickerOptions || {}

  const DateFormat = `${dateUtils.getLocalizedDateFormat(intl.locale)}` // MM/dd/yyyy or dd.MM.y

  // to support direct input such as 01032020
  const dateFormatWithoutPunctuation = dateUtils.getLocalizedDateFormatWithoutPunctuation(intl.locale)
  const dateFormats = [
    DateFormat,
    dateFormatWithoutPunctuation,
    dateUtils.useTwoDigitYear(DateFormat), // to support direct input such as 01.03.22
    dateUtils.useTwoDigitYear(dateFormatWithoutPunctuation) // to support direct input such as 010322
  ]

  if (device.isPrimaryTouchDevice()) {
    return (
      <DatePickerNative
        id={id}
        value={value}
        onChange={onChange}
        hasTime={false}
        immutable={immutable}
        placeholder={placeholder}
        events={events}
        format={DateFormat}
        {...datePickerOptions}
      />
    )
  }

  return (
    <DatePicker
      id={id}
      value={value}
      onChange={onChange}
      dateFormats={dateFormats}
      hasTime={false}
      immutable={immutable}
      placeholder={placeholder}
      events={events}
      {...datePickerOptions}
    />
  )
}

DateEdit.propTypes = {
  id: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    datePickerOptions: PropTypes.shape({
      minDate: PropTypes.string,
      maxDate: PropTypes.string,
      dateToValue: PropTypes.func,
      valueToDate: PropTypes.func
    })
  }),
  events: PropTypes.object
}

export default DateEdit
