import {
  extractPostcodeCity,
  mapLocationSuggestionsToOptions,
  mapLocationValueToValue,
  mapValueToLocationValue
} from './geoSearchUtils'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('geoSearchUtils', () => {
      describe('extractPostcodeCity', () => {
        test('should return null when no value has been provided', () => {
          const value = undefined
          const searchTerm = extractPostcodeCity(value)
          expect(searchTerm).to.be.null
        })
        test('should extract postcode', () => {
          const value = '8100'
          const searchTerm = extractPostcodeCity(value)
          expect(searchTerm).to.eql({postcode: '8100'})
        })
        test('should extract city', () => {
          const value = 'Zürich'
          const searchTerm = extractPostcodeCity(value)
          expect(searchTerm).to.eql({city: 'Zürich'})
        })
        test('should extract postcode and city', () => {
          const searchTerm1 = extractPostcodeCity('8100 Zürich')
          expect(searchTerm1).to.eql({postcode: '8100', city: 'Zürich'})

          const searchTerm2 = extractPostcodeCity('Zürich 8100')
          expect(searchTerm2).to.eql({postcode: '8100', city: 'Zürich'})
        })
      })

      describe('mapValueToLocationValue', () => {
        test('should not map value if location is not set', () => {
          const value = {distance: 30}
          const locationValue = mapValueToLocationValue(value)
          expect(locationValue).to.eql(undefined)
        })
        test('should map value if location is set', () => {
          const value = {
            distance: 30,
            latitude: 0.8,
            longitude: 0.15,
            key: '8100',
            display: 'Zürich',
            postcode: '8100',
            city: 'Zürich',
            country: {key: 'CH'}
          }
          const locationValue = mapValueToLocationValue(value)
          expect(locationValue).to.eql({
            latitude: 0.8,
            longitude: 0.15,
            key: '8100',
            display: 'Zürich',
            postcode: '8100',
            city: 'Zürich',
            country: {key: 'CH'}
          })
        })
      })

      describe('mapLocationValueToValue', () => {
        test('should remove location properties from value when location value is not set', () => {
          const value = {
            distance: 30,
            latitude: 0.8,
            longitude: 0.15,
            key: '8100',
            display: 'Zürich',
            postcode: '8100',
            city: 'Zürich',
            country: {key: 'CH'}
          }
          const locationValue = null
          const newValue = mapLocationValueToValue(value, locationValue)
          expect(newValue).to.eql({distance: 30})
        })
        test('should update location properties on value when location value is set', () => {
          const value = {
            distance: 30,
            latitude: 0.8,
            longitude: 0.15,
            key: '8100',
            display: 'Zürich',
            postcode: '8100',
            city: 'Zürich',
            country: {key: 'CH'}
          }
          const locationValue = {
            latitude: 0.9,
            longitude: 0.25,
            key: '8500',
            display: 'Frauenfeld',
            postcode: '8500',
            city: 'Frauenfeld',
            country: {key: 'DE'}
          }
          const newValue = mapLocationValueToValue(value, locationValue)
          expect(newValue).to.eql({
            distance: 30,
            latitude: 0.9,
            longitude: 0.25,
            key: '8500',
            display: 'Frauenfeld',
            postcode: '8500',
            city: 'Frauenfeld',
            country: {key: 'DE'}
          })
        })
      })

      describe('mapLocationSuggestionsToOptions', () => {
        test('should handle unset suggestions', () => {
          const options = mapLocationSuggestionsToOptions(undefined)
          expect(options).to.eql([])
        })
        test('should map suggestions and convert coordinates', () => {
          const suggestions = [
            {
              postcode: '8500',
              city: 'Frauenfeld',
              country: {display: 'Schweiz'},
              state: 'TG',
              latitude: 47.55976852979618,
              longitude: 8.897661969621016
            }
          ]
          const options = mapLocationSuggestionsToOptions(suggestions)
          expect(options).to.eql([
            {
              key: '8500',
              display: '8500 Frauenfeld - TG / Schweiz',
              postcode: '8500',
              city: 'Frauenfeld',
              country: {display: 'Schweiz'},
              latitude: 0.8300745523313263,
              longitude: 0.1552934970993704
            }
          ])
        })
      })
    })
  })
})
