import PropTypes from 'prop-types'
import {useEffect, useCallback} from 'react'
import {env} from 'tocco-util'

import {StyledCheckbox, StyledHtmlFormatter, StyledTermsEdit, StyledTermsLink} from './StyledTermsEdit'

const TermsEdit = props => {
  const {loadTerms, terms} = props.options
  const {onChange, value, name, id, immutable} = props

  const buildPayload = useCallback(
    checked => ({
      termsConditionKey: terms.key,
      widgetConfigKey: env.getWidgetConfigKey(),
      checked,
      pickAllProperties: true
    }),
    [terms]
  )

  const clearValue = useCallback(() => onChange(null), [onChange])

  const setValue = useCallback(checked => onChange(buildPayload(checked)), [onChange, buildPayload])

  const handleChange = useCallback(
    checked => {
      if (onChange) {
        if (checked) {
          setValue(checked)
        } else {
          clearValue()
        }
      }
    },
    [onChange, setValue, clearValue]
  )

  const handleClick = e => handleChange(e.target.checked)

  // loadTerms shouldn't be in deps array as TermsEdit is rerendered too often and else each time a rest request is done
  useEffect(() => {
    loadTerms()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  // no checkbox is shown, but terms are always mandatory, so set an unchecked value
  useEffect(() => {
    if (terms && !value && (!terms?.checkbox || immutable)) {
      setValue(false)
    }
  }, [terms, value, immutable, setValue])

  const Checkbox = terms?.checkbox && (
    <StyledCheckbox type="checkbox" checked={value?.checked || false} name={name} onChange={handleClick} id={id} />
  )

  const TermsLink = terms?.link && (
    <StyledTermsLink target="_blank" href={terms.link} rel="noreferrer">
      <StyledHtmlFormatter value={terms.label} breakWords />
    </StyledTermsLink>
  )

  return (
    !immutable && (
      <StyledTermsEdit>
        {Checkbox}
        {TermsLink || <StyledHtmlFormatter value={terms?.label} breakWords />}
      </StyledTermsEdit>
    )
  )
}

TermsEdit.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.shape({checked: PropTypes.bool}),
  name: PropTypes.string,
  id: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    type: PropTypes.string,
    loadTerms: PropTypes.func,
    terms: PropTypes.shape({
      key: PropTypes.string,
      label: PropTypes.string,
      link: PropTypes.string,
      checkbox: PropTypes.bool
    })
  })
}

export default TermsEdit
