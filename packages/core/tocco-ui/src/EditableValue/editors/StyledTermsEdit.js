import styled, {css} from 'styled-components'

import HtmlFormatter from '../../FormattedValue/formatters/HtmlFormatter'
import {scale, themeSelector} from '../../utilStyles'

export const StyledTermsEdit = styled.div`
  margin: ${scale.space(-1.4)} 0;
  display: flex;
`

export const StyledCheckbox = styled.input`
  margin-right: ${scale.space(-1.5)};
`

const styledLink = css`
  &,
  * {
    color: ${themeSelector.color('secondary')};
  }
  text-decoration: underline;

  &:hover,
  &:focus {
    color: ${themeSelector.color('secondaryLight')};
  }

  &:active {
    * {
      color: ${themeSelector.color('secondary')};
    }
  }
`

export const StyledTermsLink = styled.a`
  ${styledLink}
`

export const StyledHtmlFormatter = styled(HtmlFormatter)`
  &&& {
    p {
      margin: 0;
    }

    a {
      ${styledLink}
    }
  }
`
