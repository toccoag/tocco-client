import _pick from 'lodash/pick'
import PropTypes from 'prop-types'

import Select from '../../Select'

const pickForArray = array => array.map(i => _pick(i, ['key', 'display']))

const PseudoSelect = ({id, immutable, onChange, options: {isMulti}, value}) => {
  const handleChange = onChangeOptions => {
    const selectedKeys = isMulti ? onChangeOptions.map(o => o.key) : [onChangeOptions?.key]
    const newSelectedOptions = value.options.map(o => ({
      ...o,
      ...(selectedKeys.includes(o.key) ? {selected: true} : {selected: false})
    }))
    onChange({options: newSelectedOptions})
  }

  return (
    <Select
      id={id}
      immutable={immutable}
      isMulti={isMulti}
      onChange={handleChange}
      value={pickForArray(value.options.filter(o => o.selected))}
      openMenuOnClick
      options={pickForArray(value.options)}
    />
  )
}

PseudoSelect.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.shape({
    options: PropTypes.arrayOf(
      PropTypes.shape({
        key: PropTypes.string.isRequired,
        display: PropTypes.string.isRequired,
        selected: PropTypes.bool.isRequired
      })
    )
  }),
  options: PropTypes.shape({
    isMulti: PropTypes.bool
  }),
  immutable: PropTypes.bool,
  id: PropTypes.string
}

export default PseudoSelect
