import PropTypes from 'prop-types'
import {injectIntl, useIntl} from 'react-intl'
import {date, device} from 'tocco-util'

import DatePicker from './DatePicker'
import DatePickerNative from './DatePickerNative'

export const DateTimeEdit = ({onChange, options, value, immutable, placeholder}) => {
  const intl = useIntl()

  const datePickerOptions = options?.datePickerOptions || {}

  const DateTimeFormat = `${date.getLocalizedDateTimeFormat(intl.locale)}` // MM/dd/yyyy HH:mm or dd.MM.y HH:mm
  const DateFormat = `${date.getLocalizedDateFormat(intl.locale)}` // MM/dd/yyyy or dd.MM.y

  // to support direct input such as 01032020 1230
  const dateTimeFormatWithoutPunctuation = date.getLocalizedDateTimeFormatWithoutPunctuation(intl.locale)
  const dateTimeFormats = [
    DateTimeFormat,
    dateTimeFormatWithoutPunctuation,
    date.useTwoDigitYear(DateTimeFormat), // to support direct input such as 01.03.22 12:30
    date.useTwoDigitYear(dateTimeFormatWithoutPunctuation) // to support direct input such as 010322 12:30
  ]

  const dateFormatWithoutPunctuation = date.getLocalizedDateFormatWithoutPunctuation(intl.locale)
  const dateFormats = [
    DateFormat,
    dateFormatWithoutPunctuation,
    date.useTwoDigitYear(DateFormat), // to support direct input such as 01.03.22
    date.useTwoDigitYear(dateFormatWithoutPunctuation) // to support direct input such as 010322
  ]

  if (device.isPrimaryTouchDevice()) {
    return (
      <DatePickerNative
        value={value}
        onChange={onChange}
        hasTime
        immutable={immutable}
        placeholder={placeholder}
        format={DateTimeFormat}
        {...datePickerOptions}
      />
    )
  }

  return (
    <DatePicker
      value={value}
      onChange={onChange}
      dateTimeFormats={dateTimeFormats}
      dateFormats={dateFormats}
      hasTime
      immutable={immutable}
      placeholder={placeholder}
      {...datePickerOptions}
    />
  )
}

DateTimeEdit.propTypes = {
  intl: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
  immutable: PropTypes.bool,
  options: PropTypes.shape({
    datePickerOptions: PropTypes.shape({
      minDate: PropTypes.string,
      maxDate: PropTypes.string,
      dateToValue: PropTypes.func,
      valueToDate: PropTypes.func
    })
  })
}

export default injectIntl(DateTimeEdit)
