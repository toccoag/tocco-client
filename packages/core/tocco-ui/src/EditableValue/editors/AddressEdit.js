import PropTypes from 'prop-types'
import Autosuggest from 'react-autosuggest'
import FocusWithin from 'react-simple-focus-within'

import Link from '../../Link'
import LoadingSpinner from '../../LoadingSpinner'
import Typography from '../../Typography'
import {StyledEditableControl} from '../StyledEditableValue'
import {getGoogleMapsAddressLink} from '../utils'

import {StyledAddressEdit} from './StyledLocationEdit'

const AddressEdit = ({onChange: onChangeProp, options, value: valueProp, id, immutable, name}) => {
  const returnGetSuggestion = attr => suggestion => suggestion[attr]

  const getAddressLine = item => {
    const {street, houseNr} = item
    if (houseNr) {
      return `${street} ${houseNr}`
    }
    return street
  }

  const renderSuggestion = suggestion => {
    const {street, state, country, postcode, city} = suggestion

    if (!street) {
      return null
    }

    const addressLine = getAddressLine(suggestion)

    const cantonString = state ? `- ${state}` : ''
    const countryString = country ? `/ ${country.display}` : ''

    return (
      <Typography.Span>
        {addressLine}, {postcode} {city} {cantonString} {countryString}
      </Typography.Span>
    )
  }

  const onChange =
    field =>
    (event, {newValue}) => {
      onChangeProp({...valueProp, [field]: newValue})
    }

  const onSuggestionSelected = (event, {suggestion}) => {
    if (suggestion.validationRequired === true) {
      options.validateLocation(suggestion, onChangeProp)
    } else {
      onChangeProp(suggestion)
    }
  }

  const returnOnSuggestionFetchRequested = ({value: street}) => {
    const {country} = options.locationValues
    const searchParams = {
      street
    }
    options.fetchSuggestions(searchParams, country)
  }

  const showGoogleMaps = ({street, city, postcode}) => Boolean(street && (city || postcode))

  const value = valueProp && valueProp.street ? getAddressLine(valueProp) : ''

  const inputProps = {
    id,
    value,
    onChange: onChange('street'),
    disabled: immutable,
    name: 'address-line1',
    autocomplete: 'street-address'
  }

  return (
    <FocusWithin>
      {({getRef}) => (
        <StyledAddressEdit immutable={immutable} name={name} ref={getRef}>
          <Autosuggest
            suggestions={options.suggestions || []}
            onSuggestionsFetchRequested={returnOnSuggestionFetchRequested}
            getSuggestionValue={returnGetSuggestion('street')}
            renderSuggestion={renderSuggestion}
            onSuggestionsClearRequested={() => {}}
            inputProps={inputProps}
            onSuggestionSelected={onSuggestionSelected}
            focusInputOnSuggestionClick={false}
            shouldRenderSuggestions={v => v && !immutable}
          />
          <StyledEditableControl>
            {options.isLoading && <LoadingSpinner size="1.8rem" />}
            {showGoogleMaps(options.locationValues) && (
              <Link
                href={getGoogleMapsAddressLink(options.locationValues)}
                icon="map-marked"
                tabIndex={-1}
                target="_blank"
                dense={false}
                title={options.mapButtonTitle || 'Show on map'}
                neutral
              />
            )}
          </StyledEditableControl>
        </StyledAddressEdit>
      )}
    </FocusWithin>
  )
}

const locationObjectPropType = PropTypes.shape({
  city: PropTypes.string,
  postcode: PropTypes.string,
  street: PropTypes.string,
  country: PropTypes.shape({
    display: PropTypes.string,
    key: PropTypes.string
  }),
  canton: PropTypes.string,
  district: PropTypes.string
})

AddressEdit.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.shape({
    street: PropTypes.string
  }),
  options: PropTypes.shape({
    suggestions: PropTypes.arrayOf(locationObjectPropType),
    fetchSuggestions: PropTypes.func,
    validateLocation: PropTypes.func,
    isLoading: PropTypes.bool,
    mapButtonTitle: PropTypes.string,
    locationValues: locationObjectPropType
  }),
  name: PropTypes.string,
  id: PropTypes.string,
  immutable: PropTypes.bool
}

export default AddressEdit
