import PropTypes from 'prop-types'

import Select from '../../Select'

import {
  extractPostcodeCity,
  mapLocationSuggestionsToOptions,
  mapLocationValueToValue,
  mapValueToLocationValue
} from './geoSearchUtils'
import IntegerEdit from './IntegerEdit'
import {StyledDistanceWrapper, StyledLocationWrapper, StyledWrapper} from './StyledGeoSearchEdit'

const GeoSearchEdit = ({id, immutable, value, onChange, options = {}}) => {
  const {
    suggestions,
    isLoading,
    fetchSuggestions,
    latitudeField,
    longitudeField,
    emptySearchText,
    noResultsText,
    kmSuffixText
  } = options

  const distanceValue = value ? value.distance : 0
  const locationValue = mapValueToLocationValue(value)

  const handleLocationChange = newLocationValue => {
    onChange({
      ...mapLocationValueToValue(value, newLocationValue),
      latitudeField,
      longitudeField
    })
  }

  const handleDistanceChange = newDistanceValue => {
    onChange({...value, distance: newDistanceValue})
  }

  const searchLocationOptions = currentValue => {
    const searchOptions = extractPostcodeCity(currentValue)
    fetchSuggestions(searchOptions, null)
  }

  const fetchLocationOptions = currentValue => {
    if (!currentValue) {
      // clear suggestions when no value has been selected
      fetchSuggestions(null, null)
    }
  }

  const locationOptions = mapLocationSuggestionsToOptions(suggestions)

  return (
    <StyledWrapper>
      <StyledLocationWrapper>
        <Select
          id={id}
          immutable={immutable}
          onChange={handleLocationChange}
          isLoading={isLoading}
          options={locationOptions}
          searchOptions={searchLocationOptions}
          fetchOptions={fetchLocationOptions}
          value={locationValue}
          noResultsComp={({inputValue}) => (inputValue ? noResultsText : emptySearchText)}
        />
      </StyledLocationWrapper>
      <StyledDistanceWrapper>
        <IntegerEdit
          value={distanceValue}
          onChange={handleDistanceChange}
          immutable={immutable}
          options={{minValue: 0, maxValue: 100, suffix: ` ${kmSuffixText}`}}
        />
      </StyledDistanceWrapper>
    </StyledWrapper>
  )
}

GeoSearchEdit.propTypes = {
  onChange: PropTypes.func,
  immutable: PropTypes.bool,
  id: PropTypes.string,
  value: PropTypes.shape({
    distance: PropTypes.number,
    key: PropTypes.string,
    display: PropTypes.string,
    postcode: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.object,
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    latitudeField: PropTypes.string,
    longitudeField: PropTypes.string
  }),
  options: PropTypes.shape({
    suggestions: PropTypes.array,
    isLoading: PropTypes.bool,
    fetchSuggestions: PropTypes.func,
    latitudeField: PropTypes.string,
    longitudeField: PropTypes.string
  })
}

export default GeoSearchEdit
