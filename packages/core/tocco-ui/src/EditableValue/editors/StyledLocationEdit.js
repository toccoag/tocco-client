import _get from 'lodash/get'
import styled from 'styled-components'

import {scale, themeSelector} from '../../utilStyles'
import {StyledEditableWrapperCss, StyledInputCss, StyledEditableControl} from '../StyledEditableValue'

export const StyledLocationEdit = styled.div`
  && {
    ${StyledEditableWrapperCss}
    position: relative;

    div {
      :nth-of-type(1) {
        flex: 0 0 13ch;
        margin-right: ${scale.space(-2)};
      }

      :nth-of-type(2) {
        flex: 1 1 auto;
      }
    }

    > span {
      flex: 0 0 auto;
      margin-right: ${scale.space(-2)};
      color: ${({immutable, theme}) => (immutable ? _get(theme, 'colors.textDisabled') : _get(theme, 'colors.text'))};
    }

    ${StyledEditableControl} {
      margin-left: auto;
    }

    .react-autosuggest__input {
      ${StyledInputCss}
      width: 100%;
    }

    .react-autosuggest__container {
      width: 100%;
    }

    .react-autosuggest__suggestions-container--open {
      width: calc(100% + 17.5px);
      background-color: ${themeSelector.color('paper')};
      box-shadow: 0 0 6px ${themeSelector.color('signal.info')};
      border: 1px solid ${themeSelector.color('secondaryLight')};
      bottom: -9px;
      left: -10px;
      max-height: 300px;
      overflow-y: auto;
      position: absolute;
      transform: translateY(100%);
      z-index: 2; /* higher than StyledIndicatorsContainerWrapper */
    }

    .react-autosuggest__suggestions-list {
      margin: 0;
      padding: 0;
      list-style-type: none;
    }

    .react-autosuggest__suggestion {
      cursor: pointer;
      padding: 8px 12px;
    }

    .react-autosuggest__suggestion--highlighted {
      background-color: ${themeSelector.color('backgroundItemHover')};
    }
  }
`

export const StyledAddressEdit = styled(StyledLocationEdit)`
  && {
    div {
      :nth-of-type(1) {
        flex: auto;
        width: 100%;
      }
    }
  }
`
