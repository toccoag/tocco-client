import {format} from 'date-fns'
import PropTypes from 'prop-types'
import {useRef} from 'react'
import {injectIntl} from 'react-intl'
import {withTheme} from 'styled-components'

import Ball from '../../Ball'

import {StyledDateInput, StyledDateInputWrapper, StyledClearButton} from './StyledDatePickerNative'

const convertISOToDatetimeLocal = value => (value ? format(value, "yyyy-MM-dd'T'HH:mm") : value)
const convertDatetimeLocalToISO = value => (value ? new Date(value).toISOString() : value)

/**
 * Native date(time)picker
 *  - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/date
 *  - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/datetime-local
 *
 * The native date picker can be used as "fallback" instead of the `DatePicker`.
 * The implementation details are up to browsers and cannot be changed much.
 *
 * In contrary to the `DatePicker` the date(time) can only be entered by the picker.
 * No keyboard input is supported!
 *
 * date:
 *  - format of value: `yyyy-MM-dd`
 *  - does not have to be converted
 * datetime-local:
 *  - format of value: `yyyy-MM-ddTHH:mm`
 *  - value has to be converted to ISO string
 */
const DatePickerNative = ({value, onChange, immutable, hasTime, minDate, maxDate, format: formatStr, intl}) => {
  const msg = msgId => intl.formatMessage({id: msgId})
  const inputRef = useRef(null)

  const hasValue = Boolean(value)
  const showClearButton = !immutable && hasValue

  const convertISOToValue = d => (hasTime ? convertISOToDatetimeLocal(d) : d)
  const convertValueToISO = d => (hasTime ? convertDatetimeLocalToISO(d) : d)

  const handleChange = e => {
    onChange(convertValueToISO(e.target.value))
  }

  const handleClearClick = e => {
    onChange(null)

    e.preventDefault()
    e.stopPropagation()
  }

  return (
    <StyledDateInputWrapper>
      <span>{value ? format(value, formatStr) : ''}</span>
      <span>
        <StyledDateInput
          type={hasTime ? 'datetime-local' : 'date'}
          value={convertISOToValue(value) || ''}
          onChange={handleChange}
          {...(minDate ? {min: convertISOToValue(minDate)} : {})}
          {...(maxDate ? {max: convertISOToValue(maxDate)} : {})}
          ref={inputRef}
        />
        {showClearButton && (
          <StyledClearButton
            data-cy="btn-clear"
            icon="times"
            tabIndex={-1}
            aria-label={msg('client.component.datePicker.clearDateLabel')}
            onClick={handleClearClick}
          />
        )}
        {!immutable && <Ball data-cy="btn-calendar" icon="calendar" tabIndex={-1} />}
      </span>
    </StyledDateInputWrapper>
  )
}

DatePickerNative.propTypes = {
  id: PropTypes.string,
  intl: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  hasTime: PropTypes.bool,
  immutable: PropTypes.bool,
  minDate: PropTypes.string,
  maxDate: PropTypes.string,
  format: PropTypes.string.isRequired,
  events: PropTypes.object
}

export default withTheme(injectIntl(DatePickerNative))
