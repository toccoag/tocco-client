import {renderHook, act} from '@testing-library/react'

import useTimeEdit from './useTimeEdit'

const changeValue = (result, value) => {
  act(() => {
    result.current.inputProps.onChange({target: {value}})
  })
}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('useTimeEdit', () => {
      const defaultValue = '12:45'
      const onChangeSpy = sinon.spy()
      const defaultArgs = [defaultValue, onChangeSpy]

      beforeEach(() => {
        onChangeSpy.resetHistory()
      })

      test('should format value without seconds', () => {
        const value = '12:45:00'
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        expect(result.current.inputProps.value).to.eql('12:45')
      })

      test('should handle null value', () => {
        const value = null
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        expect(result.current.inputProps.value).to.eql('')
      })

      test('should update input value on value change', async () => {
        const {result, rerender} = renderHook(({value, onChange}) => useTimeEdit(value, onChange), {
          initialProps: {
            value: '12:45:00',
            onChange: onChangeSpy
          }
        })

        rerender({value: '13:56'})

        expect(result.current.inputProps.value).to.eql('13:56')
      })

      test('should change input value for valid intermediate inputs', () => {
        const value = null
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '1')

        expect(result.current.inputProps.value).to.eql('1')
      })

      test('should allow adjusting hours with valid minutes', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        changeValue(result, '1:45')

        expect(result.current.inputProps.value).to.eql('1:45')
      })

      test('should prevent invalid hours', () => {
        const value = null
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '3')
        changeValue(result, '33')

        expect(result.current.inputProps.value).to.eql('3')
      })

      test('should prevent invalid minutes', () => {
        const value = null
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '12:45')
        changeValue(result, '12:98')

        expect(result.current.inputProps.value).to.eql('12:45')
      })

      test('should prevent invalid characters', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        changeValue(result, 'a')

        expect(result.current.inputProps.value).to.eql('12:45')
      })

      test('should add `:` automatically when start typing the hours', () => {
        const value = null
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '1')
        changeValue(result, '12')

        expect(result.current.inputProps.value).to.eql('12:')
      })

      test('should remove `:` automatically when removing minutes', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        changeValue(result, '12:4')
        changeValue(result, '12:')
        changeValue(result, '12')

        expect(result.current.inputProps.value).to.eql('1')
      })

      test('should remove `:` automatically when having only one hour digit', () => {
        const value = ''
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '9:45')
        changeValue(result, '9:4')
        changeValue(result, '9:')
        changeValue(result, '9')

        expect(result.current.inputProps.value).to.eql('')
      })

      test('should call onChange callback for intermediate values and complete input value', () => {
        const value = ''
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '13:')

        expect(result.current.inputProps.value).to.eql('13:')
        expect(onChangeSpy).to.have.been.calledWith('13:00')
      })

      test('should call onChange callback with valid value', () => {
        const value = ''
        const {result} = renderHook(() => useTimeEdit(value, onChangeSpy))

        changeValue(result, '13:45')

        expect(result.current.inputProps.value).to.eql('13:45')
        expect(onChangeSpy).to.have.been.calledWith('13:45')
      })

      test('should call onChange callback with empty value', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        changeValue(result, '')

        expect(result.current.inputProps.value).to.eql('')
        expect(onChangeSpy).to.have.been.calledWith('')
      })

      test('should interpret value on blur as valid time', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        changeValue(result, '2')

        expect(result.current.inputProps.value).to.eql('2')

        act(() => {
          result.current.inputProps.onBlur()
        })

        expect(result.current.inputProps.value).to.eql('02:00')
      })

      test('should clear input value and call onChange callback', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        act(() => {
          result.current.clearButtonProps.onClick()
        })

        expect(result.current.inputProps.value).to.eql('')
        expect(onChangeSpy).to.have.been.calledWith('')
      })

      test('should select value on focus', () => {
        const {result} = renderHook(() => useTimeEdit(...defaultArgs))

        const focusEvent = {target: {select: sinon.spy()}}
        act(() => {
          result.current.inputProps.onFocus(focusEvent)
        })

        expect(focusEvent.target.select).to.have.been.called
      })
    })
  })
})
