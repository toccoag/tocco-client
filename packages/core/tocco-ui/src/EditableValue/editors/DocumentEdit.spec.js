import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import DocumentEdit from './DocumentEdit'

const EMPTY_FUNC = () => {}

const mockValue = {
  mimeType: 'image/png',
  fileExtension: 'png',
  sizeInBytes: 3336,
  fileName: 'example.js',
  thumbnailLink: '',
  binaryLink: ''
}

const mockOptions = {
  uploadText: 'upload',
  uploadingText: 'uploading...',
  upload: EMPTY_FUNC
}

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('DocumentEdit', () => {
        test('should render', async () => {
          testingLibrary.renderWithIntl(<DocumentEdit value={mockValue} options={mockOptions} />)
          await screen.findAllByTestId('icon')

          expect(screen.queryByRole('button')).to.exist
        })
      })
    })
  })
})
