import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SearchFilterEdit from './SearchFilterEdit'

jest.mock('../../Select', () => props => <div data-testid="select" />)

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('SearchFilterEdit ', () => {
        test('should render a Single-Select component', () => {
          const options = {
            isMulti: false
          }

          testingLibrary.renderWithIntl(<SearchFilterEdit options={options} value={{key: '1'}} onChange={() => {}} />)
          expect(screen.queryAllByTestId('select')).to.have.length(1)
        })

        test('should render a Multi-Select Component', () => {
          const options = {
            isMulti: true
          }

          testingLibrary.renderWithIntl(<SearchFilterEdit options={options} value={[{key: '1'}]} onChange={() => {}} />)
          expect(screen.queryAllByTestId('select')).to.have.length(1)
        })
      })
    })
  })
})
