import {screen} from '@testing-library/react'
import {expect} from 'chai'
import {testingLibrary} from 'tocco-test-util'

import EmailEdit from './EmailEdit'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('typeEditors', () => {
      describe('EmailEdit ', () => {
        test('should show the value and an email address', async () => {
          const value = 'support@tocco.ch'
          testingLibrary.renderWithIntl(<EmailEdit value={value} />)
          await screen.findAllByTestId('icon')

          jestExpect(screen.getByRole('textbox')).toHaveValue(value)
          expect(screen.getByRole('link')).to.exist
        })

        test('should not show the email icon if field is empty', () => {
          const value = ''
          testingLibrary.renderWithIntl(<EmailEdit value={value} />)
          expect(screen.queryByRole('link')).to.not.exist
        })
      })
    })
  })
})
