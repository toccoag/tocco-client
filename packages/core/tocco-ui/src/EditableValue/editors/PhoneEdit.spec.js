import {screen, waitFor, fireEvent} from '@testing-library/react'
import {expect} from 'chai'
import {testingLibrary} from 'tocco-test-util'

import PhoneEdit from './PhoneEdit'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('editors', () => {
      describe('PhoneEdit ', () => {
        test('should call on change with phone number in e.164 format', async () => {
          const onChangeSpy = sinon.spy()
          const onLibLoaded = sinon.spy()
          testingLibrary.renderWithIntl(
            <PhoneEdit value="0792345678" onChange={onChangeSpy} onLibLoaded={onLibLoaded} />
          )

          await waitFor(() => {
            expect(onLibLoaded).to.have.been.calledOnce
          })
          fireEvent.change(screen.getByRole('textbox'), {target: {value: '0793456789'}})
          expect(onChangeSpy).to.have.been.calledWith('+41793456789')
        })
      })
    })
  })
})
