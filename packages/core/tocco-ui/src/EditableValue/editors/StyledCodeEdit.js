import styled from 'styled-components'

import {scale} from '../../utilStyles'

export const StyledEditorWrapper = styled.div`
  margin-top: ${scale.space(-1.5)};
`
