import styled from 'styled-components'

import {scale, themeSelector} from '../../utilStyles'

export const StyledWrapper = styled.div`
  display: flex;
  justify-content: stretch;
`

export const StyledLocationWrapper = styled.div`
  flex-grow: 1;
`

export const StyledDistanceWrapper = styled.div`
  width: 50px;
  border-left: 1px solid ${themeSelector.color('border')};
  margin-left: ${scale.space(-0.8)};
  padding-left: ${scale.space(0.5)};
`
