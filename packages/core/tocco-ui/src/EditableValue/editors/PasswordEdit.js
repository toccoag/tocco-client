import PropTypes from 'prop-types'
import {useState} from 'react'

import Button from '../../Button'
import {StyledEditableControl, StyledEditableWrapper} from '../StyledEditableValue'

import {StyledPasswordEdit} from './StyledPasswordEdit'

const PasswordEdit = ({value, onChange, id, events, immutable, options}) => {
  const [type, setType] = useState('password')

  const {autoFocus, extraHigh, name, required} = options

  return (
    <StyledEditableWrapper immutable={immutable}>
      <StyledPasswordEdit
        autoFocus={autoFocus}
        disabled={immutable}
        id={id}
        name={name}
        onChange={onChange}
        onFocus={events?.onFocus}
        onAnimationStart={events?.onAnimationStart}
        value={value || ''}
        required={required}
        type={type}
        data-cy={options['data-cy']}
        extraHigh={extraHigh}
      />
      {!immutable && (
        <StyledEditableControl>
          {type === 'password' ? (
            <Button icon="eye" iconOnly onClick={() => setType('text')} tabIndex={-1} />
          ) : (
            <Button icon="eye-slash" iconOnly onClick={() => setType('password')} tabIndex={-1} />
          )}
        </StyledEditableControl>
      )}
    </StyledEditableWrapper>
  )
}

PasswordEdit.propTypes = {
  events: PropTypes.object,
  options: PropTypes.shape({
    name: PropTypes.string,
    'data-cy': PropTypes.string,
    autoFocus: PropTypes.bool,
    extraHigh: PropTypes.bool,
    required: PropTypes.bool
  }),
  onChange: PropTypes.func,
  value: PropTypes.node,
  id: PropTypes.string,
  immutable: PropTypes.bool
}

export default PasswordEdit
