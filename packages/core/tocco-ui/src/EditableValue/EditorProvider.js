import _isEmpty from 'lodash/isEmpty'
import _omit from 'lodash/omit'
import PropTypes from 'prop-types'

import Popover from '../Popover'

import AddressEdit from './editors/AddressEdit'
import BooleanSingleSelect from './editors/BooleanSingleSelect'
import BoolEdit from './editors/BoolEdit'
import ChoiceEdit from './editors/ChoiceEdit'
import CodeEdit from './editors/CodeEdit'
import DateEdit from './editors/DateEdit'
import DateTimeEdit from './editors/DateTimeEdit'
import DocumentEdit from './editors/DocumentEdit'
import DurationEdit from './editors/DurationEdit'
import EmailEdit from './editors/EmailEdit'
import GeoSearchEdit from './editors/GeoSearchEdit'
import HtmlEdit from './editors/HtmlEdit'
import IntegerEdit from './editors/IntegerEdit'
import LocationEdit from './editors/LocationEdit'
import MultiRemoteSelect from './editors/MultiRemoteSelect'
import MultiSelect from './editors/MultiSelect'
import NumberEdit from './editors/NumberEdit'
import PasswordEdit from './editors/PasswordEdit'
import PhoneEdit from './editors/PhoneEdit'
import PseudoMultiDocumentEdit from './editors/PseudoMultiDocumentEdit'
import PseudoSelect from './editors/PseudoSelect'
import RemoteSelect from './editors/RemoteSelect'
import SearchFilterEdit from './editors/SearchFilterEdit'
import SingleSelect from './editors/SingleSelect'
import StringEdit from './editors/StringEdit'
import TermsEdit from './editors/TermsEdit'
import TextEdit from './editors/TextEdit'
import TimeEdit from './editors/TimeEdit'
import UrlEdit from './editors/UrlEdit'

/**
 * Mapping: componentType (e.g. document) to component
 */
export const map = {
  boolean: BoolEdit,
  'boolean-select': BooleanSingleSelect,
  choice: ChoiceEdit,
  code: CodeEdit,
  date: DateEdit,
  datetime: DateTimeEdit,
  document: DocumentEdit,
  duration: DurationEdit,
  email: EmailEdit,
  geosearch: GeoSearchEdit,
  html: HtmlEdit,
  integer: IntegerEdit,
  address: AddressEdit,
  location: LocationEdit,
  'multi-remote': MultiRemoteSelect,
  'multi-select': MultiSelect,
  number: NumberEdit,
  password: PasswordEdit,
  phone: PhoneEdit,
  'pseudo-select': PseudoSelect,
  'pseudo-multi-document': PseudoMultiDocumentEdit,
  remote: RemoteSelect,
  'search-filter': SearchFilterEdit,
  'single-select': SingleSelect,
  string: StringEdit,
  terms: TermsEdit,
  text: TextEdit,
  url: UrlEdit,
  time: TimeEdit
}

const checkboxComponentTypes = ['boolean', 'choice', 'terms']

const EditorProvider = ({componentType, value, options, id, events, placeholder, readOnly = false, popoverContent}) => {
  if (map[componentType]) {
    const Component = map[componentType]

    /**
     * Redux-Form Blur Workaround
     *  - onBlur event takes `event.nativeEvent.text` as a field value
     *    - https://github.com/redux-form/redux-form/blob/d1067cb6f0fb76d76d91462676cd0b2c3927f9db/src/events/getValue.js#L23
     *    - https://github.com/redux-form/redux-form/blob/master/src/ConnectedField.js#L176
     *
     *  - for components with non-text values (e.g. IntegerEdit)
     *    blurring the field will reset the value to the corresponding text value
     *    and any parsing functions on onChange will not get triggered anymore
     *
     * React Select Blur Workaround
     *  - blurring the select input will empty the value
     *    - known react-select issue: https://github.com/erikras/redux-form/issues/82
     *    - passing the value to the onBlur event handler keeps the selected value
     */
    const handleBlur = () => {
      if (typeof events?.onBlur === 'function') {
        events.onBlur(value)
      }
    }

    /**
     * blur workaround for Safari
     * - Safari does not trigger onBlur when clicking on a checkbox
     * https://github.com/redux-form/redux-form/issues/3330
     * https://github.com/redux-form/redux-form/issues/878#issuecomment-286815716
     *
     * - without blur event the field is not touched
     * - we only show errors on toched fields
     *
     * solution:
     * - invoke onBlur event for redux field manually
     * - sets touched to choice field
     */
    const handleClick = e => {
      if (checkboxComponentTypes.includes(componentType)) {
        if (typeof events?.onClick === 'function') {
          events.onClick(e)
        }

        if (typeof events?.onBlur === 'function') {
          events.onBlur()
        }
      }
    }

    /**
     * Form event handlers passed in by redux-forms.
     * To overwrite default behaviour attach specific event handler to child element and
     * use `event.stopPropagation()` to not let event bubble up.
     *
     * the redux form events onDrop and onDragStart are not passed as form event handlers
     * because else dragging a file over a string field will clear it and dragging the
     * preview image of a document will clear the field.
     */
    const formEventHandlers = {
      ..._omit(events, ['onChange', 'onDrop', 'onDragStart']),
      onBlur: handleBlur,
      onClick: handleClick
    }

    return (
      <div {...formEventHandlers} data-cy="form-field">
        <Popover content={popoverContent}>
          <Component
            value={value}
            onChange={events?.onChange}
            {...(_isEmpty(options) ? {} : {options})}
            id={id}
            immutable={readOnly}
            placeholder={placeholder}
            events={events}
          />
        </Popover>
      </div>
    )
  }

  // eslint-disable-next-line no-console
  console.log('No type-editor defined for component type', componentType)
  return null
}

EditorProvider.propTypes = {
  componentType: PropTypes.oneOf(Object.keys(map)).isRequired,
  events: PropTypes.shape({
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onDragStart: PropTypes.func,
    onDrop: PropTypes.func,
    onClick: PropTypes.func
  }),
  id: PropTypes.string,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  options: PropTypes.object,
  value: PropTypes.any,
  popoverContent: PropTypes.node
}

export default EditorProvider
