import EditableValue from './'

export default {
  title: 'Tocco-UI/EditableValue',
  component: EditableValue,
  decorators: [
    Story => (
      <div
        style={{
          border: '1px solid #7e7e7e',
          padding: '2px',
          maxWidth: '250px',
          borderRadius: '3px'
        }}
      >
        {Story()}
      </div>
    )
  ],
  argTypes: {
    type: {control: {type: 'text'}},
    events: {defaultValue: {onChange: () => {}}}
  }
}

const EditableValueStory = args => <EditableValue {...args} />

export const BooleanValue = EditableValueStory.bind({})
BooleanValue.args = {value: true, type: 'boolean', title: 'Boolean'}
BooleanValue.argTypes = {value: {type: 'boolean'}}
BooleanValue.storyName = 'Boolean'

export const DateValue = EditableValueStory.bind({})
DateValue.args = {value: '2019-12-18', type: 'date'}
DateValue.argTypes = {value: {type: 'string'}}
DateValue.storyName = 'Date'

export const DateTimeValue = EditableValueStory.bind({})
DateTimeValue.args = {value: '2017-01-25T15:15:00.000Z', type: 'datetime'}
DateTimeValue.storyName = 'Date Time'

export const DocumentValue = EditableValueStory.bind({})
DocumentValue.args = {
  value: {
    mimeType: 'image/png',
    fileExtension: 'png',
    sizeInBytes: 3336,
    fileName: 'Blue-Square.png',
    binaryLink: 'http://link.to/my/image.png',
    thumbnailLink:
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgAQMAAADYVuV7AAAACXBIWXMAAA7' +
      'EAAAOxAGVKw4bAAAABlBMVEUCd72z5fwcX0uLAAAAHElEQVQ4y2NgwAns/8PBn1HOKGeUM8oZrBycAAD' +
      'OggXZNnQmgAAAAABJRU5ErkJggg=='
  },
  type: 'document',
  options: {
    upload: () => {},
    uploadText: 'upload Text',
    uploadingText: 'uploading Text',
    deleteText: 'delete Text',
    downloadText: 'download Text'
  }
}
DocumentValue.argTypes = {value: {type: 'object'}, upload: {action: 'upload'}}
DocumentValue.storyName = 'Document'

export const DurationValue = EditableValueStory.bind({})
DurationValue.args = {value: 83456678, type: 'duration'}
DurationValue.argTypes = {value: {type: 'number'}}
DurationValue.storyName = 'Duration'

export const HtmlValue = EditableValueStory.bind({})
HtmlValue.args = {value: '<h1>Test</h1> TEst <span>TEST</span>', type: 'html'}
HtmlValue.argTypes = {value: {type: 'string'}}
HtmlValue.storyName = 'Html'

export const IntegerValue = EditableValueStory.bind({})
IntegerValue.args = {
  value: 1001,
  type: 'integer',
  options: {
    maxValue: 300000,
    minValue: 1000
  }
}
IntegerValue.argTypes = {value: {type: 'number'}}
IntegerValue.storyName = 'Integer'

export const LocationValue = EditableValueStory.bind({})
LocationValue.args = {
  value: {city: 'Zürich', country: 'CH', postcode: '8000', state: 'ZH', street: 'Bahnhofstrasse 1'},
  type: 'location',
  options: {
    suggestions: [
      {
        address: 'Bundeshaus',
        city: 'Bern',
        country: {display: 'CH'},
        district: 'Ostermundigen',
        postcode: '1234',
        state: 'BE'
      },
      {
        address: 'Rue 123',
        city: 'Lausanne',
        country: {display: 'CH'},
        district: 'District de Lausanne',
        postcode: '5678',
        state: 'VD'
      }
    ],
    fetchSuggestions: () => {},
    isLoading: false,
    mapButtonTitle: 'map Button Title',
    locationValues: {city: 'Zürich', country: 'CH', postcode: '8000', state: 'ZH', street: 'Bahnhofstrasse 1'}
  }
}
LocationValue.argTypes = {value: {type: 'object'}, fetchSuggestions: {action: 'fetchSuggestions'}}
LocationValue.storyName = 'Location'

export const MultiSelectValue = args => (
  <EditableValue
    {...args}
    options={{...args.options, loadTooltip: args.loadTooltip, fetchOptions: args.fetchOptions}}
  />
)
MultiSelectValue.args = {
  value: [
    {key: 2, display: 'V: 2'},
    {key: 3, display: 'V: 3'}
  ],
  type: 'multi-select',
  options: {
    options: Array.from(Array(100).keys()).map(v => ({key: v, display: `V: ${v}`})),
    noResultsText: 'no results found',
    isLoading: false,
    tooltips: {2: 'Tooltip for Two'},
    loadTooltip: () => {},
    fetchOptions: () => {}
  }
}
MultiSelectValue.argTypes = {
  value: {type: 'object'},
  fetchOptions: {action: 'fetchOptions', table: {disable: true}},
  loadTooltip: {action: 'loadTooltip', table: {disable: true}}
}
MultiSelectValue.storyName = 'Multi Select'

export const SingleSelectValue = {...MultiSelectValue}
SingleSelectValue.args = {...SingleSelectValue.args, type: 'single-select', value: {key: 2, display: 'V: 2'}}
SingleSelectValue.storyName = 'Single Select'

export const MultiRemoteValue = args => (
  <EditableValue
    {...args}
    options={{
      ...args.options,
      loadTooltip: args.loadTooltip,
      fetchOptions: args.fetchOptions,
      searchOptions: args.searchOptions,
      openAdvancedSearch: args.openAdvancedSearch
    }}
  />
)
MultiRemoteValue.args = {
  value: [{key: 2, display: 'Two v'}],
  type: 'multi-remote',
  options: {
    options: [
      {key: 1, display: 'One'},
      {key: 2, display: 'Two'},
      {key: 3, display: 'Three'}
    ],
    isLoading: false,
    tooltips: {2: 'Tooltip for Two'},
    clearValueText: 'clear Value Text',
    searchPromptText: 'search Prompt Text',
    noResultsText: 'no results found',
    moreOptionsAvailable: true,
    moreOptionsAvailableText: 'more Options Available Text',
    loadTooltip: () => {},
    fetchOptions: () => {},
    searchOptions: () => {},
    openAdvancedSearch: () => {}
  }
}

MultiRemoteValue.argTypes = {
  value: {type: 'object'},
  fetchOptions: {action: 'fetchOptions'},
  loadTooltip: {action: 'loadTooltip'},
  openAdvancedSearch: {action: 'openAdvancedSearch'},
  searchOptions: {action: 'searchOptions'}
}
MultiRemoteValue.storyName = 'Multi Remote'

export const RemoteValue = {...MultiRemoteValue}
RemoteValue.args = {...MultiRemoteValue.args, type: 'remote', value: {key: 2, display: 'Two v'}}
RemoteValue.storyName = 'Remote'

export const NumberValue = EditableValueStory.bind({})
NumberValue.args = {
  value: 123.45,
  type: 'number',
  options: {
    prePointDigits: 8,
    postPointDigits: 2,
    minValue: -30000,
    maxValue: 30000,
    decimalScale: 5,
    allowNegative: true,
    fixedDecimalScale: true,
    prefix: '#',
    suffix: '%'
  }
}
NumberValue.argTypes = {
  value: {type: 'number'}
}
NumberValue.storyName = 'Number'

export const PhoneValue = EditableValueStory.bind({})
PhoneValue.args = {value: '+41761234567', type: 'phone'}
PhoneValue.argTypes = {value: {type: 'string'}}
PhoneValue.storyName = 'Phone'

export const StringValue = EditableValueStory.bind({})
StringValue.args = {value: 'Test', type: 'string'}
StringValue.argTypes = {value: {type: 'string'}}
StringValue.storyName = 'String'

export const TextValue = EditableValueStory.bind({})
TextValue.args = {value: 'This is the first line.\nTo be continued...', type: 'text'}
TextValue.argTypes = {value: {type: 'string'}}
TextValue.storyName = 'Text'

export const TimeValue = EditableValueStory.bind({})
TimeValue.args = {value: '18:30', type: 'time'}
TimeValue.argTypes = {value: {type: 'string'}}
TimeValue.storyName = 'Time'

export const UrlValue = EditableValueStory.bind({})
UrlValue.args = {value: 'http://www.tooco.ch', type: 'url'}
UrlValue.argTypes = {value: {type: 'string'}}
UrlValue.storyName = 'Url'
