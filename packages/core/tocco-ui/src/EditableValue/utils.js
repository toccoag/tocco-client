/*
 * Returns format separators provided by intl API.
 */
export const parseLocalePlaceholder = countryCode => {
  const thousandSeparator = (1111).toLocaleString(countryCode).replace(/1/g, '')
  const decimalSeparator = (1.1).toLocaleString(countryCode).replace(/1/g, '')

  return {thousandSeparator, decimalSeparator}
}

/*
 * String to number converter
 */
export const convertStringToNumber = stringValue =>
  !stringValue || isNaN(stringValue) ? null : parseFloat(stringValue)

export const ensureNegative = number => Math.abs(number) * -1

/*
 * Convert two numbers as hours and minutes to milliseconds
 */
export const calculateMilliseconds = (hours, minutes) => {
  if (!hours && !minutes) {
    return null
  }

  if (hours < 0 || minutes < 0) {
    hours = ensureNegative(hours)
    minutes = ensureNegative(minutes)
  }

  const hoursMilliseconds = (hours || 0) * 60 * 60000
  const minutesMilliseconds = (minutes || 0) * 60000
  return hoursMilliseconds + minutesMilliseconds
}

/*
 * convert locationInput to google maps address link
 * only use street, postcode, city and country the other fields are ignored
 */
export const getGoogleMapsAddressLink = locationInput => {
  const mapsBaseAddress = 'https://www.google.com/maps/search/?api=1&query='

  const queryParams = [locationInput.street, locationInput.postcode, locationInput.city, locationInput.country]
    .filter(Boolean)
    .map(value => {
      const str = typeof value === 'object' && value.display ? value.display : value
      return encodeURIComponent(str)
    })
    .join('+')

  return `${mapsBaseAddress}${queryParams}`
}
