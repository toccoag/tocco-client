import {screen, waitFor} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import EditableValue from './'

describe('tocco-ui', () => {
  describe('EditableValue', () => {
    describe('<EditableValue>', () => {
      test('should export component that', () => {
        const testValue = 'test'

        testingLibrary.renderWithTheme(<EditableValue type="string" value={testValue} />)
        expect(screen.getByDisplayValue(testValue)).exist
      })

      test('should render popover content', async () => {
        testingLibrary.renderWithTheme(
          <EditableValue type="string" value="test" popoverContent={<p>popover text</p>} />
        )

        const user = userEvent.setup()
        await user.hover(screen.getByRole('textbox'))
        await waitFor(() => {
          expect(screen.queryByText('popover text')).to.exist
        })
      })
    })
  })
})
