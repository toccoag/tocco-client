/* eslint-disable max-len */
import {createGlobalStyle} from 'styled-components'

import {StyledFontAwesomeAdapterWrapper} from '../Icon'
import {themeSelector, declareFont, scale} from '../utilStyles'

const secondaryLight = themeSelector.color('secondaryLight')
const paper = themeSelector.color('paper')
const text = themeSelector.color('text')
const secondary = themeSelector.color('secondary')
const borderRadius = themeSelector.radii('form')

// put long css rule in variable to avoid eslint problem https://github.com/eslint/eslint/issues/11138
const disableTitleCss = `
  .react-datepicker__current-month.react-datepicker__current-month--hasYearDropdown.react-datepicker__current-month--hasMonthDropdown {
    // Avoid showing the month and year a second time when it has month and year dropdowns.
    display: none;
  }
`

export const GlobalDatePickerStyles = createGlobalStyle`
  .react-datepicker-popper {
    && {
      /* higher than StyledHolder and very high value to prevent other elements blocking it when implemented as a widget */
      z-index: 9100;
      margin: 0;
    }
  }

  .react-datepicker-wrapper {
    width: 100%;

    .react-datepicker__input-container {
      ${StyledFontAwesomeAdapterWrapper} {
        color: ${text};
      }
    }
  }

  .react-datepicker.react-datepicker .react-datepicker__time-container--with-today-button,
  .react-datepicker.react-datepicker .react-datepicker__time-container .react-datepicker__time {
    border-radius: ${themeSelector.radii('form')} !important;
    height: 100%;
  }

  .react-datepicker {
    && {
      * {
        box-sizing: content-box; /* prevent box-sizing problems in widgets */
      }
      border-radius: ${borderRadius};
      border: 1px solid ${secondaryLight};
      background-color: ${paper};
      ${declareFont()}
      font-size: ${({isWidget}) => (isWidget ? scale.font(-1.5) : scale.font(1))};

      .react-datepicker__header {
        ${declareFont()}
        border-radius: 0;
        background-color: ${secondaryLight};
        padding-bottom: 0;
        border-top-left-radius: ${borderRadius};
        border-top-right-radius: ${borderRadius};
      }

      .react-datepicker__navigation {
        top: 3px;
      }

      .react-datepicker__navigation--previous,
      .react-datepicker__navigation--next {
        margin-top: ${scale.space(-0.6)};
      }
      ${disableTitleCss}

      .react-datepicker__year-read-view--down-arrow,
      .react-datepicker__month-read-view--down-arrow,
      .react-datepicker__month-year-read-view--down-arrow,
      .react-datepicker__navigation-icon:before {
        border-color: ${paper} !important;
        border-width: 1.5px 1.5px 0 0;
      }

      .react-datepicker__month-read-view--selected-month,
      .react-datepicker__year-read-view--selected-year {
        font-size: ${scale.font(1)};
        font-weight: ${themeSelector.fontWeight('bold')};
      }

      .react-datepicker__current-month {
        ${declareFont({
          fontWeight: themeSelector.fontWeight('bold')
        })}
      }

      .react-datepicker__day-name {
        color: ${paper};
        font-size: ${({isWidget}) => (isWidget ? scale.font(-2.5) : scale.font(-2))};
        margin: ${({isWidget}) => (isWidget ? 0 : null)};
      }

      .react-datepicker__day,
      .react-datepicker__day-name {
        border: 1px solid transparent;
        padding: ${({isWidget}) => (isWidget ? scale.space(-2.5) : scale.space(-1.8))};
        outline: none;
        margin: ${({isWidget}) => (isWidget ? 0 : null)};
      }

      .react-datepicker__day.react-datepicker__day--selected {
        background-color: ${secondary};
        border-color: ${secondary};
        border-radius: 50%;
        color: #fff; /* static in all themes */

        &:focus {
          border-color: ${secondary};
          background-color: ${secondaryLight};
        }
      }

      .react-datepicker__day--keyboard-selected {
        background-color: ${secondaryLight};
        border-color: ${secondaryLight};
        border-radius: 50%;
        color: #fff !important; /* static in all themes */
      }

      .react-datepicker__day--today,
      .react-datepicker__month-text--today,
      .react-datepicker__quarter-text--today,
      .react-datepicker__year-text--today {
        border-radius: 50%;
        border-color: ${secondary};
      }

      .react-datepicker__today-button {
        ${declareFont()}
        background-color: ${paper};
        border-top: 0;
        padding: ${({isWidget}) => (isWidget ? scale.space(-1.5) : scale.space(-0.5))};
        padding-top: 0;

        &:hover > button {
          background-color: ${themeSelector.color('backgroundItemHover')};
        }

        &:last-child {
          border-radius: ${borderRadius};
        }
      }

      .react-datepicker__input-time-container {
        display: flex;
        justify-content: center;
        margin: 5px 0;
        align-items: baseline;
        gap: ${scale.space(-0.5)};
        background-color: ${paper};

        .react-datepicker-time__input {
          margin: 0;

          input {
            width: 80px;
            border: 1px solid ${themeSelector.color('border')};
            border-radius: ${themeSelector.radii('form')};
            text-align: center;
            margin-top: ${scale.space(-1)};
            margin-bottom: ${scale.space(-1)};
          }
        }
      }

      .react-datepicker-time__header {
        font-size: ${scale.font(1)};
        color: ${paper};
        font-weight: ${themeSelector.fontWeight('bold')};
        padding-bottom: ${scale.space(-1)};
      }

      .react-datepicker__header__dropdown.react-datepicker__header__dropdown--scroll {
        width: 250px;
        margin: 8px auto;
        display: grid;
        grid-template-columns: 140px 70px;
      }

      .react-datepicker__month-read-view,
      .react-datepicker__year-read-view {
        color: ${paper};
        visibility: visible !important;
      }

      .react-datepicker__year-option,
      .react-datepicker__month-option,
      .react-datepicker__month-year-option {
        font-size: ${scale.font(0)};
      }

      .react-datepicker__year-option:first-of-type,
      .react-datepicker__month-option:first-of-type,
      .react-datepicker__month-year-option:first-of-type,
      .react-datepicker__year-option:last-of-type,
      .react-datepicker__month-option:last-of-type,
      .react-datepicker__month-year-option:last-of-type {
        border-radius: 0;
      }

      .react-datepicker__year-read-view--down-arrow,
      .react-datepicker__month-read-view--down-arrow,
      .react-datepicker__month-year-read-view--down-arrow {
        top: 8px;
        right: 0;
        width: 5px;
        height: 5px;

        /* decrease padding to prevent overflow on small screens */
        @media only screen and (max-width: 480px) {
          right: -8px;
        }
      }

      .react-datepicker__year-dropdown-container--select,
      .react-datepicker__month-dropdown-container--select,
      .react-datepicker__month-year-dropdown-container--select,
      .react-datepicker__year-dropdown-container--scroll,
      .react-datepicker__month-dropdown-container--scroll,
      .react-datepicker__month-year-dropdown-container--scroll {
        margin: 0 2px;
      }

      .react-datepicker__year-read-view--down-arrow {
        right: -4px;

        /* decrease padding to prevent overflow on small screens */
        @media only screen and (max-width: 480px) {
          right: 3px;
        }
      }

      .react-datepicker__day-names {
        margin-bottom: 0;
      }

      .react-datepicker__day,
      .react-datepicker__month-text,
      .react-datepicker__quarter-text,
      .react-datepicker__year-text {
        color: ${text};

        &:hover {
          border-color: ${secondaryLight};
          background-color: ${secondaryLight};
          border-radius: 50%;
          color: #fff;
        }
      }

      .react-datepicker__time-container .react-datepicker__time .react-datepicker__time-box {
        height: 100%;
        border-radius: ${themeSelector.radii('form')};
        background-color: ${paper};

        ul.react-datepicker__time-list {
          height: 100% !important;

          & li.react-datepicker__time-list-item {
            height: auto;
          }

          & li.react-datepicker__time-list-item:hover {
            background-color: ${secondaryLight};
          }

          & li.react-datepicker__time-list-item--selected {
            background-color: ${secondary};
          }
        }
      }

      .react-datepicker__year-dropdown {
        left: 40%;
        top: 45px;
      }

      .react-datepicker__navigation--years-upcoming:before {
        border-width: 1.5px 1.5px 0 0;
        border-color: ${text};
        border-style: solid;
        content: '';
        display: block;
        position: absolute;
        top: 8px;
        left: 12px;
        height: 5px;
        width: 5px;
        transform: rotate(-45deg);
      }

      .react-datepicker__navigation--years-previous:before {
        border-width: 1.5px 1.5px 0 0;
        border-color: ${text};
        border-style: solid;
        content: '';
        display: block;
        position: absolute;
        top: 6px;
        left: 12px;
        height: 5px;
        width: 5px;
        transform: rotate(135deg);
      }

      .react-datepicker__year-dropdown,
      .react-datepicker__month-dropdown,
      .react-datepicker__month-year-dropdown {
        background-color: ${paper};
        border-radius: 0;
        border-color: ${secondaryLight};
        box-shadow: 0 0 5px rgb(0 0 0 / 30%);
      }

      .react-datepicker__year-option:hover,
      .react-datepicker__month-option:hover,
      .react-datepicker__month-year-option:hover {
        background-color: ${themeSelector.color('backgroundItemHover')};
      }

      .react-datepicker__month-option--selected {
        left: 10px;
      }

      .react-datepicker__day--outside-month {
        color: ${themeSelector.color('textDisabled')};
      }

      .react-datepicker__time-container--with-today-button {
        border-color: ${themeSelector.color('border')};
        border-radius: ${themeSelector.radii('form')};
        right: -88px;
        top: -0.5px;
      }

      .react-datepicker__header--time {
        display: none;
      }

      .react-datepicker__month {
        margin-left: 0;
        margin-top: 0;
        text-align: unset;
      }

      .react-datepicker__week-number {
        color: ${themeSelector.color('paper')};
        margin: 0;
        padding: ${({isWidget}) => (isWidget ? scale.space(-1.5) : scale.space(-0.8))};
        background-color: ${secondaryLight};
      }
      
      .react-datepicker__week-number--keyboard-selected {
        border-radius: 0;
        font-weight: ${themeSelector.fontWeight('bold')};
      }

      .react-datepicker__day-name:first-child {
        visibility: hidden;
        position: relative;
      }

      .react-datepicker__day-name:first-child:after {
        content: "W";
        visibility: visible;
        position: absolute;
        left: -8px;
        right: 0;
      }
    }
  }
`
