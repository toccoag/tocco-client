import PropTypes from 'prop-types'
import React from 'react'

import {StyledChildrenWrapper} from './StyledComponents'

const ChildrenWrapper = React.forwardRef(({children, onOpen}, ref) => (
  <StyledChildrenWrapper onClick={onOpen} ref={ref}>
    {children}
  </StyledChildrenWrapper>
))

ChildrenWrapper.propTypes = {
  children: PropTypes.any,
  onOpen: PropTypes.func
}

export default ChildrenWrapper
