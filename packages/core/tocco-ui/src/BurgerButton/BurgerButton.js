import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'

import {StyledBurgerButton} from './StyledComponents'

const BurgerButton = ({isOpen, intl, toggleMenu}) => {
  const msg = id => intl.formatMessage({id})

  return (
    <StyledBurgerButton
      id="burger-button"
      isOpen={isOpen}
      aria-label={msg('client.component.burgerButton.Label')}
      onClick={toggleMenu}
    >
      <span />
      <span />
      <span />
    </StyledBurgerButton>
  )
}

BurgerButton.propTypes = {
  intl: PropTypes.object.isRequired,
  isOpen: PropTypes.bool,
  toggleMenu: PropTypes.func.isRequired
}

export default injectIntl(BurgerButton)
