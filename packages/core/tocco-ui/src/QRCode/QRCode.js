import PropTypes from 'prop-types'
import {QRCodeSVG} from 'qrcode.react'

/**
 * Use to display a string as QR-Code Image.
 */
const QRCode = ({value}) => <QRCodeSVG value={value} />

QRCode.propTypes = {
  /**
   * Value that will be shown as qr-code. e.g. an Url.
   */
  value: PropTypes.string
}

export default QRCode
