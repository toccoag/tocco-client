import Box from './LayoutBox'
import Container from './LayoutContainer'
import StyledLayoutBox from './StyledLayoutBox'
import StyledLayoutContainer from './StyledLayoutContainer'

export {StyledLayoutBox, StyledLayoutContainer}

export default {
  Box,
  Container
}
