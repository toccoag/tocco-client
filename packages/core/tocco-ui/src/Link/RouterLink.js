import {Link} from 'react-router-dom'
import styled, {css} from 'styled-components'

import {declareFont, themeSelector} from '../utilStyles'

export const routerLinkStyle = css`
  ${declareFont()}
  text-decoration: none;
  color: ${themeSelector.color('secondary')};

  * {
    color: ${themeSelector.color('text')};
    text-decoration: none;
  }

  &:hover,
  &:hover *,
  &:focus,
  &:focus * {
    color: ${themeSelector.color('secondaryLight')};
    text-decoration: ${({neutral}) => (neutral ? 'none' : 'underline')};
  }

  &:active,
  &:active * {
    color: ${({neutral}) => (neutral ? themeSelector.color('text') : themeSelector.color('secondaryLight'))};
    text-decoration: ${({neutral}) => (neutral ? 'none' : 'underline')};
  }
`

/**
 * A styled version of Router v6 Link
 */
export default styled(Link)`
  ${routerLinkStyle}
`
