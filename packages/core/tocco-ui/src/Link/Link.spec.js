import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Link from './Link'

describe('tocco-ui', () => {
  describe('Link', () => {
    test('should have 3 defaultProps', () => {
      testingLibrary.renderWithIntl(<Link />)

      expect(screen.getByRole('link')).to.exist

      expect(screen.getByRole('link').getAttribute('href')).to.eq('#')
      expect(screen.getByRole('link').getAttribute('target')).to.eq('_self')
    })

    test('should pass 10 props to StyledLink', () => {
      const onClickSpy = sinon.spy()
      testingLibrary.renderWithIntl(
        <Link
          alt="alt text"
          breakWords={false}
          download="name.ext"
          href="/url"
          neutral={true}
          rel="rel"
          onClick={onClickSpy}
          tabIndex={1}
          target="_blank"
          title="title text"
        />
      )

      const link = screen.getByRole('link')
      fireEvent.click(link)
      expect(onClickSpy).to.have.been.called

      expect(link.getAttribute('alt')).to.eq('alt text')
      expect(link.getAttribute('download')).to.eq('name.ext')
      expect(link.getAttribute('href')).to.eq('/url')
      expect(link.getAttribute('rel')).to.eq('rel')
      expect(link.getAttribute('tabindex')).to.eq('1')
      expect(link.getAttribute('target')).to.eq('_blank')
      expect(link.getAttribute('title')).to.eq('title text')
    })

    test('should show label as title attribute', () => {
      testingLibrary.renderWithIntl(<Link breakWords={false} label="label text" />)
      expect(screen.getByRole('link').getAttribute('title')).to.eq('label text')
    })

    test('should not show label as title attribute', () => {
      testingLibrary.renderWithIntl(<Link breakWords={true} label="label text" />)
      expect(screen.getByRole('link').getAttribute('title')).to.be.null
    })

    test('should display icon', async () => {
      testingLibrary.renderWithIntl(<Link icon="cog" />)
      expect(await screen.findByTestId('icon-cog')).to.exist
    })

    test('should display label', () => {
      testingLibrary.renderWithIntl(<Link label="label text" />)
      expect(screen.getByRole('link').innerHTML).to.eq('label text')
    })

    test('should render children label', () => {
      testingLibrary.renderWithIntl(<Link>label text</Link>)
      expect(screen.getByRole('link').innerHTML).to.eq('label text')
    })
  })
})
