import {waitFor} from '@testing-library/dom' // Import the waitFor function
import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event' // Recommended for user interaction
import {expect} from 'chai'
import {testingLibrary} from 'tocco-test-util'

import Popover from './Popover'

describe('tocco-ui', () => {
  describe('Popover', () => {
    test('should show popover content on mouseover and hide on mouseout with delay', async () => {
      testingLibrary.renderWithTheme(
        <Popover content={<span data-testid="content">Popover</span>}>
          <span data-testid="child">Test</span>
        </Popover>
      )

      expect(screen.queryAllByTestId('child')).to.have.lengthOf(1)
      expect(screen.queryAllByTestId('content')).to.have.lengthOf(0)
      const user = userEvent.setup()

      // Simulate hover using userEvent
      await user.hover(screen.getByText('Test'))

      await waitFor(() => {
        expect(screen.queryAllByTestId('content')).to.have.lengthOf(1)
      })

      // Simulate mouseout
      await user.unhover(screen.getByText('Test'))

      // Use waitFor to ensure it's fully disappeared
      await waitFor(() => {
        expect(screen.queryAllByTestId('content')).to.have.lengthOf(0)
      })
    })
  })
})
