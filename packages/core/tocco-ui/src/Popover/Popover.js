import {
  useFloating,
  useHover,
  useInteractions,
  flip,
  FloatingPortal,
  arrow,
  shift,
  offset,
  FloatingArrow
} from '@floating-ui/react'
import PropTypes from 'prop-types'
import {useState, useRef} from 'react'

import {GlobalAppClassName} from '../GlobalStyles'

import {StyledBox, StyledFloatingUi} from './StyledPopover'

const Popover = ({children, content, isPlainHtml = true, rimless = false}) => {
  const [isOpen, setIsOpen] = useState(false)
  const arrowRef = useRef(null)

  const {refs, floatingStyles, context} = useFloating({
    open: isOpen,
    onOpenChange: setIsOpen,
    middleware: [flip(), shift({padding: 20}), arrow({element: arrowRef}), offset(5)]
  })

  const hover = useHover(context, {
    delay: {
      open: 500,
      close: 0
    }
  })

  const {getReferenceProps, getFloatingProps} = useInteractions([hover])

  return (
    <>
      <span ref={refs.setReference} {...getReferenceProps()}>
        {children}
      </span>

      {isOpen && content && (
        <FloatingPortal>
          <StyledFloatingUi
            ref={refs.setFloating}
            style={floatingStyles}
            rimless={rimless}
            {...getFloatingProps()}
            className={GlobalAppClassName}
          >
            <FloatingArrow ref={arrowRef} context={context} />
            <StyledBox isPlainHtml={isPlainHtml}>{content}</StyledBox>
          </StyledFloatingUi>
        </FloatingPortal>
      )}
    </>
  )
}

Popover.propTypes = {
  /**
   * Content in the popover.
   */
  content: PropTypes.node,
  /**
   * Reference to the popover.
   */
  children: PropTypes.node,
  /**
   * Remove space between content and border if useful (e.g. content is an image only). Default is {false}.
   */
  rimless: PropTypes.bool,
  /**
   * Add typographic styles for nested content. If content is already completely
   * styled disable this option (e.g. styled-components). Default is {true}.
   */
  isPlainHtml: PropTypes.bool
}

export default Popover
