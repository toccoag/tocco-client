import styled from 'styled-components'

import {StyledHtmlFormatter} from '../FormattedValue/formatters/StyledComponents'
import {declareTypograhpy} from '../Typography'
import {scale, themeSelector} from '../utilStyles'

export const StyledFloatingUi = styled.div`
  && {
    pointer-events: none; /* prevent flickering of tooltip */
    background-color: ${themeSelector.color('backgroundPopover')};
    max-width: 400px;
    z-index: 100000010;
    padding: ${({rimless}) => (rimless ? '0' : scale.space(0))};

    * {
      color: ${themeSelector.color('textPopover')};
    }

    svg path {
      /* style FloatingArrow SVG */
      fill: ${themeSelector.color('backgroundPopover')};
    }
  }
`

export const StyledBox = styled.div`
  && {
    ${props => props.isPlainHtml && declareTypograhpy(props, 'html')};

    &,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p,
    ${StyledHtmlFormatter} {
      color: ${themeSelector.color('textPopover')};
      overflow-wrap: break-word;
    }
  }
`
