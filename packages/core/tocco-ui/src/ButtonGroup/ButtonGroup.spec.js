import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Button from '../Button'

import ButtonGroup from './ButtonGroup'

describe('tocco-ui', () => {
  describe('ButtonGroup', () => {
    test('should wrap children in a styled container', () => {
      testingLibrary.renderWithTheme(
        <ButtonGroup>
          <Button label="btn 1" />
          <Button label="btn 2" />
        </ButtonGroup>
      )

      expect(screen.queryAllByRole('button')).to.have.length(2)
      expect(screen.getByText('btn 1')).exist
      expect(screen.getByText('btn 2')).exist
    })
  })
})
