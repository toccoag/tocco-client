import styled, {css} from 'styled-components'

import {StyledButton} from '../Button'

/* without this styling the focus line gets clipped on connecting edges */
const outlineFix = css`
  position: relative;

  &:focus {
    z-index: 1;
  }
`

const StyledButtonGroup = styled.div`
  display: flex;

  ${StyledButton} {
    ${outlineFix}
  }

  &&& > ${StyledButton} {
    &:not(:first-child) {
      margin-left: -1px;
      border-top-left-radius: 0;
      border-bottom-left-radius: 0;
    }

    &:not(:last-child) {
      margin-right: 0;
      border-right: 0;
      border-top-right-radius: 0;
      border-bottom-right-radius: 0;
    }
  }
`

export default StyledButtonGroup
