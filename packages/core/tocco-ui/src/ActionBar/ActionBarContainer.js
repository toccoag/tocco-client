import styled from 'styled-components'

const ActionBarContainer = styled.div`
  display: grid;
  grid-template-rows: [action-start] auto [table-start] minmax(0, 1fr);
  height: 100%;
  width: 100%;
`

export default ActionBarContainer
