import styled from 'styled-components'
import {theme} from 'tocco-ui'

const ActionBar = styled.div`
  display: flex;
  background-color: ${theme.color('paper')};
  margin-bottom: 3px;
  padding: 8px;
  grid-row-start: action-start;
  flex-wrap: wrap;
`

export default ActionBar
