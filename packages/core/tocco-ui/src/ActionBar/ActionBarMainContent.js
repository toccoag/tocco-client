import styled from 'styled-components'
import {StretchingTableContainer} from 'tocco-ui'

const ActionBarMainContent = styled.div`
  grid-row-start: table-start;

  ${StretchingTableContainer} {
    overflow-x: hidden;
  }
`

export default ActionBarMainContent
