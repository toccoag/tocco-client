import PropTypes from 'prop-types'

import {ScrollBehaviour} from './scrollBehaviour'

export const scrollBehaviourPropType = PropTypes.oneOf(Object.keys(ScrollBehaviour).map(key => ScrollBehaviour[key]))
