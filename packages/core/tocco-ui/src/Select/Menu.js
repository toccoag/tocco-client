import _omit from 'lodash/omit'
import PropTypes from 'prop-types'

import {GlobalAppClassName} from '../GlobalStyles'

import {StyledTether, StyledMenu} from './StyledComponents'

const Menu = props => {
  const {selectProps, children, theme} = props

  return (
    <StyledTether
      attachment="top left"
      targetAttachment="bottom left"
      constraints={[
        {
          to: 'window',
          attachment: 'together'
        }
      ]}
      renderTarget={ref => <div ref={ref} />}
      renderElement={ref => (
        <div ref={ref} className={GlobalAppClassName}>
          <StyledMenu
            {..._omit(props, ['innerRef', 'theme'])}
            wrapperWidth={selectProps.wrapperWidth}
            wrapperHeight={selectProps.wrapperHeight}
            selectTheme={theme}
          >
            {children}
          </StyledMenu>
        </div>
      )}
    />
  )
}

Menu.propTypes = {
  children: PropTypes.node,
  selectProps: PropTypes.shape({
    wrapperHeight: PropTypes.number,
    wrapperWidth: PropTypes.number
  }),
  theme: PropTypes.object
}

export default Menu
