import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Select from './Select'

describe('tocco-ui', () => {
  describe('Select', () => {
    describe('<Select>', () => {
      test('should render a combobox', async () => {
        testingLibrary.renderWithIntl(<Select />)
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('combobox')).to.have.length(1)
      })

      test('should render advanced search button', async () => {
        testingLibrary.renderWithIntl(<Select openAdvancedSearch={() => {}} immutable={false} />)
        expect(screen.queryAllByRole('button')).to.have.length(1)
        expect(await screen.findByTestId('icon-search')).to.exist
      })

      test('should not render advanced search button when disabled', () => {
        testingLibrary.renderWithIntl(<Select openAdvancedSearch={() => {}} immutable={true} />)
        expect(screen.queryAllByRole('button')).to.have.length(0)
      })

      test('should not render advanced search button without function', async () => {
        testingLibrary.renderWithIntl(<Select immutable={false} />)
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('button')).to.have.length(0)
      })

      test('should render remote create button', async () => {
        testingLibrary.renderWithIntl(<Select createPermission={true} />)
        expect(screen.queryAllByRole('button')).to.have.length(1)
        expect(await screen.findByTestId('icon-plus')).to.exist
      })

      test('should not render remote create button when not allowed', async () => {
        testingLibrary.renderWithIntl(<Select createPermission={false} />)
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('button')).to.have.length(0)
      })

      test('should not render remote create button when disabled', () => {
        testingLibrary.renderWithIntl(<Select immutable={true} />)
        expect(screen.queryAllByRole('button')).to.have.length(0)
      })
    })
  })
})
