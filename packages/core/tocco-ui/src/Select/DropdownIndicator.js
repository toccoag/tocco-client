import PropTypes from 'prop-types'
import {components} from 'react-select'

import Ball from '../Ball/Ball'

import {StyledDropdownIndicatorWrapper} from './StyledComponents'

const DropdownIndicator = props => {
  const {
    selectProps: {isDisabled, menuIsOpen}
  } = props
  if (isDisabled) {
    return null
  }

  return (
    <components.DropdownIndicator {...props}>
      <StyledDropdownIndicatorWrapper>
        <Ball icon={menuIsOpen ? 'chevron-up' : 'chevron-down'} visible tabIndex={-1} />
      </StyledDropdownIndicatorWrapper>
    </components.DropdownIndicator>
  )
}

DropdownIndicator.propTypes = {
  immutable: PropTypes.bool,
  selectProps: PropTypes.shape({
    menuIsOpen: PropTypes.bool,
    isDisabled: PropTypes.bool
  })
}

export default DropdownIndicator
