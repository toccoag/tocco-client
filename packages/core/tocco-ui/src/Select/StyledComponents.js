import {components} from 'react-select'
import TetherComponent from 'react-tether'
import styled, {css} from 'styled-components'

import {declareFont, scale, themeSelector} from '../utilStyles'

export const StyledClickableWrapper = styled.span`
  pointer-events: auto;
  cursor: pointer;
`

export const StyledReactSelectOuterWrapper = styled.div`
  outline-style: none;
  cursor: ${({immutable}) => immutable && 'text'};
`

export const StyledReactSelectInnerWrapper = styled.div`
  outline-style: none;
`

export const StyledIndicatorsContainerWrapper = styled.div`
  position: relative;
  z-index: 1;
  background-color: ${themeSelector.color('paper')};
  border-radius: ${themeSelector.radii('form')};
  ${({isTopAligned}) =>
    isTopAligned &&
    css`
      position: absolute;
      right: 0;
      top: 0;
    `};
`

export const StyledTether = styled(TetherComponent)`
  && {
    z-index: 1;
  }
`

export const StyledNonTether = styled.div`
  position: absolute;
  padding-bottom: ${scale.space(1.5)};
  z-index: 2; /* higher than StyledTether and StyledIndicatorsContainerWrapper */
`

export const StyledMenu = styled(components.Menu).attrs(props => ({
  theme: props.selectTheme
}))`
  && {
    margin: 8px 0 8px -10px;
    width: calc(${({wrapperWidth}) => wrapperWidth}px + 20px);
    position: relative;

    .tether-target-attached-top & {
      transform: translateY(-${({wrapperHeight}) => wrapperHeight + 6}px);
    }
  }
`

export const StyledMoreOptionsAvailable = styled.div`
  && {
    ${declareFont({
      color: themeSelector.color('signal.info'),
      lineHeight: 'normal'
    })}
    cursor: default;
    padding: ${scale.space(-0.5)} ${scale.space(-0.2)};
  }
`

export const StyledSingleValueWrapper = styled.div`
  overflow-x: hidden;
  position: relative;
  color: ${({isDisabled}) => (isDisabled ? themeSelector.color('text') : 'inherit')};
`

/**
 * Toggling the icon is very slow on Safari.
 * To change the icon with `transform` css property is very fast.
 */
export const StyledDropdownIndicatorWrapper = styled.div`
  && {
    position: relative;
    width: 25px;
    height: 25px;
    display: flex;
    align-items: center;
  }
`

export const StyledMultiValueWrapper = styled.div`
  overflow: hidden;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`

export const StyledMultiValueLabelWrapper = styled.span`
  overflow: hidden;
  text-overflow: ellipsis;

  span {
    padding: ${scale.space(-1.1)} !important; /* overwrite line stlyes */
  }
`

export const reactSelectStyles = outerTheme => {
  const paperColor = outerTheme.colors.paper
  const hoverColor = outerTheme.colors.backgroundItemHover
  const selectedColor = outerTheme.colors.backgroundItemSelected
  const textColor = outerTheme.colors.text
  const borderColor = outerTheme.colors.borderSelect
  const textDisabled = outerTheme.colors.textDisabled
  const borderRadius = outerTheme.radii.form

  const typography = {
    color: textColor,
    fontFamily: outerTheme.fontFamily.regular,
    fontSize: `${outerTheme.fontSize.base}rem`,
    fontWeight: outerTheme.fontWeights.regular,
    lineHeight: outerTheme.lineHeights.regular
  }
  const showInputAlwaysOnTop = state =>
    state.isMulti && (state.selectProps.hasAdvancedSearch || state.selectProps.hasDocsTreeSearch)

  const messageStyle = base => ({
    ...base,
    color: outerTheme.colors.signal.info,
    cursor: 'default',
    textAlign: 'left'
  })

  return {
    container: (base, state) => ({
      ...base,
      ...typography,
      outline: 0
    }),
    control: (base, state) => ({
      ...base,
      backgroundColor: 'transparent',
      borderWidth: 0,
      boxShadow: null,
      minHeight: 0,
      flexWrap: 'nowrap',
      position: 'relative',
      display: showInputAlwaysOnTop(state) ? 'block' : base.display
    }),
    noOptionsMessage: messageStyle,
    loadingMessage: messageStyle,
    option: (base, state) => {
      let backgroundColor = paperColor

      if (state.isSelected) {
        backgroundColor = selectedColor
      } else if (state.isFocused) {
        backgroundColor = hoverColor
      }

      return {
        ...base,
        color: outerTheme.colors.text,
        backgroundColor,
        cursor: 'pointer',
        ':active': {
          backgroundColor: state.isSelected ? selectedColor : hoverColor
        }
      }
    },
    indicatorsContainer: (base, state) => ({
      ...base,
      alignSelf: 'flex-end',
      '> *': {
        padding: '0 !important;' // resets react-select padding
      }
    }),
    input: (base, state) => {
      let indicatorWidth = 50
      if (state.selectProps.hasDocsTreeSearch) {
        indicatorWidth += 25
      }
      if (state.selectProps.hasAdvancedSearch) {
        indicatorWidth += 25
      }
      if (state.selectProps.hasCreatePermission) {
        indicatorWidth += 25
      }

      return {
        ...base,
        minHeight: state.isDisabled ? '0' : '2.6rem',
        paddingTop: state.isDisabled ? '0' : '2px',
        paddingBottom: state.isDisabled ? '0' : '2px',
        ...(showInputAlwaysOnTop(state) ? {width: `calc(100% - ${indicatorWidth}px)`} : {}),
        overflow: 'hidden',
        margin: '0',
        color: textColor
      }
    },
    menu: (base, state) => ({
      backgroundColor: outerTheme.colors.paper,
      boxShadow: `0 0 6px ${borderColor}`,
      borderRadius
    }),
    menuList: (base, state) => ({
      ...base,
      ...typography,
      border: `1px solid ${borderColor}`,
      borderRadius,
      padding: '0',
      backgroundColor: outerTheme.colors.paper
    }),
    multiValue: (base, state) => ({
      ...base,
      borderRadius,
      backgroundColor: outerTheme.colors.backgroundMultiValue,
      justifyContent: 'space-between',
      ...(showInputAlwaysOnTop(state) && !state.isDisabled ? {width: '100%', maxWidth: '300px'} : {})
    }),
    multiValueLabel: (base, state) => ({
      ...base,
      borderRadius,
      color: textColor,
      fontSize: 'inherit',
      lineHeight: 1.8
    }),
    multiValueRemove: (base, state) => ({
      ...base,
      borderRadius: 0,
      color: textColor,
      display: state.isDisabled ? 'none' : 'flex',
      ':hover': {
        ...base[':hover'],
        color: textColor,
        backgroundColor: selectedColor,
        cursor: 'pointer'
      }
    }),
    singleValue: (base, state) => ({
      ...base,
      color: state.isDisabled ? textDisabled : textColor,
      margin: 0,
      whiteSpace: 'wrap'
    }),
    valueContainer: (base, state) => ({
      ...base,
      padding: 0,
      margin: 0,
      minHeight: state.isDisabled ? '0' : '2.6rem',
      gap: scale.space(-2.5)({theme: outerTheme})
    })
  }
}
