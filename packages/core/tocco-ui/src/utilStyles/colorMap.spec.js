import {colorizeBorder, colorizeText} from './colorMap'

const props = {
  theme: {
    colors: {
      text: '#000',
      textLight: '#595959',
      textLighter: '#8c8c8c',
      border: '#CCC',
      signal: {
        success: '#337f37'
      }
    }
  },
  signal: 'success'
}

describe('tocco-ui', () => {
  describe('utilStyles', () => {
    describe('colorizeText', () => {
      test('should return signal color', () => {
        const value = colorizeText.signal(props)
        expect(value).to.equal('#337f37')
      })
      test('should return base of text color', () => {
        const value = colorizeText.base(props)
        expect(value).to.equal('#000')
      })
      test('should return shade 1 of text color', () => {
        const value = colorizeText.shade1(props)
        expect(value).to.equal('#595959')
      })
      test('should return shade 2 of text color', () => {
        const value = colorizeText.shade2(props)
        expect(value).to.equal('#8c8c8c')
      })
    })
    describe('colorizeBorder', () => {
      test('should return base of paper color', () => {
        const value = colorizeBorder.base(props)
        expect(value).to.equal('#CCC')
      })
      test('should return signal color', () => {
        const value = colorizeBorder.signal(props)
        expect(value).to.equal('#337f37')
      })
      test('should return text color', () => {
        const value = colorizeBorder.transparent(props)
        expect(value).to.equal('transparent')
      })
    })
  })
})
