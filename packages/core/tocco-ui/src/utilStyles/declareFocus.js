import _get from 'lodash/get'
import {transparentize} from 'polished'
import {css} from 'styled-components'

import {design} from '.'

export const declareFocus = props => {
  const infoText = _get(props.theme, 'colors.signal.info', design.fallbackColors.INFO)
  return css`
    transition: border-color ease-in-out 100ms;
    will-change: border-color;

    &:focus,
    &:focus-within {
      border-color: ${infoText};
      box-shadow: 0 0 6px ${transparentize(0.4, infoText)};
      outline: 0;

      /**
       * Disable box-shadow for Safari
       * in order to be able to tab through the form fluently
       */
      @media not all and (min-resolution: 0.001dpcm) {
        @supports (-webkit-appearance: none) {
          box-shadow: none;
        }
      }
    }
  `
}
