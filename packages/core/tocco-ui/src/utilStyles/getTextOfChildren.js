import React from 'react'

const getTextOfChildren = children => {
  let output = ''
  React.Children.map(children, (child, i) => {
    if (typeof child === 'string' || typeof child === 'number') {
      output = `${output}${child}`
    } else if (typeof child === 'object' && child.props.children) {
      output = `${output}${getTextOfChildren(child.props.children)}`
    } else if (typeof child === 'object' && child.props.dangerouslySetInnerHTML) {
      output = `${output} ${getTextFromHtml(child.props.dangerouslySetInnerHTML.__html)}`
    }
  })
  return output
}

const getTextFromHtml = html => {
  const divContainer = document.createElement('div')
  divContainer.innerHTML = html
  return divContainer.textContent || divContainer.innerText || ''
}

export default getTextOfChildren
