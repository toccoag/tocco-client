import {css} from 'styled-components'

import {scale, themeSelector} from '../utilStyles'

const getColorFromLabelType = type => {
  switch (type) {
    case 'danger':
      return {
        backgroundColor: 'signal.dangerLight',
        color: 'textLabel'
      }
    case 'info':
      return {
        backgroundColor: 'signal.infoLight',
        color: 'textLabel'
      }
    case 'warning':
      return {
        backgroundColor: 'signal.warningLight',
        color: 'textLabel'
      }
    case 'status':
      return {
        backgroundColor: 'signal.statusLight',
        color: 'textLabel'
      }
    case 'success':
      return {
        backgroundColor: 'signal.successLight',
        color: 'textLabel'
      }
    default:
      return {}
  }
}

const declareColoredLabel = type => {
  const {color, backgroundColor} = getColorFromLabelType(type)
  const hasColor = Boolean(color) && Boolean(backgroundColor)

  return css`
    padding: ${scale.space(-3)} ${scale.space(-1.5)};
    display: inline-block;
    border-radius: ${themeSelector.radii('statusLabel')};
    ${({theme}) =>
      hasColor &&
      `
      background-color: ${themeSelector.color(backgroundColor)({theme})};
      color: ${themeSelector.color(color)({theme})};
      `}
  `
}

export default declareColoredLabel
