import {lighten, readableColor, tint} from 'polished'
import {css} from 'styled-components'

import {theme as themeSelector} from './index'

const getBackgroundColor = (ink, look, theme) => {
  const isPrimaryBackgroundColor = ink === 'primary' && look === 'raised'
  const isSecondaryBackgroundColor = ink === 'secondary' && look === 'raised'

  if (isPrimaryBackgroundColor) {
    return themeSelector.color('primary')({theme})
  } else if (isSecondaryBackgroundColor) {
    return themeSelector.color('secondary')({theme})
  }

  return themeSelector.color('paper')({theme})
}

const getFontColor = (ink, look, theme, backgroundColor) => {
  const isPrimaryFontColor = ink === 'primary' && look !== 'raised'

  if (isPrimaryFontColor) {
    return themeSelector.color('primary')({theme})
  }

  return getMatchingFontColor(backgroundColor, theme)
}

const getHoverFontColor = (look, theme, backgroundColor) => {
  if (look === 'raised') {
    return themeSelector.color('paper')({theme})
  }

  return getMatchingFontColor(backgroundColor, theme)
}

const getBorder = (ink, look, theme) => {
  const isSecondaryBorderColor = ink !== 'primary' && look === 'raised'

  if (isSecondaryBorderColor) {
    return `0px 0px 0px 1px ${themeSelector.color('secondaryLight')({theme})} inset;`
  }

  return 'none'
}

const getHoverBackgroundColor = (ink, look, theme) => {
  if (look === 'raised') {
    if (ink === 'primary') {
      return themeSelector.color('primaryLight')({theme})
    }
    return themeSelector.color('secondaryLight')({theme})
  }

  return themeSelector.color('backgroundItemHover')({theme})
}

const getMatchingFontColor = (color, theme) =>
  readableColor(color, themeSelector.color('text')({theme}), themeSelector.color('paper')({theme}))

export default props => {
  const {ink, look, theme} = props
  const backgroundColor = getBackgroundColor(ink, look, theme)
  const fontColor = getFontColor(ink, look, theme, backgroundColor)
  const border = getBorder(ink, look, theme)
  const hoverBackgroundColor = getHoverBackgroundColor(ink, look, theme)
  const hoverFontColor = getHoverFontColor(look, theme, backgroundColor)

  return css`
    && {
      color: ${fontColor};
    }
    background: ${backgroundColor};
    box-shadow: ${border};
    outline: none;
    border: none;

    &:active,
    &[aria-pressed='true'] {
      background: ${lighten(0.1, hoverBackgroundColor)};
    }

    &:disabled,
    &[disabled] {
      background: ${theme.name !== 'dark' ? tint(0.41, backgroundColor) : 'transparent'};
      opacity: 0.5;
      cursor: text;
    }

    &:hover:not(:disabled),
    &:hover:not([disabled]) {
      background: ${hoverBackgroundColor};
      color: ${hoverFontColor};
    }
  `
}
