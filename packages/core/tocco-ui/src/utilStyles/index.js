import {colorizeBorder, colorizeText} from './colorMap'
import declareColoredLabel from './declareColoredLabel'
import {declareFocus} from './declareFocus'
import declareFont from './declareFont'
import {declareNoneWrappingText, declareWrappingText} from './declareWrapping'
import design from './designConstants'
import filterObjectByKeysStartingWith from './filterObjectByKeysStartingWith'
import getTextOfChildren from './getTextOfChildren'
import interactiveStyling from './interactiveStyling'
import scale from './modularScale'
import {validateCssDimension} from './propTypesValidator'
import theme from './resolveThemePath'
import {useCollapseOnMobile} from './useCollapseOnMobile'
import {useOnEnterHandler} from './useOnEnterHandler'
import {useWindowSize} from './useWindowSize'

const themeSelector = theme
export {
  interactiveStyling,
  colorizeBorder,
  colorizeText,
  declareFocus,
  declareFont,
  declareColoredLabel,
  declareNoneWrappingText,
  declareWrappingText,
  design,
  filterObjectByKeysStartingWith,
  getTextOfChildren,
  scale,
  theme,
  themeSelector,
  validateCssDimension,
  useWindowSize,
  useCollapseOnMobile,
  useOnEnterHandler
}
