/* eslint-disable react/prop-types */
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {getTextOfChildren} from '../utilStyles'

describe('tocco-ui', () => {
  describe('utilStyles', () => {
    describe('getTextOfChildren', () => {
      const Comp = ({children}) => <div data-testid="test" data-value={getTextOfChildren(children)}></div>

      test('should get text from all nodes and respect spaces', () => {
        // eslint-disable-next-line max-len
        testingLibrary.renderWithTheme(<Comp><i>A<i><i>B</i>C<i>D</i></i>E <i><i>F  </i>G   <i>H    </i></i>I</i></Comp>) // prettier-ignore

        expect(screen.getByTestId('test').dataset.value).to.eql('ABCDE F  G   H    I')
      })

      test('should get text from string and number', () => {
        const number = 1
        testingLibrary.renderWithTheme(
          <Comp>
            <i>{number}.</i>
            <i>A</i>
          </Comp>
        )

        expect(screen.getByTestId('test').dataset.value).to.eql('1.A')
      })

      test('should get text from dangerouslySetInnerHTML', () => {
        testingLibrary.renderWithTheme(
          <Comp>
            <span dangerouslySetInnerHTML={{__html: '<p>1.<i>A</i></p>'}} />
          </Comp>
        )

        expect(screen.getByTestId('test').dataset.value).to.eql(' 1.A')
      })
    })
  })
})
