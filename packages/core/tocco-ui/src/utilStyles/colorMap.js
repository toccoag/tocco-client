import _get from 'lodash/get'

const colorizeText = {
  base: ({theme}) => _get(theme, 'colors.text'),
  shade1: ({theme}) => _get(theme, 'colors.textLight'),
  shade2: ({theme}) => _get(theme, 'colors.textLighter'),
  signal: ({theme, signal}) => _get(theme, `colors.signal.${signal}`),
  hasValue: ({theme}) => _get(theme, 'colors.signal.info')
}

const colorizeBorder = {
  base: ({theme}) => _get(theme, 'colors.border'),
  signal: ({theme, signal}) => _get(theme, `colors.signal.${signal}`),
  isDirty: ({theme}) => _get(theme, 'colors.secondaryLight'),
  transparent: () => 'transparent'
}

export {colorizeBorder, colorizeText}
