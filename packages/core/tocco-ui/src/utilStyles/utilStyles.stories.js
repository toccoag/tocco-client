import styled from 'styled-components'

import declareColoredLabel from './declareColoredLabel'

export default {
  title: 'Tocco-Ui/utilStyles',
  argTypes: {
    type: {
      options: ['danger', 'info', 'warning', 'status', 'success', ''],
      control: {type: 'select'}
    }
  }
}

const Label = styled.div`
  ${({type}) => declareColoredLabel(type)}
`

export const DeclareColoredLabel = args => <Label type={args.type}>{args.type}</Label>

DeclareColoredLabel.args = {
  type: 'danger'
}
