import {useEffect} from 'react'

export const useOnEnterHandler = onEnterCallback => {
  useEffect(() => {
    const handleKeyInput = e => {
      if (e.key === 'Enter' && e.target.tagName !== 'TEXTAREA') {
        e.preventDefault()
        e.stopPropagation()
        onEnterCallback()
      }
    }

    document.addEventListener('keydown', handleKeyInput)

    return () => {
      document.removeEventListener('keydown', handleKeyInput)
    }
  }, [onEnterCallback])
}
