import styled, {css} from 'styled-components'
import {device} from 'tocco-util'

import {Button, declareFont, scale} from '../index'
import {interactiveStyling, themeSelector} from '../utilStyles'

export const StyledIconWrapper = styled.div`
  margin-left: ${scale.space(-1.7)};
  margin-right: -${scale.space(-2.1)};
`

export const StyledIconButtonWrapper = styled(Button)`
  padding-left: ${scale.space(-0.5)};
  padding-right: ${scale.space(-0.5)};
`

export const StyledFloatingUiWrapper = styled.div`
  position: absolute;
`

export const StyledFloatingUi = styled.div`
  min-height: calc(2 * ${scale.space(-1.4)} + ${scale.font(0)}); /* min-height of 1 entry */
  width: max-content;
  overflow: hidden auto;
  background: ${themeSelector.color('paper')};
  box-shadow: 0 0 5px rgba(0 0 0 / 30%);
  border: 1px solid ${themeSelector.color('secondaryLight')};
  border-radius: ${themeSelector.radii('menu')};
  z-index: 9999999; /* higher than StyledModalHolder */
  box-sizing: border-box;
`

export const StyledMenuItem = styled.div`
  min-width: 200px;
  max-width: 300px;
`

export const StyledItemLabel = styled.div`
  cursor: ${({hasOnClick}) => (hasOnClick ? 'pointer' : 'default')};
  ${declareFont()}
  ${props => (props.hasOnClick ? interactiveStyling(props) : '')}
  /* increased padding on touchdevices */
  padding: ${device.isPrimaryTouchDevice()
    ? css`
        ${scale.space(-1.4)}
        ${scale.space(-0.5)}
      ${scale.space(-1.4)}
      `
    : css`
        ${scale.space(-2)}
        ${scale.space(-0.5)}
      ${scale.space(-2)}
      `}
    calc(${scale.space(-0.5)} + ${({level}) => (level || 0) * 10 + 'px'});
  font-weight: ${({isGroup}) => (isGroup ? themeSelector.fontWeight('bold') : themeSelector.fontWeight('regular'))};
  box-shadow: none;

  &:disabled,
  &[disabled] {
    color: ${themeSelector.color('textDisabled')};
    cursor: text;
  }
`
