import {FloatingPortal} from '@floating-ui/react'
import PropTypes from 'prop-types'
import React from 'react'

import {GlobalAppClassName} from '../GlobalStyles'

import {StyledFloatingUi, StyledFloatingUiWrapper} from './StyledComponents'

/**
 * Menu realised with floating-ui.
 */
const Menu = ({floatingStyles, getFloatingProps, refs, isOpen, onClose, children}) => {
  if (!isOpen) {
    return null
  }

  return (
    <StyledFloatingUiWrapper>
      {children && isOpen && (
        <FloatingPortal>
          <StyledFloatingUi
            ref={refs.setFloating}
            style={floatingStyles}
            {...getFloatingProps()}
            className={GlobalAppClassName}
          >
            {React.Children.map(children, child => child && React.cloneElement(child, {onClose}))}
          </StyledFloatingUi>
        </FloatingPortal>
      )}
    </StyledFloatingUiWrapper>
  )
}

Menu.propTypes = {
  /**
   * Element to attach floatin-ui menu to
   */
  referenceElement: PropTypes.any,
  /**
   * Will be called if menu is closed
   */
  onClose: PropTypes.func.isRequired,
  /**
   * Tree of <MenuItems>
   */
  children: PropTypes.node,
  /**
   * Whether menu is open or not
   */
  isOpen: PropTypes.bool.isRequired,
  /**
   * Styles for the floating-ui element
   */
  floatingStyles: PropTypes.object.isRequired,
  /**
   * Prop getter function for the floating-ui element
   */
  getFloatingProps: PropTypes.func.isRequired,
  /**
   * Object containing reference elements for the floating-ui element
   */
  refs: PropTypes.object.isRequired
}

export default Menu
