import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ButtonMenu from './ButtonMenu'
import MenuItem from './MenuItem'

describe('tocco-ui', () => {
  describe('Menu', () => {
    describe('ButtonMenu', () => {
      test('should render a Button and not render Menu when its closed', async () => {
        testingLibrary.renderWithIntl(
          <ButtonMenu buttonProps={{icon: 'facebook'}}>
            <MenuItem>Test</MenuItem>
          </ButtonMenu>
        )
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('button')).to.have.length(1)
        expect(screen.queryAllByText('Test')).to.have.length(0)
      })

      test('should render a Buttongroup with default click handler', async () => {
        const onClick = sinon.spy()
        testingLibrary.renderWithIntl(
          <ButtonMenu onClick={onClick} label="testbutton" buttonProps={{icon: 'facebook'}}>
            <MenuItem>Test</MenuItem>
          </ButtonMenu>
        )
        await screen.findAllByTestId('icon')

        const menuButton = screen.getByTitle('testbutton')
        fireEvent.click(menuButton)
        expect(screen.queryAllByText('Test')).to.have.length(0)
        expect(onClick).to.be.calledOnce
      })

      test('should set menu open on click and call onOpen only once', async () => {
        const onOpen = sinon.spy()
        testingLibrary.renderWithIntl(
          <ButtonMenu buttonProps={{icon: 'facebook'}} onOpen={onOpen}>
            <MenuItem>Test</MenuItem>
          </ButtonMenu>
        )
        await screen.findAllByTestId('icon')

        // initial state -> menu closed
        const menuButton = screen.getByRole('button')
        expect(await screen.findByTestId('icon-chevron-down')).to.exist
        expect(screen.queryAllByText('Test')).to.have.length(0)

        // 1st click -> menu opened
        fireEvent.click(menuButton)
        expect(await screen.findByTestId('icon-chevron-up')).to.exist
        expect(screen.queryAllByText('Test')).to.have.length(1)
        expect(onOpen).to.be.calledOnce

        // 2nd click -> menu closed again
        fireEvent.click(menuButton)
        expect(await screen.findByTestId('icon-chevron-down')).to.exist
        expect(screen.queryAllByText('Test')).to.have.length(0)
        expect(onOpen).to.be.calledOnce
      })
    })
  })
})
