import {useFloating, useInteractions, offset, useClick, useDismiss, autoUpdate, flip, size} from '@floating-ui/react'
import PropTypes from 'prop-types'
import {useState} from 'react'
import {flushSync} from 'react-dom'

import {Button, ButtonGroup, Icon, Menu} from '../'
import {StyledLabelWrapper} from '../Button/StyledButton'

import {StyledIconButtonWrapper, StyledIconWrapper} from './StyledComponents'

/**
 *  Button with a menu that pops out on click.
 */
const ButtonMenu = props => {
  const {onOpen, children, onClick, buttonProps, label, icon} = props
  const [maxHeight, setMaxHeight] = useState(null)
  const [menuOpen, setMenuOpen] = useState(false)
  const [onOpenCalled, setOnOpenCalled] = useState(false)

  const handleClick = () => {
    setMenuOpen(!menuOpen)
    if (onOpen && !onOpenCalled) {
      onOpen()
      setOnOpenCalled(true)
    }
  }

  const handleClose = () => {
    setMenuOpen(false)
  }

  const chevronIcon = menuOpen ? 'chevron-up' : 'chevron-down'

  const {refs, floatingStyles, context} = useFloating({
    open: menuOpen,
    onOpenChange: setMenuOpen,
    placement: 'bottom-start',
    middleware: [
      offset(5),
      flip(),
      size({
        apply({availableHeight}) {
          // add some spacing between bottom of viewport and menu for aesthetic purposes
          flushSync(() => setMaxHeight(availableHeight - 15))
        }
      })
    ],
    whileElementsMounted: autoUpdate
  })

  const click = useClick(context)
  const dismiss = useDismiss(context)

  const {getReferenceProps, getFloatingProps} = useInteractions([click, dismiss])

  if (onClick) {
    return (
      <>
        <ButtonGroup ref={refs.setReference} {...getReferenceProps()}>
          <Button {...(buttonProps || {})} onClick={onClick} label={label} data-cy={props['data-cy']} icon={icon} />
          <StyledIconButtonWrapper
            icon={chevronIcon}
            onClick={handleClick}
            {...(buttonProps || {})}
            data-cy="btn-buttonmenu-open"
          />
        </ButtonGroup>
        {menuOpen && (
          <Menu
            floatingStyles={{...floatingStyles, maxHeight}}
            getFloatingProps={getFloatingProps}
            refs={refs}
            isOpen={menuOpen}
            onClose={handleClose}
          >
            {children}
          </Menu>
        )}
      </>
    )
  }

  return (
    <>
      <Button
        data-cy={props['data-cy']}
        {...(buttonProps || {})}
        ref={refs.setReference}
        {...getReferenceProps()}
        onClick={handleClick}
        icon={icon}
      >
        <StyledLabelWrapper>{label}</StyledLabelWrapper>
        <StyledIconWrapper>
          <Icon icon={chevronIcon} />
        </StyledIconWrapper>
      </Button>
      {menuOpen && (
        <Menu
          floatingStyles={{...floatingStyles, maxHeight}}
          getFloatingProps={getFloatingProps}
          refs={refs}
          isOpen={menuOpen}
          onClose={handleClose}
        >
          {children}
        </Menu>
      )}
    </>
  )
}

ButtonMenu.propTypes = {
  /**
   * Optional handler. If defined, a split button will be rendered
   */
  onClick: PropTypes.func,
  /**
   * Will be shown on the button
   */
  label: PropTypes.string,
  /**
   * Will be passed to the underlying Button
   */
  buttonProps: PropTypes.object,
  /**
   * Tree of <MenuItem>
   */
  children: PropTypes.any,
  /**
   * Callback when the menu is opened (only called once)
   */
  onOpen: PropTypes.func,
  /**
   * cypress selector string
   */
  'data-cy': PropTypes.string,
  /**
   * icon to display for the menu
   */
  icon: PropTypes.string
}

export default ButtonMenu
