import {useFloating, useInteractions, offset, useClick, useDismiss, autoUpdate, flip, size} from '@floating-ui/react'
import PropTypes from 'prop-types'
import {useState} from 'react'
import {flushSync} from 'react-dom'

import {Ball} from '../'

import {Menu} from './'

/**
 * Ball Button with a menu that pops out on click.
 */
const BallMenu = ({buttonProps, onOpen, children, isTransparent}) => {
  const [maxHeight, setMaxHeight] = useState(null)
  const [menuOpen, setMenuOpen] = useState(false)
  const [onOpenCalled, setOnOpenCalled] = useState(false)

  const handleClick = e => {
    setMenuOpen(!menuOpen)
    e.stopPropagation()

    if (onOpen && !onOpenCalled) {
      onOpen()
      setOnOpenCalled(true)
    }
  }

  const handleClose = () => {
    setMenuOpen(false)
  }

  const {refs, floatingStyles, context} = useFloating({
    open: menuOpen,
    onOpenChange: setMenuOpen,
    placement: 'bottom-start',
    middleware: [
      offset(7),
      flip(),
      size({
        apply({availableHeight}) {
          // add some spacing between bottom of viewport and menu for aesthetic purposes
          flushSync(() => setMaxHeight(availableHeight - 15))
        }
      })
    ],
    whileElementsMounted: autoUpdate
  })

  const click = useClick(context)
  const dismiss = useDismiss(context)

  const {getReferenceProps, getFloatingProps} = useInteractions([click, dismiss])

  return (
    <span ref={refs.setReference} {...getReferenceProps()}>
      <Ball {...buttonProps} onClick={handleClick} isTransparent={isTransparent} />
      {menuOpen && (
        <Menu
          floatingStyles={{...floatingStyles, maxHeight}}
          getFloatingProps={getFloatingProps}
          refs={refs}
          isOpen={menuOpen}
          onClose={handleClose}
        >
          {children}
        </Menu>
      )}
    </span>
  )
}

BallMenu.propTypes = {
  /**
   * Tree of <MenuItem>
   */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
  /**
   * Object of properties as described in Ball component
   */
  buttonProps: PropTypes.object,
  /**
   * Callback when the menu is opened (only called once)
   */
  onOpen: PropTypes.func,
  /**
   * Boolean to set transparency of the Ball component
   */
  isTransparent: PropTypes.bool
}

export default BallMenu
