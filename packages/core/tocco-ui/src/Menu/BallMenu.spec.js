import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import BallMenu from './BallMenu'
import MenuItem from './MenuItem'

describe('tocco-ui', () => {
  describe('Menu', () => {
    describe('BallMenu', () => {
      test('should render a Ball and not render Menu when its closed', async () => {
        testingLibrary.renderWithIntl(
          <BallMenu buttonProps={{icon: 'facebook'}}>
            <MenuItem>Test</MenuItem>
          </BallMenu>
        )
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByRole('button')).to.have.length(1)
        expect(screen.queryAllByText('Test')).to.have.length(0)
      })

      test('should set menu open on click and call onOpen only once', async () => {
        const onOpen = sinon.spy()
        testingLibrary.renderWithIntl(
          <BallMenu buttonProps={{icon: 'facebook'}} onOpen={onOpen}>
            <MenuItem>Test</MenuItem>
          </BallMenu>
        )
        await screen.findAllByTestId('icon')

        const ballMenuButton = screen.queryByRole('button')
        expect(screen.queryAllByText('Test')).to.have.length(0)

        fireEvent.click(ballMenuButton)
        expect(screen.queryAllByText('Test')).to.have.length(1)

        fireEvent.click(ballMenuButton)
        expect(screen.queryAllByText('Test')).to.have.length(0)

        expect(onOpen).to.be.calledOnce
      })
    })
  })
})
