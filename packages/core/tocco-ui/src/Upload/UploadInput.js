import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'
import {injectIntl} from 'react-intl'

import Button from '../Button'

import {StyledUploadInput, StyledSection, StyledDropzone} from './StyledComponents'

const UploadInput = ({id, onDrop: onDropProp, immutable, text, onChoose, intl, allowedFileTypes}) => {
  const onDrop = acceptedFiles => {
    const previewAddedFiles = acceptedFiles.map(file =>
      Object.assign(file, {
        preview: URL.createObjectURL(file)
      })
    )
    onDropProp(previewAddedFiles[0])
  }
  const handleClick = e => {
    onChoose()
    e.stopPropagation()
  }

  const msg = idParam => intl.formatMessage({id: idParam})

  return (
    <StyledUploadInput immutable={immutable} data-cy={`upload-${id}`}>
      <Dropzone onDrop={onDrop} multiple={false} disabled={immutable} title={text || 'drag and drop or click'}>
        {({getRootProps, getInputProps}) => (
          <StyledSection>
            <StyledDropzone {...getRootProps()}>
              <input {...getInputProps()} accept={allowedFileTypes.join(', ')} />
              <Button
                icon="arrow-to-top"
                label={msg('client.component.upload.uploadFile')}
                iconOnly={true}
                data-cy={`btn-upload-${id}`}
              />
            </StyledDropzone>
          </StyledSection>
        )}
      </Dropzone>
      {onChoose && (
        <Button onClick={handleClick} icon="folder" label={msg('client.component.upload.browseDocs')} iconOnly={true} />
      )}
    </StyledUploadInput>
  )
}

UploadInput.propTypes = {
  id: PropTypes.string,
  immutable: PropTypes.bool,
  onDrop: PropTypes.func.isRequired,
  onChoose: PropTypes.func,
  text: PropTypes.string,
  intl: PropTypes.object.isRequired,
  allowedFileTypes: PropTypes.arrayOf(PropTypes.string)
}

export default injectIntl(UploadInput)
