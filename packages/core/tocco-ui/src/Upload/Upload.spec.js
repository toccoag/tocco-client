import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Upload from './Upload'

const EMPTY_FUNC = () => {}

const sampleValue = {
  binaryLink: 'https://unsplash.it/150',
  thumbnailLink: 'https://unsplash.it/150',
  fileName: 'example_file.jpg'
}

describe('tocco-ui', () => {
  describe('Upload', () => {
    test('should show input if no value set', async () => {
      testingLibrary.renderWithIntl(
        <>
          <Upload onUpload={EMPTY_FUNC} />{' '}
        </>
      )
      expect(await screen.findByTestId('icon-arrow-to-top')).to.exist
    })

    test('should show view if value set', () => {
      testingLibrary.renderWithTheme(
        <>
          <Upload onUpload={EMPTY_FUNC} value={sampleValue} />{' '}
        </>
      )
      expect(screen.queryByTitle('example_file.jpg')).to.exist
      expect(screen.queryByTitle('download')).to.exist
    })
  })
})
