import PropTypes from 'prop-types'

import {ActionBarContainer} from '../ActionBar'

import {StyledSidepanelMainContent} from './StyledComponents'

const SidepanelMainContent = ({actionBar, children}) => {
  const content = actionBar ? <ActionBarContainer>{children}</ActionBarContainer> : children
  return <StyledSidepanelMainContent>{content}</StyledSidepanelMainContent>
}

SidepanelMainContent.propTypes = {
  actionBar: PropTypes.bool,
  children: PropTypes.any
}

export default SidepanelMainContent
