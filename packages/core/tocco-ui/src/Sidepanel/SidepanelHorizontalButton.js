import PropTypes from 'prop-types'

import {
  StyleButtonWrapper,
  StyledAddRemoveButton,
  StyledMenuWrapper,
  StyledSidepanelHorizontalButton,
  StyledLabel
} from './StyledHorizontalButtonComponents'

const SidepanelHorizontalButton = ({setActive, active, label, title, showAddRemoveBtn = true, menuElement}) => (
  <StyledSidepanelHorizontalButton
    active={active}
    onClick={() => setActive(!active)}
    data-cy="btn-horizontal"
    withoutBackground
  >
    <StyledLabel title={title}>{label}</StyledLabel>
    <StyleButtonWrapper>
      {showAddRemoveBtn && (
        <StyledAddRemoveButton
          active={active}
          onClick={e => {
            setActive(false)
            e.stopPropagation()
          }}
          icon={active ? 'minus' : 'plus'}
          dense
          data-cy="btn-add-remove"
          withoutBackground
        />
      )}
      {menuElement && <StyledMenuWrapper active={active}>{menuElement}</StyledMenuWrapper>}
    </StyleButtonWrapper>
  </StyledSidepanelHorizontalButton>
)

SidepanelHorizontalButton.propTypes = {
  label: PropTypes.string,
  title: PropTypes.string,
  setActive: PropTypes.func.isRequired,
  active: PropTypes.bool,
  showAddRemoveBtn: PropTypes.bool,
  menuElement: PropTypes.element
}

export default SidepanelHorizontalButton
