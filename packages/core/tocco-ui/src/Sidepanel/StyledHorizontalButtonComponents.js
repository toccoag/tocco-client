/* stylelint-disable no-descending-specificity */
/* stylelint-disable rule-empty-line-before */
import styled, {css} from 'styled-components'
import {device} from 'tocco-util'

import Button from '../Button/Button'
import {StyledItemLabel} from '../Menu'
import {scale, themeSelector} from '../utilStyles'

export const StyledAddRemoveButton = styled(Button)`
  &&& {
    background-color: transparent;
    border: 0;
    height: auto;
    border-radius: 0;
    padding: ${scale.space(-3.5)};

    && {
      &:hover *,
      &:focus * {
        color: ${({active}) =>
          active ? themeSelector.color('paper') : themeSelector.color('hoverInteractiveIcon')} !important;
      }
    }
  }
`

export const StyledMenuWrapper = styled.div`
  &&& {
    button {
      background-color: transparent;
      padding: ${scale.space(-3.5)};
    }

    &:hover button {
      color: ${({active}) => (active ? themeSelector.color('paper') : themeSelector.color('hoverInteractiveIcon'))};
      cursor: pointer;
    }

    button:focus {
      color: ${({active}) => (active ? themeSelector.color('paper') : themeSelector.color('hoverInteractiveIcon'))};
    }
  }
`

export const StyleButtonWrapper = styled.div`
  display: flex;
  ${/* sc-selector */ StyledMenuWrapper} button,
  ${/* sc-selector */ StyledAddRemoveButton} * {
    color: transparent;
  }
`

export const StyledSidepanelHorizontalButton = styled(Button)`
  width: 100%;
  border-radius: ${themeSelector.radii('button')};
  display: flex;
  padding: 0.3rem 0 0.3rem 1rem;
  margin-bottom: 0.2rem;
  background-color: ${({active}) => (active ? themeSelector.color('secondary') : 'transparent')};
  justify-content: space-between;

  && {
    *:not(${/* sc-selector */ StyledItemLabel}) {
      color: ${({active}) => active && themeSelector.color('paper')};
    }
  }

  /* allow hover styles only on non-touch devices */
  ${!device.isPrimaryTouchDevice() &&
  css`
    &:hover {
      span,
      ${/* sc-selector */ StyledMenuWrapper} button,
      ${/* sc-selector */ StyledAddRemoveButton} * {
        color: ${themeSelector.color('paper')};
      }
      background-color: ${({active}) => !active && themeSelector.color('secondaryLight')};
      cursor: pointer;
    }
  `}

  /* apply font color only on touch devices to always show them */
  ${device.isPrimaryTouchDevice() &&
  css`
    span,
    ${/* sc-selector */ StyledMenuWrapper} button,
    ${/* sc-selector */ StyledAddRemoveButton} * {
      color: ${themeSelector.color('secondary')};
    }
  `}
`

export const StyledLabel = styled.span`
  width: 85%;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: left;
`
