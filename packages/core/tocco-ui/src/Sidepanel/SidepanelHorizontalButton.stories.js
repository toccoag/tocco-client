import {BallMenu, MenuItem} from '..'

import {SidepanelHorizontalButton, SidepanelContainer, Sidepanel, SidepanelHeader, SidepanelMainContent} from '.'

export default {
  title: 'Tocco-UI/Sidepanel',
  component: SidepanelHorizontalButton,
  argTypes: {}
}

const Menu = () => (
  <BallMenu buttonProps={{icon: 'ellipsis-v'}}>
    <MenuItem>Editieren</MenuItem>
    <MenuItem>Löschen</MenuItem>
  </BallMenu>
)

const Template = args => {
  return (
    <SidepanelContainer sidepanelPosition="left" sidepanelCollapsed={false}>
      <Sidepanel>
        <SidepanelHeader />
        <div>
          <SidepanelHorizontalButton {...args} />
        </div>
      </Sidepanel>
      <SidepanelMainContent>
        <div>Content</div>
      </SidepanelMainContent>
    </SidepanelContainer>
  )
}

export const HorizontalButton = Template.bind({})
HorizontalButton.args = {
  label: 'Label',
  description: 'Description',
  active: false,
  showAddRemoveBtn: true,
  menuElement: <Menu />
}
