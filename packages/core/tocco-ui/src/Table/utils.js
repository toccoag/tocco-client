export const getLabel = column =>
  !column.useLabel || column.useLabel.toLowerCase() === 'yes' ? column.label || '' : ''
