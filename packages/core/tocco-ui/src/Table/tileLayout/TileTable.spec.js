import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import TileTable from './TileTable'

const defaultProps = {
  paginationInfo: {
    totalCount: 1,
    recordsPerPage: 10,
    currentPage: 1
  },
  columns: [],
  data: [],
  onRowClick: () => {}
}

describe('tocco-ui', () => {
  describe('TileTable', () => {
    beforeEach(() => {
      window.ResizeObserver = ResizeObserver
    })

    test('should render table', async () => {
      testingLibrary.renderWithIntl(<TileTable {...defaultProps} />)
      await screen.findAllByTestId('icon')

      expect(screen.queryByText('client.component.table.noData')).to.exist
      expect(screen.queryByText('client.component.pagination.text')).to.exist
    })

    test('should ignore table layout scope', async () => {
      testingLibrary.renderWithIntl(
        <TileTable
          {...defaultProps}
          columns={[
            {id: 'tableColumn', layoutScope: 'table', children: []},
            {id: 'tileColumn', layoutScope: 'tile', children: []},
            {id: 'anyColumn', children: []}
          ]}
          data={[{tableColumn: 'table data', tileColumn: 'tile data', anyColumn: 'any data'}]}
        />
      )
      await screen.findAllByTestId('icon')

      expect(screen.queryByText('tile data')).to.exist
      expect(screen.queryByText('any data')).to.exist
      expect(screen.queryByText('table data')).to.not.exist
    })
  })
})
