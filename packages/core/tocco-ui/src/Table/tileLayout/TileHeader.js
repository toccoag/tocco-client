import PropTypes from 'prop-types'

import {dataPropType} from '../propTypes'
import MultiSelectHeader from '../selection/MultiSelectHeader'
import selectionStyles, {selectionStylePropType} from '../selection/selectionStyles'

import {StyledTileHead, StyledTileHeadRow} from './StyledComponents'

const TileHeader = ({
  data,
  tileHeader: Header,
  selectionStyle,
  hasSelectionHeader,
  isSelected,
  selectionChange,
  selectionFilterFn
}) => {
  return (
    <StyledTileHead>
      <StyledTileHeadRow>
        {hasSelectionHeader && selectionStyle === selectionStyles.MULTI && (
          <MultiSelectHeader
            isSelected={isSelected}
            selectionChange={selectionChange}
            selectionFilterFn={selectionFilterFn}
            data={data}
          />
        )}
        {Header && <Header />}
      </StyledTileHeadRow>
    </StyledTileHead>
  )
}

TileHeader.propTypes = {
  tileHeader: PropTypes.elementType,
  selectionStyle: selectionStylePropType,
  hasSelectionHeader: PropTypes.bool,
  isSelected: PropTypes.func.isRequired,
  data: dataPropType.isRequired,
  selectionChange: PropTypes.func.isRequired,
  selectionFilterFn: PropTypes.func
}

export default TileHeader
