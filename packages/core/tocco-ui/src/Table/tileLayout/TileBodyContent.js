import PropTypes from 'prop-types'

import {InProgressRow, NoDataRow} from '../FullRows'

const TileBodyContent = ({loading, hasData, children}) => {
  if (loading) {
    return <InProgressRow />
  }
  if (hasData === false) {
    return <NoDataRow />
  }
  return children
}

TileBodyContent.propTypes = {
  loading: PropTypes.bool,
  hasData: PropTypes.bool,
  children: PropTypes.node
}

export default TileBodyContent
