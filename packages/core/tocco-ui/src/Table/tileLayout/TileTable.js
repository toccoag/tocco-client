import PropTypes from 'prop-types'
import React, {useRef} from 'react'
import {js} from 'tocco-util'

import {scrollBehaviourPropType} from '../../util/propTypes'
import {ScrollBehaviour} from '../../util/scrollBehaviour'
import {columnPropType, dataPropType, keyPropType} from '../propTypes'
import {selectionStylePropType} from '../selection/selectionStyles'
import useSelection from '../selection/useSelection'
import TableFooter from '../TableFooter'
import useEnhancedColumns from '../useEnhancedColumns'

import {StretchingTableContainer, StyledTableWrapper, StyledTable, StyledTableBody} from './StyledComponents'
import TileBodyContent from './TileBodyContent'
import TileHeader from './TileHeader'
import TileTableRow from './TileTableRow'

const TileTable = ({
  columns: columnsProp,
  selection,
  data,
  onSelectionChange,
  selectionStyle,
  hasSelectionHeader = true,
  paginationInfo,
  dataLoadingInProgress,
  onRowClick,
  clickable,
  onPageChange,
  onPageRefresh,
  scrollBehaviour = ScrollBehaviour.INLINE,
  selectionFilterFn,
  lastOpened,
  tileHeader
}) => {
  const isLastOpened = key => lastOpened && key === lastOpened

  const tableEl = useRef(null)

  const {isSelected, selectionChange} = useSelection(selection, data, onSelectionChange)

  const [columns] = useEnhancedColumns(columnsProp, 'tile', {
    showNumbering: false,
    selection,
    selectionStyle,
    isSelected,
    selectionChange,
    selectionFilterFn,
    hasSelectionHeader
  })

  const handlePageChange = onPageChange
  const handlePageRefresh = onPageRefresh

  const trOnClick = entity => e => {
    if (e.shiftKey) {
      selectionChange(entity.__key, true, true)
    } else if (e.metaKey || e.ctrlKey) {
      selectionChange(entity.__key)
    } else if (clickable && onRowClick) {
      onRowClick(entity.__key)
    }
  }

  const itemCount = data.length

  return (
    <StyledTableWrapper>
      <StretchingTableContainer data-cy="tbl">
        <div>
          <StyledTable ref={tableEl} columns={columns} scrollBehaviour={scrollBehaviour}>
            <TileHeader
              columns={columns}
              data={data}
              selectionStyle={selectionStyle}
              hasSelectionHeader={hasSelectionHeader}
              isSelected={isSelected}
              selectionChange={selectionChange}
              selectionFilterFn={selectionFilterFn}
              tileHeader={tileHeader}
            />
            <StyledTableBody>
              <TileBodyContent loading={dataLoadingInProgress} hasData={data.length > 0}>
                {Array.from({length: itemCount}, (_, index) => {
                  const rowData = data[index]
                  return (
                    <TileTableRow
                      key={`list-row-${rowData.__key}`}
                      rowData={rowData}
                      columns={columns}
                      onClick={trOnClick}
                      clickable={clickable}
                      index={index}
                      selected={isSelected(rowData.__key)}
                      lastOpened={isLastOpened(rowData.__key)}
                    />
                  )
                })}
              </TileBodyContent>
            </StyledTableBody>
          </StyledTable>
        </div>
      </StretchingTableContainer>
      <TableFooter
        onPageChange={handlePageChange}
        onPageRefresh={handlePageRefresh}
        paginationInfo={paginationInfo}
        tableEl={tableEl}
      />
    </StyledTableWrapper>
  )
}

TileTable.propTypes = {
  columns: PropTypes.arrayOf(columnPropType).isRequired,
  data: dataPropType,
  dataLoadingInProgress: PropTypes.bool,
  paginationInfo: PropTypes.shape({
    totalCount: PropTypes.number,
    currentPage: PropTypes.number,
    recordsPerPage: PropTypes.number
  }),
  selectionStyle: selectionStylePropType,
  hasSelectionHeader: PropTypes.bool,
  selection: PropTypes.arrayOf(keyPropType),
  tileHeader: PropTypes.elementType,
  scrollBehaviour: scrollBehaviourPropType,
  clickable: PropTypes.bool,
  onSelectionChange: PropTypes.func,
  onPageChange: PropTypes.func,
  onPageRefresh: PropTypes.func,
  onRowClick: PropTypes.func,
  selectionFilterFn: PropTypes.func,
  lastOpened: keyPropType
}

const areEqual = (prevProps, nextProps) => {
  const diff = Object.keys(js.difference(prevProps, nextProps))
  return diff.length === 0
}

export default React.memo(TileTable, areEqual)
