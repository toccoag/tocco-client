import styled, {css} from 'styled-components'

import {declareFont, scale, StyledFontAwesomeAdapterWrapper, themeSelector} from '../../index'
import {ScrollBehaviour} from '../../util/scrollBehaviour'
import {StyledTableFooter} from '../StyledComponents'

export const StyledTableCell = styled.td`
  && {
    padding: ${scale.space(-1)} ${scale.space(-1.5)};
    align-content: center;
    box-sizing: content-box;
    background-color: transparent;
    display: grid;

    ${StyledFontAwesomeAdapterWrapper} {
      color: ${themeSelector.color('text')};
      display: inline-block;
      margin-right: ${scale.space(-2)};
    }

    & > * {
      justify-self: ${({alignment}) => (alignment === 'justify' ? 'stretch' : alignment)};
    }

    ${({asTitle}) =>
      asTitle &&
      css`
        & span {
          font-weight: ${themeSelector.fontWeight('bold')};
          font-size: ${scale.font(3)};
          margin-bottom: ${scale.space(-2)};
        }
      `}

    ${({hasLabel}) =>
      hasLabel &&
      css`
        grid-template-columns: 40% 60%;
        gap: ${scale.space(-1.5)};
        overflow: hidden;

        @media only screen and (max-width: 500px) {
          display: flex;
          flex-direction: column;
          margin-bottom: ${scale.space(-1)};
        }
      `}
  }
`

export const StyledTableCellLabel = styled.span`
  && {
    background-color: transparent;
    ${declareFont({
      color: themeSelector.color('textLight')
    })}
    display: inline-block;
    text-align: left;
    text-overflow: ellipsis;
    overflow: hidden;
    justify-self: start;

    ${({hasIcon}) =>
      hasIcon &&
      css`
        display: grid;
        grid-template-columns: 20px auto;
        gap: ${scale.space(-1.5)};
      `}

    @media only screen and (max-width: 500px) {
      font-weight: ${themeSelector.fontWeight('bold')};
    }
  }
`

const selectionStyles = css`
  display: contents;
  ${({clickable}) => clickable && 'cursor: pointer;'}

  &.selected {
    background-color: ${({theme}) => theme.colors.backgroundItemSelected};
  }

  &.lastOpened {
    background-color: ${({theme}) => theme.colors.backgroundItemLastOpened};
  }

  &.selectableRow:not(.selected):not(.lastOpened):hover {
    background-color: ${({theme}) => theme.colors.backgroundItemHover};
  }
`

export const StyledTileHead = styled.thead`
  display: contents;
`

export const StyledTileHeadRow = styled.tr`
  position: sticky;
  grid-column: 1 / -1;
  top: 0;
  ${declareFont({fontWeight: themeSelector.fontWeight('bold')})};
  user-select: none;
  display: flex;
  padding: 0;
  max-height: 30px;
`

export const StyledTableRow = styled.tr`
  ${selectionStyles}

  display: inline-block;
  border: solid 1px ${themeSelector.color('borderTable')};
  border-radius: ${themeSelector.radii('form')};
  background-color: ${themeSelector.color('paper')};
  padding: ${scale.space(-1)};
`

export const StyledTableBody = styled.tbody`
  display: contents;
`

export const StyledTable = styled.table`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));
  gap: ${scale.space(-0.375)};
  grid-auto-rows: min-content;
  overflow: auto;
  border-collapse: collapse;
  min-width: 100%;
  background-color: ${themeSelector.color('backgroundTableTiles')};
  padding: ${scale.space(0)};
  border-radius: ${themeSelector.radii('form')};
  ${({scrollBehaviour}) =>
    scrollBehaviour === ScrollBehaviour.INLINE &&
    css`
      position: absolute;
      inset: 0;
    `}

  /**
   * adapt tile width on small screens
   *  - able to show wider tiles on larger screens
   *  - only shrink tile width if only shown one per line
   **/
  @media only screen and (max-width: 500px) {
    grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  }
`

export const StretchingTableContainer = styled.div`
  grid-row-start: table-start;
  position: relative;
  overflow-x: auto;
`

export const StyledTableWrapper = styled.div`
  display: grid;
  background-color: ${themeSelector.color('paper')};
  grid-template: [table-start] 1fr [pagination-start] auto auto / 100%;
  height: 100%;

  /* reset external styles in old client */
  input[type='checkbox'],
  input[type='radio'] {
    margin: 3px 3px 3px 4px !important;
    min-width: 12px;
  }

  ${StyledTableFooter} {
    border-top: 0;
  }
`

export const StyledHeaderContentWrapper = styled.span`
  display: flex;
`

export const StyledHeaderContent = styled.div`
  width: 100%;
`
