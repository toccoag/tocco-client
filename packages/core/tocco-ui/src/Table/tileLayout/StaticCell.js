import PropTypes from 'prop-types'
import React from 'react'
import {js} from 'tocco-util'

import {Icon, Typography} from '../../index'
import tableLayouts from '../layouts'
import {columnPropType, rowDataPropType} from '../propTypes'
import {getLabel} from '../utils'

import {StyledTableCell, StyledTableCellLabel} from './StyledComponents'

const isEmpty = (column, rowData) => {
  if (!column.children.map(d => d.componentType).some(d => d !== 'field')) {
    return !rowData[column.id]
  }
  return false
}

const StaticCell = React.memo(({column, rowData, rowIdx}) => {
  if (isEmpty(column, rowData)) {
    return null
  }

  const label = !column.useAsTitle ? getLabel(column) : ''
  const hasLabel = Boolean(label)
  const hasIcon = Boolean(column.tileIcon)

  return (
    <StyledTableCell
      data-cy={`tbl-cell-${column.id}`}
      hasLabel={hasLabel}
      alignment={column.alignment}
      asTitle={column.useAsTitle}
    >
      {hasLabel && (
        <StyledTableCellLabel hasIcon={hasIcon}>
          {hasIcon && <Icon icon={column.tileIcon} />} {label}
        </StyledTableCellLabel>
      )}
      {column.CellRenderer ? (
        <column.CellRenderer rowData={rowData} column={column} rowIdx={rowIdx} layout={tableLayouts.tile} />
      ) : (
        <Typography.Span breakWords>{rowData[column.id]}</Typography.Span>
      )}
    </StyledTableCell>
  )
})

StaticCell.propTypes = {
  rowData: rowDataPropType.isRequired,
  column: columnPropType.isRequired,
  rowIdx: PropTypes.number
}

const areEqual = (prevProps, nextProps) => {
  if (!prevProps.column.dynamic) {
    return true
  }
  const diff = Object.keys(js.difference(prevProps, nextProps))
  return diff.length === 0
}

export default React.memo(StaticCell, areEqual)
