import PropTypes from 'prop-types'

import StaticCell from './StaticCell'
import {StyledTableRow} from './StyledComponents'

const TileTableRow = ({rowData, clickable, onClick, index, columns, selected, lastOpened}) => {
  const sortedColumns = columns.sort((a, b) => {
    if (a.useAsTitle === b.useAsTitle) {
      return 0
    }
    return a.useAsTitle ? -1 : 1
  })

  return (
    <StyledTableRow
      className={`selectableRow ${selected && 'selected'} ${lastOpened && 'lastOpened'}`}
      onClick={onClick(rowData)}
      clickable={clickable}
      data-cy="tbl-list-row"
    >
      {sortedColumns.map(column => (
        <StaticCell
          key={`table-static-cell-${rowData.__key}-${column.id}`}
          selected={selected}
          rowData={rowData}
          column={column}
          rowIdx={index}
        />
      ))}
    </StyledTableRow>
  )
}

TileTableRow.propTypes = {
  rowData: PropTypes.object,
  columns: PropTypes.array,
  clickable: PropTypes.bool,
  onClick: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.bool,
  lastOpened: PropTypes.bool
}

export default TileTableRow
