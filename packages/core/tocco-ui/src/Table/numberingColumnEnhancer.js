import PropTypes from 'prop-types'
import styled from 'styled-components'
import {StyledSpan} from 'tocco-ui'

export const getNumberingCell = showNumbering =>
  showNumbering
    ? {
        id: 'numbering-column',
        width: 40,
        resizable: false,
        fixedPosition: true,
        dynamic: true,
        sorting: {
          sortable: false
        },
        HeaderRenderer: () => null,
        CellRenderer: NumberingCellRenderer
      }
    : null

const StyledNumbering = styled(StyledSpan)`
  text-align: right;
  width: 100%;
`

const NumberingCellRenderer = ({rowIdx}) => {
  return <StyledNumbering title={rowIdx + 1}>{rowIdx + 1}</StyledNumbering>
}

NumberingCellRenderer.propTypes = {
  rowIdx: PropTypes.number.isRequired
}
