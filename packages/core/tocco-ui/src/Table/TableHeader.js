import PropTypes from 'prop-types'
import {useCallback} from 'react'
import {dragAndDrop, resize} from 'tocco-util'

import {columnPropType, dataPropType} from './propTypes'
import ResizingController from './ResizingController'
import SortingState from './SortingState'
import {getStickyStyling} from './stickyUtil'
import {
  StyledHeaderContentWrapper,
  StyledHeaderContent,
  StyledDnD,
  StyledDraggable,
  StyledTableHead,
  StyledTableHeaderCell,
  StyledTableRow
} from './StyledComponents'
import {getLabel} from './utils'

const ThContent = ({column, data}) => {
  const label = getLabel(column)

  return column.HeaderRenderer ? (
    <column.HeaderRenderer column={column} data={data} />
  ) : (
    <StyledHeaderContentWrapper>
      <StyledHeaderContent title={label} dangerouslySetInnerHTML={{__html: label}} />
      <SortingState column={column} />
    </StyledHeaderContentWrapper>
  )
}

ThContent.propTypes = {
  data: dataPropType,
  column: columnPropType
}

const TableHeader = props => {
  const {
    columns,
    data,
    onColumnPositionChange,
    onSortingChange,
    tableEl,
    onColumnWidthChanging,
    onColumnWidthChanged,
    columnWidths
  } = props
  const {dndEvents, dndState, dndCss} = dragAndDrop.useDnD(onColumnPositionChange)

  // tableEl is a ref and has not to be added to the deps array
  const currentResizeElementSelector = useCallback(
    resizeElement => tableEl.current.querySelector(`th[id='tbl-header-cell-${resizeElement.id}']`),
    [] // eslint-disable-line react-hooks/exhaustive-deps
  )

  const handleResize = useCallback((el, {width}) => onColumnWidthChanging(el.id, width), [onColumnWidthChanging])
  const handleResizeFinished = useCallback(el => onColumnWidthChanged(el.id), [onColumnWidthChanged])

  const {startResize, resizingEvents, resizeState} = resize.useResize(
    currentResizeElementSelector,
    handleResize,
    handleResizeFinished
  )

  const {isResizing, resizingElement} = resizeState

  const handleTableHeaderClick = column => e => {
    if (!isResizing && column.sorting?.sortable) {
      onSortingChange(column.id, e.shiftKey)
    }
  }

  const StyledTableHeaderCells = columns.map((column, index) => {
    const headerCellKey = `tbl-header-cell-${column.id}`
    const headerCellDropKey = `header-cell-drop-${column.id}`
    const isSortable = column.sorting && column.sorting.sortable
    const isResizingThisCell = isResizing && column.id === resizingElement?.id
    const isDraggedOver = dndState.currentlyDragOver === column.id && dndState.currentlyDragging !== column.id
    const isDragged = dndState.currentlyDragging === column.id

    const nextColumn = columns.length > index + 1 ? columns[index + 1] : null
    const isLeftDraggedOverNextCol =
      nextColumn &&
      dndState.currentlyDragOver === nextColumn.id &&
      dndState.horizontalDropPosition === dragAndDrop.HorizontalDropPosition.Left
    const isRightDraggedOverThisCol =
      isDraggedOver && dndState.horizontalDropPosition === dragAndDrop.HorizontalDropPosition.Right

    const showDropIndicator = isRightDraggedOverThisCol || isLeftDraggedOverNextCol

    /**
     * - The drop indicator of the drag and drop feature has to be within/part of the draggable dom element.
     *   Otherwise the dragged element cannot be dropped on the drop indicator. (https://toccoag.atlassian.net/browse/TOCDEV-9132)
     * - The drop indicator has to be added to the dom without moving the content
     *   Otherwise the table header is not aligned with the table content (https://toccoag.atlassian.net/browse/TOCDEV-9635)
     */

    return (
      <StyledTableHeaderCell
        key={headerCellKey}
        data-cy={headerCellKey}
        id={headerCellKey}
        onClick={handleTableHeaderClick(column)}
        isResizingAnyCell={isResizing}
        isResizingThisCell={isResizingThisCell}
        isSortable={isSortable}
        alignment={column.alignment}
        style={getStickyStyling(column, index, true, columns, columnWidths)}
        {...(!column.fixedPosition && dndEvents(column.id))}
      >
        <StyledDraggable
          id={headerCellDropKey}
          key={headerCellDropKey}
          showDropIndicator={showDropIndicator}
          css={dndCss}
          {...(!column.fixedPosition && {draggable: true})}
        >
          <StyledDnD isDragged={isDragged}>
            <ThContent column={column} data={data} />
          </StyledDnD>
        </StyledDraggable>
        {column.resizable && !dndState.currentlyDragging && (
          <ResizingController column={column} startResize={startResize} />
        )}
      </StyledTableHeaderCell>
    )
  })

  return (
    <StyledTableHead {...resizingEvents}>
      <StyledTableRow>{StyledTableHeaderCells}</StyledTableRow>
    </StyledTableHead>
  )
}

TableHeader.propTypes = {
  columns: PropTypes.arrayOf(columnPropType).isRequired,
  data: dataPropType,
  onColumnWidthChanging: PropTypes.func,
  onColumnWidthChanged: PropTypes.func,
  onColumnPositionChange: PropTypes.func,
  onSortingChange: PropTypes.func,
  tableEl: PropTypes.shape({current: PropTypes.object}),
  columnWidths: PropTypes.arrayOf(PropTypes.number)
}

export default TableHeader
