import PropTypes from 'prop-types'
import React, {useCallback, useEffect, useRef, useState} from 'react'
import {js, react} from 'tocco-util'

import {scrollBehaviourPropType} from '../util/propTypes'
import {ScrollBehaviour} from '../util/scrollBehaviour'

import tableLayouts, {tableLayoutPropType} from './layouts'
import {columnPropType, dataPropType, keyPropType} from './propTypes'
import {selectionStylePropType} from './selection/selectionStyles'
import useSelection from './selection/useSelection'
import {extractColumnWidths} from './stickyUtil'
import {StretchingTableContainer, StyledTableWrapper} from './StyledComponents'
import TableFooter from './TableFooter'
import TableHeader from './TableHeader'
import TableRow from './TableRow'
import TileTable from './tileLayout'
import useEnhancedColumns from './useEnhancedColumns'
import VirtualTable from './VirtualTable'

const Table = ({
  columns: columnsProp,
  selection,
  data,
  onSelectionChange,
  selectionStyle,
  hasSelectionHeader = true,
  paginationInfo,
  onColumnPositionChange,
  onSortingChange,
  dataLoadingInProgress,
  onRowClick,
  clickable,
  onPageChange,
  onPageRefresh,
  layout = tableLayouts.table,
  scrollBehaviour = ScrollBehaviour.INLINE,
  onColumnWidthChange,
  selectionFilterFn,
  lastOpened,
  showNumbering,
  disableVirtualTable,
  tileHeader
}) => {
  const {isSelected, selectionChange} = useSelection(selection, data, onSelectionChange)

  const [columns, setColumns] = useEnhancedColumns(columnsProp, 'table', {
    showNumbering,
    selection,
    selectionStyle,
    isSelected,
    selectionChange,
    selectionFilterFn,
    hasSelectionHeader
  })

  const tableEl = useRef(null)
  const [wrapperRef, contentRect] = react.useResizeObserver()
  const [columnWidths, setColumnWidths] = useState([])
  const listRef = useRef(null)

  const scrollToTopAndPropagate =
    delegate =>
    (...args) => {
      if (listRef.current) {
        listRef.current.scrollToItem(0)
      }
      if (delegate) {
        delegate(...args)
      }
    }

  const handlePageChange = scrollToTopAndPropagate(onPageChange)
  const handlePageRefresh = onPageRefresh ? scrollToTopAndPropagate(onPageRefresh) : undefined
  const handleSortingChange = scrollToTopAndPropagate(onSortingChange)

  const onColumnWidthChanged = useCallback(
    columnId => {
      const finalWidth = columns.find(c => c.id === columnId)?.width
      if (onColumnWidthChange && typeof finalWidth !== 'undefined') {
        onColumnWidthChange(columnId, finalWidth)
      }
    },
    [columns, onColumnWidthChange]
  )

  const onColumnWidthChanging = useCallback(
    (columnId, width) => {
      setColumns([
        ...columns.map(c =>
          c.id === columnId
            ? {
                ...c,
                width
              }
            : c
        )
      ])
    },
    [columns, setColumns]
  )

  // if contentRect changes, something in the table has a different size
  // columns is also in the depdency array as a user can manually change the width of a sticky column
  useEffect(() => {
    setColumnWidths(extractColumnWidths(tableEl))
  }, [contentRect, columns])

  const isLastOpened = key => lastOpened && key === lastOpened

  const trOnClick = entity => e => {
    if (e.shiftKey) {
      selectionChange(entity.__key, true, true)
    } else if (e.metaKey || e.ctrlKey) {
      selectionChange(entity.__key)
    } else if (clickable && onRowClick) {
      onRowClick(entity.__key)
    }
  }

  if (layout === tableLayouts.tile) {
    return (
      <TileTable
        columns={columnsProp}
        selection={selection}
        data={data}
        onSelectionChange={onSelectionChange}
        selectionStyle={selectionStyle}
        hasSelectionHeader={hasSelectionHeader}
        paginationInfo={paginationInfo}
        dataLoadingInProgress={dataLoadingInProgress}
        onRowClick={onRowClick}
        clickable={clickable}
        onPageChange={onPageChange}
        onPageRefresh={onPageRefresh}
        scrollBehaviour={scrollBehaviour}
        selectionFilterFn={selectionFilterFn}
        lastOpened={lastOpened}
        tileHeader={tileHeader}
      />
    )
  }

  return (
    <StyledTableWrapper ref={wrapperRef}>
      <StretchingTableContainer data-cy="tbl">
        <VirtualTable
          itemCount={data.length}
          itemSize={30}
          tableEl={tableEl}
          columns={columns}
          scrollBehaviour={scrollBehaviour}
          dataLoadingInProgress={dataLoadingInProgress}
          hasData={data.length > 0}
          ref={listRef}
          header={
            <TableHeader
              columns={columns}
              data={data}
              onColumnWidthChanging={onColumnWidthChanging}
              onColumnWidthChanged={onColumnWidthChanged}
              onColumnPositionChange={onColumnPositionChange}
              tableEl={tableEl}
              onSortingChange={handleSortingChange}
              columnWidths={columnWidths}
            />
          }
          row={TableRow(data, columns, columnWidths, trOnClick, isSelected, isLastOpened, clickable)}
          forceCompleteMode={disableVirtualTable}
        />
      </StretchingTableContainer>
      <TableFooter
        onPageChange={handlePageChange}
        onPageRefresh={handlePageRefresh}
        paginationInfo={paginationInfo}
        tableEl={tableEl}
      />
    </StyledTableWrapper>
  )
}

Table.propTypes = {
  /**
   * Array of column definitions. Determines how the cells are rendered.
   */
  columns: PropTypes.arrayOf(columnPropType).isRequired,
  /**
   * List of data to be displayed in the table. Should contain a __key property.
   */
  data: dataPropType,
  /**
   * True if data loading is in process.
   */
  dataLoadingInProgress: PropTypes.bool,
  /**
   * Used to display pagination
   */
  paginationInfo: PropTypes.shape({
    totalCount: PropTypes.number,
    currentPage: PropTypes.number,
    recordsPerPage: PropTypes.number
  }),
  /**
   * Determines if a select row is added
   */
  selectionStyle: selectionStylePropType,
  /**
   * Show selection header (only used for selction style multi; default: true)
   */
  hasSelectionHeader: PropTypes.bool,
  /**
   * List of selected keys
   */
  selection: PropTypes.arrayOf(keyPropType),
  /*
   * Defines the table layout (default: table)
   */
  layout: tableLayoutPropType,
  tileHeader: PropTypes.elementType,
  /**
   * Defines how the table to handle scroll (default: inline)
   */
  scrollBehaviour: scrollBehaviourPropType,
  /**
   * Boolean flag to disable clicking on rows
   */
  clickable: PropTypes.bool,
  /**
   * Callback for selection changes
   */
  onSelectionChange: PropTypes.func,
  /**
   * Callback on page change
   */
  onPageChange: PropTypes.func,
  /**
   * Callback on page reload
   */
  onPageRefresh: PropTypes.func,
  /**
   * Callback if a column is drag and dropped to a new position
   */
  onColumnPositionChange: PropTypes.func,
  /**
   * Callback if sorting changed
   */
  onSortingChange: PropTypes.func,
  /**
   * Callback if column got resized with drag and drop
   */
  onColumnWidthChange: PropTypes.func,
  /**
   * Callback if a row is clicked
   */
  onRowClick: PropTypes.func,
  /**
   * Returns a bool to determine if passed row is selectable
   */
  selectionFilterFn: PropTypes.func,
  /**
   * Key of last opened entity (if null no row is highlighted)
   */
  lastOpened: keyPropType,
  /**
   * Activate row number (Default: false)
   */
  showNumbering: PropTypes.bool,
  /**
   * Flag to disable virtual windowed table (Default: false)
   */
  disableVirtualTable: PropTypes.bool
}

const areEqual = (prevProps, nextProps) => {
  const diff = Object.keys(js.difference(prevProps, nextProps))
  return diff.length === 0
}

export default React.memo(Table, areEqual)
