import {useState, useCallback, useMemo} from 'react'

const removeAllTextSelections = () => {
  if (document.getSelection) {
    document.getSelection().removeAllRanges()
  }
}

const useSelection = (selection, data, onSelectChange) => {
  const [lastSelected, setLastSelected] = useState(null)
  const currentKeys = useMemo(() => (data ? data.map(e => e.__key) : []), [data])

  const isSelected = useCallback(key => (selection || []).includes(key), [selection])

  const selectionChange = useCallback(
    (key, value, shiftSelection) => {
      if (Array.isArray(key)) {
        onSelectChange(key, value)
        setLastSelected(null)
      } else {
        if (shiftSelection) {
          removeAllTextSelections()

          const idxLastSelection = lastSelected ? currentKeys.findIndex(k => k === lastSelected) : 0
          const idxSelection = currentKeys.findIndex(k => k === key)
          onSelectChange(
            currentKeys.slice(Math.min(idxLastSelection, idxSelection), Math.max(idxLastSelection, idxSelection) + 1),
            value
          )
        } else {
          onSelectChange([key], value || !isSelected(key))
          setLastSelected(key)
        }
      }
    },
    [currentKeys, onSelectChange, isSelected, lastSelected]
  )

  return {
    isSelected,
    selectionChange
  }
}

export default useSelection
