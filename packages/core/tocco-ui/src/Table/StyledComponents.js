import _get from 'lodash/get'
import styled, {css} from 'styled-components'

import {Ball, declareFont, scale, StyledBall, StyledFontAwesomeAdapterWrapper, themeSelector} from '../'
import {ScrollBehaviour} from '../util/scrollBehaviour'

const borderColor = ({theme}) => _get(theme, 'colors.border')
const basePadding = scale.space(-1.5)

export const StyledTableFooter = styled.div`
  grid-row-start: pagination-start;
  display: flex;
  flex-wrap: wrap;
  padding-top: ${scale.space(-0.5)};
  padding-bottom: ${scale.space(-1)};
  border-top: 1px solid ${borderColor};

  ${StyledBall} {
    margin-right: ${scale.space(-1)};
  }
`

export const StyledReloadButton = styled(Ball)`
  height: 25px;
  width: 25px;
`

export const StyledTableCell = styled.td`
  && {
    padding: ${basePadding};
    background-color: ${themeSelector.color('paper')};
    align-content: center;
    box-sizing: content-box;

    &,
    input {
      text-align: ${({alignment}) => alignment};
    }

    ${StyledFontAwesomeAdapterWrapper} {
      color: ${themeSelector.color('text')};
      display: inline-block;
    }
  }
`

export const StyledDnD = styled.div`
  padding: ${basePadding};
  width: 100%;
  opacity: ${({isDragged}) => (isDragged ? 0.5 : 1)};
`

export const StyledResizeHandle = styled.span`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  background: ${({theme}) => theme.colors.backgroundItemSelected};
  opacity: 0;
  width: 3px;
  cursor: col-resize;
`

export const StyledTableHeaderCell = styled.th`
  && {
    position: sticky;
    top: 0;
    background-color: ${themeSelector.color('paper')};
    text-align: ${({alignment}) => alignment};
    border-bottom: 2px solid ${borderColor};
    ${declareFont({fontWeight: themeSelector.fontWeight('bold')})};
    user-select: none;
    cursor: ${({isSortable}) => (isSortable ? 'pointer' : 'auto')};
    white-space: nowrap;
    display: flex;
    padding: 0;
    z-index: 2; /* higher than StyledIndicatorsContainerWrapper */
    overflow: hidden;
    ${({id}) => id === 'header-cell-navigation-column' && 'z-index: 1'};
    ${({isResizingThisCell, theme}) =>
      isResizingThisCell &&
      `
    background-color: ${theme.colors.backgroundItemHover};

    > ${StyledResizeHandle} {
      opacity: 1;
    }
  `}
    ${({isResizingAnyCell, theme}) =>
      !isResizingAnyCell &&
      `
    &:hover {
      background-color: ${theme.colors.backgroundItemHover};

      > ${StyledResizeHandle} {
        opacity: 1;
      }
    }
  `}
    ${({isResizingThisCell, isSortable, fixedPosition, theme}) =>
      !isSortable
        ? `
    &:hover {
      background-color: ${theme.colors.paper};
    }
    `
        : !isResizingThisCell &&
          !isSortable &&
          !fixedPosition &&
          `
        &:hover {
          background-color: transparent;
        }
    `}
  }
`

const selectionStyles = css`
  display: contents;
  ${({clickable}) => clickable && 'cursor: pointer;'}

  &.selected {
    > ${StyledTableCell} {
      background-color: ${({theme}) => theme.colors.backgroundItemSelected};
    }
  }

  &.lastOpened {
    > ${StyledTableCell} {
      background-color: ${({theme}) => theme.colors.backgroundItemLastOpened};
    }
  }

  &.selectableRow:not(.selected):not(.lastOpened):hover {
    > ${StyledTableCell} {
      background-color: ${({theme}) => theme.colors.backgroundItemHover};
    }
  }
`

export const StyledSortingSpan = styled.span`
  height: 100%;
  margin-right: 2px;
`

export const StyledSortingRank = styled.span`
  position: relative;
  top: -0.5rem;
  font-weight: ${themeSelector.fontWeight('regular')};
`

export const StyledFullRowProgress = styled.td`
  && {
    grid-column: 1 / -1;
    padding-top: ${basePadding} 0 0 0;
    height: 50px;
    text-align: center;
    border: 0;
  }
`

export const StyledFullRow = styled.td`
  && {
    grid-column: 1 / -1;
    padding-top: ${basePadding} 0 0 0;
    height: 50px;
    text-align: center;
    border: 0;
    ${declareFont()};
  }
`

export const StyledTableHead = styled.thead`
  ${selectionStyles}
`

export const StyledTableRow = styled.tr`
  ${selectionStyles}

  && > td {
    ${({even, theme}) => even && `background-color: ${theme.colors.backgroundTableEvenRow}`};
  }
`

export const StyledTableBody = styled.tbody`
  ${selectionStyles}
`

export const StyledTable = styled.table`
  ${({overflowAuto}) => (overflowAuto ? `overflow: auto;` : null)}
  display: grid;
  border-collapse: collapse;
  grid-template-columns: ${({columns}) =>
    columns
      .map(column => {
        if (column.width) {
          return column.width + 'px'
        }
        return column.shrinkToContent ? 'max-content' : 'minmax(100px, auto)'
      })
      .join(' ')};
  grid-auto-rows: min-content;
  min-width: 100%;
  ${({scrollBehaviour}) =>
    scrollBehaviour === ScrollBehaviour.INLINE &&
    css`
      position: absolute;
      inset: 0;
    `}
`

export const StretchingTableContainer = styled.div`
  grid-row-start: table-start;
  position: relative;
  overflow-x: auto;
`

export const StyledTableWrapper = styled.div`
  display: grid;
  background-color: ${themeSelector.color('paper')};
  grid-template-rows: [table-start] 1fr [pagination-start] auto auto;
  grid-template-columns: 100%;
  height: 100%;

  /* reset external styles in old client */
  input[type='checkbox'],
  input[type='radio'] {
    margin: 3px 3px 3px 4px !important;
    min-width: 12px;
  }
`

export const StyledDraggable = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  box-sizing: border-box;
  background-color: ${themeSelector.color('paper')};
  padding: 1px; /* default th padding */
  position: relative;

  ${({showDropIndicator, theme}) =>
    showDropIndicator &&
    css`
      &:after {
        content: '';
        width: 3px;
        top: 0;
        right: 0;
        height: 100%;
        position: absolute;
        background-color: ${theme.colors.text};
      }
    `}
`

export const StyledHeaderContentWrapper = styled.span`
  display: flex;
`

export const StyledHeaderContent = styled.div`
  width: 100%;
`
