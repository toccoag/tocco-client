import PropTypes from 'prop-types'
import React, {forwardRef, useContext, useState} from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import {FixedSizeList} from 'react-window'

import {scrollBehaviourPropType} from '../util'

import {InProgressRow, NoDataRow} from './FullRows'
import {columnPropType} from './propTypes'
import {StyledTable, StyledTableBody} from './StyledComponents'

/**
 * If there are more than this number of records on one page, we render a windowed list (see `FixedSizeList` of
 * `react-window` library) to make sure everything still renders fast enough.
 *
 * The windowed list has the downside, that it feels bit "flickery" at times, because the rows are shown and
 * hidden dynamically. Especially the positioning of the sticky header row isn't very nice, because it moves
 * a couple of pixels during the scrolling movement (e.g. it looks like the header is drawn down during fast
 * upward scrolling movements, which doesn't look very nice, even though it's repositioned immediately).
 *
 * Therefore, we use the windowed list only if we have to when the number of records hits the browser's
 * rendering capabilities.
 */
const WINDOWED_LIST_THRESHOLD = 100

const ListMode = {
  COMPLETE: 'complete',
  WINDOWED: 'windowed'
}

/** Context for cross component communication */
const VirtualTableContext = React.createContext({
  top: 0,
  header: <></>,
  tableEl: null,
  columns: null,
  scrollBehaviour: null,
  dataLoadingInProgress: false,
  hasData: null,
  itemCount: null,
  listMode: null
})

/**
 * The virtual table. It basically accepts all of the same params as the original FixedSizeList.
 *
 * Credits for this to make the `FixedSizeList` of `react-window` work with tables goes to:
 * https://lkcozy.github.io/code-notes/jsreactreact-window
 */
const VirtualTable = forwardRef(
  (
    {
      row,
      header,
      tableEl,
      columns,
      scrollBehaviour,
      dataLoadingInProgress,
      hasData,
      itemCount,
      forceCompleteMode,
      ...rest
    },
    listRef
  ) => {
    const [top, setTop] = useState(0)

    const listMode = !forceCompleteMode && itemCount > WINDOWED_LIST_THRESHOLD ? ListMode.WINDOWED : ListMode.COMPLETE

    return (
      <VirtualTableContext.Provider
        value={{
          top,
          setTop,
          header,
          tableEl,
          columns,
          scrollBehaviour,
          dataLoadingInProgress,
          hasData,
          itemCount,
          listMode
        }}
      >
        {listMode === ListMode.COMPLETE ? (
          <CompleteList row={row} />
        ) : (
          <AutoSizer>
            {({height, width}) => (
              <FixedSizeList
                height={height}
                width={width}
                {...rest}
                itemCount={itemCount}
                innerElementType={Inner}
                onItemsRendered={props => {
                  const style =
                    listRef.current &&
                    // eslint-disable-next-line react/prop-types
                    listRef.current._getItemStyle(props.overscanStartIndex)
                  setTop((style && style.top) || 0)

                  // Call the original callback
                  // eslint-disable-next-line chai-friendly/no-unused-expressions
                  rest.onItemsRendered && rest.onItemsRendered(props)
                }}
                ref={listRef}
              >
                {row}
              </FixedSizeList>
            )}
          </AutoSizer>
        )}
      </VirtualTableContext.Provider>
    )
  }
)

VirtualTable.propTypes = {
  row: PropTypes.func,
  header: PropTypes.node,
  tableEl: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({current: PropTypes.instanceOf(Element)})]),
  columns: PropTypes.arrayOf(columnPropType).isRequired,
  scrollBehaviour: scrollBehaviourPropType,
  dataLoadingInProgress: PropTypes.bool,
  hasData: PropTypes.bool,
  itemCount: PropTypes.number,
  forceCompleteMode: PropTypes.bool
}

const Content = ({dataLoadingInProgress, hasData, rows}) => {
  if (dataLoadingInProgress) {
    return <InProgressRow />
  }
  if (hasData === false) {
    return <NoDataRow />
  }
  return rows
}

Content.propTypes = {
  dataLoadingInProgress: PropTypes.bool,
  hasData: PropTypes.bool,
  rows: PropTypes.node
}

/**
 * The Inner component of the virtual list. This is the "Magic".
 * Capture what would have been the top elements position and apply it to the table.
 * Other than that, render the header.
 */
const Inner = React.forwardRef(function Inner({children, ...rest}, ref) {
  const {header, top, tableEl, columns, scrollBehaviour, dataLoadingInProgress, hasData, listMode} =
    useContext(VirtualTableContext)
  return (
    <div {...rest} ref={ref}>
      <StyledTable
        ref={tableEl}
        columns={columns}
        scrollBehaviour={scrollBehaviour}
        style={listMode === ListMode.WINDOWED ? {top, position: 'absolute', width: '100%', height: '100%'} : null}
        overflowAuto={listMode === ListMode.COMPLETE}
      >
        {header}
        <StyledTableBody>
          <Content dataLoadingInProgress={dataLoadingInProgress} hasData={hasData} rows={children} />
        </StyledTableBody>
      </StyledTable>
    </div>
  )
})

Inner.propTypes = {
  children: PropTypes.node
}

const CompleteList = ({row, listRef}) => {
  const {itemCount} = useContext(VirtualTableContext)
  const rowElements = itemCount > 0 ? Array.from({length: itemCount}, (_, index) => row({index})) : null
  return <Inner ref={listRef}>{rowElements}</Inner>
}

CompleteList.propTypes = {
  row: PropTypes.func,
  listRef: PropTypes.func
}

export default VirtualTable
