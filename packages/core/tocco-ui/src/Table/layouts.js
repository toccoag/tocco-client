import PropTypes from 'prop-types'

const tableLayouts = {
  table: 'table',
  tile: 'tile'
}

const tableLayoutPropType = PropTypes.oneOf(Object.keys(tableLayouts).map(key => tableLayouts[key]))

export {tableLayouts as default, tableLayoutPropType}
