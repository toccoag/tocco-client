import StaticCell from './StaticCell'
import {getStickyStyling} from './stickyUtil'
import {StyledTableRow} from './StyledComponents'

const TableRow =
  (data, columns, columnWidths, onRowClick, isSelected, isLastOpened, clickable) =>
  // eslint-disable-next-line react/prop-types
  ({index, style}) => {
    const rowData = data[index]
    return (
      <StyledTableRow
        key={`list-row-${rowData.__key}`}
        style={style}
        className={`selectableRow ${isSelected(rowData.__key) && 'selected'} ${
          isLastOpened(rowData.__key) && 'lastOpened'
        }`}
        onClick={onRowClick(rowData)}
        clickable={clickable}
        data-cy="tbl-list-row"
        even={index % 2 === 0}
      >
        {columns.map((column, columnIndex) => (
          <StaticCell
            key={`table-static-cell-${rowData.__key}-${column.id}`}
            selected={isSelected(rowData.__key)}
            rowData={rowData}
            column={column}
            rowIdx={index}
            style={getStickyStyling(column, columnIndex, false, columns, columnWidths)}
          />
        ))}
      </StyledTableRow>
    )
  }

export default TableRow
