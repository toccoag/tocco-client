import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'

import {Pagination} from '../'

import {StyledTableFooter, StyledReloadButton} from './StyledComponents'

const TableFooter = ({onPageChange, onPageRefresh, paginationInfo, intl, tableEl}) => {
  const showPagination = Boolean(paginationInfo)
  const showRefresh = typeof onPageRefresh === 'function'
  const showFooter = showPagination || showRefresh

  const msg = id => intl.formatMessage({id})

  if (!showFooter) {
    return null
  }

  return (
    <StyledTableFooter>
      {showPagination && (
        <Pagination
          onPageChange={onPageChange}
          currentPage={paginationInfo.currentPage}
          totalCount={paginationInfo.totalCount}
          recordsPerPage={paginationInfo.recordsPerPage}
          tableEl={tableEl}
        >
          {showRefresh && (
            <StyledReloadButton
              type="button"
              title={msg('client.component.table.refreshTitle')}
              icon="sync"
              onClick={onPageRefresh}
              data-cy="btn-tbl-refresh"
            />
          )}
        </Pagination>
      )}
    </StyledTableFooter>
  )
}

TableFooter.propTypes = {
  intl: PropTypes.object.isRequired,
  onPageRefresh: PropTypes.func,
  onPageChange: PropTypes.func,
  /**
   * Used to display pagination
   */
  paginationInfo: PropTypes.shape({
    totalCount: PropTypes.number,
    currentPage: PropTypes.number,
    recordsPerPage: PropTypes.number
  }),
  tableEl: PropTypes.shape({current: PropTypes.instanceOf(Element)})
}

export default injectIntl(TableFooter)
