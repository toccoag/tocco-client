import {useEffect, useMemo, useState} from 'react'

import {getNumberingCell} from './numberingColumnEnhancer'
import {getSelectionCell} from './selection/selectionColumnEnhancer'
import {modifyColumnsProp} from './stickyUtil'

const useEnhancedColumns = (
  initialColumns,
  scope,
  {showNumbering, selection, selectionStyle, isSelected, selectionChange, selectionFilterFn, hasSelectionHeader}
) => {
  const modifiedColumnsProp = useMemo(
    () => modifyColumnsProp(initialColumns).filter(({layoutScope}) => !layoutScope || layoutScope === scope),
    [initialColumns, scope]
  )
  const [columns, setColumns] = useState(modifiedColumnsProp)

  useEffect(() => {
    const numberingColumn = getNumberingCell(showNumbering)
    const selectionColumn = getSelectionCell(
      selectionStyle,
      modifiedColumnsProp,
      isSelected,
      selectionChange,
      selectionFilterFn,
      hasSelectionHeader
    )

    setColumns([
      ...(numberingColumn ? [numberingColumn] : []),
      ...(selectionColumn ? [selectionColumn] : []),
      ...modifiedColumnsProp
    ])
  }, [
    setColumns,
    modifiedColumnsProp,
    selection,
    isSelected,
    selectionChange,
    selectionFilterFn,
    selectionStyle,
    hasSelectionHeader,
    showNumbering
  ])

  return [columns, setColumns]
}

export default useEnhancedColumns
