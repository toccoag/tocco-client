import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../__mocks__/ResizeObserver'

import Table from './Table'

const defaultProps = {
  paginationInfo: {
    totalCount: 1,
    recordsPerPage: 10,
    currentPage: 1
  },
  columns: [],
  data: [],
  onRowClick: () => {}
}

describe('tocco-ui', () => {
  describe('Table', () => {
    beforeEach(() => {
      window.ResizeObserver = ResizeObserver
    })

    test('should render table', async () => {
      testingLibrary.renderWithIntl(<Table {...defaultProps} />)
      await screen.findAllByTestId('icon')

      expect(screen.queryByText('client.component.table.noData')).to.exist
      expect(screen.queryByText('client.component.pagination.text')).to.exist
    })

    test('should ignore tile layout scope', async () => {
      testingLibrary.renderWithIntl(
        <Table
          {...defaultProps}
          columns={[
            {id: 'tableColumn', layoutScope: 'table'},
            {id: 'tileColumn', layoutScope: 'tile'},
            {id: 'anyColumn'}
          ]}
          data={[{tableColumn: 'table data', tileColumn: 'tile data', anyColumn: 'any data'}]}
        />
      )
      await screen.findAllByTestId('icon')

      expect(screen.queryByText('table data')).to.exist
      expect(screen.queryByText('any data')).to.exist
      expect(screen.queryByText('tile data')).to.not.exist
    })
  })
})
