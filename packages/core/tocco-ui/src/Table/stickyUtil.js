// compute css left value for a sticky column, the value is the sum of the previous sticky column widths
const getLeft = (columnIndex, columns, columnWidths) =>
  columnWidths
    .slice(0, columnIndex)
    .filter((width, index) => columns[index].sticky === true)
    .reduce((a, b) => {
      return a + b
    }, 0)

export const getStickyStyling = (column, columnIndex, isHeader, columns, columnWidths) => {
  if (!column.sticky) {
    return {}
  }

  const stickyCount = columns.filter(col => col.sticky).length

  return {
    position: 'sticky',
    left: getLeft(columnIndex, columns, columnWidths),
    zIndex: isHeader ? 3 : 2,
    borderRight: columnIndex === stickyCount - 1 ? '2px solid lightgray' : '2px solid transparent'
  }
}

/*
 * adjust column definition for sticky columns:
 * - a sticky column should never be dragable
 * - explicit set dynamic that React.memo in StaticCell.js checks if a rerender is necessary
 * (e.g. column width is manual changed and the table body must be rerendered)
 */
export const modifyColumnsProp = columnsProp =>
  columnsProp.map(column => ({
    ...column,
    ...(column.sticky ? {dynamic: true, fixedPosition: true} : {})
  }))

// extract the column widths from the DOM as we use `minmax(100px, auto)` as width
export const extractColumnWidths = tableEl => {
  const ths = tableEl.current?.querySelectorAll('th')
  const items = ths ? [...ths] : []
  return items.map(item => item.getBoundingClientRect().width)
}
