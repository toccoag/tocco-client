import {getLabel} from './utils'

describe('tocco-ui', () => {
  describe('Table', () => {
    describe('utils', () => {
      describe('getLabel', () => {
        test('should return empty label for hidden label', () => {
          const label = getLabel({useLabel: 'hide', label: 'abc'})
          expect(label).to.eql('')
        })

        test('should return empty label for no label', () => {
          const label = getLabel({useLabel: 'no', label: 'abc'})
          expect(label).to.eql('')
        })

        test('should return label for yes label', () => {
          const label = getLabel({useLabel: 'no', label: 'abc'})
          expect(label).to.eql('')
        })

        test('should ignore case', () => {
          const label = getLabel({useLabel: 'No', label: 'abc'})
          expect(label).to.eql('')
        })
      })
    })
  })
})
