import {FormattedMessage} from 'react-intl'

import {LoadingSpinner, Typography} from '..'

import {StyledFullRow, StyledFullRowProgress, StyledTableRow} from './StyledComponents'

export const InProgressRow = () => (
  <StyledTableRow>
    <StyledFullRowProgress>
      <LoadingSpinner size="20" />
      <Typography.P>
        <FormattedMessage id="client.component.table.dataLoading" />
      </Typography.P>
    </StyledFullRowProgress>
  </StyledTableRow>
)

export const NoDataRow = () => (
  <StyledTableRow data-cy="tbl-list-no-data-row">
    <StyledFullRow>
      <Typography.Span>
        <FormattedMessage id="client.component.table.noData" />
      </Typography.Span>
    </StyledFullRow>
  </StyledTableRow>
)
