import PropTypes from 'prop-types'

import {StyledResizeHandle} from './StyledComponents'

const ResizingController = ({column, startResize}) => (
  <StyledResizeHandle data-cy={`tbl-header-cell-resizing-controller-${column.id}`} onMouseDown={startResize(column)} />
)

ResizingController.propTypes = {
  column: PropTypes.shape({
    resizable: PropTypes.bool,
    id: PropTypes.string
  }).isRequired,
  startResize: PropTypes.func.isRequired
}

export default ResizingController
