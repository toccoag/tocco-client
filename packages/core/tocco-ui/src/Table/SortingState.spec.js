import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SortingState from './SortingState'

describe('tocco-ui', () => {
  describe('Table', () => {
    describe('SortingState', () => {
      test('should render arrow up', async () => {
        testingLibrary.renderWithIntl(<SortingState column={{id: 'firstname', sorting: {sortRank: 1, order: 'asc'}}} />)
        expect(await screen.findByTestId('icon-sort-up')).to.exist
      })

      test('should render arrow down', async () => {
        testingLibrary.renderWithIntl(
          <SortingState column={{id: 'firstname', sorting: {sortRank: 1, order: 'desc'}}} />
        )
        expect(await screen.findByTestId('icon-sort-down')).to.exist
      })

      test('should render sup if second sorting is set', async () => {
        testingLibrary.renderWithIntl(
          <SortingState column={{id: 'firstname', sorting: {sortRank: 2, order: 'desc'}}} />
        )
        expect(await screen.findByTestId('icon-sort-down')).to.exist
        expect(screen.queryByText('2')).to.exist
      })

      test('should render sup if third sorting is set', async () => {
        testingLibrary.renderWithIntl(<SortingState column={{id: 'firstname', sorting: {sortRank: 3, order: 'asc'}}} />)
        expect(await screen.findByTestId('icon-sort-up')).to.exist
        expect(screen.queryByText('3')).to.exist
      })
    })
  })
})
