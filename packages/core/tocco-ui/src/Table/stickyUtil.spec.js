import {getStickyStyling, modifyColumnsProp} from './stickyUtil'

describe('tocco-ui', () => {
  describe('Table', () => {
    describe('stickyUtil', () => {
      describe('getStickyStyling', () => {
        const columns = [
          {idx: 0, sticky: true},
          {idx: 1, sticky: true},
          {idx: 2, sticky: false}
        ]
        const columnWidths = [50, 100]

        test('column is not sticky', () => {
          const columnIndex = 2
          const column = columns[columnIndex]
          const isHeader = false

          const expectedResult = {}
          expect(getStickyStyling(column, columnIndex, isHeader, columns, columnWidths)).to.eql(expectedResult)
        })

        test('column is sticky', () => {
          const columnIndex = 0
          const column = columns[columnIndex]
          const isHeader = false

          const expectedResult = {
            borderRight: '2px solid transparent',
            position: 'sticky',
            left: 0,
            zIndex: 2
          }
          expect(getStickyStyling(column, columnIndex, isHeader, columns, columnWidths)).to.eql(expectedResult)
        })

        test('column in header is sticky', () => {
          const columnIndex = 0
          const column = columns[columnIndex]
          const isHeader = true

          const expectedResult = {
            borderRight: '2px solid transparent',
            position: 'sticky',
            left: 0,
            zIndex: 3
          }
          expect(getStickyStyling(column, columnIndex, isHeader, columns, columnWidths)).to.eql(expectedResult)
        })

        test('column is sticky and sum left value of previous sticky columns', () => {
          const columnIndex = 1
          const column = columns[columnIndex]
          const isHeader = false

          const expectedResult = {
            borderRight: '2px solid lightgray',
            position: 'sticky',
            left: 50,
            zIndex: 2
          }
          expect(getStickyStyling(column, columnIndex, isHeader, columns, columnWidths)).to.eql(expectedResult)
        })

        test('during "initialization" columnWidths is empty and logic should not fail', () => {
          const columnIndex = 1
          const column = columns[columnIndex]
          const isHeader = false

          const expectedResult = {
            borderRight: '2px solid lightgray',
            position: 'sticky',
            left: 0,
            zIndex: 2
          }
          expect(getStickyStyling(column, columnIndex, isHeader, columns, [])).to.eql(expectedResult)
        })
      })

      describe('modifyColumnsProp', () => {
        test('is sticky', () => {
          const props = [{sticky: true, otherProp: 'test'}]
          const expectedResult = [{sticky: true, dynamic: true, fixedPosition: true, otherProp: 'test'}]
          expect(modifyColumnsProp(props)).to.eql(expectedResult)
        })

        test('is not sticky', () => {
          const props = [{sticky: false, otherProp: 'test'}]
          expect(modifyColumnsProp(props)).to.eql(props)
        })
      })
    })
  })
})
