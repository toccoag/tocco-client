import styled from 'styled-components'

import Icon from '../Icon'
import {themeSelector} from '../utilStyles'

export const StyledIcon = styled(Icon)`
  color: ${themeSelector.color('loadingSpinner')};
`
