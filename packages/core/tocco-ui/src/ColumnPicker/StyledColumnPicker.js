import styled from 'styled-components'
import {dragAndDrop} from 'tocco-util'

import {StyledLi, StyledLabel} from '../Typography'
import {scale, themeSelector} from '../utilStyles'

export const StyledColumnPickerWrapper = styled.div`
  padding-right: 3px; /* make sure drop indicator does not overlap outer elements on the right most side */
`

export const StyledControlsWrapper = styled.div`
  margin-left: 3px; /* rest of checkboxes are moved 3px to the right to make room for drop indicator */
  margin-top: ${scale.space(0.7)};
`

export const StyledUl = styled.ul`
  list-style-type: none;
  margin-top: ${scale.space(-0.5)} !important; /* Nice2 Reset */
  margin-left: 3px; /* make room for drop indicator when it appears on the left-most side */
  padding-left: 0 !important; /* Nice2 Reset */
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));

  ${StyledLabel} {
    padding: 0 !important; /* Nice2 Reset */
  }
`

export const StyledCheckbox = styled.input`
  vertical-align: top;
  margin-right: ${scale.space(-1)} !important; /* Nice2 Reset */

  &:hover {
    cursor: pointer;
  }
`

export const StyledId = styled.span`
  color: ${themeSelector.color('text')};
  font-style: italic;
`

export const StyledItem = styled(StyledLi)`
  display: flex;
  padding: ${scale.space(-2)} ${scale.space(-2)} ${scale.space(-2)} 0;
  box-sizing: border-box;

  /* when border appears, the content moves over by the width of the border,
  so we move the content back the same width */
  margin-left: ${({dropPosition}) => (dropPosition === dragAndDrop.HorizontalDropPosition.Left ? `-3px` : '0')};
  border-left: ${({dropPosition, theme}) =>
    dropPosition === dragAndDrop.HorizontalDropPosition.Left ? `3px solid ${theme.colors.secondary}` : 'none'};
  border-right: ${({dropPosition, theme}) =>
    dropPosition === dragAndDrop.HorizontalDropPosition.Right ? `3px solid ${theme.colors.secondary}` : 'none'};
  ${({draggable, theme}) =>
    draggable &&
    `
    &:hover {
      background-color: ${theme.colors.backgroundItemHover};

      &,
      label {
        cursor: pointer;
      }
    }
  `}
`

export const StyledNumber = styled.span`
  display: inline-block;
  margin-right: ${scale.space(-1)};
  color: ${themeSelector.color('text')};
  font-size: ${scale.font(0)};
`
