import PropTypes from 'prop-types'
import {html} from 'tocco-util'

import Typography from '../Typography'

import {StyledCheckbox, StyledId, StyledItem, StyledNumber} from './StyledColumnPicker'

const ColumnListItem = ({index, dndProps, column, onSelectionChange}) => (
  <StyledItem {...dndProps} css={dndProps.dndCss}>
    <StyledCheckbox
      type="checkbox"
      id={column.id}
      checked={!column.hidden}
      onChange={event => onSelectionChange(column.id, event.target.checked)}
    />
    <Typography.Label htmlFor={column.id}>
      <StyledNumber>{index}.</StyledNumber>
      {column.label ? (
        <span dangerouslySetInnerHTML={{__html: html.sanitizeHtml(column.label)}} />
      ) : (
        <StyledId>{column.id}</StyledId>
      )}
    </Typography.Label>
  </StyledItem>
)

ColumnListItem.propTypes = {
  index: PropTypes.number,
  column: PropTypes.shape({
    id: PropTypes.string,
    label: PropTypes.string,
    hidden: PropTypes.bool
  }),
  dndProps: PropTypes.object,
  onSelectionChange: PropTypes.func
}

export default ColumnListItem
