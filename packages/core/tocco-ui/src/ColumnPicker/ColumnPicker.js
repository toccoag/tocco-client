import PropTypes from 'prop-types'
import {useState, useMemo, useCallback} from 'react'
import {dragAndDrop} from 'tocco-util'

import SearchBox from '../SearchBox'
import Typography from '../Typography'

import ColumnListItem from './ColumnListItem'
import {StyledColumnPickerWrapper, StyledControlsWrapper, StyledCheckbox, StyledUl} from './StyledColumnPicker'

const sortColumnsByVisibility = columns => [...columns].sort((a, b) => a.hidden - b.hidden)
const getFilteredColumns = (columns, searchTerm) =>
  columns.filter(column => searchTerm === null || column.label.match(new RegExp(searchTerm, 'i')))

const ColumnPicker = ({
  dndEnabled,
  columns: unsortedColumns = [],
  sortColumns: customSortColumns,
  onColumnsChange,
  intl
}) => {
  const sortColumns = cols => (customSortColumns ? customSortColumns(cols) : sortColumnsByVisibility(cols))
  const columns = sortColumns(unsortedColumns)

  const insertDraggedItem = (dragging, target, horizontalPosition) => {
    return (acc, current) => {
      if (current.id === target) {
        if (horizontalPosition === dragAndDrop.HorizontalDropPosition.Left) {
          return [...acc, dragging, current]
        } else if (horizontalPosition === dragAndDrop.HorizontalDropPosition.Right) {
          return [...acc, current, dragging]
        }
      }
      return [...acc, current]
    }
  }

  const [searchTerm, setSearchTerm] = useState(null)
  const changeColumnPosition = useCallback(
    (currentlyDragging, currentlyDragOver, dropPosition) => {
      if (currentlyDragging !== currentlyDragOver) {
        const currentlyDraggingItem = columns.find(c => c.id === currentlyDragging)
        const updateColumns = prevColumns =>
          prevColumns
            .filter(c => c !== currentlyDraggingItem)
            .reduce(insertDraggedItem(currentlyDraggingItem, currentlyDragOver, dropPosition), [])

        onColumnsChange(updateColumns)
      }
    },
    [columns, onColumnsChange]
  )
  const {dndEvents, dndState, dndCss} = dragAndDrop.useDnD(changeColumnPosition)

  const filteredColumns = useMemo(() => getFilteredColumns(columns, searchTerm), [columns, searchTerm])
  const someUnchecked = filteredColumns.some(column => column.hidden)

  const handleColumnSelectionChange = (columnId, checked) => {
    const updateColumns = prevColumns =>
      sortColumns(
        prevColumns.map(c =>
          c.id === columnId
            ? {
                ...c,
                hidden: !checked
              }
            : c
        )
      )

    onColumnsChange(updateColumns)
  }

  const toggleAllCheckBoxes = e => {
    const isChecked = e.target.checked

    const updateColumns = prevColumns => {
      const prevFilteredColumns = getFilteredColumns(prevColumns, searchTerm)

      return sortColumns([
        ...prevColumns.map(column => ({
          ...column,
          hidden: prevFilteredColumns.includes(column) ? !isChecked : column.hidden
        }))
      ])
    }

    onColumnsChange(updateColumns)
  }

  const isColumnDraggedOver = column =>
    dndState.currentlyDragOver === column.id && dndState.currentlyDragging !== column.id

  return (
    <StyledColumnPickerWrapper>
      <SearchBox
        placeholder={intl.formatMessage({id: 'client.component.list.preferences.columns.search'})}
        onSearch={setSearchTerm}
      />
      <StyledControlsWrapper>
        <StyledCheckbox type={'checkbox'} onChange={toggleAllCheckBoxes} checked={!someUnchecked} />
        <Typography.Span>
          {intl.formatMessage({id: 'client.component.list.preferences.columns.selectDeselectAllCheckbox'})}
        </Typography.Span>
      </StyledControlsWrapper>
      <StyledUl>
        {filteredColumns.map((column, idx) => (
          <ColumnListItem
            key={column.id}
            index={idx + 1}
            column={column}
            dndProps={{
              draggable: dndEnabled,
              dndCss,
              dropPosition: isColumnDraggedOver(column) && dndState.horizontalDropPosition,
              ...(dndEnabled && {...dndEvents(column.id)})
            }}
            onSelectionChange={handleColumnSelectionChange}
          />
        ))}
      </StyledUl>
    </StyledColumnPickerWrapper>
  )
}

ColumnPicker.propTypes = {
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      label: PropTypes.string,
      hidden: PropTypes.bool.isRequired
    })
  ).isRequired,
  onColumnsChange: PropTypes.func.isRequired,
  dndEnabled: PropTypes.bool.isRequired,
  sortColumns: PropTypes.func,
  intl: PropTypes.object.isRequired
}

export default ColumnPicker
