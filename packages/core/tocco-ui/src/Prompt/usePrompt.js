import {useEffect} from 'react'
import {useBlocker} from 'react-router-dom'
import {saga} from 'tocco-util'

const usePrompt = ({when, message, showPendingChangesModal}) => {
  useEffect(() => {
    if (when) {
      saga.activatePreventFromLeaving()
    }

    return () => {
      saga.deactivatePreventFromLeaving()
    }
  }, [when])

  const blocker = useBlocker(
    ({currentLocation, nextLocation}) => when && currentLocation.pathname !== nextLocation.pathname
  )

  useEffect(() => {
    if (blocker.state === 'blocked') {
      showPendingChangesModal(message, confirm => (confirm ? blocker.proceed() : blocker.reset()))
    }
  }, [blocker, showPendingChangesModal, message])
}

export default usePrompt
