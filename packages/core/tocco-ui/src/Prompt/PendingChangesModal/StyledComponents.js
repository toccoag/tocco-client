import styled from 'styled-components'

import {scale, themeSelector} from '../../utilStyles'

export const StyledButtonWrapper = styled.div`
  padding-top: ${scale.space(0)};
  background-color: ${themeSelector.color('paper')};
  display: flex;
  justify-content: flex-end;
`
