import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'

import Button from '../../Button'

import {StyledButtonWrapper} from './StyledComponents'

const PendingChangesModal = ({onYes, onNo}) => (
  <StyledButtonWrapper>
    <Button ink="primary" look="raised" type="button" onClick={onYes} autoFocus>
      <FormattedMessage id="client.common.yes" />
    </Button>
    <Button type="button" onClick={onNo}>
      <FormattedMessage id="client.common.no" />
    </Button>
  </StyledButtonWrapper>
)

PendingChangesModal.propTypes = {
  onYes: PropTypes.func.isRequired,
  onNo: PropTypes.func.isRequired
}

export default PendingChangesModal
