import {CKEditor} from 'ckeditor4-react'
import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {useIntl} from 'react-intl'
import {consoleLogger} from 'tocco-util'

import fontSizes from './fontSizes'
import {StyledHtmlEditor} from './StyledHtmlEditor'

/**
 * Workaround: co-existence with ckeditor from legacy actions
 *  - rename ckeditor.js from NA to ckeditor-new.js
 *  - define basepath explicitly => otherwise ext-extension path from legacy ckeditor is used automatically
 */
window.NEW_CKEDITOR_BASEPATH = `${__webpack_public_path__}ckeditor4/`

const getCKEditorLanguageFromLocale = locale => {
  // new langs have to be bundled into the build => build/lib/webpack.js#getCKEditorPlugin
  switch (locale) {
    case 'fr-CH':
      return 'fr'
    case 'it-CH':
      return 'it'
    case 'en-US':
      return 'en'
    case 'fr':
    case 'en':
    case 'it':
    case 'de':
      return locale
    case 'de-CH':
      return 'de-ch'
    case 'de-DE':
    default:
      return 'de'
  }
}

/*
CKEditor4 Implementation:
The CKEditor4 is loaded asynchronously via `editorUrl`. Per default the editor is loaded from their CDN.
In order to host the editor by ourself we copy the sources to our output bundle
(see: build/lib/webpack.js#getCKEditorPlugin) from the node_modules/ckeditor4. The
ckeditor4 package is added as devDependency in the package.json.
It is not imported from the code, only copied during build process.
Additional community plugins (e.g. codemirror) has to be bundled as well. Mainly the community plugins cannot be
added by npm. Therefore the have to be downloaded and added to the source code manually.

How to add a new community plugin:
 1. Download plugin and add to ./extraPlugins/<plugin>
 2. Extend the webpack build to copy the new plugin to the output build
    => build/lib/webpack.js#getCKEditorPlugin
 3. Add the plugin in the config to the `extraPlugins` list
 4. Optional: extend toolbar config to show plugin buttons

How to remove specific toolbar buttons from the default config:
```js
ckEditorConfig = {
  removeButtons: ['TextColor']
}
```
*/
const HtmlEditor = ({id, value = '', onChange, contentLang = 'de', defaultLinkTarget, ckEditorConfig}) => {
  const intl = useIntl()
  const {locale} = intl

  /**
   * Workaround for updating ckeditor content via auto complete
   * - Force rerender of ckeditor else the old value is shown. the ckeditor has only an option to set the initial value
   *   with the property `initData`. Using the offical `setData` on the ckeditor instance is not trivial in our setup.
   * - Only check if ckeditor should be reinitialized if the `value` property is changed. else there are two rerenders.
   *   first `ckeditorKey` is modified in `handleChange` and the first rerender would be done.
   *   `onChange` updates the redux state which is propagated to `value` property and the second rerender would be done.
   *   During manually editing the field no rerender is done as `currentInternalValue` and `value` are in sync.
   * - an error occurs if during the ckeditor initialization the key is changed. if there is a default email template,
   *   the auto complete is done before the ckeditor is ready. to avoid the issue only update the key if the ckeditor
   *   was loaded. after `onLoaded` check if the values was changed and if necessary a rerender should be triggered.
   */
  const [currentInternalValue, setCurrentInternalValue] = useState(value)
  const [isLoaded, setIsLoaded] = useState(false)
  const [ckeditorKey, setCkeditorKey] = useState(Math.random())
  useEffect(() => {
    if (isLoaded && currentInternalValue !== value) {
      setCkeditorKey(Math.random())
    }
  }, [value, isLoaded]) // eslint-disable-line react-hooks/exhaustive-deps

  const handleChange = event => {
    const data = event.editor.getData()
    setCurrentInternalValue(data)
    onChange(data)
  }

  /**
   * Workaround: Saving in source code editing
   * - onChange event is not fired while being in source code editing mode
   * - use onKey instead and get value from editable element
   */
  const handleKey = event => {
    if (event.editor.mode === 'source') {
      // wait for key event has been executed
      setTimeout(() => {
        const editable = event.editor.editable()
        const data = editable.getValue()
        onChange(data)
      }, 0)
    }
  }

  const config = {
    language: getCKEditorLanguageFromLocale(locale),
    contentsLanguage: contentLang,
    // freemarker should not be escaped => [@loadValue path=&quot;relEvent.abbreviation&quot; /]
    entities: false,
    // deactivate acf and allow all content (e.g. inline styles, svgs, divs, ...)
    // https://ckeditor.com/docs/ckeditor4/latest/guide/dev_advanced_content_filter.html
    // https://ckeditor.com/docs/ckeditor4/latest/guide/dev_acf.html
    allowedContent: true,
    toolbar: [
      ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat'],
      ['Format', 'Font', 'FontSize', 'TextColor'],
      ['NumberedList', 'BulletedList'],
      ['Link', 'Unlink'],
      ['Table', 'SpecialChar', 'Image'],
      ['PasteText'],
      ['Source']
    ],
    // extra plugins have to be bundled into the build => build/lib/webpack.js#getCKEditorPlugin
    extraPlugins: ['colorbutton', 'colordialog', 'dialogadvtab', 'font', 'codemirror'],
    removeButtons: [],
    removeDialogTabs: '',
    removePlugins: ['elementspath'],
    // https://github.com/w8tcha/CKEditor-CodeMirror-Plugin
    codemirror: {
      mode: 'htmlmixed',
      enableCodeFormatting: true,
      autoFormatOnStart: true
    },
    versionCheck: false,
    fontSize_sizes: fontSizes,
    ...ckEditorConfig
  }

  /**
   * The default values will be set for all ckeditor instances.
   *
   * e.g.
   * When setting the default link target to '_blank' for one editor the default will be set to `_blank`
   * for all further editors which will be loaded.
   *
   * Therefore always set the default explicitly.
   *
   * !! Attention:
   * For now, the editors on the same page have the same configuration.
   * Not sure how it behaves, when different editors on one page have
   * different settings and they will be initialised at the same time.
   */
  const handleDialogDefinition = event => {
    try {
      const dialogName = event.data.name
      const dialogDefinition = event.data.definition

      if (dialogName === 'link') {
        // set default protocol to https://
        const infoTab = dialogDefinition.getContents('info')
        const protocolField = infoTab.get('protocol')
        protocolField.default = 'https://'

        // set default link target (usually to _blank or nothing)
        const targetTab = dialogDefinition.getContents('target')
        const targetField = targetTab.get('linkTargetType')
        targetField.default = defaultLinkTarget || 'notSet'
      }
    } catch (error) {
      consoleLogger.logError(`Could not set default link attributes.`)
    }
  }

  return (
    <StyledHtmlEditor id={id}>
      <CKEditor
        key={ckeditorKey}
        config={config}
        initData={value}
        onBeforeLoad={ckeditorInstance => ckeditorInstance.on('dialogDefinition', handleDialogDefinition)}
        type="classic"
        onChange={handleChange}
        onKey={handleKey}
        editorUrl={`${__webpack_public_path__}ckeditor4/ckeditor-new.js`}
        onLoaded={() => setIsLoaded(true)}
      />
    </StyledHtmlEditor>
  )
}

HtmlEditor.propTypes = {
  id: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.string,
  contentLang: PropTypes.oneOf(['de', 'fr', 'it', 'en']),
  defaultLinkTarget: PropTypes.string,
  ckEditorConfig: PropTypes.object
}

export default HtmlEditor
