const fontSizes = {
  '8px': '~6pt',
  '9px': '~7pt',
  '10px': '~7.5pt',
  '11px': '~8pt',
  '12px': '~9pt',
  '13px': '~10pt',
  '14px': '~10.5pt',
  '15px': '~11pt',
  '16px': '~12pt',
  '17px': '~13pt',
  '18px': '~13.5pt',
  '19px': '~14pt',
  '20px': '~14.5pt',
  '22px': '~16pt',
  '24px': '~18pt',
  '26px': '~20pt',
  '28px': '~21pt',
  '29px': '~22pt',
  '32px': '~24pt',
  '36px': '~26pt',
  '37px': '~28pt',
  '48px': '~36pt',
  '64px': '~48pt',
  '72px': '~54pt'
}
const fontSizesString = Object.entries(fontSizes)
  .map(([size, label]) => `${size} (${label})/${size}`)
  .join(';')

export default fontSizesString
