export const populateInitialWindowState = () => {
  window.__INITIAL_STATE__ = {
    app: {
      intlInitialised: true,
      cacheInitialised: true
    }
  }
}

export const restoreInitialWindowState = () => {
  window.__INITIAL_STATE__ = undefined
}
