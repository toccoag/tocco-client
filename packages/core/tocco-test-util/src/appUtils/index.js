import {populateInitialWindowState, restoreInitialWindowState} from './appUtils'
export default {populateInitialWindowState, restoreInitialWindowState}
