import {screen} from '@testing-library/react'

export const getByTextIncludes = text =>
  screen.getByText((content, element) => content !== '' && element.textContent.includes(text))
