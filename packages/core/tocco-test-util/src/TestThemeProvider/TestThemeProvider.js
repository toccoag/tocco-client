import PropTypes from 'prop-types'
import {StyleSheetManager, ThemeProvider} from 'styled-components'
import {ToccoTheme, utils} from 'tocco-theme'

const TestThemeProvider = ({children}) => (
  <ThemeProvider theme={ToccoTheme.defaultTheme}>
    <StyleSheetManager shouldForwardProp={utils.shouldForwardProp}>{children}</StyleSheetManager>
  </ThemeProvider>
)

export default TestThemeProvider

TestThemeProvider.propTypes = {
  children: PropTypes.node
}
