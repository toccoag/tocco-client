import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {react, reducer as reducerUtil} from 'tocco-util'

import SchedulerContainer from './containers/SchedulerContainer'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'scheduler'

const EXTERNAL_EVENTS = [
  /**
   * This event is fired when the visible date range changes. e.g. switch from day to week view
   *
   * Payload:
   * - `dateRange` containing new startDate and endDate
   */
  'onDateRangeChange',
  /**
   * Gets fired when a user manually removed a calendar from the view by pressing "x" button
   *
   * Payload:
   * - `id`: id of source entity
   * - `calendarType`: type of calendar
   */
  'onCalendarRemove',
  'onCalendarRemoveAll',
  /**
   * Fired when an event in the calendar gets clicked
   *
   * Payload:
   * - `id`: id of source entity
   * - `model`: name of the entity
   */
  'onEventClick',
  /**
   * Fired when refresh button gets clicked
   */
  'onRefresh'
]

const initApp = (id, input, events, publicPath) => {
  const content = <SchedulerContainer />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === 'scheduler') {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

const SchedulerApp = props => {
  const {component, store} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})

  react.useDidUpdate(() => {
    getDispatchActions(props).forEach(action => {
      if (component != null) {
        store.dispatch(action)
      }
    })
  }, [props])

  return component
}

SchedulerApp.propTypes = {
  calendars: PropTypes.arrayOf(
    PropTypes.shape({
      calendarType: PropTypes.string.isRequired,
      events: PropTypes.arrayOf(
        PropTypes.shape({
          description: PropTypes.string,
          start: PropTypes.number,
          end: PropTypes.number,
          allDay: PropTypes.bool
        })
      ),
      key: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      model: PropTypes.string.isRequired
    })
  ),
  schedulerRef: PropTypes.object,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => ({...propTypes, [event]: PropTypes.func}), {}),
  /**
   * ISO Language Code
   */
  locale: PropTypes.string
}

export default SchedulerApp
