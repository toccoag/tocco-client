import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import Conflict from './Conflict'

describe('scheduler', () => {
  describe('components', () => {
    describe('Conflict', () => {
      test('should return null if conflictstatus is undefined', () => {
        const {container} = testingLibrary.renderWithIntl(<Conflict intl={IntlStub} />)
        jestExpect(container).toBeEmpty()
      })

      test('should return null if conflictstatus is none', () => {
        const {container} = testingLibrary.renderWithIntl(<Conflict intl={IntlStub} conflictStatus="none" />)
        jestExpect(container).toBeEmpty()
      })

      test('should render a check icon if conflict is accepted', () => {
        testingLibrary.renderWithIntl(<Conflict intl={IntlStub} conflictStatus="accepted" />)
        expect(screen.queryByText('✓ client.scheduler.conflictAccepted')).to.exist
      })

      test('should render a times icon if conflict is existing', () => {
        testingLibrary.renderWithIntl(<Conflict intl={IntlStub} conflictStatus="existing" />)
        expect(screen.queryByText('✕ client.scheduler.conflictExisting')).to.exist
      })
    })
  })
})
