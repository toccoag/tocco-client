import {screen, waitFor} from '@testing-library/react'
import {addHours, setHours} from 'date-fns'
import {testingLibrary} from 'tocco-test-util'

import FullCalendar from './FullCalendar'

describe('scheduler', () => {
  describe('components', () => {
    describe('Fullcalendar', () => {
      beforeEach(() => {
        jest.useFakeTimers()
      })

      afterEach(() => {
        jest.useRealTimers()
      })

      const baseProps = {
        onRefresh: () => {},
        events: [],
        resources: [],
        onEventClick: () => {},
        onCalendarRemove: () => {}
      }

      test('should render calendar', () => {
        const {container} = testingLibrary.renderWithIntl(<FullCalendar {...baseProps} />)

        const fullcalendarElements = container.querySelectorAll('.fc')
        expect(fullcalendarElements.length).to.eql(1)
      })

      const mockResources = [{title: 'Dummy_entity 0', id: '0Dummy_entity', entityKey: '0', calendarType: 'dummy'}]

      test('should show resources', async () => {
        testingLibrary.renderWithIntl(<FullCalendar {...baseProps} resources={mockResources} />)
        await waitFor(() => {
          expect(screen.queryByText(mockResources[0].title)).to.exist
        })
      })

      test('should show events', async () => {
        const start = setHours(new Date(), 7)
        const end = addHours(new Date(start), 2)
        const mockEvents = [
          {
            resourceId: '0Dummy_entity',
            title: 'Lecture IT 2',
            start: start.getTime(),
            end: end.getTime()
          }
        ]

        const {rerender} = testingLibrary.renderWithIntl(
          <FullCalendar locale="de" {...baseProps} resources={mockResources} />
        )
        rerender(<FullCalendar locale="de" {...baseProps} resources={mockResources} events={mockEvents} />)
        await waitFor(() => {
          expect(screen.queryByText(mockEvents[0].title)).to.exist
        })
      })
    })
  })
})
