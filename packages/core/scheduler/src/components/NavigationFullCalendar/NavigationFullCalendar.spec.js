import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import NavigationFullCalendar, {getButtonInkProps} from './NavigationFullCalendar'

describe('scheduler', () => {
  describe('components', () => {
    describe('FullCalendar', () => {
      describe('NavigationFullCalendar', () => {
        test('should get button ink prop', () => {
          expect(getButtonInkProps('a', 'a')).to.deep.equal({ink: 'secondary'})
          expect(getButtonInkProps('a', 'b')).to.deep.equal({})
        })

        test('should render title and buttons', async () => {
          const changeRange = sinon.spy()
          const changeView = sinon.spy()
          const chooseNext = sinon.spy()
          const choosePrev = sinon.spy()
          const chooseToday = sinon.spy()
          const goToDate = sinon.spy()
          const refresh = sinon.spy()
          const mockProps = {
            changeRange,
            changeView,
            chooseNext,
            choosePrev,
            chooseToday,
            goToDate,
            isLoading: false,
            refresh,
            title: 'title text',
            type: 'timelineWeek'
          }

          testingLibrary.renderWithIntl(<NavigationFullCalendar {...mockProps} />)
          const user = userEvent.setup()
          await screen.findByTestId('icon-chevron-left')

          await user.click(screen.getByText('client.scheduler.today'))
          expect(chooseToday).to.have.been.calledOnce

          await user.click(screen.getByTestId('icon-chevron-left'))
          expect(choosePrev).to.have.been.calledOnce

          await user.click(screen.getByTestId('icon-chevron-right'))
          expect(chooseNext).to.have.been.calledOnce

          await user.click(screen.getByTestId('icon-sync'))
          expect(refresh).to.have.been.calledOnce

          await user.click(screen.getByText('client.scheduler.day'))
          expect(changeView).to.have.been.calledWith('dayView')

          await user.click(screen.getByText('client.scheduler.week'))
          expect(changeView).to.have.been.calledWith('weekViewSimple')

          await user.click(screen.getByText('client.scheduler.month'))
          expect(changeView).to.have.been.calledWith('monthView')
        })

        test('should toggle between weekView and weekViewSimple', async () => {
          const changeRange = sinon.spy()
          const changeView = sinon.spy()
          const chooseNext = sinon.spy()
          const choosePrev = sinon.spy()
          const chooseToday = sinon.spy()
          const goToDate = sinon.spy()
          const refresh = sinon.spy()
          const mockProps = {
            changeRange,
            changeView,
            chooseNext,
            choosePrev,
            chooseToday,
            goToDate,
            isLoading: false,
            refresh,
            title: 'title text',
            type: 'weekViewSimple'
          }

          const {rerender} = testingLibrary.renderWithIntl(<NavigationFullCalendar {...mockProps} />)
          const user = userEvent.setup()

          await user.click(screen.getByText('client.scheduler.week'))
          expect(changeView).to.have.been.calledWith('weekView')

          rerender(<NavigationFullCalendar {...mockProps} type="weekView" />)

          await user.click(screen.getByText('client.scheduler.week'))
          expect(changeView).to.have.been.calledWith('weekViewSimple')
        })
      })
    })
  })
})
