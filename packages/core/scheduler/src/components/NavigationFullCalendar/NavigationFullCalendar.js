import {getISOWeek} from 'date-fns'
import PropTypes from 'prop-types'
import {useCallback} from 'react'
import {FormattedMessage, injectIntl} from 'react-intl'
import {Button, ButtonGroup, Icon, Typography, DatePicker} from 'tocco-ui'

import {
  StyledNavigationFullCalendar,
  StyledButtonGroupReloadWrapper,
  StyledReloadButtonWrapper
} from './StyledComponents'

export const getButtonInkProps = (viewType, type) => (viewType === type ? {ink: 'secondary'} : {})

const msg = (id, intl) => intl.formatMessage({id})

const NavigationFullCalendar = ({
  date,
  changeRange,
  changeView,
  chooseNext,
  choosePrev,
  chooseToday,
  goToDate,
  intl,
  isLoading,
  refresh,
  title,
  type
}) => {
  const handleAction = useCallback(
    action => {
      action()
      changeRange()
    },
    [changeRange]
  )

  const weekNumber = type !== 'monthView' ? `(${msg('client.scheduler.cw', intl)}: ${getISOWeek(date)})` : ''

  return (
    <StyledNavigationFullCalendar data-cy="calendar-navigation">
      <div>
        <Button look="raised" onClick={() => handleAction(chooseToday)} data-cy="btn-calendar-today">
          <FormattedMessage id="client.scheduler.today" />
        </Button>
        <ButtonGroup>
          <Button
            look="raised"
            onClick={() => handleAction(choosePrev)}
            icon="chevron-left"
            title={msg('client.scheduler.previous', intl)}
            data-cy="btn-calendar-prev"
          />
          <Button
            look="raised"
            onClick={() => handleAction(chooseNext)}
            icon="chevron-right"
            title={msg('client.scheduler.next', intl)}
            data-cy="btn-calendar-next"
          />
        </ButtonGroup>
        <DatePicker value={date} onChange={changedDate => handleAction(() => goToDate(changedDate))}>
          <Typography.Span>
            {title} {weekNumber}
          </Typography.Span>
          <Icon style={{marginLeft: '5px'}} icon="chevron-down" />
        </DatePicker>
      </div>
      <StyledButtonGroupReloadWrapper>
        <ButtonGroup>
          <Button
            look="raised"
            {...getButtonInkProps('dayView', type)}
            onClick={() => changeView('dayView')}
            data-cy="btn-calendar-day"
          >
            <FormattedMessage id="client.scheduler.day" />
          </Button>
          <Button
            look="raised"
            {...getButtonInkProps('weekView', type)}
            {...getButtonInkProps('weekViewSimple', type)}
            onClick={() => (type !== 'weekViewSimple' ? changeView('weekViewSimple') : changeView('weekView'))}
            data-cy="btn-calendar-week"
          >
            <FormattedMessage id="client.scheduler.week" />
          </Button>
          <Button
            look="raised"
            {...getButtonInkProps('monthView', type)}
            onClick={() => changeView('monthView')}
            data-cy="btn-calendar-month"
          >
            <FormattedMessage id="client.scheduler.month" />
          </Button>
        </ButtonGroup>
        <StyledReloadButtonWrapper>
          <Button
            icon={isLoading ? '' : 'sync'}
            onClick={() => !isLoading && refresh()}
            pending={isLoading}
            title={msg('client.scheduler.reload', intl)}
            data-cy="btn-calendar-reload"
          />
        </StyledReloadButtonWrapper>
      </StyledButtonGroupReloadWrapper>
    </StyledNavigationFullCalendar>
  )
}

NavigationFullCalendar.propTypes = {
  date: PropTypes.instanceOf(Date),
  changeRange: PropTypes.func.isRequired,
  changeView: PropTypes.func.isRequired,
  chooseNext: PropTypes.func.isRequired,
  choosePrev: PropTypes.func.isRequired,
  chooseToday: PropTypes.func.isRequired,
  goToDate: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  refresh: PropTypes.func.isRequired,
  title: PropTypes.string,
  type: PropTypes.string
}

export default injectIntl(NavigationFullCalendar)
