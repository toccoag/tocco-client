import {testingLibrary} from 'tocco-test-util'

import Scheduler from './Scheduler'

describe('scheduler', () => {
  describe('components', () => {
    describe('Scheduler', () => {
      test('should render FullCalendar', () => {
        const {container} = testingLibrary.renderWithIntl(<Scheduler calendars={[]} />)

        const fullcalendarElements = container.querySelectorAll('.fc')
        expect(fullcalendarElements.length).to.eql(1)
      })
    })
  })
})
