import {screen} from '@testing-library/react'
import {appFactory} from 'tocco-app-extensions'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'
import entityListReducers, {sagas} from '../../modules/reducers'

import ListView from './ListView'

const EMPTY_FUNC = () => {}

const table = {
  componentType: 'table',
  layoutType: 'table',
  children: []
}

const mainActionBar = {
  id: 'main-action-bar',
  componentType: 'action-bar',
  children: []
}

const formDefinition = children => ({
  children
})

const props = {
  initialize: EMPTY_FUNC,
  formDefinition: formDefinition([table, mainActionBar]),
  intl: IntlStub,
  orderBy: null,
  selectable: true,
  onSelectChange: EMPTY_FUNC,
  selection: [],
  refresh: EMPTY_FUNC,
  currentPageIds: ['1', '4'],
  showSelectionController: true,
  columnDisplayPreferences: {},
  preferencesLoaded: true
}

const reducers = {...entityListReducers, actions: () => ({})}
const store = appFactory.createStore(reducers, sagas)

/* eslint-disable react/prop-types */
jest.mock('../../../../app-extensions/src/errorLogging/components/ErrorBoundaryContainer', () => ({children}) => (
  <div data-testid="error-boundary">{children}</div>
))
jest.mock('../../containers/TableContainer', () => () => <div data-testid="table"></div>)
/* eslint-enable react/prop-types */

describe('entity-list', () => {
  describe('components', () => {
    describe('ListView', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      test('should render ', () => {
        testingLibrary.renderWithStore(<ListView {...props} />, {store})

        expect(screen.queryByText('client.entity-list.selectionAll')).to.exist
        expect(screen.queryByTestId('table')).to.exist
      })

      test('should not render action bar if not in model', () => {
        testingLibrary.renderWithStore(<ListView {...props} formDefinition={formDefinition([table])} />, {store})

        expect(screen.queryByText('client.entity-list.selectionAll')).to.not.exist
        expect(screen.queryByTestId('table')).to.exist
      })
    })
  })
})
