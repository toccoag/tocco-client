import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, ColumnPicker, useOnEnterHandler, StyledStickyButtonWrapper} from 'tocco-ui'

const ColumnModal = ({onOk, initialColumns, dndEnabled, sortColumns}) => {
  const [columns, setColumns] = useState(initialColumns)

  const handleOk = () => onOk(columns)
  useOnEnterHandler(handleOk)

  return (
    <>
      <ColumnPicker columns={columns} onColumnsChange={setColumns} sortColumns={sortColumns} dndEnabled={dndEnabled} />
      <StyledStickyButtonWrapper>
        <Button onClick={handleOk} ink="primary" look={'raised'} data-cy="btn-ok">
          <FormattedMessage id="client.component.list.preferences.columns.okButton" />
        </Button>
      </StyledStickyButtonWrapper>
    </>
  )
}

ColumnModal.propTypes = {
  onOk: PropTypes.func.isRequired,
  initialColumns: PropTypes.array,
  dndEnabled: PropTypes.bool,
  sortColumns: PropTypes.func
}

export default ColumnModal
