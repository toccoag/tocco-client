import PropTypes from 'prop-types'
import {useEffect, useRef, useState} from 'react'
import {createRoot} from 'react-dom/client'
import {FormattedMessage} from 'react-intl'
import {Ball, BallMenu, MenuItem, Popover, SidepanelHeader} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import BasicSearchFormContainer from '../../containers/BasicSearchFormContainer'
import SearchFilterList from '../SearchFilterList'

import {Box, StyledGutter, StyledSplit, StyledSplitWrapper} from './StyedComponents'

const SEARCH_FILTER_BUTTON_HEIGHT = 28
const SEARCH_FILTER_PADDING = 10
const MAX_HEIGHT_THRESHOLD = 30
const MAX_SIZE_SEARCH_FILTER = 25

const getGutter = () => () => {
  const gutterEl = document.createElement('div')
  const root = createRoot(gutterEl)
  root.render(<StyledGutter tabIndex={0} />)
  return gutterEl
}

const SearchView = ({
  resetSearch,
  msg,
  searchFilters,
  saveSearchFilter,
  saveDefaultSearchFilter,
  resetDefaultSearchFilter,
  displaySearchFieldsModal,
  resetSearchFields,
  searchFormDirty,
  initialized,
  setQueryViewVisible,
  loadSearchAsQuery,
  theme
}) => {
  const splitWrapperEl = useRef(null)
  const searchFormEl = useRef(null)
  const [size, setSize] = useState([MAX_SIZE_SEARCH_FILTER, 100 - MAX_SIZE_SEARCH_FILTER])
  const [searchFilterExpanded, setSearchFilterExpanded] = useState(false)
  const [showExpandSearchFilter, setShowExpandSearchFilter] = useState(false)

  customHooks.useAutofocus(searchFormEl, {selectFulltextFields: true}, [initialized])

  useEffect(() => {
    const searchFilterHeight = searchFilters
      ? searchFilters.length * SEARCH_FILTER_BUTTON_HEIGHT + SEARCH_FILTER_PADDING
      : SEARCH_FILTER_BUTTON_HEIGHT
    const splitWrapperHeight = splitWrapperEl.current.clientHeight
    const searchFilterHeightPercentage = Math.ceil(100 / (splitWrapperHeight / searchFilterHeight))

    if (!searchFilterExpanded && searchFilterHeightPercentage > MAX_HEIGHT_THRESHOLD) {
      setShowExpandSearchFilter(true)
      setSize([MAX_SIZE_SEARCH_FILTER, 100 - MAX_SIZE_SEARCH_FILTER])
    } else {
      setSize([searchFilterHeightPercentage, 100 - searchFilterHeightPercentage])
    }
  }, [searchFilters, searchFilterExpanded])

  const isSingleSearchFilterActive = searchFilters?.filter(s => s.active).length === 1

  const openAsQuery = () => {
    loadSearchAsQuery()
  }
  const enableQueryView = () => setQueryViewVisible(true)
  const toggleSearchFilterExpansion = () => setSearchFilterExpanded(!searchFilterExpanded)
  const expandBallIcon = searchFilterExpanded ? 'chevron-up' : 'chevron-down'
  const expandBallTitle = searchFilterExpanded ? msg('client.entity-list.contract') : msg('client.entity-list.expand')

  return (
    <>
      <SidepanelHeader>
        <Ball
          data-cy="btn-query-view"
          icon="code"
          onClick={enableQueryView}
          title={msg('client.entity-list.query_view')}
        />
        {showExpandSearchFilter && (
          <Ball icon={expandBallIcon} onClick={toggleSearchFilterExpansion} title={expandBallTitle} />
        )}
        <Ball data-cy="btn-reset" icon="times" onClick={resetSearch} title={msg('client.component.list.reset')} />
        <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-search-menu'}}>
          <MenuItem
            disabled={!isSingleSearchFilterActive}
            onClick={saveDefaultSearchFilter}
            data-cy="menuitem-save-search-filter"
          >
            <Popover
              content={
                !isSingleSearchFilterActive && (
                  <FormattedMessage id="client.entity-list.search.settings.defaultFilter.save.info" />
                )
              }
            >
              <FormattedMessage id="client.entity-list.search.settings.defaultFilter.save" />
            </Popover>
          </MenuItem>
          <MenuItem onClick={resetDefaultSearchFilter} data-cy="menuitem-reser-search-filter">
            <FormattedMessage id="client.entity-list.search.settings.defaultFilter.reset" />
          </MenuItem>
          <MenuItem onClick={saveSearchFilter} disabled={!searchFormDirty} data-cy="menuitem-save-search-as-filter">
            <FormattedMessage id="client.entity-list.search.settings.saveAsFilter" />
          </MenuItem>
          <MenuItem onClick={displaySearchFieldsModal} data-cy="menuitem-adjust-search-form">
            <FormattedMessage id="client.component.list.search.settings.searchForm.edit" />
          </MenuItem>
          <MenuItem onClick={resetSearchFields} data-cy="menuitem-reset-search-form">
            <FormattedMessage id="client.component.list.search.settings.searchForm.reset" />
          </MenuItem>
          <MenuItem onClick={openAsQuery} data-cy="menuitem-open-search-as-tql-query">
            <FormattedMessage id="client.entity-list.query.search.open" />
          </MenuItem>
        </BallMenu>
      </SidepanelHeader>
      <StyledSplitWrapper ref={splitWrapperEl}>
        <StyledSplit direction="vertical" gutterSize={15} sizes={size} minSize={[90, 0]} gutter={getGutter()}>
          <Box>
            <SearchFilterList />
          </Box>
          <Box ref={searchFormEl}>
            <BasicSearchFormContainer />
          </Box>
        </StyledSplit>
      </StyledSplitWrapper>
    </>
  )
}

SearchView.propTypes = {
  initialized: PropTypes.bool.isRequired,
  msg: PropTypes.func.isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.object),
  resetSearch: PropTypes.func.isRequired,
  saveSearchFilter: PropTypes.func.isRequired,
  saveDefaultSearchFilter: PropTypes.func.isRequired,
  resetDefaultSearchFilter: PropTypes.func.isRequired,
  displaySearchFieldsModal: PropTypes.func.isRequired,
  resetSearchFields: PropTypes.func.isRequired,
  searchFormDirty: PropTypes.bool,
  setQueryViewVisible: PropTypes.func.isRequired,
  loadSearchAsQuery: PropTypes.func.isRequired,
  theme: PropTypes.object.isRequired
}

export default SearchView
