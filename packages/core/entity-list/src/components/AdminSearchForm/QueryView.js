import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Ball, BallMenu, EditableValue, FormattedValue, MenuItem, SidepanelHeader, StatedValue} from 'tocco-ui'

import {StyledErrorMessage, StyledQueryBox} from './StyedComponents'

const detectSignal = (hasError, touched) => {
  if (hasError) {
    return 'danger'
  } else if (touched) {
    return 'info'
  }
}

const QueryView = ({
  setQueryViewVisible,
  msg,
  entityModel,
  query,
  queryError,
  loadSearchAsQuery,
  saveQueryAsFilter,
  setQuery,
  runQuery,
  clearQuery
}) => {
  const codeEditorEvents = {
    onChange: setQuery
  }
  const codeEditorOptions = {
    mode: 'tql',
    implicitModel: entityModel,
    autoFocusOnMount: true
  }
  const doesQueryExist = Boolean(query?.trim().length > 0)
  const hasQueryErrors = queryError && Object.entries(queryError).length !== 0
  const isQueryFalsy = !doesQueryExist || hasQueryErrors
  const disableQueryView = () => setQueryViewVisible(false)

  const queryErrorValues = Object.values(queryError || {})

  const signal = detectSignal(hasQueryErrors, doesQueryExist)

  return (
    <>
      <SidepanelHeader>
        <Ball
          data-cy="btn-search-view"
          icon="filter"
          onClick={disableQueryView}
          title={msg('client.entity-list.search_view')}
        />
        <Ball
          data-cy="btn-search"
          icon="search"
          onClick={runQuery}
          title={msg('client.entity-list.query.search')}
          disabled={isQueryFalsy}
        />
        <Ball
          data-cy="btn-clear"
          icon="times"
          onClick={clearQuery}
          title={msg('client.component.list.reset')}
          disabled={!doesQueryExist}
        />
        <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-query-menu'}}>
          <MenuItem onClick={loadSearchAsQuery} data-cy="menuitem-open-search-as-tql-query">
            <FormattedMessage id="client.entity-list.query.search.open" />
          </MenuItem>
          <MenuItem
            onClick={saveQueryAsFilter}
            disabled={isQueryFalsy}
            data-cy="menuitem-save-tql-query-as-search-filter"
          >
            <FormattedMessage id="client.entity-list.query.filter.save" />
          </MenuItem>
        </BallMenu>
      </SidepanelHeader>
      <StyledQueryBox>
        <StatedValue
          isDisplay={true}
          hasValue={true}
          label={msg('client.entity-list.query.entity-model')}
          labelPosition="inside"
        >
          <FormattedValue type="string" value={entityModel} />
        </StatedValue>
        <StatedValue
          touched={doesQueryExist}
          fixLabel={true}
          hasValue={doesQueryExist}
          label={msg('client.entity-list.query.editor')}
          signal={signal}
          labelPosition="inside"
        >
          <EditableValue
            id="input-tql-query"
            type="code"
            value={query}
            options={codeEditorOptions}
            events={codeEditorEvents}
          />
        </StatedValue>
        {queryErrorValues.map((error, index) => (
          <StyledErrorMessage key={index} data-cy="query-error-message">
            {error}
          </StyledErrorMessage>
        ))}
      </StyledQueryBox>
    </>
  )
}

QueryView.propTypes = {
  msg: PropTypes.func.isRequired,
  setQueryViewVisible: PropTypes.func.isRequired,
  entityModel: PropTypes.string.isRequired,
  query: PropTypes.string,
  queryError: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.node, PropTypes.string]))),
  loadSearchAsQuery: PropTypes.func.isRequired,
  saveQueryAsFilter: PropTypes.func.isRequired,
  setQuery: PropTypes.func.isRequired,
  runQuery: PropTypes.func.isRequired,
  clearQuery: PropTypes.func.isRequired
}

export default QueryView
