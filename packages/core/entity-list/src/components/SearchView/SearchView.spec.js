import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import searchFormTypes from '../../util/searchFormTypes'

import SearchView from './SearchView'

const EMPTY_FUNC = () => {}

/* eslint-disable react/prop-types */
jest.mock('../../../../app-extensions/src/errorLogging/components/ErrorBoundaryContainer', () => ({children}) => (
  <div data-testid="error-boundary">{children}</div>
))
jest.mock('../../containers/BasicSearchFormContainer', () => () => <div data-testid="basic-search-form"></div>)
jest.mock('../../containers/FullTextSearchFormContainer', () => () => <div data-testid="fulltext-search-form"></div>)
jest.mock('../AdminSearchForm', () => () => <div data-testid="admin-search-form"></div>)
/* eslint-enable react/prop-types */

describe('entity-list', () => {
  describe('components', () => {
    describe('SearchView', () => {
      test('should show search form depending on type', () => {
        const {rerender} = testingLibrary.renderWithIntl(
          <SearchView initializeSearchForm={EMPTY_FUNC} searchFormType={searchFormTypes.FULLTEXT} />
        )
        expect(screen.queryByTestId('fulltext-search-form')).to.exist

        rerender(<SearchView initializeSearchForm={EMPTY_FUNC} searchFormType={searchFormTypes.SIMPLE_ADVANCED} />)
        expect(screen.queryByTestId('basic-search-form')).to.exist

        rerender(<SearchView initializeSearchForm={EMPTY_FUNC} searchFormType={searchFormTypes.ADMIN} />)
        expect(screen.queryByTestId('admin-search-form')).to.exist
      })
    })
  })
})
