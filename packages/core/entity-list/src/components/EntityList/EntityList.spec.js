import {screen} from '@testing-library/react'
import {appFactory} from 'tocco-app-extensions'
import {testingLibrary} from 'tocco-test-util'

import reducers, {sagas} from '../../modules/reducers'

import EntityList from './EntityList'

describe('entity-list', () => {
  describe('components', () => {
    describe('EntityList', () => {
      test('should render ListView', async () => {
        const store = appFactory.createStore(reducers, sagas, {})

        testingLibrary.renderWithStore(<EntityList />, {store})
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('client.component.list.loadingText')).to.exist
      })
    })
  })
})
