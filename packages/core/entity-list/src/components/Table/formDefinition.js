import {form} from 'tocco-app-extensions'

export const createSimpleFormDefinition = (intl, options) => {
  const msg = id => intl.formatMessage({id})

  const formDefinition = form.createSimpleForm({
    children: [
      form.createHorizontalBox({
        children: [
          form.createVerticalBox({
            children: [
              form.createFieldSet({
                path: 'numOfRows',
                dataType: 'single-select-box',
                label: msg('client.component.list.preferences.numOfRows'),
                additionalFieldAttributes: {
                  options: [...options]
                },
                validation: form.createValidation(form.createMandatoryValidation())
              })
            ]
          })
        ]
      })
    ]
  })

  return formDefinition
}
