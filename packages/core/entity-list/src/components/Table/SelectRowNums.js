import PropTypes from 'prop-types'
import {useState} from 'react'
import {injectIntl} from 'react-intl'
import SimpleFormApp from 'tocco-simple-form/src/main'

import {createSimpleFormDefinition} from './formDefinition'

const SelectNumRows = ({intl, onOk, pageLimitOptions, numOfRows}) => {
  const msg = id => intl.formatMessage({id})
  const options = pageLimitOptions.map(option => ({key: option, display: `${option}`}))
  const matchingValue = options.find(option => option.key === numOfRows)
  const [value, setValue] = useState(matchingValue)
  const handleOk = () => onOk(value?.key)
  const handleChange = ({values}) => {
    setValue(values.numOfRows)
  }

  return (
    <>
      <SimpleFormApp
        form={createSimpleFormDefinition(intl, options)}
        labelPosition="inside"
        mode="create"
        onChange={handleChange}
        onSubmit={handleOk}
        submitText={msg('client.component.list.preferences.numOfRows.okButton')}
        defaultValues={{
          numOfRows: value
        }}
        validate
      />
    </>
  )
}

SelectNumRows.propTypes = {
  intl: PropTypes.object.isRequired,
  onOk: PropTypes.func.isRequired,
  pageLimitOptions: PropTypes.arrayOf(PropTypes.number).isRequired,
  numOfRows: PropTypes.number
}

export default injectIntl(SelectNumRows)
