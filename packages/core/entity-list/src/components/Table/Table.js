import _get from 'lodash/get'
import PropTypes from 'prop-types'
import React, {useCallback, useMemo} from 'react'
import {columnPropType, scrollBehaviourPropType, selectionStylePropType, Table as UiTable} from 'tocco-ui'
import {js} from 'tocco-util'

import {sortColumnsByPreferences} from '../../util/columns'

import {markingCell} from './markingCell'
import {navigationCell} from './navigationCell'
import TableTileHeaderContainer from './TableTileHeaderContainer'

const Table = ({
  showLink,
  showNumbering,
  clickable,
  navigationStrategy,
  disablePreferencesMenu,
  parent,
  markable,
  columnDefinitions,
  widths,
  positions,
  entities,
  changeWidth,
  inProgress,
  currentPage,
  entityCount,
  limit,
  changePage,
  refresh,
  setSortingInteractive,
  tableSelectionStyle,
  onSelectChange,
  selection,
  selectionFilterFn,
  scrollBehaviour,
  onRowClick,
  changePosition,
  lastOpened,
  layout = 'table'
}) => {
  const showNavigationLink = showLink && clickable && navigationStrategy && !!navigationStrategy.DetailLinkRelative
  const showPreferencesMenu = !disablePreferencesMenu
  const columns = useMemo(
    () => [
      ...((showNavigationLink || showPreferencesMenu) && layout !== 'tile' // do not show navigation arrow on tiles
        ? [navigationCell(showNavigationLink, navigationStrategy, parent)]
        : []),
      ...(markable ? [markingCell()] : []),
      ...sortColumnsByPreferences(
        columnDefinitions.map(a => ({...a, width: _get(widths, a.id)})),
        positions
      )
    ],
    [
      columnDefinitions,
      markable,
      navigationStrategy,
      showNavigationLink,
      parent,
      positions,
      showPreferencesMenu,
      widths,
      layout
    ]
  )
  const onColumnPositionChange = useCallback(
    (dragging, dragOver, horizontalDropPosition) => changePosition(dragging, dragOver, horizontalDropPosition, columns),
    [changePosition, columns]
  )

  return (
    <UiTable
      data={entities}
      columns={columns}
      onColumnWidthChange={changeWidth}
      dataLoadingInProgress={inProgress}
      paginationInfo={{
        currentPage,
        totalCount: entityCount,
        recordsPerPage: limit
      }}
      onPageChange={changePage}
      onPageRefresh={refresh}
      onSortingChange={setSortingInteractive}
      selectionStyle={tableSelectionStyle}
      onSelectionChange={onSelectChange}
      selection={selection}
      selectionFilterFn={selectionFilterFn}
      scrollBehaviour={scrollBehaviour}
      onRowClick={onRowClick}
      clickable={clickable}
      onColumnPositionChange={onColumnPositionChange}
      lastOpened={lastOpened}
      showNumbering={showNumbering}
      layout={layout}
      tileHeader={TableTileHeaderContainer}
    />
  )
}

Table.propTypes = {
  columnDefinitions: PropTypes.arrayOf(columnPropType).isRequired,
  entities: PropTypes.array.isRequired,
  entityCount: PropTypes.number,
  inProgress: PropTypes.bool,
  currentPage: PropTypes.number,
  limit: PropTypes.number,
  onRowClick: PropTypes.func,
  clickable: PropTypes.bool,
  setSortingInteractive: PropTypes.func,
  refresh: PropTypes.func,
  changePage: PropTypes.func.isRequired,
  tableSelectionStyle: selectionStylePropType,
  onSelectChange: PropTypes.func,
  selection: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
  selectionFilterFn: PropTypes.func,
  scrollBehaviour: scrollBehaviourPropType,
  parent: PropTypes.shape({
    key: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    reverseRelationName: PropTypes.string,
    relation: PropTypes.string
  }),
  showLink: PropTypes.bool,
  showNumbering: PropTypes.bool,
  navigationStrategy: PropTypes.objectOf(PropTypes.func),
  changePosition: PropTypes.func.isRequired,
  positions: PropTypes.objectOf(PropTypes.number),
  changeWidth: PropTypes.func.isRequired,
  widths: PropTypes.objectOf(PropTypes.number),
  markable: PropTypes.bool,
  disablePreferencesMenu: PropTypes.bool,
  lastOpened: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  layout: PropTypes.string
}

const areEqual = (prevProps, nextProps) => {
  const diff = js.difference(prevProps, nextProps)
  return Object.keys(diff).length === 0
}

export default React.memo(Table, areEqual)
