import {connect} from 'react-redux'

import {showTableLayout} from '../../modules/preferences/actions'

import TableTileHeader from './TableTileHeader'

const mapActionCreators = {
  showTableLayout
}

export default connect(null, mapActionCreators)(TableTileHeader)
