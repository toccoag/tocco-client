import {screen} from '@testing-library/react'
import PropTypes from 'prop-types'
import {testingLibrary} from 'tocco-test-util'

import {navigationCell} from './navigationCell'

describe('entity-list', () => {
  describe('components', () => {
    describe('Table', () => {
      describe('navigationCell', () => {
        describe('CellRenderer', () => {
          const FakeLink = ({entityKey, entityModel, relation}) => (
            <p>
              {entityKey} {entityModel} {relation}
            </p>
          )

          FakeLink.propTypes = {
            entityKey: PropTypes.string,
            entityModel: PropTypes.string,
            relation: PropTypes.string
          }

          const navigationStrategy = {
            DetailLinkRelative: FakeLink
          }
          const rowData = {__key: 'key', __model: 'model'}
          const parent = {relationName: 'parent'}

          test('should not render link when navigation hidden', () => {
            const cellConfig = navigationCell(false, navigationStrategy, parent)
            testingLibrary.renderWithIntl(<cellConfig.CellRenderer rowData={rowData} />)

            expect(screen.queryByText('key model')).to.not.exist
          })
          test('should render link', () => {
            const cellConfig = navigationCell(true, navigationStrategy, {})
            testingLibrary.renderWithIntl(<cellConfig.CellRenderer rowData={rowData} />)

            expect(screen.queryByText('key model')).to.exist
          })
          test('should render link with parent', () => {
            const cellConfig = navigationCell(true, navigationStrategy, parent)
            testingLibrary.renderWithIntl(<cellConfig.CellRenderer rowData={rowData} />)

            expect(screen.queryByText('key model parent')).to.exist
          })
          test('should render link with parent and form suffix', () => {
            const parentWithSuffix = {relationName: 'parent', formSuffix: 'suffix'}
            const cellConfig = navigationCell(true, navigationStrategy, parentWithSuffix)
            testingLibrary.renderWithIntl(<cellConfig.CellRenderer rowData={rowData} />)

            expect(screen.queryByText('key model suffix')).to.exist
          })
        })
      })
    })
  })
})
