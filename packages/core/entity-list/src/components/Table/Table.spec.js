import {screen} from '@testing-library/react'
import {appFactory} from 'tocco-app-extensions'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'
import reducers, {sagas} from '../../modules/reducers'

import Table from './Table'

const EMPTY_FUNC = () => {}

const defaultProps = {
  entityCount: 1,
  initialize: EMPTY_FUNC,
  limit: 10,
  columnDefinitions: [],
  changePage: EMPTY_FUNC,
  entities: [],
  intl: IntlStub,
  orderBy: null,
  sorting: [],
  currentPage: 1,
  changeWidth: EMPTY_FUNC,
  changePosition: EMPTY_FUNC
}

describe('entity-list', () => {
  describe('components', () => {
    describe('Table', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
      })

      test('should render table', async () => {
        const store = appFactory.createStore(reducers, sagas, {navigationStrategy: {}})
        testingLibrary.renderWithStore(<Table {...defaultProps} />, {store})
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('client.component.table.noData')).to.exist
        expect(screen.queryByText('client.component.pagination.text')).to.exist
      })
    })
  })
})
