import {connect} from 'react-redux'

import {
  displayColumnModal,
  resetSorting,
  resetPreferences,
  resetColumns,
  resetWidths,
  displayTableRowsModal,
  showTileLayout
} from '../../modules/preferences/actions'
import {getDefaultTableLayout} from '../../modules/selectors'

import NavigationCellHeader from './NavigationCellHeader'

const mapActionCreators = {
  displayColumnModal,
  resetSorting,
  resetPreferences,
  resetColumns,
  resetWidths,
  displayTableRowsModal,
  showTileLayout
}

const mapStateToProps = (state, props) => {
  return {
    sortable: state.input.sortable,
    disablePreferencesMenu: state.list.disablePreferencesMenu,
    isTileLayoutAvailable: getDefaultTableLayout(state) === 'tile'
  }
}

export default connect(mapStateToProps, mapActionCreators)(NavigationCellHeader)
