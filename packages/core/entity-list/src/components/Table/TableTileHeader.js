import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {BallMenu, MenuItem} from 'tocco-ui'

import {getCurrentDevice} from '../../util/preferences'

const TableTileHeader = ({showTableLayout}) => (
  <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-preferences'}} isTransparent>
    <MenuItem onClick={() => showTableLayout(getCurrentDevice())} data-cy="menuitem-tableLayout">
      <FormattedMessage id="client.component.list.preferences.tableLayout" />
    </MenuItem>
  </BallMenu>
)

TableTileHeader.propTypes = {
  showTableLayout: PropTypes.func.isRequired
}

export default TableTileHeader
