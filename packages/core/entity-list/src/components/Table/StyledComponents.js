import styled from 'styled-components'
import {themeSelector, scale, colorizeBorder, StyledFontAwesomeAdapterWrapper, Button} from 'tocco-ui'

export const StyledMarkingWrapper = styled(Button)`
  && {
    ${StyledFontAwesomeAdapterWrapper} {
      ${({marked, theme}) => marked && `color: ${theme.colors.secondary};`}
    }

    &:hover ${StyledFontAwesomeAdapterWrapper} {
      color: ${themeSelector.color('secondaryLight')};
    }
  }
`

export const StyledEditableValueWrapper = styled.div`
  border: 1px solid ${colorizeBorder.base};
  padding-right: ${scale.space(-1)};
  padding-left: ${scale.space(-1)};
  border-radius: ${themeSelector.radii('form')};
`

export const StyledNavigationCellHeader = styled.div`
  position: relative;
  right: 3px;
`
