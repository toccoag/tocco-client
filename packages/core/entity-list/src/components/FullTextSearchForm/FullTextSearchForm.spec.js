import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import FullTextSearchForm from './'

const EMPTY_FUNC = () => {}

describe('entity-list', () => {
  describe('components', () => {
    describe('FullTextSearchForm', () => {
      test('should render SearchBox component', async () => {
        const store = configureStore({reducer: () => {}})

        testingLibrary.renderWithStore(<FullTextSearchForm submitSearchForm={EMPTY_FUNC} intl={IntlStub} />, {store})
        await screen.findAllByTestId('icon')

        expect(screen.queryByPlaceholderText('client.entity-list.fullTextPlaceholder')).to.exist
      })
    })
  })
})
