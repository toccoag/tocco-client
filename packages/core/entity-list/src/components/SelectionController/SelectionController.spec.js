import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SelectionController from './SelectionController'

const EMPTY_FUNC = () => {}

describe('entity-list', () => {
  describe('components', () => {
    const baseProps = {
      clearSelection: EMPTY_FUNC,
      intl: {formatMessage: EMPTY_FUNC},
      queryCount: 123,
      selection: [],
      setSelectionMode: EMPTY_FUNC,
      toggleShowSelectedRecords: EMPTY_FUNC
    }

    describe('Selection', () => {
      it('should display message selectionQuery and queryCount', () => {
        testingLibrary.renderWithIntl(<SelectionController {...baseProps} />, {
          intlMessages: {'client.entity-list.selectionAll': 'Alle ({count})'}
        })

        expect(screen.queryByText('Alle (123)')).to.exist
        expect(screen.queryAllByRole('button')).to.have.length(0)
      })

      it('should display message selectionSelection and count of selected items ', async () => {
        const selection = new Array(99)

        testingLibrary.renderWithIntl(<SelectionController {...baseProps} selection={selection} />, {
          intlMessages: {'client.entity-list.selectionSelection': 'Selektiert ({count})'}
        })
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('Selektiert (99)')).to.exist
        expect(screen.queryAllByRole('button')).to.have.length(1)
      })
    })
  })
})
