import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import searchFormTypes from '../../util/searchFormTypes'

import SearchForm from './'

const EMPTY_FUNC = () => {}

describe('entity-list', () => {
  describe('components', () => {
    describe('BasicSearchForm', () => {
      const defaultStoreBuilder = () => ({
        formData: {
          relationEntities: {data: {}},
          tooltips: {data: {}},
          config: {configSelector: () => ({})}
        },
        form: {
          detailForm: {}
        }
      })

      const entityModel = require('../../dev/test-data/userModel.json')
      const searchFormDefinition = require('../../dev/test-data/searchFormDefinition.json')

      const props = {
        initializeSearchForm: EMPTY_FUNC,
        entityModel,
        searchFormDefinition,
        setSearchInput: EMPTY_FUNC,
        relationEntities: {},
        searchInputs: {},
        loadRelationEntities: EMPTY_FUNC,
        loadTooltip: EMPTY_FUNC,
        submitSearchForm: EMPTY_FUNC,
        resetSearch: EMPTY_FUNC,
        intl: IntlStub,
        searchFormType: searchFormTypes.SIMPLE_ADVANCED,
        simpleSearchFields: [],
        showExtendedSearchForm: true,
        preselectedSearchFields: [],
        setShowExtendedSearchForm: EMPTY_FUNC,
        loadSearchFilters: EMPTY_FUNC,
        openAdvancedSearch: EMPTY_FUNC,
        changeFieldValue: EMPTY_FUNC,
        formValues: {}
      }

      test('should render nothing if searchFormDefinition empty', () => {
        const store = configureStore({
          reducer: () => ({
            form: {
              detailForm: {}
            }
          })
        })

        const {container} = testingLibrary.renderWithStore(<SearchForm {...props} searchFormDefinition={{}} />, {store})

        jestExpect(container).toBeEmptyDOMElement()
      })

      test('should render needed components', () => {
        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(<SearchForm {...props} />, {store})

        expect(screen.queryByLabelText('Person')).to.exist
        expect(screen.queryByLabelText('Adresse')).to.exist
        expect(screen.queryByTitle('Personen-Code 1')).to.exist
      })

      test('should render only the simple search fields from the form definition', () => {
        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(
          <SearchForm {...props} searchFormType={searchFormTypes.SIMPLE} showExtendedSearchForm={false} />,
          {store}
        )

        expect(screen.queryByLabelText('Person')).to.exist
        expect(screen.queryByLabelText('Adresse')).to.not.exist
        expect(screen.queryByTitle('Personen-Code 1')).to.not.exist
      })

      test('should render simple search fields from props (override form definition)', () => {
        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(
          <SearchForm
            {...props}
            searchFormType={searchFormTypes.SIMPLE}
            showExtendedSearchForm={false}
            simpleSearchFields={['txtFulltext', 'relUser_code1']}
          />,
          {store}
        )

        expect(screen.queryByLabelText('Person')).to.exist
        expect(screen.queryByLabelText('Adresse')).to.not.exist
        expect(screen.queryByTitle('Personen-Code 1')).to.exist
      })

      test('should not show hidden value', () => {
        const preselectedSearchFields = [
          {
            id: 'relUser_code1',
            hidden: true
          }
        ]

        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(
          <SearchForm
            {...props}
            searchFormType={searchFormTypes.ADVANCED}
            simpleSearchFields={[]}
            preselectedSearchFields={preselectedSearchFields}
          />,
          {store}
        )

        expect(screen.queryByLabelText('Person')).to.exist
        expect(screen.queryByLabelText('Adresse')).to.exist
        expect(screen.queryByTitle('Personen-Code 1')).to.not.exist
      })

      test('should focus search field', () => {
        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(
          <SearchForm {...props} searchFormType={searchFormTypes.ADVANCED} simpleSearchFields={[]} />,
          {store}
        )

        const textBoxes = screen.queryAllByRole('textbox')
        expect(textBoxes).to.have.length(2)
        jestExpect(textBoxes[0]).toHaveFocus()
        jestExpect(textBoxes[1]).not.toHaveFocus()
      })

      test('should not focus search field when in sub grid ', () => {
        const store = configureStore({reducer: defaultStoreBuilder})

        testingLibrary.renderWithStore(
          <SearchForm {...props} searchFormType={searchFormTypes.ADVANCED} simpleSearchFields={[]} isSubGrid={true} />,
          {store}
        )

        const textBoxes = screen.queryAllByRole('textbox')
        expect(textBoxes).to.have.length(2)
        jestExpect(textBoxes[0]).not.toHaveFocus()
        jestExpect(textBoxes[1]).not.toHaveFocus()
      })
    })
  })
})
