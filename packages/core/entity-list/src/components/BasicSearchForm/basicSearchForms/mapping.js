import {env} from 'tocco-util'

import BasicSeachForm from './admin'
import WidgetBasicSearchForm from './widget'

export const mapping = {
  [env.EmbedType.admin]: BasicSeachForm,
  [env.EmbedType.widget]: WidgetBasicSearchForm
}
