import PropTypes from 'prop-types'
import {useRef} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Ball} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import searchFormTypes from '../../../../util/searchFormTypes'

import {StyledSearchFormButtons} from './StyledBasicSearchForm'

const REDUX_FORM_NAME = 'searchForm'

const AdminBasicSearchForm = ({
  searchFormType,
  entity,
  form: formName,
  formValues,
  intl,
  preselectedSearchFields,
  searchFormDefinition,
  setShowExtendedSearchForm,
  showExtendedSearchForm,
  simpleSearchFields: inputSimpleSearchFields,
  submitSearchForm,
  isSubGrid
}) => {
  const searchFormEl = useRef(null)

  customHooks.useAutofocus(searchFormEl, {selectFulltextFields: true, disabled: isSubGrid}, [searchFormDefinition])

  if (!searchFormDefinition.children) {
    return null
  }

  const msg = id => intl.formatMessage({id})

  const handleSubmit = e => {
    e.preventDefault()
    e.stopPropagation()
    submitSearchForm()
  }

  const isHidden = (searchFields, name) => searchFields?.find(f => f.id === name)?.hidden || false

  const toggleExtendedSearchForm = () => {
    setShowExtendedSearchForm(!showExtendedSearchForm)
  }

  const fields = form.getFieldDefinitions(searchFormDefinition)
  const simpleSearchFields =
    inputSimpleSearchFields?.length > 0
      ? inputSimpleSearchFields
      : fields.filter(field => field.simpleSearch === true).map(field => field.path || field.id)
  const hasExtendedOnlySearchFields = !fields.every(field => simpleSearchFields.includes(field.id))
  const isExtendable = searchFormType === searchFormTypes.SIMPLE_ADVANCED && hasExtendedOnlySearchFields

  const shouldRenderField = name =>
    !isHidden(preselectedSearchFields, name) && (showExtendedSearchForm || simpleSearchFields.includes(name))

  return (
    <form onSubmit={handleSubmit} ref={searchFormEl}>
      {isExtendable && (
        <StyledSearchFormButtons>
          <Ball
            data-cy="btn-extend-search"
            icon={`chevron-${showExtendedSearchForm ? 'up' : 'down'}`}
            onClick={toggleExtendedSearchForm}
            title={msg('client.common.extendedSearch')}
          />
        </StyledSearchFormButtons>
      )}
      <form.FormBuilder
        entity={entity}
        formName={formName}
        formDefinition={searchFormDefinition}
        formValues={formValues}
        fieldMappingType="search"
        beforeRenderField={shouldRenderField}
        mode="search"
        labelPosition="inside"
      />
    </form>
  )
}

AdminBasicSearchForm.propTypes = {
  intl: PropTypes.object.isRequired,
  searchFormDefinition: PropTypes.shape({
    children: PropTypes.array
  }).isRequired,
  submitSearchForm: PropTypes.func.isRequired,
  searchFormType: PropTypes.string.isRequired,
  simpleSearchFields: PropTypes.arrayOf(PropTypes.string),
  showExtendedSearchForm: PropTypes.bool,
  setShowExtendedSearchForm: PropTypes.func.isRequired,
  preselectedSearchFields: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      hidden: PropTypes.bool
    })
  ),
  entity: PropTypes.object,
  form: PropTypes.string,
  formValues: PropTypes.object,
  isSubGrid: PropTypes.bool
}

export default reduxForm({
  form: REDUX_FORM_NAME,
  destroyOnUnmount: false
})(AdminBasicSearchForm)
