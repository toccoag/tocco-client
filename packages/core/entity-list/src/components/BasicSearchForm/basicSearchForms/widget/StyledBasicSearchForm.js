import styled from 'styled-components'
import {StyledLayoutBox, scale, Button} from 'tocco-ui'

export const StyledSimpleSearchFormWrapper = styled.div`
  justify-content: flex-end;
  margin-bottom: ${scale.space(-0.5)};
  display: grid;
  align-items: center;
  grid-template-columns: repeat(auto-fit, minmax(429px, 1fr));
  grid-template-rows: auto;
  gap: ${scale.space(-0.5)};

  ${StyledLayoutBox} {
    margin-bottom: 0;
    display: contents;

    > div {
      margin-bottom: 0;
    }
  }
`

export const StyledExtendButton = styled(Button)`
  padding-top: ${scale.space(0)};
  padding-bottom: ${scale.space(0.1)};
  border-radius: 32px;
  margin-right: 0;
  justify-content: center;
`
