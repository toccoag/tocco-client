import {env} from 'tocco-util'

import {mapping} from './basicSearchForms/mapping'

const BasicSeachForm = env.embedStrategyFactory({mapping})

export default BasicSeachForm
