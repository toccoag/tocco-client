import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {BallMenu, MenuItem} from 'tocco-ui'

const SearchFilterMenu = ({canEdit, canDelete, navigationStrategy, deleteSearchFilter, primaryKey, active}) => {
  const entries = []

  if (canEdit) {
    entries.push(
      <MenuItem key={'edit-button'} onClick={() => navigationStrategy.openDetail('Search_filter', primaryKey)}>
        <FormattedMessage id="client.entity-list.search.settings.editFilter" />
      </MenuItem>
    )
  }
  if (canDelete && typeof deleteSearchFilter === 'function') {
    entries.push(
      <MenuItem key={'delete-button'} onClick={deleteSearchFilter}>
        <FormattedMessage id="client.entity-list.search.settings.deleteFilter" />
      </MenuItem>
    )
  }

  if (entries.length === 0) {
    return null
  }

  return <BallMenu buttonProps={{icon: 'ellipsis-v'}}>{entries}</BallMenu>
}

SearchFilterMenu.propTypes = {
  canEdit: PropTypes.bool,
  canDelete: PropTypes.bool,
  primaryKey: PropTypes.string,
  deleteSearchFilter: PropTypes.func,
  navigationStrategy: PropTypes.objectOf(PropTypes.func),
  active: PropTypes.bool
}

export default SearchFilterMenu
