import styled from 'styled-components'

export const SearchFilterListWrapper = styled.div`
  margin-top: 0.4rem;
  margin-bottom: 0.4rem;
`

export const StyledMessageWrapper = styled.div`
  padding-left: 8px;
`
