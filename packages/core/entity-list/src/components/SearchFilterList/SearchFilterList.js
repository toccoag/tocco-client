import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Typography, SidepanelHorizontalButton} from 'tocco-ui'

import SearchFilterMenu from './SearchFilterMenu'
import {SearchFilterListWrapper, StyledMessageWrapper} from './StyledComponents'
import {searchFilterCompare} from './utils'

const AdminSearchForm = props => {
  const {searchFilters, setSearchFilterActive, executeSearch, navigationStrategy, deleteSearchFilter} = props
  if (!searchFilters) {
    return null
  }

  if (searchFilters.length === 0) {
    return (
      <StyledMessageWrapper>
        <Typography.I>
          <FormattedMessage id="client.entity-list.noSearchFilters" />
        </Typography.I>
      </StyledMessageWrapper>
    )
  }

  return (
    <SearchFilterListWrapper>
      {[...searchFilters].sort(searchFilterCompare).map(searchFilter => (
        <SidepanelHorizontalButton
          key={searchFilter.uniqueId}
          primaryKey={searchFilter.key}
          active={searchFilter.active}
          label={searchFilter.label}
          title={!searchFilter.description ? searchFilter.label : `${searchFilter.label}: ${searchFilter.description}`}
          setActive={exclusive => {
            setSearchFilterActive(searchFilter.uniqueId, !searchFilter.active, exclusive)
            executeSearch()
          }}
          menuElement={
            <SearchFilterMenu
              canEdit={searchFilter.editAllowed}
              canDelete={searchFilter.deleteAllowed}
              navigationStrategy={navigationStrategy}
              deleteSearchFilter={() => deleteSearchFilter(searchFilter.key)}
              primaryKey={searchFilter.key}
              active={searchFilter.active}
            />
          }
        />
      ))}
    </SearchFilterListWrapper>
  )
}

AdminSearchForm.propTypes = {
  searchFilters: PropTypes.arrayOf(PropTypes.object),
  setSearchFilterActive: PropTypes.func.isRequired,
  executeSearch: PropTypes.func.isRequired,
  navigationStrategy: PropTypes.objectOf(PropTypes.func),
  deleteSearchFilter: PropTypes.func.isRequired
}

export default AdminSearchForm
