import PropTypes from 'prop-types'
import {useState} from 'react'
import {injectIntl} from 'react-intl'
import SimpleFormApp from 'tocco-simple-form/src/main'

import {createSimpleFormDefinition} from './formDefinition'

const SearchFilterNameForm = ({intl, onSave}) => {
  const [name, setName] = useState('')

  const handleSave = () => onSave(name)
  const msg = id => intl.formatMessage({id})

  const handleChange = ({values}) => {
    setName(values.label)
  }

  return (
    <SimpleFormApp
      form={createSimpleFormDefinition(intl)}
      labelPosition="inside"
      mode="create"
      onChange={handleChange}
      onSubmit={handleSave}
      submitText={msg('client.entity-list.search.settings.saveAsFilter.button')}
    />
  )
}

SearchFilterNameForm.propTypes = {
  onSave: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default injectIntl(SearchFilterNameForm)
