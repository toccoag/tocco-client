/* eslint-disable react/prop-types */

import EntityListApp from './main'
import searchFormTypes from './util/searchFormTypes'

export default {
  title: 'Core/Entity List',
  component: EntityListApp,
  argTypes: {
    searchFormType: {
      options: Object.values(searchFormTypes),
      control: {type: 'select'}
    },
    searchFormPosition: {
      options: ['top', 'left'],
      control: {type: 'select'}
    },
    scrollBehaviour: {
      options: ['none', 'inline'],
      control: {type: 'select'}
    },
    selectionStyle: {
      options: ['single', 'multi', 'multi_explicit', 'none'],
      control: {type: 'select'}
    }
  }
}

const EntityListStory = ({containerHeight, ...args}) => {
  return (
    <div style={{width: '1500px', height: containerHeight, padding: '5px'}}>
      <EntityListApp {...args} />
    </div>
  )
}

export const Story = EntityListStory.bind({})
Story.args = {
  id: 'entity-list-storybook',
  entityName: 'User',
  formName: 'User',
  containerHeight: '300px',
  searchFormType: searchFormTypes.SIMPLE,
  searchFormPosition: 'top',
  limit: 20,
  scrollBehaviour: 'inline'
}
