import {form} from 'tocco-app-extensions'

import {getCurrentTableLayout, getDefaultTableLayout} from './selectors'

const pretendTouchDevice = (touch = true) => {
  window.matchMedia = () => ({
    matches: touch
  })
}

const state = {
  list: {
    formDefinition: {
      children: [
        {
          componentType: form.componentTypes.TABLE,
          mobileLayout: 'tile',
          desktopLayout: 'table'
        }
      ]
    }
  },
  preferences: {
    layouts: {
      mobile: 'table',
      desktop: 'tile'
    }
  }
}

describe('entity-list', () => {
  describe('modules', () => {
    describe('selectors', () => {
      describe('getDefaultTableLayout', () => {
        test('should return mobile layout from form definition', () => {
          pretendTouchDevice()

          expect(getDefaultTableLayout(state)).to.eql('tile')
        })

        test('should return desktop layout from form definition', () => {
          pretendTouchDevice(false)

          expect(getDefaultTableLayout(state)).to.eql('table')
        })

        test('should return undefined when form definition is not loaded yet', () => {
          const emptyState = {
            list: {
              formDefinition: null
            }
          }

          expect(getDefaultTableLayout(emptyState)).to.be.undefined
        })
      })

      describe('getCurrentTableLayout', () => {
        test('should return saved preferences for mobile', () => {
          pretendTouchDevice()

          expect(getCurrentTableLayout(state)).to.eql('table')
        })
        test('should return saved preferences for desktop', () => {
          pretendTouchDevice(false)

          expect(getCurrentTableLayout(state)).to.eql('tile')
        })
        test('should return default for mobile if no preferences are set', () => {
          pretendTouchDevice()

          expect(getCurrentTableLayout({...state, preferences: {layouts: {}}})).to.eql('tile')
        })
        test('should return default for desktop if no preferences are set', () => {
          pretendTouchDevice(false)

          expect(getCurrentTableLayout({...state, preferences: {layouts: {}}})).to.eql('table')
        })
      })
    })
  })
})
