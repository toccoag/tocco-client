import {device} from 'tocco-util'

import {getFormDefinition, getTable} from '../util/api/forms'
import {getCurrentDevice} from '../util/preferences'

export const getDefaultTableLayout = state => {
  const formDefinition = getFormDefinition(state)
  if (formDefinition) {
    const table = getTable(formDefinition)
    return device.isPrimaryTouchDevice() ? table.mobileLayout : table.desktopLayout
  }
  return undefined
}

export const getCurrentTableLayout = state => {
  const layoutPreferences = state.preferences.layouts
  const preferenceLayout = layoutPreferences[getCurrentDevice()]
  const tableLayout = getDefaultTableLayout(state)
  const layout = preferenceLayout || tableLayout
  return layout
}
