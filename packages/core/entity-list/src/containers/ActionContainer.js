import {connect} from 'react-redux'
import {actions} from 'tocco-app-extensions'

import {deriveSelectionFromState} from '../util/actions'

const mapStateToProps = (state, props) => ({
  selection: props.getSelectionForAction ? props.getSelectionForAction(state) : deriveSelectionFromState(state)
})

const mapActionCreators = {}

export default connect(mapStateToProps, mapActionCreators)(actions.Action)
