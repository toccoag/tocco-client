import PropTypes from 'prop-types'
import React from 'react'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  formData,
  notification,
  reports
} from 'tocco-app-extensions'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {scrollBehaviourPropType} from 'tocco-ui'
import {navigationStrategy, reducer as reducerUtil} from 'tocco-util'

import {Box, StyledSplitWrapper} from './components/AdminSearchForm/StyedComponents'
import ColumnModal from './components/ColumnModal'
import EntityList from './components/EntityList'
import {getActionBarContent} from './components/ListView/getActionBarContent'
import {StyledActionWrapper, StyledListView} from './components/ListView/StyledComponents'
import SelectNumRows from './components/Table/SelectRowNums'
import customActions from './customActions'
import {getDispatchActions, transformInput} from './input'
import {refresh} from './modules/list/actions'
import {customEndpointActionPrepareHandler} from './modules/list/sagas'
import reducers, {sagas} from './modules/reducers'
import selectionReducer, {sagas as selectionSagas} from './modules/selection'
import * as selectionActions from './modules/selection/actions'
import {entitiesListTransformer} from './util/api/entities'
import * as formsApi from './util/api/forms'
import * as columnsUtil from './util/columns'
import * as preferencesUtil from './util/preferences'
import {searchFormTypePropTypes} from './util/searchFormTypes'
import {validateSearchFields} from './util/searchFormValidation'
import {showSelectionComponent, getTableSelectionStyle, combineSelection} from './util/selection'
import {selectionStylePropType} from './util/selectionStyles'

const packageName = 'entity-list'

const EXTERNAL_EVENTS = [
  /**
   * This event is fired when a list row is clicked
   *
   * Payload: `id` (The id of the record)
   */
  'onRowClick',
  'emitAction',
  /**
   * This event is fired when the selection changes
   *
   * Payload: An array containing the ids of the new selection
   */
  'onSelectChange',
  /**
   * This event is fired when the store for the app is created.
   * Note that the event will neved be fired if a store is passed to the app via the `store` input property
   */
  'onStoreCreate',
  /**
   * Is fired when the user click in the arrow in the admin search form to collapse the search form
   *
   * Payload: `collapsed` boolean. Whether it was opened or closed
   */
  'onSearchFormCollapsedChange'
]

const initApp = (id, input, events, publicPath) => {
  const content = <EntityList />

  let store = input.store

  const allCustomActions = {
    ...customActions(input),
    ...(input.customActions || {})
  }

  const defaultInput = {
    limit: 10,
    scope: 'list',
    showLink: false,
    sortable: true,
    lazyData: {},
    tql: null,
    keys: null,
    searchFilters: null,
    constriction: null,
    entityName: '',
    formName: '',
    searchFormPosition: 'top',
    parent: null,
    searchFormCollapsed: false,
    scrollBehaviour: 'inline'
  }

  const actualInput = {...defaultInput, ...transformInput(input)}

  if (!store) {
    store = appFactory.createStore(reducers, sagas, actualInput, packageName)

    externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
    actionEmitter.addToStore(store, state => state.input.emitAction)
    errorLogging.addToStore(store, false)
    notification.addToStore(store, false)
    reports.addToStore(store)
    actions.addToStore(store, state => ({
      formApp: SimpleFormApp,
      listApp: EntityListApp,
      detailApp: state.input.detailApp,
      customActions: allCustomActions,
      appComponent: state.input.actionAppComponent,
      navigationStrategy: state.input.navigationStrategy,
      context: {
        viewName: 'list',
        formName: state.input.formName,
        ...(state.input.contextParams || {})
      },
      customPreparationHandlers: [customEndpointActionPrepareHandler],
      customActionEventHandlers: state.input.customActionEventHandlers
    }))
    formData.addToStore(store, state => ({listApp: EntityListApp, navigationStrategy: state.input.navigationStrategy}))

    store.dispatch(externalEvents.fireExternalEvent('onStoreCreate', store))
  } else {
    store.dispatch(refresh())
  }

  const app = appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['actiongroup', 'component', 'common']
  })

  if (module.hot) {
    module.hot.accept('./modules/reducers', () => {
      const hotReducers = require('./modules/reducers').default
      reducerUtil.hotReloadReducers(app.store, hotReducers)
    })
  }

  return app
}

;(() => {
  if (__DEV__ && __PACKAGE_NAME__ === 'entity-list') {
    const input = require('./dev/input.json')

    const app = initApp(packageName, input)
    appFactory.renderApp(app.component)
  } else {
    appFactory.registerAppInRegistry(packageName, initApp)
  }
})()

const EntityListApp = props => {
  const {component} = appFactory.useApp({
    initApp,
    props: transformInput(props),
    packageName: props.id,
    externalEvents: EXTERNAL_EVENTS
  })

  return <React.Fragment>{component}</React.Fragment>
}

EntityListApp.propTypes = {
  /**
   * Unique arbitraray id to distinguish the entity-list on a page.
   */
  id: PropTypes.string.isRequired,
  /**
   * Name of the entity that should be listed
   */
  entityName: PropTypes.string.isRequired,
  /**
   * `formName`/`scope` and `formName`/search will be the used forms.
   * Will only be used for the search form and not for the others, if `listFormDefinition` is set.
   */
  formName: PropTypes.string.isRequired,
  /**
   * Scope for the form definitions (default: `'list'`)
   */
  scope: PropTypes.string,
  /**
   * The store to use for the app. If not set, a new store is created and emitted via the `onStoreCreate` event
   */
  store: PropTypes.object,
  /**
   * Amount of records per page
   */
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /**
   * Defines what type of search form is shown.
   *
   * - none (no search form shown)
   * - fulltext (only one fulltext search field)
   * - simple (simple search only)
   * - simple_advanced (usual (simple) search form with advanced expansion)
   * - advanced (extended advanced search form only)
   * - admin (full search with search filter, works only in admin for layouting reasons)
   */
  searchFormType: searchFormTypePropTypes,
  /**
   * Defines whether the search form shown on the top of the list or on the left.
   */
  searchFormPosition: PropTypes.oneOf(['top', 'left']),
  /**
   * Will be applied to the request parameter `_filter` on the list.
   */
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  /**
   * Will be applied to the request parameter `_contriction` on the list.
   * If the form definition has a constriction as well, the constriction of the form will be overridden.
   */
  constriction: PropTypes.string,
  /**
   * Map of custom action handlers.
   * Note that this prop should only be used if you have an action without a component
   * (i.e. an action that only puts a redux action to trigger a saga).
   * If rendering of a component is involved (like it is in most cases), you should use `actionAppComponent`.
   */
  customActions: PropTypes.object,
  /**
   * List of predefined search-values.
   */
  preselectedSearchFields: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.arrayOf(PropTypes.number),
        PropTypes.arrayOf(PropTypes.string)
      ]),
      hidden: PropTypes.bool
    })
  ),
  /**
   * A TQL condition to add to the search query additionally
   */
  tql: PropTypes.string,
  /**
   * Array of keys which will be added to the list search request to only filter by given keys.
   */
  keys: PropTypes.arrayOf(PropTypes.string),
  /**
   * List of field, that should be visible when simple search is active.
   * If empty fulltext search field will be displayed in simple search.
   */
  simpleSearchFields: PropTypes.string,
  /**
   * Defines what type of selection is possible.
   */
  selectionStyle: selectionStylePropType,
  /**
   * Defines if it is taking as much space as needed (`'none'`) or if it will fit into its outer container
   * (`'inline'`). When set to `'inline'` the outer container has to have e predefined height (Default: `inline`)
   *
   * - none: Does not handle scroll internally and will take as much space as needed.
   *   The container / page needs to handle the scroll
   * - inline: Does handle scroll internally and takes the space given by the container.
   *   Containers needs to have a predefined height
   */
  scrollBehaviour: scrollBehaviourPropType,
  /**
   * Minimum height (e.g. `"200px"`) which the list should have
   * to be able to show any content when the `scrollBehaviour` is set to `'inline.'`
   */
  tableMinHeight: PropTypes.string,
  /**
   * Array of keys of the selected records.
   */
  selection: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.string, PropTypes.number])),
  /**
   * If true, the selection controller is not visible (Default: false)
   * selection controller components include the button to display all selected records and
   * the component to switch to the "select-all" mode
   */
  disableSelectionController: PropTypes.bool,
  /**
   * Function that returns a bool if a row is selectable.
   */
  selectionFilterFn: PropTypes.func,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {}),
  /**
   * If true, a click on the row (outside of the selection checkbox) toggles the selection of that particular row.
   */
  selectOnRowClick: PropTypes.bool,
  /**
   * If set, the result gets filtered to only show related entities. (e.g. for Relations-View)
   * disableRelationNavigation can be used to disable further navigation
   */
  parent: PropTypes.shape({
    id: PropTypes.string,
    value: PropTypes.string,
    disableRelationNavigation: PropTypes.bool
  }),
  /**
   * If true a link is shown in each row to open the record. A detail Link factory needs to be provided
   */
  showLink: PropTypes.bool,
  /**
   * Activate row number (Default: false)
   */
  showNumbering: PropTypes.bool,
  /**
   * Component to render custom actions (Needs the appId and selection object property).
   */
  actionAppComponent: PropTypes.elementType,
  /**
   * Object consisting of various link factories. For more information see tocco-util/navigationStrategy documentation.
   */
  navigationStrategy: navigationStrategy.propTypes,
  /**
   * Parameters which will be added to the context that is passed to the invoked action.
   */
  contextParams: PropTypes.object,
  /**
   * If true, the admin search form on the left is collapsed and not visible by default.
   */
  searchFormCollapsed: PropTypes.bool,
  /**
   * Function to modify the form definition.
   */
  modifyFormDefinition: PropTypes.func,
  /**
   * Array of report ids to display on the list in the main action bar.
   */
  reportIds: PropTypes.arrayOf(PropTypes.string),
  /**
   * List from definition. This input will omit a list-form fetch request.
   */
  listFormDefinition: PropTypes.object,
  /**
   * Detail app (tocco-entity-detail) must be provided to support create modals on remote fields.
   */
  detailApp: PropTypes.func,
  /**
   * true if the list is used as a sub grid on a detail form, false otherwise
   * disables search field focusing for instance
   */
  isSubGrid: PropTypes.bool,
  /**
   *  If set to false the table is not sortable (Default true)
   */
  sortable: PropTypes.bool,
  /**
   * Map of custom cell renderers which might be specified in list form definition (`client-renderer` attribute)
   */
  cellRenderers: PropTypes.objectOf(PropTypes.func),
  /**
   * By default, all action of the form definitions are displayed. Change to false to hide them
   */
  showActions: PropTypes.bool
}

export default EntityListApp
export {
  Box,
  ColumnModal,
  combineSelection,
  entitiesListTransformer,
  formsApi,
  getActionBarContent,
  getTableSelectionStyle,
  preferencesUtil,
  columnsUtil,
  searchFormTypePropTypes,
  selectionActions,
  selectionReducer,
  selectionSagas,
  selectionStylePropType,
  SelectNumRows,
  showSelectionComponent,
  StyledActionWrapper,
  StyledListView,
  StyledSplitWrapper,
  validateSearchFields
}
