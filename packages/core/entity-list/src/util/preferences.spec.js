import {dragAndDrop} from 'tocco-util'

import {
  getPositions,
  changePosition,
  getPositionsPreferencesToSave,
  getPositionsFromColumns,
  getSorting,
  getSortingPreferencesToSave,
  getAdditionalSortingPreferencesToSave,
  getColumnPreferencesToSave,
  getColumns,
  getWidths,
  changeWidth,
  getWidthsPreferencesToSave,
  getLayouts,
  getLayoutPreferencesToSave,
  changeLayout
} from './preferences'

describe('entity-list', () => {
  describe('util', () => {
    describe('preferences', () => {
      describe('getPositions', () => {
        test('should return object with positions', () => {
          const preferences = {
            'User_list.firstname.position': '0',
            'User_list.lastname.position': '1',
            'User_list.relGender.position': '2',
            'User_list.relGender.hidden': 'false'
          }

          const expectedResult = {
            firstname: 0,
            lastname: 1,
            relGender: 2
          }

          expect(getPositions(preferences, 'User_list')).to.eql(expectedResult)
        })

        test('should handle multi step paths', () => {
          const preferences = {
            'User_list.relRegistration.relRegistration_status.pk.position': '0'
          }

          const expectedResult = {
            'relRegistration.relRegistration_status.pk': 0
          }

          expect(getPositions(preferences, 'User_list')).to.eql(expectedResult)
        })
      })

      describe('getPositionsFromColumns', () => {
        test('should return object with positions', () => {
          const columns = [
            {
              fixedPosition: true,
              id: 'selection'
            },
            {
              id: 'firstname'
            },
            {
              id: 'lastname'
            },
            {
              id: 'relGender'
            }
          ]

          const expectedResult = {
            firstname: 0,
            lastname: 1,
            relGender: 2
          }

          expect(getPositionsFromColumns(columns)).to.eql(expectedResult)
        })
      })

      describe('changePositions', () => {
        test('should set new order to positions for right drop position', () => {
          const positions = {
            firstname: 0,
            lastname: 1,
            relGender: 2,
            email: 3
          }

          const expectedResult = {
            firstname: 0,
            relGender: 1,
            lastname: 2,
            email: 3
          }

          expect(changePosition(positions, 'lastname', 'relGender', dragAndDrop.HorizontalDropPosition.Right)).to.eql(
            expectedResult
          )
        })

        test('should set new order to positions for left drop position', () => {
          const positions = {
            firstname: 0,
            lastname: 1,
            relGender: 2,
            email: 3
          }

          const expectedResult = {
            firstname: 0,
            relGender: 1,
            lastname: 2,
            email: 3
          }

          expect(changePosition(positions, 'relGender', 'lastname', dragAndDrop.HorizontalDropPosition.Left)).to.eql(
            expectedResult
          )
        })
      })

      describe('getPositionsPreferencesToSave', () => {
        test('should return a key value object with full path as key', () => {
          const positions = {
            firstname: 0,
            lastname: 1,
            relGender: 2,
            email: 3
          }

          const expectedResult = {
            'User_list.firstname.position': '0',
            'User_list.lastname.position': '1',
            'User_list.relGender.position': '2',
            'User_list.email.position': '3'
          }

          expect(getPositionsPreferencesToSave('User_list', positions)).to.eql(expectedResult)
        })
      })

      describe('getWidths', () => {
        test('should get widths from preferences', () => {
          const preferences = {
            'User_list.firstname.position': '0',
            'User_list.lastname.position': '1',
            'User_list.sortingField': 'firstname',
            'User_list.sortingDirection': 'asc',
            'User_list.firstname.width': '289'
          }

          const expectedWidths = {firstname: 289}
          expect(getWidths(preferences)).to.eql(expectedWidths)
        })
      })

      describe('changeWidth', () => {
        test('should get widths from preferences', () => {
          const newWidth = {
            field: 'field',
            width: 250
          }

          const widths = {otherField: 300}
          const expectedWidths = {otherField: 300, field: 250}

          expect(changeWidth(widths, newWidth.field, newWidth.width)).to.deep.equal(expectedWidths)
        })
      })

      describe('getWidthsPreferencesToSave', () => {
        test('should get widths from preferences', () => {
          const widths = {firstname: 289, lastname: 0}
          const formName = 'User_list'

          const expectedPreferences = {
            'User_list.firstname.width': '289',
            'User_list.lastname.width': '0'
          }
          expect(getWidthsPreferencesToSave(formName, widths)).to.eql(expectedPreferences)
        })
      })

      describe('getSorting', () => {
        test('should transform preferences to sorting', () => {
          const preferences = {
            'User_list.firstname.position': '5',
            'Principal_list.sortingField': 'first_field',
            'Principal_list.sortingDirection': 'asc',
            'Principal_list.sortingField.1': 'second_field',
            'Principal_list.sortingDirection.1': 'desc'
          }

          const expectedSorting = [
            {
              field: 'first_field',
              order: 'asc'
            },
            {
              field: 'second_field',
              order: 'desc'
            }
          ]

          expect(getSorting(preferences)).to.deep.equal(expectedSorting)
        })
      })

      describe('getSortingPreferencesToSave', () => {
        test('should transform sorting to preference', () => {
          const sorting = {
            field: 'field',
            order: 'asc'
          }
          const expectedPreference = {
            'User_list.sortingField': 'field',
            'User_list.sortingDirection': 'asc'
          }
          expect(getSortingPreferencesToSave('User_list', sorting)).to.deep.equal(expectedPreference)
        })
      })

      describe('getAdditionalSortingPreferencesToSave', () => {
        test('should transform sorting to preference', () => {
          const sorting = {
            field: 'field',
            order: 'asc'
          }
          const expectedPreference = {
            'User_list.sortingField.1': 'field',
            'User_list.sortingDirection.1': 'asc'
          }
          expect(getAdditionalSortingPreferencesToSave('User_list', sorting, 1)).to.deep.equal(expectedPreference)
        })
      })

      describe('getColumns', () => {
        test('should transform preferences to columns', () => {
          const preferences = {
            'User_list.firstname.position': '5',
            'Principal_list.sortingField': 'first_field',
            'Principal_list.sortingDirection': 'asc',
            'User_list.first_field.hidden': 'true',
            'User_list.second_field.hidden': 'false',
            'User_list.third_field.with.multiple.paths.hidden': 'false'
          }

          const expectedColumns = {
            first_field: false,
            second_field: true,
            'third_field.with.multiple.paths': true
          }

          expect(getColumns(preferences)).to.deep.equal(expectedColumns)
        })
      })

      describe('getColumnPreferencesToSave', () => {
        test('should transform columns to preference', () => {
          const columns = {
            first_field: true,
            second_field: false
          }
          const expectedPreference = {
            'User_list.first_field.hidden': 'false',
            'User_list.second_field.hidden': 'true'
          }
          expect(getColumnPreferencesToSave('User_list', columns)).to.deep.equal(expectedPreference)
        })
      })

      describe('getLayouts', () => {
        test('should transform preferences to layout', () => {
          const preferences = {
            'User_list.firstname.position': '5',
            'Principal_list.tableLayout.desktop': 'table',
            'Principal_list.tableLayout.mobile': 'tile'
          }

          const expectedLayout = {
            desktop: 'table',
            mobile: 'tile'
          }

          expect(getLayouts(preferences)).to.deep.equal(expectedLayout)
        })
      })
      describe('changeLayout', () => {
        test('should transform layouts to change one', () => {
          const layouts = {
            desktop: 'table',
            mobile: 'table'
          }
          const expectedLayouts = {
            desktop: 'table',
            mobile: 'tile'
          }

          expect(changeLayout(layouts, 'mobile', 'tile')).to.deep.equal(expectedLayouts)
        })
        test('should transform layouts to add one', () => {
          const layouts = {
            desktop: 'table'
          }
          const expectedLayouts = {
            desktop: 'table',
            mobile: 'tile'
          }

          expect(changeLayout(layouts, 'mobile', 'tile')).to.deep.equal(expectedLayouts)
        })
      })

      describe('getLayoutPreferencesToSave', () => {
        test('should transform layout to preference', () => {
          const device = 'mobile'
          const layout = 'tile'

          const expectedPreference = {
            'User_list.tableLayout.mobile': 'tile'
          }
          expect(getLayoutPreferencesToSave('User_list', device, layout)).to.deep.equal(expectedPreference)
        })
      })
    })
  })
})
