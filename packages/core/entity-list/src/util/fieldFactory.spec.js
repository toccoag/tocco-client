import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import fieldFactory from './fieldFactory'

describe('entity-list', () => {
  describe('util', () => {
    const getStore = () =>
      configureStore({
        reducer: () => ({
          formData: {navigationStrategy: {}, config: {configSelector: () => ({})}},
          input: {formName: 'User'},
          list: {}
        })
      })

    describe('fieldFactory', () => {
      test('should return FormattedValue', () => {
        const field = {
          id: 'firstname-field', // does not match path by intention (-> should use path to get data)
          path: 'firstname',
          dataType: 'string'
        }
        const entity = {
          firstname: 'Donald'
        }

        testingLibrary.renderWithStore(<>{fieldFactory(field, entity, IntlStub)}</>, {
          store: getStore()
        })

        expect(screen.queryByText('Donald')).to.exist
      })

      test('should return FormattedValue and add type specific props', () => {
        const field = {
          id: 'doc',
          path: 'doc',
          dataType: 'document'
        }
        const entity = {
          doc: {fileName: 'test.pdf', binaryLink: '/to/binary', thumbnailLink: '/to/thumbnail'}
        }

        testingLibrary.renderWithStore(<>{fieldFactory(field, entity, IntlStub)}</>, {
          store: getStore()
        })

        expect(screen.queryAllByRole('link')).to.have.length(2)
      })

      test('should return array with separator', () => {
        const field = {
          id: 'relSomething.xy',
          path: 'relSomething.xy',
          dataType: 'string'
        }
        const entity = {
          'relSomething.xy': ['V1', 'V2']
        }

        testingLibrary.renderWithStore(<>{fieldFactory(field, entity, IntlStub)}</>, {
          store: getStore()
        })

        expect(
          screen.getByText(
            (_content, element) => element.childNodes.length > 1 && element.textContent.startsWith('V1;\u00A0V2')
          )
        ).to.exist
      })

      test('should handle empty array as value', () => {
        const field = {
          id: 'relSomething.xy',
          path: 'relSomething.xy',
          dataType: 'string'
        }
        const entity = {
          'relSomething.xy': []
        }

        const {container} = testingLibrary.renderWithStore(<>{fieldFactory(field, entity, IntlStub)}</>, {
          store: getStore()
        })

        jestExpect(container.firstChild).toBeEmptyDOMElement()
      })

      test('should not return separator with only one value', () => {
        const field = {
          id: 'relSomething.xy',
          path: 'relSomething.xy',
          dataType: 'string'
        }
        const entity = {
          'relSomething.xy': ['V1']
        }

        testingLibrary.renderWithStore(<>{fieldFactory(field, entity, IntlStub)}</>, {
          store: getStore()
        })

        expect(screen.queryByText('V1')).to.exist
        expect(screen.queryByText(',')).to.not.exist
      })
    })
  })
})
