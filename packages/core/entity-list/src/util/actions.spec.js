import * as actions from './actions'

describe('entity-list', () => {
  describe('util', () => {
    describe('actions', () => {
      test('should handle query selection ', () => {
        const state = {
          input: {
            entityName: 'User',
            selectionStyle: 'multi'
          },
          selection: {
            query: {search: 'test'},
            queryCount: 99,
            selection: []
          },
          list: {
            sorting: [{field: 'test', order: 'asc'}]
          }
        }

        const expectedSelection = {
          entityName: 'User',
          type: 'QUERY',
          query: {search: 'test', sort: 'test asc'},
          count: 99
        }

        expect(actions.deriveSelectionFromState(state)).to.eql(expectedSelection)
      })

      test('should handle selection', () => {
        const state = {
          input: {
            entityName: 'User'
          },
          selection: {
            selection: [1, 2, 4]
          },
          list: {
            sorting: []
          }
        }

        const expectedSelection = {
          entityName: 'User',
          type: 'ID',
          ids: [1, 2, 4],
          query: {sort: ''},
          count: 3
        }

        expect(actions.deriveSelectionFromState(state)).to.eql(expectedSelection)
      })

      test('should return selection with multi_explicit', () => {
        const state = {
          input: {
            entityName: 'User',
            selectionStyle: 'multi_explicit'
          },
          selection: {
            selection: []
          },
          list: {
            sorting: []
          }
        }

        const expectedSelection = {
          entityName: 'User',
          type: 'ID',
          ids: [],
          query: {sort: ''},
          count: 0
        }

        expect(actions.deriveSelectionFromState(state)).to.eql(expectedSelection)
      })
    })
  })
})
