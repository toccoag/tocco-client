import styled from 'styled-components'
import {StyledButton, scale} from 'tocco-ui'

export const StyledColumnContentWrapper = styled.div`
  display: flex;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  > * {
    margin-right: ${scale.space(-2)};
  }
`

export const StyledActionWrapper = styled.span`
  display: inline-block;

  ${StyledButton} {
    display: inline-block;
    width: 100%;

    > * {
      overflow: hidden;
    }
  }
`

export const StyledSpan = styled.span`
  overflow: hidden;

  p {
    margin-block-start: 0; /* reset user agent style to prevent margin within table */
  }
`
