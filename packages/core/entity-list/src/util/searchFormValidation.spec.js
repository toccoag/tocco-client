import {form} from 'tocco-app-extensions'

import {validateSearchFields} from './searchFormValidation'

describe('entity-list', () => {
  describe('util', () => {
    describe('searchFormValidation', () => {
      describe('validateSearchFields', () => {
        const formDefinition = form.createSimpleForm({
          children: [
            form.createVerticalBox({
              children: [
                form.createFieldSet({
                  path: 'txtFulltext',
                  dataType: 'fulltext-search',
                  additionalFieldAttributes: {
                    minChars: 3
                  }
                }),
                form.createFieldSet({
                  path: 'relAddress_user.relAddress',
                  dataType: 'string',
                  additionalFieldAttributes: {
                    minChars: null
                  }
                }),
                form.createFieldSet({
                  path: 'firstname',
                  dataType: 'string'
                })
              ]
            })
          ]
        })

        test('should not return en error if nothing is set', () => {
          const values = {}

          const errors = validateSearchFields(values, formDefinition)
          expect(Object.keys(errors)).to.have.length(0)
        })

        test('should return an error for text-field that to not fulfill minimum length', () => {
          const values = {
            txtFulltext: 'ab'
          }

          const errors = validateSearchFields(values, formDefinition)
          expect(Object.keys(errors)).to.have.length(1)
          expect(errors).to.have.property('txtFulltext')
        })

        test('should not return an error for text-field that to fulfill minimum length', () => {
          const values = {
            txtFulltext: 'abc',
            'relAddress_user.relAddress': 'a'
          }

          const errors = validateSearchFields(values, formDefinition)
          expect(Object.keys(errors)).to.have.length(0)
        })
      })
    })
  })
})
