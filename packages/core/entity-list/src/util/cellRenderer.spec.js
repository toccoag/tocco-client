import {configureStore} from '@reduxjs/toolkit'
import {screen, waitFor} from '@testing-library/react'
import {appFactory} from 'tocco-app-extensions'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import cellRenderer from './cellRenderer'

describe('entity-list', () => {
  describe('util', () => {
    const getStore = () =>
      configureStore({
        reducer: () => ({
          formData: {navigationStrategy: {}, config: {configSelector: () => ({})}},
          input: {formName: 'User'},
          list: {
            lazyData: {
              displayExpressions: {User: {23: {myDisplayExpression: '<h1>test</h1>'}}}
            }
          }
        })
      })

    describe('cellRenderer', () => {
      test('should return a formattedValue for componentType field', () => {
        const field = {
          componentType: 'field',
          id: 'firstname-field', // does not match path by intention (-> should use path to get data)
          path: 'firstname',
          dataType: 'string'
        }
        const entity = {
          firstname: 'Donald'
        }

        const parent = {}

        testingLibrary.renderWithStore(<>{cellRenderer(field, entity, parent, IntlStub)}</>, {
          store: getStore()
        })

        expect(screen.queryByText('Donald')).to.exist
      })

      test('should return an html formattedValue for DisplayExpressions', async () => {
        const field = {
          componentType: 'display',
          id: 'myDisplayExpression'
        }
        const entity = {
          __key: '23'
        }
        const parent = {}

        testingLibrary.renderWithStore(<>{cellRenderer(field, entity, parent, IntlStub)}</>, {
          store: getStore()
        })

        await waitFor(() => {
          expect(screen.queryByText('test')).to.exist
        })
      })

      test('should return an action for componentType action', () => {
        const field = {
          componentType: 'action',
          id: 'myAction'
        }
        const entity = {__model: 'User', __key: '123'}
        const parent = {}
        const store = appFactory.createStore({actions: () => ({})}, [])

        testingLibrary.renderWithStore(<>{cellRenderer(field, entity, parent, IntlStub)}</>, {
          store
        })

        expect(screen.queryByRole('button')).to.exist
      })
    })
  })
})
