import {device as deviceUtils, dragAndDrop} from 'tocco-util'

export const getCurrentDevice = () => (deviceUtils.isPrimaryTouchDevice() ? 'mobile' : 'desktop')

export const getPositions = (preferences, formName) => {
  const fieldStartIndex = formName.length + 1
  const suffix = '.position'
  return Object.keys(preferences)
    .filter(key => key.startsWith(formName) && key.endsWith(suffix))
    .reduce((acc, key) => {
      const fieldName = key.substring(fieldStartIndex, key.length - suffix.length)
      return {
        ...acc,
        [fieldName]: Number(preferences[key])
      }
    }, {})
}

export const getPositionsFromColumns = columns =>
  columns
    .filter(c => !c.fixedPosition)
    .reduce(
      (acc, c, idx) => ({
        ...acc,
        [c.id]: idx
      }),
      {}
    )

export const changePosition = (positions, field, targetField, horizontalDropPosition) =>
  Object.keys(positions)
    .sort((a, b) => positions[a] - positions[b])
    .filter(key => key !== field)
    .reduce((acc, key) => {
      if (horizontalDropPosition === dragAndDrop.HorizontalDropPosition.Left) {
        return [...acc, ...(key === targetField ? [field] : []), key]
      } else if (horizontalDropPosition === dragAndDrop.HorizontalDropPosition.Right) {
        return [...acc, key, ...(key === targetField ? [field] : [])]
      }
      return [...acc, key]
    }, [])
    .reduce(
      (acc, key, idx) => ({
        ...acc,
        [key]: idx
      }),
      {}
    )

export const getPositionsPreferencesToSave = (formName, positions) =>
  Object.keys(positions).reduce(
    (acc, key) => ({
      ...acc,
      [`${formName}.${key}.position`]: positions[key].toString()
    }),
    {}
  )

export const getWidths = preferences =>
  Object.keys(preferences)
    .filter(key => key.endsWith('.width'))
    .reduce((acc, key) => {
      const fieldName = key.match(/.*\.([^.]+)\.width$/)
      return {
        ...acc,
        [fieldName[1]]: Number(preferences[key])
      }
    }, {})

export const changeWidth = (widths, field, width) => ({...widths, [field]: width})

export const getWidthsPreferencesToSave = (formName, widths) =>
  Object.keys(widths).reduce(
    (acc, key) => ({
      ...acc,
      [`${formName}.${key}.width`]: widths[key].toString()
    }),
    {}
  )

export const getSorting = preferences => {
  const keyRegex = /^.*\.sorting(Field|Direction)\.?([1-9]*)$/
  return Object.entries(preferences)
    .filter(([key]) => keyRegex.test(key))
    .map(([key, value]) => [transformSortingKey(key.match(keyRegex)), value])
    .reduce((acc, [key, value]) => {
      acc[key.index] = {
        ...acc[key.index],
        [key.field]: value
      }
      return acc
    }, [])
}

const transformSortingKey = keyMatch => ({
  field: transformSortingField(keyMatch[1]),
  index: keyMatch[2] ? parseInt(keyMatch[2]) : 0
})

const transformSortingField = field => (field === 'Direction' ? 'order' : field.toLowerCase())

export const getSortingPreferencesToSave = (formName, sorting) => ({
  [`${formName}.sortingField`]: sorting.field,
  [`${formName}.sortingDirection`]: sorting.order
})

export const getAdditionalSortingPreferencesToSave = (formName, sorting, index) => ({
  [`${formName}.sortingField.${index}`]: sorting.field,
  [`${formName}.sortingDirection.${index}`]: sorting.order
})

export const getColumns = preferences => {
  const keyRegex = /^[^.]+\.(.+)\.hidden$/
  return Object.entries(preferences)
    .filter(([key]) => keyRegex.test(key))
    .reduce((acc, [key, value]) => {
      const fieldName = key.match(keyRegex)[1]
      return {
        ...acc,
        // old preferences marked hidden columns as true, now we mark visible columns as true
        [fieldName]: value === 'false'
      }
    }, {})
}

export const getColumnPreferencesToSave = (formName, columns) =>
  Object.keys(columns).reduce(
    (acc, key) => ({
      ...acc,
      // old preferences marked hidden columns as true, now we mark visible columns as true
      [`${formName}.${key}.hidden`]: (!columns[key]).toString()
    }),
    {}
  )

export const getActualLimit = state => state.preferences.numOfRows || state.input.limit

export const getLayouts = preferences => {
  const keyRegex = /^.*\.tableLayout\.(desktop|mobile)$/
  return Object.entries(preferences)
    .filter(([key]) => keyRegex.test(key))
    .map(([key, value]) => [key.match(keyRegex)[1], value]) // get device from regex match
    .reduce((acc, [device, value]) => ({...acc, [device]: value}), [])
}

export const changeLayout = (layouts, device, layout) => ({
  ...layouts,
  [device]: layout
})

export const getLayoutPreferencesToSave = (formName, device, layout) => ({
  [`${formName}.tableLayout.${device}`]: layout
})
