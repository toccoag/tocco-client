import {sortColumnsForColumnModal} from './columns'

describe('entity-list', () => {
  describe('util', () => {
    describe('columns', () => {
      describe('sortColumnsForColumnModal', () => {
        test('should show all currently visible columns first', () => {
          const columns = [
            {id: 'a', hidden: true},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const formColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const positions = {}

          const expectedColumns = [
            {id: 'b', hidden: false},
            {id: 'c', hidden: false},
            {id: 'a', hidden: true}
          ]

          expect(sortColumnsForColumnModal(columns, formColumns, positions)).to.deep.eql(expectedColumns)
        })

        test('should sort visible columns by custom positions', () => {
          const columns = [
            {id: 'a', hidden: true},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const formColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const positions = {
            c: 1,
            b: 2
          }

          const expectedColumns = [
            {id: 'c', hidden: false},
            {id: 'b', hidden: false},
            {id: 'a', hidden: true}
          ]

          expect(sortColumnsForColumnModal(columns, formColumns, positions)).to.deep.eql(expectedColumns)
        })

        test('should sort visible columns by default positions as default', () => {
          const columns = [
            {id: 'a', hidden: true},
            {id: 'c', hidden: false},
            {id: 'b', hidden: false}
          ]
          const formColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const positions = {}

          const expectedColumns = [
            {id: 'b', hidden: false},
            {id: 'c', hidden: false},
            {id: 'a', hidden: true}
          ]

          expect(sortColumnsForColumnModal(columns, formColumns, positions)).to.deep.eql(expectedColumns)
        })

        test('should ignore default position when custom positions are applied', () => {
          const columns = [
            {id: 'b', hidden: false},
            {id: 'a', hidden: false},
            {id: 'c', hidden: false}
          ]
          const formColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]
          const positions = {
            c: 1,
            b: 2,
            a: 3
          }

          const expectedColumns = [
            {id: 'c', hidden: false},
            {id: 'b', hidden: false},
            {id: 'a', hidden: false}
          ]

          expect(sortColumnsForColumnModal(columns, formColumns, positions)).to.deep.eql(expectedColumns)
        })

        test('should hidden column in the correct order', () => {
          const columns = [
            {id: 'a', hidden: false},
            {id: 'c', hidden: false},
            {id: 'b', hidden: false}
          ]
          const formColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: true},
            {id: 'c', hidden: false}
          ]
          const positions = {}

          const expectedColumns = [
            {id: 'a', hidden: false},
            {id: 'b', hidden: false},
            {id: 'c', hidden: false}
          ]

          expect(sortColumnsForColumnModal(columns, formColumns, positions)).to.deep.eql(expectedColumns)
        })
      })
    })
  })
})
