import {screen} from '@testing-library/react'
import {expect} from 'chai'
import _omit from 'lodash/omit'
import {appFactory} from 'tocco-app-extensions'
import {testingLibrary} from 'tocco-test-util'

import entityListReducers, {sagas} from '../../modules/reducers'

import * as forms from './forms'

const reducers = {...entityListReducers, actions: () => ({})}

/* eslint-disable react/prop-types */
jest.mock('../../../../app-extensions/src/field/fieldFactory', () => (mapping, dataType) => ({formField}) => (
  <div data-testid={`field-${formField.id}`}>{formField.label}</div>
))
jest.mock('../../../../app-extensions/src/formData/index', () => ({
  addToStore: () => {},
  FormDataContainer: ({children}) => <div data-testid="form-data">{children}</div>
}))
/* eslint-enable react/prop-types */

describe('entity-list', () => {
  describe('util', () => {
    describe('api', () => {
      describe('forms', () => {
        describe('getSorting', () => {
          test('should return sorting array of table', () => {
            const sorting = [
              {
                field: 'user_nr',
                order: 'asc'
              }
            ]
            const formDefinition = {
              children: [
                {
                  layoutType: 'table',
                  componentType: 'table',
                  sorting
                }
              ]
            }
            const result = forms.getSorting(formDefinition)
            expect(result).to.eql(sorting)
          })

          test('should return empty array in case of no sorting', () => {
            const formDefinition = {
              children: [
                {
                  layoutType: 'table',
                  componentType: 'table'
                }
              ]
            }
            const result = forms.getSorting(formDefinition)
            expect(result).to.eql([])
          })
        })

        describe('getColumnDefinition', () => {
          test('should return an array', () => {
            const field1 = {id: 'name1', componentType: 'field', dataType: 'string', label: 'label'}
            const field2 = {id: 'name2', componentType: 'field', dataType: 'decimal', label: 'label'}

            const formDefinition = {
              children: [
                {
                  id: 'lb1',
                  label: 'label1',
                  useLabel: 'YES',
                  children: [field1],
                  sortable: true,
                  widthFixed: false,
                  width: null,
                  shrinkToContent: true,
                  sticky: true,
                  useAsTitle: true,
                  tileIcon: 'tag',
                  layoutScope: 'tile'
                },
                {
                  id: 'lb2',
                  label: 'label2',
                  children: [field2],
                  sortable: false,
                  widthFixed: true,
                  width: 500
                }
              ]
            }

            const result = forms.getColumnDefinition({table: formDefinition})

            const expectedColumnDefinition = [
              {
                label: 'label1',
                id: 'lb1',
                children: [field1],
                sorting: {sortable: true},
                resizable: true,
                alignment: 'left',
                shrinkToContent: true,
                sticky: true,
                useLabel: 'YES',
                useAsTitle: true,
                tileIcon: 'tag',
                layoutScope: 'tile'
              },
              {
                label: 'label2',
                id: 'lb2',
                children: [field2],
                sorting: {sortable: false},
                resizable: false,
                alignment: 'right',
                shrinkToContent: false,
                sticky: false,
                useLabel: undefined,
                useAsTitle: undefined,
                tileIcon: undefined,
                layoutScope: undefined
              }
            ]

            expect(result[0]).to.have.property('CellRenderer')
            expect(result.map(r => _omit(r, ['CellRenderer']))).to.eql(expectedColumnDefinition)
          })

          test('should ignore HIDDEN columns and hidden fields', () => {
            const field1 = {id: 'name1', componentType: 'field', label: 'label'}
            const field2Hidden = {id: 'name2', componentType: 'field', hidden: true, label: 'label'}

            const formDefinition = {
              children: [
                {
                  hidden: false,
                  id: 'lb1',
                  label: 'label1',
                  children: [field1],
                  sortable: true,
                  widthFixed: false,
                  width: null
                },
                {
                  hidden: false,
                  id: 'lb2',
                  label: 'label2',
                  children: [field2Hidden],
                  sortable: true,
                  widthFixed: false,
                  width: null
                },
                {
                  hidden: true,
                  id: 'lb3',
                  label: 'label3',
                  children: [field1],
                  sortable: true,
                  widthFixed: false,
                  width: null
                }
              ]
            }

            const result = forms.getColumnDefinition({table: formDefinition})

            expect(result).to.have.length(1)
            expect(result[0].id).to.eql('lb1')
          })

          test('should ignore columns hidden in preferences', () => {
            const displayableField = {id: 'name1', componentType: 'field', label: 'label'}
            const formDefinition = {
              children: [
                {
                  hidden: false,
                  id: 'lb1',
                  label: 'label1',
                  sortable: true,
                  widthFixed: false,
                  width: null,
                  children: [displayableField]
                },
                {
                  hidden: false,
                  id: 'lb2',
                  label: 'label2',
                  sortable: true,
                  widthFixed: false,
                  width: null,
                  children: [displayableField]
                }
              ]
            }

            const columnDisplayPreferences = {
              lb2: false
            }

            const result = forms.getColumnDefinition({table: formDefinition, columnDisplayPreferences})

            expect(result).to.have.length(1)
            expect(result[0].id).to.eql('lb1')
          })

          test('should set columns as not sortable if set', () => {
            const field1 = {id: 'name1', sortable: true, componentType: 'field', dataType: 'string', label: 'label'}
            const field2 = {id: 'name2', sortable: false, componentType: 'field', dataType: 'decimal', label: 'label'}

            const formDefinition = {
              children: [
                {
                  id: 'lb1',
                  label: 'label1',
                  useLabel: 'YES',
                  children: [field1],
                  sortable: true,
                  widthFixed: false,
                  width: null,
                  shrinkToContent: true
                },
                {
                  id: 'lb2',
                  label: 'label2',
                  children: [field2],
                  sortable: false,
                  widthFixed: true,
                  width: 500
                }
              ]
            }

            const result = forms.getColumnDefinition({table: formDefinition, sortable: false})

            expect(result[0].sorting.sortable).to.eql(false)
            expect(result[1].sorting.sortable).to.eql(false)

            const result2 = forms.getColumnDefinition({table: formDefinition, sortable: true})

            expect(result2[0].sorting.sortable).to.eql(true)
            expect(result2[1].sorting.sortable).to.eql(false)
          })
        })

        describe('getColumnCellRenderer', () => {
          test('should return fall back cellrenderer', () => {
            const contentField = {id: 'content-field', componentType: 'field', label: 'field label', path: 'path'}
            const fallbackAction = {
              id: 'fallback-action',
              componentType: 'action',
              label: 'action label',
              onlyShowOnEmptyColumn: true
            }
            const columnDefinition = {
              id: 'column',
              label: 'column label',
              children: [contentField, fallbackAction]
            }

            const renderer = forms.getColumnCellRenderer({columnDefinition})

            const resultWithData = renderer({
              rowData: {
                path: 'test'
              },
              column: columnDefinition
            })

            expect(resultWithData).to.have.length(1)

            const store = appFactory.createStore(reducers, sagas, {navigationStrategy: {}})
            const {rerender} = testingLibrary.renderWithStore(resultWithData[0], {store})
            expect(screen.queryByTestId('field-content-field')).to.exist

            const resultWithFallback = renderer({
              rowData: {
                path: null
              },
              column: columnDefinition
            })

            rerender(resultWithFallback[0])

            expect(resultWithFallback).to.have.length(1)
            expect(screen.queryByText('action label')).to.exist
          })

          test('should return default cell renderer', () => {
            const field = {id: 'content-field', componentType: 'field', label: 'field label', path: 'path'}
            const action = {id: 'fallback action', componentType: 'action', label: 'action label'}
            const columnDefinition = {
              id: 'column',
              label: 'column label',
              children: [field, action]
            }

            const renderer = forms.getColumnCellRenderer({columnDefinition})

            const store = appFactory.createStore(reducers, sagas, {navigationStrategy: {}})
            testingLibrary.renderWithStore(
              renderer({
                rowData: {
                  path: 'test'
                },
                column: columnDefinition
              }),
              {store}
            )
            expect(screen.queryByTestId('field-content-field')).to.exist
            expect(screen.queryByText('action label')).to.exist
          })

          test('should return custom cell renderer if defined', () => {
            const field = {id: 'name1', componentType: 'field', label: 'label'}
            const columnDefinition = {
              id: 'column',
              label: 'column label',
              children: [field],
              clientRenderer: 'my-test-renderer'
            }

            // eslint-disable-next-line react/prop-types
            const CustomComponent = ({text}) => <div>{text}</div>

            const cellRenderers = {
              'my-test-renderer': (rowData, column, defaultRenderer) => {
                return <CustomComponent text={rowData.text} />
              }
            }

            const renderer = forms.getColumnCellRenderer({columnDefinition, cellRenderers})

            const result = renderer({
              rowData: {
                text: 'foo'
              }
            })

            expect(result).to.eql(<CustomComponent text="foo" />)
          })
        })

        describe('getFields', () => {
          test('should return array of all fields but none more than once', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  layoutType: 'table',
                  children: [
                    {
                      hidden: false,
                      id: 'lb1',
                      label: 'Fullname',
                      useLabel: 'YES',
                      componentType: 'layout',
                      layoutType: 'vertical-box',
                      children: [
                        {
                          path: 'firstname',
                          dataType: 'text',
                          componentType: 'field',
                          hidden: false,
                          label: 'Firstname'
                        },
                        {path: 'lastname', dataType: 'text', componentType: 'field', hidden: false, label: 'Lastname'}
                      ],
                      sortable: true
                    },
                    {
                      hidden: false,
                      name: 'lb3',
                      label: null,
                      componentType: 'layout',
                      layoutType: 'vertical-box',
                      children: [
                        {
                          path: 'firstname',
                          dataType: 'text',
                          componentType: 'field',
                          hidden: false,
                          label: 'Firstname'
                        },
                        {path: 'email', dataType: 'text', componentType: 'field', hidden: false, label: 'Mail'}
                      ],
                      sortable: true
                    }
                  ]
                }
              ]
            }

            const result = forms.getFields(formDefinition)

            const expectedPaths = ['firstname', 'lastname', 'email']
            expect(result.paths).to.eql(expectedPaths)
          })

          test('should ignore actions and other fields', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  layoutType: 'table',
                  children: [
                    {
                      hidden: false,
                      id: 'box2',
                      label: null,
                      componentType: 'layout',
                      layoutType: 'vertical-box',
                      children: [
                        {
                          id: 'description',
                          path: 'description',
                          componentType: 'field'
                        },
                        {
                          componentType: 'action',
                          id: 'exampleSimpelAction'
                        },
                        {
                          componentType: 'field',
                          id: 'firstname',
                          path: 'firstname'
                        },
                        {
                          id: 'displayExpression',
                          componentType: 'display'
                        }
                      ],
                      sortable: true
                    }
                  ]
                }
              ]
            }

            const result = forms.getFields(formDefinition)
            const expectedResult = ['description', 'firstname']
            expect(result.paths).to.eql(expectedResult)
          })

          test('should return relationFields and displayExpressions', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  layoutType: 'table',
                  children: [
                    {
                      hidden: false,
                      id: 'box2',
                      label: null,
                      componentType: 'layout',
                      layoutType: 'vertical-box',
                      children: [
                        {
                          id: 'relUser',
                          path: 'relUser',
                          componentType: 'field',
                          dataType: 'single-select-box'
                        },
                        {
                          componentType: 'action',
                          id: 'exampleSimpelAction'
                        },
                        {
                          componentType: 'field',

                          id: 'firstname',
                          path: 'firstname'
                        },
                        {
                          id: 'display1',
                          componentType: 'display'
                        },
                        {
                          id: 'display2',
                          componentType: 'display'
                        },
                        {
                          id: 'relXy',
                          path: 'relXy',
                          componentType: 'field',
                          dataType: 'multi-remote-field'
                        }
                      ],
                      sortable: true
                    }
                  ]
                }
              ]
            }

            const result = forms.getFields(formDefinition)
            expect(result.relationFields).to.eql(['relUser', 'relXy'])
            expect(result.displayExpressionFields).to.eql(['display1', 'display2'])
          })
        })

        describe('getSelectable', () => {
          const getFormDefinition = (selectable, hasMainActionBar, hasAction) => ({
            children: [
              ...(hasMainActionBar
                ? [
                    {
                      id: 'main-action-bar',
                      componentType: 'action-bar',
                      children: [
                        ...(hasAction
                          ? [
                              {
                                id: 'output',
                                label: 'Ausgabe'
                              }
                            ]
                          : [])
                      ]
                    }
                  ]
                : []),
              {
                layoutType: 'table',
                componentType: 'table',
                selectable
              }
            ]
          })

          test('is selectable if selectable flag is true', () => {
            const result = forms.getSelectable(getFormDefinition(true, true, true), 'list')
            expect(result).to.be.true
          })

          test('is selectable even if there is no action', () => {
            const result = forms.getSelectable(getFormDefinition(true, false, false), 'list')
            expect(result).to.be.true
          })

          test('is not selectable if selectable flag is false', () => {
            const result = forms.getSelectable(getFormDefinition(false, false, false), 'list')
            expect(result).to.be.false
          })

          test('is not selectable even if there is an action', () => {
            const result = forms.getSelectable(getFormDefinition(false, true, true), 'list')
            expect(result).to.be.false
          })

          test('is selectable if selectable flag is not set and there is an action', () => {
            const result = forms.getSelectable(getFormDefinition(null, true, true), 'list')
            expect(result).to.be.true
          })

          test('is not selectable if selectable flag is not set and there is no action', () => {
            const result = forms.getSelectable(getFormDefinition(null, false, false), 'list')
            expect(result).to.be.false
          })

          test('is not selectable if selectable flag is not set and there is an empty main action bar', () => {
            const result = forms.getSelectable(getFormDefinition(null, true, false), 'list')
            expect(result).to.be.false
          })

          test('is always selectable for other scopes', () => {
            const result = forms.getSelectable(getFormDefinition(true, false, false), 'remotefield')
            expect(result).to.be.true
          })
        })

        describe('getClickable', () => {
          const getFormDefinition = clickable => ({
            children: [
              {
                layoutType: 'table',
                componentType: 'table',
                ...(clickable !== null ? {clickable} : {})
              }
            ]
          })

          test('should return clickable boolean of the form definition', () => {
            const result = forms.getClickable(getFormDefinition(true))
            expect(result).to.be.true
          })

          test('should return clickable boolean false of the form definition', () => {
            const result = forms.getClickable(getFormDefinition(false))
            expect(result).to.be.false
          })

          test('should return true if clickable not in definition', () => {
            const result = forms.getClickable(getFormDefinition(null))
            expect(result).to.be.true
          })
        })

        describe('getEndpoint', () => {
          const getFormDefinition = endpoint => ({
            children: [
              {
                layoutType: 'table',
                componentType: 'table',
                ...(endpoint !== null ? {endpoint} : {})
              }
            ]
          })

          test('should return endpoint', () => {
            const endpoint = 'nice2/rest/xc'
            const result = forms.getEndpoint(getFormDefinition(endpoint))
            expect(result).to.eql(endpoint)
          })

          test('should return null if endpoint is not defined', () => {
            const result = forms.getEndpoint(getFormDefinition(null))
            expect(result).to.be.null
          })

          test('should return null if endpoint is empty', () => {
            const result = forms.getEndpoint(getFormDefinition(''))
            expect(result).to.be.null
          })
        })

        describe('getSearchEndpoint', () => {
          const getFormDefinition = searchEndpoint => ({
            children: [
              {
                layoutType: 'table',
                componentType: 'table',
                ...(searchEndpoint !== null ? {searchEndpoint} : {})
              }
            ]
          })

          test('should return search endpoint', () => {
            const searchEndpoint = 'nice2/rest/xc'
            const result = forms.getSearchEndpoint(getFormDefinition(searchEndpoint))
            expect(result).to.eql(searchEndpoint)
          })

          test('should return null if search endpoint is not defined', () => {
            const result = forms.getSearchEndpoint(getFormDefinition(null))
            expect(result).to.be.null
          })

          test('should return null if search endpoint is empty', () => {
            const result = forms.getSearchEndpoint(getFormDefinition(''))
            expect(result).to.be.null
          })
        })

        describe('getConstriction', () => {
          const getFormDefinition = constriction => ({
            children: [
              {
                layoutType: 'table',
                componentType: 'table',
                ...(constriction !== null ? {constriction} : {})
              }
            ]
          })

          test('should return constriction', () => {
            const constriction = 'sample'
            const result = forms.getConstriction(getFormDefinition(constriction))
            expect(result).to.eql(constriction)
          })

          test('should return null if constriction is not defined', () => {
            const result = forms.getConstriction(getFormDefinition(null))
            expect(result).to.be.null
          })

          test('should return null if constriction is empty', () => {
            const result = forms.getConstriction(getFormDefinition(''))
            expect(result).to.be.null
          })
        })

        describe('changeParentFieldType', () => {
          test('should change the type of a parent field to single-select-box', () => {
            const searchFormDefinition = require('../../dev/test-data/searchFormDefinition.json')
            const result = forms.changeParentFieldType(searchFormDefinition, 'relUser_code1')
            const flatten = forms.getFormFieldFlat(result)
            expect(flatten.relUser_code1).to.eql('single-select-box')
          })

          test('should not change the type of any other field or the form structure', () => {
            const searchFormDefinition = require('../../dev/test-data/searchFormDefinition.json')
            const result = forms.changeParentFieldType(searchFormDefinition, 'relUser_code1')

            expect(result.children[0].children[0]).to.eql(searchFormDefinition.children[0].children[0])

            const flatten = forms.getFormFieldFlat(result)
            expect(flatten.txtFulltext).to.eql('fulltext-search')
            expect(flatten['relAddress_user.relAddress']).to.eql('fulltext-search')
            expect(flatten.relUser_code1).to.eql('single-select-box')
          })

          test('should not break if children property is not defined', () => {
            const form = {
              id: 'User_detail_relDummySubGrid_search',
              label: null,
              children: [
                {
                  id: 'box1',
                  componentType: 'layout',
                  layoutType: 'vertical-box',
                  hidden: false,
                  label: null,
                  useLabel: 'NO'
                }
              ],
              componentType: 'form',
              modelName: 'Dummy_entity'
            }

            const result = forms.changeParentFieldType(form, 'relUser')
            expect(result).to.deep.eql(form)
          })
        })
      })

      describe('getTableColumns', () => {
        const formDefinition = {
          children: [
            {
              componentType: 'table',
              children: [
                {
                  id: 'tile',
                  layoutScope: 'tile',
                  children: []
                },
                {
                  id: 'table',
                  layoutScope: 'table',
                  children: []
                },
                {
                  id: 'hidden',
                  hidden: true,
                  children: []
                },
                {
                  id: 'reverse',
                  children: [{path: 'reverse'}]
                },
                {
                  id: 'other',
                  children: [{path: 'other'}]
                }
              ]
            }
          ]
        }

        test('should return table columns', () => {
          const expectedResult = [
            {
              id: 'table',
              layoutScope: 'table',
              hidden: undefined,
              children: []
            },
            {
              id: 'hidden',
              hidden: true,
              children: []
            },
            {
              id: 'reverse',
              children: [{path: 'reverse'}],
              hidden: undefined
            },
            {
              id: 'other',
              children: [{path: 'other'}],
              hidden: undefined
            }
          ]
          const result = forms.getTableColumns(formDefinition)
          expect(result).to.eql(expectedResult)
        })

        test('should filter parent reverse relation', () => {
          const expectedResult = [
            {
              id: 'table',
              layoutScope: 'table',
              hidden: undefined,
              children: []
            },
            {
              id: 'hidden',
              hidden: true,
              children: []
            },
            {
              id: 'other',
              children: [{path: 'other'}],
              hidden: undefined
            }
          ]
          const result = forms.getTableColumns(formDefinition, {reverseRelationName: 'reverse'})
          expect(result).to.eql(expectedResult)
        })

        test('should hide columns according to preferences', () => {
          const expectedResult = [
            {
              id: 'table',
              layoutScope: 'table',
              hidden: undefined,
              children: []
            },
            {
              id: 'hidden',
              hidden: false,
              children: []
            },
            {
              id: 'reverse',
              children: [{path: 'reverse'}],
              hidden: undefined
            },
            {
              id: 'other',
              children: [{path: 'other'}],
              hidden: true
            }
          ]
          const result = forms.getTableColumns(formDefinition, null, {other: false, hidden: true})
          expect(result).to.eql(expectedResult)
        })
      })
    })
  })
})
