import _forOwn from 'lodash/forOwn'
import {FormattedMessage} from 'react-intl'
import {form} from 'tocco-app-extensions'
import {cache} from 'tocco-util'

export const validateSearchFields = (values, formDefinition) => {
  const errors = {}

  _forOwn(values, (value, fieldName) => {
    let fieldDefinition = cache.getObjectCache('fieldDefinition', fieldName)
    if (!fieldDefinition) {
      fieldDefinition = getFieldDefinition(form.transformFieldNameBack(fieldName), formDefinition)
      cache.addObjectCache('fieldDefinition', fieldName, fieldDefinition)
    }

    if (fieldDefinition) {
      const fieldErrors = validateField(value, fieldDefinition)
      if (fieldErrors) {
        errors[fieldName] = fieldErrors
      }
    }
  })

  return errors
}

const validateField = (value, {dataType, minChars}) => {
  const typeValidator = typeValidators[dataType]

  if (typeValidator) {
    return typeValidator(value, {...(minChars ? {minLength: minChars} : {})})
  }
  return null
}

const getFieldDefinition = (fieldName, formDefinition) => {
  return form.findInChildren(formDefinition, element => element.id === fieldName && !(element.children?.length > 0))[0]
}

const stringValidator =
  defaultMinLength =>
  (value, {minLength = defaultMinLength}) => {
    if (value.length > 0 && value.length < minLength) {
      return {
        minLength: [
          <FormattedMessage
            key="minLength"
            id="client.entity-list.searchFormValidationMinLength"
            values={{minLength}}
          />
        ]
      }
    }
  }

const typeValidators = {
  string: stringValidator(1),
  'fulltext-search': stringValidator(2)
}
