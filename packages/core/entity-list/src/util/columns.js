import _get from 'lodash/get'
import _isEmpty from 'lodash/isEmpty'

export const compareColumnsByPreferences = (columnA, columnB, positions) =>
  _get(positions, [columnA.id], Number.MAX_VALUE) - _get(positions, [columnB.id], Number.MAX_VALUE)

export const sortColumnsByPreferences = (columns, positions) =>
  [...columns].sort((a, b) => compareColumnsByPreferences(a, b, positions))

const compareColumnsByDefaultPosition = (columnA, columnB, formColumns) =>
  formColumns.findIndex(col => col.id === columnA.id) - formColumns.findIndex(col => col.id === columnB.id)

export const sortColumnsForColumnModal = (columns, formColumns, positions) =>
  [...columns].sort((a, b) => {
    const comparisonByVisibility = a.hidden - b.hidden

    const comparisonByPosition = _isEmpty(positions)
      ? compareColumnsByDefaultPosition(a, b, formColumns)
      : compareColumnsByPreferences(a, b, positions)

    return comparisonByVisibility === 0 ? comparisonByPosition : comparisonByVisibility
  })
