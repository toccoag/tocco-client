export const deriveSelectionFromState = state => {
  const {selection, input, list} = state

  const sortingString = list.sorting.map(({field, order}) => `${field} ${order}`).join(', ')

  if (selection.selection.length > 0 || input.selectionStyle === 'multi_explicit') {
    return {
      entityName: state.input.entityName,
      type: 'ID',
      ids: selection.selection,
      query: {sort: sortingString},
      count: selection.selection.length || 0
    }
  }

  return {
    entityName: state.input.entityName,
    type: 'QUERY',
    query: {...selection.query, sort: sortingString},
    count: selection.queryCount || 0
  }
}
