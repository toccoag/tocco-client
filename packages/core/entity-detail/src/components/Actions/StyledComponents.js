import styled from 'styled-components'
import {Button, themeSelector} from 'tocco-ui'

export const StyledSaveButton = styled(Button)`
  border: 1px solid transparent;
  ${({hasErrors, theme}) =>
    hasErrors &&
    `
      border-color: ${theme.colors.signal.danger};
      background-color: ${theme.colors.paper};
      && {
        color: ${theme.name === 'dark' ? theme.colors.text : theme.colors.signal.danger};
      }
      &:hover {
        background-color: ${theme.colors.signal.danger};
      }
  `}
  ${({theme}) => `
      &:hover {
        color: ${theme.name === 'dark' ? theme.colors.text : theme.colors.paper};
      }
  `}
  display: block;
`

export const StyledActionSpan = styled.span`
  cursor: pointer;
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  &:hover {
    color: ${themeSelector.color('secondaryLight')};
  }
  ${({marked, theme}) => marked && `color: ${theme.colors.secondary};`}
`
