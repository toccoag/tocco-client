import PropTypes from 'prop-types'
import {Popover} from 'tocco-ui'

import modes from '../../util/modes'
import ErrorItems from '../ErrorItems'

import {StyledSaveButton} from './StyledComponents'

const SaveButton = ({submitting, mode, hasErrors, formErrors, icon, readonly, label}) => {
  if (mode !== modes.CREATE && readonly) {
    return null
  }

  return (
    <Popover content={hasErrors ? <ErrorItems formErrors={formErrors} /> : null} placement="bottom">
      <StyledSaveButton
        data-cy="btn-detail-form_submit"
        id="detail-save_button"
        disabled={submitting}
        ink="primary"
        label={label}
        look={hasErrors ? 'flat' : 'raised'}
        pending={submitting}
        type="submit"
        hasErrors={hasErrors}
        icon={hasErrors ? 'floppy-disk-circle-xmark' : icon}
      />
    </Popover>
  )
}

SaveButton.propTypes = {
  submitting: PropTypes.bool,
  mode: PropTypes.oneOf(Object.values(modes)),
  hasErrors: PropTypes.bool,
  formErrors: PropTypes.object,
  icon: PropTypes.string,
  readonly: PropTypes.bool,
  label: PropTypes.string
}

export default SaveButton
