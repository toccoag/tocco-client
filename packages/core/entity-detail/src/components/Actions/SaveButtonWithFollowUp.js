import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Popover} from 'tocco-ui'

import {submitForm as submitFormAction} from '../../modules/entityDetail/actions'
import modes from '../../util/modes'
import ErrorItems from '../ErrorItems'

import {StyledSaveButton} from './StyledComponents'

const SaveButtonWithFollowUp = ({
  submitting,
  mode,
  hasErrors,
  icon,
  label,
  formErrors,
  submitForm,
  properties,
  readonly
}) => {
  if (mode !== modes.CREATE && readonly) {
    return null
  }

  return (
    <Popover content={hasErrors ? <ErrorItems formErrors={formErrors} /> : null} placement="bottom">
      <StyledSaveButton
        data-cy="btn-detail-form_advanced-submit"
        id="detail-advanced_save_button"
        disabled={submitting || hasErrors}
        look="raised"
        label={label}
        icon={icon}
        onClick={() => submitForm({action: properties?.action})}
      />
    </Popover>
  )
}

SaveButtonWithFollowUp.propTypes = {
  submitting: PropTypes.bool,
  mode: PropTypes.oneOf(Object.values(modes)),
  hasErrors: PropTypes.bool,
  formErrors: PropTypes.object,
  icon: PropTypes.string,
  label: PropTypes.string,
  submitForm: PropTypes.func.isRequired,
  properties: PropTypes.shape({
    action: PropTypes.string
  }),
  readonly: PropTypes.bool
}

const mapActionCreators = {
  submitForm: submitFormAction
}

export default connect(null, mapActionCreators)(SaveButtonWithFollowUp)
