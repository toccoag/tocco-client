import PropTypes from 'prop-types'
import styled from 'styled-components'
import EntityListApp from 'tocco-entity-list/src/main'
import {viewPersistor} from 'tocco-util'

import EntityDetailApp from '../../main'

const StyledListApp = styled.div`
  width: 100%;
`

const SubGrid = ({
  detailFormName,
  formField,
  appId,
  entityName,
  entityKey,
  limit = 25,
  showSearchForm,
  navigationStrategy,
  onRowClick,
  navigateToCreate,
  emitAction,
  locale,
  modifyFormDefinition,
  actionAppComponent,
  reportIds
}) => {
  const formBase = `${detailFormName}_${formField.formSuffix.replace('.', '_')}`

  const id = `${appId}-subgrid-${entityName}-${entityKey}-${formField.targetEntity}-${formField.reverseRelation}`

  return (
    <StyledListApp>
      <EntityListApp
        id={id}
        entityName={formField.targetEntity}
        formName={formBase}
        limit={limit}
        searchFormType={showSearchForm ? 'simple_advanced' : 'none'}
        navigationStrategy={navigationStrategy}
        showLink={true}
        onRowClick={e => {
          if (onRowClick) {
            onRowClick({
              id: e.id,
              gridName: formField.formSuffix,
              relationName: formField.path
            })
          }
        }}
        onNavigateToCreate={() => {
          navigateToCreate(formField.path)
        }}
        parent={{
          key: entityKey,
          model: entityName,
          ...(formField.reverseRelation
            ? {
                reverseRelationName: formField.reverseRelation,
                relationName: formField.path
              }
            : {disableRelationNavigation: true}),
          formSuffix: formField.formSuffix
        }}
        emitAction={emitAction}
        store={viewPersistor.viewInfoSelector(id).store}
        onStoreCreate={store => {
          viewPersistor.persistViewInfo(id, {store})
        }}
        scrollBehaviour="none"
        locale={locale}
        modifyFormDefinition={modifyFormDefinition}
        actionAppComponent={actionAppComponent}
        reportIds={reportIds}
        detailApp={EntityDetailApp}
        isSubGrid={true}
      />
    </StyledListApp>
  )
}

SubGrid.propTypes = {
  entityKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  entityName: PropTypes.string.isRequired,
  detailFormName: PropTypes.string.isRequired,
  formField: PropTypes.shape({
    path: PropTypes.string,
    formSuffix: PropTypes.string,
    targetEntity: PropTypes.string,
    reverseRelation: PropTypes.string
  }).isRequired,
  onRowClick: PropTypes.func,
  navigateToCreate: PropTypes.func.isRequired,
  showSubGridsCreateButton: PropTypes.bool,
  appId: PropTypes.string,
  showSearchForm: PropTypes.bool,
  limit: PropTypes.number,
  emitAction: PropTypes.func.isRequired,
  navigationStrategy: PropTypes.objectOf(PropTypes.func),
  locale: PropTypes.string,
  modifyFormDefinition: PropTypes.func,
  actionAppComponent: PropTypes.elementType,
  reportIds: PropTypes.arrayOf(PropTypes.string)
}

export default SubGrid
