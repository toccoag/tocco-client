import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import SubGrid from './SubGrid'

/* eslint-disable react/prop-types */
jest.mock('tocco-entity-list/src/main', () => props => (
  <div data-testid="entity-list">
    <div>entityName: {props.entityName}</div>
    <div>formName: {props.formName}</div>
    <div>limit: {props.limit}</div>
    <div>reverse: {props.parent.reverseRelationName}</div>
    <div>formSuffix: {props.parent.formSuffix}</div>
    <div>navigation: {props.parent.disableRelationNavigation ? 'false' : 'true'}</div>

    <button
      onClick={() => {
        props.onRowClick({id: 'testId'})
      }}
    >
      row click
    </button>
  </div>
))
jest.mock('../../main', () => () => <div data-testid="entity-detail" />)
/* eslint-enable react/prop-types */

describe('entity-detail', () => {
  describe('components', () => {
    describe('SubGrid', () => {
      const testProps = {
        detailFormName: 'User',
        formField: {
          path: 'relFoo',
          formSuffix: 'formSuffix',
          targetEntity: 'Foo',
          reverseRelation: 'relReverse'
        },
        navigateToCreate: () => {},
        showSubGridsCreateButton: false,
        dispatchEmittedAction: () => {},
        entityName: 'User',
        emitAction: () => {}
      }

      test('should render', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)
        expect(screen.getByTestId('entity-list')).to.exist
      })

      test('should render with entityName', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('entityName: Foo')).to.exist
      })

      test('should render with formName', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('formName: User_formSuffix')).to.exist
      })

      test('should render with default limit 25', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('limit: 25')).to.exist
      })

      test('should render with custom limit', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} limit={10} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('limit: 10')).to.exist
      })

      test('should render with rowClick', () => {
        const rowClickSpy = sinon.spy()
        testingLibrary.renderWithIntl(<SubGrid {...testProps} onRowClick={rowClickSpy} />)

        const buttonElement = screen.getByRole('button')
        fireEvent.click(buttonElement)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(rowClickSpy).to.have.been.calledWith({
          id: 'testId',
          gridName: 'formSuffix',
          relationName: 'relFoo'
        })
      })

      test('should add reverse relation to parent', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('reverse: relReverse')).to.exist
        expect(screen.getByText('navigation: true')).to.exist
      })

      test('should add form suffix to parent', () => {
        testingLibrary.renderWithIntl(<SubGrid {...testProps} />)

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('formSuffix: formSuffix')).to.exist
        expect(screen.getByText('navigation: true')).to.exist
      })

      test('should disable navigation without reverse relation', () => {
        testingLibrary.renderWithIntl(
          <SubGrid {...{...testProps, formField: {...testProps.formField, reverseRelation: ''}}} />
        )

        expect(screen.getByTestId('entity-list')).to.exist
        expect(screen.getByText('navigation: false')).to.exist
      })
    })
  })
})
