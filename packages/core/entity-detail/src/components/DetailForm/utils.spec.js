import {getEntityWritablePaths, hasWritablePath} from './utils'

describe('entity-detail', () => {
  describe('components', () => {
    describe('DetailForm', () => {
      describe('utils', () => {
        describe('hasWritablePath', () => {
          const defaultEntityPaths = {
            unique_id: {
              type: 'string',
              writable: false
            },
            label: {
              type: 'string',
              writable: true
            },
            description: {
              type: 'string',
              writable: true
            }
          }
          const defaultFieldDefinitions = [
            {
              path: 'unique_id',
              readonly: false
            },
            {
              path: 'label',
              readonly: false
            },
            {
              path: 'description',
              readonly: true
            }
          ]

          test('no entity paths', () => {
            const entityPaths = {}
            expect(hasWritablePath(entityPaths, defaultFieldDefinitions)).to.be.false
          })

          test('no writable entity paths', () => {
            const entityPaths = {
              unique_id: {
                type: 'string',
                writable: false
              }
            }
            expect(hasWritablePath(entityPaths, defaultFieldDefinitions)).to.be.false
          })

          test('no field', () => {
            const fieldDefinitions = []
            expect(hasWritablePath(defaultEntityPaths, fieldDefinitions)).to.be.false
          })

          test('no writable field', () => {
            const fieldDefinitions = [
              {
                path: 'unique_id',
                readonly: true
              }
            ]
            expect(hasWritablePath(defaultEntityPaths, fieldDefinitions)).to.be.false
          })

          test('no writable path', () => {
            expect(hasWritablePath(defaultEntityPaths, defaultFieldDefinitions)).to.be.true
          })
        })

        describe('getEntityWritablePaths', () => {
          test('simple paths', () => {
            const entityPaths = {
              firstname: {
                type: 'string',
                writable: true
              },
              lastname: {
                type: 'string',
                writable: false
              }
            }
            expect(getEntityWritablePaths(entityPaths)).to.be.eql(['firstname'])
          })

          test('nested paths', () => {
            const entityPaths = {
              'relAddress_user[publication]': {
                type: 'entity',
                writable: false,
                value: {
                  paths: {
                    relAddress: {
                      type: 'entity',
                      writable: false,
                      value: {
                        paths: {
                          city_c: {
                            type: 'string',
                            writable: true
                          },
                          zip_c: {
                            type: 'string',
                            writable: false
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            expect(getEntityWritablePaths(entityPaths)).to.be.eql(['relAddress_user[publication].relAddress.city_c'])
          })

          test('simple writable relation', () => {
            const entityPaths = {
              'relAddress_user[publication]': {
                type: 'entity',
                writable: true,
                value: null
              }
            }
            expect(getEntityWritablePaths(entityPaths)).to.be.eql(['relAddress_user[publication]'])
          })

          test('add all writable paths of a nested relation', () => {
            const entityPaths = {
              'relAddress_user[publication]': {
                type: 'entity',
                writable: true,
                value: {
                  paths: {
                    relAddress: {
                      type: 'entity',
                      writable: true,
                      value: null
                    }
                  }
                }
              }
            }
            expect(getEntityWritablePaths(entityPaths)).to.be.eql([
              'relAddress_user[publication]',
              'relAddress_user[publication].relAddress'
            ])
          })
        })
      })
    })
  })
})
