import _intersection from 'lodash/intersection'

export const getEntityWritablePaths = paths => {
  const simplePaths = Object.entries(paths)
    .filter(([, value]) => value.writable)
    .map(([key]) => key)

  const nestedPaths = Object.entries(paths)
    .filter(([, value]) => value.type === 'entity' && value.value !== null)
    .flatMap(([key, value]) => getEntityWritablePaths(value.value.paths).map(path => `${key}.${path}`))

  return [...simplePaths, ...nestedPaths]
}

export const hasWritablePath = (entityPaths, fieldDefinitions) => {
  const writableEntityPaths = getEntityWritablePaths(entityPaths)
  const writableFormPaths = fieldDefinitions.filter(f => !f.readonly).map(f => f.path)
  return _intersection(writableEntityPaths, writableFormPaths).length > 0
}
