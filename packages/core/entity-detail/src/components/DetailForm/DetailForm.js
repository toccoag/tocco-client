import PropTypes from 'prop-types'
import {useEffect, useMemo, useRef} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {scrollBehaviourPropType} from 'tocco-ui'
import {react as customHooks, env} from 'tocco-util'

import DetailFooterContainer from '../../containers/DetailFooterContainer'
import SubGrid from '../../util/detailView/fromFieldFactories/subGrid'
import {SaveButton, MarkButton, SaveButtonWithFollowUp} from '../Actions'

import {StyledForm} from './StyledComponents'
import {hasWritablePath} from './utils'

const DetailForm = props => {
  const {
    fireTouched,
    dirty,
    submitting,
    mode,
    formErrors,
    valid,
    submitForm,
    entity,
    formDefinition,
    fieldDefinitions,
    formValues,
    anyTouched,
    form: formName,
    customRenderedActions,
    scrollBehaviour,
    entityModel,
    labelPosition,
    hideFooter
  } = props

  const formEl = useRef(null)

  customHooks.useAutofocus(formEl)
  const formEventProps = form.hooks.useFormEvents({submitForm})

  useEffect(() => {
    fireTouched(dirty)
  }, [dirty, fireTouched])

  const arePathsReadonly = !hasWritablePath(entity.paths, fieldDefinitions)
  const allCustomRenderedActions = useMemo(
    () => ({
      save: actionDefinition => (
        <SaveButton
          submitting={submitting}
          mode={mode}
          hasErrors={!valid && anyTouched}
          formErrors={formErrors}
          readonly={arePathsReadonly}
          {...actionDefinition}
        />
      ),
      save_with_follow_up: actionDefinition => (
        <SaveButtonWithFollowUp
          submitting={submitting}
          mode={mode}
          hasErrors={!valid && anyTouched}
          formErrors={formErrors}
          readonly={arePathsReadonly}
          {...actionDefinition}
        />
      ),
      mark: actionDefinition => <MarkButton {...actionDefinition} />,
      ...customRenderedActions
    }),
    [submitting, mode, valid, anyTouched, formErrors, customRenderedActions, arePathsReadonly]
  )

  const showFooter = env.isInAdminEmbedded() && !hideFooter

  return (
    <StyledForm {...formEventProps} ref={formEl} scrollBehaviour={scrollBehaviour}>
      <form.FormBuilder
        entity={entity}
        formName={formName}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType={formDefinition.readOnly ? 'readonly' : 'editable'}
        mode={mode}
        componentMapping={{[form.componentTypes.SUB_TABLE]: SubGrid}}
        customRenderedActions={allCustomRenderedActions}
        entityModel={entityModel}
        labelPosition={labelPosition}
        readonly={mode !== 'create' && arePathsReadonly}
      />
      {showFooter && <DetailFooterContainer />}
    </StyledForm>
  )
}

DetailForm.propTypes = {
  mode: PropTypes.oneOf(['update', 'create']),
  entityModel: PropTypes.object,
  submitForm: PropTypes.func.isRequired,
  formDefinition: PropTypes.object.isRequired,
  fieldDefinitions: PropTypes.arrayOf(PropTypes.object).isRequired,
  entity: PropTypes.object.isRequired,
  form: PropTypes.string.isRequired,
  touch: PropTypes.func.isRequired,
  formValues: PropTypes.object,
  submitting: PropTypes.bool,
  formErrors: PropTypes.object,
  valid: PropTypes.bool,
  dirty: PropTypes.bool,
  lastSave: PropTypes.number,
  fireTouched: PropTypes.func.isRequired,
  anyTouched: PropTypes.bool,
  customRenderedActions: PropTypes.objectOf(PropTypes.func),
  scrollBehaviour: scrollBehaviourPropType,
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  hideFooter: PropTypes.bool
}

export default reduxForm({
  form: 'detailForm',
  destroyOnUnmount: false
})(DetailForm)
