import PropTypes from 'prop-types'
import {FormattedRelativeTime} from 'react-intl'
import {FormattedValue} from 'tocco-ui'
import {date as dateUtil} from 'tocco-util'

import {StyledTimestampRelativeValue, StyledTimestampValueWrapper} from './StyledComponents'

const TimestampValue = ({value}) => {
  const timeDiff = dateUtil.selectUnit(new Date(value))

  return (
    <StyledTimestampValueWrapper>
      <FormattedValue value={value} type="datetime" />
      <StyledTimestampRelativeValue>
        (<FormattedRelativeTime value={timeDiff.diffValue} unit={timeDiff.unit} />)
      </StyledTimestampRelativeValue>
    </StyledTimestampValueWrapper>
  )
}

TimestampValue.propTypes = {
  value: PropTypes.string.isRequired
}

export default TimestampValue
