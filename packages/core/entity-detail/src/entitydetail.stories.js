import EntityDetailApp from './main'

export default {
  title: 'Core/Entity Detail',
  component: EntityDetailApp,
  argTypes: {
    mode: {
      options: ['update', 'create'],
      control: {type: 'select'}
    },
    scrollBehaviour: {
      options: ['none', 'inline'],
      control: {type: 'select'}
    }
  }
}

const EntityDetailStory = args => <EntityDetailApp {...args} />

export const Story = EntityDetailStory.bind({})
Story.args = {
  entityName: 'User',
  entityId: '1',
  formName: 'User',
  mode: 'update',
  scrollBehaviour: 'inline'
}
