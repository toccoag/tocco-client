import {isDirty as formsIsDirtySelector} from 'redux-form'
import {channel} from 'redux-saga'
import {select, call, put, take, race} from 'redux-saga/effects'
import {notification} from 'tocco-app-extensions'
import {PendingChangesModal} from 'tocco-ui'

import {FORM_SUBMISSION_FAILED, FORM_SUBMITTED, submitForm} from '../entityDetail/actions'
import {FORM_ID} from '../entityDetail/sagas'

const ActionsWithNavigation = ['new', 'copy']
const ActionsWithSave = ['save_with_follow_up']
const hasNavigationInAction = ({id, fullscreen}) =>
  ActionsWithNavigation.includes(id) || ActionsWithSave.includes(id) || fullscreen
const canExecuteOnPendingChanges = definition => definition.canExecuteOnPendingChanges

export function* pendingChangesHandler({definition}) {
  const isDirtySelector = yield call(formsIsDirtySelector, FORM_ID)
  const isDirty = yield select(isDirtySelector)

  /**
   * Do not show pending changes prompt for actions which navigate to another page.
   * Router user confirmation does handle prompt in these cases already.
   */
  if (isDirty && !hasNavigationInAction(definition) && !canExecuteOnPendingChanges(definition)) {
    const confirmResponse = yield call(promptConfirm, definition.id)
    if (confirmResponse === 'continue') {
      return {abort: false}
    }
    if (confirmResponse === 'abort') {
      return {abort: true}
    }
    if (confirmResponse === 'save') {
      // save changes and continue then
      yield put(submitForm())
      const response = yield race({
        submitted: take(FORM_SUBMITTED),
        failed: take(FORM_SUBMISSION_FAILED)
      })

      // only continue when save was successful
      const abort = !response.submitted
      return {abort}
    }
  }

  return {
    abort: false
  }
}

export function* promptConfirm(id) {
  const answerChannel = yield call(channel)

  yield put(
    notification.modal(
      `action-pending-changes-${id}`,
      'client.entity-detail.confirmDirtyBeforeActionTitle',
      'client.entity-detail.confirmDirtyBeforeAction',
      ({close}) => {
        const onYes = () => {
          close()
          answerChannel.put('save')
        }
        const onNo = () => {
          close()
          answerChannel.put('continue')
        }
        return <PendingChangesModal onYes={onYes} onNo={onNo} />
      },
      true,
      () => answerChannel.put('abort')
    )
  )
  return yield take(answerChannel)
}
