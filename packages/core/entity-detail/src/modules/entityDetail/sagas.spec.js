import {actions as formActions, actionTypes as formActionTypes, isValid as isValidSelector} from 'redux-form'
import {all, call, put, select, takeEvery, takeLatest, take} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {
  actions as actionExtensions,
  externalEvents,
  form,
  remoteEvents,
  rest,
  reports,
  appFactory
} from 'tocco-app-extensions'
import {api, intl} from 'tocco-util'

import {createEntity, updateEntity} from '../../util/api/entities'
import modes from '../../util/modes'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

const FORM_ID = 'detailForm'

describe('entity-detail', () => {
  describe('modules', () => {
    describe('entityDetail', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(appFactory.INPUT_CHANGED, sagas.inputChanged),
                takeLatest(actions.LOAD_DETAIL_VIEW, sagas.loadDetailView),
                takeLatest(actions.UNLOAD_DETAIL_VIEW, sagas.unloadDetailView),
                takeLatest(actions.TOUCH_ALL_FIELDS, form.sagasUtils.touchAllFields, sagas.formSagaConfig),
                takeEvery(actions.SUBMIT_FORM, sagas.submitForm),
                takeEvery(actions.FIRE_TOUCHED, sagas.fireTouched),
                takeEvery(actions.NAVIGATE_TO_CREATE, sagas.navigateToCreate),
                takeEvery(remoteEvents.REMOTE_EVENT, sagas.remoteEvent),
                takeLatest(actions.NAVIGATE_TO_ACTION, sagas.navigateToAction),
                takeLatest(actions.UPDATE_MARKED, sagas.updateMarked),
                takeLatest(actionExtensions.actions.ACTION_INVOKED, sagas.reloadAfterAction),
                takeEvery(formActionTypes.CHANGE, sagas.fireChanged),
                takeEvery(formActionTypes.INITIALIZE, sagas.fireChanged)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadDetailView saga', () => {
          const entityId = 99
          const formName = 'UserSearch'
          const entityName = 'User'
          const model = {}
          const fieldDefinitions = []
          const formDefaultValues = {}
          const formValues = {}

          test('should fetch entity and set it in store', () => {
            const mode = 'update'

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {mode}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  fieldDefinitions
                ],
                [matchers.call.fn(sagas.loadData)]
              ])
              .call(sagas.loadData)
              .run()
          })

          test('should load create view', () => {
            const mode = 'create'
            const modifyEntityPaths = p => p

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {entityId, formName, entityName, mode, modifyEntityPaths}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  fieldDefinitions
                ],
                [matchers.call.fn(form.getDefaultValues, fieldDefinitions), formDefaultValues],
                [matchers.call.fn(form.entityToFormValues, {}, fieldDefinitions), formValues]
              ])
              .put(actions.setEntity({paths: {}, model: entityName}))
              .put(formActions.initialize(FORM_ID, formValues))
              .run()
          })

          test('should load create view with default values', () => {
            const mode = 'create'
            const defaultValues = [
              {id: 'firstname', value: 'Hans'},
              {id: 'shortname', value: 'Hansi'}
            ]
            const mockedFormDefaultValues = {lastname: 'Muster', shortname: 'Musti'}
            const expectedFormValues = {firstname: 'Hans', lastname: 'Muster', shortname: 'Hansi'}

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {entityId, formName, entityName, mode, defaultValues}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  fieldDefinitions
                ],
                [matchers.call.fn(form.getDefaultValues, fieldDefinitions), mockedFormDefaultValues],
                [matchers.call.fn(form.entityToFormValues, {}, fieldDefinitions), expectedFormValues]
              ])
              .put(actions.setEntity({paths: {}, model: entityName}))
              .put(formActions.initialize(FORM_ID, expectedFormValues))
              .run()
          })

          test('should load create view without modifyEntityPaths', () => {
            const mode = 'create'

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {entityId, formName, entityName, mode}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  fieldDefinitions
                ],
                [matchers.call.fn(form.getDefaultValues, fieldDefinitions), formDefaultValues],
                [matchers.call.fn(form.entityToFormValues, {}, fieldDefinitions), formValues]
              ])
              .put(actions.setEntity({paths: {}, model: entityName}))
              .put(formActions.initialize(FORM_ID, formValues))
              .run()
          })

          test('should start initial action', () => {
            const mode = 'update'
            const actionDefinition = {
              componentType: 'action',
              id: 'action-id'
            }
            const formDefinition = {
              children: [
                {
                  componentType: 'action-bar',
                  children: [
                    {
                      componentType: 'action-group',
                      children: [
                        {
                          componentType: 'action',
                          id: 'other'
                        },
                        actionDefinition
                      ]
                    }
                  ]
                }
              ]
            }
            const selection = {entityName, type: 'ID', ids: [entityId], count: 1}

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {mode, initialAction: 'action-id', entityName, entityId}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  {formDefinition}
                ],
                [matchers.call.fn(sagas.loadData)]
              ])
              .put(actionExtensions.actions.actionInvoke(actionDefinition, selection))
              .run()
          })

          test('should not call invalid initial action', () => {
            const mode = 'update'
            const actionDefinition = {
              componentType: 'action',
              id: 'action-id'
            }
            const formDefinition = {
              children: [
                {
                  componentType: 'action-bar',
                  children: [
                    {
                      componentType: 'action-group',
                      children: [
                        {
                          componentType: 'action',
                          id: 'other'
                        },
                        actionDefinition
                      ]
                    }
                  ]
                }
              ]
            }
            const selection = {entityName, type: 'ID', ids: [entityId], count: 1}

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {mode, initialAction: 'invalid', entityName, entityId}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  {formDefinition}
                ],
                [matchers.call.fn(sagas.loadData)]
              ])
              .not.put(actionExtensions.actions.actionInvoke(actionDefinition, selection))
              .run()
          })
          test('should not call removeDeleteButton action', () => {
            const mode = 'update'
            const actionDefinition = {
              componentType: 'action',
              id: 'action-id'
            }
            const formDefinition = {
              children: [
                {
                  componentType: 'action-bar',
                  children: [
                    {
                      componentType: 'action-group',
                      children: [
                        {
                          componentType: 'action',
                          id: 'other'
                        },
                        actionDefinition
                      ]
                    }
                  ]
                }
              ]
            }
            const selection = {entityName, type: 'ID', ids: [entityId], count: 1}

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {mode, initialAction: 'invalid', entityName, entityId}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  {formDefinition}
                ],
                [matchers.call.fn(sagas.loadData)]
              ])
              .not.call.fn(sagas.removeDeleteButton)
              .not.put(actionExtensions.actions.actionInvoke(actionDefinition, selection))
              .run()
          })

          test('should call removeDeleteButton action', () => {
            const mode = 'update'
            const actionDefinition = {
              componentType: 'action',
              id: 'action-id'
            }
            const formDefinition = {
              children: [
                {
                  componentType: 'action-bar',
                  children: [
                    {
                      componentType: 'action',
                      id: 'delete',
                      appId: 'delete'
                    },
                    {
                      componentType: 'output',
                      id: 'output',
                      appId: 'output'
                    }
                  ]
                }
              ]
            }
            const selection = {entityName, type: 'ID', ids: [entityId], count: 1}

            return expectSaga(sagas.loadDetailView)
              .provide([
                [select(sagas.inputSelector), {mode, initialAction: 'invalid', entityName, entityId}],
                [matchers.call.fn(sagas.loadEntityModel, entityName), model],
                [
                  matchers.call.fn(sagas.loadDetailFormDefinition, formName, mode, entityName, entityId),
                  {formDefinition}
                ],
                [matchers.call.fn(sagas.loadData)],
                [matchers.call.fn(sagas.removeDeleteButton)]
              ])
              .call.fn(sagas.removeDeleteButton)
              .not.put(actionExtensions.actions.actionInvoke(actionDefinition, selection))
              .run()
          })
        })

        describe('removeDeleteButton', () => {
          const formDefinition = {
            children: [
              {
                componentType: 'action-bar',
                children: [
                  {
                    componentType: 'action',
                    id: 'delete',
                    appId: 'delete'
                  },
                  {
                    componentType: 'output',
                    id: 'output',
                    appId: 'output'
                  }
                ]
              }
            ]
          }
          const expectedFormDefinition = {
            children: [
              {
                componentType: 'action-bar',
                children: [
                  {
                    componentType: 'output',
                    id: 'output',
                    appId: 'output'
                  }
                ]
              }
            ]
          }
          test('should remove the delete button', () => {
            const entityName = 'User'
            const entityId = 1
            const requestResponse = {
              status: 200,
              body: {
                entities: [
                  {
                    hasDeletePermission: false
                  }
                ]
              }
            }
            return expectSaga(sagas.removeDeleteButton, formDefinition)
              .provide([
                [select(sagas.inputSelector), {entityName, entityId}],
                [matchers.call.fn(rest.requestSaga), requestResponse]
              ])
              .put(actions.setFormDefinition(expectedFormDefinition))
              .run()
          })

          test('should remove the delete button for 403', () => {
            const entityName = 'User'
            const entityId = 1
            const requestResponse = {
              status: 403
            }
            return expectSaga(sagas.removeDeleteButton, formDefinition)
              .provide([
                [select(sagas.inputSelector), {entityName, entityId}],
                [matchers.call.fn(rest.requestSaga), requestResponse]
              ])
              .put(actions.setFormDefinition(expectedFormDefinition))
              .run()
          })

          test('should not remove the delete button', () => {
            const entityName = 'User'
            const entityId = 1
            const requestResponse = {
              status: 200,
              body: {
                entities: [
                  {
                    hasDeletePermission: true
                  }
                ]
              }
            }
            return expectSaga(sagas.removeDeleteButton, formDefinition)
              .provide([
                [select(sagas.inputSelector), {entityName, entityId}],
                [matchers.call.fn(rest.requestSaga), requestResponse]
              ])
              .not.put(actions.setFormDefinition(expectedFormDefinition))
              .run()
          })
        })

        describe('submitForm saga', () => {
          const dirtyFormValues = {}
          const flatEntity = {}
          const entity = {paths: {}}

          test('should call create submit', () => {
            const mode = modes.CREATE
            const fieldDefinitions = []

            return expectSaga(sagas.submitForm)
              .provide([
                [select(intl.localeSelector), 'de'],
                [select(sagas.inputSelector), {mode}],
                [select(sagas.entityDetailSelector), {fieldDefinitions}],
                [select(isValidSelector(FORM_ID)), true],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), dirtyFormValues],
                [matchers.call.fn(form.formValuesToFlattenEntity, dirtyFormValues, fieldDefinitions), flatEntity],
                [matchers.call.fn(api.toEntity, {}), entity],
                [matchers.call.fn(sagas.createFormSubmit)]
              ])
              .put.like({action: {type: formActions.startSubmit().type}})
              .call(sagas.createFormSubmit, entity, fieldDefinitions, undefined)
              .put(actions.setFormSubmitted())
              .run()
          })

          test('should call create submit with follow up', () => {
            const mode = modes.CREATE
            const followUp = {action: 'action id'}
            const fieldDefinitions = []

            return expectSaga(sagas.submitForm, {payload: {followUp}})
              .provide([
                [select(intl.localeSelector), 'de'],
                [select(sagas.inputSelector), {mode}],
                [select(sagas.entityDetailSelector), {fieldDefinitions}],
                [select(isValidSelector(FORM_ID)), true],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), dirtyFormValues],
                [matchers.call.fn(form.formValuesToFlattenEntity, dirtyFormValues, fieldDefinitions), flatEntity],
                [matchers.call.fn(api.toEntity, {}), entity],
                [matchers.call.fn(sagas.createFormSubmit)]
              ])
              .call(sagas.createFormSubmit, entity, fieldDefinitions, followUp)
              .run()
          })

          test('should call update submit', () => {
            const mode = modes.UPDATE
            const fieldDefinitions = []

            return expectSaga(sagas.submitForm)
              .provide([
                [select(intl.localeSelector), 'de'],
                [select(sagas.inputSelector), {mode}],
                [select(sagas.entityDetailSelector), {fieldDefinitions}],
                [select(isValidSelector(FORM_ID)), true],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), dirtyFormValues],
                [matchers.call.fn(form.formValuesToFlattenEntity, dirtyFormValues, fieldDefinitions), flatEntity],
                [matchers.call.fn(api.toEntity, {}), entity],
                [matchers.call.fn(sagas.updateFormSubmit)]
              ])
              .call(sagas.updateFormSubmit, entity, fieldDefinitions)
              .put(actions.setFormSubmitted())
              .run()
          })

          test('should ignore follow up on update submit', () => {
            const mode = modes.UPDATE
            const followUp = {action: 'action id'}
            const fieldDefinitions = []

            return expectSaga(sagas.submitForm, {payload: {followUp}})
              .provide([
                [select(intl.localeSelector), 'de'],
                [select(sagas.inputSelector), {mode}],
                [select(sagas.entityDetailSelector), {fieldDefinitions}],
                [select(isValidSelector(FORM_ID)), true],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), dirtyFormValues],
                [matchers.call.fn(form.formValuesToFlattenEntity, dirtyFormValues, fieldDefinitions), flatEntity],
                [matchers.call.fn(api.toEntity, {}), entity],
                [matchers.call.fn(sagas.updateFormSubmit)]
              ])
              .call(sagas.updateFormSubmit, entity, fieldDefinitions)
              .put(actions.setFormSubmitted())
              .run()
          })

          test('should handle thrown errors', () => {
            const mode = modes.UPDATE
            const fieldDefinitions = []
            const error = new Error('error')
            return expectSaga(sagas.submitForm)
              .provide([
                [select(sagas.inputSelector), {mode}],
                [select(sagas.entityDetailSelector), {fieldDefinitions}],
                [select(isValidSelector(FORM_ID)), true],
                [matchers.call.fn(sagas.submitValidate)],
                [matchers.call.fn(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), dirtyFormValues],
                [matchers.call.fn(form.formValuesToFlattenEntity, dirtyFormValues, fieldDefinitions), flatEntity],
                [matchers.call.fn(api.toEntity, {}), entity],
                [matchers.call.fn(sagas.updateFormSubmit), throwError(error)]
              ])
              .call(form.sagasUtils.handleSubmitError, sagas.formSagaConfig, error)
              .put(actions.setFormSubmissionFailed())
              .run()
          })

          test('should call handleInvalidForm on invalid form', () =>
            expectSaga(sagas.submitForm)
              .provide({
                select() {
                  return false
                }
              })
              .call(form.sagasUtils.handleInvalidForm, sagas.formSagaConfig)
              .not.call(sagas.updateFormSubmit, entity)
              .not.call(sagas.createFormSubmit, entity)
              .put(actions.setFormSubmissionFailed())
              .run())
        })

        describe('updateFormSubmit saga', () => {
          const entity = {key: '123', model: 'User', paths: {lastname: 'test'}}
          test('should call updateEntity, load data, show notification and set lastsaved', () => {
            const updateResponse = {status: 200}
            return expectSaga(sagas.updateFormSubmit, entity)
              .provide([
                [matchers.call.fn(updateEntity), updateResponse],
                [matchers.call.fn(sagas.loadData), null],
                [matchers.call.fn(sagas.showNotification), null]
              ])
              .call.like({fn: updateEntity})
              .call.like({fn: sagas.loadData})
              .call.like({fn: sagas.showNotification})
              .put.like({action: {type: formActions.stopSubmit().type}})
              .put.like({action: {type: actions.setLastSave().type}})
              .run()
          })

          test('should call delete event on 404 response', () => {
            const updateResponse = {status: 404}
            return expectSaga(sagas.updateFormSubmit, entity)
              .provide([[matchers.call.fn(updateEntity), updateResponse]])
              .call.like({fn: updateEntity})
              .not.call.like({fn: sagas.loadData})

              .put(externalEvents.fireExternalEvent('onEntityDeleted'))
              .run()
          })
        })

        describe('createFormSubmit saga', () => {
          const entity = {paths: {}}
          const fieldDefinitions = []
          const formDefinition = {}
          const createdEntityId = 99

          test('should call api and store response', () => {
            return expectSaga(sagas.createFormSubmit, entity, fieldDefinitions)
              .provide([
                [call(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), {formDefinition}],
                [call(createEntity, entity, fieldDefinitions, formDefinition), createdEntityId]
              ])
              .put(externalEvents.fireExternalEvent('onEntityCreated', {id: createdEntityId, followUp: undefined}))
              .call(sagas.showNotification, 'success', 'createSuccessfulTitle', 'createSuccessfulMessage')
              .run()
          })

          test('should include follow up', () => {
            const followUp = {action: 'action id'}
            return expectSaga(sagas.createFormSubmit, entity, fieldDefinitions, followUp)
              .provide([
                [call(form.sagasUtils.getCurrentEntityState, sagas.formSagaConfig), {formDefinition}],
                [call(createEntity, entity, fieldDefinitions, formDefinition), createdEntityId]
              ])
              .put(externalEvents.fireExternalEvent('onEntityCreated', {id: createdEntityId, followUp}))
              .call(sagas.showNotification, 'success', 'createSuccessfulTitle', 'createSuccessfulMessage')
              .run()
          })
        })

        describe('loadDetailFormDefinition saga', () => {
          test('should load formDefinition, save to store and return ', () => {
            const formName = 'User'
            const mode = 'update'
            const formDefinition = {}
            const fieldDefinitions = {}
            const modifyFormDefinition = formDef => formDef
            return expectSaga(sagas.loadDetailFormDefinition, formName, mode)
              .provide([
                [call(rest.fetchForm, formName, mode), formDefinition],
                [select(sagas.inputSelector), {modifyFormDefinition}],
                [call(form.getFieldDefinitions, formDefinition), fieldDefinitions]
              ])
              .put(actions.setFormDefinition(formDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .returns({formDefinition, fieldDefinitions})
              .run()
          })

          test('should handle report ids', () => {
            const entityName = 'Entity_name'
            const formName = 'User'
            const mode = 'update'
            const formDefinition = {children: []}
            const modifiedFormDefinition = {children: [{id: 'fake modified child'}]}
            const fieldDefinitions = {}
            const reportDefinitions = [{}]
            const label = 'Ausgabe'
            const modifyFormDefinition = formDef => formDef
            return expectSaga(sagas.loadDetailFormDefinition, formName, mode, entityName)
              .provide([
                [matchers.call.fn(rest.fetchForm, formName, mode), formDefinition],
                [select(sagas.inputSelector), {modifyFormDefinition, reportIds: ['report-id']}],
                [matchers.call.fn(form.getFieldDefinitions), fieldDefinitions],
                [matchers.call.fn(form.addReports), modifiedFormDefinition],
                [take(reports.SET_REPORTS), {payload: {reports: reportDefinitions}}],
                [select(sagas.textResourceSelector), {'client.actiongroup.output': label}]
              ])
              .put(reports.loadReports(['report-id'], entityName, 'detail'))
              .call.like({
                fn: form.addReports,
                args: [formDefinition, reportDefinitions, label]
              })
              .returns({formDefinition: modifiedFormDefinition, fieldDefinitions})
              .run()
          })
        })

        describe('submitValidate saga', () => {
          test('should call submitValidation', () => {
            const formValues = {firstname: 'test'}
            const initialValues = {firstname: 'test1'}
            const mode = 'update'
            const fieldDefinitions = []
            const formDefinition = {}

            return expectSaga(sagas.submitValidate)
              .provide([
                [
                  matchers.call.fn(form.sagasUtils.getCurrentEntityState),
                  {formValues, initialValues, mode, fieldDefinitions, formDefinition}
                ],
                [matchers.call.fn(form.submitValidation)]
              ])
              .call(form.submitValidation, formValues, initialValues, fieldDefinitions, formDefinition, mode)
              .run()
          })
        })

        describe('fireTouched saga', () => {
          test('should fire external event if state changed', () => {
            const gen = sagas.fireTouched(actions.fireTouched(true))

            expect(gen.next().value).to.eql(select(sagas.entityDetailSelector))
            expect(gen.next({touched: false}).value).to.eql(
              put(externalEvents.fireExternalEvent('onTouchedChange', {touched: true}))
            )
            expect(gen.next().value).to.eql(put(actions.setTouched(true)))

            expect(gen.next().done).to.be.true
          })

          test('should not fire external event if state did not change', () => {
            const gen = sagas.fireTouched(actions.fireTouched(true))

            expect(gen.next().value).to.eql(select(sagas.entityDetailSelector))
            expect(gen.next({touched: true}).done).to.be.true
          })
        })

        describe('loadData saga', () => {
          test('should fetch the entity and call form initialize', () => {
            const entityDetail = {
              entityModel: {markable: true},
              fieldDefinitions: [],
              formDefinition: {markable: true}
            }
            return expectSaga(sagas.loadData)
              .provide([
                [select(sagas.inputSelector), {modifyEntityPaths: p => p}],
                [select(sagas.entityDetailSelector), entityDetail],
                [matchers.fork.fn(sagas.loadMarked)],
                [matchers.call.fn(sagas.loadEntity), {paths: {}}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplays), {}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplayExpressions), {}]
              ])
              .fork.like({fn: sagas.loadMarked})
              .call.like({fn: sagas.loadEntity})
              .call.like({fn: form.entityToFormValues})
              .put.like({action: {type: formActions.initialize().type}})
              .run()
          })

          test('should fetch the entity and call form initialize without modifyEntityPaths', () => {
            const entityDetail = {
              entityModel: {markable: true},
              fieldDefinitions: [],
              formDefinition: {markable: true}
            }
            return expectSaga(sagas.loadData)
              .provide([
                [select(sagas.inputSelector), {}],
                [select(sagas.entityDetailSelector), entityDetail],
                [matchers.fork.fn(sagas.loadMarked)],
                [matchers.call.fn(sagas.loadEntity), {paths: {}}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplays), {}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplayExpressions), {}]
              ])
              .fork.like({fn: sagas.loadMarked})
              .call.like({fn: sagas.loadEntity})
              .call.like({fn: form.entityToFormValues})
              .put.like({action: {type: formActions.initialize().type}})
              .run()
          })

          test('should use old values for meta fields on soft reset', () => {
            const entityDetail = {
              entityModel: {markable: true},
              fieldDefinitions: [],
              formDefinition: {markable: true}
            }
            const initialValues = {
              version: 1,
              __version: 1,
              update_timestamp: 100,
              update_user: 'update user',
              field: 'initial value',
              other_field: 'value'
            }
            const expectedFormValues = {
              __key: 1,
              __model: 'model',
              __version: 1,
              version: 1,
              field: 'entity value',
              update_timestamp: 100,
              update_user: 'update user'
            }
            return expectSaga(sagas.loadData, false)
              .provide([
                [select(sagas.inputSelector), {}],
                [select(sagas.entityDetailSelector), entityDetail],
                [select(sagas.formInitialValueSelector, FORM_ID), initialValues],
                [matchers.fork.fn(sagas.loadMarked)],
                [
                  matchers.call.fn(sagas.loadEntity),
                  {key: 1, version: 2, model: 'model', paths: {field: {value: 'entity value'}}}
                ],
                [matchers.call.fn(sagas.enhanceEntityWithDisplays), {}],
                [matchers.call.fn(sagas.enhanceEntityWithDisplayExpressions), {}]
              ])
              .fork.like({fn: sagas.loadMarked})
              .call.like({fn: sagas.loadEntity})
              .call.like({fn: form.entityToFormValues})
              .put.like({action: {type: formActions.initialize().type, payload: expectedFormValues}})
              .run()
          })
        })

        describe('reloadAfterAction saga', () => {
          test('if selectionDeleted is false, refresh page', () => {
            const payload = {
              response: {
                flags: {
                  selectionDeleted: false
                }
              },
              definition: {
                id: 'otherActionId'
              }
            }

            return expectSaga(sagas.reloadAfterAction, {payload})
              .provide([
                [matchers.call.fn(sagas.loadData), {}],
                [select(sagas.inputSelector), {entityId: '1'}]
              ])
              .put(externalEvents.fireExternalEvent('onRefresh'))
              .put(actionExtensions.reevaluateActionCondition('1'))
              .run()
          })

          test('if selectionDeleted is undefined, refresh page', () => {
            const payload = {
              response: {flags: {}},
              definition: {
                id: 'otherActionId'
              }
            }

            return expectSaga(sagas.reloadAfterAction, {payload})
              .provide([
                [matchers.call.fn(sagas.loadData), {}],
                [select(sagas.inputSelector), {entityId: '1'}]
              ])
              .put(externalEvents.fireExternalEvent('onRefresh'))
              .put(actionExtensions.reevaluateActionCondition('1'))
              .run()
          })

          test('if selectionDeleted is false, fire onEntityDeleted', () => {
            const payload = {
              response: {
                flags: {
                  selectionDeleted: true
                }
              },
              definition: {
                id: 'otherActionId'
              }
            }

            return expectSaga(sagas.reloadAfterAction, {payload})
              .provide([[select(sagas.inputSelector), {}]])
              .put(externalEvents.fireExternalEvent('onEntityDeleted'))
              .run()
          })

          test('for action delete do nothing', () => {
            const payload = {
              response: {flags: {}},
              definition: {
                id: 'delete'
              }
            }

            return expectSaga(sagas.reloadAfterAction, {payload})
              .provide([[select(sagas.inputSelector), {}]])
              .not.put(externalEvents.fireExternalEvent('onRefresh'))
              .not.put(externalEvents.fireExternalEvent('onEntityDeleted'))
              .run()
          })
        })

        describe('navigateToCreate saga', () => {
          test('should call navigationStrategy', () => {
            const payload = {
              relationName: 'relUser'
            }

            const navigationStrategy = {
              navigateToCreateRelative: () => {}
            }

            return expectSaga(sagas.navigateToCreate, {payload})
              .provide([[select(sagas.inputSelector), {navigationStrategy}]])
              .call(navigationStrategy.navigateToCreateRelative, payload.relationName)
              .run()
          })
        })

        describe('navigateToAction saga', () => {
          test('should call external event navigateToAction', () => {
            const payload = {
              selection: {type: 'ID'},
              definition: {appId: 'input-edit'}
            }

            const navigationStrategy = {
              navigateToActionRelative: () => {}
            }

            return expectSaga(sagas.navigateToAction, {payload})
              .provide([[select(sagas.inputSelector), {navigationStrategy}]])
              .call(navigationStrategy.navigateToActionRelative, payload.definition, payload.selection)
              .run()
          })
        })

        describe('remoteEvent saga', () => {
          const deleteEventAction = remoteEvents.remoteEvent({
            type: 'entity-delete-event',
            payload: {
              entities: [
                {entityName: 'User', key: '1'},
                {entityName: 'Principal', key: '2'}
              ]
            }
          })

          test('should call external event onEntityDeleted', () => {
            const inputState = {
              entityName: 'User',
              entityId: '1'
            }
            return expectSaga(sagas.remoteEvent, deleteEventAction)
              .provide([[select(sagas.inputSelector), inputState]])
              .put(externalEvents.fireExternalEvent('onEntityDeleted'))
              .run()
          })

          test('should not call external event onEntityDeleted if irrelevant event', () => {
            const inputState = {
              entityName: 'Classroom',
              entityId: '99'
            }
            return expectSaga(sagas.remoteEvent, deleteEventAction)
              .provide([[select(sagas.inputSelector), inputState]])
              .not.put(externalEvents.fireExternalEvent('onEntityDeleted'))
              .run()
          })

          test('should call load date on entity update event', () => {
            const updateEventAction = remoteEvents.remoteEvent({
              type: 'entity-update-event',
              payload: {
                entities: [{entityName: 'User', key: '1'}]
              }
            })

            const inputState = {
              entityName: 'User',
              entityId: '1'
            }

            return expectSaga(sagas.remoteEvent, updateEventAction)
              .provide([
                [select(sagas.inputSelector), inputState],
                [matchers.call.fn(sagas.loadData), {paths: {}}]
              ])
              .call(sagas.loadData, false)
              .not.put(actionExtensions.reevaluateActionCondition('1'))
              .run()
          })

          test('should reload on entity update event', () => {
            const updateEventAction = remoteEvents.remoteEvent({
              type: 'entity-update-event',
              payload: {
                entities: [{entityName: 'User', key: '1'}],
                reload: true
              }
            })

            const inputState = {
              entityName: 'User',
              entityId: '1'
            }

            return expectSaga(sagas.remoteEvent, updateEventAction)
              .provide([
                [select(sagas.inputSelector), inputState],
                [matchers.call.fn(sagas.loadData), {paths: {}}]
              ])
              .call(sagas.loadData, true)
              .put(actionExtensions.reevaluateActionCondition('1'))
              .run()
          })

          test('should trigger external action event', () => {
            const triggerActionEvent = remoteEvents.remoteEvent({
              type: 'action-trigger-event',
              payload: {
                func: actions.setMarked,
                args: [true]
              }
            })

            const inputState = {
              entityName: 'User',
              entityId: '1'
            }

            return expectSaga(sagas.remoteEvent, triggerActionEvent)
              .provide([[select(sagas.inputSelector), inputState]])
              .put(actions.setMarked(true))
              .run()
          })
        })
      })
    })
  })
})
