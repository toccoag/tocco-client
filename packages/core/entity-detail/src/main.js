import PropTypes from 'prop-types'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  formData,
  keyDown,
  notification,
  form,
  reports
} from 'tocco-app-extensions'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {scrollBehaviourPropType} from 'tocco-ui'
import {navigationStrategy, reducer as reducerUtil} from 'tocco-util'

import {SaveButton} from './components/Actions'
import ErrorItems from './components/ErrorItems'
import DetailViewContainer from './containers/DetailViewContainer'
import customActions from './customActions'
import {pendingChangesHandler} from './modules/actions/sagas'
import {loadDetailView} from './modules/entityDetail/actions'
import reducers, {sagas, formSagaConfig} from './modules/reducers'
import shortcuts from './shortcuts'

const packageName = 'entity-detail'

const EXTERNAL_EVENTS = [
  /**
   * Is fired when a row of a sub grid is clicked
   *
   * Payload:
   * - `id` of the clicked record
   * - `gridName` name of the sub grid
   * - `relationName` name of the sub grid relation
   */
  'onSubGridRowClick',
  /**
   * Is fired when a a record got created
   *
   * Payload:
   * - `id` of the newly created record
   * - 'followUp' object containing actionId
   */
  'onEntityCreated',
  /**
   * Is fired when a a record got sucessfully updated
   */
  'onEntityUpdated',
  /**
   * Is fired when the loaded record got deleted
   */
  'onEntityDeleted',
  /**
   * This event is fired when form is initialized and on every field change
   *
   * Payload:
   * - `formValues` object with current form values
   * - `entity` entity object filled with current values
   */
  'onFormValuesChange',
  /**
   * This event is fired when the touched state changes
   *
   * Payload:
   * - `touched` boolean flag which indicates if the form is touched
   */
  'onTouchedChange',
  'onRefresh',
  'emitAction'
]

const initApp = (id, input, events, publicPath) => {
  const content = <DetailViewContainer />

  const defaultInput = {
    scrollBehaviour: 'inline'
  }

  const store = appFactory.createStore(reducers, sagas, {...defaultInput, ...input}, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store, state => state.input.emitAction)
  errorLogging.addToStore(store, false)
  notification.addToStore(store, false)
  form.addToStore(store, formSagaConfig)
  reports.addToStore(store)
  actions.addToStore(store, state => ({
    formApp: SimpleFormApp,
    listApp: state.input.listApp,
    detailApp: EntityDetailApp,
    customActions: {
      ...customActions,
      ...(state.input.customActions || {})
    },
    appComponent: state.input.actionAppComponent,
    navigationStrategy: state.input.navigationStrategy,
    context: {
      viewName: 'detail',
      formName: state.input.formName
    },
    customPreparationHandlers: [pendingChangesHandler],
    customActionEventHandlers: state.input.customActionEventHandlers
  }))
  formData.addToStore(store, state => ({
    listApp: state.input.listApp,
    detailApp: EntityDetailApp,
    docsApp: state.input.docsApp,
    navigationStrategy: state.input.navigationStrategy,
    chooseDocument: state.input.chooseDocument
  }))
  keyDown.addToStore(store, shortcuts)

  const app = appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [loadDetailView()],
    publicPath,
    textResourceModules: ['actiongroup', 'component', 'common', 'entity-list', 'entity-detail']
  })

  if (module.hot) {
    module.hot.accept('./modules/reducers', () => {
      const hotReducers = require('./modules/reducers').default
      reducerUtil.hotReloadReducers(app.store, hotReducers)
    })
  }

  return app
}

;(() => {
  if (__DEV__ && __PACKAGE_NAME__ === 'entity-detail') {
    const input = require('./dev/input.json')

    const app = initApp(packageName, input)
    appFactory.renderApp(app.component)
  } else {
    appFactory.registerAppInRegistry(packageName, initApp)
  }
})()

const EntityDetailApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName: props.id, externalEvents: EXTERNAL_EVENTS})
  // Fragments-Workaround to support propTypes in Storybook
  return <>{component}</>
}

EntityDetailApp.propTypes = {
  /**
   * Entity which is represented by the detail view.
   */
  entityName: PropTypes.string.isRequired,
  /**
   * The id of the visible entity (`undefined` for `create` mode otherwise required)
   */
  entityId: PropTypes.string,
  /**
   * Detail form name without the scope (e.g. `'Widget'` as `formName` will load `'Widget_detail.xml'`)
   */
  formName: PropTypes.string.isRequired,
  /**
   * Defines the mode of the detail form.
   */
  mode: PropTypes.oneOf(['update', 'create']),
  /**
   * Array of object with attributes id, value.Only for Create mode
   * e.g. [{id: 'lastname', value: 'Simpson}, {id:'relGender', value: '1'}]
   */
  defaultValues: PropTypes.array,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {}),
  /**
   * Object consisting of various link factories. For more information see tocco-util/navigationStrategy documentation.
   */
  navigationStrategy: navigationStrategy.propTypes,
  chooseDocument: PropTypes.func,
  /**
   * Component to render custom actions. Needs the appId and selection object property
   */
  actionAppComponent: PropTypes.elementType,
  /**
   * Map of custom action handlers.
   * Note that this prop should only be used if you have an action without a component
   * (i.e. an action that only puts a redux action to trigger a saga).
   * If rendering of a component is involved (like it is in most cases), you should use `actionAppComponent`.
   */
  customActions: PropTypes.object,
  /**
   * Map of custom action components.
   * Note that this prop should only be used if you have an action without a component
   * and without any pre-check conditions.
   */
  customRenderedActions: PropTypes.objectOf(PropTypes.func),
  /**
   * Function to modify the form definitions before rendering the forms.
   */
  modifyFormDefinition: PropTypes.func,
  /**
   * Function to modify the entity paths. Useful when new pseudeo form fields has been added.
   */
  modifyEntityPaths: PropTypes.func,
  /**
   * Array of report ids to display on the detail in the main action bar.
   */
  reportIds: PropTypes.arrayOf(PropTypes.string),
  /**
   * List app (tocco-entity-list) must be provided to support remote fields
   */
  listApp: PropTypes.func,
  /**
   * Docs app (tocco-docs-browser) must be provided to support remote fields
   */
  docsApp: PropTypes.func,
  /**
   * Defines if the entity detail is taking as much space as needed (`'none'`) or if it will fit into
   * its outer container (`'inline'`). When set to `'inline'` the outer container has to have e predefined height.
   */
  scrollBehaviour: scrollBehaviourPropType,
  /**
   * Defines whether the form field labels should be inside or outside (with or without responsiveness) of the field.
   * The posisiton 'outside-responsive' will show the label left on wide screen and on top on small screens.
   */
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  /**
   * id of action to start after loading data
   */
  initialAction: PropTypes.string,
  /**
   * Defines if the footer is visible. Per default the footer is only visible if the embed type is admin.
   */
  hideFooter: PropTypes.bool
}

export default EntityDetailApp
export {SaveButton, ErrorItems}
