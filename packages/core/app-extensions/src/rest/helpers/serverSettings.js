import _omit from 'lodash/omit'
import {call} from 'redux-saga/effects'
import {cache} from 'tocco-util'

import {requestSaga} from '../rest'

/**
 * To fetch server settings containing:
 * runEnv
 * captchaKey
 */
export function* fetchServerSettings() {
  const settingsResponse = yield call(requestSaga, 'client/settings')
  const settings = _omit(settingsResponse.body, ['_links'])
  return settings
}

export function* fetchServerRevision() {
  const settingsResponse = yield call(requestSaga, 'client/settings')
  const settings = _omit(settingsResponse.body, ['_links'])
  return settings.niceRevision
}

export function* fetchServerMaxLimit() {
  const cachedLimit = cache.getObjectCache('server', 'maxLimit')
  if (cachedLimit) {
    return cachedLimit
  } else {
    const {maxLimit} = yield call(fetchServerSettings)
    cache.addObjectCache('server', 'maxLimit', maxLimit)
    return maxLimit
  }
}
