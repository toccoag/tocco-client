import {call} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {cache} from 'tocco-util'

import {requestSaga} from '../rest'

import {fetchServerMaxLimit, fetchServerSettings} from './serverSettings'

describe('app-extensions', () => {
  describe('rest', () => {
    describe('helpers', () => {
      describe('serverSettings', () => {
        describe('fetchServerSettings', () => {
          test('should return settings from server', () => {
            const settings = {runEnv: 'TEST', captchaKey: 'xaho34nLKN'}
            return expectSaga(fetchServerSettings)
              .provide([[call(requestSaga, 'client/settings'), {body: settings}]])
              .returns(settings)
              .run()
          })
        })
        describe('fetchServerMaxLimit', () => {
          beforeEach(() => {
            cache.clearObjectCache()
          })
          test('should cache max limit from server settings', async () => {
            const settings = {maxLimit: 1000}
            return expectSaga(fetchServerMaxLimit)
              .provide([[matchers.call.fn(fetchServerSettings), settings]])
              .returns(1000)
              .run()
              .then(() => {
                expect(cache.getObjectCache('server', 'maxLimit')).to.eql(1000)
              })
          })
          test('should use cache if exists', () => {
            cache.addObjectCache('server', 'maxLimit', 1000)
            return expectSaga(fetchServerMaxLimit).not.call.fn(fetchServerSettings).returns(1000).run()
          })
        })
      })
    })
  })
})
