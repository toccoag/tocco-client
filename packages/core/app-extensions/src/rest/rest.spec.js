import fetchMock from 'fetch-mock'
import {call, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {throwError} from 'redux-saga-test-plan/providers'
import {env, intl} from 'tocco-util'
import {v4 as uuid} from 'uuid'

import {blockingInfo, removeBlockingInfo} from '../notification/modules/blocking/actions'
import {toaster} from '../notification/modules/toaster/actions'

import {handleClientQuestion} from './clientQuestions'
import InformationError from './InformationError'
import {sendRequest} from './request'
import {getParameterString, prepareRequest, requestSaga, setLocale, simpleRequest, prepareBlockingInfo} from './rest'

describe('app-extensions', () => {
  describe('rest', () => {
    describe('getParameterString', () => {
      test('should order params', () => {
        const params = {
          param1: 'val1',
          param3: 'val3',
          param2: 'val2'
        }
        const res = getParameterString(params)

        expect(res).to.eql('?param1=val1&param2=val2&param3=val3')
      })

      test('should remove empty params', () => {
        const params = {
          param1: '',
          param2: 'val2'
        }
        const res = getParameterString(params)

        expect(res).to.eql('?param2=val2')
      })

      test('should decode string', () => {
        const params = {
          param1: '%'
        }
        const res = getParameterString(params)

        expect(res).to.eql('?param1=%25')
      })

      test('should handle arrays string', () => {
        const params = {
          param1: ['1', '2']
        }
        const res = getParameterString(params)

        expect(res).to.eql('?param1=1&param1=2')
      })

      test('should handle undefined params', () => {
        const res = getParameterString(undefined)

        expect(res).to.eql('')
      })
    })

    describe('simpleRequest', () => {
      beforeEach(() => {
        fetchMock.hardReset()
        fetchMock.mockGlobal()
      })

      test('should return with accepted errorCode', () => {
        const statusCode = 400
        const bodyObj = {
          errorCode: 'SAVE_FAILED',
          errors: {
            firstname: {
              illegal: ['SPEICHERFEHLER: Vorname tocco1 nicht toleriert.']
            }
          }
        }
        const body = new Blob([JSON.stringify(bodyObj, null, 2)], {type: 'application/json'})
        const mockedResponse = new Response(body, {status: statusCode})

        fetchMock.get('*', mockedResponse)
        const resource = 'entities/2.0/Contact'
        const options = {
          acceptedErrorCodes: ['SAVE_FAILED']
        }

        return simpleRequest(resource, options).should.be.rejectedWith(mockedResponse)
      })

      test('should trow exception on unaccepted errorCode', done => {
        const statusCode = 400
        const bodyObj = {
          errorCode: 'SAVE_FAILED'
        }

        const body = new Blob([JSON.stringify(bodyObj, null, 2)], {type: 'application/json'})
        const mockedResponse = new Response(body, {status: statusCode, statusText: 'Some error'})

        fetchMock.get('*', mockedResponse)
        const resource = 'entities/2.0/Contact'
        simpleRequest(resource).catch(() => {
          done()
        })
      })

      test('should set content type header', async () => {
        fetchMock.get('*', {})

        await simpleRequest('', {body: undefined})

        const headers = fetchMock.callHistory.lastCall().options.headers
        expect(headers.randomxyxc).to.be.undefined
        expect(headers['content-type']).to.eql('application/json')
      })

      test('should set content type header if body is FormData (browser will set content type)', async () => {
        fetchMock.post('*', {})

        await simpleRequest('', {method: 'POST', body: new FormData()})

        const headers = fetchMock.callHistory.lastCall().options.headers
        expect(headers['content-type']).to.be.undefined
      })

      test('should set business unit header', async () => {
        fetchMock.get('*', {})

        await simpleRequest('')
        const headers = fetchMock.callHistory.lastCall().options.headers
        expect(headers['x-business-unit']).to.be.undefined

        env.setBusinessUnit('my_test_bu')
        simpleRequest('')
        env.setBusinessUnit(null)

        const headers2 = fetchMock.callHistory.lastCall().options.headers
        expect(headers2['x-business-unit']).to.eql('my_test_bu')
      })

      test('should use ordered params', async () => {
        fetchMock.get('*', {})
        const resource = 'entities/2.0/Contact'
        const options = {
          queryParams: {
            _search: 'test',
            xyz: 'abc'
          }
        }
        await simpleRequest(resource, options)

        const lastCall = fetchMock.callHistory.lastCall()
        expect(lastCall.url).to.eql('http://localhost/nice2/rest/entities/2.0/Contact?_search=test&xyz=abc')
      })

      test('should return with accepted status code', done => {
        const statusCode = 400

        const body = new Blob(['{}'], {type: 'application/json'})
        const mockedResponse = new Response(body, {status: statusCode})

        fetchMock.get('*', mockedResponse)
        const resource = 'entities/2.0/Contact'
        simpleRequest(resource, {
          acceptedStatusCodes: [400]
        }).then(response => {
          expect(response.status).to.eql(statusCode)
          done()
        })
      })

      test('should trow exception on unaccepted status code', done => {
        const statusCode = 400

        const body = new Blob(['{}'], {type: 'application/json'})
        const mockedResponse = new Response(body, {status: statusCode})

        fetchMock.get('*', mockedResponse)
        const resource = 'entities/2.0/Contact'
        simpleRequest(resource).catch(() => {
          done()
        })
      })
    })

    describe('requestSaga', () => {
      test('should call prepareRequest, blockingInfo, handleClientQuestions and sendRequest', () => {
        const resource = 'entities/2.0/Contact'
        const options = {
          queryParams: {
            _search: 'test',
            xyz: 'abc'
          },
          method: 'POST',
          body: {
            foo: 'bar'
          },
          acceptedErrorCodes: ['MY_ERROR_CODE'],
          acceptedStatusCodes: [400]
        }

        const gen = requestSaga(resource, options)

        expect(gen.next().value).to.eql(call(setLocale, options))

        expect(gen.next().value).to.eql(call(prepareRequest, resource, options))

        const requestData = prepareRequest(resource, options)

        expect(gen.next(requestData).value).to.eql(call(prepareBlockingInfo, options))

        expect(gen.next({}).value).to.eql(
          call(
            sendRequest,
            requestData.url,
            requestData.options,
            options.acceptedErrorCodes,
            options.acceptedStatusCodes
          )
        )

        const resp = {}

        expect(gen.next(resp).value).to.eql(call(handleClientQuestion, resp, requestData, options, {}))

        const next = gen.next(resp)

        expect(next.value).to.equal(resp) // expect same (not just equal)
        expect(next.done).to.be.true
      })

      test('should notify about unexpected information errors', () => {
        const resource = 'entities/2.0/Contact'
        const options = {
          method: 'POST',
          body: {
            foo: 'bar'
          }
        }

        const error = new InformationError('message')
        return expectSaga(requestSaga, resource, options)
          .provide([
            [select(intl.localeSelector), 'fr'],
            [matchers.call.fn(prepareRequest), {}],
            [matchers.call.fn(sendRequest), throwError(error)]
          ])
          .put(
            toaster({
              type: 'info',
              title: 'client.common.information',
              body: 'message'
            })
          )
          .run()
      })

      test('should put blocking info actions', () => {
        const randomId = 'uuid'
        const blockingInfoTitle = 'blocking info title'
        const resource = 'entities/2.0/Contact'
        const options = {
          method: 'POST',
          body: {
            foo: 'bar'
          },
          blockingInfoTitle
        }

        return expectSaga(requestSaga, resource, options)
          .provide([
            [select(intl.localeSelector), 'fr'],
            [matchers.call.fn(prepareRequest), {}],
            [matchers.call.fn(sendRequest), {}],
            [matchers.call.fn(handleClientQuestion), {}],
            [matchers.call.fn(uuid), randomId]
          ])
          .put(blockingInfo(randomId, blockingInfoTitle))
          .put(removeBlockingInfo(randomId))
          .run()
      })

      test('should put remove blocking info action on error', () => {
        const blockingInfoTitle = 'blocking info title'
        const resource = 'entities/2.0/Contact'
        const options = {
          method: 'POST',
          body: {
            foo: 'bar'
          },
          blockingInfoTitle
        }

        const error = new InformationError('message')
        return expectSaga(requestSaga, resource, options)
          .provide([
            [select(intl.localeSelector), 'fr'],
            [matchers.call.fn(prepareRequest), {}],
            [matchers.call.fn(sendRequest), throwError(error)],
            [matchers.call.fn(uuid), 'uuid']
          ])
          .put(blockingInfo('uuid', blockingInfoTitle))
          .put(removeBlockingInfo('uuid'))
          .run()
      })
    })

    describe('prepareRequest', () => {
      test('should append params to query', () => {
        const resource = 'entities/2.0/Contact'
        const options = {
          queryParams: {
            _search: 'test',
            xyz: 'abc'
          }
        }
        const requestData = prepareRequest(resource, options)

        expect(requestData.url).to.eql('/nice2/rest/entities/2.0/Contact?_search=test&xyz=abc')
      })

      test('should use GET as default method', () => {
        const requestData = prepareRequest('entities/2.0/Contact')
        expect(requestData.options.method).to.eql('GET')
      })

      test('should use specified method', () => {
        const options = {
          method: 'POST'
        }
        const requestData = prepareRequest('entities/2.0/Contact', options)
        expect(requestData.options.method).to.eql('POST')
      })

      test('should include credentials', () => {
        const requestData = prepareRequest('entities/Contact')
        expect(requestData.options.credentials).to.eql('include')
      })

      test('should add serialized body to options', () => {
        const resource = 'entities/2.0/Contact'
        const options = {
          method: 'POST',
          body: {
            foo: 'bar'
          }
        }
        const requestData = prepareRequest(resource, options)

        expect(requestData.options.body).to.eql('{"foo":"bar"}')
        expect(requestData.options.headers.get('Content-Type')).to.eql('application/json')
      })

      test('should add X-Business-Unit header if business unit set', () => {
        env.setBusinessUnit('my_test_bu')
        const requestData = prepareRequest('entities/2.0/Contact')
        env.setBusinessUnit(null)

        expect(requestData.options.headers.get('X-Business-Unit')).to.eql('my_test_bu')
      })

      test('should add X-Origin-Id header', () => {
        const requestData = prepareRequest('entities/Contact')
        expect(requestData.options.headers.get('X-Origin-Id')).to.be.not.undefined
        expect(requestData.options.headers.get('X-Origin-Id')).to.be.not.null
      })

      test('should add same X-Origin-Id for session', () => {
        const requestData = prepareRequest('entities/Contact')
        const originId = requestData.options.headers.get('X-Origin-Id')

        const requestData2 = prepareRequest('entities/User')
        const originId2 = requestData2.options.headers.get('X-Origin-Id')
        expect(originId).to.eql(originId2)
      })

      test('should use backend URL from options if set', () => {
        const resource = 'entities/2.0/Contact'
        const options = {
          backendUrl: 'https://my-backend.ch'
        }
        const requestData = prepareRequest(resource, options)

        expect(requestData.url).to.eql('https://my-backend.ch/nice2/rest/entities/2.0/Contact')
      })

      test('should use absolute url if ressource is one', () => {
        const absoluteResource = 'http://www.tocco.ch/nice2/rest/entities'
        const requestData = prepareRequest(absoluteResource)

        expect(requestData.url).to.eql('http://www.tocco.ch/nice2/rest/entities')

        const absoluteResource2 = 'https://www.tocco.ch/nice2/rest/entities'
        const requestData2 = prepareRequest(absoluteResource2)

        expect(requestData2.url).to.eql('https://www.tocco.ch/nice2/rest/entities')
      })

      test('should use nice2 url if resource is one', () => {
        const absoluteResource = 'nice2/login'
        const requestData = prepareRequest(absoluteResource, {backendUrl: 'backend'})

        expect(requestData.url).to.eql('backend/nice2/login')
      })

      test('should use Content-Type passed in options', () => {
        const resource = 'endpoint'
        const options = {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
          }),
          body: 'foo=bar'
        }

        const requestData = prepareRequest(resource, options)
        const contentTypeHeader = requestData.options.headers.get('Content-Type')
        expect(contentTypeHeader).to.eql('application/x-www-form-urlencoded; charset=utf-8')
      })

      test('should use passed Headers object', () => {
        const resource = 'endpoint'
        const optionHeaders = new Headers({
          'Content-Type': 'application/json'
        })
        const options = {
          headers: optionHeaders,
          body: {some: 'thing'}
        }

        const requestData = prepareRequest(resource, options)
        const headers = requestData.options.headers
        expect(headers).to.eql(optionHeaders)
        const contentTypeHeader = headers.get('Content-Type')
        expect(contentTypeHeader).to.eql('application/json')
      })

      test('should create Headers from passed object', () => {
        const resource = 'endpoint'
        const options = {
          headers: {
            'Content-Type': 'application/json'
          },
          body: {some: 'thing'}
        }

        const requestData = prepareRequest(resource, options)
        const headers = requestData.options.headers
        expect(headers).to.be.an.instanceOf(Headers)
        const contentTypeHeader = headers.get('Content-Type')
        expect(contentTypeHeader).to.eql('application/json')
      })

      test('should create empty Headers when nothing passed', () => {
        const resource = 'endpoint'
        const options = {
          body: {some: 'thing'}
        }

        const requestData = prepareRequest(resource, options)
        const headers = requestData.options.headers
        expect(headers).to.be.an.instanceOf(Headers)
        const contentTypeHeader = headers.get('Content-Type')
        expect(contentTypeHeader).to.eql('application/json')
      })

      test('should remove leading slash', () => {
        const resource = '/endpoint'
        const options = {
          body: {some: 'thing'}
        }

        const requestData = prepareRequest(resource, options)
        const url = requestData.url
        expect(url).to.eq('/nice2/rest/endpoint')
      })

      test('should set _widget_key query param if not already set', () => {
        env.setWidgetConfigKey('my-key')
        const requestData = prepareRequest('entities/Contact')
        expect(requestData.url).to.eq('/nice2/rest/entities/Contact?_widget_key=my-key')
      })

      test('should not override _widget_key query param if already set', () => {
        env.setWidgetConfigKey('my-key')
        const requestData = prepareRequest('entities/Contact', {queryParams: {_widget_key: 'initial-key'}})
        expect(requestData.url).to.eq('/nice2/rest/entities/Contact?_widget_key=initial-key')
      })

      test('should not set _widget_key query param if not defined in env', () => {
        env.setWidgetConfigKey(undefined)
        const requestData = prepareRequest('entities/Contact')
        expect(requestData.url).to.eq('/nice2/rest/entities/Contact')
      })
    })

    describe('setLocale', () => {
      test('should set locale if no query params in options', () => {
        const options = {}
        const expectedOptions = {
          queryParams: {
            locale: 'fr'
          }
        }

        return expectSaga(setLocale, options)
          .provide([[select(intl.localeSelector), 'fr']])
          .returns(expectedOptions)
          .run()
      })

      test('should set locale if no locale in query params', () => {
        const options = {
          queryParams: {
            foo: 'bar'
          }
        }
        const expectedOptions = {
          queryParams: {
            foo: 'bar',
            locale: 'fr'
          }
        }

        return expectSaga(setLocale, options)
          .provide([[select(intl.localeSelector), 'fr']])
          .returns(expectedOptions)
          .run()
      })

      test('should override locale if already set in query params', () => {
        const options = {
          queryParams: {
            foo: 'bar',
            locale: 'de'
          }
        }
        const expectedOptions = {
          queryParams: {
            foo: 'bar',
            locale: 'de'
          }
        }

        return expectSaga(setLocale, options)
          .provide([[select(intl.localeSelector), 'fr']])
          .returns(expectedOptions)
          .run()
      })
    })

    describe('prepareBlockingInfo', () => {
      test('should return actions if title is defined', () => {
        const randomId = 'uuid'
        const blockingInfoTitle = 'title'
        const options = {blockingInfoTitle}
        const expectedActions = {
          addBlockingInfoAction: blockingInfo(randomId, blockingInfoTitle),
          removeBlockingInfoAction: removeBlockingInfo(randomId)
        }
        return expectSaga(prepareBlockingInfo, options)
          .provide([[matchers.call.fn(uuid), randomId]])
          .returns(expectedActions)
          .run()
      })

      test('should return empty object if title is undefined', () => {
        const options = {}
        const expectedActions = {}
        return expectSaga(prepareBlockingInfo, options).returns(expectedActions).run()
      })
    })
  })
})
