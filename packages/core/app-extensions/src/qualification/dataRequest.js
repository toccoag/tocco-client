import {call} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

/**
 * used in qualification action because they have very similar endpoints and data structures
 */
export function* requestData(url, selection, sorting, dataFormColumns, searchQueries, recordsPerPage, currentPage) {
  const paths = getPathsFromTable(dataFormColumns)
  const searchBean = rest.buildRequestQuery({
    limit: recordsPerPage,
    sorting,
    page: currentPage,
    where: searchQueries.join(' and '),
    paths
  })
  return yield call(rest.requestSaga, url, {
    method: 'POST',
    body: {
      selection,
      searchBean
    }
  })
}

const getPathsFromTable = dataFormColumns => [
  'pk',
  ...dataFormColumns.flatMap(column => column.children.map(field => field.path))
]
