const defaultColumnOptions = {
  resizable: true,
  sorting: {
    sortable: false
  },
  dynamic: true,
  readOnly: false,
  alignment: 'left',
  shrinkToContent: false,
  sticky: false
}

export const createReadonlyColumn = (id, label, CellRendererComponent, cellRendererProps = {}, options = {}) => ({
  id,
  label,
  CellRenderer: props => <CellRendererComponent {...{...cellRendererProps, ...props}} />,
  ...defaultColumnOptions,
  readOnly: true,
  ...options
})

export const createEditableColumn = (id, label, CellRendererComponent, cellRendererProps = {}, options = {}) => ({
  id,
  label,
  CellRenderer: props => <CellRendererComponent {...{...cellRendererProps, ...props}} />,
  ...defaultColumnOptions,
  ...options
})

export const createButtonColumn = (id, label, CellRendererComponent, cellRendererProps = {}, options = {}) => ({
  id,
  label,
  CellRenderer: props => <CellRendererComponent {...{...cellRendererProps, ...props}} />,
  ...defaultColumnOptions,
  alignment: 'center',
  shrinkToContent: true,
  ...options
})

export const createActionColumn = createButtonColumn
