import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import ButtonCellRenderer from './ButtonCellRenderer'

describe('app-extensions', () => {
  describe('tableFrom', () => {
    describe('cellrenderers', () => {
      describe('ButtonCellRenderer', () => {
        test('should show icon', async () => {
          const rowData = {key: '1'}
          testingLibrary.renderWithIntl(
            <ButtonCellRenderer rowData={rowData} formField={{icon: 'sliders'}} onClick={() => {}} />
          )
          await screen.findAllByTestId('icon')
          expect(await screen.findByTestId('icon-sliders')).to.exist
        })

        test('should call onClick', async () => {
          const rowData = {key: '1'}
          const onClick = sinon.spy()
          testingLibrary.renderWithIntl(
            <ButtonCellRenderer rowData={rowData} formField={{icon: 'sliders'}} onClick={onClick} />
          )
          const icon = await screen.findByTestId('icon')

          const user = userEvent.setup()
          await user.click(icon)
          expect(onClick).to.have.been.calledWith(rowData)
        })
      })
    })
  })
})
