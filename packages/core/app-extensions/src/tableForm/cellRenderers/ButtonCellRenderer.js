import PropTypes from 'prop-types'
import {Button} from 'tocco-ui'

import {StyledCellRenderer} from './StyledComponents'

const ButtonCellRenderer = ({rowData, formField, disabled, onClick}) => (
  <StyledCellRenderer>
    {/* wrapper allows button to be centered despite display: inline-flex */}
    <span>
      <Button
        icon={formField.icon}
        label={formField.label}
        onClick={() => onClick(rowData, formField)}
        iconOnly={true}
        withoutBackground={true}
        disabled={disabled}
      />
    </span>
  </StyledCellRenderer>
)

ButtonCellRenderer.propTypes = {
  disabled: PropTypes.bool,
  rowData: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  formField: PropTypes.object.isRequired
}

export default ButtonCellRenderer
