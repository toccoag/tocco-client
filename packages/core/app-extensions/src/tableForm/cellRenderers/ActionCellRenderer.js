import PropTypes from 'prop-types'

import actions from '../../actions'

import {StyledCellRenderer} from './StyledComponents'

const ActionCellRenderer = ({rowData, formField, entityModel, disabled}) => (
  <StyledCellRenderer>
    {/* wrapper allows button to be centered despite display: inline-flex */}
    <span>
      <actions.Action
        definition={formField}
        selection={{entityName: entityModel, ids: [rowData.__key], type: 'ID'}}
        mode="update"
        disabled={disabled}
      />
    </span>
  </StyledCellRenderer>
)

ActionCellRenderer.propTypes = {
  disabled: PropTypes.bool,
  rowData: PropTypes.object.isRequired,
  entityModel: PropTypes.string.isRequired,
  formField: PropTypes.object.isRequired
}

export default ActionCellRenderer
