import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ActionCellRenderer from './ActionCellRenderer'

describe('app-extensions', () => {
  describe('tableFrom', () => {
    describe('cellRenderers', () => {
      describe('ActionCellRenderer', () => {
        const store = configureStore({
          reducer: () => ({
            actions: {}
          })
        })

        test('should render action', async () => {
          const rowData = {__key: '1'}
          testingLibrary.renderWithStore(
            <ActionCellRenderer
              rowData={rowData}
              formField={{icon: 'bolt', actionType: 'custom', componentType: 'action', id: 'action'}}
              entityModel={'Some_model'}
            />,
            {
              store
            }
          )
          await screen.findAllByTestId('icon')
          expect(await screen.findByTestId('icon-bolt-lightning')).to.exist
        })
      })
    })
  })
})
