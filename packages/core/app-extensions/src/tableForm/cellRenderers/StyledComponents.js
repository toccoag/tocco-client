import styled from 'styled-components'
import {scale} from 'tocco-ui'

export const StyledCellRenderer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-top: ${scale.space(-1.0)};
`
