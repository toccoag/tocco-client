import PropTypes from 'prop-types'
import {field} from 'tocco-app-extensions'
import {StatedCellValue} from 'tocco-ui'

const EditableCellRenderer = ({
  rowData,
  column,
  type,
  setValue,
  validate,
  mappingType = 'editable',
  formField = {}
}) => {
  const id = `${rowData.index || rowData.__key}:${column.id}`

  const Field = field.factory(mappingType, type)
  const errors = rowData.errors?.[column.id] || {}
  const value = rowData[column.id]

  const borderless = type === 'boolean' || ['list', 'readOnly'].includes(mappingType)

  return (
    <StatedCellValue error={errors} id={id} borderless={borderless}>
      <Field
        info={{id}}
        formField={{dataType: type, ...formField}}
        value={value}
        mappingType={mappingType}
        events={{
          ...(setValue ? {onChange: updatedValue => setValue(rowData.__key, column.id, updatedValue)} : {}),
          ...(validate ? {onBlur: () => validate(rowData)} : {}),
          onFocus: ({target}) => {
            if (typeof target.select === 'function') {
              target.select()
            }
          }
        }}
      />
    </StatedCellValue>
  )
}

EditableCellRenderer.propTypes = {
  rowData: PropTypes.object.isRequired,
  column: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
  mappingType: PropTypes.string,
  formField: PropTypes.object,
  setValue: PropTypes.func,
  validate: PropTypes.func
}

export default EditableCellRenderer
