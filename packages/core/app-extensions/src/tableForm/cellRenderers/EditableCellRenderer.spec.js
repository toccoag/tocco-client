import {act, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {FormattedMessage} from 'react-intl'
import {testingLibrary} from 'tocco-test-util'

import EditableCellRenderer from './EditableCellRenderer'

describe('app-extensions', () => {
  describe('tableFrom', () => {
    describe('cellRenderers', () => {
      describe('EditableCellRenderer', () => {
        test('should display value', async () => {
          testingLibrary.renderWithIntl(
            <EditableCellRenderer
              rowData={{index: 0, other: 'wrong', column: 'value', errors: {}}}
              column={{id: 'column'}}
              type="string"
              mappingType="readOnly"
            />
          )
          expect(screen.getByText('value')).to.exist
        })
        test('should display value', async () => {
          testingLibrary.renderWithIntl(
            <EditableCellRenderer
              rowData={{other: 'wrong', column: 'value', errors: {}}}
              column={{id: 'column'}}
              type={'string'}
              setValue={() => {}}
              validate={() => {}}
            />
          )
          expect(screen.getByRole('textbox').value).to.eq('value')
        })

        test('should display errors', async () => {
          testingLibrary.renderWithIntl(
            <EditableCellRenderer
              rowData={{
                errors: {
                  column: {
                    mandatory: [<FormattedMessage key="anyError" id="error-message" />]
                  }
                }
              }}
              column={{id: 'column'}}
              type={'string'}
              setValue={() => {}}
              validate={() => {}}
            />
          )
          await screen.findAllByTestId('icon')

          expect(screen.getByTestId('icon-times')).to.exist
          expect(screen.getByText('error-message')).to.exist
        })

        test('should validate on blur', async () => {
          const validate = sinon.spy()
          const rowData = {__key: 1, errors: {}}
          testingLibrary.renderWithIntl(
            <EditableCellRenderer
              rowData={rowData}
              column={{id: 'column'}}
              type={'string'}
              setValue={() => {}}
              validate={validate}
            />
          )

          const user = userEvent.setup()
          await user.type(screen.getByRole('textbox'), '[Tab]')
          expect(validate).to.have.been.calledWith(rowData)
        })

        describe('with timers', () => {
          beforeEach(() => {
            jest.useFakeTimers()
          })

          afterEach(() => {
            jest.clearAllTimers()
          })

          afterAll(() => {
            jest.useRealTimers()
          })

          test('should set value on edit', async () => {
            const setValue = sinon.spy()
            testingLibrary.renderWithIntl(
              <EditableCellRenderer
                rowData={{__key: 1, errors: {}}}
                column={{id: 'column'}}
                type={'string'}
                setValue={setValue}
                validate={() => {}}
              />
            )

            const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})
            await user.type(screen.getByRole('textbox'), 'value')
            act(() => {
              jest.runAllTimers()
            })
            expect(setValue).to.have.been.calledWith(1, 'column', 'value')
          })
        })
      })
    })
  })
})
