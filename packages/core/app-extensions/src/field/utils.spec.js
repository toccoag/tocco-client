import {getPathEntityFields} from './utils'

describe('app-extensions', () => {
  describe('field', () => {
    describe('utils', () => {
      test('get simple field', () => {
        const firstNamePath = {
          type: 'string',
          writable: true,
          value: 'Stefan'
        }
        const fieldName = 'firstname'
        const entity = {
          paths: {
            firstname: firstNamePath
          }
        }

        expect(getPathEntityFields(fieldName, entity)).to.be.eql([firstNamePath])
      })

      test('get simple relation', () => {
        const genderPath = {
          type: 'entity',
          writable: true,
          value: null
        }
        const fieldName = 'relGender'
        const entity = {
          paths: {
            relGender: genderPath
          }
        }

        expect(getPathEntityFields(fieldName, entity)).to.be.eql([genderPath])
      })

      test('get nested field', () => {
        const fieldName = 'relGender.label'
        const entity = {
          paths: {
            relGender: {
              type: 'entity',
              writable: true,
              value: {
                paths: {
                  label: {
                    type: 'string',
                    writable: true,
                    value: null
                  }
                }
              }
            }
          }
        }

        const expected = [
          {
            type: 'entity',
            writable: true,
            value: {
              paths: {
                label: {
                  type: 'string',
                  writable: true,
                  value: null
                }
              }
            }
          },
          {
            type: 'string',
            writable: true,
            value: null
          }
        ]

        expect(getPathEntityFields(fieldName, entity)).to.be.eql(expected)
      })
    })
  })
})
