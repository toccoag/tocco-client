import percent from './percent'

describe('app-extensions', () => {
  describe('field', () => {
    describe('formattedComponentConfigs', () => {
      describe('percent', () => {
        describe('getValue', () => {
          test('test getValue mapping to decimal', () => {
            expect(percent.getValue({value: 100})).to.eql(1)
            expect(percent.getValue({value: 50.12})).to.eql(0.5012)
            expect(percent.getValue({value: 0})).to.eql(0)
            expect(percent.getValue({value: null})).to.eql(null)
          })
        })
      })
    })
  })
})
