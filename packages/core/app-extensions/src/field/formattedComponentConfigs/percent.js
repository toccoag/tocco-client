import _get from 'lodash/get'

export default {
  getValue: ({value}) => {
    if (value) {
      return value / 100 // FormattedNumber assumes percentage is delivered as decimal
    }

    return value
  },
  getOptions: ({formField}) => ({
    postPointDigits: _get(formField, 'validation.decimalDigits.postPointDigits', null)
  })
}
