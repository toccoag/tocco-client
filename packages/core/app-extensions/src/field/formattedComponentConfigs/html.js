export default {
  getOptions: ({formField}) => ({escapeHtml: Boolean(formField.escapeHtml)})
}
