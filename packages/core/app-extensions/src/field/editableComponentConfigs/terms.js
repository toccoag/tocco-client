export default {
  getOptions: ({formData, formField}) => ({
    terms: formData.terms?.[formField.type],
    loadTerms: () => formData.loadTerms(formField.type)
  }),
  getEvents: ({events, formData, formName, formField}) => ({
    ...events,
    onChange: value => {
      // unchecked values are placeholders and not actually entered by user, do not dirty the form
      if (value && !value.checked) {
        formData.changePristineFieldValue(formName, formField.path, value)
      } else {
        events.onChange(value)
      }
    }
  })
}
