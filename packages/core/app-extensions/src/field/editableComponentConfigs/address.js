import _get from 'lodash/get'

import {transformFieldName} from '../../form/reduxForm'
import {getPathEntityFields} from '../utils'

export default {
  hasValue: ({formValues, formField}) => {
    const locationMapping = formField.locationMapping
    return !!formValues[transformFieldName(locationMapping.street)]
  },
  getValue: ({formField, formData}) => {
    const locationMapping = formField.locationMapping
    return {
      street: formData.formValues[transformFieldName(locationMapping.street)]
    }
  },
  // hasValue is used to check if the field is empty and should be hidden if it's read-only
  hideIfEmptyValue: () => true,
  getEvents: ({formField, formName, formData, events}) => {
    const locationMapping = formField.locationMapping || {}
    const onChange = locationObject => {
      for (const key in locationMapping) {
        if (locationMapping[key] && locationObject[key] !== undefined) {
          const value = locationObject[key] !== null ? locationObject[key] : ''
          formData.changeFieldValue(formName, locationMapping[key], value)
        }
      }

      const {street} = locationObject
      formData.changeFieldValue(formName, formField.id, {
        ...(street !== undefined ? {street: street || ''} : {})
      })

      // also update the location field (postcode and city wrapper field) to trigger validation of it
      if (formField.postcodeCityField) {
        const {postcode, city} = locationObject
        formData.changeFieldValue(formName, formField.postcodeCityField, {
          ...(postcode !== undefined ? {postcode: postcode || ''} : {}),
          ...(city !== undefined ? {city: city || ''} : {})
        })
      }
    }
    return {
      ...events,
      onChange,
      onBlur: () => {
        formData.touchField(formName, formField.id)
      }
    }
  },
  dataContainerProps: ({formField, formName}) => {
    return {
      locations: [formField.id],
      formValues: {
        formName,
        fields: formField.locationMapping
          ? Object.values(formField.locationMapping).map(name => transformFieldName(name))
          : {}
      }
    }
  },
  getOptions: ({formField, formData}) => ({
    fetchSuggestions: (searchTerm, country) =>
      formData.loadLocationsSuggestions(formField.id, searchTerm, country, _get(formField, 'countries')),
    validateLocation: (location, callbackAction) =>
      formData.validateLocation(formField.id, location, _get(formField, 'countries'), callbackAction),
    isLoading: _get(formData, ['locations', formField.id, 'isLoading'], false),
    suggestions: _get(formData, ['locations', formField.id, 'suggestions'], null),
    mapButtonTitle: formData.intl.formatMessage({id: 'client.component.location.mapButtonTitle'}),
    locationValues: Object.keys(formField.locationMapping || {}).reduce(
      (acc, key) => ({...acc, [key]: formData.formValues[transformFieldName(formField.locationMapping[key])]}),
      {}
    )
  }),
  getMandatoryValidation: ({formField}) => {
    const locationMapping = formField.locationMapping
    const streetValidation = formField.siblings.find(s => s.id === locationMapping.street)?.validation
    return streetValidation?.mandatory
  },
  isReadOnly: ({formField, entity}) => {
    const locationMapping = formField.locationMapping
    const street = getPathEntityFields(locationMapping.street, entity).slice(-1)[0]
    return street?.writable === false
  }
}
