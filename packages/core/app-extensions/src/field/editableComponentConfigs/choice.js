export default {
  hasValue: ({value}) => value.options.filter(o => o.checked).length > 0,
  // a pseudo field should always be visible even if nothing is selected and it is not writable
  hideIfEmptyValue: () => false,
  getOptions: ({formField}) => ({
    selectionType: formField.selectionType,
    renderHtmlInOptions: formField.renderHtmlInOptions
  })
}
