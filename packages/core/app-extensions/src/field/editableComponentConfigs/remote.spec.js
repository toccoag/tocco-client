import {env} from 'tocco-util'

import remote from './remote'

describe('app-extensions', () => {
  describe('field', () => {
    describe('editableComponentConfigs', () => {
      describe('remote', () => {
        describe('createPermission', () => {
          const getFormField = additionalProperties => ({
            relationName: 'relCountry',
            id: 'relCountry',
            ...additionalProperties
          })

          const getFormData = relationModel => ({
            intl: {
              formatMessage: id => id
            },
            entityModel: {
              paths: {
                relCountry: relationModel
              }
            }
          })

          test('should return undefined if no relation name is passed', () => {
            const formField = {}
            const formData = getFormData()
            const options = remote.getOptions({formField, formData})

            expect(options.createPermission).to.be.undefined
          })

          test('should return false if createPermission is not set', () => {
            const formField = getFormField()
            const formData = getFormData()
            const options = remote.getOptions({formField, formData})

            expect(options.createPermission).to.be.false
          })

          test('should return true if createPermission is true and other properties are undefined', () => {
            const formField = getFormField({displayCreateButton: undefined})
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: undefined})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.true
          })

          test('should return false if useRemoteFieldNewButton is false', () => {
            const formField = getFormField()
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: false})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.false
          })

          test('should return false if displayCreateButton is false', () => {
            const formField = getFormField({displayCreateButton: false})
            const customFormData = getFormData({createPermission: true})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.false
          })

          test('should return true if all properties are true', () => {
            const formField = getFormField({displayCreateButton: true})
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: true})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.true
          })

          test('should return true as a widget', () => {
            env.setEmbedType('widget')
            const formField = getFormField({displayCreateButtonWidget: true})
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: true})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.true
          })

          test('should return false as a widget when display is not set', () => {
            env.setEmbedType('widget')
            const formField = getFormField()
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: true})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.false
          })

          test('should return false if createPermission is not set in widget', () => {
            env.setEmbedType('widget')
            const formField = getFormField()
            const formData = getFormData()
            const options = remote.getOptions({formField, formData})

            expect(options.createPermission).to.be.false
          })

          test('should return false if useRemoteFieldNewButton is false in widget', () => {
            env.setEmbedType('widget')
            const formField = getFormField()
            const customFormData = getFormData({createPermission: true, useRemoteFieldNewButton: false})
            const options = remote.getOptions({formField, formData: customFormData})

            expect(options.createPermission).to.be.false
          })
        })

        describe('fetchOptions', () => {
          test('should respect itemCount as limit', () => {
            const formField = {itemCount: 33}
            const formData = {
              loadRelationEntities: (id, targetEntity, props) => {
                expect(props.limit).to.eql(33)
              },
              intl: {formatMessage: id => id}
            }
            remote.getOptions({formField, formData}).fetchOptions(null)
          })

          test('should use fallback limit without itemCount', () => {
            const formField = {}
            const formData = {
              loadRelationEntities: (id, targetEntity, props) => {
                expect(props.limit).to.eql(25)
              },
              intl: {formatMessage: id => id}
            }
            remote.getOptions({formField, formData}).fetchOptions(null)
          })
        })

        describe('searchOptions', () => {
          test('should respect itemCount as limit', () => {
            const formField = {itemCount: 33}
            const formData = {
              loadRelationEntities: (id, targetEntity, props) => {
                expect(props.limit).to.eql(33)
              },
              intl: {formatMessage: id => id}
            }
            remote.getOptions({formField, formData}).searchOptions(null)
          })

          test('should use fallback limit without itemCount', () => {
            const formField = {}
            const formData = {
              loadRelationEntities: (id, targetEntity, props) => {
                expect(props.limit).to.eql(50)
              },
              intl: {formatMessage: id => id}
            }
            remote.getOptions({formField, formData}).searchOptions(null)
          })
        })
      })
    })
  })
})
