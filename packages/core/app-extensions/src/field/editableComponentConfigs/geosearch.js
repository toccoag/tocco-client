import _get from 'lodash/get'

const DefaultDistance = 10

export default {
  dataContainerProps: ({formField, formName}) => ({
    locations: [formField.id],
    formValues: {
      formName,
      fields: [formField.id]
    }
  }),
  getValue: ({formField, formData}) => {
    const value = formData.formValues[formField.id]
    return {
      ...value,
      ...(!value?.distance ? {distance: DefaultDistance} : {})
    }
  },
  getOptions: ({formField, formData}) => ({
    latitudeField: formField.latitudeField,
    longitudeField: formField.longitudeField,
    noResultsText: formData.intl.formatMessage({id: 'client.component.geosearch.noResultsText'}),
    emptySearchText: formData.intl.formatMessage({id: 'client.component.geosearch.emptySearchText'}),
    kmSuffixText: formData.intl.formatMessage({id: 'client.component.geosearch.km'}),
    suggestions: _get(formData, ['locations', formField.id, 'suggestions'], []),
    isLoading: _get(formData, ['locations', formField.id, 'isLoading'], false),
    fetchSuggestions: (searchTerm, country) => {
      if (searchTerm) {
        formData.loadLocationsSuggestions(formField.id, searchTerm, country, _get(formField, 'countries'))
      } else {
        formData.setLocationSuggestions(formField.id, [])
      }
    }
  })
}
