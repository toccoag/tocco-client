const getCKEditorConfig = config => {
  // only email and correspondence editor contain all font features
  if (!['email_editor', 'correspondence_editor'].includes(config)) {
    return {
      removeButtons: ['Font', 'FontSize', 'TextColor']
    }
  }
}

const getOptions = config => ({
  defaultLinkTarget: ['terms_editor'].includes(config) ? '_blank' : 'notSet',
  ckEditorConfig: getCKEditorConfig(config)
})

export default {
  fixLabel: () => true,
  getOptions: ({formField}) => getOptions(formField.htmlconfig)
}
