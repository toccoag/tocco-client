import address from './address'

describe('app-extensions', () => {
  describe('field', () => {
    describe('editableComponentConfigs', () => {
      describe('address', () => {
        describe('isReadOnly', () => {
          const testIsReadOnly = writable => {
            const formField = {
              id: 'address',
              locationMapping: {
                street: 'street'
              }
            }
            const entity = {
              paths: {
                street: {
                  type: 'string',
                  writable,
                  value: null
                }
              }
            }

            return address.isReadOnly({formField, entity})
          }

          test('should be read only as street field is not writable', () => {
            expect(testIsReadOnly(false)).to.eql(true)
          })

          test('should not be read only as street field is writable', () => {
            expect(testIsReadOnly(true)).to.eql(false)
          })
        })
      })
    })
  })
})
