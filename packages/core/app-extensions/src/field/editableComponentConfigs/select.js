import _get from 'lodash/get'

export default {
  dataContainerProps: ({formField}) => ({
    relationEntities: [formField.id],
    tooltips: [formField.targetEntity]
  }),
  getOptions: ({formField, formData}) => {
    const formFieldOptions = formField?.options
    const formDataOptions = _get(formData, ['relationEntities', formField.id, 'data'])
    return {
      options: formDataOptions || formFieldOptions || [],
      isLoading: _get(formData, ['relationEntities', formField.id, 'isLoading'], false),
      tooltips: _get(formData, ['tooltips', formField.targetEntity], null),
      loadTooltip: id => formData?.loadTooltip(formField.targetEntity, id),
      noResultsText: formData?.intl.formatMessage({id: 'client.component.remoteselect.noResultsText'}),
      fetchOptions: () => {
        if (formField.targetEntity) {
          formData?.loadRelationEntities(formField.id, formField.targetEntity, {
            forceReload: false,
            constriction: formField.constriction,
            where: formField.condition,
            optionDisplay: formField.optionDisplay
          })
        }
      }
    }
  }
}
