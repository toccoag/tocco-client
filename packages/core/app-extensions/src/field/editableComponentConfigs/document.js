export default {
  getOptions: ({formField, formName, formData}) => ({
    upload: (document, onChange) => {
      formData.uploadDocument(formName, formField.id, document, onChange)
    },
    ...(formData.chooseDocument
      ? {
          choose: onChange => formData.chooseDocument(formData.setDocument, formName, formField.id, onChange)
        }
      : {}),
    uploadText: formData.intl.formatMessage({id: 'client.component.upload.upload'}),
    uploadingText: formData.intl.formatMessage({id: 'client.component.upload.uploading'}),
    downloadText: formData.intl.formatMessage({id: 'client.component.upload.downloadTitle'}),
    deleteText: formData.intl.formatMessage({id: 'client.component.upload.deleteTitle'}),
    allowedFileTypes: formField.allowedFileTypes
  }),
  fixLabel: () => true
}
