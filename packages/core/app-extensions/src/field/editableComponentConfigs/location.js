import _get from 'lodash/get'

import {transformFieldName} from '../../form/reduxForm'
import {getPathEntityFields} from '../utils'

/**
 * `location` field covers `city` and `postcode` value.
 */
export default {
  hasValue: ({formValues, formField}) => {
    const locationMapping = formField.locationMapping
    return !!(
      formValues[transformFieldName(locationMapping.city)] || formValues[transformFieldName(locationMapping.postcode)]
    )
  },
  // hasValue is used to check if the field is empty and should be hidden if it's read-only
  hideIfEmptyValue: () => true,
  getValue: ({formField, formData}) => {
    const locationMapping = formField.locationMapping

    return {
      postcode: formData.formValues[transformFieldName(locationMapping.postcode)],
      city: formData.formValues[transformFieldName(locationMapping.city)]
    }
  },
  getEvents: ({formField, formName, formData, events}) => {
    const locationMapping = formField.locationMapping || {}
    const onChange = locationObject => {
      for (const key in locationMapping) {
        if (
          locationMapping[key] &&
          locationObject[key] !== undefined &&
          key !== 'street' // don't update/reset street from location field
        ) {
          const value = locationObject[key] !== null ? locationObject[key] : ''
          formData.changeFieldValue(formName, locationMapping[key], value)
        }
      }

      const {postcode, city} = locationObject
      formData.changeFieldValue(formName, formField.id, {
        ...(postcode !== undefined ? {postcode: postcode || ''} : {}),
        ...(city !== undefined ? {city: city || ''} : {})
      })
    }
    return {
      ...events,
      onChange,
      onBlur: () => {
        formData.touchField(formName, formField.id)
      }
    }
  },
  dataContainerProps: ({formField, formName}) => ({
    locations: [formField.id],
    formValues: {
      formName,
      fields: formField.locationMapping
        ? Object.values(formField.locationMapping).map(name => transformFieldName(name))
        : {}
    }
  }),
  getOptions: ({formField, formData}) => ({
    fetchSuggestions: (searchTerm, country) =>
      formData.loadLocationsSuggestions(formField.id, searchTerm, country, _get(formField, 'countries')),
    validateLocation: (location, callbackAction) =>
      formData.validateLocation(formField.id, location, _get(formField, 'countries'), callbackAction),
    isLoading: _get(formData, ['locations', formField.id, 'isLoading'], false),
    suggestions: _get(formData, ['locations', formField.id, 'suggestions'], null),
    mapButtonTitle: formData.intl.formatMessage({id: 'client.component.location.mapButtonTitle'}),
    locationValues: Object.keys(formField.locationMapping || {}).reduce(
      (acc, key) => ({...acc, [key]: formData.formValues[transformFieldName(formField.locationMapping[key])]}),
      {}
    )
  }),
  getMandatoryValidation: ({formField}) => {
    const locationMapping = formField.locationMapping
    const cityValidation = formField.siblings.find(s => s.id === locationMapping.city)?.validation
    const postcodeValidation = formField.siblings.find(s => s.id === locationMapping.postcode)?.validation

    return cityValidation?.mandatory || postcodeValidation?.mandatory
  },
  isReadOnly: ({formField, entity}) => {
    const locationMapping = formField.locationMapping
    const city = getPathEntityFields(locationMapping.city, entity).slice(-1)[0]
    const postcode = getPathEntityFields(locationMapping.postcode, entity).slice(-1)[0]
    return city?.writable === false || postcode?.writable === false
  }
}
