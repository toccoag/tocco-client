import _get from 'lodash/get'
import {env} from 'tocco-util'

const settings = {
  SEARCH_RESULT_LIMIT: 50,
  SUGGESTION_LIMIT: 25
}

const joinConditions = conditions => conditions.filter(Boolean).join(' and ')

const getExcludeConstraint = value =>
  !value || !Array.isArray(value) || value.length === 0 ? undefined : `not KEYS(${value.map(v => v.key).join(', ')})`

const checkCreateButtonPermission = (formData, formField) =>
  formField.relationName &&
  _get(formData, ['entityModel', 'paths', formField.relationName, 'createPermission'], false) &&
  _get(formData, ['entityModel', 'paths', formField.relationName, 'useRemoteFieldNewButton'], true)

const getValueWithDefaultDisplay = (value, formData) =>
  value === null
    ? null
    : {
        ...value,
        display: _get(formData, ['relationEntitiesDefaultDisplays', value.model, value.key], value?.display)
      }

export default {
  dataContainerProps: ({formField}) => ({
    relationEntities: [formField.id],
    tooltips: [formField.targetEntity],
    navigationStrategy: true
  }),
  getOptions: ({formField, formName, formData}) => ({
    options: _get(formData, ['relationEntities', formField.id, 'data'], []),
    moreOptionsAvailable: _get(formData, ['relationEntities', formField.id, 'moreEntitiesAvailable'], false),
    isLoading: _get(formData, ['relationEntities', formField.id, 'isLoading'], false),
    fetchOptions: currentValue => {
      formData.loadRelationEntities(formField.id, formField.targetEntity, {
        forceReload: true,
        limit: formField.itemCount || settings.SUGGESTION_LIMIT,
        formBase: formField.formBase,
        formName: formField.formName,
        where: joinConditions([formField.condition, getExcludeConstraint(currentValue)]),
        loadRemoteFieldConfiguration: true,
        constriction: formField.constriction,
        optionDisplay: formField.optionDisplay
      })
    },
    searchOptions: (searchTerm, value) => {
      formData.loadRelationEntities(formField.id, formField.targetEntity, {
        searchTerm,
        limit: formField.itemCount || settings.SEARCH_RESULT_LIMIT,
        forceReload: true,
        formBase: formField.formBase,
        formName: formField.formName,
        where: joinConditions([formField.condition, getExcludeConstraint(value)]),
        loadRemoteFieldConfiguration: true,
        constriction: formField.constriction,
        optionDisplay: formField.optionDisplay
      })
    },
    openDocsTreeSearch: formField.displayDocsTree
      ? value => formData.openDocsTreeSearch(formName, formField, value)
      : undefined,
    /**
     * `displayAdvancedSearch` can be null or undefined.
     * In these cases the default value should be `true`.
     * Only when set explicitly to `false` the advanced search should be hidden.
     */
    openAdvancedSearch:
      formField.displayAdvancedSearch === false
        ? undefined
        : (searchTerm, value) => formData.openAdvancedSearch(formName, formField, searchTerm, value),
    tooltips: _get(formData.tooltips, formField.targetEntity, null),
    loadTooltip: id => formData.loadTooltip(formField.targetEntity, id),
    noResultsText: formData.intl.formatMessage({id: 'client.component.remoteselect.noResultsText'}),
    moreOptionsAvailableText: formData.intl.formatMessage({
      id: 'client.component.remoteselect.moreOptionsAvailableText'
    }),
    DetailLink:
      formData.navigationStrategy && formData.navigationStrategy.DetailLink
        ? ({entityKey, children, ...props}) => (
            <formData.navigationStrategy.DetailLink
              entityName={formField.targetEntity}
              entityKey={entityKey}
              {...props}
            >
              {children}
            </formData.navigationStrategy.DetailLink>
          )
        : null,
    createPermission:
      checkCreateButtonPermission(formData, formField) &&
      (env.isInWidgetEmbedded()
        ? _get(formField, 'displayCreateButtonWidget', false)
        : _get(formField, 'displayCreateButton', true)),
    openRemoteCreate: value => formData.openRemoteCreate(formField, formName, value)
  }),
  getEvents: ({events, formData}) => ({
    ...events,
    onChange: value => {
      const valueWithDefaultDisplay = Array.isArray(value)
        ? value.map(v => getValueWithDefaultDisplay(v, formData))
        : getValueWithDefaultDisplay(value, formData)
      events?.onChange(valueWithDefaultDisplay)
    }
  })
}
