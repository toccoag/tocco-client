import _get from 'lodash/get'

/**
 * if a relation is empty, no further  information can be pulled from any fields or relations behind that path.
 * we want to display fields of empty relations so a new entity can be created.
 *
 * therefore we step through the entire path, reading the information at each step
 * and using the last available to test if the field should be rendered.
 * for a field to be rendered, the last available step needs to be writable.
 *
 * for actual rendering, using the actual last step (which in these special cases is undefined) is fine,
 * since it then gets rendered like it is in create mode.
 */
export const getPathEntityFields = (fieldName, entity) =>
  fieldName
    .split('.')
    .reduce((acc, name) => {
      const last = acc[acc.length - 1]
      return [...acc, last ? `${last}.${name}` : name]
    }, [])
    // use array as getter path to support selectors as valid object keys (e.g. relAddress[publication])
    .map(path => `paths.${path.split('.').join('.value.paths.')}`.split('.'))
    .map(path => _get(entity, path))
