import {mapStateToProps} from './FormDataContainer'

describe('app-extensions', () => {
  describe('formData', () => {
    describe('FormDataContainer', () => {
      describe('mapStateToProps', () => {
        const state = {
          formData: {
            relationEntities: {
              data: {
                relUser: [{key: 1}, {key: 3}],
                relUser2: [{key: 33}]
              }
            },
            tooltips: {data: {}},
            config: {configSelector: () => ({})}
          },
          form: {
            detailForm: {
              values: {
                canton_c: 'ZH',
                city_c: 'Zurich',
                postcode: '8000'
              }
            }
          },
          input: {
            chooseDocument: () => {}
          }
        }

        test('should extract relation entities from state', () => {
          const formData = mapStateToProps(state, {relationEntities: 'relUser2'})
          expect(formData.relationEntities).to.eql({relUser2: [{key: 33}]})
        })
      })
    })
  })
})
