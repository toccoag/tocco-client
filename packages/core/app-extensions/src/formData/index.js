import {addToStore} from './formData'
import FormDataContainer from './FormDataContainer'
import {setTermsCurrentEntity} from './terms/actions'

export default {
  addToStore,
  FormDataContainer,
  setTermsCurrentEntity
}
