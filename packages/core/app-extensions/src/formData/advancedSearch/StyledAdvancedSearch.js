import styled from 'styled-components'
import {scale, StyledStickyButtonWrapper} from 'tocco-ui'

export const StyledAdvancedSearch = styled.div`
  && {
    > *:not(:last-child) {
      margin-bottom: ${scale.space(-1)};
    }
  }
`

export const StyledAdvancedSearchButtonWrapper = styled(StyledStickyButtonWrapper)`
  padding-top: ${scale.space(0.5)};
`
