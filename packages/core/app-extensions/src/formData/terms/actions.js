export const LOAD_TERMS = 'formData/LOAD_TERMS'
export const SET_TERMS = 'formData/SET_TERMS'
export const SET_TERMS_CURRENT_ENTITY = 'formData/SET_TERMS_CURRENT_ENTITY'

export const loadTerms = type => ({
  type: LOAD_TERMS,
  payload: {
    type
  }
})

export const setTerms = (type, terms) => ({
  type: SET_TERMS,
  payload: {
    type,
    terms
  }
})

export const setTermsCurrentEntity = (model, key) => ({
  type: SET_TERMS_CURRENT_ENTITY,
  payload: {
    currentEntity: {
      model,
      key
    }
  }
})
