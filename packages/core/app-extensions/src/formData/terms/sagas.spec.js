import {takeEvery, select} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {env} from 'tocco-util'

import * as actions from './actions'
import * as sagas from './sagas'

describe('app-extensions', () => {
  describe('formData', () => {
    describe('terms', () => {
      describe('sagas', () => {
        describe('main saga', () => {
          test('should fork sagas', () => {
            const saga = testSaga(sagas.default)
            saga.next().all([takeEvery(actions.LOAD_TERMS, sagas.loadTerms)])
          })
        })

        describe('loadTerms', () => {
          test('should load terms', () => {
            env.setWidgetConfigKey('my-key')

            const terms = {
              currentEntity: {
                model: 'Event',
                key: '1'
              }
            }
            const resource = '/client/terms/conditions/load'
            const options = {
              method: 'POST',
              body: {
                currentEntityModel: 'Event',
                currentEntityKey: '1',
                widgetConfigKey: 'my-key'
              }
            }
            const body = {}
            return expectSaga(sagas.loadTerms, {payload: {type: 'conditions'}})
              .provide([
                [select(sagas.termsSelector), terms],
                [matchers.call(rest.requestSaga, resource, options), {body}]
              ])
              .put(actions.setTerms('conditions', body))
              .run()
          })
        })
      })
    })
  })
})
