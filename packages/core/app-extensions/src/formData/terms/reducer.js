import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

export const setTerms = (state, {payload: {type, terms}}) => ({
  ...state,
  terms: {
    ...state.terms,
    [type]: terms
  }
})

const ACTION_HANDLERS = {
  [actions.SET_TERMS]: setTerms,
  [actions.SET_TERMS_CURRENT_ENTITY]: reducerUtil.singleTransferReducer('currentEntity')
}

const initialState = {
  terms: {},
  currentEntity: undefined
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
