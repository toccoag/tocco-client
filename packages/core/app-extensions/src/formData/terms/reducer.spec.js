import * as actions from './actions'
import reducer from './reducer'

describe('app-extensions', () => {
  describe('formData', () => {
    describe('terms', () => {
      describe('reducer', () => {
        test('setTerms', () => {
          const initialState = {
            terms: {
              otherTerms: {
                label: 'terms other'
              }
            },
            currentEntity: undefined
          }
          const terms = {
            label: 'terms'
          }
          const expectedState = {
            terms: {
              conditions: {
                label: 'terms'
              },
              otherTerms: {
                label: 'terms other'
              }
            },
            currentEntity: undefined
          }
          expect(reducer(initialState, actions.setTerms('conditions', terms))).to.deep.equal(expectedState)
        })
      })
    })
  })
})
