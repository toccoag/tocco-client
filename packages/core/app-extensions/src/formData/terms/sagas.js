import {all, put, takeEvery, select, call} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {env} from 'tocco-util'

import * as actions from './actions'

export const termsSelector = state => state.formData.terms

export default function* sagas() {
  yield all([takeEvery(actions.LOAD_TERMS, loadTerms)])
}

export function* loadTerms({payload: {type}}) {
  const {currentEntity} = yield select(termsSelector)
  const widgetConfigKey = env.getWidgetConfigKey()
  const {body} = yield call(rest.requestSaga, `/client/terms/${type}/load`, {
    method: 'POST',
    body: {
      currentEntityModel: currentEntity?.model,
      currentEntityKey: currentEntity?.key,
      widgetConfigKey
    }
  })
  yield put(actions.setTerms(type, body))
}
