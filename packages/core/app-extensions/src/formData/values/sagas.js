import {actions as formActions, getFormInitialValues} from 'redux-form'
import {all, put, takeEvery, select} from 'redux-saga/effects'

import form from '../../form'

import * as tooltipActions from './actions'

export default function* sagas() {
  yield all([
    takeEvery(tooltipActions.CHANGE_FIELD_VALUE, changeValue),
    takeEvery(tooltipActions.CHANGE_PRISTINE_FIELD_VALUE, changePristineValue),
    takeEvery(tooltipActions.TOUCH_FIELD, touchField)
  ])
}

export function* changeValue({payload: {formName, fieldName, value}}) {
  yield put(formActions.change(formName, form.transformFieldName(fieldName), value))
}

export function* changePristineValue({payload: {formName, fieldName, value}}) {
  const initialValues = yield select(getFormInitialValues(formName))
  const newValues = {
    ...initialValues,
    [form.transformFieldName(fieldName)]: value
  }
  yield put(formActions.initialize(formName, newValues, true))
}

export function* touchField({payload: {formName, fieldName}}) {
  yield put(formActions.touch(formName, form.transformFieldName(fieldName)))
}
