import _isEqual from 'lodash/isEqual'
import _pick from 'lodash/pick'
import PropTypes from 'prop-types'
import React from 'react'
import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {openAdvancedSearch, openDocsTreeSearch} from './advancedSearch/actions'
import {formDataConfigSelector} from './formData'
import {loadLocationsSuggestions, setLocationSuggestions, validateLocation} from './locations/actions'
import {loadRelationEntities} from './relationEntities/actions'
import {openRemoteCreate} from './remoteCreate/actions'
import {loadSearchFilters} from './searchFilters/actions'
import {loadTerms} from './terms/actions'
import {loadTooltip} from './tooltips/actions'
import {setDocument, uploadDocument} from './upload/actions'
import {changeFieldValue, touchField, changePristineFieldValue} from './values/actions'

const FormData = props => <React.Fragment>{React.cloneElement(props.children, {formData: props})}</React.Fragment>

FormData.propTypes = {
  children: PropTypes.node
}

export const mapStateToProps = (
  state,
  {formValues, tooltips, locations, relationEntities, searchFilters, navigationStrategy}
) => {
  const config = formDataConfigSelector(state)
  return {
    ...(relationEntities
      ? {
          relationEntities: _pick(state.formData.relationEntities.data, relationEntities),
          relationEntitiesDefaultDisplays: state.formData.relationEntities.defaultDisplays
        }
      : {}),
    ...(tooltips ? {tooltips: _pick(state.formData.tooltips?.data, tooltips)} : {}),
    ...(searchFilters ? {searchFilters: _pick(state.formData.searchFilters, searchFilters)} : {}),
    ...(locations ? {locations: _pick(state.formData.locations, locations)} : {}),
    ...(formValues && state.form[formValues.formName]
      ? {formValues: _pick(state.form[formValues.formName].values, formValues.fields)}
      : {}),
    ...(navigationStrategy && config.navigationStrategy
      ? {navigationStrategy: config.navigationStrategy}
      : {navigationStrategy: undefined}),
    ...(config.chooseDocument ? {chooseDocument: config.chooseDocument} : {}),
    terms: state.formData.terms?.terms
  }
}

const mapActionCreators = {
  loadTerms,
  loadRelationEntities,
  loadTooltip,
  openAdvancedSearch,
  openDocsTreeSearch,
  uploadDocument,
  setDocument,
  changeFieldValue,
  changePristineFieldValue,
  touchField,
  loadSearchFilters,
  loadLocationsSuggestions,
  setLocationSuggestions,
  validateLocation,
  openRemoteCreate
}

const FormDataContainer = connect(mapStateToProps, mapActionCreators, null, {
  areStatePropsEqual: _isEqual
})(injectIntl(FormData))

export default FormDataContainer
