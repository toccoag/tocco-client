import _get from 'lodash/get'
import _uniq from 'lodash/uniq'
import {all, call} from 'redux-saga/effects'
import {api, cache} from 'tocco-util'

import rest from '../../rest'

const LOCATION_ENDPOINT = 'location/suggestions'
const countryCodeField = 'iso2'

// Caches country response to reduce resolving of key or ISO code.
// e.g. {'CH': {key: '22', display: 'Switzerland'}
const getCountryCache = () => cache.getObjectCache('country', 'all') || {}

export function* getCountryCodeByKey(key) {
  const countryCache = getCountryCache()

  for (const prop in countryCache) {
    if (countryCache[prop].key === key) {
      return prop
    }
  }

  const entity = yield call(rest.fetchEntity, 'Country', key, {paths: [countryCodeField]})
  const display = yield call(rest.fetchDisplay, 'Country', key)
  const countryCode = _get(entity, ['paths', countryCodeField, 'value'])

  const updatedCountryCache = {
    ...countryCache,
    [countryCode]: {key: entity.key, display}
  }
  cache.addObjectCache('country', 'all', updatedCountryCache)

  return countryCode
}

export function* loadCountries(suggestions) {
  const allCountries = yield all(suggestions.map(suggestion => suggestion.country))

  const allCountriesUniq = yield call(_uniq, allCountries)
  const countryCache = getCountryCache()

  const notLoaded = yield all(
    allCountriesUniq.reduce(
      (accumulator, currentValue) => [...accumulator, ...(countryCache[currentValue] ? [] : [currentValue])],
      []
    )
  )

  if (notLoaded.length > 0) {
    const query = {
      conditions: {
        [countryCodeField]: notLoaded
      },
      paths: [countryCodeField]
    }

    const countriesResponse = yield call(rest.fetchAllEntities, 'Country', query, {method: 'GET'})
    const displays = yield call(rest.fetchDisplays, api.getDisplayRequest(countriesResponse))

    const updatedCountryCache = {
      ...countryCache,
      ...countriesResponse.reduce(
        (acc, value) => ({
          ...acc,
          [_get(value, ['paths', countryCodeField, 'value'])]: {
            key: value.key,
            display: _get(displays, ['Country', value.key])
          }
        }),
        {}
      )
    }
    cache.addObjectCache('country', 'all', updatedCountryCache)
    return updatedCountryCache
  }

  return countryCache
}

export const transformToSuggestions = (suggestions, countries) =>
  suggestions.map(suggestion => transformToSuggestion(suggestion, countries))

export const transformToSuggestion = (suggestion, countries) => {
  const street = getAddressLine(suggestion)

  const newObj = {
    ...suggestion,
    country: countries[suggestion.country]
  }

  delete newObj.street
  delete newObj.houseNr

  if (street) {
    newObj.street = street
  }

  return newObj
}

const getAddressLine = suggestion => {
  const {street, houseNr} = suggestion
  if (houseNr) {
    return `${street} ${houseNr}`
  }
  return street
}

export function* requestSuggestions(street, city, postcode, country) {
  const options = {
    queryParams: {
      ...(street ? {street} : {}),
      ...(city ? {city} : {}),
      ...(postcode ? {postcode} : {}),
      ...(country ? {country} : {})
    },
    method: 'GET'
  }

  const response = yield call(rest.requestSaga, LOCATION_ENDPOINT, options)
  return response.body.data
}

export function* requestValidation(location) {
  const url = `${LOCATION_ENDPOINT}/validate`

  const options = {
    body: location,
    method: 'POST'
  }

  const response = yield call(rest.requestSaga, url, options)
  return response.body
}

export function* getCountry(country, fieldCountries) {
  return country ? yield call(getCountryCodeByKey, country.key) : fieldCountries
}
