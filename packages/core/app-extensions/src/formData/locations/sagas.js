import {all, call, put, takeLatest} from 'redux-saga/effects'

import * as actions from './actions'
import {
  getCountry,
  loadCountries,
  requestSuggestions,
  requestValidation,
  transformToSuggestion,
  transformToSuggestions
} from './utils'

export default function* sagas() {
  yield all([
    takeLatest(actions.LOAD_LOCATION_SUGGESTIONS, loadLocations),
    takeLatest(actions.VALIDATE_LOCATION, validateLocation)
  ])
}

export function* loadLocations({payload: {field, searchInput, countryValue, fieldCountries}}) {
  yield put(actions.setLocationSuggestionsLoading(field))

  const {street, city, postcode} = searchInput
  const country = yield call(getCountry, countryValue, fieldCountries)
  const suggestionsResponse = yield call(requestSuggestions, street, city, postcode, country)

  const countries = yield call(loadCountries, suggestionsResponse)
  const suggestions = yield call(transformToSuggestions, suggestionsResponse, countries)

  yield put(actions.setLocationSuggestions(field, suggestions))
}

export function* validateLocation({payload: {location, fieldCountries, callbackAction}}) {
  const requestData = {
    ...location,
    country: yield call(getCountry, location.country, fieldCountries)
  }

  const validatedLocation = yield call(requestValidation, requestData)

  const countries = yield call(loadCountries, [validatedLocation])
  const result = yield call(transformToSuggestion, validatedLocation, countries)

  callbackAction(result)
}
