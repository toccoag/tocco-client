export const UPLOAD_DOCUMENT = 'formData/UPLOAD_DOCUMENT'
export const SET_DOCUMENT = 'formData/SET_DOCUMENT'

export const uploadDocument = (formName, field, file, onChange) => ({
  type: UPLOAD_DOCUMENT,
  payload: {
    formName,
    file,
    field,
    onChange
  }
})

export const setDocument = (formName, field, resourceId, onChange) => ({
  type: SET_DOCUMENT,
  payload: {
    formName,
    field,
    resourceId,
    onChange
  }
})
