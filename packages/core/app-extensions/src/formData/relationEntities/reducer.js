import _merge from 'lodash/merge'

import * as actions from './actions'

export const setRelationEntities = (state, {payload: {fieldName, entities, moreEntitiesAvailable, searchTerm}}) => ({
  ...state,
  data: {
    ...state.data,
    [fieldName]: {
      data: entities,
      isLoading: false,
      moreEntitiesAvailable,
      searchTerm
    }
  }
})

export const setRelationEntitiesLoading = (state, {payload: {fieldName, clearData}}) => ({
  ...state,
  data: {
    ...state.data,
    [fieldName]: {
      ...(clearData
        ? {
            searchTerm: undefined,
            data: []
          }
        : {
            searchTerm: state.data[fieldName]?.searchTerm,
            data: state.data[fieldName]?.data || []
          }),
      isLoading: true
    }
  }
})

export const mergeDefaultDisplays = (state, {payload: {displays}}) => ({
  ...state,
  defaultDisplays: _merge(state.defaultDisplays, displays)
})

const ACTION_HANDLERS = {
  [actions.SET_RELATION_ENTITIES]: setRelationEntities,
  [actions.SET_RELATION_ENTITIES_LOADING]: setRelationEntitiesLoading,
  [actions.ADD_RELATION_ENTITIES_DEFAULT_DISPLAYS]: mergeDefaultDisplays
}

const initialState = {
  data: {},
  defaultDisplays: {}
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
