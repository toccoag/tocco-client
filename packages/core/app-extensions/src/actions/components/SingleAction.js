import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'
import {Button} from 'tocco-ui'

import {isValidSelection, selectionText} from './selectionHelper'

export const SingleAction = ({definition, onClick, selectedCount, disabled, intl}) => {
  const shouldUseLabel = def => !(def.useLabel === 'NO' || def.useLabel === 'HIDE')
  const isDisabled = definition.readonly === true || !isValidSelection(selectedCount, definition) || disabled
  const label = definition.label && shouldUseLabel(definition) ? definition.label : ''
  const title = selectionText(selectedCount, definition, intl) || (!shouldUseLabel(definition) ? definition.label : '')
  const handleClick = e => {
    onClick(definition)
    e.stopPropagation()
  }
  const styleAsIcon = definition.buttonType === 'ICON'
  const styleAsText = definition.buttonType === 'TEXT'

  const buttonStyle = definition.buttonStyle?.toLowerCase()
  const otherProps =
    definition.buttonType === 'REGULAR'
      ? {
          look: 'raised',
          ...(buttonStyle === 'paper' ? {} : {ink: buttonStyle})
        }
      : {
          look: styleAsIcon || styleAsText ? 'flat' : 'raised',
          withoutBackground: styleAsIcon || styleAsText
        }

  return (
    <Button
      iconOnly={styleAsIcon}
      removePadding={styleAsText}
      data-cy={`btn-action-${definition.id}`}
      onClick={handleClick}
      icon={definition.icon}
      label={label}
      title={title}
      disabled={isDisabled}
      {...otherProps}
    />
  )
}

SingleAction.propTypes = {
  intl: PropTypes.object.isRequired,
  definition: PropTypes.object.isRequired,
  onClick: PropTypes.func,
  selectedCount: PropTypes.number,
  disabled: PropTypes.bool
}

export default injectIntl(SingleAction)
