import _omit from 'lodash/omit'
import PropTypes from 'prop-types'
import {useEffect} from 'react'

import selectionHelper from '../../selection'
import componentTypes from '../actionComponentTypes'
import {modeFitsScopes} from '../actions'

import {doesActionConditionMatch, gatherActionConditionNames} from './actionConditionHelper'
import ActionGroup from './ActionGroup'
import SingleAction from './SingleAction'

const ActionVisual = ({
  definition,
  onClick,
  selection,
  parent,
  mode,
  callback,
  disabled,
  customRenderedActions,
  actionConditions,
  addActionCondition
}) => {
  useEffect(() => {
    const conditionNames = gatherActionConditionNames(definition)
    if (selection.type === 'ID' && selection.ids.length === 1 && conditionNames.length > 0) {
      // entity name can be ignored as condition belongs to a specific model
      conditionNames.forEach(conditionName => addActionCondition(selection.ids[0], conditionName))
    }
  }, [definition, selection, addActionCondition])

  if (!doesActionConditionMatch(definition, selection, actionConditions)) {
    return null
  }

  if (customRenderedActions && customRenderedActions[definition.id]) {
    return customRenderedActions[definition.id](definition)
  }

  if (!modeFitsScopes(mode, definition.scopes)) {
    return null
  }

  const ActionType = definition.componentType === componentTypes.ACTION_GROUP ? ActionGroup : SingleAction

  return (
    <ActionType
      definition={definition}
      onClick={currentDefinition => {
        onClick(_omit(currentDefinition, ['label']), selection, parent, callback)
      }}
      disabled={disabled}
      actionConditions={actionConditions}
      selection={selection}
      selectedCount={selection.count}
    />
  )
}

ActionVisual.propTypes = {
  /**
   * Object as derived from the form definition.
   */
  definition: PropTypes.shape({
    id: PropTypes.string,
    type: PropTypes.string,
    useLabel: PropTypes.string,
    icon: PropTypes.string,
    label: PropTypes.string,
    config: PropTypes.object,
    scopes: PropTypes.arrayOf(PropTypes.string),
    componentType: PropTypes.string,
    children: PropTypes.array,
    buttonType: PropTypes.oneOf(['REGULAR', 'ICON', 'TEXT']),
    buttonStyle: PropTypes.oneOf(['PAPER', 'PRIMARY']),
    conditionName: PropTypes.string
  }).isRequired,
  selection: selectionHelper.propType.isRequired,
  onClick: PropTypes.func.isRequired,
  callback: PropTypes.func,
  /**
   * Used to render action conditionally if mode fits definition mode.
   */
  mode: PropTypes.oneOf(['create', 'update']),
  /**
   * Depending on the context a parent entity of the current entity can be passed. This information
   * can be important when an action is executed.
   */
  parent: PropTypes.shape({
    key: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    reverseRelationName: PropTypes.string
  }),
  disabled: PropTypes.bool,
  /**
   * Used if a custom Rendering is desired. E.g. save button
   */
  customRenderedActions: PropTypes.objectOf(PropTypes.func),
  /**
   * object of evaluated action conditions if action should be showed
   */
  actionConditions: PropTypes.object,
  /**
   * action to add action condition and evaluate if action should be showed
   */
  addActionCondition: PropTypes.func
}

const Action = props => {
  if (props.definition.componentType === componentTypes.ACTION_BAR) {
    return props.definition.children.map(child => (
      <ActionVisual key={`action-${props.definition.id}-${child.id}`} {...props} definition={child} />
    ))
  }

  return <ActionVisual {...props} />
}

Action.propTypes = {
  definition: PropTypes.shape({
    id: PropTypes.string,
    componentType: PropTypes.string,
    children: PropTypes.array
  }).isRequired
}

export default Action
