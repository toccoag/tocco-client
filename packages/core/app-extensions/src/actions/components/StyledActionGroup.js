import styled from 'styled-components'
import {scale} from 'tocco-ui'

export const StyledActionGroupIcon = styled.span`
  margin-right: ${scale.space(-1.9)};
`
