import PropTypes from 'prop-types'
import {useState, useMemo} from 'react'
import {injectIntl} from 'react-intl'

import simpleFormConnector from '../containers/simpleFormConnector'
import {
  getCustomValues,
  getFormDefinition,
  getGroupedValues,
  reportSettingsDefinitionPropType,
  transformValues
} from '../utils/report'

import {StyledReportSettings} from './StyledReportSettings'

export const ReportSettings = ({settingsDefinition, formApp, onSubmit, listApp, docsApp, intl}) => {
  const msg = id => intl.formatMessage({id})
  const hasCustomSettings = settingsDefinition.customSettings?.entity
  const [settings, setSettings] = useState({
    valid: false,
    customSettingsValid: !hasCustomSettings
  })

  const formDefinition = getFormDefinition(settingsDefinition, intl)
  const customSettingsFormDefinition = hasCustomSettings ? settingsDefinition.customSettings.form.form : undefined

  const SimpleFormContainer = useMemo(() => simpleFormConnector(formApp), [formApp])

  const handleSettingsChange = ({values, valid}) => {
    setSettings(s => ({
      ...s,
      values,
      valid
    }))
  }

  const getForm = () => {
    if (customSettingsFormDefinition) {
      return {...formDefinition, children: [...formDefinition.children, ...customSettingsFormDefinition.children]}
    }
    return formDefinition
  }

  const handleSubmit = () => {
    const customSettings = hasCustomSettings ? getCustomValues(customSettingsFormDefinition, settings.values) : {}

    const groupedValues = {
      ...getGroupedValues(settingsDefinition, transformValues(settings.values)),
      customSettings
    }

    onSubmit(groupedValues)
  }

  return (
    <StyledReportSettings>
      <SimpleFormContainer
        listApp={listApp}
        docsApp={docsApp}
        form={getForm()}
        onChange={handleSettingsChange}
        submitText={msg('client.common.report.generate')}
        onSubmit={handleSubmit}
        mode="create"
        labelPosition="inside"
      />
    </StyledReportSettings>
  )
}

ReportSettings.propTypes = {
  intl: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired,
  listApp: PropTypes.func.isRequired,
  docsApp: PropTypes.func.isRequired,
  formApp: PropTypes.func.isRequired,
  settingsDefinition: reportSettingsDefinitionPropType.isRequired
}

export default injectIntl(ReportSettings)
