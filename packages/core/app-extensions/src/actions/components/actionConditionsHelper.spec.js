import {remoteLogger} from 'tocco-util'

import {doesActionConditionMatch, gatherActionConditionNames} from './actionConditionHelper'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('components', () => {
      describe('actionConditionsHelper', () => {
        describe('doesActionConditionMatch', () => {
          const conditionName = 'condition name'
          const actionConditions = {
            [conditionName]: {
              1: true,
              2: false
            }
          }
          test('should pass if no condition is defined', () => {
            const definition = {}
            expect(doesActionConditionMatch(definition, {}, actionConditions)).to.be.true
          })

          test('should log and pass if is query selection', () => {
            const logErrorFn = jest.fn()
            jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

            const definition = {conditionName}
            const selection = {type: 'QUERY'}
            expect(doesActionConditionMatch(definition, selection, actionConditions)).to.be.true
          })

          test('should log and pass if is multiple selection', () => {
            const logErrorFn = jest.fn()
            jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

            const definition = {conditionName}
            const selection = {type: 'ID', ids: ['1', '2']}
            expect(doesActionConditionMatch(definition, selection, actionConditions)).to.be.true
          })

          test('should pass if condition is met', () => {
            const definition = {conditionName}
            const selection = {type: 'ID', ids: ['1']}
            expect(doesActionConditionMatch(definition, selection, actionConditions)).to.be.true
          })

          test('should not pass if condition is not evaluated yet', () => {
            const definition = {conditionName}
            const selection = {type: 'ID', ids: ['3']}
            expect(doesActionConditionMatch(definition, selection, actionConditions)).to.be.false
          })

          test('should not pass if condition is not met', () => {
            const definition = {conditionName}
            const selection = {type: 'ID', ids: ['2']}
            expect(doesActionConditionMatch(definition, selection, actionConditions)).to.be.false
          })
        })

        describe('gatherActionConditionNames', () => {
          test('should find top most condition', () => {
            const definition = {conditionName: 'top condition'}
            expect(gatherActionConditionNames(definition)).to.deep.eq(['top condition'])
          })

          test('should find conditions on all levels', () => {
            const definition = {
              conditionName: 'top condition',
              children: [
                {
                  children: [{conditionName: 'bottom condition'}]
                }
              ]
            }
            expect(gatherActionConditionNames(definition)).to.deep.eq(['top condition', 'bottom condition'])
          })

          test('should remove duplicates', () => {
            const definition = {
              conditionName: 'top condition',
              children: [
                {
                  conditionName: 'top condition',
                  children: [{conditionName: 'bottom condition'}, {conditionName: 'bottom condition'}]
                }
              ]
            }
            expect(gatherActionConditionNames(definition)).to.deep.eq(['top condition', 'bottom condition'])
          })
        })
      })
    })
  })
})
