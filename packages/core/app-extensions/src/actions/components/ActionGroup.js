import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'
import {ButtonMenu, Icon, MenuItem} from 'tocco-ui'

import selectionHelper from '../../selection'

import {doesActionConditionMatch} from './actionConditionHelper'
import {isValidSelection, selectionText} from './selectionHelper'
import {StyledActionGroupIcon} from './StyledActionGroup'

const getChildMenuItems = input => {
  const {onClick, definition, intl, index, actionConditions, selection} = input
  const selectedCount = selection.count

  if (!doesActionConditionMatch(definition, selection, actionConditions)) {
    return null
  }

  const validSelection = isValidSelection(selectedCount, definition)
  const disabled = definition.readonly === true || !validSelection
  const title = selectionText(selectedCount, definition, intl)
  const isActionGroup = definition.componentType === 'action-group'
  const icon = isActionGroup ? definition.icon : undefined

  const children = definition.children
    ?.map(child => getChildMenuItems({...input, definition: child}))
    ?.filter(child => child)
  if (definition.componentType === 'action-group' && children.length === 0) {
    return null
  }

  return (
    <MenuItem
      {...(!isActionGroup && !disabled && {onClick: () => onClick(definition)})}
      disabled={disabled}
      title={title}
      key={`MenuItem-${definition.id}-${index}`}
      data-cy={`menuitem-${definition.id}`}
    >
      <span>
        {icon && (
          <StyledActionGroupIcon>
            <Icon icon={icon} />
          </StyledActionGroupIcon>
        )}
        {definition.label}
      </span>
      {definition.componentType === 'action-group' && children}
    </MenuItem>
  )
}

const ActionGroup = props => {
  const {definition, onClick} = props
  const hasDefaultAction = definition.defaultAction && Object.keys(definition.defaultAction).length > 0

  const label = hasDefaultAction ? definition.defaultAction.label : definition.label
  const onClickHandler = hasDefaultAction
    ? () => {
        onClick(definition.defaultAction)
      }
    : null
  const actionId = hasDefaultAction ? definition.defaultAction.id : definition.id
  const children = definition.children
    .map((childDefinition, idx) => getChildMenuItems({...props, index: idx, definition: childDefinition}))
    .filter(child => child)

  if (children.length === 0) {
    return null
  }

  return (
    <ButtonMenu
      buttonProps={{look: 'raised'}}
      label={label}
      data-cy={`btn-action-${actionId}`}
      onClick={onClickHandler}
      icon={definition.icon}
    >
      {children}
    </ButtonMenu>
  )
}

ActionGroup.propTypes = {
  intl: PropTypes.object.isRequired,
  definition: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  actionConditions: PropTypes.object,
  selection: selectionHelper.propType.isRequired
}

export default injectIntl(ActionGroup)
