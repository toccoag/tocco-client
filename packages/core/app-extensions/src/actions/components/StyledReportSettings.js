import styled from 'styled-components'
import {scale} from 'tocco-ui'

export const StyledReportSettings = styled.div`
  margin-bottom: ${scale.space(1)};

  && {
    > div:first-of-type {
      margin-bottom: 10px;
    }
  }
`
