import PropTypes from 'prop-types'
import {withTheme} from 'styled-components'
import {LoadMask} from 'tocco-ui'
import {bundle} from 'tocco-util'

const DynamicAction = ({packageName, appName, theme, ...props}) => {
  const ActionApp = bundle.useBundledApp({
    packageName,
    appName
  })

  return ActionApp ? <ActionApp customTheme={theme} {...props} /> : <LoadMask />
}

DynamicAction.propTypes = {
  packageName: PropTypes.string.isRequired,
  appName: PropTypes.string.isRequired,
  theme: PropTypes.object
}

export default withTheme(DynamicAction)
