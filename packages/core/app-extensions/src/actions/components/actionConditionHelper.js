import _get from 'lodash/get'
import _uniq from 'lodash/uniq'
import {remoteLogger} from 'tocco-util'

export const gatherActionConditionNames = definition => {
  const conditionNames = [definition.conditionName]
  if (definition.children?.length > 0) {
    definition.children
      .flatMap(child => gatherActionConditionNames(child))
      .forEach(conditionName => conditionNames.push(conditionName))
  }
  return _uniq(conditionNames.filter(conditionName => conditionName?.length > 0))
}

export const doesActionConditionMatch = (definition, selection, actionConditions) => {
  if (definition.conditionName) {
    if (selection.type === 'ID' && selection.ids.length === 1) {
      if (!_get(actionConditions, [definition.conditionName, selection.ids[0]], false)) {
        return false
      }
    } else {
      remoteLogger.logError('Action conditions are only allowed for ID selections of a single entity')
    }
  }
  return true
}
