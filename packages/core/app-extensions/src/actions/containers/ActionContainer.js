import {connect} from 'react-redux'

import Action from '../components/Action'
import {actionInvoke} from '../modules/actions'
import {addActionCondition} from '../modules/actions/actions'

const mapActionCreators = {
  onClick: actionInvoke,
  addActionCondition
}

const mapStateToProps = (state, props) => ({
  actionConditions: state.actions.actionConditions
})

export default connect(mapStateToProps, mapActionCreators)(Action)
