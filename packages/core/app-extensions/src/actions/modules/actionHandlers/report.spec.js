import {channel} from 'redux-saga'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {env} from 'tocco-util'
import {v4 as uuid} from 'uuid'

import notification from '../../../notification'
import {MODAL} from '../../../notification/modules/modal/actions'
import rest from '../../../rest'

import reportSaga, {
  handleReportInAdmin,
  handleReportInWidget,
  displayReportSettings,
  getSettingsDefinition,
  generateReport
} from './report'
import {invokeActionAsync} from './simpleAction'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('modules', () => {
      describe('actionHandler', () => {
        describe('report', () => {
          const mockData = {
            definition: {
              reportId: 'test_report'
            },
            selection: {
              entity: 'User',
              ids: [1, 2, 33]
            },
            settingsDefinition: {
              description: {
                name: 'Sample Report'
              },
              customSettings: {
                entity: {
                  name: 'SessionOnlyEntity'
                }
              },
              generalSettings: [
                {
                  id: 'filename',
                  defaultValue: 'Name'
                },
                {
                  id: 'corporateDesign',
                  defaultValue: {
                    key: '1'
                  }
                }
              ]
            },
            formValues: {
              customSettings: {someBool: false}
            }
          }

          describe('handler', () => {
            test('should handle report in admin', () => {
              env.setEmbedType('admin')

              return expectSaga(reportSaga, mockData.definition, mockData.selection)
                .provide([[matchers.call.fn(handleReportInAdmin), {}]])
                .call(handleReportInAdmin, mockData.definition, mockData.selection, undefined)
                .run()
            })

            test('should handle report in widget', () => {
              env.setEmbedType('widget')

              return expectSaga(reportSaga, mockData.definition, mockData.selection)
                .provide([[matchers.call.fn(handleReportInWidget), {}]])
                .call(handleReportInWidget, mockData.definition, mockData.selection, undefined)
                .run()
            })
          })

          describe('handleReportInAdmin', () => {
            test('handleReportInAdmin', () => {
              const settingsModalId = 'abc-def-123-456'
              return expectSaga(handleReportInAdmin, mockData.definition, null)
                .provide([
                  [matchers.call.fn(getSettingsDefinition), mockData.settingsDefinition],
                  [channel, {}],
                  {
                    take() {
                      return {}
                    }
                  },
                  [matchers.call.fn(displayReportSettings), settingsModalId],
                  [matchers.call.fn(generateReport), {}]
                ])
                .put(notification.removeModal(settingsModalId))
                .run()
            })
          })

          describe('handleReportInWidget', () => {
            test('has no custom settings', () => {
              const specificSettingsDefinition = {
                ...mockData.settingsDefinition,
                customSettings: null
              }
              return expectSaga(handleReportInWidget, mockData.definition, null)
                .provide([
                  [matchers.call.fn(getSettingsDefinition), specificSettingsDefinition],
                  [matchers.call.fn(generateReport), {}]
                ])
                .not.call.like({fn: displayReportSettings})
                .not.put.like(notification.removeModal())
                .run()
            })

            test('with custom settings', () => {
              const settingsModalId = 'abc-def-123-456'
              return expectSaga(handleReportInWidget, mockData.definition, null)
                .provide([
                  [matchers.call.fn(getSettingsDefinition), mockData.settingsDefinition],
                  [channel, {}],
                  {
                    take() {
                      return {}
                    }
                  },
                  [matchers.call.fn(displayReportSettings), settingsModalId],
                  [matchers.call.fn(generateReport), {}]
                ])
                .call.like({fn: displayReportSettings})
                .put(notification.removeModal(settingsModalId))
                .run()
            })
          })

          describe('displayReportSettings', () => {
            test('should open a modal component and returns an id', () => {
              const id = '04d39342-5e87-40eb-bd76-832435ccb147'
              return expectSaga(displayReportSettings, mockData.settingsDefinition, null, null)
                .provide([
                  [matchers.call.fn(getSettingsDefinition), mockData.settingsDefinition],
                  [matchers.call.fn(uuid), id]
                ])
                .put.like({action: {type: MODAL}})
                .returns(id)
                .run()
            })
          })

          describe('getSettingsDefinition', () => {
            test('load settings definition', () => {
              return expectSaga(getSettingsDefinition, mockData.settingsDefinition, mockData.selection)
                .provide([[matchers.call.fn(rest.requestSaga), {body: mockData.settingsDefinition}]])
                .returns(mockData.settingsDefinition)
                .run()
            })
          })

          describe('generateReport', () => {
            test('generateReport', () => {
              const settings = {}
              const definition = {
                reportId: 'test_report',
                endpoint: 'report/generations'
              }
              const params = {
                additionalProperties: {
                  reportId: mockData.definition.reportId,
                  generalSettings: undefined,
                  recipientSettings: undefined
                },
                formData: {
                  model: 'SessionOnlyEntity',
                  key: undefined,
                  version: undefined,
                  paths: {}
                }
              }

              return expectSaga(
                generateReport,
                mockData.definition,
                mockData.settingsDefinition,
                mockData.selection,
                settings
              )
                .provide([[matchers.call.fn(rest.requestSaga), {status: 200, body: {}}]])
                .call(invokeActionAsync, definition, mockData.selection, null, params)
                .run()
            })
          })
        })
      })
    })
  })
})
