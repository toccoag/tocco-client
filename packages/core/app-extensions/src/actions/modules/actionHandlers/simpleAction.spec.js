import {FormattedMessage} from 'react-intl'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {download} from 'tocco-util'

import notification from '../../../notification'
import {TOASTER_KEY_PREFIX} from '../../../notification/modules/socket/socket'
import {TOASTER} from '../../../notification/modules/toaster/actions'
import rest from '../../../rest'

import simpleAction, {invokeRequest, showToaster} from './simpleAction'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('modules', () => {
      describe('actionHandler', () => {
        describe('simpleAction', () => {
          describe('simpleAction', () => {
            test('should invoke request and return an remote event', async () => {
              const definition = {}
              const selection = {entityName: 'User'}
              const parent = {}
              const {returnValue} = await expectSaga(simpleAction, definition, selection, parent)
                .provide([[matchers.call.fn(invokeRequest), {success: true, flags: {reloadDetail: true}}]])
                .call.fn(invokeRequest)
                .run()
              expect(returnValue.success).to.be.true
              expect(returnValue.remoteEvents).to.have.length(1)
              expect(returnValue.remoteEvents[0]).to.eql({
                type: 'entity-update-event',
                payload: {entities: [{entityName: 'User'}], parent, reload: true}
              })
            })
          })

          describe('invokeRequest', () => {
            test('should try downloading', async () => {
              const definition = {}
              const selection = {entityName: 'User'}
              const parent = {}
              await expectSaga(invokeRequest, definition, selection, parent)
                .provide([
                  [matchers.call.fn(rest.requestSaga), {body: {params: {downloadUrl: 'download'}}}],
                  [matchers.call.fn(rest.requestBytesSaga), {body: new Blob(['data'])}]
                ])
                .call.like({fn: download.downloadReadableStream})
                .run()
            })

            test('should add notification title', async () => {
              const definition = {endpoint: 'endpoint', progressMsg: 'progress message'}
              const selection = {entityName: 'User'}
              const options = {
                method: 'POST',
                body: {
                  entity: 'User',
                  selection: {entityName: 'User'},
                  parent: {},
                  formProperties: undefined
                },
                acceptedErrorCodes: ['VALIDATION_FAILED', 'ERROR_ID'],
                blockingInfoTitle: 'progress message'
              }
              const parent = {}
              await expectSaga(invokeRequest, definition, selection, parent)
                .provide([[matchers.call.fn(rest.requestSaga), {body: {}}]])
                .call(rest.requestSaga, 'endpoint', options)
                .run()
            })

            test('should show error id', async () => {
              const definition = {}
              const selection = {entityName: 'User'}
              const parent = {}
              const errorId = 'abc123'
              await expectSaga(invokeRequest, definition, selection, parent)
                .provide([[matchers.call.fn(rest.requestSaga), {body: {errorCode: 'ERROR_ID', errorId}}]])
                .put(
                  notification.toaster({
                    type: 'error',
                    title: 'client.common.unexpectedError',
                    body: <FormattedMessage id="client.component.actions.errorIdText" values={{errorId}} />
                  })
                )
                .run()
            })
          })

          describe('showToaster', () => {
            test('showToaster', () => {
              const title = 'rest.action.task.scheduled'
              const notificationKey = '1'
              const key = `${TOASTER_KEY_PREFIX}${notificationKey}`
              const response = {body: {title, notificationKey}}
              const type = 'info'
              return expectSaga(showToaster, response, type)
                .put.like({
                  action: {
                    type: TOASTER,
                    payload: {
                      toaster: {
                        type,
                        title,
                        key
                      }
                    }
                  }
                })
                .run()
            })
          })
        })
      })
    })
  })
})
