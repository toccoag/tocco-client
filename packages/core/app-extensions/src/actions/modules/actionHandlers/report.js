import {channel} from 'redux-saga'
import {call, put, take} from 'redux-saga/effects'
import {api, env} from 'tocco-util'
import {v4 as uuid} from 'uuid'

import notification from '../../../notification'
import rest from '../../../rest'
import ReportSettings from '../../components/ReportSettings'

import {invokeActionAsync} from './simpleAction'

export default function* (actionDefinition, selection, parent, params, config) {
  if (env.isInWidgetEmbedded()) {
    yield call(handleReportInWidget, actionDefinition, selection, config)
  } else {
    yield call(handleReportInAdmin, actionDefinition, selection, config)
  }
}

export function* handleReportInAdmin(actionDefinition, selection, config) {
  const settingsDefinition = yield call(getSettingsDefinition, actionDefinition, selection)
  const answerChannel = yield call(channel)
  const settingsModalId = yield call(displayReportSettings, settingsDefinition, answerChannel, config)
  const settings = yield take(answerChannel)
  yield call(generateReport, actionDefinition, settingsDefinition, selection, settings)
  yield put(notification.removeModal(settingsModalId))
}

/*
 * For widgets only show custom settings, if there are no custom settings directly generate report
 */
export function* handleReportInWidget(actionDefinition, selection, config) {
  const settingsDefinition = yield call(getSettingsDefinition, actionDefinition, selection)

  const generalSettings = mapGeneralSettings(settingsDefinition)
  const recipientSettings = {}
  let customSettings

  if (settingsDefinition.customSettings) {
    // only show custom settings in modal
    const modifiedSettingsDefinition = {
      ...settingsDefinition,
      generalSettings: [],
      recipientSettings: []
    }
    const answerChannel = yield call(channel)
    const settingsModalId = yield call(displayReportSettings, modifiedSettingsDefinition, answerChannel, config)
    const settings = yield take(answerChannel)
    yield put(notification.removeModal(settingsModalId))
    customSettings = settings.customSettings
  } else {
    customSettings = null
  }
  const settings = {generalSettings, recipientSettings, customSettings}
  yield call(generateReport, actionDefinition, settingsDefinition, selection, settings)
}

const mapGeneralSettings = settingsDefinition =>
  settingsDefinition.generalSettings.reduce(
    (acc, field) => ({
      ...acc,
      [field.id]: field.defaultValue?.key || field.defaultValue
    }),
    {}
  )

export function* displayReportSettings(settingsDefinition, answerChannel, config) {
  const onSubmit = formValues => answerChannel.put(formValues)
  const settingsModalId = yield call(uuid)

  yield put(
    notification.modal(
      settingsModalId,
      settingsDefinition.description.name,
      null,
      () => (
        <ReportSettings
          listApp={config.listApp}
          docsApp={config.docsApp}
          formApp={config.formApp}
          onSubmit={onSubmit}
          settingsDefinition={settingsDefinition}
        />
      ),
      true
    )
  )

  return settingsModalId
}

export function* getSettingsDefinition(actionDefinition, selection) {
  const options = {
    body: selection,
    method: 'POST'
  }

  const resource = `report/${actionDefinition.reportId}/settings`
  const {body: settingsDefinition} = yield call(rest.requestSaga, resource, options)
  return settingsDefinition
}

export function* generateReport(actionDefinition, settingsDefinition, selection, settings) {
  const {generalSettings, recipientSettings, customSettings} = settings
  actionDefinition.endpoint = 'report/generations'
  const customSettingsEntity = settingsDefinition.customSettings
    ? api.toEntity({
        __model: settingsDefinition.customSettings.entity.name,
        ...customSettings
      })
    : null
  const params = {
    additionalProperties: {
      reportId: actionDefinition.reportId,
      generalSettings,
      recipientSettings
    },
    formData: customSettingsEntity
  }

  yield call(invokeActionAsync, actionDefinition, selection, null, params)
}
