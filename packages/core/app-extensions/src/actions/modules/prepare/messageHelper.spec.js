import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {generateMessage} from './messageHelper'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('prepare', () => {
      describe('messageHelper', () => {
        test('should return message if entities missing', () => {
          const preCheck = {
            confirmMessage: 'message'
          }
          const result = generateMessage(preCheck)
          expect(result).to.be.eql('message')
        })

        test('should return message if entities empty', () => {
          const preCheck = {
            confirmMessage: 'message',
            entities: {}
          }
          const result = generateMessage(preCheck)
          expect(result).to.be.eql('message')
        })

        test('should return message with entities', () => {
          const preCheck = {
            confirmMessage: 'message',
            entities: {
              User: ['1', '2', '3'],
              Address: ['2', '3', '4']
            }
          }
          const result = generateMessage(preCheck)
          testingLibrary.renderWithIntl(result)

          expect(screen.getByText('message')).to.exist
          expect(screen.getByText((content, element) => element.textContent === 'client.entities.User (3)')).to.exist
          expect(screen.getByText((content, element) => element.textContent === 'client.entities.Address (3)')).to.exist
        })
      })
    })
  })
})
