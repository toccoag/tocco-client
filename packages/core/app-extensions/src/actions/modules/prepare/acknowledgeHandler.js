import {FormattedMessage} from 'react-intl'
import {channel} from 'redux-saga'
import {call, put, take} from 'redux-saga/effects'

import notification from '../../../notification'

import {generateMessage} from './messageHelper'

export default function* acknowledgeHandler({preparationResponse, config: {navigationStrategy} = {}}) {
  if (
    preparationResponse.preCheck &&
    preparationResponse.preCheck.success === false &&
    preparationResponse.preCheck.confirmMessage
  ) {
    const {customTitle} = preparationResponse.preCheck
    yield call(promptAcknowledge, generateMessage(preparationResponse.preCheck, navigationStrategy), customTitle)
    return {abort: true}
  }

  return {abort: false}
}

export function* promptAcknowledge(message, customTitle) {
  const answerChannel = yield call(channel)
  const onAction = () => answerChannel.put(false)

  yield put(
    notification.info(
      customTitle || 'client.component.actions.confirmTitle',
      message,
      <FormattedMessage id="client.common.ok" />,
      onAction
    )
  )
  return yield take(answerChannel)
}
