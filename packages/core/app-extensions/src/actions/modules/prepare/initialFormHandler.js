import {channel} from 'redux-saga'
import {call, put, take} from 'redux-saga/effects'
import {api} from 'tocco-util'

import notification from '../../../notification'
import rest from '../../../rest'
import simpleFormConnector from '../../containers/simpleFormConnector'

import acknowledgeHandler from './acknowledgeHandler'
import confirmHandler from './confirmHandler'
import preCheckHandler from './preCheckHandler'
import {processHandlers} from './prepare'

export default function* initialFormHandler({preparationResponse, definition, selection, config}) {
  if (preparationResponse.initialFormValues) {
    const formValues = yield call(
      handleInitialForm,
      preparationResponse.initialFormValues,
      config,
      definition,
      selection
    )

    return {
      abort: formValues === null,
      params: {formData: formValues}
    }
  }
  return {
    abort: false
  }
}

export const wrapFormValues = formValues => ({formValues})

export function* handleInitialForm(
  {formDefinition, defaultValues, conditions, formTitle, formMessage, noButtons},
  config,
  definition,
  selection
) {
  const answerChannel = yield call(channel)

  const id = new Date().valueOf()
  const onSend = ({values}) => answerChannel.put(wrapFormValues(values))
  const cancelCallback = () => answerChannel.put({cancel: true})
  const SimpleFormContainer = simpleFormConnector(config.formApp)
  const modifiedForm = addConditionsToFormDefinition(formDefinition.form, conditions)
  const modal = notification.modal(
    id,
    formTitle,
    formMessage,
    () => (
      <SimpleFormContainer
        form={modifiedForm}
        detailApp={config.detailApp}
        listApp={config.listApp}
        docsApp={config.docsApp}
        onSubmit={onSend}
        defaultValues={defaultValues}
        mode="create"
        entityName={formDefinition.model}
        labelPosition="inside"
        noButtons={noButtons}
        navigationStrategy={config.navigationStrategy || {}}
      />
    ),
    true,
    cancelCallback
  )
  yield put(modal)
  const toasterKey = `${id}-error-toaster`
  while (true) {
    const response = yield take(answerChannel)
    if (response.cancel) {
      // remove validation toaster if modal is closed
      yield put(notification.removeToaster(toasterKey, false))
      return null
    }
    const formData = response.formValues ? {model: formDefinition.model, paths: response.formValues} : null
    const validationSuccessful = yield call(validateForm, formData, definition, selection, toasterKey, config)

    if (validationSuccessful) {
      yield put(notification.removeModal(id))
      return formData
    }
  }
}

export const addConditionsToFormDefinition = (formDefinition, conditions) => {
  if (!conditions || Object.keys(conditions).length === 0) {
    return formDefinition
  }

  return {
    ...formDefinition,
    ...(api.relationFieldTypes.includes(formDefinition.dataType) && conditions[formDefinition.id]
      ? {condition: conditions[formDefinition.id]}
      : {}),
    ...(formDefinition.children
      ? {children: formDefinition.children.map(c => addConditionsToFormDefinition(c, conditions))}
      : {})
  }
}

function* doValidationRequest(formData, definition, selection) {
  const response = yield call(rest.requestSaga, `${definition.endpoint}/validate`, {
    method: 'POST',
    body: {
      entity: selection.entityName,
      selection,
      formData,
      formProperties: definition.properties
    }
  })
  return response.body
}

// this list is a subset of the handlers in ./prepare
const getHandlers = () => [acknowledgeHandler, preCheckHandler, confirmHandler]

function* validateForm(formData, definition, selection, toasterKey, config) {
  const validationResponse = definition.endpoint ? yield call(doValidationRequest, formData, definition, selection) : {}
  const handlers = yield call(getHandlers)

  const {abort, abortMessage} = yield call(processHandlers, definition, selection, config, validationResponse, handlers)
  // reuse same toaster if the validation fails mutliple times
  if (abort && abortMessage) {
    yield put(
      notification.toaster({
        key: toasterKey,
        type: 'warning',
        title: 'client.component.actions.validationFailed',
        body: abortMessage
      })
    )
  } else {
    // remove old validation message if validation was successful
    yield put(notification.removeToaster(toasterKey, false))
  }

  return !abort
}
