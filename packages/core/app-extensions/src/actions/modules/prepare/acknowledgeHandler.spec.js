import {channel} from 'redux-saga'
import {expectSaga} from 'redux-saga-test-plan'

import {INFO} from '../../../notification/modules/interactive/actions'

import acknowledgeHandler from './acknowledgeHandler'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('prepare', () => {
      describe('acknowledgeHandler', () => {
        test('should prompt a info and return abort true', () => {
          const preparationResponse = {
            preCheck: {
              success: false,
              confirmMessage: 'Are you sure?'
            }
          }

          const channelMock = channel()

          return expectSaga(acknowledgeHandler, {preparationResponse})
            .provide([
              {
                call(effect, next) {
                  return effect.fn === channel ? channelMock : next()
                }
              }
            ])
            .put.like({action: {type: INFO}})
            .dispatch(channelMock.put(true))
            .returns({
              abort: true
            })
            .run()
        })

        test('should not prompt a info if not asked for', () => {
          const preparationResponse = {
            preCheck: {
              success: false,
              confirmMessage: null
            }
          }

          return expectSaga(acknowledgeHandler, {preparationResponse})
            .not.put.like({action: {type: INFO}})
            .returns({
              abort: false
            })
            .run()
        })

        test('should not prompt a info if sucessful precheck', () => {
          const preparationResponse = {
            preCheck: {
              success: true,
              confirmMessage: 'Are you sure?'
            }
          }

          return expectSaga(acknowledgeHandler, {preparationResponse})
            .not.put.like({action: {type: INFO}})
            .returns({
              abort: false
            })
            .run()
        })
      })
    })
  })
})
