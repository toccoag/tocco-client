import {channel} from 'redux-saga'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import {MODAL} from '../../../notification/modules/modal/actions'

import initialFormHandler, {addConditionsToFormDefinition} from './initialFormHandler'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('prepare', () => {
      describe('initialFormHandler', () => {
        test('should dispatch confirm and return abort eql false in case of positive answer', () => {
          const preparationResponse = {
            initialFormValues: {
              formDefinition: {
                model: 'User',
                form: {}
              },
              formData: {},
              formTitle: 'title',
              formMessage: 'message'
            }
          }
          const definition = {}

          const channelMock = channel()

          return expectSaga(initialFormHandler, {preparationResponse, definition, config: {formApp: () => {}}})
            .provide([
              {
                call(effect, next) {
                  return effect.fn === channel ? channelMock : next()
                }
              }
            ])
            .put.like({action: {type: MODAL}})
            .dispatch(channelMock.put({formValues: {firstname: 'test'}}))
            .returns({
              abort: false,
              params: {formData: {model: 'User', paths: {firstname: 'test'}}}
            })
            .run()
        })

        test('should validate: first time fails but second time is successful', () => {
          const preparationResponse = {
            initialFormValues: {
              formDefinition: {
                model: 'User',
                form: {}
              },
              formData: {},
              formTitle: 'title',
              formMessage: 'message'
            }
          }
          const definition = {endpoint: 'moduleName/actions/actionName'}
          const selection = {
            entityName: 'User'
          }

          const resource = 'moduleName/actions/actionName/validate'
          const getOptions = paths => ({
            method: 'POST',
            body: {
              entity: selection.entityName,
              selection,
              formData: {model: 'User', paths},
              formProperties: definition.properties
            }
          })

          const firstPaths = {firstname: ''}
          const firstResponse = {body: {preCheck: {success: false, message: 'custom message'}}}

          const secondPaths = {firstname: 'test'}
          const secondResponse = {body: {preCheck: {success: true}}}

          const channelMock = channel()

          return expectSaga(initialFormHandler, {
            preparationResponse,
            definition,
            selection,
            config: {formApp: () => {}}
          })
            .provide([
              {
                call(effect, next) {
                  return effect.fn === channel ? channelMock : next()
                }
              },
              [matchers.call(rest.requestSaga, resource, getOptions(firstPaths)), firstResponse],
              [matchers.call(rest.requestSaga, resource, getOptions(secondPaths)), secondResponse]
            ])
            .put.like({action: {type: MODAL}})
            .dispatch(channelMock.put({formValues: firstPaths}))
            .put.like({action: {type: 'notification/TOASTER'}})
            .dispatch(channelMock.put({formValues: secondPaths}))
            .put.like({action: {type: 'notification/REMOVE_TOASTER'}})
            .returns({
              abort: false,
              params: {formData: {model: 'User', paths: {firstname: 'test'}}}
            })
            .run()
        })

        test('should abort loop if modal is closed', () => {
          const preparationResponse = {
            initialFormValues: {
              formDefinition: {
                model: 'User',
                form: {}
              },
              formData: {},
              formTitle: 'title',
              formMessage: 'message'
            }
          }
          const definition = {endpoint: 'moduleName/actions/actionName'}
          const selection = {
            entityName: 'User'
          }

          const channelMock = channel()

          return expectSaga(initialFormHandler, {
            preparationResponse,
            definition,
            selection,
            config: {formApp: () => {}}
          })
            .provide([
              {
                call(effect, next) {
                  return effect.fn === channel ? channelMock : next()
                }
              }
            ])
            .put.like({action: {type: MODAL}})
            .dispatch(channelMock.put({cancel: true}))
            .put.like({action: {type: 'notification/REMOVE_TOASTER'}})
            .returns({
              abort: true,
              params: {formData: null}
            })
            .run()
        })

        test('should return abort in case of negative answer', () => {
          const preparationResponse = {
            initialFormValues: {
              formDefinition: {form: {}},
              formData: {},
              formTitle: 'title',
              formMessage: 'message'
            }
          }
          const definition = {}

          const channelMock = channel()

          return expectSaga(initialFormHandler, {preparationResponse, definition, config: {formApp: () => {}}})
            .provide([
              {
                call(effect, next) {
                  return effect.fn === channel ? channelMock : next()
                }
              }
            ])
            .put.like({action: {type: MODAL}})
            .dispatch(channelMock.put({formValues: null}))
            .returns({
              abort: true,
              params: {formData: null}
            })
            .run()
        })
        test('should not show form and return abort false if no form is defined', () => {
          const preparationResponse = {initialFormValues: null}

          return expectSaga(initialFormHandler, {preparationResponse, config: {formApp: () => {}}})
            .not.put.like({action: {type: MODAL}})
            .returns({
              abort: false
            })
            .run()
        })
      })

      describe('addConditionsToFormDefinition', () => {
        const formDefinition = {
          componentType: 'form',
          children: [
            {
              componentType: 'layout',
              children: [
                {
                  id: 'relEvent_status',
                  componentType: 'field-set',
                  children: [
                    {
                      id: 'relEvent_status',
                      dataType: 'single-remote-field'
                    }
                  ]
                },
                {
                  id: 'description',
                  componentType: 'field-set',
                  children: [
                    {
                      id: 'description',
                      dataType: 'string'
                    }
                  ]
                }
              ]
            }
          ]
        }

        test('if no conditions are set do not modify form', () => {
          const conditions = {}
          const modifiedForm = addConditionsToFormDefinition(formDefinition, conditions)
          expect(modifiedForm).to.equal(formDefinition)
        })

        test('if no condition matches a field do not modify form', () => {
          const conditions = {relEvent_type: 'system_entity'}
          const modifiedForm = addConditionsToFormDefinition(formDefinition, conditions)
          expect(modifiedForm).to.deep.equal(formDefinition)
        })

        test('if condition matches a field do modify form', () => {
          const conditions = {relEvent_status: 'evaluation_allowed'}
          const modifiedForm = addConditionsToFormDefinition(formDefinition, conditions)
          expect(modifiedForm).to.not.deep.equal(formDefinition)
          expect(modifiedForm.children[0].children[0].children[0].condition).to.equal('evaluation_allowed')
        })
      })
    })
  })
})
