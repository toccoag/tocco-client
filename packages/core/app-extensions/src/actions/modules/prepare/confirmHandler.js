import {FormattedMessage} from 'react-intl'
import {channel} from 'redux-saga'
import {call, put, take} from 'redux-saga/effects'

import notification from '../../../notification'

import {generateMessage} from './messageHelper'

export default function* confirmHandler({preparationResponse, config: {navigationStrategy} = {}}) {
  if (
    preparationResponse.preCheck &&
    preparationResponse.preCheck.success === true &&
    preparationResponse.preCheck.confirmMessage
  ) {
    const {defaultAction, customTitle} = preparationResponse.preCheck
    const confirmResponse = yield call(
      promptConfirm,
      generateMessage(preparationResponse.preCheck, navigationStrategy),
      defaultAction,
      customTitle
    )
    return {abort: !confirmResponse}
  }

  return {abort: false}
}

export function* promptConfirm(message, defaultAction, customTitle) {
  const answerChannel = yield call(channel)
  const onYes = () => answerChannel.put(true)
  const onCancel = () => answerChannel.put(false)

  yield put(
    notification.confirm(
      customTitle || 'client.component.actions.confirmTitle',
      message,
      <FormattedMessage id="client.common.ok" />,
      <FormattedMessage id="client.common.cancel" />,
      onYes,
      onCancel,
      defaultAction
    )
  )
  return yield take(answerChannel)
}
