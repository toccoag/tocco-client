import _isEmpty from 'lodash/isEmpty'
import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Typography} from 'tocco-ui'
import {navigationStrategy as navigationStrategyPropTypes} from 'tocco-util'

export const EntitiesMessage = ({message, entities, navigationStrategy}) => {
  return (
    <>
      <Typography.P>{message}</Typography.P>
      <Typography.P>
        {Object.entries(entities)
          .map(([model, keys]) => (
            <Typography.Span key={model}>
              <FormattedMessage id={`client.entities.${model}`} /> (
              {navigationStrategy?.ListLink ? (
                <navigationStrategy.ListLink entityName={model} entityKeys={keys.slice(0, 100)}>
                  {keys.length}
                </navigationStrategy.ListLink>
              ) : (
                <Typography.Span>{keys.length}</Typography.Span>
              )}
              )
            </Typography.Span>
          ))
          .reduce((acc, curr) => [...acc, curr, ', '], [])
          .slice(0, -1)}
      </Typography.P>
    </>
  )
}

EntitiesMessage.propTypes = {
  message: PropTypes.string.isRequired,
  entities: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string)).isRequired,
  navigationStrategy: navigationStrategyPropTypes.propTypes
}

export const generateMessage = (preCheck, navigationStrategy) => {
  const {confirmMessage, entities} = preCheck
  if (_isEmpty(entities)) {
    return confirmMessage
  } else {
    return <EntitiesMessage message={confirmMessage} entities={entities} navigationStrategy={navigationStrategy} />
  }
}
