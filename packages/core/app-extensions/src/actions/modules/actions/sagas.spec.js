import {all, debounce, select, takeEvery} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'

import rest from '../../../rest'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('modules', () => {
      describe('actions', () => {
        describe('sagas', () => {
          describe('root saga', () => {
            test('should handle actions', () => {
              const generator = rootSaga()
              expect(generator.next().value).to.deep.equal(
                all([
                  debounce(10, actions.ADD_ACTION_CONDITION, sagas.evaluationActionConditions),
                  takeEvery(actions.REEVALUATE_ACTION_CONDITION, sagas.reevaluateActionCondition)
                ])
              )
              expect(generator.next().done).to.be.true
            })
          })

          describe('evaluationActionConditions', () => {
            test('evaluationActionConditions', () => {
              const actionsSelector = {
                actionConditions: {
                  'my-condition': {
                    1: null,
                    2: true,
                    3: false
                  },
                  'other-condition': {
                    1: null,
                    2: null,
                    4: null
                  }
                }
              }

              const conditions = [
                {
                  conditionName: 'my-condition',
                  keys: ['1']
                },
                {
                  conditionName: 'other-condition',
                  keys: ['1', '2', '4']
                }
              ]

              return expectSaga(sagas.evaluationActionConditions)
                .provide([
                  [select(sagas.actionsSelector), actionsSelector],
                  [matchers.call.fn(sagas.doRequest), {}]
                ])
                .call(sagas.doRequest, conditions)
                .run()
            })
          })

          describe('reevaluateActionCondition', () => {
            test('reevaluateActionCondition', () => {
              const key = '2'

              const actionsSelector = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: true,
                    3: false
                  },
                  'other-condition': {
                    1: false,
                    2: false,
                    4: false
                  }
                }
              }

              const conditions = [
                {
                  conditionName: 'my-condition',
                  keys: ['2']
                },
                {
                  conditionName: 'other-condition',
                  keys: ['2']
                }
              ]

              return expectSaga(sagas.reevaluateActionCondition, {payload: {key}})
                .provide([
                  [select(sagas.actionsSelector), actionsSelector],
                  [matchers.call.fn(sagas.doRequest), {}]
                ])
                .call(sagas.doRequest, conditions)
                .run()
            })
          })

          describe('doRequest', () => {
            test('doRequest', () => {
              const conditions = [
                {
                  conditionName: 'my-condition',
                  keys: ['1']
                },
                {
                  conditionName: 'other-condition',
                  keys: ['1', '2', '4']
                }
              ]

              const request = {
                method: 'POST',
                body: {
                  conditions: [
                    {
                      conditionName: 'my-condition',
                      keys: ['1']
                    },
                    {
                      conditionName: 'other-condition',
                      keys: ['1', '2', '4']
                    }
                  ]
                }
              }
              const results = {
                'my-condition': {
                  1: true
                },
                'other-condition': {
                  1: false,
                  2: true,
                  4: false
                }
              }
              return expectSaga(sagas.doRequest, conditions)
                .provide([[matchers.call.fn(rest.requestSaga, 'client/actionConditions', request), {body: {results}}]])
                .put(actions.setActionConditionResults(results))
                .run()
            })

            test('empty list to evaluate', () => {
              const conditions = []

              return expectSaga(sagas.doRequest, conditions)
                .not.put.like({action: {type: actions.SET_ACTION_CONDITION_RESULTS}})
                .run()
            })
          })
        })
      })
    })
  })
})
