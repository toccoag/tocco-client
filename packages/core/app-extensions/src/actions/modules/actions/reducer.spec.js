import * as actions from './actions'
import reducer from './reducer'

describe('app-extensions', () => {
  describe('actions', () => {
    describe('modules', () => {
      describe('actions', () => {
        describe('reducer', () => {
          describe('addActionCondition', () => {
            test('add new condition', () => {
              const stateBefore = {
                actionConditions: {
                  'other-condition': {
                    3: null
                  }
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: null
                  },
                  'other-condition': {
                    3: null
                  }
                }
              }

              expect(reducer(stateBefore, actions.addActionCondition('1', 'my-condition'))).to.deep.equal(
                expectedStateAfter
              )
            })

            test('add new key to existing condition', () => {
              const stateBefore = {
                actionConditions: {
                  'my-condition': {
                    1: true
                  }
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: null
                  }
                }
              }

              expect(reducer(stateBefore, actions.addActionCondition('2', 'my-condition'))).to.deep.equal(
                expectedStateAfter
              )
            })

            test('try adding existing key to condition but existing value has a higher priority', () => {
              const stateBefore = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: null
                  }
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: null
                  }
                }
              }

              expect(reducer(stateBefore, actions.addActionCondition('1', 'my-condition'))).to.deep.equal(
                expectedStateAfter
              )
            })
          })

          describe('setActionConditionResults', () => {
            test('set result for all keys of condition', () => {
              const stateBefore = {
                actionConditions: {
                  'my-condition': {
                    1: null
                  },
                  'other-condition': {
                    3: null
                  }
                }
              }
              const results = {
                'my-condition': {
                  1: true
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: true
                  },
                  'other-condition': {
                    3: null
                  }
                }
              }

              expect(reducer(stateBefore, actions.setActionConditionResults(results))).to.deep.equal(expectedStateAfter)
            })

            test('set result for some keys of condition', () => {
              const stateBefore = {
                actionConditions: {
                  'my-condition': {
                    1: null,
                    2: null
                  }
                }
              }
              const results = {
                'my-condition': {
                  1: true
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: null
                  }
                }
              }

              expect(reducer(stateBefore, actions.setActionConditionResults(results))).to.deep.equal(expectedStateAfter)
            })

            test('set result for multiple conditions', () => {
              const stateBefore = {
                actionConditions: {
                  'my-condition': {
                    1: null,
                    2: null
                  },
                  'other-condition': {
                    2: null,
                    3: null
                  }
                }
              }
              const results = {
                'my-condition': {
                  1: true,
                  2: false
                },
                'other-condition': {
                  3: true
                }
              }
              const expectedStateAfter = {
                actionConditions: {
                  'my-condition': {
                    1: true,
                    2: false
                  },
                  'other-condition': {
                    2: null,
                    3: true
                  }
                }
              }

              expect(reducer(stateBefore, actions.setActionConditionResults(results))).to.deep.equal(expectedStateAfter)
            })
          })
        })
      })
    })
  })
})
