import _merge from 'lodash/merge'

import * as actions from './actions'

const addActionCondition = (state, {payload: {key, condition}}) => ({
  ...state,
  actionConditions: {
    ...state.actionConditions,
    [condition]: {
      // null means that the action condition is yet not evaluated for this key
      [key]: null,
      // already existing key-value pairs have a higher priority else the result is overriden and loaded again
      ...state.actionConditions[condition]
    }
  }
})

const setActionConditionResults = (state, {payload: {results}}) => {
  return {
    ...state,
    actionConditions: {
      ...state.actionConditions,
      ..._merge(
        {},
        ...Object.entries(results).map(([key, value]) => ({
          [key]: {
            ...state.actionConditions[key],
            ...value
          }
        }))
      )
    }
  }
}

const ACTION_HANDLERS = {
  [actions.ADD_ACTION_CONDITION]: addActionCondition,
  [actions.SET_ACTION_CONDITION_RESULTS]: setActionConditionResults
}

const initialState = {
  actionConditions: {}
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
