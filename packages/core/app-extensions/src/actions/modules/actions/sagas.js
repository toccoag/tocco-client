import {all, call, debounce, put, select, takeEvery} from 'redux-saga/effects'

import rest from '../../../rest'

import * as actions from './actions'

export const actionsSelector = state => state.actions

export default function* sagas() {
  yield all([
    // debounce is used that not for each action-entity combination a single request is done
    debounce(10, actions.ADD_ACTION_CONDITION, evaluationActionConditions),
    takeEvery(actions.REEVALUATE_ACTION_CONDITION, reevaluateActionCondition)
  ])
}

export function* evaluationActionConditions() {
  const {actionConditions} = yield select(actionsSelector)

  const conditions = Object.entries(actionConditions).map(([key, value]) => ({
    conditionName: key,
    keys: mapNullValuesKeysToList(value)
  }))

  yield call(doRequest, conditions)
}

export function* reevaluateActionCondition({payload: {key}}) {
  const {actionConditions} = yield select(actionsSelector)

  const conditions = Object.keys(actionConditions).map(conditionName => ({
    conditionName,
    keys: [key]
  }))

  yield call(doRequest, conditions)
}

export function* doRequest(conditions) {
  if (conditions.length === 0) {
    return
  }

  const {
    body: {results}
  } = yield call(rest.requestSaga, 'client/actionConditions', {
    method: 'POST',
    body: {
      conditions
    }
  })

  yield put(actions.setActionConditionResults(results))
}

const mapNullValuesKeysToList = obj => {
  return Object.entries(obj)
    .filter(([key, value]) => value === null)
    .map(([key, value]) => key)
}
