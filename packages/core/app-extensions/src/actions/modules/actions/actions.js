export const ADD_ACTION_CONDITION = 'actions/ADD_ACTION_CONDITION'
export const SET_ACTION_CONDITION_RESULTS = 'actions/SET_ACTION_CONDITION_RESULT'
export const REEVALUATE_ACTION_CONDITION = 'actions/REEVALUATE_ACTION_CONDITION'

export const addActionCondition = (key, condition) => ({
  type: ADD_ACTION_CONDITION,
  payload: {
    key,
    condition
  }
})

export const setActionConditionResults = results => ({
  type: SET_ACTION_CONDITION_RESULTS,
  payload: {
    results
  }
})

export const reevaluateActionCondition = key => ({
  type: REEVALUATE_ACTION_CONDITION,
  payload: {
    key
  }
})
