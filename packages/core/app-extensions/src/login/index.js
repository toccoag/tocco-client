import {doSessionCheck, setAdminAllowed, setLoggedIn, SET_LOGGED_IN} from './actions'
import {addToStore} from './login'
import {doRequest, doSessionRequest} from './sagas'

export default {
  addToStore,
  setLoggedIn,
  SET_LOGGED_IN,
  setAdminAllowed,
  doSessionCheck,
  doSessionRequest,
  doRequest
}
