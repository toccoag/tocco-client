import {Typography} from 'tocco-ui'

const MultipleFieldsSeparator = () => <Typography.Span breakWords={false}>;&nbsp;</Typography.Span>

export default MultipleFieldsSeparator
