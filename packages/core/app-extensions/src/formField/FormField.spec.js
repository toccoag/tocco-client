import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import FormField from './FormField'

describe('app-extensions', () => {
  describe('formField', () => {
    describe('FormField', () => {
      const getStore = () =>
        configureStore({
          reducer: () => ({
            formData: {
              relationEntities: {data: {}},
              tooltips: {data: {}},
              config: {configSelector: () => ({})}
            },
            form: {
              detailForm: {}
            }
          })
        })

      test('should return a StatedValue with correct EditableValue', () => {
        const formDefinitionField = {
          name: 'firstname',
          dataType: 'string',
          readonly: false,
          children: [],
          label: 'Vorname'
        }
        const formFieldData = {
          formDefinitionField
        }

        testingLibrary.renderWithStore(<FormField fieldMappingType="editable" data={formFieldData} />, {
          store: getStore()
        })

        expect(screen.queryByText('Vorname')).to.exist
        expect(screen.queryByRole('textbox')).to.exist
      })

      test('should return array with separator', () => {
        const formDefinitionField = {
          name: 'firstname',
          dataType: 'string',
          readonly: false,
          children: [],
          label: 'Vorname'
        }
        const formFieldData = {
          formDefinitionField,
          value: ['V1', 'V2']
        }

        testingLibrary.renderWithStore(<FormField fieldMappingType="editable" data={formFieldData} />, {
          store: getStore()
        })

        expect(
          screen.getByText(
            (_content, element) => element.childNodes.length > 1 && element.textContent.startsWith(`V1;\u00A0V2`)
          )
        ).to.exist
      })

      test('should handle empty array as value', () => {
        const formDefinitionField = {
          name: 'firstname',
          dataType: 'string',
          readonly: false,
          children: [],
          label: 'Vorname'
        }
        const formFieldData = {
          formDefinitionField,
          value: []
        }

        testingLibrary.renderWithStore(<FormField fieldMappingType="editable" data={formFieldData} />, {
          store: getStore()
        })

        expect(screen.queryAllByText(',')).to.have.length(0)
      })

      test('should not return separator with only one value', () => {
        const formDefinitionField = {
          name: 'firstname',
          dataType: 'string',
          readonly: false,
          children: [],
          label: 'Vorname'
        }
        const formFieldData = {
          formDefinitionField,
          value: ['V1']
        }

        testingLibrary.renderWithStore(<FormField fieldMappingType="editable" data={formFieldData} />, {
          store: getStore()
        })

        expect(screen.queryAllByText('V1')).to.have.length(1)
        expect(screen.queryAllByText(',')).to.have.length(0)
      })
    })
  })
})
