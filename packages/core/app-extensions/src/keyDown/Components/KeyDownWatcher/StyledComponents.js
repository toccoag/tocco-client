import styled from 'styled-components'
import {themeSelector} from 'tocco-ui'

export const StyledKeyDownWatcher = styled.div`
  display: contents;

  :focus-visible {
    outline: 2px solid ${themeSelector.color('signal.info')};
  }
`
