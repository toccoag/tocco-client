import PropTypes from 'prop-types'
import {useCallback, useEffect} from 'react'

import {getMatchingConfig} from '../../utils'

import {StyledKeyDownWatcher} from './StyledComponents'

const KeyDownWatcher = ({config, children, keyDownHandler, ...props}) => {
  const handleKeyDown = useCallback(
    (event, global = false) => {
      const matchingConfig = getMatchingConfig(config, event, global)
      if (matchingConfig) {
        event.preventDefault()
        event.stopPropagation()
      }
      keyDownHandler(matchingConfig)
    },
    [config, keyDownHandler]
  )

  useEffect(() => {
    const onDocumentKeyDown = event => handleKeyDown(event, true)

    document.addEventListener('keydown', onDocumentKeyDown)

    return () => {
      document.removeEventListener('keydown', onDocumentKeyDown)
    }
  }, [handleKeyDown])

  return (
    <StyledKeyDownWatcher tabIndex="0" onKeyDown={handleKeyDown} {...props}>
      {children}
    </StyledKeyDownWatcher>
  )
}

KeyDownWatcher.propTypes = {
  children: PropTypes.node,
  config: PropTypes.array.isRequired,
  keyDownHandler: PropTypes.func.isRequired
}

export default KeyDownWatcher
