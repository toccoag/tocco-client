import {connect} from 'react-redux'

import InitLoadMask from './InitLoadMask'

const mapStateToProps = state => ({
  intlInitialised: state.app ? state.app.intlInitialised : true,
  cacheInitialised: state.app ? state.app.cacheInitialised : true
})

export default connect(mapStateToProps)(InitLoadMask)
