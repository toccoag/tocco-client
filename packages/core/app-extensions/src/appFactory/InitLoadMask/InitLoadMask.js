import PropTypes from 'prop-types'
import {LoadMask} from 'tocco-ui'

const InitLoadMask = ({children, intlInitialised, cacheInitialised}) => (
  <LoadMask required={[cacheInitialised, intlInitialised]}>{children}</LoadMask>
)

InitLoadMask.propTypes = {
  children: PropTypes.any,
  intlInitialised: PropTypes.bool,
  cacheInitialised: PropTypes.bool
}

export default InitLoadMask
