import {renderApp, reloadApp, createApp, registerAppInRegistry, createBundleableApp} from './appFactory'
import {reinitialiseCache, forceCacheInvalidation} from './store/app'
import {setInput, inputChanged, INPUT_CHANGED, INPUT_INITIALIZED} from './store/input'
import {createStore} from './store/store'
import {setThemeType, initThemeType} from './store/theme'
import useApp, {getEvents} from './useApp'

export default {
  renderApp,
  reloadApp,
  createApp,
  createStore,
  registerAppInRegistry,
  createBundleableApp,
  useApp,
  getEvents,
  setInput,
  inputChanged,
  initThemeType,
  setThemeType,
  INPUT_CHANGED,
  INPUT_INITIALIZED,
  reinitialiseCache,
  forceCacheInvalidation
}
