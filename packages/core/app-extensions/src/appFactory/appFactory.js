import _union from 'lodash/union'
import {createRoot} from 'react-dom/client'
import {consoleLogger} from 'tocco-util'

import errorLogging from '../errorLogging'

import App from './App'
import {setTextResourceModules, changeLocale, initialiseCache} from './store/app'
import {inputInitialized} from './store/input'
import {initThemeType} from './store/theme'

export const createApp = (
  name,
  content,
  store,
  {input = {}, actions = undefined, publicPath = undefined, textResourceModules = []}
) => {
  try {
    const theme = input.customTheme

    if (publicPath) {
      setWebpacksPublicPath(publicPath)
    }

    if (actions) {
      dispatchActions(actions, store)
    }

    store.dispatch(inputInitialized())

    const locale = input ? input.locale : null
    const actualTextResourceModules = _union([name], textResourceModules)
    store.dispatch(setTextResourceModules(actualTextResourceModules))
    store.dispatch(initialiseCache({shouldSetupIntl: true, locale}))
    store.dispatch(initThemeType())

    const setLocale = async newLocale => {
      return store.dispatch(changeLocale(newLocale))
    }

    return {
      component: <App store={store} content={content} theme={theme} />,
      store,
      methods: {setLocale}
    }
  } catch (error) {
    try {
      store.dispatch(errorLogging.logError('Error', 'Error creating react application: ', error))
    } catch (loggingError) {
      consoleLogger.logError('Error creating react application: ', error)
      consoleLogger.logError('Unable to log error: ', loggingError)
    }
  }
}

export const renderApp = (app, mountElementName = 'root') => {
  const mountElement = document.getElementById(mountElementName)
  const root = createRoot(mountElement)
  root.render(app)
}

export const reloadApp = (app, mountElementName = 'root') => {
  const mountElement = document.getElementById(mountElementName)
  const root = createRoot(mountElement)
  root.unmount()
  renderApp(app, mountElementName)
}

export const registerAppInRegistry = (appName, initFunction) => {
  if (window.reactRegistry) {
    window.reactRegistry.register(appName, initFunction)
  }
}

export const createBundleableApp = (name, init, app) => ({
  name,
  init,
  App: app,
  setWebpacksPublicPath
})

const dispatchActions = (actions, store) => {
  actions.forEach(action => {
    store.dispatch(action)
  })
}

const setWebpacksPublicPath = publicPath => {
  /* eslint camelcase: 0 */
  __webpack_public_path__ = publicPath
}
