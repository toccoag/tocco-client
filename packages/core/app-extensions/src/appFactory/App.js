import PropTypes from 'prop-types'
import {useCallback} from 'react'
import {IntlProvider} from 'react-intl-redux/lib'
import {Provider} from 'react-redux'
import {GlobalAppClassName} from 'tocco-ui'

import keyDown from '../keyDown'

import InitLoadMask from './InitLoadMask'
import {StyledApp} from './StyledComponents'
import ThemeWrapper from './ThemeWrapperContainer'
import './styles.css'

const App = ({store, content, theme}) => {
  const wrapperCallback = useCallback(node => {
    if (node) {
      import(/* webpackChunkName: "vendor-fontawesome" */ '@fortawesome/fontawesome-svg-core').then(fontawesome => {
        fontawesome.dom.watch({
          autoReplaceSvgRoot: node,
          observeMutationsRoot: node
        })
      })
    }
  }, [])

  return (
    <Provider store={store}>
      <ThemeWrapper customTheme={theme}>
        <keyDown.KeyDownWatcher className={GlobalAppClassName}>
          <InitLoadMask>
            <IntlProvider>
              <StyledApp ref={wrapperCallback}>{content}</StyledApp>
            </IntlProvider>
          </InitLoadMask>
        </keyDown.KeyDownWatcher>
      </ThemeWrapper>
    </Provider>
  )
}

App.propTypes = {
  store: PropTypes.object.isRequired,
  content: PropTypes.node.isRequired,
  theme: PropTypes.object
}

export default App
