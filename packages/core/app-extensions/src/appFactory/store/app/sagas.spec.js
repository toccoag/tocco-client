import {takeLatest} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {viewPersistor} from 'tocco-util'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'
import {clearLongTermCache, clearObjectCache, hasInvalidCache, setupLongTermCache, setupObjectCache} from './utils'

describe('app-extensions', () => {
  describe('appFactory', () => {
    describe('store', () => {
      describe('app', () => {
        describe('sagas', () => {
          describe('root saga', () => {
            test('should fork sagas', () => {
              const saga = testSaga(rootSaga)
              saga
                .next()
                .all([
                  takeLatest(actions.SETUP_INTL, sagas.setupIntl),
                  takeLatest(actions.CHANGE_LOCALE, sagas.changeLocale),
                  takeLatest(actions.INITIALISE_CACHE, sagas.initCache)
                ])
            })
          })

          describe('initCache', () => {
            beforeEach(() => {
              sagas.resetCacheInitialised()
            })

            afterEach(() => {
              sagas.resetCacheInitialised()
            })

            test('should clear invalid object cache', () => {
              const cacheInfo = {invalidObjectCache: true, invalidLongTermCache: false, cache: {username: 'willi'}}
              return expectSaga(sagas.initCache, {payload: {locale: undefined}})
                .provide([[matchers.call.fn(hasInvalidCache), cacheInfo]])
                .call(hasInvalidCache, {locale: undefined})
                .call(clearObjectCache)
                .call(viewPersistor.clearPersistedViews)
                .not.call(clearLongTermCache)
                .call(setupObjectCache, cacheInfo.cache)
                .not.call.fn(setupLongTermCache)
                .put(actions.setCacheInitialised(true))
                .run()
            })

            test('should clear invalid longterm cache', () => {
              const cacheInfo = {invalidObjectCache: true, invalidLongTermCache: true, cache: {revision: 'abc'}}
              return expectSaga(sagas.initCache, {payload: {locale: undefined}})
                .provide([[matchers.call.fn(hasInvalidCache), cacheInfo]])
                .call(hasInvalidCache, {locale: undefined})
                .call(clearObjectCache)
                .call(viewPersistor.clearPersistedViews)
                .call(clearLongTermCache)
                .call(setupObjectCache, cacheInfo.cache)
                .call(setupLongTermCache, cacheInfo.cache)
                .put(actions.setCacheInitialised(true))
                .run()
            })

            test('should keep valid cache', () => {
              const cacheInfo = {invalidObjectCache: false, invalidLongTermCache: false, cache: {username: 'willi'}}
              return expectSaga(sagas.initCache, {payload: {locale: undefined}})
                .provide([[matchers.call.fn(hasInvalidCache), cacheInfo]])
                .call(hasInvalidCache, {locale: undefined})
                .not.call(clearObjectCache)
                .not.call(clearLongTermCache)
                .not.call.fn(setupObjectCache)
                .not.call.fn(setupLongTermCache)
                .put(actions.setCacheInitialised(true))
                .run()
            })

            test('should use forced locale if provided', () => {
              const cacheInfo = {invalidObjectCache: false, invalidLongTermCache: false, cache: {username: 'willi'}}
              return expectSaga(sagas.initCache, {payload: {locale: 'de'}})
                .provide([[matchers.call.fn(hasInvalidCache), cacheInfo]])
                .call(hasInvalidCache, {locale: 'de'})
                .not.call(clearObjectCache)
                .not.call(clearLongTermCache)
                .not.call.fn(setupObjectCache)
                .not.call.fn(setupLongTermCache)
                .put(actions.setCacheInitialised(true))
                .run()
            })

            test('should run only once', async () => {
              const cacheInfo = {invalidObjectCache: false, invalidLongTermCache: false, cache: {username: 'willi'}}
              await expectSaga(sagas.initCache, {payload: {locale: undefined}})
                .provide([[matchers.call.fn(hasInvalidCache), cacheInfo]])
                .run()

              return expectSaga(sagas.initCache, {payload: {locale: undefined}})
                .not.call(hasInvalidCache, {locale: undefined})
                .not.call(clearObjectCache)
                .not.call(clearLongTermCache)
                .not.call.fn(setupObjectCache)
                .not.call.fn(setupLongTermCache)
                .put(actions.setCacheInitialised(true))
                .run()
            })
          })
        })
      })
    })
  })
})
