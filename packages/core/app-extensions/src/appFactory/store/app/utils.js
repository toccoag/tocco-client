import {call} from 'redux-saga/effects'
import {intl, cache} from 'tocco-util'

import rest from '../../../rest'

export function* hasInvalidCache({locale: forcedLocale} = {}) {
  const revision = yield call(rest.fetchServerRevision)

  const userInfo = yield call(intl.loadUserWithLocale)
  const {locale, username, businessUnitId} = userInfo
  const actualLocale = forcedLocale || locale

  const {
    locale: cachedLocale,
    businessUnitId: cachedBusinessUnitId,
    username: cachedUsername
  } = cache.getObjectCacheMetaInfo()

  const {revision: cachedLongTermRevision} = cache.getLongTermMetaInfo()

  const hasLocaleChanged = !cachedLocale || cachedLocale !== actualLocale
  const hasBusinessUnitChanged = cachedBusinessUnitId !== businessUnitId
  const hasLoginChanged = cachedUsername !== username

  const hasLongTermRevisionChanged = cachedLongTermRevision !== revision

  return {
    invalidObjectCache: hasLocaleChanged || hasBusinessUnitChanged || hasLoginChanged,
    invalidLongTermCache: hasLongTermRevisionChanged,
    cache: {
      locale: actualLocale,
      businessUnitId,
      username,
      revision
    }
  }
}

export function* clearLongTermCache() {
  yield call(cache.clearLongTerm)
}

export function* clearObjectCache() {
  yield call(cache.clearObjectCache)
}

export function* setupLongTermCache({revision}) {
  yield call(cache.setLongTermMetaInfo, {revision})
}

export function* setupObjectCache({locale, businessUnitId, username}) {
  yield call(cache.setObjectCacheMetaInfo, {locale, businessUnitId, username})
}
