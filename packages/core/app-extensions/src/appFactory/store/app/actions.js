export const SETUP_INTL = 'app/SETUP_INTL'
export const CHANGE_LOCALE = 'app/CHANGE_LOCALE'
export const SET_TEXT_RESOURCE_MODULES = 'app/SET_TEXT_RESOURCE_MODULES'
export const SET_INTL_INITIALISED = 'app/SET_INTL_INITIALISED'
export const INITIALISE_CACHE = 'app/INITIALISE_CACHE'
export const SET_CACHE_INITIALISED = 'app/SET_CACHE_INITIALISED'

export const setupIntl = locale => ({
  type: SETUP_INTL,
  payload: {
    locale
  }
})

export const changeLocale = locale => ({
  type: CHANGE_LOCALE,
  payload: {
    locale
  }
})

export const setTextResourceModules = textResourceModules => ({
  type: SET_TEXT_RESOURCE_MODULES,
  payload: {
    textResourceModules
  }
})

export const setIntlInitialised = intlInitialised => ({
  type: SET_INTL_INITIALISED,
  payload: {
    intlInitialised
  }
})

export const initialiseCache = ({shouldSetupIntl = false, forceInvalidation = false, locale} = {}) => ({
  type: INITIALISE_CACHE,
  payload: {
    shouldSetupIntl,
    forceInvalidation,
    locale
  }
})

export const setCacheInitialised = cacheInitialised => ({
  type: SET_CACHE_INITIALISED,
  payload: {
    cacheInitialised
  }
})
