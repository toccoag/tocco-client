import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_TEXT_RESOURCE_MODULES]: reducerUtil.singleTransferReducer('textResourceModules'),
  [actions.SET_INTL_INITIALISED]: reducerUtil.singleTransferReducer('intlInitialised'),
  [actions.SET_CACHE_INITIALISED]: reducerUtil.singleTransferReducer('cacheInitialised')
}

const initialState = {
  textResourceModules: [],
  intlInitialised: false,
  cacheInitialised: false
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
