import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {intl, cache} from 'tocco-util'

import rest from '../../../rest'

import {hasInvalidCache} from './utils'

const clearCache = () => {
  cache.setLongTermMetaInfo({revision: ''})
  cache.setObjectCacheMetaInfo({locale: '', businessUnitId: '', username: ''})
}

describe('app-extensions', () => {
  describe('cache', () => {
    describe('utils', () => {
      describe('hasInvalidCache', () => {
        beforeEach(() => {
          clearCache()
        })

        afterEach(() => {
          clearCache()
        })

        const userInfo = {
          locale: 'de',
          username: 'hans',
          businessUnitId: 'test'
        }

        const setupCache = (overrideMetaInfo = {}) => {
          cache.setLongTermMetaInfo({revision: 'abc'})
          const defaultMetaInfo = {locale: 'de', businessUnitId: 'test', username: 'hans'}
          cache.setObjectCacheMetaInfo({...defaultMetaInfo, ...overrideMetaInfo})
        }

        test('should return valid if nothing has changed', () => {
          setupCache()

          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: false,
              invalidLongTermCache: false,
              cache: {
                ...userInfo,
                revision: 'abc'
              }
            })
            .run()
        })

        test('should return invalid if revision has changed', () => {
          setupCache()

          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'def'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: false,
              invalidLongTermCache: true,
              cache: {
                ...userInfo,
                revision: 'def'
              }
            })
            .run()
        })

        test('should return invalid if username has changed', () => {
          setupCache({username: 'susi'})

          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: true,
              invalidLongTermCache: false,
              cache: {
                ...userInfo,
                revision: 'abc'
              }
            })
            .run()
        })

        test('should return invalid if business unit has changed', () => {
          setupCache({businessUnitId: '123'})

          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: true,
              invalidLongTermCache: false,
              cache: {
                ...userInfo,
                revision: 'abc'
              }
            })
            .run()
        })

        test('should return invalid if locale has changed', () => {
          setupCache({locale: 'en'})

          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: true,
              invalidLongTermCache: false,
              cache: {
                ...userInfo,
                revision: 'abc'
              }
            })
            .run()
        })

        test('should use provided locale', () => {
          setupCache({locale: 'en'})

          return expectSaga(hasInvalidCache, {locale: 'en'})
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: false,
              invalidLongTermCache: false,
              cache: {
                ...userInfo,
                locale: 'en',
                revision: 'abc'
              }
            })
            .run()
        })

        test('should return invalid if nothing is in cache yet', () => {
          return expectSaga(hasInvalidCache)
            .provide([
              [matchers.call(rest.fetchServerRevision), 'abc'],
              [matchers.call(intl.loadUserWithLocale), userInfo]
            ])
            .returns({
              invalidObjectCache: true,
              invalidLongTermCache: true,
              cache: {
                ...userInfo,
                revision: 'abc'
              }
            })
            .run()
        })
      })
    })
  })
})
