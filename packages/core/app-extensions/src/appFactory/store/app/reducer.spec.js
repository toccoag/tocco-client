import * as actions from './actions'
import reducer from './reducer'

const INITIAL_STATE = {
  textResourceModules: [],
  intlInitialised: false,
  cacheInitialised: false
}

describe('app-extensions', () => {
  describe('appFactory', () => {
    describe('store', () => {
      describe('app', () => {
        describe('reducer', () => {
          test('should create a valid initial state', () => {
            expect(reducer(undefined, {})).to.deep.equal(INITIAL_STATE)
          })

          test('should set initialised', () => {
            const stateAfter = reducer(INITIAL_STATE, actions.setCacheInitialised(true))
            expect(stateAfter).to.have.property('cacheInitialised')
            expect(stateAfter.cacheInitialised).to.be.true
          })
        })
      })
    })
  })
})
