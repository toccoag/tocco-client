import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {intl, viewPersistor} from 'tocco-util'

import * as actions from './actions'
import {clearObjectCache, clearLongTermCache, hasInvalidCache, setupObjectCache, setupLongTermCache} from './utils'

const appSelector = state => state.app

let cacheInitialised = false
export const resetCacheInitialised = () => {
  cacheInitialised = false
}

export default function* mainSagas() {
  yield all([
    takeLatest(actions.SETUP_INTL, setupIntl),
    takeLatest(actions.CHANGE_LOCALE, changeLocale),
    takeLatest(actions.INITIALISE_CACHE, initCache)
  ])
}

export function* setIntlLocale(locale) {
  const {textResourceModules} = yield select(appSelector)
  const changeLocaleAction = yield call(intl.getChangeLocaleAction, textResourceModules, locale)
  yield put(changeLocaleAction)
}

export function* setupIntl({payload: {locale: forcedLocale}}) {
  let locale = forcedLocale
  if (!locale) {
    locale = yield call(intl.getUserLocale)
  }

  yield call(setIntlLocale, locale)
  yield put(actions.setIntlInitialised(true))
}

export function* changeLocale({payload: {locale}}) {
  yield call(reinitialiseCache, {locale})
  yield call(setIntlLocale, locale)
}

export function* initCache({payload: {shouldSetupIntl, forceInvalidation, locale: forcedLocale}}) {
  /**
   * Run cache initialisation only once per page request.
   * When we nest client packages they get empty stores and do not know if parent already
   * have initialised the cache. Use local variable in file therefore and update child store.
   */
  if (cacheInitialised) {
    yield put(actions.setCacheInitialised(true))
    if (shouldSetupIntl) {
      yield put(actions.setupIntl(forcedLocale))
    }
    return
  }

  const {
    invalidObjectCache: needsObjectCacheInvalidation,
    invalidLongTermCache: needsLongTermInvalidation,
    cache: cacheData
  } = yield call(hasInvalidCache, {locale: forcedLocale})

  if (needsLongTermInvalidation || forceInvalidation) {
    yield call(clearLongTermCache)
    yield call(setupLongTermCache, cacheData)
  }

  if (needsObjectCacheInvalidation || forceInvalidation) {
    yield call(clearObjectCache)
    yield call(setupObjectCache, cacheData)
    yield call(viewPersistor.clearPersistedViews)
  }

  cacheInitialised = true
  yield put(actions.setCacheInitialised(true))

  if (shouldSetupIntl) {
    yield put(actions.setupIntl(forcedLocale))
  }
}

/**
 * re-initialises cache and waits until cache has setup properly
 * should be invoked via saga directly so that waiting for setup is working
 * e.g. `yield call(appFactory.reinitialiseCache)`
 */
export function* reinitialiseCache({locale} = {}) {
  yield call(resetCacheInitialised)
  yield put(actions.initialiseCache({shouldSetupIntl: false, locale}))
  yield take(actions.SET_CACHE_INITIALISED)
}

/**
 * forcefully invalidates cache and waits until cache has setup properly
 * should be invoked via saga directly so that waiting for setup is working
 * e.g. `yield call(appFactory.forceCacheInvalidation)`
 */
export function* forceCacheInvalidation() {
  yield call(resetCacheInitialised)
  yield put(actions.initialiseCache({shouldSetupIntl: false, forceInvalidation: true}))
  yield take(actions.SET_CACHE_INITIALISED)
}
