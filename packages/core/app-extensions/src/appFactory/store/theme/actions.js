export const INIT_THEMETYPE = 'theme/INIT_THEMETYPE'
export const SET_THEMETYPE = 'theme/SET_THEMETYPE'

export const initThemeType = () => ({
  type: INIT_THEMETYPE
})

export const setThemeType = themeType => ({
  type: SET_THEMETYPE,
  payload: {
    themeType
  }
})
