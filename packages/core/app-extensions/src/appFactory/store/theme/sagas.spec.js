import {takeLatest} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {cache} from 'tocco-util'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('app-extensions', () => {
  describe('appFactory', () => {
    describe('store', () => {
      describe('theme', () => {
        describe('sagas', () => {
          describe('root saga', () => {
            test('should fork sagas', () => {
              const saga = testSaga(rootSaga)
              saga.next().all([takeLatest(actions.INIT_THEMETYPE, sagas.initThemeType)])
            })

            describe('initThemeType', () => {
              test('should use default theme type when cache is not set', () => {
                return expectSaga(sagas.initThemeType)
                  .provide([[matchers.call.fn(cache.getLongTerm), undefined]])
                  .put(actions.setThemeType('light'))
                  .run()
              })

              test('should use cached theme type when available', () => {
                return expectSaga(sagas.initThemeType)
                  .provide([[matchers.call.fn(cache.getLongTerm), 'dark']])
                  .put(actions.setThemeType('dark'))
                  .run()
              })
            })
          })
        })
      })
    })
  })
})
