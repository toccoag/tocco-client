import {initThemeType, setThemeType} from './actions'
import reducer from './reducer'
import sagas from './sagas'

export default reducer
export {sagas, initThemeType, setThemeType}
