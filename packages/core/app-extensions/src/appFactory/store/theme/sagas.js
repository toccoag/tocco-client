import {all, call, put, takeLatest} from 'redux-saga/effects'
import {cache} from 'tocco-util'

import * as actions from './actions'

export default function* mainSagas() {
  yield all([takeLatest(actions.INIT_THEMETYPE, initThemeType)])
}

export function* initThemeType() {
  const cachedThemeType = yield call(cache.getLongTerm, 'admin', 'theme')
  const themeType = cachedThemeType || 'light'

  yield put(actions.setThemeType(themeType))
}
