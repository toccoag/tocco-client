import {configureStore} from '@reduxjs/toolkit'
import {intlReducer} from 'react-intl-redux'
import {combineReducers} from 'redux'
import createSagaMiddleware from 'redux-saga'
import {fork} from 'redux-saga/effects'
import {reducer as reducerUtil, saga as sagaUtil} from 'tocco-util'

import errorLogging from '../../errorLogging'

import appReducer, {sagas as appSagas} from './app'
import inputReducer from './input'
import themeReducer, {sagas as themeSagas} from './theme'

export const getInitialState = input => {
  const initialState = window.__INITIAL_STATE__ || {}

  if (input) {
    initialState.input = input
    initialState.intl = {
      locale: input.locale // make sure the desired locale is set right from the start (instead of default 'en')
    }
  }
  return initialState
}

export const createStore = (reducers = {}, sagas = [], input = {}, name = '') => {
  const preloadedState = getInitialState(input)
  const sagaMiddleware = createSagaMiddleware()

  const devTools = __DEV__ ? {name, shouldHotReload: false} : undefined

  const allReducers = {
    ...reducers,
    input: inputReducer,
    app: appReducer,
    intl: intlReducer,
    theme: themeReducer
  }
  reducers = combineReducers(allReducers)
  const store = configureStore({
    reducer: reducers,
    devTools,
    preloadedState,
    middleware: getDefaultMiddleWare => [sagaMiddleware]
  })

  const allSagas = [appSagas, themeSagas, ...(sagas || [])]

  store.allReducers = allReducers
  store.sagas = allSagas
  store.sagaMiddleware = sagaMiddleware
  store.hotReloadReducers = reducerUtil.hotReloadReducers

  if (allSagas && allSagas.length > 0) {
    const rootSaga = sagaUtil.createGenerator(allSagas.map(s => fork(s)))
    sagaMiddleware.run(sagaUtil.autoRestartSaga(rootSaga, null, errorLogging.logError))
  }

  return store
}
