import _merge from 'lodash/merge'
import PropTypes from 'prop-types'
import {useMemo} from 'react'
import {ThemeProvider, withTheme, StyleSheetManager} from 'styled-components'
import {ToccoTheme, WidgetTheme, utils} from 'tocco-theme'
import {GlobalStyles} from 'tocco-ui'
import {env} from 'tocco-util'

const getDefaultTheme = themeType => {
  const embedType = env.getEmbedType()
  if (embedType === env.EmbedType.widget) {
    return WidgetTheme
  }

  return themeType === 'light' ? ToccoTheme.defaultTheme : ToccoTheme.darkTheme
}
const ThemeWrapper = ({themeType, customTheme, children, theme}) => {
  const defaultTheme = getDefaultTheme(themeType)
  const mergedTheme = useMemo(() => _merge({}, defaultTheme, customTheme), [defaultTheme, customTheme])

  return (
    <ThemeProvider theme={theme || mergedTheme}>
      <StyleSheetManager shouldForwardProp={utils.shouldForwardProp}>
        <GlobalStyles />
        {children}
      </StyleSheetManager>
    </ThemeProvider>
  )
}

ThemeWrapper.propTypes = {
  customTheme: PropTypes.object,
  theme: PropTypes.object,
  themeType: PropTypes.string,
  children: PropTypes.node
}

export default withTheme(ThemeWrapper)
