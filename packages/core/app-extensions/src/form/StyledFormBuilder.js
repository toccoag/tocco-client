import styled from 'styled-components'
import {scale, themeSelector} from 'tocco-ui'

export const StyledActionsWrapper = styled.div`
  background-color: ${themeSelector.color('paper')};
  padding-bottom: ${scale.space(-0.5)};
  display: flex;
  flex-wrap: wrap;
  z-index: 2; /* Higher than StyledIndicatorsContainerWrapper */
  position: ${({sticky}) => sticky && 'sticky'};
  top: ${({sticky, theme}) => sticky && (theme.specificProperties?.actionBarStickyTopPosition || 0)};

  & > * {
    margin-top: ${scale.space(-0.5)};
  }
`
