import {transformFieldName} from '../reduxForm'
import {getFormFieldDefinition, hasError} from '../utils'

import {mandatoryError} from './mandatory'
import {syncValidateField} from './syncValidation'

const getField = (id, formDefinition) => {
  if (formDefinition.id === id) {
    return getFormFieldDefinition(formDefinition)
  }

  if (!formDefinition.children || formDefinition.children.length === 0) {
    return null
  }

  return formDefinition.children.reduce((acc, child) => acc || getField(id, child), null)
}

export const locationValidator = (_value, fieldDefinition, formDefinition, values) => {
  const locationMapping = fieldDefinition.locationMapping

  const cityField = getField(locationMapping.city, formDefinition)
  const postcodeField = getField(locationMapping.postcode, formDefinition)

  const cityErrors = syncValidateField(cityField, values[transformFieldName(cityField.path)], {})
  const postcodeErrors = syncValidateField(postcodeField, values[transformFieldName(postcodeField.path)], {})

  return hasError(cityErrors) || hasError(postcodeErrors)
    ? {
        [fieldDefinition.id]: {
          ...cityErrors[transformFieldName(cityField.path)],
          ...postcodeErrors[transformFieldName(postcodeField.path)]
        }
      }
    : {}
}

export const choiceValidator = (_value, fieldDefinition, formDefinition, values) => {
  const transformedPath = transformFieldName(fieldDefinition.path)
  const selectedOptions = values[transformedPath].options.filter(o => o.checked).length
  const minSelection = fieldDefinition.validation.minSelection
  const maxSelection = fieldDefinition.validation.maxSelection
  const placeholders = {
    ...(Number.isInteger(minSelection) ? {minSelection} : {}),
    ...(Number.isInteger(maxSelection) ? {maxSelection} : {})
  }

  if (
    Number.isInteger(minSelection) &&
    Number.isInteger(maxSelection) &&
    (selectedOptions < minSelection || selectedOptions > maxSelection)
  ) {
    const messageId =
      fieldDefinition?.validation?.messages?.choiceMinMax || 'client.component.form.syncValidationRequiredChoiceMinMax'
    return {
      [transformedPath]: mandatoryError(messageId, placeholders)
    }
  } else if (Number.isInteger(maxSelection) && selectedOptions > maxSelection) {
    const messageId =
      fieldDefinition?.validation?.messages?.choiceMax || 'client.component.form.syncValidationRequiredChoiceMax'
    return {
      [transformedPath]: mandatoryError(messageId, placeholders)
    }
  } else if (Number.isInteger(minSelection) && selectedOptions < minSelection) {
    const messageId =
      fieldDefinition?.validation?.messages?.choiceMin || 'client.component.form.syncValidationRequiredChoiceMin'
    return {
      [transformedPath]: mandatoryError(messageId, placeholders)
    }
  } else if (fieldDefinition.validation.mandatory && selectedOptions === 0) {
    const messageId =
      fieldDefinition?.validation?.messages?.choice || 'client.component.form.syncValidationRequiredChoice'
    return {
      [transformedPath]: mandatoryError(messageId)
    }
  }

  return {}
}

export const pseudoSelectValidator = (_value, fieldDefinition, formDefinition, values) => {
  const transformedPath = transformFieldName(fieldDefinition.path)
  const selectedOptions = values[transformedPath].options.filter(o => o.selected).length
  const mandatory = fieldDefinition?.validation?.mandatory

  if (mandatory && selectedOptions === 0) {
    const customMessage = fieldDefinition.validation.messages?.mandatory
    return {
      [transformedPath]: mandatoryError(customMessage)
    }
  }

  return {}
}

export default {
  location: locationValidator,
  choice: choiceValidator,
  'pseudo-select': pseudoSelectValidator
}
