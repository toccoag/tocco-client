import {mandatoryError} from './mandatory'
import {locationValidator, choiceValidator, pseudoSelectValidator} from './syncTypeValidators'
import {maxLengthValidator} from './syncValidators'

const maxLenthError = maxLengthValidator('123456', 5)

describe('app-extensions', () => {
  describe('form', () => {
    describe('validators', () => {
      describe('syncTypeValidators', () => {
        describe('location', () => {
          test('should validate city and postcode', () => {
            const fieldDefinition = {
              id: 'location',
              locationMapping: {
                city: 'city',
                postcode: 'postcode'
              }
            }
            const formDefinition = {
              id: 'box',
              children: [
                {
                  id: 'city',
                  children: [
                    {
                      path: 'city',
                      validation: {
                        mandatory: true
                      }
                    }
                  ]
                },
                {
                  id: 'postcode',
                  children: [
                    {
                      path: 'postcode',
                      validation: {
                        mandatory: true,
                        length: {
                          toIncluding: 5
                        }
                      }
                    }
                  ]
                }
              ]
            }
            const values = {
              postcode: '123456'
            }

            const expectedErrors = {
              location: {
                ...mandatoryError(),
                ...maxLenthError
              }
            }

            const errors = locationValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty errors if field is valid', () => {
            const fieldDefinition = {
              id: 'location',
              locationMapping: {
                city: 'city',
                postcode: 'postcode'
              }
            }
            const formDefinition = {
              id: 'box',
              children: [
                {
                  id: 'city',
                  children: [
                    {
                      path: 'city',
                      validation: {}
                    }
                  ]
                },
                {
                  id: 'postcode',
                  children: [
                    {
                      path: 'postcode',
                      validation: {
                        mandatory: true
                      }
                    }
                  ]
                }
              ]
            }
            const values = {
              postcode: '123456'
            }

            const expectedErrors = {}

            const errors = locationValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should validate relAddress.city and relAddress.postcode', () => {
            const fieldDefinition = {
              id: 'location',
              locationMapping: {
                city: 'relAddress.city',
                postcode: 'relAddress.postcode'
              }
            }
            const formDefinition = {
              id: 'box',
              children: [
                {
                  id: 'relAddress.city',
                  children: [
                    {
                      path: 'relAddress.city',
                      validation: {
                        mandatory: true
                      }
                    }
                  ]
                },
                {
                  id: 'relAddress.postcode',
                  children: [
                    {
                      path: 'relAddress.postcode',
                      validation: {
                        mandatory: true,
                        length: {
                          toIncluding: 5
                        }
                      }
                    }
                  ]
                }
              ]
            }
            const values = {
              'relAddress--postcode': '123456'
            }

            const expectedErrors = {
              location: {
                ...mandatoryError(),
                ...maxLenthError
              }
            }

            const errors = locationValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })
        })

        describe('choice', () => {
          const getFormFieldDefinition = validation => ({
            id: 'choice',
            path: 'choice',
            validation
          })

          const getFormDefinition = validation => ({
            id: 'box',
            children: [
              {
                id: 'choice',
                children: [
                  {
                    path: 'choice',
                    validation
                  }
                ]
              }
            ]
          })

          test('should validate choice', () => {
            const validation = {mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: false
                  }
                ]
              }
            }

            const expectedErrors = {
              choice: {
                ...mandatoryError('client.component.form.syncValidationRequiredChoice')
              }
            }

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty errors if field is valid', () => {
            const validation = {}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: true
                  }
                ]
              }
            }

            const expectedErrors = {}

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty errors if field is not mandatory', () => {
            const validation = {}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: false
                  }
                ]
              }
            }

            const expectedErrors = {}

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty errors if selected options are equal to minSelection', () => {
            const validation = {minSelection: 1, mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: true
                  }
                ]
              }
            }

            const expectedErrors = {}

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return error if selected options are less than minSelection', () => {
            const validation = {minSelection: 1, mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: false
                  }
                ]
              }
            }

            const expectedErrors = {
              choice: {
                ...mandatoryError('client.component.form.syncValidationRequiredChoiceMin', {minSelection: 1})
              }
            }

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return error if selected options are less than minSelection (with defined maxSelection)', () => {
            const validation = {minSelection: 1, maxSelection: 2, mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: false
                  }
                ]
              }
            }

            const messageValues = {minSelection: 1, maxSelection: 2}
            const expectedErrors = {
              choice: {
                ...mandatoryError('client.component.form.syncValidationRequiredChoiceMinMax', messageValues)
              }
            }

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty errors if selected options are equal to maxSelection', () => {
            const validation = {maxSelection: 2}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: true
                  },
                  {
                    checked: true
                  }
                ]
              }
            }

            const expectedErrors = {}

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return error if selected options are less than minSelection', () => {
            const validation = {maxSelection: 1, mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: true
                  },
                  {
                    checked: true
                  }
                ]
              }
            }

            const expectedErrors = {
              choice: {
                ...mandatoryError('client.component.form.syncValidationRequiredChoiceMax', {maxSelection: 1})
              }
            }

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return error if selected options are less than minSelection (with defined minSelection)', () => {
            const validation = {minSelection: 0, maxSelection: 1}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              choice: {
                options: [
                  {
                    checked: true
                  },
                  {
                    checked: true
                  }
                ]
              }
            }

            const messageValues = {minSelection: 0, maxSelection: 1}
            const expectedErrors = {
              choice: {
                ...mandatoryError('client.component.form.syncValidationRequiredChoiceMinMax', messageValues)
              }
            }

            const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          describe('custom messages', () => {
            test('should return custom message for choice', () => {
              const validation = {mandatory: true, messages: {choice: 'custom'}}
              const fieldDefinition = getFormFieldDefinition(validation)
              const formDefinition = getFormDefinition(validation)
              const values = {
                choice: {
                  options: [
                    {
                      checked: false
                    }
                  ]
                }
              }

              const expectedErrors = {
                choice: {
                  ...mandatoryError('custom')
                }
              }

              const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

              expect(errors).to.deep.equal(expectedErrors)
            })

            test('should return custom message for choiceMin', () => {
              const validation = {minSelection: 1, mandatory: true, messages: {choiceMin: 'custom'}}
              const fieldDefinition = getFormFieldDefinition(validation)
              const formDefinition = getFormDefinition(validation)
              const values = {
                choice: {
                  options: [
                    {
                      checked: false
                    }
                  ]
                }
              }

              const expectedErrors = {
                choice: {
                  ...mandatoryError('custom', {minSelection: 1})
                }
              }

              const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

              expect(errors).to.deep.equal(expectedErrors)
            })

            test('should return custom message for choiceMax', () => {
              const validation = {maxSelection: 1, mandatory: true, messages: {choiceMax: 'custom'}}
              const fieldDefinition = getFormFieldDefinition(validation)
              const formDefinition = getFormDefinition(validation)
              const values = {
                choice: {
                  options: [
                    {
                      checked: true
                    },
                    {
                      checked: true
                    }
                  ]
                }
              }

              const expectedErrors = {
                choice: {
                  ...mandatoryError('custom', {maxSelection: 1})
                }
              }

              const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

              expect(errors).to.deep.equal(expectedErrors)
            })

            test('should return custom message for choiceMinMax', () => {
              const validation = {minSelection: 0, maxSelection: 1, messages: {choiceMinMax: 'custom'}}
              const fieldDefinition = getFormFieldDefinition(validation)
              const formDefinition = getFormDefinition(validation)
              const values = {
                choice: {
                  options: [
                    {
                      checked: true
                    },
                    {
                      checked: true
                    }
                  ]
                }
              }

              const messageValues = {minSelection: 0, maxSelection: 1}
              const expectedErrors = {
                choice: {
                  ...mandatoryError('custom', messageValues)
                }
              }

              const errors = choiceValidator(undefined, fieldDefinition, formDefinition, values)

              expect(errors).to.deep.equal(expectedErrors)
            })
          })
        })

        describe('pseudo-select', () => {
          const getFormFieldDefinition = validation => ({
            id: 'pseudoSelect',
            path: 'pseudoSelect',
            validation
          })

          const getFormDefinition = validation => ({
            id: 'box',
            children: [
              {
                id: 'pseudoSelect',
                children: [
                  {
                    path: 'pseudoSelect',
                    validation
                  }
                ]
              }
            ]
          })

          test('should validate pseudo-select', () => {
            const validation = {mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              pseudoSelect: {
                options: [
                  {
                    selected: false
                  }
                ]
              }
            }

            const expectedErrors = {
              pseudoSelect: {
                ...mandatoryError('client.component.form.syncValidationRequired')
              }
            }

            const errors = pseudoSelectValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should validate pseudo-select with custom message', () => {
            const mandatoryMsg = 'client.custom.message'
            const validation = {mandatory: true, messages: {mandatory: mandatoryMsg}}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              pseudoSelect: {
                options: [
                  {
                    selected: false
                  }
                ]
              }
            }

            const expectedErrors = {
              pseudoSelect: {
                ...mandatoryError('client.custom.message')
              }
            }

            const errors = pseudoSelectValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })

          test('should return empty error if option is selected', () => {
            const validation = {mandatory: true}
            const fieldDefinition = getFormFieldDefinition(validation)
            const formDefinition = getFormDefinition(validation)
            const values = {
              pseudoSelect: {
                options: [
                  {
                    selected: true
                  }
                ]
              }
            }

            const expectedErrors = {}

            const errors = pseudoSelectValidator(undefined, fieldDefinition, formDefinition, values)

            expect(errors).to.deep.equal(expectedErrors)
          })
        })
      })
    })
  })
})
