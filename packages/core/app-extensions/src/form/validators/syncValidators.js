import {FormattedMessage} from 'react-intl'
import {js} from 'tocco-util'
import isEmail from 'validator/lib/isEmail'
import isUrl from 'validator/lib/isURL'

export const minLengthValidator = (value, minLength, messageId = 'client.component.form.syncValidationMinLength') =>
  js.isDefined(minLength) && value.length < minLength
    ? {
        minLength: [<FormattedMessage key="syncValidationMinLength" id={messageId} values={{minLength}} />]
      }
    : null

export const minNumberValidator = (value, minValue, messageId = 'client.component.form.syncValidationMinNumber') =>
  js.isDefined(minValue) && value < minValue
    ? {
        maxNumber: [<FormattedMessage key="syncValidationMaxNumber" id={messageId} values={{minValue}} />]
      }
    : null

export const maxLengthValidator = (value, maxLength, messageId = 'client.component.form.syncValidationMaxLength') =>
  js.isDefined(maxLength) && value.length > maxLength
    ? {
        maxLength: [<FormattedMessage key="syncValidationMaxLength" id={messageId} values={{maxLength}} />]
      }
    : null

export const maxNumberValidator = (value, maxValue, messageId = 'client.component.form.syncValidationMaxNumber') =>
  js.isDefined(maxValue) && value > maxValue
    ? {
        maxNumber: [<FormattedMessage key="syncValidationMaxNumber" id={messageId} values={{maxValue}} />]
      }
    : null

export const postPointValidator = (value, limit, messageId = 'client.component.form.syncValidationPrePoint') =>
  value.toString().includes('.') && value.toString().split('.')[1].length > limit
    ? {
        prePoint: [<FormattedMessage key="syncValidationPrePoint" id={messageId} values={{limit}} />]
      }
    : null

export const prePointValidator = (value, limit, messageId = 'client.component.form.syncValidationPrePoint') =>
  limit > 0 && value.toString().split('.')[0].length > limit
    ? {
        prePoint: [<FormattedMessage key="syncValidationPrePoint" id={messageId} values={{limit}} />]
      }
    : null

export const regexValidator = (value, pattern, messageId = 'client.component.form.syncValidationRegexPattern') =>
  !RegExp(pattern).test(value)
    ? {
        prePoint: [<FormattedMessage key="syncValidationRegexPattern" id={messageId} values={{pattern}} />]
      }
    : null

export const emailValidator = (value, _, messageId = 'client.component.form.invalidEmail') =>
  !isEmail(value)
    ? {
        format: [<FormattedMessage key="invalidEmail" id={messageId} />]
      }
    : null

export const urlValidator = (value, _, messageId = 'client.component.form.invalidUrl') =>
  !isUrl(value) && !(value.includes('localhost') && isUrl(value, {require_tld: false}))
    ? {
        format: [<FormattedMessage key="invalidUrl" id={messageId} />]
      }
    : null

export default {
  'length.fromIncluding': minLengthValidator,
  'length.toIncluding': maxLengthValidator,
  'numberRange.fromIncluding': minNumberValidator,
  'numberRange.toIncluding': maxNumberValidator,
  'decimalDigits.postPointDigits': postPointValidator,
  'decimalDigits.prePointDigits': prePointValidator,
  'regex.pattern': regexValidator,
  email: emailValidator,
  url: urlValidator
}
