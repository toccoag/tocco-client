import {screen} from '@testing-library/react'
import {reduxForm} from 'redux-form'
import {testingLibrary} from 'tocco-test-util'

import appFactory from '../appFactory'

import FormBuilder from './FormBuilder'

/* eslint-disable react/prop-types */
jest.mock('../formField', () => ({
  FormField: ({data}) => {
    const {id, parentReadOnly} = data
    return (
      <div data-testid="form-field" data-prop-parentreadonly={parentReadOnly}>
        {id}
      </div>
    )
  }
}))
/* eslint-enable react/prop-types */

const testData = {
  entity: {
    key: '1',
    paths: {
      firstname: {
        type: 'field',
        value: {
          value: 'First Name',
          type: 'string',
          readable: true,
          writable: true
        }
      },
      lastname: {
        type: 'field',
        value: {
          value: 'Last Name',
          type: 'string',
          readable: true,
          writable: true
        }
      }
    }
  },
  formName: 'detail',
  formValues: {
    firstname: 'Fist Name',
    lastname: 'Last Name',
    'not-readonly-field': 'test'
  },
  fieldMappingType: 'editable',
  formDefinition: {
    id: 'UserSearch_detail',
    readonly: false,
    children: [
      {
        id: 'box1',
        componentType: 'layout',
        layoutType: 'vertical-box',
        readonly: true,
        children: [
          {
            id: 'box1',
            componentType: 'layout',
            layoutType: 'horizontal-box',
            displayType: 'READONLY',
            children: [
              {
                id: 'user_information',
                label: 'Box 1',
                componentType: 'layout',
                layoutType: 'vertical-box',
                readonly: true,
                children: [
                  {
                    id: 'firstname',
                    componentType: 'field-set',
                    label: 'Vorname',
                    scopes: ['create'],
                    hidden: false,
                    readonly: true,
                    children: [
                      {
                        id: 'firstname-field', // does not match path by intention (-> should use path to get data)
                        componentType: 'field',
                        path: 'firstname',
                        dataType: 'string',
                        label: null
                      }
                    ]
                  },
                  {
                    id: 'lastname',
                    componentType: 'field-set',
                    label: 'Nachname',
                    hidden: false,
                    readonly: true,
                    children: [
                      {
                        id: 'lastname',
                        componentType: 'field',
                        path: 'lastname',
                        dataType: 'string',
                        label: null
                      }
                    ]
                  }
                ]
              },
              {
                id: 'readonly-box',
                componentType: 'layout',
                layoutType: 'vertical-box',
                readonly: true,
                label: 'Box 2',
                children: [
                  {
                    id: 'not-readonly-field-set',
                    componentType: 'field-set',
                    label: 'Not Readonly',
                    hidden: false,
                    readonly: false,
                    children: [
                      {
                        id: 'not-readonly-field',
                        componentType: 'field',
                        path: 'not-readonly-field',
                        dataType: 'string',
                        label: null
                      }
                    ]
                  }
                ]
              }
            ],
            label: null
          }
        ]
      }
    ]
  },
  formFieldMapping: {}
}

const getBasicFormDefinition = children => ({
  id: 'UserSearch_detail',
  readonly: false,
  children: [
    {
      id: 'user_information',
      componentType: 'layout',
      layoutType: 'vertical-box',
      readonly: false,
      children
    }
  ]
})

const Form = reduxForm({form: 'test-form'})(({children}) => children)
const createStore = () => appFactory.createStore(() => {}, null, {})

const renderFormBuilder = props => {
  const store = createStore()
  testingLibrary.renderWithStore(
    <Form>
      <FormBuilder {...testData} {...props} />
    </Form>,
    {store}
  )
}

describe('app-extensions', () => {
  describe('form', () => {
    describe('FormBuilder', () => {
      test('should render layout boxes and Fields', async () => {
        renderFormBuilder()
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('Box 1')).to.exist
        expect(screen.queryByText('input-detail-firstname')).to.exist
        expect(screen.queryByText('input-detail-lastname')).to.exist

        expect(screen.queryByText('Box 2')).to.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.exist
      })

      test('should not render field if beforeRenderField returns false', async () => {
        const beforeRenderField = name => name !== 'lastname'

        const props = {
          beforeRenderField
        }
        renderFormBuilder(props)
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('Box 1')).to.exist
        expect(screen.queryByText('input-detail-firstname')).to.exist
        expect(screen.queryByText('input-detail-lastname')).to.not.exist

        expect(screen.queryByText('Box 2')).to.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.exist
      })

      test('should not require an entity (should not check readable flag in this case)', async () => {
        const entity = null
        const props = {entity}

        renderFormBuilder(props)
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('input-detail-firstname')).to.exist
        expect(screen.queryByText('input-detail-lastname')).to.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.exist
      })

      test('should not render empty values in readonly form', () => {
        const {formDefinition} = testData
        const formDefinitionReadOnly = {...formDefinition, readonly: true}
        const formValues = {lastname: undefined}
        const props = {
          formValues,
          formDefinition: formDefinitionReadOnly,
          fieldMappingType: 'readonly'
        }

        renderFormBuilder(props)

        expect(screen.queryByText('input-detail-firstname')).to.not.exist
        expect(screen.queryByText('input-detail-lastname')).to.not.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.not.exist
      })

      test('should render fields with matching scope/mode', async () => {
        const entity = null
        const props = {
          entity,
          mode: 'create'
        }

        renderFormBuilder(props)
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('input-detail-firstname')).to.exist
        expect(screen.queryByText('input-detail-lastname')).to.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.exist
      })

      test('should NOT render fields with unmatching scope/mode', async () => {
        const entity = null
        const props = {
          entity,
          mode: 'update'
        }

        renderFormBuilder(props)
        await screen.findAllByTestId('icon')

        expect(screen.queryByText('input-detail-firstname')).to.not.exist
        expect(screen.queryByText('input-detail-lastname')).to.exist
        expect(screen.queryByText('input-detail-not-readonly-field')).to.exist
      })

      test('should render children of readonly layouts to readonly', async () => {
        const entity = null
        const props = {
          entity,
          mode: 'update'
        }

        renderFormBuilder(props)
        await screen.findAllByTestId('icon')

        const el = screen.getByText('input-detail-not-readonly-field')
        expect(el.getAttribute('data-prop-parentreadonly')).to.eql('true')
      })

      describe('handle multipath fields', () => {
        const getEntityList = writable => ({
          paths: {
            relOrder: {
              type: 'entity-list',
              writable,
              value: []
            }
          }
        })

        const entity = {
          paths: {
            relOrder: {
              type: 'entity',
              writable: false,
              value: {
                _links: null,
                key: '12556',
                model: 'Order',
                version: 4,
                paths: {
                  relOrder_debitor_status: {
                    type: 'entity',
                    writable: true,
                    value: {
                      _links: null,
                      key: '2',
                      model: 'Order_debitor_status',
                      version: 2,
                      paths: {}
                    }
                  }
                }
              }
            }
          }
        }

        const getFormDefinition = (fieldId = 'relOrder_debitor_status') =>
          getBasicFormDefinition([
            {
              id: 'Order_debitor_status',
              componentType: 'field-set',
              label: 'Status',
              hidden: false,
              readonly: false,
              children: [
                {
                  id: fieldId,
                  componentType: 'field',
                  path: 'relOrder.relOrder_debitor_status',
                  dataType: 'string',
                  label: null
                }
              ]
            }
          ])

        test('should read multi paths entity fields', () => {
          // does not match path by intention (-> should use path to get data))
          const formDefinition = getFormDefinition('Order_debitor_status')

          const props = {
            entity,
            formDefinition,
            mode: 'update'
          }
          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relOrder.relOrder_debitor_status')).to.exist
        })

        test('should render multipath fields with missing parts if last available part is readonly', () => {
          const formDefinition = getFormDefinition()

          const props = {
            entity: getEntityList(false),
            formDefinition,
            mode: 'update'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relOrder.relOrder_debitor_status')).to.not.exist
        })

        test('should render multipath fields with missing parts if last available part is writable', () => {
          const formDefinition = getFormDefinition()

          const props = {
            entity: getEntityList(true),
            formDefinition,
            mode: 'update'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relOrder.relOrder_debitor_status')).to.exist
        })

        test('should render multipath fields with missing parts in create mode', () => {
          const formDefinition = getFormDefinition()

          const props = {
            entity: getEntityList(null),
            formDefinition,
            mode: 'create'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relOrder.relOrder_debitor_status')).to.exist
        })
      })

      test('should render description field', () => {
        const entity = {
          paths: {
            relOrder: {
              type: 'entity-list',
              writable: null,
              value: []
            }
          }
        }

        const formDefinition = getBasicFormDefinition([
          {
            id: 'email_change_field_description',
            label: null,
            componentType: 'field-set',
            children: [
              {
                id: 'email_change_field_description',
                label: null,
                componentType: 'description',
                title: 'description title',
                text: 'description text',
                mode: 'tooltip'
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'no',
            scopes: [],
            ignoreCopy: false
          }
        ])

        const props = {
          entity,
          formDefinition,
          mode: 'update'
        }

        renderFormBuilder(props)

        expect(screen.queryByText('input-detail-email_change_field_description')).to.exist
      })

      describe('location field', () => {
        const formDefinition = getBasicFormDefinition([
          {
            id: 'locationfield_c',
            label: 'PLZ / Ort',
            componentType: 'field-set',
            children: [
              {
                id: 'locationfield_c',
                label: null,
                componentType: 'field',
                path: null,
                dataType: 'location',
                validation: null,
                defaultValue: null,
                autoCompleteEndpoint: null,
                locationMapping: {
                  postcode: 'zip_c',
                  city: 'city_c',
                  street: 'address_c',
                  country: 'relCountry_c',
                  state: 'canton',
                  district: 'admincode2'
                },
                countries: ['DE', 'AT', 'CH', 'IT', 'FR', 'LI']
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'yes',
            scopes: [],
            ignoreCopy: false
          }
        ])

        test('should render location field for create scope', () => {
          const entity = {}
          const formName = 'detail'
          const formValues = {}

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'create',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-locationfield_c')).to.exist
        })

        test('should render location field if both fields are writable', () => {
          const entity = {
            paths: {
              zip_c: {
                type: 'postcode',
                writable: true,
                value: null
              },
              city_c: {
                type: 'string',
                writable: true,
                value: null
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            zip_c: null,
            city_c: null
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-locationfield_c')).to.exist
        })

        test('should render location field read-only if fields are not writable but have a value', () => {
          const entity = {
            paths: {
              zip_c: {
                type: 'postcode',
                writable: false,
                value: '8000'
              },
              city_c: {
                type: 'string',
                writable: false,
                value: 'Zürich'
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            zip_c: '8000',
            city_c: 'Zürich'
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-locationfield_c')).to.exist
        })

        test('should not render location field read-only if fields are not writable and have no value', () => {
          const entity = {
            paths: {
              zip_c: {
                type: 'postcode',
                writable: false,
                value: null
              },
              city_c: {
                type: 'string',
                writable: false,
                value: null
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            zip_c: null,
            city_c: null
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-locationfield_c')).to.not.exist
        })
      })

      describe('address field', () => {
        const formDefinition = getBasicFormDefinition([
          {
            id: 'addressfield_c',
            label: 'Adresse',
            componentType: 'field-set',
            children: [
              {
                id: 'addressfield_c',
                label: null,
                componentType: 'field',
                path: null,
                dataType: 'address',
                validation: null,
                defaultValue: null,
                autoCompleteEndpoint: null,
                locationMapping: {
                  postcode: 'zip_c',
                  city: 'city_c',
                  street: 'address_c',
                  country: 'relCountry_c',
                  state: 'canton',
                  district: 'admincode2'
                },
                countries: ['DE', 'AT', 'CH', 'IT', 'FR', 'LI']
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'yes',
            scopes: [],
            ignoreCopy: false
          }
        ])

        test('should render address field for create scope', () => {
          const entity = {}
          const formName = 'detail'
          const formValues = {}

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'create',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-addressfield_c')).to.exist
        })

        test('should render address field if street field is writable', () => {
          const entity = {
            paths: {
              address_c: {
                type: 'string',
                writable: true,
                value: null
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            address_c: null
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-addressfield_c')).to.exist
        })

        test('should render address field read-only if street fields is not writable but has a value', () => {
          const entity = {
            paths: {
              address_c: {
                type: 'string',
                writable: false,
                value: 'Riedtlistrasse'
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            address_c: 'Riedtlistrasse'
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-addressfield_c')).to.exist
        })

        test('should not render address field read-only if street field is not writable and has no value', () => {
          const entity = {
            paths: {
              address_c: {
                type: 'string',
                writable: false,
                value: null
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            address_c: null
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-addressfield_c')).to.not.exist
        })
      })

      describe('selector fields', () => {
        const addressEntity = {
          paths: {
            'relAddress_user[publication]': {
              type: 'entity',
              writable: true,
              value: {
                key: '1',
                model: 'Address_user',
                version: 1,
                paths: {
                  relAddress: {
                    type: 'entity',
                    writable: true,
                    value: {
                      key: '1',
                      model: 'Address',
                      version: 18,
                      paths: {
                        company_c: {
                          type: 'string',
                          writable: true,
                          value: 'Tocco AG'
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

        test('should render empty selector fields', () => {
          const formDefinition = getBasicFormDefinition([
            {
              id: 'relAddress_user[publication].relAddress.company_c',
              label: 'Korresp.-Firma',
              componentType: 'field-set',
              children: [
                {
                  id: 'relAddress_user[publication].relAddress.company_c',
                  label: null,
                  componentType: 'field',
                  path: 'relAddress_user[publication].relAddress.company_c',
                  dataType: 'string'
                }
              ],
              readonly: false,
              hidden: false,
              useLabel: 'yes',
              scopes: [],
              ignoreCopy: false
            }
          ])

          const formValues = {
            'relAddress_user=-=publication=_=--relAddress--company_c': ''
          }

          const props = {
            entity: addressEntity,
            formDefinition,
            formValues,
            mode: 'update'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relAddress_user[publication].relAddress.company_c')).to.exist
        })

        test('should not render empty selector fields on readonly form', () => {
          const formDefinition = getBasicFormDefinition([
            {
              id: 'relAddress_user[publication].relAddress.company_c',
              label: 'Korresp.-Firma',
              componentType: 'field-set',
              children: [
                {
                  id: 'relAddress_user[publication].relAddress.company_c',
                  label: null,
                  componentType: 'field',
                  path: 'relAddress_user[publication].relAddress.company_c',
                  dataType: 'string'
                }
              ],
              readonly: false,
              hidden: false,
              useLabel: 'yes',
              scopes: [],
              ignoreCopy: false
            }
          ])

          const formValues = {
            'relAddress_user=-=publication=_=--relAddress--company_c': ''
          }

          const props = {
            entity: addressEntity,
            formDefinition,
            formValues,
            mode: 'update',
            fieldMappingType: 'readonly',
            readonly: true
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-relAddress_user[publication].relAddress.company_c')).to.not.exist
        })
      })

      describe('terms field', () => {
        const formDefinition = getBasicFormDefinition([
          {
            id: 'other_field',
            componentType: 'field-set',
            children: [
              {
                id: 'other_field',
                label: null,
                componentType: 'field',
                path: 'other_field',
                dataType: 'string',
                validation: {},
                defaultValue: null
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'yes',
            scopes: [],
            ignoreCopy: false
          },
          {
            id: 'relTerms_condition_confirmation[privacy_protection]',
            label: null,
            componentType: 'field-set',
            children: [
              {
                id: 'relTerms_condition_confirmation[privacy_protection]',
                label: null,
                componentType: 'terms',
                type: 'privacy_protection',
                path: 'relTerms_condition_confirmation[privacy_protection]',
                validation: {
                  mandatory: true
                }
              }
            ],
            readonly: false,
            hidden: false,
            useLabel: 'hide',
            scopes: [],
            ignoreCopy: true
          }
        ])

        test('should render terms field for create scope', () => {
          const entity = {}
          const formName = 'detail'
          const formValues = {}

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'create',
            formFieldMapping: {},
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-other_field')).to.exist
          expect(screen.queryByText('input-detail-relTerms_condition_confirmation[privacy_protection]')).to.exist
        })

        test('should render terms field if any other field is writable', () => {
          const entity = {
            paths: {
              zip_c: {
                type: 'other_field',
                writable: true,
                value: 'value'
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            other_field: 'value'
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            formFieldMapping: {},
            fieldMappingType: 'editable'
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-other_field')).to.exist
          expect(screen.queryByText('input-detail-relTerms_condition_confirmation[privacy_protection]')).to.exist
        })

        test('should hide terms field if all other fields are not writable', () => {
          const entity = {
            paths: {
              zip_c: {
                type: 'other_field',
                writable: false,
                value: 'value'
              }
            }
          }
          const formName = 'detail'
          const formValues = {
            other_field: 'value'
          }

          const props = {
            entity,
            formName,
            formDefinition,
            formValues,
            mode: 'update',
            formFieldMapping: {},
            fieldMappingType: 'editable',
            readonly: true
          }

          renderFormBuilder(props)

          expect(screen.queryByText('input-detail-other_field')).to.exist
          expect(screen.queryByText('input-detail-relTerms_condition_confirmation[privacy_protection]')).to.not.exist
        })
      })
    })
  })
})
