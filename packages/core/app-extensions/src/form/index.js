import {asyncValidation, submitValidation} from './asyncValidation'
import componentTypes from './enums/componentTypes'
import layoutTypes from './enums/layoutTypes'
import scopes from './enums/scopes'
import {ErrorItem} from './ErrorItem'
import {addToStore} from './form'
import FormBuilder from './FormBuilder'
import {getFieldId, getFieldDefinitions, typeFieldMapping, getDefaultValues, getUsedPaths} from './formDefinition'
import formErrorsUtil from './formErrors'
import {
  adjustAllActions,
  adjustAction,
  adjustActions,
  adjustFields,
  adjustFieldSets,
  adjustByIds,
  addAction,
  addCreate,
  addBack,
  addReports,
  removeBoxes,
  removeFields,
  removeChildren,
  removeActions,
  replaceBoxChildren,
  appendChildrenToBox,
  createSimpleForm,
  createHorizontalBox,
  createVerticalBox,
  createFieldSet,
  createTable,
  createColumn,
  createActionCell,
  createButtonCell,
  createField,
  createValidation,
  createMandatoryValidation,
  createDecimalDigitsValidation,
  createNumberRangeValidation,
  findInChildren,
  idSelector
} from './formModifier'
import * as hooks from './hooks'
import {
  createChoiceOption,
  createSelectOption,
  createStringField,
  createTextField,
  createCheckbox,
  createSingleChoiceField,
  createMultipleChoiceField,
  createMultipleChoiceFieldWithSelection,
  createSelectField,
  createDisplayField,
  getMergedEntityPaths,
  splitFlattenEntity,
  transformPath,
  transformPathBack
} from './pseudoForm'
import {
  mapQuestionToPseudoField,
  isQuestionField,
  shouldRenderQuestionField,
  getQuestionFieldMapping,
  mapQuestionsToBean
} from './question'
import {
  formValuesToFlattenEntity,
  entityToFormValues,
  getDirtyFormValues,
  validationErrorToFormError,
  transformFieldName,
  transformFieldNameBack,
  isValueEmpty
} from './reduxForm'
import * as sagas from './sagas'
import * as sagasUtils from './sagasUtils'
import selectors from './selectors'
import syncValidation from './syncValidation'
import transformInputValues from './transformInputValues'
import validators from './validators'

export default {
  formErrorsUtil,
  FormBuilder,
  getFieldId,
  getFieldDefinitions,
  getDefaultValues,
  getUsedPaths,
  formValuesToFlattenEntity,
  entityToFormValues,
  getDirtyFormValues,
  validationErrorToFormError,
  transformFieldName,
  transformFieldNameBack,
  syncValidation,
  componentTypes,
  layoutTypes,
  scopes,
  validators,
  transformInputValues,
  asyncValidation,
  submitValidation,
  isValueEmpty,
  typeFieldMapping,
  sagas,
  sagasUtils,
  addToStore,
  ErrorItem,
  hooks,
  selectors,
  addAction,
  addCreate,
  addBack,
  addReports,
  removeBoxes,
  removeFields,
  removeChildren,
  removeActions,
  adjustAllActions,
  adjustAction,
  adjustActions,
  adjustFields,
  adjustFieldSets,
  adjustByIds,
  replaceBoxChildren,
  appendChildrenToBox,
  createSimpleForm,
  createHorizontalBox,
  createVerticalBox,
  createFieldSet,
  createTable,
  createField,
  createColumn,
  createActionCell,
  createButtonCell,
  createValidation,
  createMandatoryValidation,
  createDecimalDigitsValidation,
  createNumberRangeValidation,
  createChoiceOption,
  createSelectOption,
  createStringField,
  createTextField,
  createCheckbox,
  createSingleChoiceField,
  createMultipleChoiceField,
  createMultipleChoiceFieldWithSelection,
  createSelectField,
  createDisplayField,
  getMergedEntityPaths,
  splitFlattenEntity,
  transformPath,
  transformPathBack,
  findInChildren,
  idSelector,
  mapQuestionToPseudoField,
  isQuestionField,
  shouldRenderQuestionField,
  getQuestionFieldMapping,
  mapQuestionsToBean
}
