import PropTypes from 'prop-types'
import {useMemo} from 'react'
import {ButtonContextProvider} from 'tocco-ui'
import {env} from 'tocco-util'

import actions from '../actions'

import {StyledActionsWrapper} from './StyledFormBuilder'

const FormActions = ({entityName, entityKey, mode, customRenderedActions, action}) => {
  const isAdmin = env.isInAdminEmbedded()

  const selection = useMemo(() => actions.getSingleEntitySelection(entityName, entityKey), [entityName, entityKey])

  return (
    <ButtonContextProvider>
      {ref => (
        <StyledActionsWrapper ref={ref} sticky={isAdmin || action.sticky}>
          <actions.Action
            definition={action}
            selection={selection}
            mode={mode}
            customRenderedActions={customRenderedActions}
          />
        </StyledActionsWrapper>
      )}
    </ButtonContextProvider>
  )
}
FormActions.propTypes = {
  entityName: PropTypes.string,
  entityKey: PropTypes.string,
  action: PropTypes.object.isRequired,
  mode: PropTypes.string,
  customRenderedActions: PropTypes.objectOf(PropTypes.func)
}

export default FormActions
