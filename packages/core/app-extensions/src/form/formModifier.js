export const MAIN_ACTION_BAR_ID = 'main-action-bar'
export const ACTION_BAR_TYPE = 'action-bar'
export const ACTION_GROUP_TYPE = 'action-group'
export const FIELD_TYPE = 'field'
export const FIELD_SET_TYPE = 'field-set'
export const ACTION_TYPE = 'action'
export const REPORT_TYPE = 'report'
export const LAYOUT_TYPE = 'layout'
export const TABLE_TYPE = 'table'
export const COLUMN_TYPE = 'column'
export const ACTION_BACK_ID = 'back'
export const ACTION_GROUP_CREATECOPY_ID = 'createcopy'
export const ACTION_DELETE_ID = 'delete'
export const ACTION_SAVE_ID = 'save'
export const ACTION_NEW_ID = 'new'
export const ACTION_GROUP_OUTPUT_ID = 'output'
export const ACTION_GROUP_ACTIONS_ID = 'actions'

export const removeBoxes = (formDefinition, boxIds) =>
  adjustChildren(formDefinition, all(layoutSelector, idsSelector(boxIds)), () => false)

/**
 * remove fields by name anywhere in the form definition
 * @param {*} container the form definition (or any other container object)
 * @param {*} fieldIds the fields to remove
 * @returns a copy of the container with the fields removed
 */
export const removeFields = (container, fieldIds) => removeChildren(container, item => fieldIds.includes(item.id))

/**
 * add action or action group to main action bar
 */
export const addAction = (formDefinition, action, actionGroupsBefore) => {
  const formDefinitionWithActionBar = addMainActionBarIfMissing(formDefinition)
  return adjustChildren(formDefinitionWithActionBar, mainActionBarSelector, mainActionBar =>
    insertChild(mainActionBar, action, actionGroupsBefore)
  )
}

const createCreateButton = intl => ({
  id: ACTION_NEW_ID,
  label: intl.formatMessage({id: 'client.actions.create'}),
  componentType: 'action',
  children: [],
  actionType: 'custom',
  useLabel: 'YES',
  buttonType: 'REGULAR',
  icon: 'plus'
})

/**
 * add create action to main action bar
 *
 * separate from addAction because of createcopy group, if it is missing create action is added directly to action bar
 */
export const addCreate = (formDefinition, intl) => {
  if (formDefinition.id.endsWith('_create') || hasChild(formDefinition, idSelector(ACTION_GROUP_CREATECOPY_ID))) {
    return formDefinition
  }

  return addAction(formDefinition, createCreateButton(intl))
}

const createBackButton = intl => ({
  id: ACTION_BACK_ID,
  label: intl.formatMessage({id: 'client.actions.back'}),
  componentType: 'action',
  children: [],
  actionType: 'custom',
  useLabel: 'YES',
  buttonType: 'REGULAR',
  icon: 'arrow-left'
})

/**
 * add back button to main action bar
 */
export const addBack = (formDefinition, intl) => addAction(formDefinition, createBackButton(intl))

const actionGroupsBeforeReports = [
  ACTION_BACK_ID,
  ACTION_NEW_ID,
  ACTION_GROUP_CREATECOPY_ID,
  ACTION_DELETE_ID,
  ACTION_SAVE_ID
]

/**
 * add reports to form definition (use reports helper of app-extensions to create correct format of reports parameter)
 */
export const addReports = (formDefinition, reports, label) => {
  if (reports?.length === 0) {
    return formDefinition
  }

  const outputGroupExist = hasChild(formDefinition, idSelector(ACTION_GROUP_OUTPUT_ID))

  if (reports?.length === 1 && !outputGroupExist) {
    return addAction(formDefinition, reports[0], actionGroupsBeforeReports)
  } else {
    const formDefinitionWithOutputGroup = addOutputActionGroup(formDefinition, label)
    return adjustChildren(formDefinitionWithOutputGroup, idSelector(ACTION_GROUP_OUTPUT_ID), outputGroup =>
      appendChildren(outputGroup, reports)
    )
  }
}

/**
 * replace/override children of a box
 */
export const replaceBoxChildren = (formDefinition, boxId, newChildren) =>
  adjustChildren(formDefinition, idSelector(boxId), item => replaceChildren(item, newChildren))

/**
 * add children at the end of a box
 */
export const appendChildrenToBox = (formDefinition, boxId, newChildren) =>
  adjustChildren(formDefinition, idSelector(boxId), item => appendChildren(item, newChildren))

/**
 * add main action bar if it not already exists
 */
export const addMainActionBarIfMissing = formDefinition => {
  const actionBar = {
    id: MAIN_ACTION_BAR_ID,
    componentType: ACTION_BAR_TYPE,
    children: []
  }
  return prependChild(formDefinition, actionBar)
}

const addOutputActionGroup = (formDefinition, label) =>
  addActionGroup(formDefinition, actionGroupsBeforeReports, ACTION_GROUP_OUTPUT_ID, label, 'file-export')
export const addActionGroup = (formDefinition, actionGroupsBefore, id, label, icon) => {
  const group = {
    id,
    label,
    icon,
    componentType: ACTION_GROUP_TYPE,
    children: []
  }
  return addAction(formDefinition, group, actionGroupsBefore)
}

/**
 * remove actions from the action bar by ids
 * @param {object} container the container containing the actions, usually a form definition
 * @param {array} actionIds the ids to remove
 * @returns the container without the passed actions
 */
export const removeActions = (container, actionIds) => adjustActions(container, actionIds, () => false)

/**
 * run the passed `adjuster` against all actions
 *
 * handles actions at any depth of action bar and group combination or column action.
 * be sure to return a full action definition from your own adjustement function.
 * working with object destructuring is recommended for ease of use.
 * actions can be removed by returning any falsy value from the adjustment function.
 * any empty containers are removed after adjustement.
 */
export const adjustAllActions = (container, adjuster) => adjustActions(container, [], adjuster)

/**
 * looks for any action with the passed id and then runs `adjuster` to change the action.
 * multiple instances of the same action are also handled
 *
 * handles actions at any depth of action bar and group combination or column action.
 * be sure to return a full action definition from your own adjustement function.
 * working with object destructuring is recommended for ease of use.
 * actions can be removed by returning any falsy value from the adjustment function.
 * any empty containers are removed after adjustement.
 */
export const adjustAction = (container, actionId, adjuster) => adjustActions(container, [actionId], adjuster)

/**
 * looks for any actions whose id is contained in `actionIds` and then runs `adjuster` to change the actions.
 *
 * handles actions at any depth of action bar and group combination or column action.
 * be sure to return a full action definition from your own adjustement function.
 * working with object destructuring is recommended for ease of use.
 * actions can be removed by returning any falsy value from the adjustment function.
 * any empty containers are removed after adjustement.
 */
export const adjustActions = (container, actionIds, adjuster) =>
  adjustChildren(
    container,
    all(actionSelector, actionIds.length > 0 ? idsSelector(actionIds) : () => true),
    adjuster,
    child => {
      return (
        (child.componentType !== ACTION_BAR_TYPE && child.componentType !== ACTION_GROUP_TYPE) || hasChildren(child)
      )
    }
  )

/**
 * remove any field that match a predicate anywhere in the form definition
 * @param {*} container container the form definition (or any other container object)
 * @param {*} predicate the predicate function, receives the field as first and its container as the second parameter
 * @returns a copy of the container with the fields removed
 */
export const removeChildren = (parent, selector) => adjustChildren(parent, selector, () => false)

/**
 * adjust all field children of an item, also recursively adjusts children of children
 * @param {object} parent the item to adjust the children of
 * @param {function} adjuster accepts a field and returns the adjusted field
 * @returns the parent with the adjusted fields
 */
export const adjustFields = (container, adjuster) =>
  adjustChildren(container, item => item.componentType === FIELD_TYPE, adjuster)

/**
 * adjust all field set children of an item, also recursively adjusts children of children
 * @param {object} parent the item to adjust the children of
 * @param {function} adjuster accepts a field and returns the adjusted field
 * @returns the parent with the adjusted fields
 */
export const adjustFieldSets = (container, adjuster) =>
  adjustChildren(container, item => item.componentType === FIELD_SET_TYPE, adjuster)

/**
 * adjust all children with a given id of an item, also recursively adjusts children of children
 * @param {object} parent the item to adjust the children of
 * @param {array} ids accepts a box and returns the adjusted box
 * @param {function} adjuster accepts a box and returns the adjusted box
 * @returns the parent with the adjusted boxes
 */
export const adjustByIds = (container, ids, adjuster) => adjustChildren(container, idsSelector(ids), adjuster)

/**
 * adjust all children of an item that match a selector, also recursively adjusts children of children
 * @param {object} parent the item to adjust the children of
 * @param {function} selector accepts a child and returns true if it should be adjusted
 * @param {function} adjuster accepts a child and returns the adjusted child
 * @param {function} postFilter accepts a child and returns false if it should be discarded after adjustement
 * @returns the parent with the adjusted children
 */
const adjustChildren = (parent, selector, adjuster, postFilter = () => true) => ({
  ...parent,
  children: parent.children
    .map(child => {
      if (selector(child, parent)) {
        return adjuster(child)
      } else if (hasChildren(child)) {
        return adjustChildren(child, selector, adjuster, postFilter)
      } else {
        return child
      }
    })
    .filter(child => child)
    .filter(postFilter)
})

/**
 * checks if an item has a child matchin the given selector at any level
 * @param {object} parent the item to check the children
 * @param {function} selector a function that accepts a child and returns true if it should match
 * @returns true if the parent contains a child matching the given selector, false otherwise
 */
const hasChild = (parent, selector) => findInChildren(parent, selector).length > 0

/**
 * finds all children of an item that matches a selector at any level
 * @param {object} parent the item to check the children
 * @param {function} selector a function that accepts a child and returns true if it should match
 * @returns all matched children
 */
export const findInChildren = (parent, selector) => parent.children.flatMap(flatten).filter(selector)

/**
 * inserts child after some other children
 * @param {object} item the item to insert the child into
 * @param {object} child the item to insert
 * @param {array} idsBeforeChild ids of the children that should come before the inserted child
 * @returns the item with the child inserted
 */
export const insertChild = (item, child, idsBeforeChild) => insertChildren(item, [child], idsBeforeChild)
/**
 * inserts children after some other children
 * @param {object} item the item to insert the children into
 * @param {array} children the items to insert
 * @param {array} idsBeforeChild ids of the children that should come before the inserted children
 * @returns the item with the children inserted
 */
export const insertChildren = (item, children, idsBeforeChildren = []) => {
  const childrenToAdd = getMissingChildren(item, children)
  return {
    ...item,
    children: [
      ...item.children.filter(idsSelector(idsBeforeChildren)),
      ...childrenToAdd,
      ...item.children.filter(none(idsSelector([...idsBeforeChildren, ...childrenToAdd.map(child => child.id)])))
    ]
  }
}

export const prependChild = (item, child) => prependChildren(item, [child])
export const prependChildren = (item, children) => ({
  ...item,
  children: [...getMissingChildren(item, children), ...item.children]
})

export const appendChildren = (item, children) => ({
  ...item,
  children: [...item.children, ...getMissingChildren(item, children)]
})

export const replaceChildren = (item, children) => ({
  ...item,
  children
})

/**
 * filters a list of children to only include items that are not already children of the passed parent
 * @param {object} parent the item to check the children
 * @param {array} children the children to filter
 * @returns children that are missing from the parent
 */
export const getMissingChildren = (parent, children) =>
  children.filter(none(idsSelector(parent.children.map(item => item.id))))

/**
 * flattens an item so itself and all children are on the same level
 * @param {*} item the item to flatten
 * @returns the item and all its children
 */
export const flatten = item => {
  if (hasChildren(item)) {
    return [item, ...item.children.flatMap(flatten)]
  } else {
    return [item]
  }
}

const hasChildren = item => item.children && item.children.length > 0

export const all =
  (...selectors) =>
  item =>
    selectors.every(selector => selector(item))
export const none =
  (...selectors) =>
  item =>
    !selectors.some(selector => selector(item))

export const idSelector = id => idsSelector([id])
export const idsSelector = ids => item => ids.includes(item.id)
export const mainActionBarSelector = idSelector(MAIN_ACTION_BAR_ID)
export const componentTypesSelector = types => item => types.includes(item.componentType)
export const actionSelector = componentTypesSelector([ACTION_TYPE, REPORT_TYPE])
export const layoutSelector = componentTypesSelector([LAYOUT_TYPE])

export const createSimpleForm = options => ({componentType: 'form', children: [], ...options})
export const createHorizontalBox = options => ({
  id: 'box',
  label: null,
  componentType: 'layout',
  layoutType: 'horizontal-box',
  readonly: false,
  occupiesRemainingHeight: false,
  children: [],
  ...options
})

export const createVerticalBox = options => ({
  id: 'box',
  label: null,
  componentType: 'layout',
  layoutType: 'vertical-box',
  readonly: false,
  occupiesRemainingHeight: false,
  children: [],
  ...options
})

export const createTable = options => ({
  id: 'table',
  label: null,
  layoutType: 'table',
  occupiesRemainingHeight: false,
  readonly: false,
  clickable: false,
  collapsed: false,
  componentType: 'table',
  constriction: null,
  endpoint: null,
  searchEndpoint: null,
  selectable: null,
  sorting: null,
  children: [],
  ...options
})

export const createFieldSet = ({label, path, dataType, validation, additionalFieldAttributes = {}}) => ({
  id: path,
  label,
  componentType: 'field-set',
  children: [createField({label, path, dataType, validation, ...additionalFieldAttributes})],
  readonly: false,
  hidden: false,
  useLabel: 'yes'
})

export const createColumn = ({label, id, dataType, validation, options: fieldOptions, ...options}) => ({
  id,
  label,
  componentType: 'column',
  children: [createField({label, id, path: id, dataType, validation, options: fieldOptions})],
  readonly: true,
  hidden: false,
  useLabel: 'yes',

  clientRenderer: null,
  shrinkToContent: false,
  sortable: false,
  sticky: false,
  widthFixed: false,
  ...options
})

export const createField = ({label, id, path, dataType, validation, ...options}) => ({
  id: id || path,
  label,
  componentType: 'field',
  path,
  dataType,
  defaultValue: null,
  validation,
  ...options
})

export const createButtonCell = ({id, label, icon}) => ({
  id,
  label,
  icon,
  componentType: 'button',
  children: [],
  buttonStyle: 'PAPER',
  buttonType: 'ICON'
})

export const createActionCell = ({id, appId, label, icon}) => ({
  id,
  appId,
  label,
  icon,
  actionType: 'custom',
  componentType: 'action',
  children: [],
  buttonStyle: 'PAPER',
  buttonType: 'ICON',
  conditionName: null,
  dmsEntityModel: null,
  maxSelection: null,
  minSelection: null,
  onlyShowOnEmptyColumn: false,
  path: null,
  properties: {},
  runInBackgroundTask: false,
  showConfirmation: false,
  useLabel: 'YES'
})

export const createValidation = (...validationRules) => validationRules.reduce((acc, rule) => ({...acc, ...rule}), {})
export const createMandatoryValidation = () => ({mandatory: true})
export const createDecimalDigitsValidation = postPointDigits => ({decimalDigits: {postPointDigits}})
export const createNumberRangeValidation = (fromIncluding, toIncluding) => ({numberRange: {fromIncluding, toIncluding}})
