import _merge from 'lodash/merge'
import _reduce from 'lodash/reduce'
import {remoteLogger} from 'tocco-util'

const PREFIX = 'pseudoField_'

/**
 * add prefix to path for easier debugging
 * Note: the path should not contain a "." as else the path object must be nested for pseudo fields
 */
export const transformPath = path => {
  if (path.includes('.')) {
    remoteLogger.logError(`Path cannot contain a "."`)
  }

  return `${PREFIX}${path}`
}

/**
 * transform prefixed path back
 */
export const transformPathBack = path => {
  if (!path.startsWith(PREFIX)) {
    remoteLogger.logError(`Cannot transform path back if it does not start with "${PREFIX}"`)
  }

  return path.slice(PREFIX.length)
}

const createField = (path, label, dataType, validation = {}, readonly = false, otherProps = {}) => {
  const fullPath = transformPath(path)
  return {
    id: fullPath,
    label,
    componentType: 'field-set',
    readonly,
    children: [
      {
        id: fullPath,
        componentType: 'field',
        pseudoField: true,
        path: fullPath,
        dataType,
        validation,
        ...otherProps
      }
    ]
  }
}

const createEntityPath = (path, type, value) => ({
  [transformPath(path)]: {
    type,
    value,
    writable: true
  }
})

/**
 * create a choice option used by createSingleChoiceField and createMultipleChoiceField
 * @param {string} id - id of option
 * @param {object} options - object containing things like label, labelComponent, checked, immutable, hideCheckbox
 */
export const createChoiceOption = (id, options) => ({
  id,
  ...options
})

/**
 * create a select option used by createSelectField
 * @param {string} key - key of option
 * @param {string} display - display of option
 * @param {boolean} selected - true if option is selected
 */
export const createSelectOption = (key, display, selected) => ({
  key,
  display,
  selected
})

/**
 * create a field of type string which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - initial value of field
 * @param {string} label - label displayed
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createStringField = (path, value, label, mandatory = false, readonly = false) => ({
  formField: createField(path, label, 'string', {mandatory}, readonly),
  entityPath: createEntityPath(path, 'string', value)
})

/**
 * create a field of text string which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - initial (multiline) value of field
 * @param {string} label - label displayed
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createTextField = (path, value, label, mandatory = false, readonly = false) => ({
  formField: createField(path, label, 'text', {mandatory}, readonly),
  entityPath: createEntityPath(path, 'text', value)
})

/**
 * create a checkbox field which can be added to a form
 * this is represented by a choice with selection type multiple but only a single available option
 * @param {string} path - path of field
 * @param {boolean} value - true if checkbox should be checked
 * @param {string} label - label displayed
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {object} otherProps - passed to editor for further configuration
 * @param {string} customMessage - id to use for validation message (Default: same as empty field)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createCheckbox = (
  path,
  value,
  label,
  mandatory = false,
  otherProps = {},
  customMessage = 'client.component.form.syncValidationRequired'
) => {
  const option = createChoiceOption(`${path}-checkbox`, {label, checked: value})
  return {
    formField: createField(path, '', 'choice', {mandatory, messages: {choice: customMessage}}, false, {
      ...otherProps,
      selectionType: 'multiple'
    }),
    entityPath: createEntityPath(path, 'choice', {options: [option]})
  }
}

/**
 * create a field of type choice which selection type single which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - list of all available options which can be created with createChoiceOption
 * @param {string} label - label displayed
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createSingleChoiceField = (path, value, label, mandatory = false, readonly = false, otherProps = {}) => {
  if (value.filter(o => o.checked).length > 1) {
    remoteLogger.logError('Only one option can be checked in a single choice field')
  }

  return {
    formField: createField(path, label, 'choice', {mandatory}, readonly, {...otherProps, selectionType: 'single'}),
    entityPath: createEntityPath(path, 'choice', {options: value})
  }
}

/**
 * create a field of type choice which selection type multiple which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - list of all available options which can be created with createChoiceOption
 * @param {string} label - label displayed
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @param {object} otherProps - passed to editor for further configuration
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createMultipleChoiceField = (
  path,
  value,
  label,
  mandatory = false,
  readonly = false,
  otherProps = {}
) => ({
  formField: createField(path, label, 'choice', {mandatory}, readonly, {...otherProps, selectionType: 'multiple'}),
  entityPath: createEntityPath(path, 'choice', {options: value})
})

/**
 * create a field of type choice which selection type multiple which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - list of all available options which can be created with createChoiceOption
 * @param {string} label - label displayed
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @param {number} minSelection - minimum number of selected options allowed (Default: not restricted -> 0)
 * @param {number} maxSelection - maximum number of selected options allowed (Default: not restricted -> infinite)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createMultipleChoiceFieldWithSelection = (
  path,
  value,
  label,
  readonly = false,
  minSelection = undefined,
  maxSelection = undefined,
  otherProps = {}
) => {
  if (minSelection > maxSelection) {
    remoteLogger.logError('maxSelection must be larger than minSelection')
  }

  if (Number.isInteger(minSelection) && minSelection < 0) {
    remoteLogger.logError('minSelection must be at least 0')
  }

  if (Number.isInteger(maxSelection) && maxSelection <= 0) {
    remoteLogger.logError('maxSelection must be at least 1')
  }

  const validation = {
    ...(Number.isInteger(minSelection) && minSelection > 0 ? {minSelection, mandatory: true} : {}),
    ...(Number.isInteger(maxSelection) ? {maxSelection} : {})
  }

  return {
    formField: createField(path, label, 'choice', validation, readonly, {...otherProps, selectionType: 'multiple'}),
    entityPath: createEntityPath(path, 'choice', {options: value})
  }
}

/**
 * create a field of type pseudo select which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - list of all available options which can be created with createSelectOption
 * @param {string} label - label displayed
 * @param {boolean} isMulti - true if field should be multi-select field
 * @param {boolean} mandatory - true if a value is required (Default: false)
 * @param {boolean} readonly - true if field should not be editable (Default: false)
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createSelectField = (path, value, label, isMulti, mandatory = false, readonly = false) => {
  if (!isMulti && value.filter(o => o.selected).length > 1) {
    throw new Error('Only one option can be selected in a single select field')
  }

  return {
    formField: createField(path, label, 'pseudo-select', {mandatory}, readonly, {isMulti}),
    entityPath: createEntityPath(path, 'pseudo-select', {options: value})
  }
}

/**
 * create a field of type display which can be added to a form
 * @param {string} path - path of field
 * @param {string} value - value of field
 * @param {string} label - label displayed
 * @returns {object} returns form field (which should be added to the form definition)
 *                   and entityPath (which should be added to the entity paths of the displayed entity)
 */
export const createDisplayField = (path, value, label) => ({
  formField: createField(path, label, 'display', {}, true),
  entityPath: createEntityPath(path, 'display', value)
})

/**
 * merge entity paths into a single object from a list of pseudo fields
 * e.g. getMergedEntityPaths([createTextField(...), createTextField(...)])
 */
export const getMergedEntityPaths = pseudoFields => _merge({}, ...pseudoFields.map(f => f.entityPath))

/**
 * split flatten entity into two object entity (contains all regular fields) and pseudoFields
 */
export const splitFlattenEntity = (flattenEntity, fieldDefinitions) => {
  const pseudoFieldIds = fieldDefinitions.filter(f => f.pseudoField).map(f => f.id)
  const flattenEntityWithoutPseudoFields = _reduce(
    flattenEntity,
    (acc, val, key) => ({...acc, ...(pseudoFieldIds.includes(key) ? {} : {[key]: val})}),
    {}
  )
  const pseudoFields = _reduce(
    flattenEntity,
    (acc, val, key) => ({...acc, ...(pseudoFieldIds.includes(key) ? {[transformPathBack(key)]: val} : {})}),
    {}
  )
  return {
    entity: flattenEntityWithoutPseudoFields,
    pseudoFields
  }
}

/**
 * remove all pseudo fields from flatten entity and return copy
 */
export const removePseudoFieldsFromEntity = (flattenEntity, fieldDefinitions) => {
  const pseudoFieldIds = fieldDefinitions.filter(f => f.pseudoField).map(f => f.id)
  return Object.entries(flattenEntity).reduce(
    (acc, [key, value]) => ({
      ...acc,
      ...(pseudoFieldIds.includes(key) ? {} : {[key]: value})
    }),
    {}
  )
}
