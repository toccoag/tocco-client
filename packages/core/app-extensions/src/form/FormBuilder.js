// propTypes are not recognized properly in this file
import PropTypes from 'prop-types'
import React from 'react'
import {js} from 'tocco-util'

import formTraverser from './formTraverser'

const FormBuilder = props => {
  const {
    componentMapping,
    entity,
    mode,
    beforeRenderField,
    formDefinition,
    formValues,
    formName,
    entityModel,
    fieldMappingType,
    customRenderedActions,
    readonly,
    labelPosition
  } = props

  const data = {
    children: formDefinition.children,
    componentMapping,
    entity,
    mode,
    customRenderedActions,
    beforeRenderField,
    formValues,
    fieldMappingType,
    formName,
    entityModel,
    labelPosition,
    parentReadOnly: readonly || formDefinition.readonly
  }
  return formTraverser(data)
}

FormBuilder.propTypes = {
  entity: PropTypes.object,
  formName: PropTypes.string.isRequired,
  formDefinition: PropTypes.object.isRequired,
  entityModel: PropTypes.object,
  formValues: PropTypes.object,
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  readonly: PropTypes.bool,
  mode: PropTypes.string,
  componentMapping: PropTypes.objectOf(PropTypes.func),
  beforeRenderField: PropTypes.func,
  customRenderedActions: PropTypes.objectOf(PropTypes.func)
}

const areEqual = (prevProps, nextProps) => {
  const diff = Object.keys(js.difference(prevProps, nextProps))
  return diff.length === 0
}

export default React.memo(FormBuilder, areEqual)
