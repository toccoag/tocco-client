import _get from 'lodash/get'
import {FormattedMessage, FormattedRelativeTime} from 'react-intl'
import {actions as formActions, getFormValues} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {all, call, put, select, debounce, takeEvery} from 'redux-saga/effects'
import {date as dateUtil} from 'tocco-util'

import form from '../form'
import notification from '../notification'
import rest from '../rest'

import formErrorUtils from './formErrors'
import {isValueEmpty} from './reduxForm'
import {getEntityForSubmit} from './sagasUtils'

export default function* sagas(formConfig) {
  yield all([
    debounce(500, formActionTypes.CHANGE, onChange, formConfig),
    takeEvery(formActionTypes.REGISTER_FIELD, onRegister, formConfig),
    takeEvery(formActionTypes.STOP_ASYNC_VALIDATION, asyncValidationStop)
  ])
}

export function* autoComplete({formId}, triggerFieldName, entity, autoCompleteEndpoint, context) {
  const autoCompleteOptions = {
    method: 'POST',
    body: {
      triggerField: triggerFieldName,
      entity,
      context
    },
    acceptedStatusCodes: [403]
  }

  const response = yield call(rest.requestSaga, autoCompleteEndpoint, autoCompleteOptions)
  const values = _get(response, 'body.values', [])
  const formValues = yield call(loadFormValues, formId)

  yield all(
    Object.entries(values).map(([fieldName, {mode, value}]) => {
      if (mode === 'pseudo_options') {
        const pseudoPath = form.transformPath(fieldName)
        const currentValue = formValues[pseudoPath]
        const options = currentValue.options.map(option => ({
          ...option,
          selected: option.key === value.key
        }))
        return put(
          formActions.change(formId, pseudoPath, {
            ...currentValue,
            options
          })
        )
      } else if (mode === 'override' || (mode === 'if_empty' && isValueEmpty(formValues[fieldName]))) {
        return put(formActions.change(formId, fieldName, value))
      }
      return null
    })
  )
}

export function* onChange(formConfig, {meta}) {
  const {stateSelector} = formConfig
  const {field} = meta
  const fieldDefinition = (yield select(stateSelector)).fieldDefinitions.find(fd => fd.id === field)
  if (fieldDefinition && fieldDefinition.autoCompleteEndpoint) {
    const entity = yield call(getEntityForSubmit, formConfig)
    yield call(autoComplete, formConfig, field, entity, fieldDefinition.autoCompleteEndpoint, {isChange: true})
  }
}

export function* onRegister(formConfig, {payload: {name: field}, meta: {form: formId}}) {
  const {stateSelector} = formConfig
  const {fieldDefinitions, mode} = yield select(stateSelector)
  if (mode === 'create') {
    const fieldDefinition = fieldDefinitions.find(fd => fd.id === field)
    if (fieldDefinition && fieldDefinition.autoCompleteEndpoint) {
      // only call auto-complete for default values
      const formValues = yield call(loadFormValues, formId)
      if (formValues && formValues[field]) {
        const entity = yield call(getEntityForSubmit, formConfig)
        yield call(autoComplete, formConfig, field, entity, fieldDefinition.autoCompleteEndpoint, {isRegister: true})
        // reinitialize form so initial auto complete values do not mark form as dirty
        const reloadedFormValues = yield call(loadFormValues, formId)
        yield put(formActions.initialize(formId, reloadedFormValues))
      }
    }
  }
}

// helper function to allow easy mocking in test
export function* loadFormValues(formId) {
  return yield select(getFormValues(formId))
}

export function* asyncValidationStop({payload}) {
  if (payload) {
    const hasOutdatedError = formErrorUtils.hasOutdatedError(payload)
    if (hasOutdatedError) {
      const outdatedError = formErrorUtils.getOutdatedError(payload)
      const timeDiff = dateUtil.selectUnit(new Date(outdatedError.updateTimestamp))
      const titleId = `client.component.form.${outdatedError.sameEntity ? 'outdated' : 'relatedOutdated'}ErrorTitle`

      yield put(
        notification.toaster({
          type: 'warning',
          key: 'outdated-warning',
          title: (
            <FormattedMessage
              id={titleId}
              values={{
                model: outdatedError.model,
                key: outdatedError.key
              }}
            />
          ),
          body: (
            <FormattedMessage
              id="client.component.form.outdatedErrorDescription"
              values={{
                ago: <FormattedRelativeTime value={timeDiff.diffValue} unit={timeDiff.unit} />,
                user: outdatedError.updateUser
              }}
            />
          )
        })
      )
    }
  }
}
