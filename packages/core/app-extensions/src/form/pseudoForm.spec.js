import {remoteLogger} from 'tocco-util'

import {
  transformPath,
  transformPathBack,
  createChoiceOption,
  createStringField,
  createTextField,
  createSingleChoiceField,
  createMultipleChoiceField,
  createMultipleChoiceFieldWithSelection,
  createDisplayField,
  getMergedEntityPaths,
  splitFlattenEntity,
  removePseudoFieldsFromEntity
} from './pseudoForm'

describe('app-extensions', () => {
  describe('form', () => {
    describe('pseudoForm', () => {
      describe('transformPath', () => {
        test('transform path', () => {
          expect(transformPath('path')).to.deep.eq('pseudoField_path')
        })

        test('throw error if a dot is in the path', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          transformPath('my.path')

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Path cannot contain a "."')
        })
      })

      describe('transformPathBack', () => {
        test('transform path back', () => {
          expect(transformPathBack('pseudoField_path')).to.deep.eq('path')
        })

        test('throw error if prefix is missing', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          transformPathBack('path')

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql(
            'Cannot transform path back if it does not start with "pseudoField_"'
          )
        })
      })

      describe('createStringField', () => {
        test('create field', () => {
          const expected = {
            formField: {
              id: 'pseudoField_path',
              label: 'label',
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_path',
                  componentType: 'field',
                  pseudoField: true,
                  path: 'pseudoField_path',
                  dataType: 'string',
                  validation: {
                    mandatory: false
                  }
                }
              ]
            },
            entityPath: {
              pseudoField_path: {
                type: 'string',
                value: 'value',
                writable: true
              }
            }
          }
          expect(createStringField('path', 'value', 'label')).to.deep.eq(expected)
        })
      })

      describe('createTextField', () => {
        test('create field', () => {
          const expected = {
            formField: {
              id: 'pseudoField_path',
              label: 'label',
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_path',
                  componentType: 'field',
                  pseudoField: true,
                  path: 'pseudoField_path',
                  dataType: 'text',
                  validation: {
                    mandatory: false
                  }
                }
              ]
            },
            entityPath: {
              pseudoField_path: {
                type: 'text',
                value: 'value1\nvalue2',
                writable: true
              }
            }
          }
          expect(createTextField('path', 'value1\nvalue2', 'label')).to.deep.eq(expected)
        })
      })

      describe('createSingleChoiceField', () => {
        test('create field', () => {
          const options = [
            createChoiceOption('breakfast', {label: 'breakfast', checked: false}),
            createChoiceOption('lunch', {label: 'lunch', checked: true}),
            createChoiceOption('dinner', {label: 'dinner', checked: false})
          ]
          const expected = {
            formField: {
              id: 'pseudoField_path',
              label: 'label',
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_path',
                  componentType: 'field',
                  pseudoField: true,
                  path: 'pseudoField_path',
                  dataType: 'choice',
                  validation: {
                    mandatory: false
                  },
                  selectionType: 'single'
                }
              ]
            },
            entityPath: {
              pseudoField_path: {
                type: 'choice',
                value: {
                  options
                },
                writable: true
              }
            }
          }
          expect(createSingleChoiceField('path', options, 'label')).to.deep.eq(expected)
        })

        test('more than once selected in not a valid input', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          const options = [
            createChoiceOption('breakfast', {label: 'breakfast', checked: true}),
            createChoiceOption('lunch', {label: 'lunch', checked: true}),
            createChoiceOption('dinner', {label: 'dinner', checked: false})
          ]
          createSingleChoiceField('path', options, 'label')

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Only one option can be checked in a single choice field')
        })
      })

      describe('createMultipleChoiceField', () => {
        test('create field', () => {
          const options = [
            createChoiceOption('breakfast', {label: 'breakfast', checked: false}),
            createChoiceOption('lunch', {label: 'lunch', checked: true, immutable: true}),
            createChoiceOption('dinner', {label: 'dinner', checked: false})
          ]
          const expected = {
            formField: {
              id: 'pseudoField_path',
              label: 'label',
              componentType: 'field-set',
              readonly: false,
              children: [
                {
                  id: 'pseudoField_path',
                  componentType: 'field',
                  pseudoField: true,
                  path: 'pseudoField_path',
                  dataType: 'choice',
                  validation: {
                    mandatory: false
                  },
                  selectionType: 'multiple'
                }
              ]
            },
            entityPath: {
              pseudoField_path: {
                type: 'choice',
                value: {
                  options
                },
                writable: true
              }
            }
          }
          expect(createMultipleChoiceField('path', options, 'label')).to.deep.eq(expected)
        })

        test('should hide checkboxes', () => {
          const options = [
            createChoiceOption('breakfast', {label: 'breakfast', hideCheckbox: true}),
            createChoiceOption('lunch', {label: 'lunch', hideCheckbox: true}),
            createChoiceOption('dinner', {label: 'dinner', hideCheckbox: true})
          ]
          const result = createMultipleChoiceField('path', options, 'label')
          expect(result.entityPath.pseudoField_path.value.options).to.deep.eql([
            {hideCheckbox: true, id: 'breakfast', label: 'breakfast'},
            {hideCheckbox: true, id: 'lunch', label: 'lunch'},
            {hideCheckbox: true, id: 'dinner', label: 'dinner'}
          ])
        })
      })

      describe('createMultipleChoiceFieldWithSelection', () => {
        const createField = (minSelection, maxSelection) =>
          createMultipleChoiceFieldWithSelection('path', [], 'label', false, minSelection, maxSelection)
        const getValidation = field => field.formField.children[0].validation

        test('create field with minSelection 0', () => {
          const expectedValidation = {}

          expect(getValidation(createField(0, undefined))).to.deep.eq(expectedValidation)
        })

        test('create field with minSelection 1', () => {
          const expectedValidation = {
            minSelection: 1,
            mandatory: true
          }

          expect(getValidation(createField(1, undefined))).to.deep.eq(expectedValidation)
        })

        test('create field with invalid minSelection', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          getValidation(createField(-1, undefined))

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('minSelection must be at least 0')
        })

        test('create field with maxSelection', () => {
          const expectedValidation = {
            maxSelection: 1
          }

          expect(getValidation(createField(undefined, 1))).to.deep.eq(expectedValidation)
        })

        test('create field with invalid maxSelection', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          getValidation(createField(undefined, 0))

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('maxSelection must be at least 1')
        })

        test('create field with minSelection and maxSelection', () => {
          const expectedValidation = {
            minSelection: 1,
            maxSelection: 2,
            mandatory: true
          }

          expect(getValidation(createField(1, 2))).to.deep.eq(expectedValidation)
        })

        test('create field with minSelection larger than maxSelection', () => {
          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          getValidation(createField(2, 1))

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('maxSelection must be larger than minSelection')
        })

        test('create field with minSelection equals maxSelection', () => {
          const expectedValidation = {
            minSelection: 2,
            maxSelection: 2,
            mandatory: true
          }

          expect(getValidation(createField(2, 2))).to.deep.eq(expectedValidation)
        })
      })

      describe('create display field', () => {
        test('should create display field', () => {
          const expectedResult = {
            formField: {
              id: 'pseudoField_path',
              label: 'label',
              componentType: 'field-set',
              readonly: true,
              children: [
                {
                  id: 'pseudoField_path',
                  componentType: 'field',
                  pseudoField: true,
                  path: 'pseudoField_path',
                  dataType: 'display',
                  validation: {}
                }
              ]
            },
            entityPath: {
              pseudoField_path: {
                type: 'display',
                value: 'value',
                writable: true
              }
            }
          }
          const result = createDisplayField('path', 'value', 'label')
          expect(result).to.deep.equal(expectedResult)
        })
      })

      describe('getMergedEntityPaths', () => {
        test('merge paths', () => {
          const pathObject = {
            type: 'choice',
            value: {
              options: []
            },
            writable: true
          }
          const otherPathObject = {
            type: 'string',
            value: 'hi',
            writable: true
          }
          const pseudoFields = [
            {
              formField: {},
              entityPath: {
                path: pathObject
              }
            },
            {
              formField: {},
              entityPath: {
                otherPath: otherPathObject
              }
            }
          ]
          const expected = {
            path: pathObject,
            otherPath: otherPathObject
          }

          expect(getMergedEntityPaths(pseudoFields)).to.deep.eq(expected)
        })
      })
    })
  })

  describe('splitFlattenEntity', () => {
    test('split flatten entity in the regular entity and pseudoFields', () => {
      const fieldDefinitions = [
        {
          id: 'firstname'
        },
        {
          id: 'pseudoField_myField',
          pseudoField: true
        }
      ]
      const flattenEntity = {
        firstname: 'Hans',
        pseudoField_myField: {
          options: []
        }
      }
      const expected = {
        entity: {
          firstname: 'Hans'
        },
        pseudoFields: {
          myField: {
            options: []
          }
        }
      }
      expect(splitFlattenEntity(flattenEntity, fieldDefinitions)).to.deep.eq(expected)
    })
  })

  describe('removePseudoFieldsFromEntity', () => {
    test('remove pseudo field from flatten entity', () => {
      const fieldDefinitions = [
        {
          id: 'firstname'
        },
        {
          id: 'pseudoField_myField',
          pseudoField: true
        }
      ]
      const flattenEntity = {
        firstname: 'Hans',
        pseudoField_myField: {
          options: []
        }
      }
      const expected = {
        firstname: 'Hans'
      }
      expect(removePseudoFieldsFromEntity(flattenEntity, fieldDefinitions)).to.deep.eq(expected)
    })
  })
})
