import _get from 'lodash/get'
import {IntlStub} from 'tocco-test-util'

import {
  ACTION_TYPE,
  LAYOUT_TYPE,
  ACTION_GROUP_CREATECOPY_ID,
  ACTION_GROUP_ACTIONS_ID,
  ACTION_GROUP_OUTPUT_ID,
  addCreate,
  addReports,
  MAIN_ACTION_BAR_ID,
  FIELD_TYPE,
  FIELD_SET_TYPE,
  removeBoxes,
  removeFields,
  removeChildren,
  adjustFields,
  adjustFieldSets,
  removeActions,
  adjustAllActions,
  adjustAction,
  adjustActions,
  adjustByIds,
  addBack,
  replaceBoxChildren,
  appendChildrenToBox,
  addActionGroup,
  ACTION_BACK_ID,
  ACTION_NEW_ID,
  idSelector,
  idsSelector,
  actionSelector,
  all,
  none,
  flatten,
  getMissingChildren,
  replaceChildren,
  appendChildren,
  prependChildren,
  insertChildren,
  findInChildren
} from './formModifier'

describe('app-extensions', () => {
  describe('form', () => {
    describe('formModifier', () => {
      const formDefinitionFull = {
        id: 'Test_list',
        children: [
          {
            id: MAIN_ACTION_BAR_ID,
            children: [
              {
                id: ACTION_GROUP_CREATECOPY_ID
              },
              {
                reportId: 'reportFormDefinition'
              }
            ]
          }
        ]
      }

      const formDefinitionBoxes = {
        children: [
          {
            id: 'top-box',
            componentType: 'layout',
            children: [
              {
                id: 'first-box',
                componentType: 'layout',
                children: []
              },
              {
                id: 'second-box',
                componentType: 'layout',
                children: [
                  {
                    id: 'low-box',
                    componentType: 'layout',
                    children: [
                      {
                        id: 'field_1',
                        componentType: 'field'
                      },
                      {
                        id: 'field_2',
                        componentType: 'field'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }

      describe('addCreate', () => {
        test('in create scope do not add new action', () => {
          const emptyFormDefinition = {
            id: 'Test_create',
            children: []
          }
          expect(addCreate(emptyFormDefinition, IntlStub).children).has.length(0)
        })

        test('do not add create action if createcopy actiongroup already exists', () => {
          expect(addCreate(formDefinitionFull, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            ACTION_GROUP_CREATECOPY_ID,
            'reportFormDefinition'
          ])
        })

        test('add create action if no main action bar exists', () => {
          const formDefinition = {
            id: 'Test_list',
            children: []
          }
          expect(addCreate(formDefinition, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            'new'
          ])
        })

        test('add create action if no createcopy action group exists', () => {
          const formDefinition = {
            id: 'Test_list',
            children: [
              {
                id: MAIN_ACTION_BAR_ID,
                children: [
                  {
                    reportId: 'reportFormDefinition'
                  }
                ]
              }
            ]
          }

          expect(addCreate(formDefinition, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            'new',
            'reportFormDefinition'
          ])
        })
      })

      describe('addBack', () => {
        test('add back action if no main action bar exists', () => {
          const formDefinition = {
            id: 'Test_list',
            children: []
          }
          expect(addBack(formDefinition, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            'back'
          ])
        })

        test('add back action inside existing main action bar', () => {
          const formDefinition = {
            id: 'Test_list',
            children: [
              {
                id: MAIN_ACTION_BAR_ID,
                children: [
                  {
                    reportId: 'reportFormDefinition'
                  }
                ]
              }
            ]
          }

          expect(addBack(formDefinition, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            'back',
            'reportFormDefinition'
          ])
        })

        test('add back before createcopy', () => {
          const formDefinition = {
            children: [
              {
                id: MAIN_ACTION_BAR_ID,
                children: [
                  {
                    id: ACTION_GROUP_CREATECOPY_ID
                  },
                  {
                    id: ACTION_GROUP_ACTIONS_ID
                  }
                ]
              }
            ]
          }
          expect(addBack(formDefinition, IntlStub).children[0].children.map(r => r.reportId || r.id)).to.be.eql([
            'back',
            ACTION_GROUP_CREATECOPY_ID,
            ACTION_GROUP_ACTIONS_ID
          ])
        })
      })

      describe('addReports', () => {
        const reports = [
          {
            reportId: 'reportDynamic1'
          },
          {
            reportId: 'reportDynamic2'
          }
        ]

        const report = [
          {
            id: 'reportDynamic1',
            reportId: 'reportDynamic1'
          }
        ]

        test('add reports', () => {
          expect(
            addReports(formDefinitionFull, reports, 'output').children[0].children.map(r => r.reportId || r.id)
          ).to.be.eql([ACTION_GROUP_CREATECOPY_ID, 'output', 'reportFormDefinition'])
          expect(
            addReports(formDefinitionFull, reports, 'output').children[0].children[1].children.map(
              r => r.reportId || r.id
            )
          ).to.be.eql(['reportDynamic1', 'reportDynamic2'])
        })

        test('add one report', () => {
          expect(
            addReports(formDefinitionFull, report, 'output').children[0].children.map(r => r.reportId || r.id)
          ).to.be.eql([ACTION_GROUP_CREATECOPY_ID, 'reportDynamic1', 'reportFormDefinition'])
        })

        test('add report to existing group', () => {
          const formDefinitionWithOutputGroup = {
            id: 'Test_list',
            children: [
              {
                id: MAIN_ACTION_BAR_ID,
                children: [
                  {
                    id: ACTION_GROUP_OUTPUT_ID,
                    children: []
                  }
                ]
              }
            ]
          }
          const groups = addReports(formDefinitionWithOutputGroup, report, 'output').children[0].children
          expect(groups.map(r => r.id)).to.be.eql([ACTION_GROUP_OUTPUT_ID])
          expect(groups[0].children.map(r => r.reportId)).to.be.eql(['reportDynamic1'])
        })

        test('add reports after createcopy/back/new and before action group', () => {
          const formDefinition = {
            children: [
              {
                id: MAIN_ACTION_BAR_ID,
                children: [
                  {
                    id: ACTION_BACK_ID
                  },
                  {
                    id: ACTION_NEW_ID
                  },
                  {
                    id: ACTION_GROUP_CREATECOPY_ID
                  },
                  {
                    id: ACTION_GROUP_ACTIONS_ID
                  }
                ]
              }
            ]
          }
          expect(
            addReports(formDefinition, reports, 'output').children[0].children.map(r => r.reportId || r.id)
          ).to.be.eql([ACTION_BACK_ID, ACTION_NEW_ID, ACTION_GROUP_CREATECOPY_ID, 'output', ACTION_GROUP_ACTIONS_ID])
        })
      })

      describe('removeBoxes', () => {
        test('should remove boxes', () => {
          const modifiedFormDefinition = removeBoxes(formDefinitionBoxes, ['top-box'])
          expect(modifiedFormDefinition.children).to.be.empty
        })
        test('should remove boxes at any depth', () => {
          const modifiedFormDefinition = removeBoxes(formDefinitionBoxes, ['low-box'])
          expect(_get(modifiedFormDefinition, ['children', '0', 'children', '1', 'children'])).to.be.empty
        })
        test('should leave definition unchanged when boxes do not exist', () => {
          const modifiedFormDefinition = removeBoxes(formDefinitionBoxes, ['third-box'])
          expect(modifiedFormDefinition).to.deep.eq(formDefinitionBoxes)
        })
      })

      describe('removeFields', () => {
        const otherFormDefinitionBoxes = {
          children: [
            {
              id: 'top-box',
              componentType: 'layout',
              children: [
                {
                  id: 'first-box',
                  componentType: 'layout',
                  children: [
                    {
                      id: 'field_1',
                      componentType: 'field'
                    },
                    {
                      id: 'field_2',
                      componentType: 'field'
                    }
                  ]
                },
                {
                  id: 'second-box',
                  componentType: 'layout',
                  children: [
                    {
                      id: 'low-box',
                      componentType: 'layout',
                      children: [
                        {
                          id: 'field_3',
                          componentType: 'field'
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }

        test('should remove fields', () => {
          const modifiedFormDefinition = removeFields(otherFormDefinitionBoxes, ['field_1', 'field_3'])
          expect(modifiedFormDefinition.children[0].children[0].children[0].id).to.eq('field_2')
          expect(modifiedFormDefinition.children[0].children[1].children[0].children).to.be.empty
        })
        test('should not remove box', () => {
          const modifiedFormDefinition = removeFields(otherFormDefinitionBoxes, ['second-box'])
          expect(modifiedFormDefinition.children[0].children[1]).to.not.be.null
        })

        describe('removeChildren', () => {
          test('should remove fields from box', () => {
            const modifiedFormDefinition = removeChildren(
              formDefinitionBoxes,
              (item, container) => container.id === 'first-box'
            )
            expect(modifiedFormDefinition.children[0].children[0].children).to.be.empty
          })
        })
      })

      describe('removeActions', () => {
        const formDefinitionActions = {
          children: [
            {
              id: MAIN_ACTION_BAR_ID,
              componentType: 'action-bar',
              children: [
                {
                  componentType: 'action-group',
                  children: [
                    {
                      id: 'action1',
                      componentType: 'action'
                    },
                    {
                      id: 'action2',
                      componentType: 'action'
                    }
                  ]
                }
              ]
            }
          ]
        }
        test('should remove actions', () => {
          const modifiedFormDefinition = removeActions(formDefinitionActions, ['action1'])
          expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children'])).to.have.length(1)
        })
        test('should remove empty groups', () => {
          const modifiedFormDefinition = removeActions(formDefinitionActions, ['action1', 'action2'])
          expect(_get(modifiedFormDefinition, ['children'])).to.be.empty
        })
        test('should leave definition unchanged when actions do not exist', () => {
          const modifiedFormDefinition = removeActions(formDefinitionActions, ['action3'])
          expect(modifiedFormDefinition).to.deep.eq(formDefinitionActions)
        })
      })

      describe('adjustActions', () => {
        const formDefinitionActions = {
          children: [
            {
              id: MAIN_ACTION_BAR_ID,
              componentType: 'action-bar',
              children: [
                {
                  componentType: 'action-group',
                  children: [
                    {
                      id: 'action1',
                      componentType: 'action'
                    }
                  ]
                }
              ]
            }
          ]
        }
        test('should change action', () => {
          const modifiedFormDefinition = adjustActions(formDefinitionActions, ['action1'], action => ({
            ...action,
            id: 'new-id'
          }))
          expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children', '0', 'id'])).to.eq(
            'new-id'
          )
        })

        test('should change column action', () => {
          const formDefinition = {
            componentType: 'form',
            children: [
              {
                componentType: 'table',
                children: [
                  {
                    componentType: 'column',
                    children: [
                      {
                        id: 'action1',
                        componentType: 'action'
                      }
                    ]
                  }
                ],
                layoutType: 'table'
              }
            ]
          }

          const modifiedFormDefinition = adjustActions(formDefinition, ['action1'], action => ({
            ...action,
            id: 'new-id'
          }))
          expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children', '0', 'id'])).to.eq(
            'new-id'
          )
        })

        test('should leave definition unchanged when action does not exist', () => {
          const modifiedFormDefinition = adjustActions(formDefinitionActions, ['action2'], action => ({
            ...action,
            id: 'new-id'
          }))
          expect(modifiedFormDefinition).to.deep.eq(formDefinitionActions)
        })

        test('should change all actions', () => {
          const modifiedFormDefinition = adjustActions(formDefinitionActions, [], action => ({
            ...action,
            id: 'new-id'
          }))
          expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children', '0', 'id'])).to.eq(
            'new-id'
          )
        })

        describe('adjustAction', () => {
          test('should change an action', () => {
            const modifiedFormDefinition = adjustAction(formDefinitionActions, 'action1', action => ({
              ...action,
              id: 'new-id'
            }))
            expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children', '0', 'id'])).to.eq(
              'new-id'
            )
          })
        })

        describe('adjustAllActions', () => {
          test('should change all actions', () => {
            const modifiedFormDefinition = adjustAllActions(formDefinitionActions, action => ({
              ...action,
              id: 'new-id'
            }))
            expect(_get(modifiedFormDefinition, ['children', '0', 'children', '0', 'children', '0', 'id'])).to.eq(
              'new-id'
            )
          })
        })
      })

      describe('adjustByIds', () => {
        test('should adjust each child by id', () => {
          const adjustedFormDefinition = adjustByIds(formDefinitionBoxes, ['first-box', 'field_1'], child => ({
            ...child,
            label: 'adjusted'
          }))
          expect(adjustedFormDefinition.children[0].children[0].label).to.be.eq('adjusted')
          expect(adjustedFormDefinition.children[0].children[1].children[0].children[0].label).to.be.eq('adjusted')
        })

        test('should not touch other elements', () => {
          const adjustedFormDefinition = adjustByIds(formDefinitionBoxes, ['some id'], child => ({
            ...child,
            label: 'adjusted'
          }))
          expect(adjustedFormDefinition).to.deep.eq(formDefinitionBoxes)
        })
      })

      describe('replaceBoxChildren', () => {
        const newChildren = [
          {
            id: 'field_3',
            componentType: 'field'
          }
        ]

        test('replace if box id exists', () => {
          const modifiedFormDefinition = replaceBoxChildren(formDefinitionBoxes, 'low-box', newChildren)
          expect(modifiedFormDefinition.children[0].children[1].children[0].children.length).to.be.eq(1)
          expect(modifiedFormDefinition.children[0].children[1].children[0].children[0].id).to.be.eq('field_3')
        })

        test('do nothing if box id does not exist', () => {
          const modifiedFormDefinition = replaceBoxChildren(formDefinitionBoxes, 'lowest-box', newChildren)
          expect(modifiedFormDefinition).to.be.deep.eq(formDefinitionBoxes)
        })
      })

      describe('appendChildrenToBox', () => {
        const newChildren = [
          {
            id: 'field_3',
            componentType: 'field'
          }
        ]

        test('replace if box id exists', () => {
          const modifiedFormDefinition = appendChildrenToBox(formDefinitionBoxes, 'low-box', newChildren)
          expect(modifiedFormDefinition.children[0].children[1].children[0].children.length).to.be.eq(3)
          expect(modifiedFormDefinition.children[0].children[1].children[0].children[0].id).to.be.eq('field_1')
          expect(modifiedFormDefinition.children[0].children[1].children[0].children[1].id).to.be.eq('field_2')
          expect(modifiedFormDefinition.children[0].children[1].children[0].children[2].id).to.be.eq('field_3')
        })

        test('do nothing if box id does not exist', () => {
          const modifiedFormDefinition = appendChildrenToBox(formDefinitionBoxes, 'lowest-box', newChildren)
          expect(modifiedFormDefinition).to.be.deep.eq(formDefinitionBoxes)
        })
      })

      describe('addActionGroup', () => {
        const actionGroupsBeforeReports = ['createcopy', 'delete', 'save']

        test('add actionGroup', () => {
          expect(
            addActionGroup(
              formDefinitionFull,
              actionGroupsBeforeReports,
              'output',
              'label',
              'icon'
            ).children[0].children.map(r => r.reportId || r.id)
          ).to.be.eql([ACTION_GROUP_CREATECOPY_ID, 'output', 'reportFormDefinition'])
        })
      })

      describe('findInChildren', () => {
        const childChild = {
          id: 'childChild'
        }
        const child = {
          id: 'child',
          children: [childChild]
        }
        const parent = {
          id: 'parent',
          children: [child]
        }
        test('should find correct child', () => {
          expect(findInChildren(parent, idSelector('child'))).to.deep.eq([child])
        })
        test('should ignore parent', () => {
          expect(findInChildren(parent, idSelector('parent'))).to.be.empty
        })
        test('should find children over multiple levels', () => {
          expect(findInChildren(parent, idsSelector(['child', 'childChild']))).to.deep.eq([child, childChild])
        })
      })

      describe('children adjustemnt', () => {
        const newChild = {id: 'new'}
        const firstChild = {id: 'first', componentType: FIELD_TYPE}
        const secondChild = {id: 'second', componentType: FIELD_SET_TYPE}
        const item = {children: [firstChild, secondChild]}

        describe('insertChildren', () => {
          test('should insert child at front', () => {
            expect(insertChildren(item, [newChild]).children).to.deep.eq([newChild, firstChild, secondChild])
          })
          test('should insert child in the middle', () => {
            expect(insertChildren(item, [newChild], ['first']).children).to.deep.eq([firstChild, newChild, secondChild])
          })
          test('should insert child at back', () => {
            expect(insertChildren(item, [newChild], ['first', 'second']).children).to.deep.eq([
              firstChild,
              secondChild,
              newChild
            ])
          })
          test('should skip existing child', () => {
            expect(insertChildren(item, [newChild, firstChild]).children).to.deep.eq([
              newChild,
              firstChild,
              secondChild
            ])
          })
          test('should ignore empty children', () => {
            expect(insertChildren(item, [])).to.deep.eq(item)
          })
        })

        describe('prependChildren', () => {
          test('should prepend child', () => {
            expect(prependChildren(item, [newChild]).children).to.deep.eq([newChild, firstChild, secondChild])
          })
          test('should skip existing child', () => {
            expect(prependChildren(item, [newChild, firstChild]).children).to.deep.eq([
              newChild,
              firstChild,
              secondChild
            ])
          })
          test('should ignore empty children', () => {
            expect(prependChildren(item, [])).to.deep.eq(item)
          })
        })

        describe('appendChildren', () => {
          test('should append child', () => {
            expect(appendChildren(item, [newChild]).children).to.deep.eq([firstChild, secondChild, newChild])
          })
          test('should skip existing child', () => {
            expect(appendChildren(item, [newChild, firstChild]).children).to.deep.eq([
              firstChild,
              secondChild,
              newChild
            ])
          })
          test('should ignore empty children', () => {
            expect(appendChildren(item, [])).to.deep.eq(item)
          })
        })

        describe('replaceChildren', () => {
          test('should replace children', () => {
            expect(replaceChildren(item, [newChild]).children).to.deep.eq([newChild])
          })
        })

        describe('adjustFields', () => {
          test('should change fields', () => {
            expect(adjustFields(item, f => ({...f, id: 'new-id'})).children).to.deep.eq([
              {
                ...firstChild,
                id: 'new-id'
              },
              secondChild
            ])
          })
        })

        describe('adjustFieldSets', () => {
          test('should change field sets', () => {
            expect(adjustFieldSets(item, f => ({...f, id: 'new-id'})).children).to.deep.eq([
              firstChild,
              {
                ...secondChild,
                id: 'new-id'
              }
            ])
          })
        })
      })

      describe('getMissingChildren', () => {
        test('should remove children of parent', () => {
          const parent = {
            id: 'parent',
            children: [{id: 'first child'}, {id: 'second child'}]
          }
          const newChild = {id: 'new child'}
          const children = [newChild, {id: 'second child'}]
          expect(getMissingChildren(parent, children)).to.deep.eq([newChild])
        })
      })

      describe('flatten', () => {
        test('should include item itself', () => {
          const item = {id: 'id'}
          expect(flatten(item)).to.deep.eq([item])
        })
        test('should flatten to any depth', () => {
          const childChild = {id: 'childChild', children: []}
          const itemChild = {id: 'itemChild', children: [childChild]}
          const item = {id: 'item', children: [itemChild]}
          expect(flatten(item)).to.deep.eq([item, itemChild, childChild])
        })
      })

      describe('selectors', () => {
        const action = {
          id: 'action',
          componentType: ACTION_TYPE
        }
        const actionOther = {
          id: 'layout',
          componentType: ACTION_TYPE
        }
        const layout = {
          id: 'layout',
          componentType: LAYOUT_TYPE
        }
        const layoutOther = {
          id: 'action',
          componentType: LAYOUT_TYPE
        }
        test('idSelector matches items by id', () => {
          expect(idSelector('action')(action)).to.be.true
          expect(idSelector('action')(layout)).to.be.false
        })
        test('actionSelector matches actions only', () => {
          expect(actionSelector(action)).to.be.true
          expect(actionSelector(layout)).to.be.false
        })
        test('all combines selectors by logical and', () => {
          const combinedSelector = all(idSelector('action'), actionSelector)
          expect(combinedSelector(action)).to.be.true
          expect(combinedSelector(actionOther)).to.be.false
          expect(combinedSelector(layout)).to.be.false
          expect(combinedSelector(layoutOther)).to.be.false
        })
        test('none inverts selectors', () => {
          const combinedSelector = none(idSelector('action'), actionSelector)
          expect(combinedSelector(layout)).to.be.true
          expect(combinedSelector(action)).to.be.false
          expect(combinedSelector(actionOther)).to.be.false
          expect(combinedSelector(layoutOther)).to.be.false
        })
      })
    })
  })
})
