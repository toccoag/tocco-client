import {remoteLogger} from 'tocco-util'

import {
  createChoiceOption,
  createMultipleChoiceField,
  createSingleChoiceField,
  createStringField,
  createTextField,
  transformPath,
  transformPathBack
} from './pseudoForm'

/*
 * The question object should have the following structure:
 * - key: string
 * - label: string
 * - mandatory: boolean
 * - questionType: string
 * - answerOptions: list of object
 *    - key (string)
 *    - label (string)
 * - relatedEntityKey: string
 */

const getPrefixedPath = key => `question_${key}`

/**
 * Map the question to the correct pseudo field depending on the question type
 * @param {object} question (see comment top level comment for structure of object)
 * @returns {object} returns created pseudo form field
 */
export const mapQuestionToPseudoField = question => {
  const path = getPrefixedPath(question.key)

  const choiceOptions = question.answerOptions.map(answerOption =>
    createChoiceOption(answerOption.key, {label: answerOption.label, checked: false})
  )

  switch (question.questionType) {
    case 'text_single_line':
      return createStringField(path, '', question.label, question.mandatory)
    case 'text_multi_line':
      return createTextField(path, '', question.label, question.mandatory)
    case 'single_choice':
      return createSingleChoiceField(path, choiceOptions, question.label, question.mandatory)
    case 'multiple_choice':
      return createMultipleChoiceField(path, choiceOptions, question.label, question.mandatory)
    default:
      remoteLogger.logError(`Question type "${question.questionType}" is not supported`)
  }
}

/**
 * Checks if a form field is a question field depending on the field name prefix
 * @param {string} fieldName redux field name
 * @returns {boolean} true if it's a question field
 */
export const isQuestionField = fieldName => fieldName.startsWith(transformPath(getPrefixedPath('')))

/**
 * Determine if a question field should be rendered depending on if the related entity is passed in `relatedEntityKeys`
 * @param {string} fieldName redux field name
 * @param {object} relatedEntityKeys list of related entities where questions should be visible
 * @param {object} questionFieldMapping pass object created with method `getQuestionFieldMapping`
 * @returns {boolean} true if question is visible
 */
export const shouldRenderQuestionField = (fieldName, relatedEntityKeys, questionFieldMapping) => {
  const visibleQuestions = Object.entries(questionFieldMapping)
    .filter(([_key, {relatedEntityKey}]) => relatedEntityKeys.includes(relatedEntityKey))
    .map(([key, _value]) => getPrefixedPath(key))

  return visibleQuestions.includes(transformPathBack(fieldName))
}

/**
 * Map the list of questions to a helper map used by other methods in this file
 * @param {object} question (see comment top level comment for structure of object)
 * @returns {object} created question map
 */
export const getQuestionFieldMapping = questions =>
  questions.reduce(
    (acc, question) => ({
      ...acc,
      [question.key]: {
        type: question.questionType,
        relatedEntityKey: question.relatedEntityKey
      }
    }),
    {}
  )

/**
 * Map the answered question to a rest bean
 * @param {object} questionFieldMapping pass object created with method `getQuestionFieldMapping`
 * @param {object} relatedEntityKeys list of related entities where questions should be visible
 * @param {object} pseudoFields pass `form.splitFlattenEntity(...).pseudoFields` as argument
 * @returns created bean used to send as rest bean to the backend
 */
export const mapQuestionsToBean = (questionFieldMapping, relatedEntityKeys, pseudoFields) =>
  Object.entries(questionFieldMapping)
    .filter(([_key, {relatedEntityKey}]) => relatedEntityKeys.includes(relatedEntityKey))
    .map(([key, {type}]) => ({
      key,
      type,
      value: mapPseudoFieldToQuestionValue(type, key, pseudoFields)
    }))

const mapPseudoFieldToQuestionValue = (type, key, pseudoFields) => {
  const rawValue = pseudoFields[getPrefixedPath(key)]
  switch (type) {
    case 'text_single_line':
    case 'text_multi_line':
      return rawValue
    case 'single_choice':
      return rawValue.options.find(o => o.checked)?.id || null
    case 'multiple_choice':
      return rawValue.options.filter(o => o.checked).map(o => o.id)
    default:
      remoteLogger.logError(`Question type "${type}" is not supported`)
  }
}
