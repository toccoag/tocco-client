import {remoteLogger} from 'tocco-util'

import {
  mapQuestionToPseudoField,
  isQuestionField,
  shouldRenderQuestionField,
  getQuestionFieldMapping,
  mapQuestionsToBean
} from './question'

describe('app-extensions', () => {
  describe('form', () => {
    describe('question', () => {
      describe('mapQuestionToPseudoField', () => {
        const createQuestionChoice = questionType => createQuestion(questionType, [{key: '2', label: 'label option'}])

        const createQuestion = (questionType, answerOptions = []) => ({
          key: '1',
          label: 'label question',
          mandatory: true,
          questionType,
          answerOptions,
          relatedEntityKey: 1
        })

        const expectedChoice = (value, type, otherProps = {}) => ({
          entityPath: {
            pseudoField_question_1: {
              type,
              value,
              writable: true
            }
          },
          formField: {
            id: 'pseudoField_question_1',
            label: 'label question',
            componentType: 'field-set',
            readonly: false,
            children: [
              {
                id: 'pseudoField_question_1',
                componentType: 'field',
                pseudoField: true,
                path: 'pseudoField_question_1',
                dataType: type,
                validation: {
                  mandatory: true
                },
                ...otherProps
              }
            ]
          }
        })

        const valueChoice = {
          options: [
            {
              id: '2',
              label: 'label option',
              checked: false
            }
          ]
        }

        test('map text_single_line', () => {
          const question = createQuestion('text_single_line')
          const expected = expectedChoice('', 'string')
          expect(mapQuestionToPseudoField(question)).to.be.eql(expected)
        })

        test('map text_multi_line', () => {
          const question = createQuestion('text_multi_line')
          const expected = expectedChoice('', 'text')
          expect(mapQuestionToPseudoField(question)).to.be.eql(expected)
        })

        test('map single_choice', () => {
          const question = createQuestionChoice('single_choice')
          const expected = expectedChoice(valueChoice, 'choice', {selectionType: 'single'})
          expect(mapQuestionToPseudoField(question)).to.be.eql(expected)
        })

        test('map multiple_choice', () => {
          const question = createQuestionChoice('multiple_choice')
          const expected = expectedChoice(valueChoice, 'choice', {selectionType: 'multiple'})
          expect(mapQuestionToPseudoField(question)).to.be.eql(expected)
        })

        test('invalid type', () => {
          const question = createQuestion('otherType')

          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          mapQuestionToPseudoField(question)

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Question type "otherType" is not supported')
        })
      })

      describe('isQuestionField', () => {
        test('is question field', () => {
          expect(isQuestionField('pseudoField_question_1')).to.be.true
        })

        test('is not question field', () => {
          expect(isQuestionField('otherFieldName')).to.be.false
        })
      })

      describe('shouldRenderQuestionField', () => {
        const fieldName = 'pseudoField_question_1'
        const questionFieldMapping = {
          1: {type: 'text_single_line', relatedEntityKey: '123'}
        }
        test('question field should be rendered', () => {
          const relatedEntityKeys = ['123']
          expect(shouldRenderQuestionField(fieldName, relatedEntityKeys, questionFieldMapping)).to.be.true
        })

        test('question field should not be rendered', () => {
          const relatedEntityKeys = ['456']
          expect(shouldRenderQuestionField(fieldName, relatedEntityKeys, questionFieldMapping)).to.be.false
        })
      })

      describe('getQuestionFieldMapping', () => {
        test('map multiple_choice', () => {
          const question = {
            key: '1',
            questionType: 'multiple_choice',
            relatedEntityKey: '123'
          }
          const expected = {
            1: {type: 'multiple_choice', relatedEntityKey: '123'}
          }
          expect(getQuestionFieldMapping([question])).to.be.eql(expected)
        })
      })

      describe('mapQuestionsToBean', () => {
        test('only map questions which are related to an entity key', () => {
          const relatedEntityKeys = ['123', '234']
          const questionFieldMapping = {
            1: {type: 'text_single_line', relatedEntityKey: '123'},
            2: {type: 'text_single_line', relatedEntityKey: '234'},
            3: {type: 'text_single_line', relatedEntityKey: '345'}
          }
          const pseudoFields = {
            question_1: 'value1',
            question_2: 'value2',
            question_3: 'value3'
          }
          const expected = [
            {
              key: '1',
              type: 'text_single_line',
              value: 'value1'
            },
            {
              key: '2',
              type: 'text_single_line',
              value: 'value2'
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map text_single_line', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'text_single_line', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: 'value'
          }
          const expected = [
            {
              key: '1',
              type: 'text_single_line',
              value: 'value'
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map text_multi_line', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'text_multi_line', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: 'value1\nvalue2'
          }
          const expected = [
            {
              key: '1',
              type: 'text_multi_line',
              value: 'value1\nvalue2'
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map single_choice nothing selected', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'single_choice', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: {
              options: [
                {
                  id: '2',
                  label: 'label option',
                  checked: false
                }
              ]
            }
          }
          const expected = [
            {
              key: '1',
              type: 'single_choice',
              value: null
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map single_choice option selected', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'single_choice', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: {
              options: [
                {
                  id: '2',
                  label: 'label option',
                  checked: true
                }
              ]
            }
          }
          const expected = [
            {
              key: '1',
              type: 'single_choice',
              value: '2'
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map multiple_choice nothing selected', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'multiple_choice', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: {
              options: [
                {
                  id: '2',
                  label: 'label option',
                  checked: false
                },
                {
                  id: '3',
                  label: 'label option',
                  checked: false
                }
              ]
            }
          }
          const expected = [
            {
              key: '1',
              type: 'multiple_choice',
              value: []
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('map multiple_choice options selected', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'multiple_choice', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: {
              options: [
                {
                  id: '2',
                  label: 'label option',
                  checked: true
                },
                {
                  id: '3',
                  label: 'label option',
                  checked: true
                }
              ]
            }
          }
          const expected = [
            {
              key: '1',
              type: 'multiple_choice',
              value: ['2', '3']
            }
          ]
          expect(mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)).to.be.eql(expected)
        })

        test('invalid type', () => {
          const relatedEntityKeys = ['123']
          const questionFieldMapping = {
            1: {type: 'otherType', relatedEntityKey: '123'}
          }
          const pseudoFields = {
            question_1: 'value'
          }

          const logErrorFn = jest.fn()
          jest.spyOn(remoteLogger, 'logError').mockImplementation(logErrorFn)

          mapQuestionsToBean(questionFieldMapping, relatedEntityKeys, pseudoFields)

          expect(logErrorFn.mock.calls.length).to.eql(1)
          expect(logErrorFn.mock.calls[0][0]).to.eql('Question type "otherType" is not supported')
        })
      })
    })
  })
})
