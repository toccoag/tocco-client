import {Field} from 'redux-form'
import {Layout, Panel, Typography} from 'tocco-ui'
import {consoleLogger} from 'tocco-util'

import fieldUtils from '../../../field'
import layoutTypes from '../../enums/layoutTypes'
import FormActions from '../../FormActions'
import {getFieldId} from '../../formDefinition'
import {transformFieldName} from '../../reduxForm'
import ReduxFormFieldAdapter from '../../ReduxFormFieldAdapter'
import {getFormFieldDefinition} from '../../utils'

import {modeFitsScope, shouldRenderField} from './utils'

export const createFieldSet = ({
  fieldSet,
  parentReadOnly,
  siblings,
  entity,
  beforeRenderField,
  formValues,
  mode,
  fieldMappingType,
  formName,
  entityModel,
  labelPosition
}) => {
  const formFieldDefinition = getFormFieldDefinition(fieldSet)
  if (!formFieldDefinition) {
    return null
  }

  const formDefinitionField = {
    ...formFieldDefinition,
    siblings: siblings.map(getFormFieldDefinition).filter(Boolean)
  }

  const fieldName = formDefinitionField.path || formDefinitionField.id

  const pathEntityFields = fieldUtils.getPathEntityFields(fieldName, entity)

  const entityFieldForRender = pathEntityFields.filter(pathEntityField => !!pathEntityField).slice(-1)[0]
  const entityField = pathEntityFields[pathEntityFields.length - 1]

  if (
    shouldRenderField({
      parentReadOnly,
      formDefinition: formFieldDefinition,
      formDefinitionField,
      entityField: entityFieldForRender,
      entity,
      beforeRenderField,
      fieldName,
      formValues,
      mode,
      fieldMappingType
    })
  ) {
    return (
      <Field
        key={`field-${fieldName}`}
        parentReadOnly={parentReadOnly}
        name={transformFieldName(fieldName)}
        id={getFieldId(formName, fieldName)}
        formName={formName}
        entityModel={entityModel}
        component={ReduxFormFieldAdapter}
        formDefinitionField={formDefinitionField}
        entityField={entityField}
        entity={entity}
        fieldMappingType={fieldMappingType}
        format={null}
        mode={mode}
        labelPosition={labelPosition}
      />
    )
  }

  return null
}

export const createAction = ({action, entity, mode, customRenderedActions}) => {
  const {model: entityName, key: entityKey} = entity

  return (
    <FormActions
      key={`action-${action.id}`}
      action={action}
      entityKey={entityKey}
      entityName={entityName}
      mode={mode}
      customRenderedActions={customRenderedActions}
    />
  )
}

export const createLayoutComponent = ({field, elements}) => {
  const content = field.label ? (
    <Panel.Wrapper isFramed={true} isOpenInitial={!field.collapsed}>
      <Panel.Header>
        <Typography.H4>{field.label}</Typography.H4>
      </Panel.Header>
      <Panel.Body>{elements}</Panel.Body>
    </Panel.Wrapper>
  ) : (
    elements
  )

  const layoutMap = {
    [layoutTypes.HORIZONTAL_BOX]: Layout.Container,
    [layoutTypes.VERTICAL_BOX]: Layout.Box
  }

  const LayoutComponent = layoutMap[field.layoutType]
  const key = `layoutcomponent-${field.id}-${field.layoutType}`

  if (LayoutComponent) {
    return (
      <LayoutComponent key={key} occupiesRemainingHeight={field.occupiesRemainingHeight}>
        {content}
      </LayoutComponent>
    )
  } else {
    consoleLogger.logWarning(`Layout type "${field.layoutType}" for box "${field.id}" is unknown.`)
    return null
  }
}

export const createCustomComponent = ({field, mode, component}) => {
  if (!modeFitsScope(mode, field.scopes)) {
    return null
  }

  const key = `custom-component-${field.id}-${field.layoutType}`

  return component(field, key)
}
