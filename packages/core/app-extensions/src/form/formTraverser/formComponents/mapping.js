import {env} from 'tocco-util'

import * as admin from './admin/formComponents'
import * as widget from './widget/formComponents'

export const mapping = {
  [env.EmbedType.admin]: admin,
  [env.EmbedType.widget]: widget
}
