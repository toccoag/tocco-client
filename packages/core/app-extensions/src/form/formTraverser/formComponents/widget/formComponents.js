import {Layout, Panel, Typography} from 'tocco-ui'
import {consoleLogger} from 'tocco-util'

import layoutTypes from '../../../enums/layoutTypes'
import {createAction, createFieldSet, createCustomComponent} from '../formComponents'

const createLayoutComponent = ({field, elements}) => {
  const content = field.label ? (
    <Panel.Wrapper isFramed={true} isOpenInitial={!field.collapsed}>
      <Panel.Header>
        <Typography.H2>{field.label}</Typography.H2>
      </Panel.Header>
      <Panel.Body>{elements}</Panel.Body>
    </Panel.Wrapper>
  ) : (
    elements
  )

  const layoutMap = {
    [layoutTypes.HORIZONTAL_BOX]: Layout.Container,
    [layoutTypes.VERTICAL_BOX]: Layout.Box
  }

  const LayoutComponent = layoutMap[field.layoutType]
  const key = `layoutcomponent-${field.id}-${field.layoutType}`

  if (LayoutComponent) {
    return (
      <LayoutComponent key={key} occupiesRemainingHeight={field.occupiesRemainingHeight}>
        {content}
      </LayoutComponent>
    )
  } else {
    consoleLogger.logWarning(`Layout type "${field.layoutType}" for box "${field.id}" is unknown.`)
    return null
  }
}

export {createAction, createCustomComponent, createFieldSet, createLayoutComponent}
