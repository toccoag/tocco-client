import _get from 'lodash/get'

import fieldUtils from '../../../field'
import {transformFieldName} from '../../reduxForm'

export const modeFitsScope = (mode, scopes) => !mode || !scopes || scopes.length === 0 || scopes.includes(mode)

export const shouldRenderField = ({
  formDefinitionField,
  formDefinition,
  entityField,
  entity,
  parentReadOnly,
  beforeRenderField,
  fieldName,
  formValues,
  mode,
  fieldMappingType
}) => {
  if (!modeFitsScope(mode, formDefinitionField.scopes)) {
    return false
  }

  if (formDefinitionField.hidden) {
    return false
  }

  if (beforeRenderField && !beforeRenderField(fieldName)) {
    return false
  }

  if (fieldMappingType === 'search') {
    return true
  }

  // hide terms component if all entity paths are readonly
  if (formDefinitionField.componentType === 'terms' && parentReadOnly) {
    return false
  }

  const transformedFieldName = transformFieldName(fieldName)
  const type = formDefinitionField.dataType || formDefinitionField.componentType
  const typeEditable = fieldUtils.editableComponentConfigs[fieldMappingType]?.[type]
  const hasEmptyValue = () => {
    if (typeEditable && typeEditable.hideIfEmptyValue && typeEditable.hasValue) {
      return typeEditable.hideIfEmptyValue() && !typeEditable.hasValue({formValues, formField: formDefinitionField})
    } else if (formDefinitionField.componentType === 'terms') {
      // conditions must have been loaded (value not null), but none was configured (key empty)
      const value = _get(formValues, transformedFieldName)
      return value && !value.termsConditionKey
    } else if (!Object.prototype.hasOwnProperty.call(formValues || {}, transformedFieldName)) {
      return true
    } else {
      const value = formValues[transformedFieldName]
      return value == null || value === '' || (Array.isArray(value) && value.length === 0)
    }
  }

  const readOnly =
    parentReadOnly ||
    formDefinition.readonly ||
    (entityField && entityField.writable === false) ||
    (mode !== 'create' &&
      !entityField &&
      formDefinitionField.componentType !== 'description' &&
      !['location', 'address'].includes(formDefinitionField.dataType)) ||
    (mode !== 'create' &&
      typeEditable &&
      typeEditable.isReadOnly &&
      typeEditable.isReadOnly({formField: formDefinitionField, entity})) ||
    formDefinitionField.readonly ||
    formDefinitionField.componentType === 'display' ||
    formDefinitionField.componentType === 'terms' // readonly only gets decided after loading condition

  return !(readOnly && hasEmptyValue())
}
