import {env} from 'tocco-util'

import {isAction} from '../../actions/actions'
import componentTypes from '../enums/componentTypes'

import {mapping} from './formComponents/mapping'

const formTraverser = data => {
  const {
    children,
    componentMapping,
    entity,
    mode,
    customRenderedActions,
    beforeRenderField,
    formValues,
    fieldMappingType,
    formName,
    entityModel,
    labelPosition,
    parentReadOnly = false
  } = data

  const formComponents = env.embedStrategy({mapping})

  const result = []
  for (const child of children) {
    if (child.componentType === componentTypes.LAYOUT) {
      let elements = formTraverser({
        ...data,
        children: child.children,
        parentReadOnly: parentReadOnly || child.readonly
      })

      if (Array.isArray(elements)) {
        elements = elements.filter(Boolean)
      }

      if ((!Array.isArray(elements) && elements) || (Array.isArray(elements) && elements.length > 0)) {
        result.push(formComponents.createLayoutComponent({field: child, elements}))
      }
    } else if (isAction(child.componentType)) {
      result.push(formComponents.createAction({action: child, entity, mode, customRenderedActions}))
    } else if (child.componentType === componentTypes.FIELD_SET) {
      result.push(
        formComponents.createFieldSet({
          fieldSet: child,
          parentReadOnly,
          siblings: children,
          entity,
          beforeRenderField,
          formValues,
          mode,
          fieldMappingType,
          formName,
          entityModel,
          labelPosition
        })
      )
    } else if (componentMapping && componentMapping[child.componentType]) {
      result.push(
        formComponents.createCustomComponent({
          field: child,
          mode,
          component: componentMapping[child.componentType]
        })
      )
    }
  }
  return result
}

export default formTraverser
