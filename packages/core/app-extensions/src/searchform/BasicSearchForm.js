import _debounce from 'lodash/debounce'
import PropTypes from 'prop-types'
import {useState} from 'react'
import {Ball} from 'tocco-ui'
import {tqlBuilder, env, api} from 'tocco-util'

import form from '../form'

import {Box, StyledExtendSearchButtonWrapper} from './StyledComponents'

/**
 * a component that renders a given FormApp (usually SimpleFormApp) as a search form
 * and builds TQL from the values, with no actual search being triggered
 */
const BasicSearchForm = ({intl, searchFormDefinition, setSearchFields, FormApp}) => {
  const isAdmin = env.isInAdminEmbedded()
  const msg = id => intl.formatMessage({id})

  const [showExtendedSearchForm, setShowExtendedSearchForm] = useState(isAdmin)

  const toggleExtendedSearchForm = () => {
    setShowExtendedSearchForm(!showExtendedSearchForm)
  }

  if (!searchFormDefinition.children) {
    return null
  }

  const fields = form.getFieldDefinitions(searchFormDefinition)
  const simpleSearchFields = fields.filter(field => field.simpleSearch === true).map(field => field.path || field.id)
  const extendable = !isAdmin && !fields.every(field => simpleSearchFields.includes(field.id))

  const shouldRenderField = name => showExtendedSearchForm || simpleSearchFields.includes(name)

  return (
    <Box>
      {extendable && (
        <StyledExtendSearchButtonWrapper>
          <Ball
            data-cy="btn-extend-search"
            icon={`chevron-${showExtendedSearchForm ? 'up' : 'down'}`}
            onClick={toggleExtendedSearchForm}
            title={msg('client.common.extendedSearch')}
          />
        </StyledExtendSearchButtonWrapper>
      )}
      <FormApp
        form={searchFormDefinition}
        onChange={_debounce(handleChange(searchFormDefinition, setSearchFields), 500)}
        noButtons
        validate={false}
        mappingType="search"
        mode="search"
        beforeRenderField={shouldRenderField}
        labelPosition="inside"
      />
    </Box>
  )
}

const handleChange =
  (searchFormDefinition, setSearchFields) =>
  ({values}) => {
    if (Object.keys(values).length > 0) {
      const tql = transformFormValuesToTql(values, searchFormDefinition)
      setSearchFields(tql)
    } else {
      setSearchFields([])
    }
  }

const transformFormValuesToTql = (values, searchFormDefinition) =>
  Object.entries(values)
    .filter(([key]) => !Object.values(api.metaFields).includes(key))
    .map(([path, value]) => ({
      path,
      fieldType: getFieldType(path, searchFormDefinition),
      value
    }))
    .filter(({value}) => value && (!Array.isArray(value) || value.length > 0))
    .map(({path, fieldType, value}) => tqlBuilder.getTql(path, value, fieldType))
    .filter(tql => tql.length > 0)

const getFieldType = (path, searchFormDefinition) => {
  const container = searchFormDefinition.children.find(child => child.children.length > 0)
  if (container) {
    const field = container.children.find(child => child.id === path)

    return field.children.length > 0 ? field.children[0].dataType : field.dataType
  }
}

BasicSearchForm.propTypes = {
  intl: PropTypes.object.isRequired,
  searchFormDefinition: PropTypes.object.isRequired,
  setSearchFields: PropTypes.func.isRequired,
  FormApp: PropTypes.func.isRequired
}

export default BasicSearchForm
