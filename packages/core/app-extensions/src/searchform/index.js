import BasicSearchForm from './BasicSearchForm'
import {StyledExtendSearchButtonWrapper} from './StyledComponents'

export default {
  BasicSearchForm,
  StyledExtendSearchButtonWrapper
}
