import {useEffect, useState} from 'react'
import {Provider} from 'react-redux'
import {Button} from 'tocco-ui'

import appFactory from '../appFactory'
import notification from '../notification'

import errorLogging from '.'
import errorLoggingDocs from './errorLoggingDocs.mdx'

const ErrorLogStory = () => {
  const [store, setStore] = useState(null)

  useEffect(() => {
    const s = appFactory.createStore({}, undefined, {}, 'errorLogging')
    errorLogging.addToStore(s, true, ['console', 'remote', 'notification'])
    notification.addToStore(s, true)
    setStore(s)
  }, [])

  const logError = () => {
    store.dispatch(errorLogging.logError('client.errorTitle', 'client.errorDescription', new Error('Some Error')))
  }

  if (!store) {
    return null
  }

  return (
    <Provider store={store}>
      <div>
        <notification.Notifications />
        <Button label="Log Error" look="raised" onClick={logError} />
      </div>
    </Provider>
  )
}

export default {
  title: 'App-Extensions/Error Logging',
  component: ErrorLogStory,
  parameters: {
    docs: {
      page: errorLoggingDocs
    }
  }
}

export const Basic = ErrorLogStory.bind({})
