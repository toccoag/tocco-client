import {action} from '@storybook/addon-actions'
import {useEffect, useState} from 'react'
import {Provider} from 'react-redux'
import {Button, Typography} from 'tocco-ui'

import appFactory from '../appFactory'

import notification from '.'
import notificationDocs from './notificationDocs.mdx'

export default {
  title: 'app-extensions/Notification',
  component: Provider,
  parameters: {
    docs: {
      page: notificationDocs
    }
  }
}

const NotificationStory = () => {
  const [store, setStore] = useState()

  useEffect(() => {
    const s = appFactory.createStore({}, undefined, {}, 'notifier')
    notification.addToStore(s, true)
    setStore(s)
  }, [])

  if (!store) {
    return null
  }

  const longText = `Lorem ipsum dolor sit amet, at sed inermis intellegam scriptorem, usu facete apeirian ad. 
  Sit et meliore intellegam. Mel cu maluisset philosophia, pri et habeo oportere. 
  Vis in purto verear luptatum, has ne graecis qualisque. Mei ei placerat incorrupte adversarium, eum rebum nonumy ut.`

  const info = () => {
    store.dispatch(
      notification.toaster({
        type: 'info',
        title: 'client.title',
        body: 'message line contains <b>html</b>'
      })
    )
  }

  const success = () => {
    store.dispatch(
      notification.toaster({
        type: 'success',
        title: 'client.title',
        body: 'client.message'
      })
    )
  }

  const warning = () => {
    store.dispatch(
      notification.toaster({
        type: 'warning',
        title: 'client.title',
        body: longText
      })
    )
  }

  const error = () => {
    store.dispatch(
      notification.toaster({
        type: 'error',
        title: 'client.title',
        body: 'client.description'
      })
    )
  }

  const neutral = () => {
    store.dispatch(
      notification.toaster({
        type: 'neutral',
        title: 'client.title',
        body: 'client.message'
      })
    )
  }

  const confirmQuestion = () => {
    store.dispatch(
      notification.confirm(
        'Title',
        'message line contains <b>html</b>',
        'OK text',
        'Cancel text',
        action('Ok'),
        action('Cancel')
      )
    )
  }

  const yesNoQuestion = () => {
    store.dispatch(
      notification.yesNoQuestion(
        'title',
        'message',
        'Yes text',
        'No text',
        'Cancel text',
        action('Yes'),
        action('No'),
        action('Cancel')
      )
    )
  }

  const blockingInfo = () => {
    const id = Date.now()
    store.dispatch(notification.blockingInfo(id, 'Title', 'Please wait 5 seconds '))
    setTimeout(() => {
      store.dispatch(notification.removeBlockingInfo(id))
    }, 5000)
  }

  const smallModal = () => {
    const id = Date.now()
    store.dispatch(
      notification.modal(
        id,
        'Title',
        'Message',
        () => (
          <>
            <Button look="raised" label="Open blocking info on top" onClick={blockingInfo} />
          </>
        ),
        true
      )
    )
  }

  const modalComponent = () => {
    const id = Date.now()
    store.dispatch(
      notification.modal(
        id,
        'Title',
        'Message',
        props => (
          <>
            <Typography.P>Custom component starts here</Typography.P>
            <Button look="raised" ink="primary" label="Primary action" onClick={action('Primary action')} />
            <Button
              look="raised"
              label="Secondary action (and close)"
              onClick={() => {
                action('Secondary action')()
                // eslint-disable-next-line react/prop-types
                props.close()
              }}
            />
            <Button look="raised" label="Open another modal on top" onClick={smallModal} />
            <Button look="raised" label="Open blocking info on top" onClick={blockingInfo} />
          </>
        ),
        true
      )
    )
  }

  return (
    <Provider store={store}>
      <div>
        <notification.Notifications />
        <div style={{paddingBottom: '20px'}}>
          <Typography.P>Different toaster types</Typography.P>
          <Button label="Info" look="raised" onClick={info} />
          &nbsp;
          <Button label="Success" look="raised" onClick={success} />
          &nbsp;
          <Button label="Warning" look="raised" onClick={warning} />
          &nbsp;
          <Button label="Error" look="raised" onClick={error} />
          &nbsp;
          <Button label="Neutral" look="raised" onClick={neutral} />
          &nbsp;
        </div>
        <div>
          <Typography.P>Other</Typography.P>
          <Button label="Confirm" look="raised" onClick={confirmQuestion} />
          &nbsp;
          <Button label="Yes-No Question" look="raised" onClick={yesNoQuestion} />
          &nbsp;
          <Button label="Blocking Info" look="raised" onClick={blockingInfo} />
          &nbsp;
          <Button label="Modal Component" look="raised" onClick={modalComponent} />
        </div>
      </div>
    </Provider>
  )
}

const OverlayStory = () => {
  const [store, setStore] = useState()

  useEffect(() => {
    const s = appFactory.createStore({}, undefined, {}, 'notifier')
    notification.addToStore(s, true)
    setStore(s)
  }, [])

  const blockingInfo = (id, message) => {
    store.dispatch(notification.blockingInfo(id, id, message))
  }

  const modal = (id, cancelCallback) => {
    store.dispatch(notification.modal(id, id, id, () => null, true, cancelCallback))
  }

  if (!store) {
    return null
  }

  return (
    <Provider store={store}>
      <div>
        <notification.Notifications />
        <div style={{paddingBottom: '20px'}}>
          <Button
            label="Create stack of overlays"
            look="raised"
            onClick={() => {
              blockingInfo('first blocker', 'Closes 1 second after closing modal')
              modal('first modal', () =>
                setTimeout(() => {
                  store.dispatch(notification.removeBlockingInfo('first blocker'))
                }, 1000)
              )
              blockingInfo('second blocker', 'Closes 2 seconds after closing modal')
              modal('second modal', () =>
                setTimeout(() => {
                  store.dispatch(notification.removeBlockingInfo('second blocker'))
                }, 2000)
              )
              blockingInfo('auto blocker', 'Closes in 3 seconds')
              setTimeout(() => {
                store.dispatch(notification.removeBlockingInfo('auto blocker'))
              }, 3000)
            }}
          />
        </div>
      </div>
    </Provider>
  )
}

export const Basic = NotificationStory
export const OverlayStack = OverlayStory
