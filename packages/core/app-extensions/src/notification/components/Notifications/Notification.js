import PropTypes from 'prop-types'

import OverlayDisplay from '../../modules/overlay'
import ToasterDisplay from '../../modules/toaster/ToasterDisplay'

const Notification = ({navigationStrategy}) => (
  <>
    <ToasterDisplay navigationStrategy={navigationStrategy} />
    <OverlayDisplay />
  </>
)

Notification.propTypes = {
  navigationStrategy: PropTypes.object
}

export default Notification
