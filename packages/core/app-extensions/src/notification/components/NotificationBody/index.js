import NotificationBody from './NotificationBody'
import {StyledProgressOuter, StyledProgressInner} from './StyledComponents'

export default NotificationBody
export {StyledProgressOuter, StyledProgressInner}
