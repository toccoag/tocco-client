import {notificationPropType} from '../../types'
import Content from '../Content'

import {StyledMessage} from './StyledComponents'

const Message = ({notification}) => {
  const {message} = notification
  if (!message || message.trim().length === 0) {
    return null
  }
  return (
    <StyledMessage>
      <Content>{message}</Content>
    </StyledMessage>
  )
}

Message.propTypes = {
  notification: notificationPropType.isRequired
}

export default Message
