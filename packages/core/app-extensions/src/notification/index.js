import {transformNotificationResult} from './api'
import Notifications from './components/Notifications'
import {blockingInfo, removeBlockingInfo} from './modules/blocking/actions'
import NotificationCenter from './modules/center/NotificationCenter'
import {yesNoQuestion, confirm, info} from './modules/interactive/actions'
import {modal, removeModal} from './modules/modal/actions'
import {connectSocket, closeSocket} from './modules/socket/actions'
import {toaster, removeToaster} from './modules/toaster/actions'
import toasterSources from './modules/toaster/util'
import {addToStore} from './notification'

export default {
  transformNotificationResult,
  addToStore,
  Notifications,
  NotificationCenter,
  confirm,
  yesNoQuestion,
  info,
  blockingInfo,
  removeBlockingInfo,
  modal,
  removeModal,
  toaster,
  removeToaster,
  connectSocket,
  closeSocket,
  toasterSources
}
