import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useOnEnterHandler} from 'tocco-ui'
import {v4 as uuid} from 'uuid'

import {modal} from '../modal/actions'
import ModalButtons from '../modal/ModalDisplay/ModalButtons'

export function getConfirmationAction({title, message, okText, cancelText, onOk, onCancel, defaultAction = 'submit'}) {
  const id = uuid()

  const Content = ({close}) => {
    const handleKeyDown = event => {
      if (event.key === 'Enter') {
        event.preventDefault()
        if (defaultAction === 'submit') {
          onOk()
        } else if (defaultAction === 'cancel') {
          onCancel()
        }
        close()
      } else if (event.key === 'Escape') {
        onCancel()
        close()
      }
    }

    // only on mount
    useEffect(() => {
      document.addEventListener('keydown', handleKeyDown)

      return () => {
        document.removeEventListener('keydown', handleKeyDown)
      }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    const buttons = [
      {
        label: okText,
        primary: defaultAction === 'submit',
        look: defaultAction === 'submit' ? 'raised' : 'flat',
        'data-cy': 'btn-confirm-ok',
        callback: () => {
          onOk()
          close()
        }
      },
      {
        label: cancelText,
        primary: defaultAction === 'cancel',
        look: defaultAction === 'cancel' ? 'raised' : 'flat',
        'data-cy': 'btn-confirm-cancel',
        callback: () => {
          onCancel()
          close()
        }
      }
    ]

    return <ModalButtons buttons={buttons} />
  }

  Content.propTypes = {close: PropTypes.func.isRequired}

  return modal(id, title, message, Content, true, onCancel)
}

export function getYesNoAction({title, message, yesText, noText, onYes, onNo, onCancel}) {
  const id = uuid()

  const Content = ({close}) => {
    const cancelCallbackFunctions = () => {
      onNo()
      close()
    }
    useOnEnterHandler(cancelCallbackFunctions)

    const buttons = [
      {
        label: yesText,
        callback: () => {
          onYes()
          close()
        }
      },
      {
        label: noText,
        primary: true,
        look: 'raised',
        callback: cancelCallbackFunctions
      }
    ]

    Content.propTypes = {close: PropTypes.func.isRequired}

    return <ModalButtons buttons={buttons} />
  }

  return modal(id, title, message, Content, true, onCancel)
}

export function getInfoAction({title, message, okText, onOk}) {
  const id = uuid()

  const Content = ({close}) => {
    const handleKeyDown = event => {
      if (event.key === 'Enter') {
        event.preventDefault()
        onOk()
        close()
      }
    }

    // only on mount
    useEffect(() => {
      document.addEventListener('keydown', handleKeyDown)

      return () => {
        document.removeEventListener('keydown', handleKeyDown)
      }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps

    const buttons = [
      {
        label: okText,
        primary: true,
        look: 'raised',
        'data-cy': 'btn-confirm-ok',
        callback: () => {
          onOk()
          close()
        }
      }
    ]

    return <ModalButtons buttons={buttons} />
  }

  Content.propTypes = {close: PropTypes.func.isRequired}

  return modal(id, title, message, Content, false)
}
