import * as actions from './actions'
import {enhanceToaster} from './toaster'
import toasterSources from './util'

const shouldOverwriteToaster = toaster => toaster.source !== toasterSources.SYNC_ACTION

const addToaster = (state, {payload: {toaster}}) => {
  const enhancedToaster = enhanceToaster(toaster)
  const oldToaster = state.toasters[enhancedToaster.key]
  if (!oldToaster || shouldOverwriteToaster(oldToaster)) {
    return {
      ...state,
      toasters: {
        ...state.toasters,
        [enhancedToaster.key]: enhancedToaster
      }
    }
  }
  return state
}

const removeToaster = (state, {payload: {key}}) => {
  const {[key]: remove, ...keep} = state.toasters

  return {
    ...state,
    toasters: {
      ...keep
    }
  }
}

const ACTION_HANDLERS = {
  [actions.TOASTER]: addToaster,
  [actions.REMOVE_TOASTER_FROM_STORE]: removeToaster
}

const initialState = {
  toasters: {}
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
