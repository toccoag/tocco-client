import {env} from 'tocco-util'

import AdminToaster from './admin/Toaster'
import WidgetToaster from './widget/Toaster'

export const mapping = {
  [env.EmbedType.admin]: AdminToaster,
  [env.EmbedType.widget]: WidgetToaster
}
