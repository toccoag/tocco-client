import PropTypes from 'prop-types'

import {ToasterPropType} from '../../toaster'

export const toasterPropTypes = {
  toaster: ToasterPropType,
  closeToaster: PropTypes.func.isRequired,
  cancelTask: PropTypes.func.isRequired,
  navigationStrategy: PropTypes.object
}
