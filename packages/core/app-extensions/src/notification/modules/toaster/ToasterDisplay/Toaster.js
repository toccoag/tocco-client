import {env} from 'tocco-util'

import {mapping} from './toasters/mapping'

const Toaster = env.embedStrategyFactory({mapping})

export default Toaster
