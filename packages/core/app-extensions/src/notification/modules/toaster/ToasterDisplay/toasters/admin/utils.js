import {css} from 'styled-components'
import {themeSelector} from 'tocco-ui'

const colorMap = {
  info: 'info',
  success: 'success',
  error: 'danger',
  warning: 'warning'
}

const colorMapper = (type, theme) => (colorMap[type] ? theme.colors.signal[colorMap[type]] : '#ccc')

export const toasterStyles = () => css`
  background-color: ${({type, theme}) => colorMapper(type, theme)} !important;

  *,
  a:link // reset legacy styling
  {
    color: ${themeSelector.color('textToaster')} !important;
  }
`

export const closeButtonStyles = () => css`
  &:hover {
    opacity: 1;
    color: ${themeSelector.color('textToaster')};
  }
`
