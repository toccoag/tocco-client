import {toasterPropTypes} from '../propTypes'
import Toaster from '../Toaster'

import {toasterStyles, iconTitleWrapperStyles} from './utils'

const WidgetToaster = ({...props}) => (
  <Toaster
    styles={{
      toasterStyles,
      iconTitleWrapperStyles
    }}
    {...props}
  />
)

WidgetToaster.propTypes = toasterPropTypes

export default WidgetToaster
