import {css} from 'styled-components'
import {themeSelector} from 'tocco-ui'

const colorMap = {
  info: 'info',
  success: 'success',
  error: 'danger',
  warning: 'warning'
}

const colorMapper = (type, theme) => (colorMap[type] ? theme.colors.signal[colorMap[type]] : '#ccc')

export const toasterStyles = () => css`
  background-color: ${themeSelector.color('paper')} !important;
  border-radius: ${themeSelector.radii('statusLabel')};
  border: 1px solid ${themeSelector.color('border')};

  *,
  a:link // reset legacy styling
  {
    color: ${themeSelector.color('text')} !important;
  }
`

export const iconTitleWrapperStyles = () => css`
  * {
    color: ${({type, theme}) => colorMapper(type, theme)} !important;
  }
`

export const closeButtonStyles = () => css`
  &:hover {
    opacity: 1;
    color: ${themeSelector.color('text')};
  }
`
