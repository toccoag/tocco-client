import styled, {keyframes} from 'styled-components'
import {Ball, declareFont, scale, StyledH1, themeSelector} from 'tocco-ui'

const fadeIn = keyframes`
  0% {
    opacity: 0;
  }

  100% {
    opacity: 1;
  }
`

export const StyledToaster = styled.div`
  ${({styles}) => styles}
  margin-bottom: ${scale.space(-0.5)};
  padding: ${scale.space(0)};
  box-shadow: 0 7px 12px -3px rgba(0 0 0 / 70%);
  animation-name: ${fadeIn};
  animation-duration: 0.8s;
  pointer-events: auto;
  max-height: 90dvh;
  overflow-y: auto;

  @media only screen and (max-width: 500px) {
    width: 90%;
    margin-left: auto;
    margin-right: auto;
  }
`

export const StyledCloseButton = styled(Ball)`
  padding: 0;
  font-size: ${scale.font(3)};
  color: ${themeSelector.color('backgroundBody')};
  float: right;
  opacity: 0.9;

  &,
  &&:hover {
    background: transparent;
    cursor: pointer;
  }
  ${({styles}) => styles}
`

export const StyledIconTitleWrapper = styled.div`
  display: flex;
  gap: ${scale.space(0)};
  padding-right: ${scale.space(0)};

  ${StyledH1} {
    margin-top: 0;
  }
  ${({styles}) => styles}
`

export const StyledTitleWrapper = styled.span`
  font-size: ${scale.font(1.5)};
  font-weight: ${themeSelector.fontWeight('regular')};
  position: relative;
  top: 1px;
  bottom: 2px;
`

export const StyledIconWrapper = styled.div`
  font-size: ${scale.font(7)};
`

export const StyledContentWrapper = styled.div`
  padding-top: ${scale.space(-1)};
  color: ${themeSelector.color('textToaster')};
`

export const StyledToasterBox = styled.div`
  display: flex;
  flex-direction: column-reverse;
  justify-content: flex-start;
  pointer-events: none;
  ${declareFont()};
  position: fixed;
  height: calc(100% - (40px + 55px)); /* subtract height of header plus some additional padding */
  min-width: 310px;
  max-width: 400px;
  top: 40px; /* height of Header to prevent coverage */
  right: 55px;

  /* lower than StyledModalHolder and very high value 
  to prevent other elements blocking it when implemented as a widget */
  z-index: 99999;

  @media only screen and (max-width: 500px) {
    width: 100%;
    height: calc(100% - 50px); /* subtract height of header */
    right: 0;
    margin: auto;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }
`
