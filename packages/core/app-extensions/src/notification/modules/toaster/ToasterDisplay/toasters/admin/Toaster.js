import {toasterPropTypes} from '../propTypes'
import Toaster from '../Toaster'

import {toasterStyles, closeButtonStyles} from './utils'

const AdminToaster = ({...props}) => (
  <Toaster
    styles={{
      toasterStyles,
      closeButtonStyles
    }}
    {...props}
  />
)

AdminToaster.propTypes = toasterPropTypes

export default AdminToaster
