import styled, {css} from 'styled-components'
import {themeSelector, scale, declareFont} from 'tocco-ui'

import {StyledProgressOuter, StyledProgressInner} from '../../../components/NotificationBody'

const typeColorMap = {
  warning: themeSelector.color('signal.warning'),
  error: themeSelector.color('signal.danger'),
  success: themeSelector.color('signal.success'),
  info: themeSelector.color('signal.info')
}

export const StyledNotification = styled.article`
  margin-bottom: ${scale.space(0)};
  padding: ${scale.space(-1)};
  ${({read}) =>
    !read &&
    css`
      background-color: ${themeSelector.color('backgroundBody')};
    `}
`

export const StyledNotificationHeader = styled.header`
  display: flex;
  align-items: center;
  margin-bottom: ${scale.space(-1)};

  * {
    margin-top: 0 !important;
    color: ${({notificationType}) => typeColorMap[notificationType]} !important;
  }
`

export const StyledIconWrapper = styled.span`
  display: inline-block;
  margin-right: ${scale.space(-1.5)};
  font-size: ${scale.font(5)};
`

export const StyledTitleWrapper = styled.span`
  * {
    font-weight: ${themeSelector.fontWeight('regular')} !important;
  }
`

export const StyledNotificationTitleWrapper = styled.div`
  ${declareFont({
    fontSize: scale.font(2.9),
    fontWeight: themeSelector.fontWeight('bold'),
    color: themeSelector.color('secondary')
  })}
  margin-bottom: ${scale.space(-0.5)};
`

export const StyledTimeStamp = styled.div`
  font-size: ${scale.font(-2)};
`

export const StyledNotificationCenter = styled.div`
  max-height: calc(100dvh - 100px);
  overflow-y: auto;
  width: inherit;
  padding: ${scale.space(-0.5)};
  background-color: ${themeSelector.color('paper')};
  border: 1px solid ${themeSelector.color('secondaryLight')};
  border-radius: ${themeSelector.radii('menu')};

  /* within the notification center the progress bars need another color */
  ${StyledProgressOuter} {
    background-color: transparent;
    border: 0.5px solid ${themeSelector.color('text')};
  }

  ${StyledProgressInner} {
    background-color: ${themeSelector.color('textLighter')};
  }
  ${declareFont()}
`
