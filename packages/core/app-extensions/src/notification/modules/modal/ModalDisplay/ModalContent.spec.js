import {fireEvent, screen} from '@testing-library/react'
import {Link, MemoryRouter, Route, Routes} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import ModalContent from './ModalContent'

describe('app-extensions', () => {
  describe('notification', () => {
    describe('modules', () => {
      describe('modal', () => {
        describe('ModalDisplay', () => {
          describe('ModalContent', () => {
            test('should render component with close function', () => {
              const closeSpy = sinon.spy()
              const id = Date.now()
              const component = () => (
                <div data-testid="component">
                  TEST<button onClick={() => closeSpy()}>close</button>
                </div>
              )
              testingLibrary.renderWithIntl(
                <MemoryRouter>
                  <ModalContent modal={{id, component}} onClose={closeSpy} />
                </MemoryRouter>
              )
              expect(screen.queryAllByTestId('component')).to.have.length(1)
              fireEvent.click(screen.getByRole('button'))
              expect(closeSpy).to.have.been.calledOnce
            })

            test('should invoke cancel callback on close button', () => {
              const closeSpy = sinon.spy()
              const cancelSpy = sinon.spy()
              const id = Date.now()
              const component = () => <div data-testid="component">TEST</div>
              testingLibrary.renderWithIntl(
                <MemoryRouter>
                  <ModalContent modal={{id, component, cancelable: true}} onClose={closeSpy} onCancel={cancelSpy} />
                </MemoryRouter>
              )
              expect(screen.queryAllByTestId('component')).to.have.length(1)
              fireEvent.click(screen.getByRole('button'))
              expect(cancelSpy).to.have.been.calledOnce
            })

            test('should invoke close on location change', () => {
              const closeSpy = sinon.spy()
              const id = Date.now()
              const component = () => <div data-testid="component">TEST</div>
              testingLibrary.renderWithIntl(
                <MemoryRouter initialEntries={['/b']}>
                  <ModalContent modal={{id, component}} onClose={closeSpy} />
                  <Routes>
                    <Route path="a" element={<div data-testid="route-a"></div>} />
                    <Route
                      path="b"
                      element={
                        <div data-testid="route-b">
                          <Link to="/a" data-testid="link">
                            Prev
                          </Link>
                        </div>
                      }
                    />
                  </Routes>
                </MemoryRouter>
              )
              expect(screen.queryAllByTestId('route-b')).to.have.length(1)
              expect(screen.queryAllByTestId('link')).to.have.length(1)
              expect(screen.queryAllByTestId('component')).to.have.length(1)

              fireEvent.click(screen.getByTestId('link'))

              expect(closeSpy).to.have.been.calledOnce
              expect(screen.queryAllByTestId('route-a')).to.have.length(1)
            })
          })
        })
      })
    })
  })
})
