import PropTypes from 'prop-types'
import {design} from 'tocco-ui'

import {StyledModalButton, StyledModalButtonWrapper} from './StyledComponents'

const ModalButtons = ({buttons}) => (
  <StyledModalButtonWrapper>
    {buttons.map(({primary, callback, label, ...buttonProps}, i) => (
      <StyledModalButton {...(primary ? {ink: design.ink.PRIMARY} : {})} key={i} onClick={callback} {...buttonProps}>
        {label}
      </StyledModalButton>
    ))}
  </StyledModalButtonWrapper>
)

ModalButtons.propTypes = {
  buttons: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
      primary: PropTypes.bool,
      callback: PropTypes.func.isRequired
    })
  )
}

export default ModalButtons
