/* stylelint-disable no-descending-specificity */
import styled, {createGlobalStyle} from 'styled-components'
import {Button, scale, StyledTether, themeSelector, StyledLoadMask, StyledLayoutContainer} from 'tocco-ui'

export const basePadding = scale.space(0.5)

export const StyledModalContent = styled.div`
  position: relative;
  background-color: ${themeSelector.color('paper')};
  border-radius: ${themeSelector.radii('modal')};
  padding: ${basePadding};
  display: grid;
  grid-template-rows: [header] auto [body] 1fr;

  /* within the modal the load mask height must be set to auto to prevent cut off when sticky buttons are present */
  ${StyledLoadMask} {
    height: auto;
  }

  ${StyledLayoutContainer} {
    padding-right: 0;
  }

  @media (min-width: 501px) {
    top: 10dvh;
    box-shadow: 2px 2px 10px rgba(0 0 0 / 40%);
    max-height: 80%;
    min-width: 350px;
    max-width: 700px;
    margin: auto;
  }

  @media (max-width: 500px) {
    box-sizing: border-box;
    height: 100dvh;
    width: 100dvw;
  }
`

export const StyledCloseButton = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  font-size: 22px;
  opacity: 0.8;
  outline: none;
  position: relative;
  padding: 0;
  z-index: 1;
  margin-left: auto;
  align-self: flex-start;
  color: ${themeSelector.color('text')};

  &:hover {
    opacity: 1;
  }

  &:focus {
    outline: none;
  }
`

export const StyledModalWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;

  /* only latest modal should be above mask */
  z-index: ${props => (props.isLatest ? 1 : -1)};
`

export const StyledModalHeader = styled.div`
  grid-row-start: header;
  display: flex;
`

export const StyledTitleWrapper = styled.div`
  padding-bottom: ${scale.space(0.5)};
`

export const StyledModalBody = styled.div`
  grid-row-start: body;
  overflow: hidden auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const StyledMessageWrapper = styled.div`
  padding-bottom: ${scale.space(0)};
`

export const StyledModalButton = styled(Button)`
  margin-top: ${scale.space(0)};

  &:last-child {
    margin-right: 0;
  }
`

export const StyledModalButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`

export const GlobalStyles = createGlobalStyle`
  ${StyledTether} {
    && {
      z-index: 9999999; /* higher than StyledModalHolder */
    }
  }

  html,
  body {
    /* prevent scroll of background when modal is open */
    overflow: hidden !important;

    /* fix scroll to top on WP when scroll-behaviour is set to smooth */
    scroll-behavior: auto !important;
  }
`
