import PropTypes from 'prop-types'

import overlayTypes from '../../overlay/overlayTypes'

export default PropTypes.shape({
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  component: PropTypes.func,
  cancelable: PropTypes.bool,
  cancelCallback: PropTypes.func,
  type: PropTypes.oneOf(Object.values(overlayTypes))
})
