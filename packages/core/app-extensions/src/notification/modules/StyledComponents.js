import styled from 'styled-components'

export const StyledPageOverlay = styled.div`
  background-color: rgba(50 50 50 / 70%);
  height: 100%;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
`

export const StyledHolder = styled.div`
  height: 100%;
  width: 100%;
  top: 0;
  left: 0;
  position: absolute;

  /* higher than StyledHeader and very high value to prevent other elements blocking it when implemented as a widget
  lower than ext-js legacy actions */
  z-index: 9000;
`
