import PropTypes from 'prop-types'
import {Typography} from 'tocco-ui'
import {env} from 'tocco-util'

import Content from '../../../components/Content'

import {mapping} from './mapping'
import blockerPropType from './propType'
import {StyledBlockingDisplay, StyledTitleWrapper} from './StyledComponents'

const BlockerContent = ({blocker: {title, body}, isLatest}) => {
  const styles = env.embedStrategy({mapping})
  return (
    <StyledBlockingDisplay styles={styles.blockingDiplayStyles} isLatest={isLatest}>
      {title && (
        <Typography.H1>
          <StyledTitleWrapper styles={styles.titleWrapperStyles}>
            <Content>{title}</Content>
          </StyledTitleWrapper>
        </Typography.H1>
      )}
      {body && (
        <Typography.Span>
          <Content>{body}</Content>
        </Typography.Span>
      )}
    </StyledBlockingDisplay>
  )
}

BlockerContent.propTypes = {
  blocker: blockerPropType.isRequired,
  isLatest: PropTypes.bool
}

export default BlockerContent
