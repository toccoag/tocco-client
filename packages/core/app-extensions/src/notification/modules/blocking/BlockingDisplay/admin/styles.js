import {css} from 'styled-components'
import {themeSelector} from 'tocco-ui'

export const blockingDiplayStyles = css`
  background-color: ${themeSelector.color('signal.info')};

  * {
    color: ${themeSelector.color('paper')};
  }
`

export const titleWrapperStyles = css`
  color: ${themeSelector.color('paper')};
`

export default {
  blockingDiplayStyles,
  titleWrapperStyles
}
