import {env} from 'tocco-util'

import AdminBlockerStyles from './admin/styles'
import WidgetBlockerStyles from './widget/styles'

export const mapping = {
  [env.EmbedType.admin]: AdminBlockerStyles,
  [env.EmbedType.widget]: WidgetBlockerStyles
}
