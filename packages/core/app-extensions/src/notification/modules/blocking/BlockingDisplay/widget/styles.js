import {css} from 'styled-components'
import {themeSelector} from 'tocco-ui'

const blockingDiplayStyles = css`
  background-color: ${themeSelector.color('paper')};
  border: 1px solid ${themeSelector.color('border')};
  border-radius: ${themeSelector.radii('toaster')};

  * {
    color: ${themeSelector.color('text')};
  }
`

const titleWrapperStyles = css`
  color: ${themeSelector.color('signal.info')};
`

export default {
  blockingDiplayStyles,
  titleWrapperStyles
}
