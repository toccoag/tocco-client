import styled from 'styled-components'
import {scale, themeSelector} from 'tocco-ui'

export const StyledBlockingDisplay = styled.div`
  /* only latest modal should be above mask */
  z-index: ${props => (props.isLatest ? 1 : -1)};
  position: fixed;
  top: 10px;
  left: 50%;
  transform: translateX(-155px);
  width: 310px;
  padding: ${scale.space(-0.5)};
  ${({styles}) => styles}
`

export const StyledTitleWrapper = styled.span`
  font-size: ${scale.font(1.5)};
  font-weight: ${themeSelector.fontWeight('regular')};
  ${({styles}) => styles}
`
