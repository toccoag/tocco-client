import PropTypes from 'prop-types'

import overlayTypes from '../../overlay/overlayTypes'

export default PropTypes.shape({
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  body: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  type: PropTypes.oneOf(Object.values(overlayTypes))
})
