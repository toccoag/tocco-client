import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import BlockerContent from './BlockerContent'

describe('app-extensions', () => {
  describe('notification', () => {
    describe('modules', () => {
      describe('blocking', () => {
        describe('BlockingDisplay', () => {
          describe('BlockingContent', () => {
            test('should render title', () => {
              testingLibrary.renderWithIntl(<BlockerContent blocker={{title: 'blocker title'}} />)
              expect(screen.queryByText('blocker title')).to.exist
            })

            test('should render body', () => {
              testingLibrary.renderWithIntl(<BlockerContent blocker={{body: 'blocker body'}} />)
              expect(screen.queryByText('blocker body')).to.exist
            })
          })
        })
      })
    })
  })
})
