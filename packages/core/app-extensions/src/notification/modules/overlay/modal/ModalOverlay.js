import PropTypes from 'prop-types'

import ModalContent from '../../modal/ModalDisplay/ModalContent'
import modalPropType from '../../modal/ModalDisplay/propType'

const ModalOverlay = ({modal, isLatest, closeModal}) => (
  <ModalContent
    modal={modal}
    onClose={closeModal}
    onCancel={id => {
      closeModal(id)
      if (modal.cancelable && modal.cancelCallback) {
        modal.cancelCallback()
      }
    }}
    isLatest={isLatest}
  />
)

ModalOverlay.propTypes = {
  modal: modalPropType.isRequired,
  isLatest: PropTypes.bool,
  closeModal: PropTypes.func.isRequired
}

export default ModalOverlay
