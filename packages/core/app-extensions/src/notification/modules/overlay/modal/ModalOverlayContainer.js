import {connect} from 'react-redux'

import * as modalActions from '../../modal/actions'

import ModalOverlay from './ModalOverlay'

const mapActionCreators = {
  closeModal: modalActions.removeModal
}

const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapActionCreators)(ModalOverlay)
