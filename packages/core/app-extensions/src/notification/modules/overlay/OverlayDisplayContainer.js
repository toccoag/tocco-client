import {connect} from 'react-redux'

import * as modalActions from '../modal/actions'

import OverlayDisplay from './OverlayDisplay'

const mapActionCreators = {
  closeModal: modalActions.removeModal
}

const mapStateToProps = state => ({
  overlays: state.notification.overlay.overlays
})

export default connect(mapStateToProps, mapActionCreators)(OverlayDisplay)
