import PropTypes from 'prop-types'

import BlockerContent from '../../blocking/BlockingDisplay/BlockerContent'
import blockerPropType from '../../blocking/BlockingDisplay/propType'

const BlockerOverlay = ({blocker, isLatest}) => <BlockerContent blocker={blocker} isLatest={isLatest} />

BlockerOverlay.propTypes = {
  blocker: blockerPropType.isRequired,
  isLatest: PropTypes.bool
}

export default BlockerOverlay
