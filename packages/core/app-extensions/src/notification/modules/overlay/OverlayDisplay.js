import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import {GlobalAppClassName} from 'tocco-ui'

import blockerPropType from '../blocking/BlockingDisplay/propType'
import modalPropType from '../modal/ModalDisplay/propType'
import {StyledPageOverlay, StyledHolder} from '../StyledComponents'

import BlockerOverlay from './blocker/BlockerOverlay'
import ModalOverlay from './modal/ModalOverlayContainer'
import overlayTypes from './overlayTypes'

const createOverlayComponent = (overlay, idx, isLatest) => {
  if (overlay.type === overlayTypes.MODAL) {
    return <ModalOverlay key={idx} idx={idx} modal={overlay} isLatest={isLatest} />
  } else if (overlay.type === overlayTypes.BLOCKER) {
    return <BlockerOverlay key={idx} idx={idx} blocker={overlay} isLatest={isLatest} />
  } else {
    return null
  }
}

const OverlayDisplay = ({overlays}) =>
  overlays.length > 0 &&
  ReactDOM.createPortal(
    <StyledHolder className={GlobalAppClassName}>
      {overlays.map((overlay, idx) => createOverlayComponent(overlay, idx, idx + 1 === overlays.length))}
      <StyledPageOverlay />
    </StyledHolder>,
    document.body
  )

OverlayDisplay.propTypes = {
  overlays: PropTypes.arrayOf(PropTypes.oneOfType([modalPropType, blockerPropType])).isRequired
}

export default OverlayDisplay
