import {v4 as uuid} from 'uuid'

import * as blockingActions from '../blocking/actions'
import * as modalActions from '../modal/actions'

import overlayTypes from './overlayTypes'
import reducer from './reducer'

const INITIAL_STATE = {
  overlays: []
}

const runActions = actions => actions.reduce((state, action) => reducer(state, action), INITIAL_STATE)

describe('app-extensions', () => {
  describe('notification', () => {
    describe('modules', () => {
      describe('overlay', () => {
        describe('reducer', () => {
          test('should create a valid initial state', () => {
            expect(reducer(undefined, {})).to.deep.equal(INITIAL_STATE)
          })

          test('should add modals', () => {
            const id = uuid()
            const title = 'title'
            const message = 'message'
            const component = () => <div>TEST</div>
            const cancelCallback = () => {}

            const state = runActions([
              modalActions.modal(id, title, message, component),
              modalActions.modal(id, title, message, component, true, cancelCallback)
            ])

            expect(state.overlays).to.have.length(2)
            expect(state.overlays[0].cancelable).to.be.true

            expect(state.overlays[1].cancelable).to.be.true
            expect(state.overlays[1].cancelCallback).to.be.equal(cancelCallback)
            expect(state.overlays.every(({type}) => type === overlayTypes.MODAL)).to.be.true
          })

          test('should remove modals by id', () => {
            const title = 'title'
            const message = 'message'
            const component = () => <div>TEST</div>

            const state = runActions([
              modalActions.modal(1, title, message, component),
              modalActions.modal(2, title, message, component),
              modalActions.modal(2, title, message, component),
              modalActions.removeModal(2)
            ])

            expect(state.overlays).to.have.length(1)
            expect(state.overlays[0].id).to.eql(1)
          })

          test('should add a blocker', () => {
            const id = uuid()
            const title = 'title'
            const body = <div>TEST</div>

            const state = runActions([
              blockingActions.blockingInfo(id, title, body),
              blockingActions.blockingInfo(uuid(), title, body)
            ])

            expect(state.overlays).to.have.length(2)
            expect(state.overlays.every(({type}) => type === overlayTypes.BLOCKER)).to.be.true
          })

          test('should remove a blocker by id', () => {
            const state = runActions([
              blockingActions.blockingInfo(1),
              blockingActions.blockingInfo(2),
              blockingActions.blockingInfo(2),
              blockingActions.removeBlockingInfo(2)
            ])

            expect(state.overlays).to.have.length(1)
            expect(state.overlays[0].id).to.eql(1)
          })

          test('should handle mixed overlay types', () => {
            const state = runActions([
              modalActions.modal(1),
              modalActions.modal(2),
              blockingActions.blockingInfo(1),
              modalActions.removeModal(2),
              blockingActions.blockingInfo(2),
              blockingActions.removeBlockingInfo(1)
            ])

            expect(state.overlays).to.have.length(2)
            expect(state.overlays[0].id).to.eql(1)
            expect(state.overlays[0].type).to.eql(overlayTypes.MODAL)
            expect(state.overlays[1].id).to.eql(2)
            expect(state.overlays[1].type).to.eql(overlayTypes.BLOCKER)
          })
        })
      })
    })
  })
})
