import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {MemoryRouter} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import OverlayDisplay from './OverlayDisplayContainer'
import overlayTypes from './overlayTypes'

describe('app-extensions', () => {
  describe('notification', () => {
    describe('modules', () => {
      describe('overlay', () => {
        describe('OverlayDisplay', () => {
          test('should render all overlays', () => {
            const store = configureStore({
              reducer: () => ({
                notification: {
                  overlay: {
                    overlays: [
                      {
                        id: '1',
                        title: 'modal title',
                        message: 'modal message',
                        component: () => 'modal body',
                        type: overlayTypes.MODAL
                      },
                      {id: '2', title: 'blocker title', body: 'blocker body', type: overlayTypes.BLOCKER},
                      {id: '3', title: 'other title', type: 'other'}
                    ]
                  }
                }
              })
            })
            testingLibrary.renderWithStore(
              <MemoryRouter>
                <OverlayDisplay />
              </MemoryRouter>,
              {store}
            )
            expect(screen.queryByText('modal title')).to.exist
            expect(screen.queryByText('modal message')).to.exist
            expect(screen.queryByText('modal body')).to.exist
            expect(screen.queryByText('blocker title')).to.exist
            expect(screen.queryByText('blocker body')).to.exist
            expect(screen.queryByText('other title')).to.not.exist
          })
        })
      })
    })
  })
})
