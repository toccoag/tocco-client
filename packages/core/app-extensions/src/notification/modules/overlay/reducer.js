import * as blockingActions from '../blocking/actions'
import * as modalActions from '../modal/actions'

import overlayTypes from './overlayTypes'

const initialState = {
  overlays: []
}

const addModal = (state, {payload: {id, title, message, component, cancelable, cancelCallback}}) => {
  return {
    ...state,
    overlays: [...state.overlays, {id, title, message, component, cancelable, cancelCallback, type: overlayTypes.MODAL}]
  }
}

const addBlocker = (state, {payload: {id, title, body}}) => ({
  ...state,
  overlays: [...state.overlays, {id, title, body, type: overlayTypes.BLOCKER}]
})

const removeOverlay =
  type =>
  (state, {payload: {id}}) => ({
    ...state,
    overlays: state.overlays.filter(e => e.type !== type || e.id !== id)
  })

const ACTION_HANDLERS = {
  [modalActions.MODAL]: addModal,
  [modalActions.REMOVE_MODAL]: removeOverlay(overlayTypes.MODAL),
  [blockingActions.BLOCKING_INFO]: addBlocker,
  [blockingActions.REMOVE_BLOCKING_INFO]: removeOverlay(overlayTypes.BLOCKER)
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
