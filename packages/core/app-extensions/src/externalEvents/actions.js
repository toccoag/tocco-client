export const FIRE_EXTERNAL_EVENT = 'externalEvents/FIRE_EXTERNAL_EVENT'
export const FIRE_VISIBILITY_STATUS_CHANGE_EVENT = 'externalEvents/FIRE_VISIBILITY_STATUS_CHANGE_EVENT'

export const fireExternalEvent = (name, payload) => ({
  type: FIRE_EXTERNAL_EVENT,
  payload: {
    name,
    payload
  }
})

export const fireVisibilityStatusChangeEvent = status => ({
  type: FIRE_VISIBILITY_STATUS_CHANGE_EVENT,
  payload: {
    status
  }
})
