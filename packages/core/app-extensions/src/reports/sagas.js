import {all, call, put, takeLatest} from 'redux-saga/effects'

import rest from '../rest'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_REPORTS, loadReport)])
}

export function* loadReport({payload: {reportIds, entityName, scope}}) {
  if (reportIds.length === 0) {
    yield put(actions.setReports([]))
    return
  }

  const response = yield call(rest.requestSaga, 'client/reports', {
    method: 'POST',
    body: {
      reportIds,
      entityName,
      scope
    }
  })

  yield put(actions.setReports(response.body.reports))
}
