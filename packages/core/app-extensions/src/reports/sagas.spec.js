import {all, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('app-extensinos', () => {
  describe('reports', () => {
    describe('sagas', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(all([takeLatest(actions.LOAD_REPORTS, sagas.loadReport)]))
          expect(generator.next().done).to.be.true
        })
      })

      describe('loadReport', () => {
        test('empty array do nothing', () => {
          return expectSaga(sagas.loadReport, {payload: {reportIds: []}})
            .put(actions.setReports([]))
            .run()
        })

        test('load reports and save them', () => {
          const reports = [
            {
              actionType: 'report',
              componentType: 'report',
              id: 'id',
              label: 'label_de',
              reportId: 'id',
              showConfirmation: false,
              icon: 'icon'
            }
          ]

          return expectSaga(sagas.loadReport, {payload: {reportIds: ['id']}})
            .provide([[matchers.call.fn(rest.requestSaga), {body: {reports}}]])
            .put(actions.setReports(reports))
            .run()
        })
      })
    })
  })
})
