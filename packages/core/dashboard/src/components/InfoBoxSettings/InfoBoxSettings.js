import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {Button, Typography, useOnEnterHandler, StyledStickyButtonWrapper} from 'tocco-ui'

import {StyledCheckbox, StyledUl} from './StyledComponents'

const InfoBoxSettings = ({initialInfoBoxes, onOk}) => {
  const [infoBoxes, setInfoBoxes] = useState(initialInfoBoxes || [])

  const handleOk = () => onOk(infoBoxes)
  useOnEnterHandler(handleOk)

  const toggle = id => {
    const checkBoxes = boxes =>
      boxes.reduce((acc, box) => [...acc, {...box, folded: box.id === id ? !box.folded : box.folded}], [])
    setInfoBoxes(checkBoxes)
  }
  const InfoBoxesList = infoBoxes.map(box => (
    <Typography.Li key={box.id}>
      <StyledCheckbox type="checkbox" checked={!box.folded} onChange={() => toggle(box.id)} id={box.id} />
      <Typography.Label for={box.id}>{box.label}</Typography.Label>
    </Typography.Li>
  ))

  return (
    <>
      <StyledUl>{InfoBoxesList}</StyledUl>
      <StyledStickyButtonWrapper>
        <Button type="button" onClick={handleOk} ink="primary" look="raised">
          <FormattedMessage id="client.common.ok" />
        </Button>
      </StyledStickyButtonWrapper>
    </>
  )
}

InfoBoxSettings.propTypes = {
  initialInfoBoxes: PropTypes.array,
  onOk: PropTypes.func
}

export default InfoBoxSettings
