import {configureStore} from '@reduxjs/toolkit'
import {fireEvent, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Dashboard from './Dashboard'

const fireDragOver = (element, clientY) => {
  // mock drag event and set `clientY` explicitly
  // https://github.com/testing-library/dom-testing-library/issues/1070
  // https://github.com/testing-library/dom-testing-library/issues/1070
  class DragEventMock extends Event {
    clientY = clientY
  }
  window.DragEvent = DragEventMock
  fireEvent.dragOver(element, {})
}

describe('dashboard', () => {
  describe('components', () => {
    describe('Dashboard', () => {
      describe('Dashboard', () => {
        test('should render boxes in defined order', async () => {
          const store = configureStore({reducer: () => ({})})
          const content = {type: 'htmlfield', text: '<p>Hi</p>'}
          const infoBoxes = [
            {
              id: '1',
              position: '1:0',
              col: 0,
              row: 0,
              height: 100,
              label: 'Box1',
              content
            },
            {
              id: '2',
              position: '1:0',
              col: 0,
              row: 0,
              height: 100,
              label: 'Box2',
              content
            },
            {
              id: '3',
              position: '1:0',
              col: 0,
              row: 0,
              height: 100,
              label: 'Box3',
              content
            },
            {
              id: '5',
              position: '1:1',
              col: 0,
              row: 1,
              height: 100,
              label: 'Box5',
              content
            },
            {
              id: '4',
              position: '1:2',
              col: 0,
              row: 2,
              height: 100,
              label: 'Box4',
              content
            }
          ]

          testingLibrary.renderWithStore(
            <Dashboard infoBoxes={infoBoxes} saveInfoBoxHeight={() => {}} saveInfoBoxPositions={() => {}} />,
            {store}
          )
          await screen.findByTestId('icon-ellipsis-v')

          const boxes = screen.queryAllByText(textContent => textContent.startsWith('Box'))
          expect(boxes.map(b => b.textContent)).to.deep.equal(['Box1', 'Box2', 'Box3', 'Box5', 'Box4'])
        })

        test('should drag and drop over another infobox', async () => {
          const store = configureStore({reducer: () => ({})})
          const baseInfoBox = {
            height: 100,
            content: {type: 'htmlfield', text: '<p>Hi</p>'}
          }
          const infoBoxes = [
            {
              id: '1',
              position: '1:0',
              col: 0,
              row: 0,
              label: 'Box1',
              ...baseInfoBox
            },
            {
              id: '2',
              position: '1:1',
              col: 0,
              row: 1,
              label: 'Box2',
              ...baseInfoBox
            }
          ]

          const expectedInfoBoxes = [
            {
              id: '2',
              position: '1:1',
              col: 0,
              row: 0,
              label: 'Box2',
              ...baseInfoBox
            },
            {
              id: '1',
              position: '1:0',
              col: 0,
              row: 1,
              label: 'Box1',
              ...baseInfoBox
            }
          ]

          const saveInfoBoxPositions = sinon.spy()

          testingLibrary.renderWithStore(
            <Dashboard
              infoBoxes={infoBoxes}
              saveInfoBoxHeight={() => {}}
              saveInfoBoxPositions={saveInfoBoxPositions}
            />,
            {store}
          )
          await screen.findByTestId('icon-ellipsis-v')

          const infoBox1 = screen.getByText('Box1')
          const infoBox2 = screen.getByText('Box2')
          fireEvent.dragStart(infoBox1)
          fireEvent.dragEnter(infoBox2, {target: {getBoundingClientRect: () => ({y: 10, height: 100})}})
          fireDragOver(infoBox2, 80)
          fireEvent.drop(infoBox2)
          fireEvent.dragEnd(infoBox2)

          expect(saveInfoBoxPositions).to.have.been.calledWith(expectedInfoBoxes)
        })

        test('should drag and drop over another column', async () => {
          const store = configureStore({reducer: () => ({})})
          const baseInfoBox = {
            height: 100,
            content: {type: 'htmlfield', text: '<p>Hi</p>'}
          }
          const infoBoxes = [
            {
              id: '1',
              position: '1:0',
              col: 0,
              row: 0,
              label: 'Box1',
              ...baseInfoBox
            },
            {
              id: '2',
              position: '1:1',
              col: 0,
              row: 1,
              label: 'Box2',
              ...baseInfoBox
            }
          ]

          const expectedInfoBoxes = [
            {
              id: '2',
              position: '1:1',
              col: 0,
              row: 0,
              label: 'Box2',
              ...baseInfoBox
            },
            {
              id: '1',
              position: '1:0',
              col: 1,
              row: 0,
              label: 'Box1',
              ...baseInfoBox
            }
          ]

          const saveInfoBoxPositions = sinon.spy()

          const {container} = testingLibrary.renderWithStore(
            <Dashboard
              infoBoxes={infoBoxes}
              saveInfoBoxHeight={() => {}}
              saveInfoBoxPositions={saveInfoBoxPositions}
            />,
            {store}
          )
          await screen.findByTestId('icon-ellipsis-v')

          const infoBox1 = screen.getByText('Box1')
          const secondColumn = container.querySelectorAll('[class^="StyledColumn-"]')[1]

          fireEvent.dragStart(infoBox1)

          fireEvent.dragEnter(secondColumn)
          fireDragOver(secondColumn, 0)
          fireEvent.drop(secondColumn)
          fireEvent.dragEnd(secondColumn)

          expect(saveInfoBoxPositions).to.have.been.calledWith(expectedInfoBoxes)
        })

        test('should change infobox height', async () => {
          const store = configureStore({reducer: () => ({})})
          const baseInfoBox = {
            height: 100,
            label: 'Box1',
            content: {type: 'htmlfield', text: '<p>Hi</p>'}
          }
          const infoBoxes = [
            {
              id: '1',
              position: '1:0',
              col: 0,
              row: 0,
              ...baseInfoBox
            }
          ]

          const saveInfoBoxHeight = sinon.spy()

          const {container} = testingLibrary.renderWithStore(
            <Dashboard infoBoxes={infoBoxes} saveInfoBoxHeight={saveInfoBoxHeight} saveInfoBoxPositions={() => {}} />,
            {store}
          )

          await screen.findByTestId('icon-ellipsis-v')

          const resizeHandle = container.querySelector('[class^="StyledResizeHandle-"]')
          fireEvent.mouseDown(resizeHandle)
          fireEvent.mouseMove(resizeHandle, {clientX: 100, clientY: 100})
          fireEvent.mouseMove(resizeHandle, {clientX: 200, clientY: 200})
          fireEvent.mouseUp(resizeHandle)

          expect(saveInfoBoxHeight).to.have.been.calledWith('1', 100)
        })
      })
    })
  })
})
