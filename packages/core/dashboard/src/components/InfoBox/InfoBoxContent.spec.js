import {configureStore} from '@reduxjs/toolkit'
import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import InfoBoxContent from './InfoBoxContent'

jest.mock('tocco-entity-list/src/main', () => () => <div data-testid="entity-list" />)

describe('dashboard', () => {
  describe('components', () => {
    describe('InfoBox', () => {
      describe('InfoBoxContent', () => {
        test('should render a htlmfield content', () => {
          const content = {
            type: 'htmlfield',
            text: 'Inhalt'
          }
          testingLibrary.renderWithIntl(<InfoBoxContent content={content} id="info-box" />)
          expect(screen.queryByText('Inhalt')).to.exist
        })

        test('should render a searchfilter content', () => {
          const content = {
            type: 'searchfilter'
          }

          const store = configureStore({reducer: () => ({input: {}})})
          testingLibrary.renderWithStore(<InfoBoxContent content={content} id="info-box" />, {store})
          expect(screen.queryByTestId('entity-list')).to.exist
        })

        test('should render nothing for unknown type', () => {
          const content = {
            type: 'anything'
          }
          const {container} = testingLibrary.renderWithIntl(<InfoBoxContent content={content} id="info-box" />)
          jestExpect(container.firstChild).toBeEmptyDOMElement()
        })

        test('should render nothing for missing content', () => {
          const {container} = testingLibrary.renderWithIntl(<InfoBoxContent content={null} id="info-box" />)
          jestExpect(container).toBeEmptyDOMElement()
        })
      })
    })
  })
})
