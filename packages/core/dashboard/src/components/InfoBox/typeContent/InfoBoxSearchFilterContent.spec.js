import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import InfoBoxSearchFilterContent from './InfoBoxSearchFilterContent'

/* eslint-disable react/prop-types, react/no-unknown-property */
jest.mock('tocco-entity-list/src/main', () => ({sorting, ...props}) => (
  <div data-testid="entity-list" {...props} sorting={JSON.stringify(sorting)} />
))
/* eslint-enable react/prop-types */

describe('dashboard', () => {
  describe('components', () => {
    describe('InfoBox', () => {
      describe('typeContent', () => {
        describe('InfoBoxSearchFilterContent', () => {
          test('should render entity-list content', () => {
            const content = {
              searchFilterUniqueId: 'birthdays',
              entityName: 'User',
              form: 'User_infobox',
              limit: 25,
              orderBy: 'field'
            }
            testingLibrary.renderWithIntl(<InfoBoxSearchFilterContent id="infobox" content={content} />)

            const entityList = screen.getByTestId('entity-list')
            jestExpect(entityList).toHaveAttribute('searchFilters', content.searchFilterUniqueId)
            jestExpect(entityList).toHaveAttribute('entityName', content.entityName)
            jestExpect(entityList).toHaveAttribute('formName', 'User')
            jestExpect(entityList).toHaveAttribute('scope', 'infobox')
            jestExpect(entityList).toHaveAttribute('limit', `${content.limit}`)
          })

          test('should handle desc order', () => {
            const content = {
              orderBy: 'field desc'
            }

            testingLibrary.renderWithIntl(<InfoBoxSearchFilterContent id="infobox" content={content} />)

            const entityList = screen.getByTestId('entity-list')
            jestExpect(entityList).toHaveAttribute('sorting', JSON.stringify([{field: 'field', order: 'desc'}]))
          })

          test('should handle multiple sortings', () => {
            const content = {
              orderBy: 'field desc, other asc'
            }

            testingLibrary.renderWithIntl(<InfoBoxSearchFilterContent id="infobox" content={content} />)

            const entityList = screen.getByTestId('entity-list')
            jestExpect(entityList).toHaveAttribute(
              'sorting',
              JSON.stringify([
                {field: 'field', order: 'desc'},
                {field: 'other', order: 'asc'}
              ])
            )
          })
        })
      })
    })
  })
})
