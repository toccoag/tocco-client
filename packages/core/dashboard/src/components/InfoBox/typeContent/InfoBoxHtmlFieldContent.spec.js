import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import InfoBoxHtmlFieldContent from './InfoBoxHtmlFieldContent'

describe('dashboard', () => {
  describe('components', () => {
    describe('InfoBox', () => {
      describe('typeContent', () => {
        describe('InfoBoxHtmlFieldContent', () => {
          test('should render html content', () => {
            const content = {
              text: '<p data-testid="test">Hallo</p>'
            }
            testingLibrary.renderWithIntl(<InfoBoxHtmlFieldContent content={content} />)

            expect(screen.queryAllByTestId('test')).to.have.length(1)
            expect(screen.queryByText('Hallo')).to.exist
          })

          test('should sanitize html before rendering it', () => {
            const content = {
              text: '<p data-testid="test" id="test" onclick="alert(\'Hi!\')">Hallo</p>'
            }
            testingLibrary.renderWithIntl(<InfoBoxHtmlFieldContent content={content} />)

            expect(screen.queryAllByTestId('test')).to.have.length(1)
            jestExpect(screen.getByTestId('test')).toHaveAttribute('id', 'test')
            jestExpect(screen.getByTestId('test')).not.toHaveAttribute('onclick')
          })
        })
      })
    })
  })
})
