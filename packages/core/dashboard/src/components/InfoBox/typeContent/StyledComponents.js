import styled from 'styled-components'

export const StyledInfoBoxContentWrapper = styled.div`
  flex: 1;
  ${({scrollable}) => scrollable && 'overflow: auto;'}
`
