import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {BallMenu, MenuItem} from 'tocco-ui'

import {StyledMenuWrapper} from './StyledComponents'

const Menu = ({displayInfoBoxSettings, resetInfoBoxSettings}) => (
  <StyledMenuWrapper>
    <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-infobox-settings'}}>
      <MenuItem onClick={displayInfoBoxSettings} data-cy="menuitem-edit-home">
        <FormattedMessage id="client.dashboard.preferences.show" />
      </MenuItem>
      <MenuItem onClick={resetInfoBoxSettings} data-cy="menuitem-reset-home">
        <FormattedMessage id="client.dashboard.preferences.reset" />
      </MenuItem>
    </BallMenu>
  </StyledMenuWrapper>
)

Menu.propTypes = {
  displayInfoBoxSettings: PropTypes.func.isRequired,
  resetInfoBoxSettings: PropTypes.func.isRequired
}

export default Menu
