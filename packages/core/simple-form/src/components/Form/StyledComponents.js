import styled, {css} from 'styled-components'
import {StyledButton, theme, scale, StyledLayoutBox} from 'tocco-ui'

export const StyledStickyButtons = styled.div`
  position: absolute;
  bottom: 0;
  width: calc(100% - 2 * ${scale.space(0.4)});
  padding: ${scale.space(0.3)} 0;
  background-color: ${theme.color('paper')};
  z-index: 1;
  display: flex;
  justify-content: flex-end;

  ${/* sc-selector */ StyledButton}:last-child {
    margin-right: 0;
  }
`

export const StyledForm = styled.form`
  /* if buttons are present adequate bottom padding must be added since the button have absolute positioning  */
  ${({hasButtons}) =>
    hasButtons &&
    css`
      padding-bottom: ${scale.space(0.8)};

      /* for some reason :last-child and :last-of-type is not working here */
      > ${StyledLayoutBox}:nth-last-of-type(2):not(:first-child) {
        margin-bottom: 0;

        > ${StyledLayoutBox}:last-child {
          margin-bottom: 0;
        }
      }
    `}
`
