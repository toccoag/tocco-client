import PropTypes from 'prop-types'
import {useRef} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button, LoadMask, useOnEnterHandler} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import {StyledForm, StyledStickyButtons} from './StyledComponents'

const REDUX_FORM_NAME = 'simpleForm'

const UseOnEnterConditionally = ({onEnterCallback}) => {
  useOnEnterHandler(onEnterCallback)
  return <></>
}

UseOnEnterConditionally.propTypes = {
  onEnterCallback: PropTypes.func.isRequired
}

const Form = ({
  intl,
  onSubmit,
  handleSubmit: handleSubmitProp,
  form: formName,
  formDefinition,
  formValues,
  mappingType,
  mode,
  entityModel,
  noButtons,
  submitting,
  submitText,
  beforeRenderField,
  valid,
  initialized,
  labelPosition
}) => {
  const msg = id => intl.formatMessage({id})
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
  // delay closing of the window so it won't close immediately with the click
  const handleSubmitDelayed = () => sleep(400).then(() => onSubmit())

  const searchFormEl = useRef(null)
  customHooks.useAutofocus(searchFormEl, {selectFulltextFields: true}, [formDefinition])

  const actualHandleSubmit = handleSubmitProp(handleSubmitDelayed)

  return (
    <LoadMask required={[initialized]}>
      <StyledForm onSubmit={actualHandleSubmit} ref={searchFormEl} hasButtons={!noButtons}>
        <form.FormBuilder
          entity={undefined}
          formName={formName}
          formDefinition={formDefinition}
          formValues={formValues}
          fieldMappingType={mappingType || 'editable'}
          mode={mode}
          entityModel={entityModel}
          beforeRenderField={beforeRenderField}
          labelPosition={labelPosition}
        />
        {!noButtons && (
          <>
            <UseOnEnterConditionally onEnterCallback={actualHandleSubmit} />
            <StyledStickyButtons>
              <Button
                disabled={submitting || !valid}
                ink="primary"
                look="raised"
                label={submitText || msg('client.simple-form.defaultOk')}
                pending={submitting}
                type="submit"
                data-cy="btn-simpleForm-submit"
              />
            </StyledStickyButtons>
          </>
        )}
      </StyledForm>
    </LoadMask>
  )
}

Form.propTypes = {
  intl: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  formDefinition: PropTypes.object.isRequired,
  entityModel: PropTypes.object,
  valid: PropTypes.bool,
  submitting: PropTypes.bool.isRequired,
  initialized: PropTypes.bool.isRequired,
  cancelText: PropTypes.string,
  submitText: PropTypes.string,
  noButtons: PropTypes.bool,
  formValues: PropTypes.object,
  form: PropTypes.string,
  mappingType: PropTypes.oneOf(['editable', 'search', 'readonly']),
  mode: PropTypes.oneOf(['list', 'detail', 'create', 'update', 'search']),
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  beforeRenderField: PropTypes.func
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(Form)
