import {reducer as form} from 'redux-form'

import formReducer, {sagas as formSagas, formSagaConfig} from './simpleForm'

export default {
  simpleForm: formReducer,
  form
}

export const sagas = [formSagas]
export {formSagaConfig}
