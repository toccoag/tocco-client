import {actions as formActions, getFormValues, isValid} from 'redux-form'
import {CHANGE, INITIALIZE, UPDATE_SYNC_ERRORS} from 'redux-form/es/actionTypes'
import {all, call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {externalEvents, form as formUtil, rest} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import * as actions from './actions'

const FORM_ID = 'simpleForm'
export const inputSelector = state => state.input
export const selector = state => state.simpleForm

export const formStateSelector = state => ({
  ...inputSelector(state),
  ...selector(state)
})

export const formSagaConfig = {
  formId: FORM_ID,
  stateSelector: formStateSelector
}

export default function* sagas() {
  yield all([
    takeEvery(actions.INITIALIZE_QUESTION_FORM, initialize),
    takeEvery(actions.SUBMIT, submit),
    takeLatest(CHANGE, change),
    takeLatest(UPDATE_SYNC_ERRORS, change),
    takeLatest(INITIALIZE, change)
  ])
}

export function* loadEntityModel(entityName) {
  const entityModel = yield call(rest.fetchModel, entityName)
  yield put(actions.setEntityModel(entityModel))
  return entityModel
}

export function* initialize() {
  const {form, entityName, defaultValues: inputDefaultValues} = yield select(inputSelector)
  const fieldDefinitions = yield call(formUtil.getFieldDefinitions, form)
  const formDefaultValues = yield call(formUtil.getDefaultValues, fieldDefinitions)

  const initialValues = Object.entries({
    ...formDefaultValues,
    ...inputDefaultValues,
    [api.metaFields.MODEL]: form.modelName
  })
    .map(([key, value]) => [formUtil.transformFieldName(key), value])
    .reduce((acc, [key, value]) => ({...acc, [key]: value}), {})

  if (entityName) {
    yield call(loadEntityModel, entityName)
  }

  yield put(formActions.initialize(FORM_ID, initialValues))
}

export function* submit() {
  const values = yield call(getValues)
  yield put(externalEvents.fireExternalEvent('onSubmit', {values}))
}

const getChangeEventType = ({type}) => {
  if (type === INITIALIZE) {
    return 'init'
  } else if (type === UPDATE_SYNC_ERRORS) {
    return 'error'
  }
  return 'change'
}

// helper function to allow easy mocking in test
export function* loadIsValid() {
  return yield select(isValid(FORM_ID))
}

export function* change(action) {
  const values = yield call(getValues)
  const valid = yield call(loadIsValid)
  yield put(
    externalEvents.fireExternalEvent('onChange', {
      values,
      valid,
      event: getChangeEventType(action)
    })
  )
}

// helper function to allow easy mocking in test
export function* loadFormValues() {
  return yield select(getFormValues(FORM_ID))
}

export function* getValues() {
  const isMetaField = path => Object.values(api.metaFields).includes(path)
  const values = yield call(loadFormValues)
  if (!values) {
    return {}
  }

  return Object.entries(values)
    .filter(([key]) => !isMetaField(key))
    .reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key.replace('--', '.')]: value
      }),
      {}
    )
}
