import {actions as formActions} from 'redux-form'
import {CHANGE, INITIALIZE, UPDATE_SYNC_ERRORS} from 'redux-form/es/actionTypes'
import {all, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {externalEvents, form as formUtil, rest} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('simple-form', () => {
  describe('modules', () => {
    describe('simpleForm', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeEvery(actions.INITIALIZE_QUESTION_FORM, sagas.initialize),
                takeEvery(actions.SUBMIT, sagas.submit),
                takeLatest(CHANGE, sagas.change),
                takeLatest(UPDATE_SYNC_ERRORS, sagas.change),
                takeLatest(INITIALIZE, sagas.change)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadEntityModel', () => {
          test('should fetch model', () => {
            const expectedModel = {model: 'User'}
            return expectSaga(sagas.loadEntityModel, 'User')
              .provide([[matchers.call(rest.fetchModel, 'User'), expectedModel]])
              .put(actions.setEntityModel(expectedModel))
              .returns(expectedModel)
              .run()
          })
        })

        describe('initialize', () => {
          test('should init form and combines default values', () => {
            const form = {modelName: 'User'}
            const entityName = 'User'
            const defaultValues = {firstname: 'Ursula'}

            const fieldDefinitions = {}
            const formDefaultValues = {lastname: 'Meier'}
            return expectSaga(sagas.initialize)
              .provide([
                [matchers.select(sagas.inputSelector), {form, entityName, defaultValues}],
                [matchers.call(formUtil.getFieldDefinitions, form), fieldDefinitions],
                [matchers.call(formUtil.getDefaultValues, fieldDefinitions), formDefaultValues],
                [matchers.call(sagas.loadEntityModel, entityName)]
              ])
              .put(formActions.initialize('simpleForm', {__model: 'User', firstname: 'Ursula', lastname: 'Meier'}))
              .run()
          })

          test('should fetch entityModel', () => {
            const form = {modelName: 'User'}
            const entityName = 'User'
            const defaultValues = {}

            const fieldDefinitions = {}
            const formDefaultValues = {}
            return expectSaga(sagas.initialize)
              .provide([
                [matchers.select(sagas.inputSelector), {form, entityName, defaultValues}],
                [matchers.call(formUtil.getFieldDefinitions, form), fieldDefinitions],
                [matchers.call(formUtil.getDefaultValues, fieldDefinitions), formDefaultValues],
                [matchers.call(sagas.loadEntityModel, entityName)]
              ])
              .call(sagas.loadEntityModel, 'User')
              .put(formActions.initialize('simpleForm', {__model: 'User'}))
              .run()
          })
        })

        describe('loadEntityModel', () => {
          test('should fetch entityModel', () => {
            const entityModel = {model: 'User'}
            return expectSaga(sagas.loadEntityModel, 'User')
              .provide([[matchers.call(rest.fetchModel, 'User'), entityModel]])
              .put(actions.setEntityModel(entityModel))
              .run()
          })
        })

        describe('submit', () => {
          test('should fire external event with values', () => {
            const values = {firstname: 'Ursula'}
            return expectSaga(sagas.submit)
              .provide([[matchers.call(sagas.getValues), values]])
              .put(externalEvents.fireExternalEvent('onSubmit', {values}))
              .run()
          })
        })

        describe('change', () => {
          test('should fire external event with values on change', () => {
            const values = {firstname: 'Ursula'}
            return expectSaga(sagas.change, {type: CHANGE})
              .provide([
                [matchers.call(sagas.loadIsValid), true],
                [matchers.call(sagas.getValues), values]
              ])
              .put(externalEvents.fireExternalEvent('onChange', {values, valid: true, event: 'change'}))
              .run()
          })

          test('should fire external event with invalid indicator on error', () => {
            const values = {firstname: 'Ursula'}
            return expectSaga(sagas.change, {type: UPDATE_SYNC_ERRORS})
              .provide([
                [matchers.call(sagas.loadIsValid), false],
                [matchers.call(sagas.getValues), values]
              ])
              .put(externalEvents.fireExternalEvent('onChange', {values, valid: false, event: 'error'}))
              .run()
          })

          test('should fire external event with values on init', () => {
            const values = {firstname: 'Ursula'}
            return expectSaga(sagas.change, {type: INITIALIZE})
              .provide([
                [matchers.call(sagas.loadIsValid), true],
                [matchers.call(sagas.getValues), values]
              ])
              .put(externalEvents.fireExternalEvent('onChange', {values, valid: true, event: 'init'}))
              .run()
          })
        })

        describe('getValues', () => {
          test('should remove meta fields', () => {
            const values = {__key: '1', __model: 'User', __version: 3, firstname: 'Ursula'}
            return expectSaga(sagas.getValues)
              .provide([[matchers.call(sagas.loadFormValues), values]])
              .returns({firstname: 'Ursula'})
              .run()
          })

          test('should transform nested paths', () => {
            const values = {firstname: 'Ursula', 'adress--city': 'Zürich'}
            return expectSaga(sagas.getValues)
              .provide([[matchers.call(sagas.loadFormValues), values]])
              .returns({firstname: 'Ursula', 'adress.city': 'Zürich'})
              .run()
          })
        })
      })
    })
  })
})
