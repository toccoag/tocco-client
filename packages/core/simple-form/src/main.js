import PropTypes from 'prop-types'
import {appFactory, externalEvents, formData, notification, actionEmitter, form} from 'tocco-app-extensions'
import {navigationStrategy, reducer as reducerUtil} from 'tocco-util'

import FormContainer from './containers/FormContainer'
import reducers, {sagas, formSagaConfig} from './modules/reducers'
import {initializeForm, setFieldDefinitions} from './modules/simpleForm/actions'
const packageName = 'simple-form'

const EXTERNAL_EVENTS = [
  /**
   * Is fired when form is submitted and sync-validation is valid
   *
   * Payload:
   * - `values` (Validated form values)
   */
  'onSubmit',
  'onCancel',
  /**
   * Fired on any value change and on init
   *
   * Payload:
   * - `values` form values
   * - `valid` true if all fields have valid inputs
   * - `event` `init|change|error`
   */
  'onChange',
  'emitAction'
]

const initApp = (id, input, events, publicPath) => {
  const content = <FormContainer />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  form.addToStore(store, formSagaConfig)
  formData.addToStore(store, state => ({
    initialData: state.input.formData,
    detailApp: state.input.detailApp,
    listApp: state.input.listApp,
    docsApp: state.input.docsApp,
    navigationStrategy: state.input.navigationStrategy
  }))
  notification.addToStore(store, false)

  const app = appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [setFieldDefinitions(form.getFieldDefinitions(input.form)), initializeForm()],
    publicPath,
    textResourceModules: ['component', 'common']
  })

  if (module.hot) {
    module.hot.accept('./modules/reducers', () => {
      const hotReducers = require('./modules/reducers').default
      reducerUtil.hotReloadReducers(app.store, hotReducers)
    })
  }

  return app
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = require('./dev/input.json')

      const app = initApp(packageName, input)

      appFactory.renderApp(app.component)
    }
  }
})()

const SimpleFormApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

SimpleFormApp.propTypes = {
  /**
   * Whether submit and cancel buttons should be shown and the form is controlled from outside (Default: false)
   */
  noButtons: PropTypes.bool,
  submitText: PropTypes.string,
  cancelText: PropTypes.string,
  /**
   * Full mandatory form definition
   */
  form: PropTypes.object.isRequired,
  /**
   * Pass `false` if the form should not validate inputs (Default: true)
   */
  validate: PropTypes.bool,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {}),
  /**
   * Object where the keys are representing the paths. Overwrittes form default values
   * e.g. {'lastname': 'Simpson', relGender: {key: '1', display: 'Male'}}
   */
  defaultValues: PropTypes.object,
  /**
   * Provides the option to set relation-entities formData in advance
   * (To show dropdown options that are not entites for example)
   */
  formData: PropTypes.object,
  /**
   * Function that gets call before each field render to determine if field should gets rendered
   */
  beforeRenderField: PropTypes.func,
  /**
   * List app (tocco-entity-list) must be provided to support remote fields
   */
  listApp: PropTypes.func,
  /**
   * Docs app (tocco-docs-browser) must be provided to support remote fields
   */
  docsApp: PropTypes.func,
  /**
   * Detail app (tocco-entity-detail) must be provided to support create modals on remote fields
   */
  detailApp: PropTypes.func,
  /**
   * Will be passed to field factory (Default `editable`)
   */
  mappingType: PropTypes.oneOf(['editable', 'search', 'readonly']),
  /**
   * Will be passed to field factory. Per default no mode is set
   * Can be set to `search` that fields are no longer mandatory
   */
  mode: PropTypes.oneOf(['list', 'detail', 'create', 'update', 'search']),
  /**
   * When provided it will fetch the dependent model and passes it to the form builder.
   * It's needed to support creates modal on remote fields
   */
  entityName: PropTypes.string,
  labelPosition: PropTypes.oneOf(['inside', 'outside-responsive', 'outside']),
  /**
   * Object consisting of various link factories. For more information see tocco-util/navigationStrategy documentation.
   */
  navigationStrategy: navigationStrategy.propTypes
}

export default SimpleFormApp
