import {CUSTOM_ARROW_NAVIGATION_ATTRIBUTE, IGNORE_ARROW_NAVIGATION_ATTRIBUTE} from './constants'
import navigations from './navigations'

const getActiveElementId = () => {
  const [rowIndex, column] = document.activeElement.id.split(':')
  return [Number.parseInt(rowIndex), column]
}

const createElementId = (rowIndex, column) => `${rowIndex}:${column}`

const isElementFocusable = element => ['INPUT', 'TEXTAREA'].includes(element.tagName) && !element.disabled

/**
 * find the element in the given row to focus
 * there might be readonly fields mixed in, so we need to check more steps away if the first was unsuccessful
 * @param rowIndex which row to navigate in
 * @param columnIndex the index of the cell we focused last
 * @param indexDiff which direction to move in
 * @param fieldsToFocus list of ids to focus, if empty, only indexed cells will be used
 * @returns {HTMLElement}
 */
const findHorizontalElementToFocus = (rowIndex, columnIndex, indexDiff, fieldsToFocus) => {
  const nextColumnIndex = columnIndex + indexDiff
  if (fieldsToFocus.length > 0 && !fieldsToFocus[nextColumnIndex]) {
    return null
  }

  const idToFocus = fieldsToFocus.length > 0 ? fieldsToFocus[nextColumnIndex] : nextColumnIndex
  const elementToFocus = document.getElementById(createElementId(rowIndex, idToFocus))
  if (elementToFocus && isElementFocusable(elementToFocus)) {
    return elementToFocus
  }

  // if an element was found, but it was not focusable, we just look for the next one
  // if no element was found, but we have an explicit list of ids to look for, we just look for the next id
  if (elementToFocus || fieldsToFocus.length > 0) {
    return findHorizontalElementToFocus(rowIndex, nextColumnIndex, indexDiff, fieldsToFocus)
  } else {
    return null
  }
}

const focusElement = elementToFocus => {
  if (elementToFocus) {
    // Reacts NumberFormat is clingy and steals focus when using left and right arrow keys, so we steal it back
    setTimeout(() => {
      elementToFocus.focus()
    }, 0)
  }
}

export const navigateVertical = event => {
  const [row, column] = getActiveElementId()
  const indexDiff = event.key === 'ArrowUp' ? -1 : 1
  const elementToFocus = document.getElementById(createElementId(row + indexDiff, column))
  focusElement(elementToFocus)
  return Boolean(elementToFocus)
}

export const navigateHorizontal = (event, fieldsToFocus) => {
  const [row, column] = getActiveElementId()
  const currentColumnIndex = fieldsToFocus.length > 0 ? fieldsToFocus.indexOf(column) : Number.parseInt(column)
  const indexDiff = event.key === 'ArrowLeft' ? -1 : 1
  const elementToFocus = findHorizontalElementToFocus(row, currentColumnIndex, indexDiff, fieldsToFocus)
  focusElement(elementToFocus)
  return Boolean(elementToFocus)
}

export const isVerticalNavigation = event => event.key === 'ArrowUp' || event.key === 'ArrowDown'
export const isHorizontalNavigation = event => event.key === 'ArrowRight' || event.key === 'ArrowLeft'

const getCustomNavigation = () => {
  const element = document.activeElement.closest(`[${CUSTOM_ARROW_NAVIGATION_ATTRIBUTE}]`)
  return element?.getAttribute(CUSTOM_ARROW_NAVIGATION_ATTRIBUTE)
}

/**
 * @param fieldsToFocus list of ids to focus, if empty, only indexed cells will be used
 * @returns a handler that focuses fields on arrow key events
 */
const navigateTable =
  (fieldsToFocus = []) =>
  event => {
    const shouldIgnoreElement = Boolean(document.activeElement.closest(`[${IGNORE_ARROW_NAVIGATION_ATTRIBUTE}]`))
    const customNavigation = getCustomNavigation()

    const navigation = customNavigation ? navigations[customNavigation] : undefined

    if (
      ['INPUT', 'TEXTAREA'].includes(document.activeElement.tagName) &&
      ['ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowUp'].includes(event.key) &&
      !shouldIgnoreElement
    ) {
      if (navigation) {
        navigation(event, {fieldsToFocus})
      } else {
        event.preventDefault()
        if (isVerticalNavigation(event)) {
          navigateVertical(event)
        } else if (isHorizontalNavigation(event)) {
          navigateHorizontal(event, fieldsToFocus)
        }
      }
    }
  }

export default navigateTable
