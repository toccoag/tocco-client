import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import navigateTable from '../navigateTable'

describe('tocco-util', () => {
  describe('keyboard', () => {
    describe('navigations', () => {
      describe('navigateDatepicker', () => {
        it('should not navigate vertically', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="prev" data-custom-arrow-navigation="datepicker" />
              <input id="0:1" data-custom-arrow-navigation="datepicker" />
              <input id="1:0" data-testid="start" data-custom-arrow-navigation="datepicker" />
              <input id="1:1" data-custom-arrow-navigation="datepicker" />
              <input id="2:0" data-testid="next" data-custom-arrow-navigation="datepicker" />
              <input id="2:1" data-custom-arrow-navigation="datepicker" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowUp}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
          jestExpect(screen.getByTestId('prev')).not.toHaveFocus()

          await user.keyboard('{ArrowDown}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
          jestExpect(screen.getByTestId('prev')).not.toHaveFocus()
        })

        it('should navigate horizontally on empty fields', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" data-custom-arrow-navigation="datepicker" />
              <input id="0:1" data-testid="expected" data-custom-arrow-navigation="datepicker" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the left to focus
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // focus element to the right
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the right to focus
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // focus element to the left
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })

        it('should navigate within fields for field with value', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" defaultValue="abcdf" data-custom-arrow-navigation="datepicker" />
              <input id="0:1" data-testid="expected" defaultValue="abcdf" data-custom-arrow-navigation="datepicker" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the left to focus
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // keep focus on element to navigate through value
          await user.keyboard('{Home}')
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
          await user.keyboard('{ArrowRight}')
          await user.keyboard('{ArrowRight}')
          await user.keyboard('{ArrowRight}')
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the right to focus
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus on element to navigate through value
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
          await user.keyboard('{ArrowLeft}')
          await user.keyboard('{ArrowLeft}')
          await user.keyboard('{ArrowLeft}')
          await user.keyboard('{ArrowLeft}')
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })

        it('should dispatch blur event', async () => {
          const handleBlurEvent = sinon.spy()
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="one" data-custom-arrow-navigation="datepicker" />
              <input id="0:1" data-testid="two" data-custom-arrow-navigation="datepicker" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)
          screen.getByTestId('one').addEventListener('tableNavigationBlur', handleBlurEvent)
          screen.getByTestId('two').addEventListener('tableNavigationBlur', handleBlurEvent)

          await user.click(screen.getByTestId('one'))

          // only call blur when navigating away
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('one')).toHaveFocus()
          expect(handleBlurEvent).not.to.have.been.called

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('two')).toHaveFocus()
          expect(handleBlurEvent).to.have.been.calledOnce

          handleBlurEvent.resetHistory()
          await user.click(screen.getByTestId('two'))
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('two')).toHaveFocus()
          expect(handleBlurEvent).not.to.have.been.called

          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('one')).toHaveFocus()
          expect(handleBlurEvent).to.have.been.calledOnce
        })
      })
    })
  })
})
