import {isVerticalNavigation, navigateHorizontal, navigateVertical} from '../navigateTable'
import {isHorizontalNavigationWithCorrectCursorPosition} from '../utils'

const navigateString = (event, {fieldsToFocus}) => {
  if (isVerticalNavigation(event)) {
    navigateVertical(event)
    event.preventDefault()
  } else if (isHorizontalNavigationWithCorrectCursorPosition(event)) {
    // only navigate to next / prev field when cursor is already at the start or end of the input field
    navigateHorizontal(event, fieldsToFocus)
    event.preventDefault()
  }
}

export default navigateString
