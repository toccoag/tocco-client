import {CUSTOM_ARROW_NAVIGATION_ATTRIBUTE} from '../constants'
import {navigateHorizontal} from '../navigateTable'
import {isHorizontalNavigationWithCorrectCursorPosition} from '../utils'

const dispatchBlurEvent = () => {
  const element = document.activeElement.closest(`[${CUSTOM_ARROW_NAVIGATION_ATTRIBUTE}]`)
  if (element) {
    element.dispatchEvent(new Event('tableNavigationBlur', {}))
  }
}

// do not navigate vertical
const navigateDatepicker = (event, {fieldsToFocus}) => {
  if (isHorizontalNavigationWithCorrectCursorPosition(event)) {
    if (navigateHorizontal(event, fieldsToFocus)) {
      dispatchBlurEvent()
    }
    event.preventDefault()
  }
}

export default navigateDatepicker
