import navigateDatepicker from './navigateDatepicker'
import navigateString from './navigateString'
import navigateText from './navigateText'

export default {
  datepicker: navigateDatepicker,
  string: navigateString,
  text: navigateText
}
