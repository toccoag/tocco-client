import navigateString from './navigateString'

// TODO: https://toccoag.atlassian.net/browse/TOCDEV-9498 - consider multiple lines on vertical navigation
const navigateText = (event, {fieldsToFocus}) => {
  navigateString(event, {fieldsToFocus})
}

export default navigateText
