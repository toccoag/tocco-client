export const isCursorAtStart = () =>
  document.activeElement.selectionStart === 0 && document.activeElement.selectionEnd === 0

export const isCursorAtEnd = () => {
  const endPosition = document.activeElement.value.length
  return document.activeElement.selectionStart === endPosition && document.activeElement.selectionEnd === endPosition
}

export const isHorizontalNavigationWithCorrectCursorPosition = event =>
  (isCursorAtStart() && event.key === 'ArrowLeft') || (isCursorAtEnd() && event.key === 'ArrowRight')
