import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import {testingLibrary} from 'tocco-test-util'

import navigateTable from './navigateTable'

describe('tocco-util', () => {
  describe('keyboard', () => {
    describe('navigateTable', () => {
      describe('navigation by index', () => {
        it('should navigate horizontally', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" />
              <input id="0:1" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the left to focus
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // focus element to the right
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the right to focus
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // focus element to the left
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })

        it('should skip disabled elements', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" />
              <input id="0:1" disabled />
              <input id="0:2" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should skip non-input elements', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" />
              <p id="0:1" />
              <textarea id="0:2" data-testid="textarea" />
              <p id="0:3" />
              <input id="0:4" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('textarea')).toHaveFocus()

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should ignore keyhandling on excluded elements', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" />
              <input id="0:1" data-ignore-arrow-navigation data-testid="expected" />
              <input id="0:2" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should navigate vertically', async () => {
          const testTable = (
            <div onKeyDown={navigateTable()}>
              <input id="0:0" data-testid="start" />
              <input id="0:1" />
              <input id="1:0" data-testid="expected" />
              <input id="1:1" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the top to focus
          await user.keyboard('{ArrowUp}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // focus element to the bottom
          await user.keyboard('{ArrowDown}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the bottom to focus
          await user.keyboard('{ArrowDown}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // focus element to the top
          await user.keyboard('{ArrowUp}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })
      })

      describe('navigation by ids', () => {
        it('should navigate horizontally', async () => {
          const testTable = (
            <div onKeyDown={navigateTable(['first', 'second'])}>
              <input id="0:first" data-testid="start" />
              <input id="0:second" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the left to focus
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // focus element to the right
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the right to focus
          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // focus element to the left
          await user.keyboard('{ArrowLeft}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })

        it('should skip disabled element', async () => {
          const testTable = (
            <div onKeyDown={navigateTable(['first', 'second', 'third'])}>
              <input id="0:first" data-testid="start" />
              <input id="0:second" disabled />
              <input id="0:third" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should skip non-input element', async () => {
          const testTable = (
            <div onKeyDown={navigateTable(['first', 'second', 'third', 'fourth', 'fifth'])}>
              <input id="0:first" data-testid="start" />
              <p id="0:second" />
              <textarea id="0:third" data-testid="textarea" />
              <p id="0:fourth" />
              <input id="0:fifth" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('textarea')).toHaveFocus()

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should skip non-existing element', async () => {
          const testTable = (
            <div onKeyDown={navigateTable(['first', 'second', 'third'])}>
              <input id="0:first" data-testid="start" />
              <input id="0:other" />
              <input id="0:third" data-testid="expected" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          await user.keyboard('{ArrowRight}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()
        })

        it('should navigate vertically', async () => {
          const testTable = (
            <div onKeyDown={navigateTable(['first', 'second'])}>
              <input id="0:first" data-testid="start" />
              <input id="0:second" />
              <input id="1:first" data-testid="expected" />
              <input id="1:second" />
            </div>
          )

          const user = userEvent.setup()
          testingLibrary.renderWithIntl(testTable)

          await user.click(screen.getByTestId('start'))

          // keep focus if nothing to the top to focus
          await user.keyboard('{ArrowUp}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()

          // focus element to the bottom
          await user.keyboard('{ArrowDown}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // keep focus if nothing to the bottom to focus
          await user.keyboard('{ArrowDown}')
          jestExpect(screen.getByTestId('expected')).toHaveFocus()

          // focus element to the top
          await user.keyboard('{ArrowUp}')
          jestExpect(screen.getByTestId('start')).toHaveFocus()
        })
      })
    })
  })
})
