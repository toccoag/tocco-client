import {addDays} from 'date-fns'
import _isObject from 'lodash/isObject'
import {js} from 'tocco-util'

import {parseInLocalTime, parseTime, formatTQLDatetime, formatTQLTime, parseAsStartOfDayInLocalTime} from './utils'

const isDefined = v => js.isDefined(v) && v !== ''

export const getTql = (path, value, fieldType) => {
  if (!isDefined(value)) {
    return null
  }

  const type = fieldType || guessType(value)
  value = rangeMappings(type)(value)

  const isLocalized = value && typeof value === 'object' && value.localizedPaths
  const typeHandler = typeHandlers(type)

  if (isRangeValue(value)) {
    return handleRangeValue(value, typeHandler, path)
  } else if (isLocalized) {
    return handleLocalized(value.localizedPaths, value.value, typeHandler)
  } else {
    return typeHandler(path, value, '==')
  }
}

const handleRangeValue = (value, typeHandler, path) =>
  [
    ...(isDefined(value.from) ? [typeHandler(path, value.from, '>=')] : []),
    ...(isDefined(value.to) ? [typeHandler(path, value.to, value.exclusive ? '<' : '<=')] : [])
  ].join(' and ')

const isRangeValue = value => value && typeof value === 'object' && value.isRangeValue

const handleLocalized = (paths, value, typeHandler) =>
  `(${paths.map(path => typeHandler(path, value, '==')).join(' or ')})`

const rangeMappings = type => {
  switch (type) {
    case 'datetime':
    case 'createts':
    case 'updatets':
      return value => {
        if (isRangeValue(value)) {
          return value
        }
        const date = parseAsStartOfDayInLocalTime(value)
        if (!isNaN(date)) {
          return {
            from: date.toISOString(),
            to: addDays(date, 1).toISOString(),
            isRangeValue: true,
            exclusive: true
          }
        } else {
          return value
        }
      }
    default:
      return value => value
  }
}

const escapeFulltextValue = value => value.replace(/\\/g, '\\\\').replace(/"/g, '\\"').replace(/\//g, '\\\\/')

const getFulltextSearchTerm = value => {
  //  Closes quotes at end of last term in case of uneven count of quotes
  const quotesCount = value.match(/"/g)?.length || 0
  if (quotesCount % 2 !== 0) {
    value += '"'
  }

  const escapedValue = escapeFulltextValue(value)

  const endsWithSpace = value.endsWith(' ')
  const endsWithQuote = value.endsWith('"')
  const endsWithWildcard = value.endsWith('*')
  const endsWithQuestionMark = value.endsWith('?')
  const endsWithBrace = value.endsWith(')')
  const endsWithRangeOperator = value.endsWith(']') || value.endsWith('}')
  const isNumberAtEnd = /^.* [0-9.,]+/.test(value)

  /*
  Adds wildcard at end of last term, if:
  - term is not a number
  - term does not start with a quote
  - term does not end with a wildcard or question mark
  - term does not end with a brace
  - term does not end with a range operator
  */
  if (
    !endsWithQuote &&
    !endsWithWildcard &&
    !endsWithQuestionMark &&
    !endsWithSpace &&
    !endsWithBrace &&
    !isNumberAtEnd &&
    !endsWithRangeOperator
  ) {
    return `"(${escapedValue}) OR (${escapedValue}*)"`
  } else {
    return `"${escapedValue}"`
  }
}

const typeHandlers = type => {
  switch (type) {
    case 'multi-select-box':
    case 'multi-remote-field':
      return (path, value) => `KEYS("${path}",${value.map(v => v.key).join(',')})`
    case 'single-remote-field':
    case 'single-select-box':
      return (path, value) => `${path}.pk == ${value.key}`
    case 'fulltext-search':
      return (path, value) => {
        return path === 'txtFulltext'
          ? `fulltext(${getFulltextSearchTerm(value)})`
          : `fulltext(${getFulltextSearchTerm(value)}, ${path})`
      }
    case 'birthdate':
    case 'date':
    case 'create_timestamp':
      return (path, value, comp) => `${path} ${comp} date:"${value}"`
    case 'datetime':
    case 'createts':
    case 'updatets':
      return (path, value, comp) => `${path} ${comp} datetime:"${formatTQLDatetime(parseInLocalTime(value))}"`
    case 'time':
      return (path, value, comp) => `${path} ${comp} time:"${formatTQLTime(parseTime(value))}"`
    case 'compressed-text':
    case 'identifier':
    case 'string':
    case 'text':
    case 'html':
    case 'createuser':
    case 'updateuser':
    case 'login':
    case 'email':
    case 'phone':
    case 'postcode':
      return (path, value) => `${path} ~= "*${value}*"`
    case 'boolean':
      return (path, value) => (isDefined(value) ? `${path} == ${value}` : null)
    case 'marking':
      return (path, value) => (value === false ? `not exists(${path})` : `exists(${path})`)
    case 'geosearch':
      return (_path, value) => {
        return value?.latitude && value?.longitude
          ? `${value.distance || 0} > DISTANCE(${value.latitudeField}, ${value.longitudeField}, decimal:"${
              value.latitude
            }", decimal:"${value.longitude}")`
          : null
      }
    default:
      return (path, value, comp) => `${path} ${comp} ${value}`
  }
}

// For fields that are not in the search form (e.g. parent and preselectedSearchFields)
const guessType = value => {
  if (_isObject(value) && value.key) {
    return 'single-select-box'
  }

  if (Array.isArray(value) && value.every(v => !!v.key)) {
    return 'multi-select-box'
  }

  return 'string'
}
