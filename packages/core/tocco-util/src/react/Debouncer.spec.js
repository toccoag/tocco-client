import {fireEvent, screen, act} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import Debouncer from './Debouncer'

describe('tocco-util', () => {
  describe('react', () => {
    describe('Debouncer', () => {
      let clock

      beforeEach(() => {
        clock = sinon.useFakeTimers()
      })

      afterEach(() => {
        clock.restore()
      })

      const TestComponent = Debouncer(({value, onChange, xy}) => {
        return (
          <div>
            <input data-testid="input" value={value} onChange={e => onChange(e.target.value)} /> {xy}
          </div>
        )
      }, 100)

      test('should debounce value change of wrapped component', async () => {
        const initialValue = 'Test'
        const onChangeSpy = sinon.spy()

        testingLibrary.renderWithTheme(<TestComponent value={initialValue} onChange={onChangeSpy} />)

        const input = screen.getByTestId('input')

        fireEvent.change(input, {target: {value: 'Test1'}})
        fireEvent.change(input, {target: {value: 'Test2'}})

        act(() => {
          clock.tick(200)
        })

        fireEvent.change(input, {target: {value: 'Test3'}})
        fireEvent.change(input, {target: {value: 'Test4'}})

        act(() => {
          clock.tick(200)
        })

        expect(onChangeSpy).to.have.been.calledTwice
        expect(onChangeSpy).to.have.been.calledWith('Test2')
        expect(onChangeSpy).to.have.been.calledWith('Test4')
      })

      test('should pass other props', async () => {
        testingLibrary.renderWithTheme(<TestComponent value="" onChange={() => {}} xy="123" />)

        expect(screen.queryByText('123')).to.exist
      })

      test('should support custom change function name', async () => {
        const initialValue = 'Test'
        const onChangeSpy = sinon.spy()

        const TestComponent2 = Debouncer(
          ({value, onSearch, xy}) => {
            return (
              <div>
                <input data-testid="input2" value={value} onChange={e => onSearch(e.target.value)} /> {xy}
              </div>
            )
          },
          100,
          'onSearch'
        )

        testingLibrary.renderWithTheme(<TestComponent2 value={initialValue} onSearch={onChangeSpy} />)

        const input = screen.getByTestId('input2')

        fireEvent.change(input, {target: {value: 'Test1'}})
        fireEvent.change(input, {target: {value: 'Test2'}})

        act(() => {
          clock.tick(200)
        })

        fireEvent.change(input, {target: {value: 'Test3'}})
        fireEvent.change(input, {target: {value: 'Test4'}})

        act(() => {
          clock.tick(200)
        })

        expect(onChangeSpy).to.have.been.calledTwice
        expect(onChangeSpy).to.have.been.calledWith('Test2')
        expect(onChangeSpy).to.have.been.calledWith('Test4')
      })

      test('should be able to change value from outside multiple times within the given debounce delay', async () => {
        const value = 'Test'
        const onChangeSpy = sinon.spy()

        const {rerender} = testingLibrary.renderWithTheme(<TestComponent value={value} onChange={onChangeSpy} />)

        rerender(<TestComponent value="ABC" onChange={onChangeSpy} />)

        jestExpect(screen.getByTestId('input')).toHaveValue('ABC')

        rerender(<TestComponent value="DEF" onChange={onChangeSpy} />)

        jestExpect(screen.getByTestId('input')).toHaveValue('DEF')
      })
    })
  })
})
