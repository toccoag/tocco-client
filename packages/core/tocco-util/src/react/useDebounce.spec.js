/* eslint-disable react/prop-types */
import {fireEvent, screen, act} from '@testing-library/react'
import {useEffect, useState} from 'react'
import {testingLibrary} from 'tocco-test-util'

import useDebounce from './useDebounce'

describe('tocco-util', () => {
  describe('hooks', () => {
    describe('useDebounce', () => {
      let clock

      beforeEach(() => {
        clock = sinon.useFakeTimers()
      })

      afterEach(() => {
        clock.restore()
      })

      test('should call onChange callback debounced', async () => {
        const initialValue = 'Test'

        const onChangeSpy = sinon.spy()

        const TestComponent = ({value, onChange}) => {
          const [internalValue, setInternalValue] = useState(value)
          const [debouncedValue] = useDebounce(internalValue, 100)

          useEffect(() => {
            onChange(debouncedValue)
          }, [debouncedValue, onChange])

          return <input data-testid="input" onChange={e => setInternalValue(e.target.value)} value={value} />
        }

        testingLibrary.renderWithTheme(<TestComponent onChange={onChangeSpy} value={initialValue} />)

        const input = screen.getByTestId('input')

        fireEvent.change(input, {target: {value: 'Test1'}})
        fireEvent.change(input, {target: {value: 'Test2'}})

        act(() => {
          clock.tick(100)
        })

        fireEvent.change(input, {target: {value: 'Test3'}})
        fireEvent.change(input, {target: {value: 'Test4'}})

        act(() => {
          clock.tick(100)
        })

        expect(onChangeSpy).to.have.been.calledThrice
        expect(onChangeSpy).to.have.been.calledWith(initialValue)
        expect(onChangeSpy).to.have.been.calledWith('Test2')
        expect(onChangeSpy).to.have.been.calledWith('Test4')
      })

      test('should be able to reset debounce value from outside anytime', async () => {
        const initialValue = 'Test'

        const TestComponent = props => {
          const [debouncedValue, setDebouncedValue] = useDebounce(props.value, 100)

          return <input data-testid="input" onChange={e => setDebouncedValue(e.target.value)} value={debouncedValue} />
        }

        testingLibrary.renderWithTheme(<TestComponent value={initialValue} />)

        fireEvent.change(screen.getByTestId('input'), {target: {value: 'Test1'}})
        jestExpect(screen.getByTestId('input')).toHaveValue('Test1')

        fireEvent.change(screen.getByTestId('input'), {target: {value: 'Test2'}})
        jestExpect(screen.getByTestId('input')).toHaveValue('Test2')
      })
    })
  })
})
