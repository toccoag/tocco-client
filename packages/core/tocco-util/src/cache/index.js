import {
  addObjectCache,
  addLongTerm,
  getObjectCache,
  getLongTerm,
  removeObjectCache,
  removeLongTerm,
  clearObjectCache,
  clearLongTerm,
  getLongTermMetaInfo,
  setLongTermMetaInfo,
  getObjectCacheMetaInfo,
  setObjectCacheMetaInfo
} from './cache'
export default {
  addObjectCache,
  addLongTerm,
  getObjectCache,
  getLongTerm,
  removeObjectCache,
  removeLongTerm,
  clearObjectCache,
  clearLongTerm,
  getLongTermMetaInfo,
  setLongTermMetaInfo,
  getObjectCacheMetaInfo,
  setObjectCacheMetaInfo
}
