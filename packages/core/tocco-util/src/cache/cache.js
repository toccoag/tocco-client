import _unset from 'lodash/unset'

import consoleLogger from '../consoleLogger'
import nice from '../nice'

/*
 * Object cache is per browser tab and clears on reload.
 *   - depenend on locale, user, business unit
 *   - e.g. form, model, display
 * Long term cache (localStorage) is shared across tabs and browser sessions.
 *   - dependend on revision
 *   - e.g. textResources, sso module
 *
 * Object cache gets cleared
 *  - individually (e.g. displays on change entity)
 *  - on
 *    - locale change
 *    - user change
 *    - business unit change
 *
 * Long Term cache gets cleared
 *  - on
 *    - revision change
 */

const metaInfoType = 'meta'
const prefix = 'tocco.cache'

const getKey = (type, id) => `${prefix}.${type}.${id}`
const isMetaInfo = key => key.startsWith(`${prefix}.${metaInfoType}`)

const objectCache = {}

const objectWriter = (key, value) => {
  objectCache[key] = value
}
const storageWriter = storage => (key, value) => storage.setItem(key, JSON.stringify(value))

export const addObjectCache = (type, id, value) => add(type, id, value, objectWriter)
export const addLongTerm = (type, id, value) => add(type, id, value, storageWriter(localStorage))

const add = (type, id, value, write) => {
  if (__DEV__ || nice.getRunEnv() !== 'PRODUCTION') {
    return null
  }
  return write(getKey(type, id), value)
}

export const getObjectCache = (type, id) => objectCache[getKey(type, id)]
export const getLongTerm = (type, id) => getFromStorage(type, id, localStorage)

const getFromStorage = (type, id, storage) => {
  const cachedValue = storage.getItem(getKey(type, id))

  if (!cachedValue) {
    return undefined
  }

  return JSON.parse(cachedValue)
}

export const removeObjectCache = (type, id) => _unset(objectCache, getKey(type, id))
export const removeLongTerm = (type, id) => removeFromStorage(type, id, localStorage)
const removeFromStorage = (type, id, storage) => storage.removeItem(getKey(type, id))

export const clearAll = () => {
  clearObjectCache()
  clearLongTerm()
}

export const clearObjectCache = () => {
  consoleLogger.log('cache - object cache invalidated')
  Object.keys(objectCache).forEach(key => _unset(objectCache, key))
}
export const clearLongTerm = () => {
  consoleLogger.log('cache - long term cache invalidated')
  clearStorage(localStorage)
}
const clearStorage = storage => {
  const keys = Object.keys(storage)
  keys.filter(key => key.startsWith(prefix) && !isMetaInfo(key)).forEach(key => storage.removeItem(key))
}

export const getLongTermMetaInfo = () => {
  const revision = getLongTerm(metaInfoType, 'revision')

  return {
    revision
  }
}

export const setLongTermMetaInfo = ({revision}) => {
  addLongTerm(metaInfoType, 'revision', revision || '')
}

export const getObjectCacheMetaInfo = () => {
  const locale = getObjectCache(metaInfoType, 'locale')
  const businessUnitId = getObjectCache(metaInfoType, 'businessUnitId')
  const username = getObjectCache(metaInfoType, 'username')

  return {
    locale,
    businessUnitId,
    username
  }
}

export const setObjectCacheMetaInfo = ({locale, businessUnitId, username}) => {
  addObjectCache(metaInfoType, 'locale', locale || '')
  addObjectCache(metaInfoType, 'businessUnitId', businessUnitId || '')
  addObjectCache(metaInfoType, 'username', username || '')
}
