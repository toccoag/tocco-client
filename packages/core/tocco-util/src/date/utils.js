import {getHours, getMinutes, setHours, setMilliseconds, setMinutes, setSeconds} from 'date-fns'
import {de} from 'date-fns/locale/de'
import {enUS} from 'date-fns/locale/en-US'
import {frCH} from 'date-fns/locale/fr-CH'
import {it} from 'date-fns/locale/it'

import {roundDecimalPlaces} from '../js/helpers'

export const millisecondsToDuration = ms => {
  if (!ms && ms !== 0) {
    return {
      hours: '',
      minutes: '',
      seconds: ''
    }
  }

  let seconds = roundDecimalPlaces((ms / 1000) % 60, 3)

  ms -= Math.round(seconds * 1000)

  let minutes = parseInt((ms / (1000 * 60)) % 60)

  ms -= minutes * 1000 * 60

  const hours = parseInt(ms / (1000 * 60 * 60))

  // only biggest unit should be negative if negative duration
  if (minutes < 0 || hours < 0) {
    seconds = Math.abs(seconds)
  }
  if (hours < 0) {
    minutes = Math.abs(minutes)
  }

  return {
    hours,
    minutes,
    seconds
  }
}

export const formatDuration = ms => {
  const {hours, minutes, seconds} = millisecondsToDuration(ms)

  /**
   * Attention:
   * Intl.DateTimeFormat cannot be used.
   * In locale 'en' the output is: 24:00:00.000 instead of 00:00:00.000
   */
  const duration = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}`
  const durationWithSeconds = `${duration}:${seconds.toFixed(3).padStart(6, '0')}`

  /**
   * The user cannot enter the duration in seconds (hours/minutes are already accurate enough).
   * However, duration values that are automatically measured can have seconds/milliseconds values.
   * Therefore, we show seconds/milliseconds only if such values are present.
   * (e.g. System_activity)
   */
  return seconds ? durationWithSeconds : duration
}

export const getDateFnsLocale = locale => {
  switch (locale) {
    case 'fr':
      return frCH
    case 'en':
      return enUS
    case 'it':
      return it
    case 'de':
    case 'de-CH':
    case 'de-DE':
    default:
      return de
  }
}

export const getLocalizedDateFormat = locale => {
  const {formatLong} = getDateFnsLocale(locale)

  return formatLong.date({width: 'short'})
}
export const getLocalizedDateTimeFormat = locale => `${getLocalizedDateFormat(locale)} HH:mm`

export const getLocalizedDateFormatWithoutPunctuation = locale => getLocalizedDateFormat(locale).replace(/[/.]/g, '')
export const getLocalizedDateTimeFormatWithoutPunctuation = locale =>
  `${getLocalizedDateFormatWithoutPunctuation(locale)} HHmm`

export const useTwoDigitYear = format => format.replace(/y+/, 'yy')

export const setCurrentTime = date => {
  const now = new Date()

  let dateTime = new Date(date)
  dateTime = setHours(dateTime, getHours(now))
  dateTime = setMinutes(dateTime, getMinutes(now))
  dateTime = setSeconds(dateTime, 0)
  dateTime = setMilliseconds(dateTime, 0)

  return dateTime
}

export const selectUnit = value => {
  const DAY_MILLISECONDS = 1000 * 60 * 60 * 24
  const timeDifference = value - new Date().getTime()

  if (timeDifference > -DAY_MILLISECONDS) {
    const secondsDifference = Math.round(timeDifference / 1000)
    if (secondsDifference > -60) {
      return {
        diffValue: secondsDifference,
        unit: 'second'
      }
    } else if (secondsDifference > -3600) {
      return {
        diffValue: Math.ceil(secondsDifference / 60),
        unit: 'minute'
      }
    } else {
      return {
        diffValue: Math.ceil(secondsDifference / 3600),
        unit: 'hour'
      }
    }
  } else {
    const daysDifference = Math.round(timeDifference / DAY_MILLISECONDS)
    if (daysDifference > -30) {
      return {
        diffValue: daysDifference,
        unit: 'day'
      }
    } else if (daysDifference > -365) {
      return {
        diffValue: Math.ceil(daysDifference / 30),
        unit: 'month'
      }
    } else {
      return {
        diffValue: Math.ceil(daysDifference / 365),
        unit: 'year'
      }
    }
  }
}

export const getTimeZone = () => Intl.DateTimeFormat().resolvedOptions().timeZone
