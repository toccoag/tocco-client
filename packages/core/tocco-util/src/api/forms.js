/**
 * Splits form id (e.g. Root_docs_list_item_list or Reservation_infobox) into `formName` and `scope`.
 *
 * @param {string} formId
 * @returns `{formName, scope}`
 */
export const splitFormId = formId => {
  const index = formId.lastIndexOf('_')
  const formName = formId.substring(0, index)
  const scope = formId.substring(index + 1)
  return {
    formName,
    scope
  }
}
