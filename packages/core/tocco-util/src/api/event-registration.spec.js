import eventRegistration from './event-registration'

describe('tocco-util', () => {
  describe('api', () => {
    describe('event-registration', () => {
      describe('getEventRelationshipModulesQuery', () => {
        test('shoud setup query for fetching event submodules', () => {
          const eventKey = '123'

          const {query, entityName} = eventRegistration.getEventRelationshipModulesQuery(eventKey)

          expect(entityName).to.eql('Event_relationship')
          expect(query.where).to.eql(
            `relModule.pk ==  ${eventKey} and relEvent_relationship_registration.unique_id == "show"`
          )
          expect(query.paths).to.include('relCourse')
          expect(query.paths).to.include('relEvent_relationship_type.unique_id')
        })
      })
    })
  })
})
