const isDisplayableChild = child => !child.hidden

export const getSortingAttributes = (column, sorting) => {
  const idx = sorting?.findIndex(s => s.field === column.id)

  return idx >= 0
    ? {
        sortRank: idx + 1,
        order: sorting[idx].order
      }
    : {}
}

const rightAlignedTypes = [
  'counter',
  'decimal',
  'double',
  'integer',
  'latitude',
  'long',
  'longitude',
  'moneyamount',
  'percent',
  'serial',
  'sorting',
  'version',
  'number'
]

export const getColumnAlignment = column => {
  if (column?.alignment) {
    return column.alignment.toLowerCase()
  }

  if (column?.children?.length === 1) {
    return rightAlignedTypes.includes(column.children[0].dataType) ? 'right' : 'left'
  }
  if (column?.dataType) {
    return rightAlignedTypes.includes(column.dataType) ? 'right' : 'left'
  }

  return 'left'
}

/**
 * Transforms nice form column definitions in a tocco-ui table compatible format.
 * @param {array} columns - columns from table form definition
 * @param {array} sorting - sorting array with field and order attributes
 * @param {function} cellRenderer - Function to return rendered cell given data and column definition
 */
export const getColumnDefinition = (columns, sorting, cellRenderer, ...args) => {
  if (!columns) {
    return []
  }

  return columns
    .filter(column => !column.hidden && column.children.filter(isDisplayableChild).length > 0)
    .map((c, idx) => ({
      idx,
      id: c.id,
      label: c.label,
      sorting: {
        sortable: c.sortable,
        ...getSortingAttributes(c, sorting)
      },
      children: c.children.filter(isDisplayableChild),
      resizable: !c.widthFixed,
      alignment: getColumnAlignment(c),
      shrinkToContent: c.shrinkToContent || false,
      sticky: c.sticky || false,
      CellRenderer: ({rowData, column}) => column.children.map(child => cellRenderer(child, rowData, ...args))
    }))
}
