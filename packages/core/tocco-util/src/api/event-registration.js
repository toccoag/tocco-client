/**
 * Creates query to fetch shown submodules for an event.
 * @param {string} eventKey - Parent event key
 * @returns Object - {query, entityName}
 */
const getEventRelationshipModulesQuery = eventKey => {
  const query = {
    where: `relModule.pk ==  ${eventKey} and relEvent_relationship_registration.unique_id == "show"`,
    paths: ['relCourse', 'relEvent_relationship_type.unique_id', 'sorting'],
    sort: 'sorting desc'
  }

  return {query, entityName: 'Event_relationship'}
}

export default {getEventRelationshipModulesQuery}
