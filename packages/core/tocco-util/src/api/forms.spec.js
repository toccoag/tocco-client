import {splitFormId} from './forms'

describe('tocco-util', () => {
  describe('api', () => {
    describe('forms', () => {
      describe('splitFormId', () => {
        test('should split form id into form name and scope', () => {
          expect(splitFormId('Root_docs_list_item_list')).to.eql({
            formName: 'Root_docs_list_item',
            scope: 'list'
          })
        })
      })
    })
  })
})
