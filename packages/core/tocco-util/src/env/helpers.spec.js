import {
  EmbedType,
  getBackendUrl,
  getBusinessUnit,
  getEmbedType,
  getRuleProvider,
  getWidgetConfigKey,
  setBackendUrl,
  setBusinessUnit,
  setEmbedType,
  setRuleProvider,
  setWidgetConfigKey
} from './env'
import {setInputEnvs} from './helpers'

describe('tocco-util', () => {
  describe('env', () => {
    describe('helpers', () => {
      beforeEach(() => {
        setBackendUrl(undefined)
        setBusinessUnit(undefined)
        setRuleProvider(undefined)
        setEmbedType(EmbedType.admin)
        setWidgetConfigKey(undefined)
      })

      test('should fill all infos from input', () => {
        const input = {
          backendUrl: 'https://master.tocco.ch',
          businessUnit: 'test1',
          ruleProvider: 'rule-provider',
          appContext: {
            embedType: 'widget',
            widgetConfigKey: 'widget-abc'
          }
        }
        setInputEnvs(input)

        expect(getBackendUrl()).to.eql('https://master.tocco.ch')
        expect(getBusinessUnit()).to.eql('test1')
        expect(getRuleProvider()).to.eql('rule-provider')
        expect(getEmbedType()).to.eql(EmbedType.widget)
        expect(getWidgetConfigKey()).to.eql('widget-abc')
      })

      test('should ignore unset infos from input', () => {
        setBackendUrl('https://master.tocco.ch')
        setBusinessUnit('test1')
        setRuleProvider('rule-provider')
        setEmbedType(EmbedType.widget)
        setWidgetConfigKey('widget-abc')

        const input = {}
        setInputEnvs(input)

        expect(getBackendUrl()).to.eql('https://master.tocco.ch')
        expect(getBusinessUnit()).to.eql('test1')
        expect(getRuleProvider()).to.eql('rule-provider')
        expect(getEmbedType()).to.eql(EmbedType.widget)
        expect(getWidgetConfigKey()).to.eql('widget-abc')
      })
    })
  })
})
