import embedStrategyFactory, {embedStrategy} from './embedStrategyFactory'
import {
  getBackendUrl,
  getBusinessUnit,
  getRuleProvider,
  setBackendUrl,
  setBusinessUnit,
  setRuleProvider,
  getEmbedType,
  setEmbedType,
  EmbedType,
  isInAdminEmbedded,
  isInWidgetEmbedded,
  getWidgetConfigKey,
  setWidgetConfigKey,
  asInputProps,
  NULL_BUSINESS_UNIT,
  ALLOWED_EMBED_TYPES
} from './env'
import {setInputEnvs} from './helpers'

export default {
  EmbedType,
  isInAdminEmbedded,
  isInWidgetEmbedded,
  embedStrategyFactory,
  embedStrategy,
  getBackendUrl,
  setBackendUrl,
  getBusinessUnit,
  getRuleProvider,
  setBusinessUnit,
  setRuleProvider,
  getEmbedType,
  setEmbedType,
  getWidgetConfigKey,
  setWidgetConfigKey,
  asInputProps,
  NULL_BUSINESS_UNIT,
  ALLOWED_EMBED_TYPES,
  setInputEnvs
}
