import {
  EmbedType,
  getBackendUrl,
  getBusinessUnit,
  getEmbedType,
  isInAdminEmbedded,
  isInWidgetEmbedded,
  NULL_BUSINESS_UNIT,
  setBackendUrl,
  setBusinessUnit,
  setEmbedType,
  asInputProps,
  setRuleProvider,
  getRuleProvider
} from './env'

describe('tocco-util', () => {
  describe('env', () => {
    beforeEach(() => {
      setBackendUrl(undefined)
      setBusinessUnit(undefined)
      setEmbedType(EmbedType.admin)
    })

    describe('backendUrl', () => {
      test('should return __BACKEND_URL__ as initial backend url', () => {
        expect(getBackendUrl()).to.equal(__BACKEND_URL__)
      })

      test('should return set backend url', () => {
        setBackendUrl('test')
        expect(getBackendUrl()).to.equal('test')
      })
    })

    describe('businessUnit', () => {
      test('should return undefined as initial business unit', () => {
        expect(getBusinessUnit()).to.be.undefined
      })

      test('should return set business unit', () => {
        setBusinessUnit('test')
        expect(getBusinessUnit()).to.equal('test')
      })

      test('should have NULL_BUSINESS_UNIT', () => {
        setBusinessUnit(NULL_BUSINESS_UNIT)
        expect(getBusinessUnit()).to.equal('__n-u-l-l__')
      })
    })

    describe('embedType', () => {
      test('should return admin as default embed type', () => {
        expect(getEmbedType()).to.eql(EmbedType.admin)
      })

      test('should be embedded in Admin for admin embed type', () => {
        setEmbedType(EmbedType.admin)
        expect(isInAdminEmbedded()).to.be.true
      })

      test('should be embedded in Admin for legacy admin embed type', () => {
        setEmbedType(EmbedType.legacyAdmin)
        expect(isInAdminEmbedded()).to.be.true
      })

      test('should not be embedded in Admin for widget and legacy widget embed type', () => {
        setEmbedType(EmbedType.widget)
        expect(isInAdminEmbedded()).to.be.false

        setEmbedType(EmbedType.legacyWidget)
        expect(isInAdminEmbedded()).to.be.false
      })

      test('should be embedded in Widget for widget embed type', () => {
        setEmbedType(EmbedType.widget)
        expect(isInWidgetEmbedded()).to.be.true
      })

      test('should not be embedded in Widget for admin, legacy admin and legacy widget embed type', () => {
        setEmbedType(EmbedType.admin)
        expect(isInWidgetEmbedded()).to.be.false

        setEmbedType(EmbedType.legacyAdmin)
        expect(isInWidgetEmbedded()).to.be.false

        setEmbedType(EmbedType.legacyWidget)
        expect(isInWidgetEmbedded()).to.be.false
      })
    })

    describe('ruleProvider', () => {
      test('should return rule provider', () => {
        setRuleProvider('rule-provider')
        expect(getRuleProvider()).to.eql('rule-provider')
      })
    })

    describe('asInputProps', () => {
      test('should return env as input props', () => {
        setBackendUrl('test')
        const expected = {
          backendUrl: 'test',
          businessUnit: undefined,
          appContext: {
            embedType: EmbedType.admin,
            widgetConfigKey: undefined
          }
        }
        expect(asInputProps()).to.deep.eq(expected)
      })
    })
  })
})
