import consoleLogger from '../consoleLogger'

import {getEmbedType, EmbedType} from './env'

export const embedStrategy = ({mapping}) => {
  const embedType = getEmbedType() || EmbedType.admin
  return mapping[embedType] || mapping[EmbedType.admin]
}

/**
 * Returns component based on current embedType and mapping.
 * @param {Object} config {mapping, propTypes}
 * @returns Component
 */
const embedStrategyFactory = config => {
  const {mapping, propTypes} = config

  const UIProvider = props => {
    const Comp = embedStrategy({mapping})
    if (!Comp) {
      consoleLogger.logError('Unable to render embed component: No embed component defined.')
    }

    return <Comp {...props} />
  }

  UIProvider.propTypes = {
    ...propTypes
  }

  return UIProvider
}

export default embedStrategyFactory
