import {embedStrategy} from './embedStrategyFactory'
import {EmbedType, setEmbedType} from './env'

describe('tocco-util', () => {
  describe('env', () => {
    describe('embedStrategyFactory', () => {
      describe('embedStrategy', () => {
        test('should return correct mapping based on embedType', () => {
          setEmbedType(EmbedType.legacyWidget)
          const mapping = {
            [EmbedType.legacyWidget]: 'abc'
          }

          const strategy = embedStrategy({mapping})

          expect(strategy).to.eql('abc')
        })

        test('should fallback to `admin` embedType', () => {
          setEmbedType(EmbedType.legacyWidget)
          const mapping = {
            [EmbedType.admin]: 'abc'
          }

          const strategy = embedStrategy({mapping})

          expect(strategy).to.eql('abc')
        })

        test('should return undefined when `admin` is not defined either', () => {
          setEmbedType(EmbedType.legacyWidget)
          const mapping = {
            [EmbedType.widget]: 'abc'
          }

          const strategy = embedStrategy({mapping})

          expect(strategy).to.be.undefined
        })
      })
    })
  })
})
