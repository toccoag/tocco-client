import {act, renderHook} from '@testing-library/react'

import useResize from './useResize'

describe('tocco-util', () => {
  describe('resize', () => {
    describe('useResize', () => {
      let clock

      const selector = () => ({clientWidth: 50, clientHeight: 50})
      const resizeCallback = sinon.spy()
      const resizeFinishedCallback = sinon.spy()

      const event = {stopPropagation: sinon.spy(), preventDefault: sinon.spy()}

      const startResize = result => {
        const element = '1'
        act(() => {
          result.current.startResize(element)(event)
        })
      }

      const moveMouse = (result, clientX, clientY) => {
        act(() => {
          result.current.resizingEvents.onMouseMove({clientX, clientY})
        })
      }

      const mouseUp = result => {
        act(() => {
          result.current.resizingEvents.onMouseUp()
        })
      }

      const mouseLeave = result => {
        act(() => {
          result.current.resizingEvents.onMouseLeave()
        })
      }

      beforeEach(() => {
        clock = sinon.useFakeTimers()

        resizeCallback.resetHistory()
        resizeFinishedCallback.resetHistory()
        event.stopPropagation.resetHistory()
        event.preventDefault.resetHistory()
      })

      afterEach(() => {
        clock.restore()
      })

      test('should return resizeState', () => {
        const expectedResizingState = {
          resizingElement: '1',
          isResizing: true
        }

        const {result} = renderHook(() => useResize(sinon.spy(), resizeCallback, resizeFinishedCallback))

        startResize(result)

        expect(result.current.resizeState).to.deep.equal(expectedResizingState)
      })

      test('should return register and remove mouse events and call callback', () => {
        const {result} = renderHook(() => useResize(selector, resizeCallback, resizeFinishedCallback))

        startResize(result)
        moveMouse(result, 100, 100)
        moveMouse(result, 200, 200)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback).to.have.been.calledWith('1', {width: 150, height: 150})

        mouseUp(result)

        expect(resizeFinishedCallback).to.have.been.calledWith('1')
      })

      test('should stop resizing immediately when mouse up event invoked', () => {
        const {result} = renderHook(() => useResize(selector, resizeCallback, resizeFinishedCallback))

        startResize(result)
        moveMouse(result, 100, 100)
        moveMouse(result, 200, 200)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback.called).to.equal(true)

        resizeCallback.resetHistory()

        mouseUp(result)
        moveMouse(result, 300, 300)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback.called).to.equal(false)
      })

      test('should stop resizing after `onClick` has been fired', () => {
        const {result} = renderHook(() => useResize(selector, resizeCallback, resizeFinishedCallback))

        startResize(result)
        moveMouse(result, 100, 100)
        moveMouse(result, 200, 200)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback.called).to.equal(true)

        resizeCallback.resetHistory()

        mouseUp(result)

        expect(result.current.resizeState.isResizing).to.equal(true)

        act(() => {
          clock.runAll()
        })

        expect(result.current.resizeState.isResizing).to.equal(false)
      })

      test('should stop resizing immediately when mouse leave event invoked', () => {
        const {result} = renderHook(() => useResize(selector, resizeCallback, resizeFinishedCallback))

        startResize(result)
        moveMouse(result, 100, 100)
        moveMouse(result, 200, 200)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback.called).to.equal(true)

        resizeCallback.resetHistory()

        mouseLeave(result)
        moveMouse(result, 300, 300)

        act(() => {
          clock.runToFrame()
        })

        expect(resizeCallback.called).to.equal(false)
      })
    })
  })
})
