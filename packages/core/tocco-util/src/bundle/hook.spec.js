import {screen, waitFor} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {useBundledApp} from './hook'
import utils from './utils'

let stub

const config = {packageName: 'package', appName: 'app'}
const TestApp = () => <div data-testid="app" />
const bundle = {app: {App: TestApp, setWebpacksPublicPath: sinon.spy()}}
const bundleName = `tocco-${config.appName}`

describe('tocco-util', () => {
  describe('bundle', () => {
    describe('hook', () => {
      describe('useBundledApp', () => {
        afterEach(() => {
          if (stub) {
            stub.restore()
          }

          delete window[bundleName]
        })

        test('should use bundle on window', async () => {
          window[bundleName] = bundle

          const TestComponent = () => {
            const App = useBundledApp(config)
            return App ? <App /> : <div data-testid="loading" />
          }

          testingLibrary.renderWithTheme(<TestComponent />)

          await waitFor(() => {
            expect(screen.queryByTestId('app')).to.exist
          })
        })

        test('should load bundle if not yet on window', async () => {
          stub = sinon.stub(utils, 'loadBundle').returns(bundle)

          const TestComponent = () => {
            const App = useBundledApp(config)
            return App ? <App /> : <div data-testid="loading" />
          }

          testingLibrary.renderWithTheme(<TestComponent />)

          await waitFor(() => {
            expect(screen.queryByTestId('app')).to.exist
          })
          expect(stub).has.been.calledWith(config.packageName, config.appName)
        })

        test('should return null if bundle is not loaded yet', async () => {
          const TestComponent = () => {
            const App = useBundledApp(config)
            return App === null ? <div data-testid="loading" /> : <div data-testid="failed" />
          }

          testingLibrary.renderWithTheme(<TestComponent />)

          await waitFor(() => {
            expect(screen.queryByTestId('loading')).to.exist
          })
        })

        test('should return error component if bundle could not beend fetched', async () => {
          stub = sinon.stub(utils, 'loadBundle').throwsException()

          const TestComponent = () => {
            const App = useBundledApp(config)
            return App ? <App /> : <div data-testid="loading" />
          }
          const resetDEVValue = __DEV__
          // eslint-disable-next-line no-global-assign
          __DEV__ = true
          testingLibrary.renderWithTheme(<TestComponent />)

          await waitFor(() => {
            expect(screen.queryByText('Fehler beim Laden der "app" App im Package "package".')).to.exist
          })
          // eslint-disable-next-line no-global-assign
          __DEV__ = resetDEVValue
        })
      })
    })
  })
})
