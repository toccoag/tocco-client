import {useState, useEffect} from 'react'

import remoteLogger from '../remoteLogger'

import ErrorComponent from './ErrorComponent'
import utils from './utils'

const useBundledApp = ({packageName, appName}) => {
  const [Comp, setComp] = useState(null)

  useEffect(() => {
    const load = async () => {
      try {
        // Normally a relative url is used to load the bundle. If a bundled app is used in a widget,
        // the relative url is the url of the cms (site.com) and not the c-name of tocco (tocco.site.com).
        // Use the backend url set from the bootstrap script for an absolute url.
        // Note: the widget-utils is not added as dependency only for using the constant BOOTSTRAP_SCRIPT_OBJ_NAME.
        const baseUrl = window.toccoBootstrap?.backendUrl || ''
        const bundle = await utils.loadBundle(packageName, appName, baseUrl)
        const {
          app: {App, setWebpacksPublicPath}
        } = bundle

        setWebpacksPublicPath(baseUrl + utils.getDistPath(packageName))
        setComp(() => App)
      } catch (error) {
        remoteLogger.logError(error)
        setComp(() => () => <ErrorComponent packageName={packageName} appName={appName} />)
      }
    }
    load()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return Comp
}

export {useBundledApp}
