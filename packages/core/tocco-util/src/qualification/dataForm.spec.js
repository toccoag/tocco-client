import {splitDataForm} from './dataForm'

describe('tocco-util', () => {
  describe('qualification', () => {
    describe('splitDataForm', () => {
      const firstFakeField = {
        children: [
          {
            path: 'first field'
          }
        ]
      }
      const secondFakeField = {
        children: [
          {
            path: 'second field'
          }
        ]
      }

      const fakeDataColumns = [firstFakeField, secondFakeField]

      const fakeAction = {
        id: 'action',
        componentType: 'action',
        scope: 'detail'
      }

      const fakeDataForm = {
        children: [
          {
            id: 'main-action-bar',
            label: null,
            componentType: 'action-bar',
            children: [
              {
                id: 'output',
                label: 'Ausgabe'
              },
              {
                id: 'nice2.reporting.actions.ChangelogExportAction',
                label: 'Changelog exportieren'
              },
              fakeAction
            ],
            actions: [],
            defaultAction: null
          },
          {
            componentType: 'table',
            children: fakeDataColumns
          }
        ]
      }

      test('should set action and column defintions', () => {
        const {actionDefinitions, dataFormColumns} = splitDataForm(fakeDataForm)
        expect(actionDefinitions).to.deep.include(fakeAction)
        expect(dataFormColumns).to.deep.include(firstFakeField)
        expect(dataFormColumns).to.deep.include(secondFakeField)
      })
      test('should hide actions when readonly', () => {
        const {actionDefinitions} = splitDataForm(fakeDataForm, true)
        expect(actionDefinitions).to.be.empty
      })
      test('should show readonly actions', () => {
        const {actionDefinitions} = splitDataForm(fakeDataForm, true, ['action'])
        expect(actionDefinitions).to.deep.include(fakeAction)
      })
      test('should hide hidden actions even when not readonly', () => {
        const {actionDefinitions} = splitDataForm(fakeDataForm, false, [], ['action'])
        expect(actionDefinitions).to.be.empty
      })
    })
  })
})
