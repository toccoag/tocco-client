/**
 * used in qualification actions to split actions from the data columns
 * @param {object} dataForm the form
 * @param {boolean} readonly true if the action is readonly
 * @param {array} readonlyActions the actions to show in readonly mode
 * @param {array} hiddenActions the actions that should be always hidden
 * @returns an object containing actionDefinitions and dataFormColumns
 */
export const splitDataForm = (dataForm, readonly = false, readonlyActions = [], hiddenActions = []) => {
  const actionDefinitions = dataForm.children
    .find(child => child.id === 'main-action-bar')
    .children.filter(child => child.componentType === 'action')
    .filter(child => !hiddenActions.includes(child.id))
    .filter(child => !readonly || readonlyActions.includes(child.id))
    .map(child => ({
      ...child,
      scope: 'detail'
    }))

  const dataFormColumns = dataForm.children.find(child => child.componentType === 'table').children

  return {actionDefinitions, dataFormColumns}
}
