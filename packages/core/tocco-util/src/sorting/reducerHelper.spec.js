import {setSorting} from './reducerHelper'

describe('tocco-util', () => {
  describe('sorting', () => {
    describe('reducerHelper', () => {
      describe('setSorting', () => {
        const getAction = (field, add) => ({payload: {field, add}})

        test('should set sorting', () => {
          const {sorting} = setSorting({sorting: []}, getAction('field'))
          expect(sorting).to.have.length(1)
          expect(sorting[0]).to.be.eql({field: 'field', order: 'asc'})
        })
        test('should invert sorting', () => {
          const {sorting} = setSorting({sorting: [{field: 'field', order: 'asc'}]}, getAction('field'))
          expect(sorting).to.have.length(1)
          expect(sorting[0]).to.be.eql({field: 'field', order: 'desc'})
        })
        test('should add sorting', () => {
          const {sorting} = setSorting({sorting: [{field: 'field'}]}, getAction('another', true))
          expect(sorting).to.have.length(2)
          expect(sorting[1]).to.be.eql({field: 'another', order: 'asc'})
        })
        test('should add inverted sorting', () => {
          const {sorting} = setSorting(
            {sorting: [{field: 'field'}, {field: 'another', order: 'asc'}]},
            getAction('another', true)
          )
          expect(sorting).to.have.length(2)
          expect(sorting[1]).to.be.eql({field: 'another', order: 'desc'})
        })
      })
    })
  })
})
