import {setSorting} from './reducerHelper'

export default {
  reducer: setSorting
}
