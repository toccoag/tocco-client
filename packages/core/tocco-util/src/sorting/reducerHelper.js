const defaultOrder = 'asc'
const getOpposite = order => (order === 'asc' ? 'desc' : 'asc')

/**
 * can be used as a reducer function when dealing with sortings
 * @param {object} state a state object containing a sorting array
 * @param {object} action an action containing a payload of a field name if we should add to or replace the sorting
 * @returns a new state object with the new sorting
 */
export const setSorting = (state, {payload: {field, add}}) => {
  if (!add) {
    const currentSorting = state.sorting
    const order =
      currentSorting.length > 0 && currentSorting[0].field === field
        ? getOpposite(currentSorting[0].order)
        : defaultOrder
    return {
      ...state,
      sorting: [{field, order}]
    }
  } else {
    const previousFieldSorting = state.sorting.find(s => s.field === field)
    const order = previousFieldSorting ? getOpposite(previousFieldSorting.order) : defaultOrder

    return {
      ...state,
      sorting: [...state.sorting.filter(s => s.field !== field), {field, order}]
    }
  }
}
