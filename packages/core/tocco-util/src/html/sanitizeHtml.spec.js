import sanitizeHtml from './sanitizeHtml'

const expectDirtyToBeCleaned = (dirty, clean) => {
  expect(sanitizeHtml(dirty)).to.equal(clean)
}

describe('tocco-util', () => {
  describe('html', () => {
    describe('sanitizeHtml', () => {
      test('should remove event handlers from html tags', () => {
        expectDirtyToBeCleaned('<img src="X" onLoad="alert(\'You have won a prize!\')">', '<img src="X">')
      })
      test('should close all open tags', () => {
        expectDirtyToBeCleaned('<div><span>Hallo', '<div><span>Hallo</span></div>')
      })
      test('should remove single closing tags', () => {
        expectDirtyToBeCleaned('</div>Hallo', 'Hallo')
      })
      test('should remove script tags', () => {
        expectDirtyToBeCleaned("<div>Hi!<script>alert('You have won a prize!')</script></div>", '<div>Hi!</div>')
      })
      test('should be able to handle null values', () => {
        expectDirtyToBeCleaned(null, null)
      })
      test('should be able to handle undefined values', () => {
        expectDirtyToBeCleaned(undefined, undefined)
      })
      test('should be able to handle empty strings', () => {
        expectDirtyToBeCleaned('', '')
      })
      test('should remove style tags', () => {
        expectDirtyToBeCleaned('<div>Hi!<style>a {border: 1px solid red;}</style></div>', '<div>Hi!</div>')
      })
      test('should remove style attributes', () => {
        expectDirtyToBeCleaned(
          '<div style="border:1px solid red;">Hi!</div>',
          '<div style="border:1px solid red;">Hi!</div>'
        )
      })
      test('should remove invalid style properties', () => {
        expectDirtyToBeCleaned(
          '<div style="border:1px solid red;position:absolute;">Hi!</div>',
          '<div style="border:1px solid red;">Hi!</div>'
        )
      })
      test('should remove style attributes when no style property is valid', () => {
        expectDirtyToBeCleaned('<div style="position:absolute;">Hi!</div>', '<div>Hi!</div>')
      })
      test('should remove css functions', () => {
        expectDirtyToBeCleaned('<div style="width:calc(100% - 50px);">Hi!</div>', '<div>Hi!</div>')
      })
      test('should ignore malformed css', () => {
        expectDirtyToBeCleaned(
          '<div style="border:1px;width:100%">Hi!</div>',
          '<div style="border:1px;width:100%;">Hi!</div>'
        )
      })
      test('should allow not whitelisted style properties for svg element', () => {
        const dirty = '<svg><line style="color: red;"></line></svg>'
        const clean = '<svg><line style="color: red;"></line></svg>'
        expect(sanitizeHtml(dirty)).to.equal(clean)
      })
      test('should allow target attribute on links', () => {
        expectDirtyToBeCleaned(
          '<a href="https://www.google.ch" target="_blank">Hi!</a>',
          '<a target="_blank" href="https://www.google.ch">Hi!</a>'
        )
      })
      test('should allow links without target attribute', () => {
        expectDirtyToBeCleaned('<a href="https://www.google.ch">Hi!</a>', '<a href="https://www.google.ch">Hi!</a>')
      })
    })
  })
})
