import {screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import PropTypes from 'prop-types'
import {useEffect, useState} from 'react'
import {testingLibrary} from 'tocco-test-util'

import {HorizontalDropPosition} from '../dragAndDrop/useDnD'

import {useColumnPosition} from './useColumnPosition'

const TestingComponent = ({columns: columnsProp, positionChanges = []}) => {
  const [columns, setColumns, onColumnPositionChange] = useColumnPosition()
  useEffect(() => {
    setColumns(columnsProp)
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  // got to wait until columns are set or last position change is done, easiest to just have button to do them
  const [positionChangeIndex, setPositionChangeIndex] = useState(0)
  const doPositionChange = () => {
    const {drag, drop, verticalDropPosition} = positionChanges[positionChangeIndex]
    onColumnPositionChange(drag, drop, verticalDropPosition)
    setPositionChangeIndex(positionChangeIndex + 1)
  }

  return (
    <>
      <span>{columns.map(column => column.id).join(' ')}</span>
      <button onClick={doPositionChange}>change position</button>
    </>
  )
}

TestingComponent.propTypes = {
  columns: PropTypes.array,
  positionChanges: PropTypes.arrayOf(PropTypes.shape({drag: PropTypes.string, drop: PropTypes.string}))
}

describe('tocco-util', () => {
  describe('table', () => {
    describe('useColumnPosition', () => {
      test('should return columns when set', () => {
        const expectedColumns = [{id: 'first'}, {id: 'second'}]
        testingLibrary.renderWithIntl(<TestingComponent columns={expectedColumns} />)
        expect(screen.queryByText('first second')).to.exist
      })

      test('should order columns on right side', async () => {
        const expectedColumns = [{id: 'first'}, {id: 'second'}, {id: 'third'}]

        const user = userEvent.setup()
        testingLibrary.renderWithIntl(
          <TestingComponent
            columns={expectedColumns}
            positionChanges={[{drag: 'third', drop: 'first', verticalDropPosition: HorizontalDropPosition.Right}]}
          />
        )
        const doPositionChange = screen.queryByRole('button')
        await user.click(doPositionChange)
        expect(screen.queryByText('first third second')).to.exist
      })

      test('should order columns on left side', async () => {
        const expectedColumns = [{id: 'first'}, {id: 'second'}, {id: 'third'}]

        const user = userEvent.setup()
        testingLibrary.renderWithIntl(
          <TestingComponent
            columns={expectedColumns}
            positionChanges={[{drag: 'third', drop: 'first', verticalDropPosition: HorizontalDropPosition.Left}]}
          />
        )
        const doPositionChange = screen.queryByRole('button')
        await user.click(doPositionChange)
        expect(screen.queryByText('third first second')).to.exist
      })

      test('should handle multiple drags', async () => {
        const expectedColumns = [{id: 'first'}, {id: 'second'}, {id: 'third'}]
        const user = userEvent.setup()
        testingLibrary.renderWithIntl(
          <TestingComponent
            columns={expectedColumns}
            positionChanges={[
              {drag: 'third', drop: 'first', verticalDropPosition: HorizontalDropPosition.Right},
              {drag: 'first', drop: 'second', verticalDropPosition: HorizontalDropPosition.Left}
            ]}
          />
        )
        const doPositionChange = screen.queryByRole('button')
        await user.click(doPositionChange)
        await user.click(doPositionChange)
        expect(screen.queryByText('third first second')).to.exist
      })
    })
  })
})
