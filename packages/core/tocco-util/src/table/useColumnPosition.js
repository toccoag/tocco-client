import {useEffect, useState, useCallback} from 'react'

import {HorizontalDropPosition} from '../dragAndDrop/useDnD'

/**
 * handles sorting columns of a table after dragging and dropping them
 * this does not save the order to preferences
 * the returned array contains in this order:
 * - an array containing the columns to pass to the table
 * - a function to set the columns (which will then be sorted)
 * - a function to pass to the table as onColumnPositionChange
 * @returns an array with columns, setColumns and onColumnPositionChange
 */
export const useColumnPosition = () => {
  const [columns, setColumns] = useState([])
  const [sortedColumns, setSortedColumns] = useState([])
  const [columnPositions, setColumnPositions] = useState([])

  useEffect(() => {
    const copiedColumns = [...columns] // do not sort original array
    setSortedColumns(
      copiedColumns.toSorted(
        (a, b) => columnPositions.findIndex(e => e === a.id) - columnPositions.findIndex(e => e === b.id)
      )
    )
  }, [columnPositions, columns, setSortedColumns])

  const onColumnPositionChange = useCallback(
    (dragColumn, dropColumn, horizontalDropPosition) => {
      const arrangeColumns = (drag, drop, position) =>
        position === HorizontalDropPosition.Right ? [drop, drag] : [drag, drop]
      setColumnPositions(
        sortedColumns.reduce(
          (acc, c) => [
            ...acc,
            ...(c.id === dropColumn ? arrangeColumns(dragColumn, c.id, horizontalDropPosition) : []),
            ...(c.id === dragColumn || c.id === dropColumn ? [] : [c.id])
          ],
          []
        )
      )
    },
    [sortedColumns]
  )

  return [sortedColumns, setColumns, onColumnPositionChange]
}
