import addToObjectReducer from './addToObjectReducer'
import mappingTransferReducer from './mappingTransferReducer'
import {injectReducers, hotReloadReducers} from './reducer'
import singleTransferReducer from './singleTransferReducer'
import toggleReducer from './toggleReducer'

export default {
  addToObjectReducer,
  singleTransferReducer,
  mappingTransferReducer,
  injectReducers,
  hotReloadReducers,
  toggleReducer
}
