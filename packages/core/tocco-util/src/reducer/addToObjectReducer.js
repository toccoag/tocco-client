import _set from 'lodash/set'

/**
 * reducer helper to add a key value pair to an object
 * @param {string} keyAttributeName name of the key attribute in the action payload
 * @param {string} valueAttributeName name of the value attribute in the action payload
 * @param {string} pathToObject (nested) path to the object in the state
 */
export default (keyAttributeName, valueAttributeName, pathToObject) => {
  return (state, {payload}) => {
    const key = payload[keyAttributeName]
    const value = payload[valueAttributeName]
    const newState = {...state}
    const newObjectState = {...state[pathToObject], [key]: value}
    _set(newState, pathToObject, newObjectState)
    return newState
  }
}
