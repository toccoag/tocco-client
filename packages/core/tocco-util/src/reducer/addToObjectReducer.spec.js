import addToObjectReducer from './addToObjectReducer'

describe('tocco-util', () => {
  describe('reducer', () => {
    describe('addToObjectReducer', () => {
      const actionCreator = (keyAttributeName, valueAttributeName) => (key, value) => ({
        type: 'SOMETHING',
        payload: {
          [keyAttributeName]: key,
          [valueAttributeName]: value
        }
      })
      const keyAttributeName = 'keyProperty'
      const valueAttributeName = 'valueProperty'

      test('should add a key value pair to the object in the state', () => {
        const initialState = {
          otherProperty: 'test',
          mapProperty: {
            Address: 'Address_code'
          }
        }

        const key = 'User'
        const newValue = 'User_code'

        const expectedState = {
          otherProperty: 'test',
          mapProperty: {
            Address: 'Address_code',
            User: 'User_code'
          }
        }

        const action = actionCreator(keyAttributeName, valueAttributeName)(key, newValue)
        expect(addToObjectReducer(keyAttributeName, valueAttributeName, 'mapProperty')(initialState, action)).to.eql(
          expectedState
        )
      })

      test('should override an existing key value pair in the object of the state', () => {
        const initialState = {
          otherProperty: 'test',
          mapProperty: {
            Address: 'Address_code',
            User: 'User_code'
          }
        }

        const key = 'Address'
        const newValue = 'New_code'

        const expectedState = {
          otherProperty: 'test',
          mapProperty: {
            Address: 'New_code',
            User: 'User_code'
          }
        }

        const action = actionCreator(keyAttributeName, valueAttributeName)(key, newValue)
        expect(addToObjectReducer(keyAttributeName, valueAttributeName, 'mapProperty')(initialState, action)).to.eql(
          expectedState
        )
      })

      test('should add a key value pair to a nested object in the state', () => {
        const initialState = {
          otherProperty: 'test',
          outer: {
            inner: {
              Address: 'Address_code'
            }
          }
        }

        const key = 'Address'
        const newValue = 'New_code'

        const expectedState = {
          otherProperty: 'test',
          outer: {
            inner: {
              Address: 'New_code'
            }
          }
        }

        const action = actionCreator(keyAttributeName, valueAttributeName)(key, newValue)
        expect(addToObjectReducer(keyAttributeName, valueAttributeName, 'outer.inner')(initialState, action)).to.eql(
          expectedState
        )
      })
    })
  })
})
