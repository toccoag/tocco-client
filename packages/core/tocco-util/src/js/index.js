import {getOrFirst, roundDecimalPlaces, copyToClipboard, isDefined} from './helpers'
import {adjustedHTMLString} from './html'
import {difference} from './object'

export default {difference, getOrFirst, adjustedHTMLString, roundDecimalPlaces, copyToClipboard, isDefined}
