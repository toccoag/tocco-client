import {isPrimaryTouchDevice} from './device'
import {isSafari} from './userAgent'

export default {isSafari, isPrimaryTouchDevice}
