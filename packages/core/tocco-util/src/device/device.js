/**
 * Returns `true` if the primary input type is touch instead of mouse or stylus.
 *
 * Is `isPrimaryTouchDevice` is returning `false` for laptops with touch capabilities.
 * @returns boolean
 */
export const isPrimaryTouchDevice = () => 'matchMedia' in window && window.matchMedia('(pointer: coarse)').matches
