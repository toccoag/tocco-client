import {act, screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import asyncRoute from './asyncRoute'

describe('tocco-util', () => {
  describe('route', () => {
    describe('asyncRoute', () => {
      const InnerComp = () => <div data-testid="component">test</div>

      test('should only render component when loaded', async () => {
        const promise = Promise.resolve(InnerComp)

        const AsyncComp = asyncRoute(() => promise)
        const {rerender} = testingLibrary.renderWithTheme(<AsyncComp />)

        expect(screen.queryByTestId('component')).to.not.exist

        await act(async () => {
          await promise
        })

        rerender(<AsyncComp />)

        expect(screen.queryByTestId('component')).to.exist
      })
    })
  })
})
