import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useInRouterContext, useLocation} from 'react-router-dom'

import react from '../react'

const InvokeOnLocationChange = ({onLocationChange}) => {
  const location = useLocation()
  const prevPathname = react.usePrevious(location.pathname)

  useEffect(() => {
    if (location.pathname !== prevPathname && prevPathname) {
      onLocationChange()
    }
  }, [location.pathname, prevPathname, onLocationChange])

  return null
}

InvokeOnLocationChange.propTypes = {
  onLocationChange: PropTypes.func
}

/**
 * Only access `useLocation` when router provider is available
 *  - some widgets to not have a router (e.g. membership registration)
 *  - core components cannot guarantee the existence of the router provider
 */
const InvokeOnLocationChangeWrapper = ({onLocationChange}) => {
  const inRouterContext = useInRouterContext()
  return <>{inRouterContext && <InvokeOnLocationChange onLocationChange={onLocationChange} />}</>
}
InvokeOnLocationChangeWrapper.propTypes = {
  onLocationChange: PropTypes.func
}

export default InvokeOnLocationChangeWrapper
