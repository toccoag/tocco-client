import {screen, waitFor} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {loadRoute} from './route'

describe('tocco-util', () => {
  describe('route', () => {
    describe('route', () => {
      describe('loadRoute', () => {
        const InnerComp = () => <div data-testid="comp1">test</div>
        const InnerComp2 = () => <div data-testid="comp2">test</div>

        test('should load and render component', async () => {
          const promise = Promise.resolve({
            default: {
              container: InnerComp
            }
          })

          const Cmp = loadRoute(null, {}, () => promise)

          testingLibrary.renderWithTheme(<Cmp />)

          await waitFor(() => {
            expect(screen.queryByTestId('comp1')).to.exist
          })
        })

        test('should return component class from cache if loaded with key', async () => {
          const promise1 = Promise.resolve({
            default: {container: InnerComp}
          })
          const promise2 = Promise.resolve({
            default: {container: InnerComp2}
          })

          const Cmp = loadRoute(null, {}, () => promise1, 'my-route')
          const Cmp2 = loadRoute(null, {}, () => promise2, 'my-route')

          testingLibrary.renderWithTheme(
            <div>
              <Cmp />
              <Cmp2 />
            </div>
          )

          await waitFor(() => {
            expect(screen.queryAllByTestId('comp1')).to.have.length(2)
            expect(screen.queryByTestId('comp2')).to.not.exist
          })
        })

        test('should not return component class from cache if loaded without key', async () => {
          const promise1 = Promise.resolve({
            default: {container: InnerComp}
          })
          const promise2 = Promise.resolve({
            default: {container: InnerComp2}
          })

          const Cmp = loadRoute(null, {}, () => promise1)
          const Cmp2 = loadRoute(null, {}, () => promise2)

          testingLibrary.renderWithTheme(
            <div>
              <Cmp />
              <Cmp2 />
            </div>
          )

          await waitFor(() => {
            expect(screen.queryByTestId('comp1')).to.exist
            expect(screen.queryByTestId('comp2')).to.exist
          })
        })

        test('should not return component class from cache if loaded with different key', async () => {
          const promise1 = Promise.resolve({
            default: {container: InnerComp}
          })
          const promise2 = Promise.resolve({
            default: {container: InnerComp2}
          })

          const Cmp = loadRoute(null, {}, () => promise1, 'my-route-1')
          const Cmp2 = loadRoute(null, {}, () => promise2, 'my-route-2')

          testingLibrary.renderWithTheme(
            <div>
              <Cmp />
              <Cmp2 />
            </div>
          )

          await waitFor(() => {
            expect(screen.queryByTestId('comp1')).to.exist
            expect(screen.queryByTestId('comp2')).to.exist
          })
        })
      })
    })
  })
})
