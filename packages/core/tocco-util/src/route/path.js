import {match} from 'path-to-regexp'

export const extractParamsFromPath = (pathRegex, path) => {
  const fn = match(pathRegex)
  const res = fn(path)

  return res?.params || null
}
