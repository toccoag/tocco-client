import {fireEvent, screen} from '@testing-library/dom'
import {Link, MemoryRouter, Route, Routes} from 'react-router-dom'
import {testingLibrary} from 'tocco-test-util'

import InvokeOnLocationChange from './InvokeOnLocationChange'

describe('tocco-util', () => {
  describe('route', () => {
    describe('InvokeOnLocationChange', () => {
      test('should ignore if no route provider is available', () => {
        const onLocationChangeSpy = sinon.spy()
        testingLibrary.renderWithIntl(<InvokeOnLocationChange onLocationChange={onLocationChangeSpy} />)

        expect(true).to.be.true // no render failure
      })

      test('should invoke callback on location change', () => {
        const onLocationChangeSpy = sinon.spy()
        testingLibrary.renderWithIntl(
          <MemoryRouter initialEntries={['/b']}>
            <InvokeOnLocationChange onLocationChange={onLocationChangeSpy} />
            <Routes>
              <Route path="a" element={<div data-testid="route-a"></div>} />
              <Route
                path="b"
                element={
                  <div data-testid="route-b">
                    <Link to="/a" data-testid="link">
                      Prev
                    </Link>
                  </div>
                }
              />
            </Routes>
          </MemoryRouter>
        )
        expect(screen.queryAllByTestId('route-b')).to.have.length(1)
        expect(screen.queryAllByTestId('link')).to.have.length(1)

        fireEvent.click(screen.getByTestId('link'))

        expect(onLocationChangeSpy).to.have.been.calledOnce
        expect(screen.queryAllByTestId('route-a')).to.have.length(1)
      })
    })
  })
})
