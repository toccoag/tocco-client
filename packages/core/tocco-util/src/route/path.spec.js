import {extractParamsFromPath} from './path'

describe('tocco-util', () => {
  describe('route', () => {
    describe('path', () => {
      describe('extractParamsFromPath', () => {
        test('should extract multiple params', () => {
          const pathRegex = '/em{/:entityModel}*/model'
          const path = '/em/User/Business_unit/model'
          expect(extractParamsFromPath(pathRegex, path)).to.eql({entityModel: ['User', 'Business_unit']})
        })

        test('should ignore optional param', () => {
          const pathRegex = '/history/:model{/:key}?'
          const path = '/history/Widget_config'
          expect(extractParamsFromPath(pathRegex, path)).to.eql({model: 'Widget_config'})
        })

        test('should find optional param', () => {
          const pathRegex = '/history/:model{/:key}?'
          const path = '/history/Widget_config/123'
          expect(extractParamsFromPath(pathRegex, path)).to.eql({model: 'Widget_config', key: '123'})
        })

        test('should handle complex path', () => {
          const pathRegex =
            '/e/:entity{/:key}?{/:relation}*/:view(list|detail|edit|create|relations|action){/:actionId}?'
          expect(extractParamsFromPath(pathRegex, '/e/User/123/detail')).to.eql({
            entity: 'User',
            key: '123',
            view: 'detail'
          })
          expect(extractParamsFromPath(pathRegex, '/e/User/123/relBusiness_unit/list')).to.eql({
            entity: 'User',
            key: '123',
            relation: ['relBusiness_unit'],
            view: 'list'
          })
          expect(extractParamsFromPath(pathRegex, '/e/User/123/relBusiness_unit/action/fullscreen-action')).to.eql({
            entity: 'User',
            key: '123',
            relation: ['relBusiness_unit'],
            view: 'action',
            actionId: 'fullscreen-action'
          })
        })
      })
    })
  })
})
