import InvokeOnLocationChange from './InvokeOnLocationChange'
import {extractParamsFromPath} from './path'
import {loadRoute} from './route'

export default {
  loadRoute,
  extractParamsFromPath,
  InvokeOnLocationChange
}
