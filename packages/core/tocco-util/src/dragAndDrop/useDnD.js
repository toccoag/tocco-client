import {isEqual} from 'lodash'
import {useCallback, useMemo, useState} from 'react'
import {flushSync} from 'react-dom'
import {css} from 'styled-components'

const initialState = {
  currentlyDragging: null,
  currentlyDragOver: null,
  horizontalDropPosition: null,
  verticalDropPosition: null,
  offsetX: null,
  offsetY: null
}

export const HorizontalDropPosition = {
  Left: 'Left',
  Right: 'Right'
}

export const VerticalDropPosition = {
  Top: 'Top',
  Bottom: 'Bottom'
}

const calculateDropPosition = (e, offsetX, offsetY) => {
  const rightFromOffset = e.clientX - offsetX > 0
  const belowFromOffset = e.clientY - offsetY > 0
  const horizontal = rightFromOffset ? HorizontalDropPosition.Right : HorizontalDropPosition.Left
  const vertical = belowFromOffset ? VerticalDropPosition.Bottom : VerticalDropPosition.Top
  return {horizontal, vertical}
}

export default changePosition => {
  const [state, setState] = useState(initialState)
  const {currentlyDragging, currentlyDragOver, horizontalDropPosition, verticalDropPosition, offsetX, offsetY} = state

  const events = useCallback(
    item => ({
      onDragStart: e => {
        setState(currentState => ({...currentState, currentlyDragging: item}))

        /**
         * Workaround: https://github.com/facebook/react/issues/1355
         * dragEnd-event is not fired in React for already removed components.
         * That's why we need to add the event listener for this event on the dom node directly.
         */
        const draggableElement = e.target
        const onDragEnd = dragEndEvent => {
          setState(initialState)
          dragEndEvent.stopPropagation()
          if (draggableElement) {
            draggableElement.removeEventListener('dragend', onDragEnd)
          }
        }
        if (draggableElement) {
          draggableElement.addEventListener('dragend', onDragEnd, false)
        }

        e.stopPropagation()
      },
      onDragEnter: e => {
        if (currentlyDragging && !isEqual(currentlyDragOver, item)) {
          const bounding = e.target ? e.target.getBoundingClientRect() : {}
          const updatedOffsetX = bounding.x + bounding.width / 2
          const updatedOffsetY = bounding.y + bounding.height / 2
          const {horizontal, vertical} = calculateDropPosition(e, updatedOffsetX, updatedOffsetY)
          /**
           * `flushSync` is needed so that the follow up `onDragLeave` event gets the latest state.
           * Otherwise the leave event resets the state directly after the enter event "moved" to the new item.
           * This results that the ability to drop always skips one drop item in between.
           */
          flushSync(() => {
            setState(currentState => ({
              ...currentState,
              currentlyDragOver: item,
              offsetX: updatedOffsetX,
              offsetY: updatedOffsetY,
              horizontalDropPosition: horizontal,
              verticalDropPosition: vertical
            }))
          })
        }
        e.stopPropagation()
      },
      onDragOver: e => {
        if (currentlyDragging && isEqual(currentlyDragOver, item)) {
          const {horizontal, vertical} = calculateDropPosition(e, offsetX, offsetY)
          setState(currentState => ({
            ...currentState,
            horizontalDropPosition: horizontal,
            verticalDropPosition: vertical
          }))
          e.preventDefault()
          e.stopPropagation()
          return true
        }
      },
      onDragLeave: e => {
        if (currentlyDragging && isEqual(currentlyDragOver, item)) {
          e.stopPropagation()
          setState(currentState => ({
            ...currentState,
            currentlyDragOver: null,
            horizontalDropPosition: null,
            verticalDropPosition: null
          }))
        }
      },
      onDrop: e => {
        if (currentlyDragging && currentlyDragOver) {
          changePosition(currentlyDragging, currentlyDragOver, horizontalDropPosition, verticalDropPosition)
          setState(initialState)
          e.stopPropagation()
        }
      }
    }),
    [
      currentlyDragOver,
      currentlyDragging,
      horizontalDropPosition,
      verticalDropPosition,
      offsetX,
      offsetY,
      changePosition
    ]
  )

  // so that state object is not changing unnecessary although no value on object has changed
  const dndState = useMemo(
    () => ({
      currentlyDragOver,
      currentlyDragging,
      horizontalDropPosition,
      verticalDropPosition,
      offsetX,
      offsetY
    }),
    [currentlyDragOver, currentlyDragging, horizontalDropPosition, verticalDropPosition, offsetX, offsetY]
  )

  // Prevent nested DOMs to trigger onDropLeave event.
  const isDragging = Boolean(dndState.currentlyDragging)
  const dndCss = isDragging
    ? css`
        & * {
          pointer-events: none;
        }
      `
    : ''

  return {dndEvents: events, dndState, dndCss}
}
