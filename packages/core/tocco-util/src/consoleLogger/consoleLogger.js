/* eslint no-console: 0 */

export const logError = (...args) => {
  if (window.console) {
    const logFn = console.error || console.log
    logFn(...args)
  }
}

export const logWarning = (...args) => {
  if (window.console) {
    const logFn = console.warn || console.log
    logFn(...args)
  }
}

export const log = (...args) => {
  if (window.console) {
    console.log(...args)
  }
}
