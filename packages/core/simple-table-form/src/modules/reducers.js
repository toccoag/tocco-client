import simpleTableFormReducer from './simpleTableForm/reducer'
import simpleTableFormSagas from './simpleTableForm/sagas'

export default {
  simpleTableForm: simpleTableFormReducer
}

export const sagas = [simpleTableFormSagas]
