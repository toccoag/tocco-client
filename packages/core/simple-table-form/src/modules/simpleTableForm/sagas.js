import {all, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'
import {getIsValid} from './selectors'

export const inputSelector = state => state.input
export const simpleTableFormSelector = state => state.simpleTableForm

export default function* sagas() {
  yield all([takeEvery(actions.SET_VALUE, handleChange), takeLatest(actions.SET_ERRORS, handleValidate)])
}

export function* handleChange({payload}) {
  yield put(externalEvents.fireExternalEvent('onChange', {...payload}))
}

export function* handleValidate() {
  const {errors} = yield select(simpleTableFormSelector)
  const isValid = yield select(getIsValid)
  yield put(externalEvents.fireExternalEvent('onValidate', {isValid, errors}))
}
