export const SET_VALUE = 'simpleTableForm/SET_VALUE'
export const SET_ERRORS = 'simpleTableForm/SET_ERRORS'
export const SET_SORTING = 'simpleTableForm/SET_SORTING'
export const SET_DATA_LOADING_IN_PROGRESS = 'simpleTableForm/SET_DATA_LOADING_IN_PROGRESS'

export const setValue = (key, field, value) => ({
  type: SET_VALUE,
  payload: {key, field, value}
})

export const setErrors = (key, errors) => ({
  type: SET_ERRORS,
  payload: {key, errors}
})

export const setSorting = (field, add) => ({
  type: SET_SORTING,
  payload: {field, add}
})

export const setDataLoadingInProgress = dataLoadingInProgress => ({
  type: SET_DATA_LOADING_IN_PROGRESS,
  payload: {dataLoadingInProgress}
})
