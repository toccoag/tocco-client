import {reducer as reducerUtil, sorting} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  errors: {},
  dataLoadingInProgress: false,
  sorting: []
}

const setErrors = (state, {payload: {key, errors}}) => ({...state, errors: {...state.errors, [key]: errors}})

const ACTION_HANDLERS = {
  [actions.SET_ERRORS]: setErrors,
  [actions.SET_DATA_LOADING_IN_PROGRESS]: reducerUtil.singleTransferReducer('dataLoadingInProgress'),
  [actions.SET_SORTING]: sorting.reducer
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
