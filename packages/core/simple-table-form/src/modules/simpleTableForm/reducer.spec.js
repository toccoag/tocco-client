import * as actions from './actions'
import reducer from './reducer'

describe('simple-table-from', () => {
  describe('modules', () => {
    describe('simpleTableForm', () => {
      describe('reducer', () => {
        describe('SET_ERRORS', () => {
          test('should set error per row', () => {
            const stateBefore = {
              errors: {}
            }

            const expectedStateAfter = {
              errors: {
                row1: {error: true}
              }
            }

            expect(reducer(stateBefore, actions.setErrors('row1', {error: true}))).to.deep.equal(expectedStateAfter)
          })

          test('should keep errors from other rows', () => {
            const stateBefore = {
              errors: {
                row2: {anyError: true}
              }
            }

            const expectedStateAfter = {
              errors: {
                row2: {anyError: true},
                row1: {error: true}
              }
            }

            expect(reducer(stateBefore, actions.setErrors('row1', {error: true}))).to.deep.equal(expectedStateAfter)
          })

          test('should empty errors ', () => {
            const stateBefore = {
              errors: {
                row1: {error: true}
              }
            }

            const expectedStateAfter = {
              errors: {
                row1: {}
              }
            }

            expect(reducer(stateBefore, actions.setErrors('row1', {}))).to.deep.equal(expectedStateAfter)
          })
        })
      })
    })
  })
})
