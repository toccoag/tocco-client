import {all, select, takeLatest, takeEvery} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import {externalEvents} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'
import {getIsValid} from './selectors'

describe('simple-table-form', () => {
  describe('modules', () => {
    describe('simpleTableForm', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeEvery(actions.SET_VALUE, sagas.handleChange),
                takeLatest(actions.SET_ERRORS, sagas.handleValidate)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('handleChange', () => {
          test('should fire external event', () => {
            return expectSaga(sagas.handleChange, actions.setValue('key', 'column', 'value'))
              .put(externalEvents.fireExternalEvent('onChange', {key: 'key', field: 'column', value: 'value'}))
              .run()
          })
        })

        describe('handleValidate', () => {
          test('should fire external event for valid form', () => {
            return expectSaga(sagas.handleValidate)
              .provide([
                [select(sagas.simpleTableFormSelector), {errors: {}}],
                [select(getIsValid), true]
              ])
              .put(externalEvents.fireExternalEvent('onValidate', {errors: {}, isValid: true}))
              .run()
          })

          test('should fire external event for invalid form', () => {
            return expectSaga(sagas.handleValidate)
              .provide([
                [select(sagas.simpleTableFormSelector), {errors: {key: {mandatory: true}}}],
                [select(getIsValid), false]
              ])
              .put(externalEvents.fireExternalEvent('onValidate', {errors: {key: {mandatory: true}}, isValid: false}))
              .run()
          })
        })
      })
    })
  })
})
