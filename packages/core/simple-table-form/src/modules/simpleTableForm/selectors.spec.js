import {getDataWithErrors, getIsValid} from './selectors'

describe('simple-table-form', () => {
  describe('modules', () => {
    describe('simpleTableForm', () => {
      describe('selectors', () => {
        describe('getIsValid', () => {
          test('should be valid for initial state', () => {
            const state = {
              simpleTableForm: {
                errors: {}
              }
            }
            expect(getIsValid(state)).to.eql(true)
          })

          test('should be valid for empty row errors', () => {
            const state = {
              simpleTableForm: {
                errors: {
                  row1: {},
                  row2: {}
                }
              }
            }
            expect(getIsValid(state)).to.eql(true)
          })

          test('should not be valid for row errors', () => {
            const state = {
              simpleTableForm: {
                errors: {
                  row1: {mandatory: true},
                  row2: {}
                }
              }
            }
            expect(getIsValid(state)).to.eql(false)
          })
        })

        describe('getDataWithErrors', () => {
          test('should apply row errors per row', () => {
            const state = {
              simpleTableForm: {
                errors: {
                  row1: {mandatory: true},
                  row2: {}
                }
              },
              input: {
                data: [{__key: 'row1'}, {__key: 'row2'}, {__key: 'row3'}]
              }
            }

            const expectedData = [
              {__key: 'row1', errors: {mandatory: true}},
              {__key: 'row2', errors: {}},
              {__key: 'row3', errors: undefined}
            ]
            expect(getDataWithErrors(state)).to.eql(expectedData)
          })
        })
      })
    })
  })
})
