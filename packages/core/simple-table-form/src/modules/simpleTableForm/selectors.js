import _isEmpty from 'lodash/isEmpty'

export const getIsValid = state => {
  const errors = state.simpleTableForm.errors
  return Object.keys(errors).every(k => _isEmpty(errors[k]))
}

export const getDataWithErrors = state => {
  const errors = state.simpleTableForm.errors
  const data = state.input.data

  return data.map(row => ({
    ...row,
    errors: errors[row.__key]
  }))
}
