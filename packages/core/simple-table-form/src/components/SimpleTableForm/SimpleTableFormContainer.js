import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setValue, setErrors, setDataLoadingInProgress, setSorting} from '../../modules/simpleTableForm/actions'
import {getDataWithErrors} from '../../modules/simpleTableForm/selectors'

import SimpleTableForm from './SimpleTableForm'

const mapActionCreators = {
  setValue,
  setErrors,
  setDataLoadingInProgress,
  setSorting
}

const mapStateToProps = state => ({
  disabled: state.input.disabled,
  formDefinition: state.input.formDefinition,
  entityModel: state.input.entityModel,
  customButtons: state.input.customButtons,
  cellRenderers: state.input.cellRenderers,
  data: getDataWithErrors(state),
  onLoadData: state.input.onLoadData,
  recordsPerPage: state.input.recordsPerPage,
  totalCount: state.input.totalCount,
  scrollBehaviour: state.input.scrollBehaviour,
  dataLoadingInProgress: state.simpleTableForm.dataLoadingInProgress,
  sorting: state.simpleTableForm.sorting
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(SimpleTableForm))
