import PropTypes from 'prop-types'
import {useCallback, useEffect, useState} from 'react'
import {form} from 'tocco-app-extensions'
import {Table, scrollBehaviourPropType} from 'tocco-ui'
import {table, consoleLogger} from 'tocco-util'

import {getColumnDefinition} from '../../utils/table'

const SimpleTableForm = ({
  disabled,
  formDefinition,
  entityModel,
  data,
  setValue,
  setErrors,
  cellRenderers,
  customButtons,
  dataLoadingInProgress,
  onLoadData,
  recordsPerPage,
  totalCount,
  sorting,
  setSorting,
  setDataLoadingInProgress,
  scrollBehaviour
}) => {
  const tableFormDefinition = formDefinition.children.find(child => child.componentType === 'table')
  if (!tableFormDefinition) {
    consoleLogger.error('no table element found in form definition')
  }

  const handleSyncValidate = form.hooks.useSyncValidation({
    fieldDefinitions: form.getFieldDefinitions(formDefinition),
    formDefinition
  })

  const validate = useCallback(
    rowData => {
      const updatedErrors = handleSyncValidate(rowData)
      setErrors(rowData.__key, updatedErrors)
    },
    [handleSyncValidate, setErrors]
  )

  const [columnWidths, setColumnWidths] = useState({})
  const [currentPage, setCurrentPage] = useState(1)

  const [columns, setColumns, onColumnPositionChange] = table.useColumnPosition()

  useEffect(() => {
    setColumns(
      getColumnDefinition({
        table: tableFormDefinition,
        customButtons: customButtons || {},
        cellRenderers: cellRenderers || {},
        entityModel,
        validate,
        setValue,
        disabled,
        columnWidths,
        sorting
      })
    )
  }, [
    disabled,
    tableFormDefinition,
    entityModel,
    customButtons,
    cellRenderers,
    validate,
    setValue,
    setColumns,
    columnWidths,
    sorting
  ])

  const onColumnWidthChange = useCallback(
    (id, width) => {
      setColumnWidths(previousWidths => ({...previousWidths, [id]: width}))
    },
    [setColumnWidths]
  )

  useEffect(() => {
    if (onLoadData) {
      setDataLoadingInProgress(true)
      onLoadData({sorting, page: currentPage, finishedLoading: () => setDataLoadingInProgress(false)})
    }
  }, [sorting, currentPage, onLoadData, setDataLoadingInProgress])

  return (
    <Table
      columns={columns}
      data={data}
      selectionStyle="none"
      onColumnPositionChange={onColumnPositionChange}
      disableVirtualTable={true}
      onSortingChange={setSorting}
      dataLoadingInProgress={dataLoadingInProgress}
      onColumnWidthChange={onColumnWidthChange}
      {...(scrollBehaviour ? {scrollBehaviour} : {})} // only add if explicitly set - otherwise take default from table
      {...(recordsPerPage && totalCount
        ? {
            onPageChange: setCurrentPage,
            paginationInfo: {
              currentPage,
              recordsPerPage,
              totalCount
            }
          }
        : {})}
    />
  )
}

SimpleTableForm.propTypes = {
  disabled: PropTypes.bool,
  entityModel: PropTypes.string,
  formDefinition: PropTypes.object.isRequired,
  customButtons: PropTypes.object,
  cellRenderers: PropTypes.object,
  data: PropTypes.array.isRequired,
  setValue: PropTypes.func.isRequired,
  setErrors: PropTypes.func.isRequired,
  onLoadData: PropTypes.func,
  recordsPerPage: PropTypes.number,
  totalCount: PropTypes.number,
  sorting: PropTypes.array,
  setSorting: PropTypes.func,
  dataLoadingInProgress: PropTypes.bool,
  setDataLoadingInProgress: PropTypes.func,
  scrollBehaviour: scrollBehaviourPropType
}

export default SimpleTableForm
