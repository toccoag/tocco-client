import {configureStore} from '@reduxjs/toolkit'
import {act, screen} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import PropTypes from 'prop-types'
import {form} from 'tocco-app-extensions'
import {testingLibrary} from 'tocco-test-util'

import ResizeObserver from '../../../../../../__mocks__/ResizeObserver'

import SimpleTableForm from './SimpleTableForm'

const defaultFormDefinition = form.createSimpleForm({
  children: [
    form.createTable({
      children: [
        form.createColumn({
          label: 'Column 1',
          id: 'col1',
          dataType: 'string'
        }),
        form.createColumn({
          label: 'Column 2',
          id: 'col2',
          dataType: 'string',
          clientRenderer: 'col'
        }),
        form.createColumn({
          label: 'Column 3',
          id: 'col3',
          dataType: 'string',
          readonly: false,
          validation: form.createValidation(form.createMandatoryValidation())
        }),
        form.createColumn({
          label: 'Column 4',
          id: 'col4',
          dataType: 'action',
          children: [
            form.createActionCell({
              id: 'action',
              appId: 'action',
              icon: 'bolt',
              label: 'action'
            })
          ]
        })
      ]
    })
  ]
})

const ColRenderer = ({rowData, column}) => <div data-testid={`col-renderer-${rowData.__key}`}>{rowData[column.id]}</div>
ColRenderer.propTypes = {
  rowData: PropTypes.object,
  column: PropTypes.object
}

const defaultCellRenderers = {
  col: props => <ColRenderer {...props} />
}

describe('simple-table-form', () => {
  describe('components', () => {
    describe('SimpleTableForm', () => {
      beforeEach(() => {
        window.ResizeObserver = ResizeObserver
        jest.useFakeTimers()
      })
      afterEach(() => {
        jest.useRealTimers()
      })

      const defaultData = [
        {__key: 'row1', col1: 'row1Col1', col2: 'row1Col2', col3: 'row1Col3'},
        {__key: 'row2', col1: 'row2Col1', col2: 'row2Col2', col3: 'row2Col3'}
      ]
      const defaultProps = {
        disabled: false,
        formDefinition: defaultFormDefinition,
        data: defaultData,
        setValue: sinon.spy(),
        setErrors: sinon.spy(),
        cellRenderers: defaultCellRenderers,
        entityModel: 'Some_model'
      }
      const store = configureStore({
        reducer: () => ({
          actions: {}
        })
      })

      test('should render table with custom cell renderer', async () => {
        testingLibrary.renderWithStore(<SimpleTableForm {...defaultProps} />, {store})
        await screen.findAllByTestId('icon')

        expect(screen.getByText('Column 1')).to.exist
        expect(screen.getByText('Column 2')).to.exist
        expect(screen.getByText('Column 3')).to.exist
        expect(screen.getByText('Column 4')).to.exist

        expect(screen.getByText('row1Col1')).to.exist
        expect(screen.getByText('row1Col2')).to.exist
        expect(screen.getByDisplayValue('row1Col3')).to.exist
        expect(screen.getByText('row2Col1')).to.exist
        expect(screen.getByText('row2Col2')).to.exist
        expect(screen.getByDisplayValue('row2Col3')).to.exist

        expect(screen.getByTestId('col-renderer-row1')).to.exist
        expect(screen.getByTestId('col-renderer-row2')).to.exist
      })

      test('should invoke set value on change', async () => {
        const setValue = sinon.spy()
        testingLibrary.renderWithStore(<SimpleTableForm {...defaultProps} setValue={setValue} />, {store})
        await screen.findAllByTestId('icon')

        const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})

        await user.type(screen.getByDisplayValue('row1Col3'), 'abc')
        act(() => {
          jest.runAllTimers()
        })

        expect(setValue).to.have.been.calledWith('row1', 'col3', 'abc')
      })

      test('should invoke set error on validate', async () => {
        const setErrors = sinon.spy()
        testingLibrary.renderWithStore(
          <SimpleTableForm
            {...defaultProps}
            data={[{__key: 'row1', col1: 'row1Col1', col2: 'row1Col2', col3: ''}]}
            setErrors={setErrors}
          />,
          {store}
        )
        await screen.findAllByTestId('icon')

        const user = userEvent.setup({advanceTimers: jest.advanceTimersByTime})

        await user.click(screen.getByRole('textbox'))
        await user.tab()

        expect(setErrors).to.have.been.calledWith('row1', sinon.match({col3: {mandatory: sinon.match.any}}))
      })
    })
  })
})
