import PropTypes from 'prop-types'
import {actionEmitter, actions, appFactory, externalEvents} from 'tocco-app-extensions'
import {scrollBehaviourPropType} from 'tocco-ui'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import SimpleTableForm from './components/SimpleTableForm'
import reducers, {sagas} from './modules/reducers'

const packageName = 'simple-table-form'
const EXTERNAL_EVENTS = [
  'emitAction',
  /**
   * Fired on any value change
   *
   * Payload:
   * - `key`: row `__key`
   * - `field` column id
   * - `value`new value
   */
  'onChange',
  /**
   * Is fired when form has been validated (e.g. on blur)
   *
   * Payload:
   * - `isValid` (boolean)
   * - `errors` (object with errors per row)
   */
  'onValidate'
]

const initApp = (id, input, events, publicPath) => {
  const content = <SimpleTableForm />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  actionEmitter.addToStore(store, state => state.input.emitAction)
  actions.addToStore(store, state => ({
    appComponent: actions.actionFactory({}),
    customActions: {
      ...(state.input.customActions || {})
    }
  }))
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const SimpleTableFormApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

SimpleTableFormApp.propTypes = {
  /**
   * Whether form should be disabled (e.g. during submission) (Default: `false`)
   */
  disabled: PropTypes.bool,
  /**
   * Full mandatory table form definition
   */
  formDefinition: PropTypes.object.isRequired,
  /**
   * Entity model if available for actions selection context
   */
  entityModel: PropTypes.string,
  /**
   * Provide custom cell renderers for  columns with `clientRenderer`
   */
  cellRenderers: PropTypes.object,
  /**
   * Provide action functions for action cells
   */
  customActions: PropTypes.object,
  /**
   * Provides onClick function for button cells
   */
  customButtons: PropTypes.object,
  /**
   * Trigger load data on page changes
   */
  onLoadData: PropTypes.func,
  /**
   * Number of rows per page
   */
  recordsPerPage: PropTypes.number,
  /**
   * Total number of rows
   */
  totalCount: PropTypes.number,
  /**
   * Defines the scroll behaviour of the table
   */
  scrollBehaviour: scrollBehaviourPropType,
  /**
   * array of row data, each row must have `__key` property
   */
  data: PropTypes.array
}

export default SimpleTableFormApp
