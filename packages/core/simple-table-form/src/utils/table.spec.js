import {form} from 'tocco-app-extensions'

import {getColumnDefinition} from './table'

const defaultColumnOptions = {
  resizable: true,
  sorting: {
    sortable: false
  },
  dynamic: true,
  readOnly: false,
  alignment: 'left',
  shrinkToContent: false,
  sticky: false
}

describe('simple-table-form', () => {
  describe('utils', () => {
    describe('table', () => {
      describe('getColumnDefinition', () => {
        test('should create columns for each type', () => {
          const table = form.createTable({
            children: [
              form.createColumn({
                id: 'col1',
                label: 'Column 1',
                dataType: 'string'
              }),
              form.createColumn({
                id: 'col2',
                label: 'Column 2',
                dataType: 'string',
                readonly: false
              }),
              form.createColumn({
                id: 'col3',
                label: 'Column 3',
                children: [form.createActionCell({id: 'action', label: 'Action', icon: 'bolt'})]
              })
            ]
          })

          const expectedColumns = [
            jestExpect.objectContaining({
              id: 'col1',
              label: 'Column 1',
              ...defaultColumnOptions,
              readOnly: true
            }),
            jestExpect.objectContaining({
              id: 'col2',
              label: 'Column 2',
              ...defaultColumnOptions
            }),
            jestExpect.objectContaining({
              id: 'col3',
              label: 'Column 3',
              ...defaultColumnOptions,
              alignment: 'center',
              shrinkToContent: true
            })
          ]
          jestExpect(getColumnDefinition({table})).toEqual(jestExpect.arrayContaining(expectedColumns))
        })

        test('should use only readonly columns when table is disabled', () => {
          const table = form.createTable({
            children: [
              form.createColumn({
                id: 'col1',
                label: 'Column 1',
                dataType: 'string'
              }),
              form.createColumn({
                id: 'col2',
                label: 'Column 2',
                dataType: 'string',
                readonly: false
              })
            ]
          })

          const expectedColumns = [
            jestExpect.objectContaining({
              id: 'col1',
              readOnly: true
            }),
            jestExpect.objectContaining({
              id: 'col2',
              readOnly: true
            })
          ]
          jestExpect(getColumnDefinition({table, disabled: true})).toEqual(jestExpect.arrayContaining(expectedColumns))
        })

        test('should use custom cell renderer', () => {
          const cellRenderers = {
            custom: () => <div key="custom"></div>
          }
          const table = form.createTable({
            children: [
              form.createColumn({
                id: 'col1',
                label: 'Column 1',
                dataType: 'string',
                clientRenderer: 'custom'
              }),
              form.createColumn({
                id: 'col2',
                label: 'Column 2',
                dataType: 'string',
                readonly: false,
                clientRenderer: 'custom'
              })
            ]
          })

          const columns = getColumnDefinition({table, cellRenderers})

          expect(columns[0].CellRenderer().type().key).to.eql('custom')
          expect(columns[1].CellRenderer().type().key).to.eql('custom')
        })

        test('should pass setValue and validate functions to editable component', () => {
          const setValue = sinon.spy()
          const validate = sinon.spy()
          const table = form.createTable({
            children: [
              form.createColumn({
                id: 'col1',
                label: 'Column 1',
                dataType: 'string',
                readonly: false
              })
            ]
          })

          const columns = getColumnDefinition({table, setValue, validate})

          columns[0].CellRenderer({rowData: {}, column: {}}).props.setValue('val')
          columns[0].CellRenderer({rowData: {}, column: {}}).props.validate({data: true})
          expect(setValue).to.have.been.calledWith('val')
          expect(validate).to.have.been.calledWith({data: true})
        })
      })
    })
  })
})
