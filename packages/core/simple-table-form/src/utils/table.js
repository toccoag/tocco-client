import {actions, tableForm} from 'tocco-app-extensions'
import {api} from 'tocco-util'

const getFormField = column => column.children[0]

const getType = (column, disabled) => {
  if (actions.isAction(getFormField(column)?.componentType)) {
    return 'action'
  }

  if (getFormField(column)?.componentType === 'button') {
    return 'button'
  }

  if (column.readonly || disabled) {
    return 'readonly'
  }

  return 'editable'
}

const mapActionDefinitionToColumn = (column, disabled, entityModel) => {
  const formField = getFormField(column)

  return tableForm.createActionColumn(
    column.id,
    column.label,
    tableForm.ActionCellRenderer,
    {disabled, formField, entityModel},
    {
      resizable: !column.widthFixed,
      ...(column.shrinkToContent ? {shrinkToContent: column.shrinkToContent} : {}),
      ...(column.sticky ? {sticky: column.sticky} : {}),
      ...(column.alignment ? {alignment: column.alignment} : {})
    }
  )
}

const mapButtonDefinitionToColumn = (column, disabled, customButtons) => {
  const formField = getFormField(column)

  return tableForm.createButtonColumn(
    column.id,
    column.label,
    tableForm.ButtonCellRenderer,
    {disabled, formField, onClick: customButtons[formField.id]},
    {
      resizable: !column.widthFixed,
      ...(column.shrinkToContent ? {shrinkToContent: column.shrinkToContent} : {}),
      ...(column.sticky ? {sticky: column.sticky} : {}),
      ...(column.alignment ? {alignment: column.alignment} : {})
    }
  )
}

const mapReadonlyDefinitionToColumn = (column, cellRenderers, sorting) => {
  const formField = getFormField(column)

  return tableForm.createReadonlyColumn(
    column.id,
    column.label,
    cellRenderers[column.clientRenderer] || tableForm.EditableCellRenderer,
    {type: formField?.dataType, mappingType: 'list', formField},
    {
      resizable: !column.widthFixed,
      ...(column.shrinkToContent ? {shrinkToContent: column.shrinkToContent} : {}),
      ...(column.sticky ? {sticky: column.sticky} : {}),
      ...(column.alignment ? {alignment: column.alignment} : {}),
      ...(column.sorting ? {sorting: {...column.sorting, ...api.getSortingAttributes(column, sorting)}} : {})
    }
  )
}

const mapEditableDefinitionToColumn = (column, cellRenderers, validate, setValue) => {
  const formField = getFormField(column)

  return tableForm.createEditableColumn(
    column.id,
    column.label,
    cellRenderers[column.clientRenderer] || tableForm.EditableCellRenderer,
    {type: formField?.dataType, setValue, validate, mappingType: 'editable', formField},
    {
      resizable: !column.widthFixed,
      ...(column.shrinkToContent ? {shrinkToContent: column.shrinkToContent} : {}),
      ...(column.sticky ? {sticky: column.sticky} : {}),
      ...(column.alignment ? {alignment: column.alignment} : {})
    }
  )
}

export const getColumnDefinition = ({
  table,
  disabled = false,
  entityModel,
  customButtons = {},
  cellRenderers = {},
  validate,
  setValue,
  columnWidths = [],
  sorting
}) =>
  table.children
    .map(c => {
      const type = getType(c, disabled)

      if (type === 'action') {
        return mapActionDefinitionToColumn(c, disabled, entityModel)
      }

      if (type === 'button') {
        return mapButtonDefinitionToColumn(c, disabled, customButtons)
      }

      if (type === 'readonly') {
        return mapReadonlyDefinitionToColumn(c, cellRenderers, sorting)
      }

      return mapEditableDefinitionToColumn(c, cellRenderers, validate, setValue)
    })
    .map(c => {
      const width = columnWidths[c.id]
      if (width) {
        return {...c, width}
      } else {
        return c
      }
    })
