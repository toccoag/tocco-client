import {form} from 'tocco-app-extensions'
import {keyboard} from 'tocco-util'

import SimpleTableFormApp from './main'

export default {
  title: 'Core/Simple Table Form',
  component: SimpleTableFormApp,
  argTypes: {}
}

const formDefinition = form.createSimpleForm({
  children: [
    form.createTable({
      children: [
        form.createColumn({
          label: 'Boolean',
          id: 'boolean',
          dataType: 'boolean',
          shrinkToContent: true,
          alignment: 'center',
          readonly: false
        }),
        form.createColumn({
          label: 'String',
          id: 'string',
          dataType: 'string',
          readonly: false
        }),
        form.createColumn({
          label: 'Text',
          id: 'text',
          dataType: 'text',
          readonly: false
        }),
        form.createColumn({
          label: 'Date',
          id: 'date',
          dataType: 'date',
          readonly: false
        }),
        form.createColumn({
          label: 'Datetime',
          id: 'datetime',
          dataType: 'datetime',
          readonly: false
        }),
        form.createColumn({
          label: 'Integer',
          id: 'integer',
          dataType: 'integer',
          readonly: false
        }),
        form.createColumn({
          label: 'Decimal',
          id: 'decimal',
          dataType: 'decimal',
          readonly: false,
          validation: {
            decimalDigits: {
              prePointDigits: null,
              postPointDigits: 3
            }
          }
        }),
        form.createColumn({
          label: 'Single Select Box',
          id: 'single-select-box',
          dataType: 'single-select-box',
          options: [
            {
              key: 'not_ok',
              display: 'Ungenügend'
            },
            {
              key: 'ok',
              display: 'Genügend'
            },
            {
              key: 'good',
              display: 'Gut'
            },
            {
              key: 'excellent',
              display: 'Ausgezeichnet'
            }
          ],
          readonly: false
        })
      ]
    })
  ]
})
const fieldsToFocus = formDefinition.children[0].children.map(d =>
  ['date', 'datetime'].includes(d.id) ? `${d.id}-datepicker` : d.id
)

const data = [
  {
    __key: '1',
    boolean: false,
    string: 'String',
    text: 'Text',
    date: new Date().toISOString(),
    datetime: new Date().toISOString(),
    number: 1,
    integer: 10,
    decimal: 4.5
  },
  {
    __key: '2',
    boolean: false,
    string: 'String',
    text: 'Text',
    date: new Date().toISOString(),
    datetime: new Date().toISOString(),
    number: 1,
    integer: 10,
    decimal: 4.5
  }
]

const SimpleTableFormStory = ({...args}) => {
  return (
    <div style={{width: '1500px', height: '200px', padding: '5px'}} onKeyDown={keyboard.navigateTable(fieldsToFocus)}>
      <SimpleTableFormApp {...args} />
    </div>
  )
}

export const Story = SimpleTableFormStory.bind({})
Story.args = {
  formDefinition,
  data
}
