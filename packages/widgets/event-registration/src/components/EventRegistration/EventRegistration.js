import PropTypes from 'prop-types'
import {useMemo, useState} from 'react'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType, queryString} from 'tocco-util'

import {VisibilityStatus} from '../../visibilityStatus'

import {modifyFormDefinition} from './formModifier'

const getInitialAction = () => {
  const queryParams = queryString.fromQueryString(window.location.search)
  if (queryParams?.eventKey) {
    return {
      name: 'event-registration-action',
      properties: {
        eventKey: queryParams.eventKey
      }
    }
  }
  if (queryParams?.orderUuid) {
    return {
      name: 'payment-provider-action',
      properties: {
        orderUuid: queryParams.orderUuid,
        paymentStatus: queryParams.paymentStatus
      }
    }
  }
}

const EventRegistration = ({
  formBase,
  searchFormType,
  reportIds,
  searchFilters,
  limit,
  backendUrl,
  businessUnit,
  appContext,
  fireVisibilityStatusChangeEvent,
  onVisibilityStatusChange,
  locale
}) => {
  const initialAction = getInitialAction()

  const [hideEntityBrowser, setHideEntityBrowser] = useState(false)

  const customActionEventHandlers = useMemo(
    () => ({
      'event-registration-action': {
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        },
        onAuthenticationPage: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.authentication])
        },
        onRegisterPage: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.register])
        }
      },
      'payment-provider-action': {
        onPaymentSummaryPage: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.paymentSummary])
        },
        onPaymentSuccessPage: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.paymentSuccess])
          setHideEntityBrowser(true)
        },
        onPaymentFailedPage: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.paymentFailed])
          setHideEntityBrowser(true)
        },
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        }
      }
    }),
    [fireVisibilityStatusChangeEvent]
  )

  return (
    <EntityBrowserApp
      entityName="Event"
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      reportIds={reportIds}
      searchFormType={searchFormType}
      initialAction={initialAction}
      locale={locale}
      customActionEventHandlers={customActionEventHandlers}
      onVisibilityStatusChange={onVisibilityStatusChange}
      modifyFormDefinition={modifyFormDefinition}
      isHidden={hideEntityBrowser}
      onRouteChange={() => setHideEntityBrowser(false)}
    />
  )
}

EventRegistration.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  onVisibilityStatusChange: PropTypes.func,
  locale: PropTypes.string
}

export default EventRegistration
