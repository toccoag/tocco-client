import {call} from 'redux-saga/effects'
import {rest, form} from 'tocco-app-extensions'
import {api} from 'tocco-util'

export function* modifyFormDefinition(formDefinition, context) {
  const {entityId, entityName: detailEntityName} = context

  if (entityId && detailEntityName === 'Event') {
    const {query, entityName} = yield call(api.eventRegistration.getEventRelationshipModulesQuery, entityId)
    const entityCount = yield call(rest.fetchEntityCount, entityName, query)

    return entityCount > 0 ? formDefinition : form.removeBoxes(formDefinition, ['modules'])
  }

  return formDefinition
}
