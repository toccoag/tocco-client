import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {api} from 'tocco-util'

import {modifyFormDefinition} from './formModifier'

describe('event-registration', () => {
  describe('components', () => {
    describe('EventRegistration', () => {
      describe('formModifier', () => {
        describe('modifyFormDefinition', () => {
          const formDefinition = {
            componentType: 'form',
            children: [
              {
                id: 'modules',
                componentType: 'layout',
                children: [{id: 'relModule_relationship', componentType: 'sub-table'}]
              }
            ]
          }
          const subTableFormDefinition = {
            componentType: 'form',
            children: [
              {
                id: 'table',
                componentType: 'table',
                children: [{id: 'relCourse.abbreviation"', componentType: 'column'}]
              }
            ]
          }
          const entityName = 'Event'
          const entityId = '123'

          test('should remove modules sub-tables when no submodules exist', () => {
            const context = {entityName, entityId}
            const query = {where: true}
            const expectedFormDefinition = {componentType: 'form', children: []}

            return expectSaga(modifyFormDefinition, formDefinition, context)
              .provide([
                [
                  matchers.call(api.eventRegistration.getEventRelationshipModulesQuery, entityId),
                  {query, entityName: 'Event_relationship'}
                ],
                [matchers.call(rest.fetchEntityCount, 'Event_relationship', query), 0]
              ])
              .returns(expectedFormDefinition)
              .run()
          })

          test('should keep modules sub-tables when at least one submodule exist', () => {
            const context = {entityName, entityId}
            const query = {where: true}
            const expectedFormDefinition = formDefinition

            return expectSaga(modifyFormDefinition, formDefinition, context)
              .provide([
                [
                  matchers.call(api.eventRegistration.getEventRelationshipModulesQuery, entityId),
                  {query, entityName: 'Event_relationship'}
                ],
                [matchers.call(rest.fetchEntityCount, 'Event_relationship', query), 1]
              ])
              .returns(expectedFormDefinition)
              .run()
          })

          test('should do nothing for subtable form modifier', () => {
            const context = {parent: {entityName, entityId}}
            const expectedFormDefinition = subTableFormDefinition

            return expectSaga(modifyFormDefinition, subTableFormDefinition, context)
              .not.call(api.eventRegistration.getEventRelationshipModulesQuery, entityId)
              .returns(expectedFormDefinition)
              .run()
          })
        })
      })
    })
  })
})
