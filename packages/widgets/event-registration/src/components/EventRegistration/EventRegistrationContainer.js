import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import EventRegistration from './EventRegistration'

const mapActionCreators = {
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  formBase: state.input.formBase,
  searchFormType: state.input.searchFormType,
  searchFilters: state.input.searchFilters,
  limit: state.input.limit,
  backendUrl: state.input.backendUrl,
  businessUnit: state.input.businessUnit,
  appContext: state.input.appContext,
  reportIds: state.input.reportIds,
  onVisibilityStatusChange: state.input.onVisibilityStatusChange,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EventRegistration))
