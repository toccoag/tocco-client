import {v4 as uuid} from 'uuid'

import EventRegistationApp from './main'

export default {
  title: 'Widgets/Event Registration',
  component: EventRegistationApp,
  argTypes: {
    formBase: {
      table: {
        disable: true
      }
    },
    backendUrl: {
      table: {
        disable: true
      }
    },
    appContext: {
      table: {
        disable: true
      }
    },
    searchFormType: {
      options: ['none', 'simple_advanced', 'advanced'],
      control: {type: 'select'}
    },
    widgetConfigKey: {control: {type: 'text'}}
  }
}

const EventRegistrationStory = ({...args}, {globals: {embedType}}) => (
  <EventRegistationApp key={uuid()} {...args} appContext={{embedType, widgetConfigKey: args.widgetConfigKey}} />
)

export const Story = EventRegistrationStory.bind({})
Story.args = {
  formBase: 'Event_registration',
  searchFormType: 'simple_advanced',
  limit: 15,
  reportIds: []
}
