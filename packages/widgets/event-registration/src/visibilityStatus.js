export const VisibilityStatus = {
  list: 'list',
  detail: 'detail',
  authentication: 'authentication',
  register: 'register',
  paymentSummary: 'payment-summary',
  success: 'success',
  paymentSuccess: 'payment-success',
  paymentFailed: 'payment-failed'
}
