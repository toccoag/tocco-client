import {queryString} from 'tocco-util'

export const OPEN_ID_STATUS_PARAM = 'openidStatus'
export const OPEN_ID_PROVIDER_PARAM = 'openidProvider'

export const cleanupUrl = () => {
  const url = new URL(window.location.href)
  url.searchParams.delete(OPEN_ID_STATUS_PARAM)
  url.searchParams.delete(OPEN_ID_PROVIDER_PARAM)
  window.history.replaceState(null, '', url.href)
}

export const extendUrlForSuccessHandling = href => {
  const url = new URL(href)
  url.searchParams.append(OPEN_ID_STATUS_PARAM, 'successful')
  return url.href
}

const extendUrlForAutoLoginHandling = (href, provider) => {
  const url = new URL(href)
  url.searchParams.append(OPEN_ID_STATUS_PARAM, 'successful')
  url.searchParams.append(OPEN_ID_PROVIDER_PARAM, provider)
  return url.href
}

const getAbsolutUrl = urlString => {
  if (!urlString) {
    return undefined
  }

  try {
    const url = new URL(urlString, window.location.href)
    return url.href
  } catch (error) {
    return undefined
  }
}

export const getRedirectUrl = (redirectUrl, provider) => {
  const {_redirect_url: queryUrl} = queryString.fromQueryString(window.location.search)
  const url = getAbsolutUrl(queryUrl) || getAbsolutUrl(redirectUrl) || window.location.href
  return encodeURIComponent(provider ? extendUrlForAutoLoginHandling(url, provider) : extendUrlForSuccessHandling(url))
}
