import {env} from 'tocco-util'

export const initiateLogin = (loginEndpoint, provider, redirectUrl) => {
  const baseUrl = __DEV__ ? '' : env.getBackendUrl()

  const redirectUri = encodeURIComponent(`${baseUrl}/nice2/sso-callback`)
  const sourceUri = encodeURIComponent(window.location.href)
  const targetUri = redirectUrl || encodeURIComponent(window.location.href)

  const providerParam = `provider=${provider.unique_id}`
  const sourceUriParam = `&sourceUri=${sourceUri}`
  const targetUriParam = `&targetUri=${targetUri}`
  // apply nice_auth cookie on customers extranet domain instead of tocco.ch-domain
  const redirectUriParam = env.isInWidgetEmbedded() ? `&redirectUri=${redirectUri}` : ''

  const url = `${baseUrl}${loginEndpoint}?${providerParam}${sourceUriParam}${targetUriParam}${redirectUriParam}`

  window.location.href = url
}
