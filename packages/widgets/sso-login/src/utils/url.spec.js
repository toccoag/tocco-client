import {cleanupUrl, extendUrlForSuccessHandling, getRedirectUrl} from './url'

describe('sso-login', () => {
  describe('utils', () => {
    describe('url', () => {
      describe('cleanupUrl', () => {
        test('should remove openid params', () => {
          window.history.replaceState = sinon.spy()
          delete window.location
          window.location = {href: 'https://google.ch/?foo=bar&openidStatus=successful&openidProvider=google'}
          cleanupUrl()
          expect(window.history.replaceState).to.be.calledWith(null, '', 'https://google.ch/?foo=bar')
        })
      })

      describe('extendUrlForSuccessHandling', () => {
        test('should add openidStatus param to url', () => {
          const href = 'https://google.ch'
          const result = extendUrlForSuccessHandling(href)
          expect(result).to.eql('https://google.ch/?openidStatus=successful')
        })

        test('should keep existing params', () => {
          const href = 'https://google.ch/?foo=bar'
          const result = extendUrlForSuccessHandling(href)
          expect(result).to.eql('https://google.ch/?foo=bar&openidStatus=successful')
        })
      })

      describe('getRedirectUrl', () => {
        test('should get redirectUrl from querystring', () => {
          const queryRedirectUrl = 'https://tocco.ch'
          const redirectUrl = 'https://google.ch'

          delete window.location
          window.location = {
            search: `?_redirect_url=${queryRedirectUrl}`
          }

          const result = getRedirectUrl(redirectUrl, '')
          expect(result).to.eql(encodeURIComponent('https://tocco.ch/?openidStatus=successful'))
        })

        test('should get redirectUrl from input when query is not set', () => {
          const redirectUrl = 'https://google.ch'

          delete window.location
          window.location = {}

          const result = getRedirectUrl(redirectUrl, '')
          expect(result).to.eql(encodeURIComponent('https://google.ch/?openidStatus=successful'))
        })

        test('should append provider param', () => {
          const redirectUrl = 'https://google.ch'
          const expectedRedirectUrl = encodeURIComponent(
            'https://google.ch/?openidStatus=successful&openidProvider=google'
          )

          delete window.location
          window.location = {}

          const result = getRedirectUrl(redirectUrl, 'google')
          expect(result).to.eql(expectedRedirectUrl)
        })

        test('should use absolute url for querystring url', () => {
          const queryRedirectUrl = '/private'
          const redirectUrl = 'https://google.ch'

          delete window.location
          window.location = {
            search: `?_redirect_url=${queryRedirectUrl}`,
            href: `https://tocco.ch/widget`
          }

          const result = getRedirectUrl(redirectUrl, '')
          expect(result).to.eql(encodeURIComponent('https://tocco.ch/private?openidStatus=successful'))
        })

        test('should use absolute url for input url', () => {
          const redirectUrl = '/private'

          delete window.location
          window.location = {
            href: `https://tocco.ch/widget`
          }

          const result = getRedirectUrl(redirectUrl, '')
          expect(result).to.eql(encodeURIComponent('https://tocco.ch/private?openidStatus=successful'))
        })
      })
    })
  })
})
