import {connect} from 'react-redux'

import * as actions from '../../modules/actions'

import LoginBox from './LoginBox'

const mapActionCreators = {
  init: actions.init,
  loadProviders: actions.loadProviders
}

const mapStateToProps = state => ({
  providers: state.ssoLogin.providers,
  loginEndpoint: state.input.ssoLoginEndpoint,
  autoLogin: state.input.autoLogin,
  redirectUrl: state.input.redirectUrl,
  showError: state.ssoLogin.showError
})

export default connect(mapStateToProps, mapActionCreators)(LoginBox)
