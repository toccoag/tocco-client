import styled from 'styled-components'
import {StyledSpan, themeSelector, scale} from 'tocco-ui'

export const StyledButtonContainer = styled.div`
  width: 100%;
  margin-top: ${scale.space(1.2)};
  display: grid;
  grid-gap: ${scale.space(-0.4)};
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));

  @media (max-width: 1024px) {
    margin-top: ${scale.space(0.2)};
  }
`

export const StyledSsoError = styled(StyledSpan)`
  && {
    display: inline-block;
    margin-top: ${scale.space(-0.5)};
    margin-bottom: ${scale.space(-0.5)};
    text-align: center;
    width: 100%;
    color: ${themeSelector.color('signal.danger')};
  }
`
