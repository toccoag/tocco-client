import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {FormattedMessage} from 'react-intl'
import {Icon} from 'tocco-ui'

import {getRedirectUrl} from '../../utils/url'
import {useAutoLogin} from '../AutoLogin'
import ProviderButton from '../ProviderButton/ProviderButton'

import {StyledButtonContainer, StyledSsoError} from './StyledComponents'

const LoginBox = ({init, loadProviders, autoLogin, providers = [], loginEndpoint, redirectUrl, showError}) => {
  useEffect(() => {
    init()
  }, [init])

  useEffect(() => {
    loadProviders()
  }, [loadProviders])

  useAutoLogin({
    autoLogin,
    providers,
    loginEndpoint,
    redirectUrl: getRedirectUrl(redirectUrl)
  })

  const ProviderButtons = providers.map(provider => (
    <ProviderButton
      key={provider.unique_id}
      provider={provider}
      loginEndpoint={loginEndpoint}
      redirectUrl={getRedirectUrl(redirectUrl, autoLogin ? provider.unique_id : undefined)}
    />
  ))

  return (
    <>
      <StyledButtonContainer>{ProviderButtons}</StyledButtonContainer>
      {showError && (
        <StyledSsoError breakWords={true}>
          <Icon icon={'times'} />
          <FormattedMessage id="client.sso-login.error" />
        </StyledSsoError>
      )}
    </>
  )
}

LoginBox.propTypes = {
  loginEndpoint: PropTypes.string,
  init: PropTypes.func.isRequired,
  loadProviders: PropTypes.func.isRequired,
  providers: PropTypes.array,
  redirectUrl: PropTypes.string,
  autoLogin: PropTypes.bool,
  showError: PropTypes.bool
}

export default LoginBox
