import PropTypes from 'prop-types'
import {Icon} from 'tocco-ui'

import {initiateLogin} from '../../utils/login'

import StyledProviderButton from './StyledProviderButton'

const ProviderButton = ({provider, loginEndpoint, redirectUrl}) => {
  const clickHandler = () => {
    initiateLogin(loginEndpoint, provider, redirectUrl)
  }

  return (
    <StyledProviderButton
      primaryColor={provider.button_primary_color}
      secondaryColor={provider.button_secondary_color}
      onClick={clickHandler}
    >
      {provider.button_icon && <Icon position="prepend" icon={provider.button_icon} />}
      <span>{provider.button_label}</span>
    </StyledProviderButton>
  )
}

ProviderButton.propTypes = {
  provider: PropTypes.shape({
    button_primary_color: PropTypes.string,
    button_secondary_color: PropTypes.string,
    button_icon: PropTypes.string,
    button_label: PropTypes.string
  }),
  loginEndpoint: PropTypes.string.isRequired,
  redirectUrl: PropTypes.string
}

export default ProviderButton
