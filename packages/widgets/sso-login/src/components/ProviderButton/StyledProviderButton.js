import styled from 'styled-components'
import {StyledButton, theme, scale} from 'tocco-ui'
import {color} from 'tocco-util'

export default styled(StyledButton)`
  && {
    margin-right: 0;
    flex: 1;
    font-size: ${scale.font(1.3)};
    justify-content: center;
    border-radius: ${theme.radii('buttonLarge')};
    background-color: ${({primaryColor}) => primaryColor};
    color: ${({secondaryColor}) => color.getContrastColor(secondaryColor)};

    span {
      margin-left: ${scale.space(-0.8)};
      padding: ${scale.space(-0.55)} 0;
    }
  }
`
