import {screen, fireEvent} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {initiateLogin} from '../../utils/login'

import ProviderButton from './ProviderButton'

jest.mock('../../utils/login', () => {
  return {
    initiateLogin: jest.fn()
  }
})

const EMPTY_FUNC = () => {}

describe('sso-login', () => {
  describe('components', () => {
    const baseProps = {
      provider: {
        button_primary_color: '#F00',
        button_secondary_color: '#00F',
        button_icon: 'fab, microsoft',
        button_label: 'Login with MS'
      },
      redirectUrl: '',
      loadProviders: EMPTY_FUNC,
      loginEndpoint: 'test'
    }

    describe('ProviderButton', () => {
      test('should render a button', () => {
        testingLibrary.renderWithIntl(<ProviderButton {...baseProps} />)
        expect(screen.queryAllByRole('button')).to.have.length(1)
      })

      test('should invoke login', () => {
        testingLibrary.renderWithIntl(<ProviderButton {...baseProps} />)

        fireEvent.click(screen.getByRole('button'))

        expect(initiateLogin.mock.calls).has.length(1)
      })
    })
  })
})
