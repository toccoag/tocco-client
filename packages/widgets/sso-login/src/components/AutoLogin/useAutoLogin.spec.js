import {renderHook} from '@testing-library/react'
import Cookies from 'js-cookie'

import {initiateLogin} from '../../utils/login'

import useAutoLogin from './useAutoLogin'

jest.mock('js-cookie', () => {
  return {
    set: jest.fn(),
    get: jest.fn(),
    remove: jest.fn()
  }
})

jest.mock('../../utils/login', () => {
  return {
    initiateLogin: jest.fn()
  }
})

describe('sso-login', () => {
  describe('components', () => {
    describe('AutoLogin', () => {
      describe('useAutoLogin', () => {
        beforeEach(() => {
          initiateLogin.mockReset()
        })

        test('should invoke login when providers has been loaded', () => {
          const config = {
            autoLogin: true,
            providers: [],
            loginEndpoint: '/sso',
            redirectUrl: encodeURIComponent('https://google.ch')
          }
          Cookies.get.mockReturnValue('google')

          const {rerender} = renderHook(props => useAutoLogin(props), {initialProps: config})

          rerender({...config, providers: [{unique_id: 'google'}]})

          expect(initiateLogin.mock.calls).has.length(1)
        })

        test('should invoke login only once', () => {
          const config = {
            autoLogin: true,
            providers: [],
            loginEndpoint: '/sso',
            redirectUrl: encodeURIComponent('https://google.ch')
          }
          Cookies.get.mockReturnValue('google')

          const {rerender} = renderHook(props => useAutoLogin(props), {initialProps: config})

          rerender({...config, providers: [{unique_id: 'google'}]})
          rerender({...config, providers: [{unique_id: 'google'}]})
          rerender({...config, providers: [{unique_id: 'google'}]})

          expect(initiateLogin.mock.calls).has.length(1)
        })

        test('should not invoke login when provider is not set', () => {
          const config = {
            autoLogin: true,
            providers: [],
            loginEndpoint: '/sso',
            redirectUrl: encodeURIComponent('https://google.ch')
          }
          Cookies.get.mockReturnValue(undefined)

          const {rerender} = renderHook(props => useAutoLogin(props), {initialProps: config})

          rerender({...config, providers: [{unique_id: 'google'}]})

          expect(initiateLogin.mock.calls).has.length(0)
        })

        test('should not invoke login when autoLogin is disabled', () => {
          const config = {
            autoLogin: false,
            providers: [],
            loginEndpoint: '/sso',
            redirectUrl: encodeURIComponent('https://google.ch')
          }
          Cookies.get.mockReturnValue('google')

          const {rerender} = renderHook(props => useAutoLogin(props), {initialProps: config})

          rerender({...config, providers: [{unique_id: 'google'}]})

          expect(initiateLogin.mock.calls).has.length(0)
        })
      })
    })
  })
})
