import Cookies from 'js-cookie'
import {useEffect} from 'react'
import {react} from 'tocco-util'

import {initiateLogin} from '../../utils/login'

import {AUTO_LOGIN_COOKIE} from './constants'

const useAutoLogin = ({autoLogin, providers, loginEndpoint, redirectUrl}) => {
  const provider = Cookies.get(AUTO_LOGIN_COOKIE)

  const prevProviders = react.usePrevious(providers)

  useEffect(() => {
    if (autoLogin && provider && (!prevProviders || prevProviders.length === 0) && providers.length > 0) {
      const autoLoginProvider = providers.find(entity => entity.unique_id === provider)

      if (autoLoginProvider) {
        initiateLogin(loginEndpoint, autoLoginProvider, redirectUrl)
      }
    }
  }, [autoLogin, prevProviders, providers, provider, loginEndpoint, redirectUrl])
}

export default useAutoLogin
