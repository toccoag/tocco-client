import Cookies from 'js-cookie'
import {queryString} from 'tocco-util'

import {cleanupUrl} from '../../utils/url'

import {AUTO_LOGIN_COOKIE} from './constants'

const useAutoLoginSetup = () => {
  const {openidStatus, openidProvider} = queryString.fromQueryString(window.location.search)

  if (openidStatus === 'successful' && openidProvider) {
    Cookies.set(AUTO_LOGIN_COOKIE, openidProvider, {expires: 365})
    cleanupUrl()
  }
}

export default useAutoLoginSetup
