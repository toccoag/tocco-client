import {takeLatest, all, call, put, select} from 'redux-saga/effects'
import {externalEvents, rest} from 'tocco-app-extensions'
import {request, queryString, env, remoteLogger} from 'tocco-util'

import {transformProviderEntities} from '../utils/providers'
import {cleanupUrl} from '../utils/url'
import {VisibilityStatus} from '../visibilityStatus'

import * as actions from './actions'

const entityName = 'Openid_provider'

export const intlSelector = state => state.intl
export const inputSelector = state => state.input

export function* loadProviders() {
  const intl = yield select(intlSelector)

  const query = {
    paths: ['unique_id', 'label', 'button_primary_color', 'button_secondary_color', 'button_icon', 'button_label'],
    conditions: {locale: intl.locale, active: true}
  }

  const entities = yield call(rest.fetchAllEntities, entityName, query, {method: 'GET'}, transformProviderEntities)
  yield put(actions.setProviders(entities))
}

export function* doLoginRequest() {
  yield call(request.executeRequest, 'login', {
    method: 'GET',
    headers: new Headers({
      'X-Business-Unit': env.getBusinessUnit()
    })
  })
}

export function* initSso() {
  const {openidStatus} = yield call(queryString.fromQueryString, window.location.search)
  yield call(cleanupUrl)

  if (openidStatus === 'failed') {
    if (env.isInWidgetEmbedded()) {
      yield put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.loginFailed]))
    }
    yield put(actions.setShowError(true))
  } else if (openidStatus) {
    try {
      const {body: claims} = yield call(rest.requestSaga, 'client/openid/claims', {method: 'GET'})
      const result = {
        provider: claims.provider,
        sub: claims.sub,
        registration: openidStatus === 'registration',
        successful: openidStatus === 'successful'
      }

      if (openidStatus === 'registration') {
        if (env.isInWidgetEmbedded()) {
          yield put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.registrationStarted]))
        }
      }

      yield put(externalEvents.fireExternalEvent('loginCompleted', result))
    } catch (error) {
      yield put(actions.setShowError(true))
      remoteLogger.logError('Claims could not be fetched after sso login.')
    }
  }
}

export default function* mainSagas() {
  yield all([takeLatest(actions.INIT, initSso), takeLatest(actions.LOAD_PROVIDERS, loadProviders)])
}
