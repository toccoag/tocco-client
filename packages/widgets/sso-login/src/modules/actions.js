export const INIT = 'sso-login/INIT'
export const LOAD_PROVIDERS = 'sso-login/LOAD_PROVIDERS'
export const SET_PROVIDERS = 'sso-login/SET_PROVIDERS'
export const SET_SHOW_ERROR = 'sso-login/SET_SHOW_ERROR'

export const init = () => ({
  type: INIT
})

export const loadProviders = () => ({
  type: LOAD_PROVIDERS
})

export const setProviders = providers => ({
  type: SET_PROVIDERS,
  payload: {
    providers
  }
})

export const setShowError = showError => ({
  type: SET_SHOW_ERROR,
  payload: {
    showError
  }
})
