import ssoLogin from './reducer'
import rootSaga from './sagas'
import * as sagasUtils from './sagasUtils'

export default {
  ssoLogin
}

const sagas = [rootSaga]
export {sagas, sagasUtils}
