import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_PROVIDERS]: reducerUtil.singleTransferReducer('providers'),
  [actions.SET_SHOW_ERROR]: reducerUtil.singleTransferReducer('showError')
}

const initialState = {
  providers: [],
  showError: false
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
