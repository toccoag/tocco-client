import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {cache} from 'tocco-util'

import * as sagas from './sagasUtils'

describe('sso-login', () => {
  describe('modules', () => {
    describe('sagasUtils', () => {
      describe('checkSsoAvailable', () => {
        beforeEach(() => {
          cache.removeLongTerm('server', 'ssoAvailable')
        })

        test('should return cached value', () => {
          cache.addLongTerm('server', 'ssoAvailable', true)
          return expectSaga(sagas.checkSsoAvailable).not.call(sagas.isSsoAvailable).returns(true).run()
        })

        test('should check sso availability on cache miss', async () => {
          await expectSaga(sagas.checkSsoAvailable)
            .provide([[matchers.call(sagas.isSsoAvailable), false]])
            .call(sagas.isSsoAvailable)
            .returns(false)
            .run()

          const cachedSsoAvailable = cache.getLongTerm('server', 'ssoAvailable')
          expect(cachedSsoAvailable).to.be.false
        })
      })

      describe('isSsoAvailable', () => {
        test('should return true if module available and at least 1 provider', () => {
          return expectSaga(sagas.isSsoAvailable)
            .provide([
              [matchers.call(rest.fetchModules), ['nice.optional.address', 'nice.optional.sso']],
              [matchers.call(rest.fetchEntityCount, 'Openid_provider', {where: 'active == true'}), 1]
            ])
            .returns(true)
            .run()
        })

        test('should return false if module available but no provider', () => {
          return expectSaga(sagas.isSsoAvailable)
            .provide([
              [matchers.call(rest.fetchModules), ['nice.optional.address', 'nice.optional.sso']],
              [matchers.call(rest.fetchEntityCount, 'Openid_provider', {where: 'active == true'}), 0]
            ])
            .returns(false)
            .run()
        })

        test('should return false if module not available', () => {
          return expectSaga(sagas.isSsoAvailable)
            .provide([[matchers.call(rest.fetchModules), ['nice.optional.address']]])
            .returns(false)
            .run()
        })
      })
    })
  })
})
