import {call} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {cache} from 'tocco-util'

export function* isSsoAvailable() {
  const modules = yield call(rest.fetchModules)
  const ssoModuleAvailable = modules.includes('nice.optional.sso')
  if (!ssoModuleAvailable) {
    return false
  }
  const ssoProviderCount = yield call(rest.fetchEntityCount, 'Openid_provider', {where: 'active == true'})
  return ssoProviderCount > 0
}

export function* checkSsoAvailable() {
  const cachedSsoAvailable = cache.getLongTerm('server', 'ssoAvailable')
  if (cachedSsoAvailable !== undefined) {
    return cachedSsoAvailable
  } else {
    const ssoAvailable = yield call(isSsoAvailable)
    cache.addLongTerm('server', 'ssoAvailable', ssoAvailable)
    return ssoAvailable
  }
}
