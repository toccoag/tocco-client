import {takeLatest, select} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {externalEvents, rest} from 'tocco-app-extensions'
import {queryString, env} from 'tocco-util'

import {VisibilityStatus} from '../visibilityStatus'

import * as actions from './actions'
import mainSaga, * as sagas from './sagas'

describe('sso-login', () => {
  describe('modules', () => {
    describe('sagas', () => {
      describe('mainSaga', () => {
        test('should fork sagas', () => {
          const saga = testSaga(mainSaga)
          saga
            .next()
            .all([takeLatest(actions.INIT, sagas.initSso), takeLatest(actions.LOAD_PROVIDERS, sagas.loadProviders)])
        })
      })

      describe('initSso', () => {
        test('should call external event with result', () => {
          const result = {successful: true, registration: false, sub: 'sub', provider: 'google'}
          return expectSaga(sagas.initSso)
            .provide([
              [select(sagas.inputSelector), {appContext: {embedType: 'admin'}}],
              [matchers.call.fn(queryString.fromQueryString), {openidStatus: 'successful'}],
              [
                matchers.call(rest.requestSaga, 'client/openid/claims', {method: 'GET'}),
                {body: {sub: 'sub', provider: 'google'}}
              ]
            ])
            .put(externalEvents.fireExternalEvent('loginCompleted', result))
            .not.put(actions.setShowError(true))
            .run()
        })

        test('should fire visibility status change on registration', () => {
          const result = {successful: false, registration: true, sub: 'sub', provider: 'google'}
          env.setEmbedType(env.EmbedType.widget)

          return expectSaga(sagas.initSso)
            .provide([
              [matchers.call.fn(queryString.fromQueryString), {openidStatus: 'registration'}],
              [
                matchers.call(rest.requestSaga, 'client/openid/claims', {method: 'GET'}),
                {body: {sub: 'sub', provider: 'google'}}
              ]
            ])
            .put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.registrationStarted]))
            .put(externalEvents.fireExternalEvent('loginCompleted', result))
            .not.put(actions.setShowError(true))
            .run()
        })

        test('should fire visibility status change and show error on failed login', () => {
          env.setEmbedType(env.EmbedType.widget)

          return expectSaga(sagas.initSso)
            .provide([[matchers.call.fn(queryString.fromQueryString), {openidStatus: 'failed'}]])
            .put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.loginFailed]))
            .not.put.like({action: {type: 'externalEvents/FIRE_EXTERNAL_EVENT'}})
            .put(actions.setShowError(true))
            .run()
        })
      })

      describe('loadProviders', () => {
        test('should call fetchAllEntities and dispatch result', () => {
          return expectSaga(sagas.loadProviders)
            .provide([
              [select(sagas.intlSelector), {locale: 'de'}],
              [matchers.call.fn(rest.fetchAllEntities), []]
            ])
            .call.like({fn: rest.fetchAllEntities})
            .put(actions.setProviders([]))
            .run()
        })
      })
    })
  })
})
