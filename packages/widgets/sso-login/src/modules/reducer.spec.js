import * as actions from './actions'
import reducer from './reducer'

const INITIAL_STATE = {
  providers: [],
  showError: false
}

describe('sso-login-detail', () => {
  describe('modules', () => {
    describe('reducer', () => {
      test('should create a valid initial state', () => {
        expect(reducer(undefined, {})).to.deep.equal(INITIAL_STATE)
      })

      test('should handle providers action', () => {
        const stateBefore = INITIAL_STATE
        const providers = [{id: 'google'}, {id: 'microsoft'}]
        const expectedStateAfter = {
          providers,
          showError: false
        }

        expect(reducer(stateBefore, actions.setProviders(providers))).to.deep.equal(expectedStateAfter)
      })

      test('should handle show error action', () => {
        const stateBefore = INITIAL_STATE
        const expectedStateAfter = {
          providers: [],
          showError: true
        }

        expect(reducer(stateBefore, actions.setShowError(true))).to.deep.equal(expectedStateAfter)
      })
    })
  })
})
