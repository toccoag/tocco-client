import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {appContext, reducer as reducerUtil, env, consoleLogger} from 'tocco-util'

import {useAutoLoginSetup, AUTO_LOGIN_COOKIE} from './components/AutoLogin'
import LoginBox from './components/LoginBox'
import reducers, {sagas, sagasUtils} from './modules'
import {extendUrlForSuccessHandling} from './utils/url'

const packageName = 'sso-login'

const EXTERNAL_EVENTS = [
  /**
   * This app provides a global function `ssoPopUpCallback` on the window.opener scope
   * that accepts a result object as parameter.
   * As soon as this callback is invoked, the popup closes and the loginCompleted event is fired with the result object
   */
  'loginCompleted',
  'onVisibilityStatusChange'
]

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <LoginBox />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/index', () => {
          const hotReloadedReducers = require('./modules').default
          reducerUtil.hotReloadReducers(store, hotReloadedReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const SsoLoginApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  return component
}

SsoLoginApp.propTypes = {
  /**
   * Relative path to sso login endpoint e.g. `/sso`
   */
  ssoLoginEndpoint: PropTypes.string.isRequired,
  /**
   * ISO Language Code
   */
  locale: PropTypes.string,
  /**
   * Boolean to define if autoLogin is active.
   * If it's true, the component automatically initialises the authentication for the given provider
   */
  autoLogin: PropTypes.bool,
  appContext: appContext.propTypes,
  businessUnit: PropTypes.string,
  /**
   * A URL that the user gets redirected to after they're logged in.
   * If empty, the `loginCompleted` event will probably need to be used
   */
  redirectUrl: PropTypes.string
}

export default SsoLoginApp
export const app = appFactory.createBundleableApp(packageName, initApp, SsoLoginApp)
export {sagasUtils, extendUrlForSuccessHandling, useAutoLoginSetup, AUTO_LOGIN_COOKIE}
