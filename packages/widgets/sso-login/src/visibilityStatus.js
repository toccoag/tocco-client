export const VisibilityStatus = {
  loginFailed: 'login_failed',
  registrationStarted: 'registration_started'
}
