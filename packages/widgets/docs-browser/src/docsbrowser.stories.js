import {v4 as uuid} from 'uuid'

import DocsBrowserApp from './main'

export default {
  title: 'Widgets/Docs Browser',
  component: DocsBrowserApp,
  argTypes: {
    searchFormType: {
      options: ['none', 'admin'],
      control: {type: 'select'}
    },
    scrollBehaviour: {
      defaultValue: 'none',
      options: ['none', 'inline'],
      control: {type: 'select'}
    },
    widgetConfigKey: {control: {type: 'text'}}
  }
}

const DocsBrowserStory = ({...args}, {globals: {embedType}}) => (
  <DocsBrowserApp key={uuid()} {...args} appContext={{embedType, widgetConfigKey: args.widgetConfigKey}} />
)

export const Story = DocsBrowserStory.bind({})
Story.args = {
  routerType: 'standalone',
  listLimit: 20,
  scrollBehaviour: 'none',
  searchFormType: 'none',
  embedded: true,
  appContext: {
    embedType: 'widget'
  }
}
