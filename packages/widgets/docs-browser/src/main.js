import PropTypes from 'prop-types'
import {
  actionEmitter,
  appFactory,
  actions,
  errorLogging,
  externalEvents,
  notification,
  formData
} from 'tocco-app-extensions'
import EntityListApp, {selectionStylePropType} from 'tocco-entity-list/src/main'
import SimpleFormApp from 'tocco-simple-form/src/main'
import {scrollBehaviourPropType} from 'tocco-ui'
import {appContext, reducer as reducerUtil, env, consoleLogger} from 'tocco-util'

import Action from './components/Action'
import {InheritedApp, RouterlessApp, StandaloneApp} from './components/App'
import customActions from './customActions'
import chooseDocument from './modules/chooseDocument'
import reducers, {sagas} from './modules/reducers'
import {navigate} from './modules/routing/actions'
import {ignoreDefaultNavigationStrategy} from './utils/constants'
import {searchFormTypePropTypes} from './utils/searchFormTypes'

const packageName = 'docs-browser'

const EXTERNAL_EVENTS = [
  /**
   * This event is fired when the parent of the list is changed
   *
   * Payload: null (= root) or {model: 'User', key: '1'}
   */
  'onListParentChange',
  'openResource',
  /**
   * This event is fired when the selection changes
   *
   * Payload: An array containing the ids of the new selection
   */
  'onSelectChange',
  /**
   * Is fired when the user click in the arrow in the admin search form to collapse the search form
   *
   * Payload: | `collapsed` boolean. Whether it was opened or closed
   */
  'onSearchFormCollapsedChange'
]

const getContent = ({routerType, rootPath, handleNotifications}) => {
  if (routerType === 'routerless') {
    return <RouterlessApp rootPath={rootPath} handleNotifications={handleNotifications} />
  }

  if (routerType === 'inherit') {
    return <InheritedApp rootPath={rootPath} handleNotifications={handleNotifications} />
  }

  // only create new histroy object for standalone docs-browser (e.g. Widget)
  return <StandaloneApp rootPath={rootPath} handleNotifications={handleNotifications} />
}

const initApp = (id, input, events, publicPath) => {
  const defaultInput = {
    scrollBehaviour: 'inline',
    clearSelectionOnParentChange: true,
    selectOnRowClick: true,
    selectionStyle: 'multi_explicit'
  }

  const store = appFactory.createStore(reducers, sagas, {...defaultInput, ...input}, packageName)

  env.setInputEnvs(input)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store)
  actions.addToStore(store, state => ({
    formApp: SimpleFormApp,
    listApp: EntityListApp,
    customActions,
    appComponent: Action,
    navigationStrategy: state.input.navigationStrategy,
    context: {
      viewName: 'list',
      formName: state.input.formName,
      detailFormNames: {
        Domain: state.input.domainDetailFormName,
        Folder: state.input.folderDetailFormName
      }
    }
  }))
  formData.addToStore(store, state => ({listApp: EntityListApp, navigationStrategy: state.input.navigationStrategy}))
  actions.dynamicActionsAddToStore(store)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  const handleNotifications = !events.emitAction
  notification.addToStore(store, handleNotifications)

  const singleRootNode =
    Array.isArray(input.rootNodes) && input.rootNodes.length === 1 && input.rootNodes[0].entityName !== 'Resource'
      ? input.rootNodes[0]
      : null

  const rootPath = singleRootNode
    ? `/docs/${singleRootNode.entityName.toLowerCase()}/${singleRootNode.key}/list`
    : '/docs'

  const content = getContent({
    routerType: input.routerType,
    rootPath,
    handleNotifications
  })

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [navigate(input.initialLocation)],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', 'entity-list', 'entity-detail', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input, {})

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const DocsBrowserApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName, externalEvents: EXTERNAL_EVENTS})
  // Fragments-Workaround to support propTypes in Storybook
  return <>{component}</>
}

DocsBrowserApp.propTypes = {
  /**
   * Defines whether a new hash history router, the parent router or non router has to be used (default: standalone)
   *
   * - `routerless` navigation is not reflected in browser history (e.g. Move Action)
   * - `inherit` take over routing context from parent (e.g. Admin)
   * - `standalone" creates new routing context (e.g. Widget)
   */
  routerType: PropTypes.oneOf(['standalone', 'inherit', 'routerless']),
  navigationStrategy: PropTypes.object,
  /**
   * Array of visible domain types.
   */
  domainTypes: PropTypes.arrayOf(PropTypes.string),
  /**
   * Array of root nodes to use instead of the domains (array of objects with `entityName` and `key`)
   */
  rootNodes: PropTypes.arrayOf(
    PropTypes.shape({
      entityName: PropTypes.string,
      key: PropTypes.string
    })
  ),
  /**
   * If set, the docs browser will initially navigate to the given location.
   */
  initialLocation: PropTypes.string,
  /**
   * Maximum records per page.
   */
  listLimit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /**
   * Name of the document detail form (without `scope`) (default: `DmsResource`)
   */
  documentDetailFormName: PropTypes.string,
  /**
   * Name of the domain detail form to use (default: `DmsDomain`)
   */
  domainDetailFormName: PropTypes.string,
  /**
   * Name of the folder detail form to use (default: `DmsFolder`)
   */
  folderDetailFormName: PropTypes.string,
  /**
   * Defines what type of search form is shown.
   */
  searchFormType: searchFormTypePropTypes,
  /**
   * Defines what type of selection is possible.
   * If not defined and form model selectable is true, "multi" is used. Otherwise no selection is possible
   */
  selectionStyle: selectionStylePropType,
  /**
   * Function that returns a bool if a row is selectable.
   */
  selectionFilterFn: PropTypes.func,
  /**
   * Array of keys of the selected records.
   * When this is set the internal selection state management is skipped and `selection` input is taken
   */
  selection: PropTypes.arrayOf(PropTypes.string),
  /**
   * If the parent is changed should the selection be cleared (default: true)
   */
  clearSelectionOnParentChange: PropTypes.bool,
  /**
   * If true clicking on a row (de-)selects the item (default: true)
   */
  selectOnRowClick: PropTypes.bool,
  /**
   * Defines if the docs browser is taking as much space as needed (`'none'`) or if it will fit into its outer container
   * (`'inline'`). When set to `'inline'` the outer container has to have e predefined height (Default: `inline`)
   *
   * - none: Does not handle scroll internally and will take as much space as needed.
   *   The container / page needs to handle the scroll
   * - inline: Does handle scroll internally and takes the space given by the container.
   *   Containers needs to have a predefined height
   */
  scrollBehaviour: scrollBehaviourPropType,
  /**
   * If set form name is determined by this function which has (parent, keys) as input parameters
   */
  getListFormName: PropTypes.func,
  /**
   * Function to define a custom navigation on row click.
   */
  getCustomLocation: PropTypes.func,
  /**
   * The unique id of a business unit.
   * If present, all REST request will use this in their business unit header (X-Business-Unit).
   * Set input parameter to `__n-u-l-l__` if the null business unit should be used.
   */
  businessUnit: PropTypes.string,
  /**
   * If true, the admin search form on the left is collapsed and not visible by default.
   */
  searchFormCollapsed: PropTypes.bool,
  /**
   * Set backend url dynamically to point to nice2 installtion.
   * If not set it fallbacks to the build time environment `__BACKEND_URL__`.
   */
  backendUrl: PropTypes.string,
  /**
   * If true, the styling is more subtle (E.g. no background color for in the breadcrumbs; Default: false)
   */
  embedded: PropTypes.bool,
  /**
   * If false, a left padding will be applied to the Breadcrumbs (Default: false)
   */
  noLeftPadding: PropTypes.bool,
  appContext: appContext.propTypes,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}

export default DocsBrowserApp
export {chooseDocument, ignoreDefaultNavigationStrategy}
export const app = appFactory.createBundleableApp(packageName, initApp, DocsBrowserApp)
