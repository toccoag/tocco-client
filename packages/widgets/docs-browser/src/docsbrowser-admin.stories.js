import {v4 as uuid} from 'uuid'

import DocsBrowerStory from './docsbrowser.stories'
import DocsBrowserApp from './main'

export default {
  ...DocsBrowerStory,
  title: 'Admin/Docs Browser'
}

const DocsBrowserStory = ({...args}) => {
  return (
    <div style={{width: '1500px', height: '500px', padding: '5px'}}>
      <DocsBrowserApp key={uuid()} {...args} />
    </div>
  )
}

export const Admin = DocsBrowserStory.bind({})
Admin.args = {
  routerType: 'standalone',
  listLimit: 20,
  scrollBehaviour: 'inline',
  appContext: {
    embedType: 'admin'
  }
}
