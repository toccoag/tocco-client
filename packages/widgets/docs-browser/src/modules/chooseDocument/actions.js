export const CHOOSE_DOCUMENT = 'path/CHOOSE_DOCUMENT'

export const chooseDocument = (setDocument, formName, formFieldId, onChange) => ({
  type: CHOOSE_DOCUMENT,
  payload: {
    setDocument,
    formName,
    formFieldId,
    onChange
  }
})
