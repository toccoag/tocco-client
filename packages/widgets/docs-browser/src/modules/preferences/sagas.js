import _toNumber from 'lodash/toNumber'
import {channel} from 'redux-saga'
import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'
import {ColumnModal, formsApi, preferencesUtil as util, SelectNumRows, columnsUtil} from 'tocco-entity-list/src/main'
import {api} from 'tocco-util'

import {scope} from '../../utils/constants'
import * as listActions from '../list/actions'
import * as listSagas from '../list/sagas'

import {setPositions, setSorting, setColumns, setPreferencesLoaded, setNumberOfTableRows} from './actions'
import * as actions from './actions'

export const docsListSelector = state => state.docsList
export const preferencesSelector = state => state.preferences

export default function* sagas() {
  yield all([
    takeLatest(actions.LOAD_PREFERENCES, loadPreferences),
    takeLatest(actions.CHANGE_POSITION, changePosition),
    takeLatest(listActions.SET_SORTING_INTERACTIVE, saveSorting),
    takeLatest(actions.RESET_SORTING, resetSorting),
    takeLatest(actions.RESET_COLUMNS, resetColumns),
    takeLatest(actions.RESET_PREFERENCES, resetPreferences),
    takeLatest(actions.DISPLAY_COLUMN_MODAL, displayColumnModal),
    takeLatest(actions.DISPLAY_TABLE_ROWS_MODAL, displayTableRowsModal)
  ])
}

export function* loadPreferences() {
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`
  const preferences = yield call(rest.fetchUserPreferences, `${actualFormName}*`)
  yield put(setPositions(util.getPositions(preferences, actualFormName)))
  yield put(setSorting(util.getSorting(preferences)))
  yield put(setColumns(util.getColumns(preferences)))
  yield put(setNumberOfTableRows(Number(preferences[`${actualFormName}.numOfRows`])))
  yield put(setPreferencesLoaded(true))
}

export function* changePosition({payload}) {
  const {field, targetField, horizontalDropPosition, columns} = payload
  let {positions} = yield select(preferencesSelector)

  if (Object.keys(positions).length === 0) {
    positions = yield call(util.getPositionsFromColumns, columns)
  }

  const newPositions = yield call(util.changePosition, positions, field, targetField, horizontalDropPosition)
  yield put(setPositions(newPositions))
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`
  yield call(rest.deleteUserPreferences, `${actualFormName}.*.positions`)
  const positionPreferences = yield call(util.getPositionsPreferencesToSave, actualFormName, newPositions)
  yield call(rest.savePreferences, positionPreferences)
}

export function* saveSorting() {
  const {sorting: sortings} = yield select(listSagas.listSelector)
  if (sortings.length > 0) {
    const {formName} = yield select(docsListSelector)
    const actualFormName = `${formName}_${scope}`
    yield all([
      call(rest.deleteUserPreferences, `${actualFormName}.sortingField*`),
      call(rest.deleteUserPreferences, `${actualFormName}.sortingDirection*`)
    ])
    yield put(setSorting(sortings))
    const sortingPreferences = sortings
      .slice(1)
      .map((sorting, index) => util.getAdditionalSortingPreferencesToSave(actualFormName, sorting, index + 1))
      .reduce(
        (acc, sorting) => ({
          ...acc,
          ...sorting
        }),
        util.getSortingPreferencesToSave(actualFormName, sortings[0])
      )
    yield call(rest.savePreferences, sortingPreferences)
  }
}

export function* saveNumberOfTableRows(answerChannel) {
  const {numOfRows} = yield take(answerChannel)
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`

  yield put(setNumberOfTableRows(numOfRows))
  yield call(listSagas.reloadData)
  yield call(rest.savePreferences, {[`${actualFormName}.numOfRows`]: numOfRows})
}

export function* resetSorting() {
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`
  yield all([
    call(rest.deleteUserPreferences, `${actualFormName}.sortingField*`),
    call(rest.deleteUserPreferences, `${actualFormName}.sortingDirection*`)
  ])
  yield call(listSagas.setSorting)
  yield call(listSagas.reloadData)
}

export function* resetPreferences() {
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`
  yield call(rest.deleteUserPreferences, `${actualFormName}.*`)
  yield call(listSagas.setSorting)
  yield call(listSagas.reloadData)
}

export function* displayColumnModal() {
  const {formDefinition} = yield select(listSagas.listSelector)
  const {columns: preferencesColumns, positions: preferencesPositions} = yield select(preferencesSelector)
  const {parent} = yield select(docsListSelector)
  const formColumns = columnsUtil.sortColumnsByPreferences(
    formsApi.getTableColumns(formDefinition, parent, preferencesColumns),
    preferencesPositions
  )

  const sortColumns = columns => columnsUtil.sortColumnsForColumnModal(columns, formColumns, preferencesPositions)

  const answerChannel = yield call(channel)
  yield put(
    notification.modal(
      `${formDefinition.id}-column-selection`,
      'client.component.list.preferences.columns',
      null,
      ({close}) => {
        const onOk = columns => {
          close()
          answerChannel.put(columns)
        }

        return <ColumnModal onOk={onOk} initialColumns={formColumns} dndEnabled={false} sortColumns={sortColumns} />
      },
      true
    )
  )

  yield saveColumnPreferences(answerChannel, preferencesColumns, formDefinition)
}

export function* displayTableRowsModal() {
  const {formDefinition} = yield select(listSagas.listSelector)
  const answerChannel = yield call(channel)
  const {numOfRows: preferencesNumOfRows} = yield select(preferencesSelector)
  const pageLimitOptions = yield call(getAvailablePageLimitOptions)

  yield put(
    notification.modal(
      `${formDefinition.id}-numOfRows-setting`,
      'client.component.list.preferences.numOfRows',
      null,
      ({close}) => {
        const onOk = numOfRows => {
          close()
          answerChannel.put({numOfRows})
        }

        return <SelectNumRows onOk={onOk} pageLimitOptions={pageLimitOptions} numOfRows={preferencesNumOfRows} />
      },
      true
    )
  )

  yield call(saveNumberOfTableRows, answerChannel)
}

export function* getAvailablePageLimitOptions() {
  const pageLimitOptions = yield call(
    rest.fetchAllEntities,
    'Page_limit',
    {
      paths: 'unique_id',
      where: 'active'
    },
    {method: 'GET'}
  )
  return pageLimitOptions
    .map(api.getFlattenEntity)
    .map(option => option.unique_id)
    .map(_toNumber)
    .filter(option => !Number.isNaN(option))
}

function* saveColumnPreferences(answerChannel, preferencesColumns, formDefinition) {
  const columns = (yield take(answerChannel)).reduce(
    (accumulator, item) => ({
      ...accumulator,
      [item.id]: !item.hidden
    }),
    {}
  )
  const diffColumns = Object.keys(columns).reduce(
    (accumulator, columnName) => ({
      ...accumulator,
      ...(columns[columnName] !== preferencesColumns[columnName] ? {[columnName]: columns[columnName]} : {})
    }),
    {}
  )
  yield put(setColumns(columns))
  if (Object.entries(diffColumns).some(([, value]) => value)) {
    yield put(listActions.refresh())
  }
  const columnPreferences = yield call(util.getColumnPreferencesToSave, formDefinition.id, diffColumns)
  yield call(rest.savePreferences, columnPreferences)
}

export function* resetColumns() {
  const {formName} = yield select(docsListSelector)
  const actualFormName = `${formName}_${scope}`
  yield all([
    call(rest.deleteUserPreferences, `${actualFormName}.*.position`),
    call(rest.deleteUserPreferences, `${actualFormName}.*.hidden`)
  ])
  yield put(listActions.refresh())
}
