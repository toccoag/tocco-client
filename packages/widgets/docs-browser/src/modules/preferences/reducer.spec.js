import reducer from './'
import * as actions from './actions'

const EXPECTED_INITIAL_STATE = {
  positions: {},
  sorting: [],
  columns: {},
  widths: {},
  preferencesLoaded: false,
  numOfRows: undefined
}

describe('docs-list', () => {
  describe('modules', () => {
    describe('preferences', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        test('should clear sorting', () => {
          const previousSorting = {
            field: 'field',
            order: 'order'
          }
          expect(reducer({sorting: previousSorting}, actions.resetSorting()).sorting).to.eql([])
        })

        test('should clear column preferences', () => {
          const previousState = {
            positions: {field: 1},
            columns: {field: false},
            sorting: ['field']
          }
          const newState = reducer(previousState, actions.resetColumns())
          expect(newState.positions).to.eql({})
          expect(newState.columns).to.eql({})
          expect(newState.sorting).to.be.deep.eq(previousState.sorting)
        })

        test('should change width', () => {
          const newWidth = {
            field: 'field',
            width: 250
          }
          const expectedStateAfter = {
            widths: {otherField: 300, field: 250}
          }
          expect(
            reducer({widths: {otherField: 300}}, actions.changeWidth(newWidth.field, newWidth.width))
          ).to.deep.equal(expectedStateAfter)
        })
      })
    })
  })
})
