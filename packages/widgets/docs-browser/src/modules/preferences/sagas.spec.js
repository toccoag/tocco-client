import {channel} from 'redux-saga'
import {takeLatest, all, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'
import {preferencesUtil as util} from 'tocco-entity-list/src/main'
import {dragAndDrop} from 'tocco-util'

import * as listActions from '../list/actions'
import {listSelector} from '../list/sagas'
import * as listSagas from '../list/sagas'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('docs-list', () => {
  describe('modules', () => {
    describe('preferences', () => {
      describe('rootSaga', () => {
        test('should fork child sagas', () => {
          const generator = rootSaga()
          expect(generator.next().value).to.deep.equal(
            all([
              takeLatest(actions.LOAD_PREFERENCES, sagas.loadPreferences),
              takeLatest(actions.CHANGE_POSITION, sagas.changePosition),
              takeLatest(listActions.SET_SORTING_INTERACTIVE, sagas.saveSorting),
              takeLatest(actions.RESET_SORTING, sagas.resetSorting),
              takeLatest(actions.RESET_COLUMNS, sagas.resetColumns),
              takeLatest(actions.RESET_PREFERENCES, sagas.resetPreferences),
              takeLatest(actions.DISPLAY_COLUMN_MODAL, sagas.displayColumnModal),
              takeLatest(actions.DISPLAY_TABLE_ROWS_MODAL, sagas.displayTableRowsModal)
            ])
          )
          expect(generator.next().done).to.be.true
        })
        describe('loadPreferences', () => {
          test('should fetch user preferences and dispatch', () => {
            const preferences = {
              'User_list.firstname.position': '5',
              'Principal_list.sortingField': 'first_field',
              'Principal_list.sortingDirection': 'asc',
              'Principal_list.sortingField.1': 'second_field',
              'Principal_list.sortingDirection.1': 'desc'
            }

            const expectedSorting = [
              {
                field: 'first_field',
                order: 'asc'
              },
              {
                field: 'second_field',
                order: 'desc'
              }
            ]

            return expectSaga(sagas.loadPreferences)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [matchers.call.fn(rest.fetchUserPreferences), preferences]
              ])
              .put.actionType(actions.SET_POSITIONS)
              .put(actions.setSorting(expectedSorting))
              .run()
          })
        })

        describe('changePosition ', () => {
          test('should put new field on the right, delete preferences and save new one ', () => {
            return expectSaga(sagas.changePosition, {
              payload: {
                field: 'firstname',
                targetField: 'mail',
                horizontalDropPosition: dragAndDrop.HorizontalDropPosition.Right
              }
            })
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [select(sagas.preferencesSelector), {positions: {firstname: 0, mail: 1}}],
                [matchers.call.fn(rest.deleteUserPreferences)],
                [matchers.call.fn(rest.savePreferences)]
              ])
              .put.actionType(actions.SET_POSITIONS)
              .call(rest.savePreferences, {
                'Docs_list_item_list.mail.position': '0',
                'Docs_list_item_list.firstname.position': '1'
              })
              .call.like({fn: rest.deleteUserPreferences})
              .run()
          })

          test('should put new field on the left, delete preferences and save new one ', () => {
            return expectSaga(sagas.changePosition, {
              payload: {
                field: 'firstname',
                targetField: 'mail',
                horizontalDropPosition: dragAndDrop.HorizontalDropPosition.Left
              }
            })
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [select(sagas.preferencesSelector), {positions: {firstname: 1, mail: 0}}],
                [matchers.call.fn(rest.deleteUserPreferences)],
                [matchers.call.fn(rest.savePreferences)]
              ])
              .put.actionType(actions.SET_POSITIONS)
              .call(rest.savePreferences, {
                'Docs_list_item_list.mail.position': '1',
                'Docs_list_item_list.firstname.position': '0'
              })
              .call.like({fn: rest.deleteUserPreferences})
              .run()
          })
        })

        describe('saveSorting ', () => {
          test('should save sorting from list as preference', () => {
            const providedSorting = [
              {
                field: 'first_field',
                order: 'asc'
              },
              {
                field: 'second_field',
                order: 'desc'
              }
            ]
            const expectedPreferences = {
              'Docs_list_item_list.sortingField': 'first_field',
              'Docs_list_item_list.sortingDirection': 'asc',
              'Docs_list_item_list.sortingField.1': 'second_field',
              'Docs_list_item_list.sortingDirection.1': 'desc'
            }
            return expectSaga(sagas.saveSorting)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [select(listSelector), {sorting: providedSorting}],
                [matchers.call.fn(rest.deleteUserPreferences)],
                [matchers.call.fn(rest.savePreferences)]
              ])
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.sortingField*')
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.sortingDirection*')
              .put(actions.setSorting(providedSorting))
              .call(rest.savePreferences, expectedPreferences)
              .run()
          })

          test('should not save sorting if none exists', () => {
            return expectSaga(sagas.saveSorting)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [select(listSelector), {sorting: []}]
              ])
              .not.call.like({fn: rest.savePreferences})
              .not.call.like({fn: rest.deleteUserPreferences})
              .run()
          })
        })

        describe('resetSorting ', () => {
          test('should remove sorting preferences', () => {
            return expectSaga(sagas.resetSorting)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [matchers.call.fn(rest.deleteUserPreferences)],
                [matchers.call.fn(listSagas.setSorting)],
                [matchers.call.fn(listSagas.reloadData)]
              ])
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.sortingField*')
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.sortingDirection*')
              .call.like({fn: listSagas.setSorting})
              .call.like({fn: listSagas.reloadData})
              .run()
          })
        })

        describe('resetPreferences ', () => {
          test('should remove all preferences', () => {
            return expectSaga(sagas.resetPreferences)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [matchers.call.fn(rest.deleteUserPreferences)],
                [matchers.call.fn(listSagas.setSorting)],
                [matchers.call.fn(listSagas.reloadData)]
              ])
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.*')
              .call.like({fn: listSagas.setSorting})
              .call.like({fn: listSagas.reloadData})
              .run()
          })
        })

        describe('displayColumnModal ', () => {
          test('should open modal', () => {
            const preferencesSelector = {columns: {first_field: true, second_field: false, third_field: false}}
            return expectSaga(sagas.displayColumnModal)
              .provide([
                [
                  select(listSagas.listSelector),
                  {
                    formDefinition: {
                      id: 'Some_list',
                      children: [
                        {
                          componentType: 'table',
                          children: [
                            {
                              id: 'first_field'
                            },
                            {
                              id: 'second_field'
                            }
                          ]
                        }
                      ]
                    }
                  }
                ],
                [select(sagas.docsListSelector), {}],
                [select(sagas.preferencesSelector), preferencesSelector],
                [channel, {}],
                {
                  take() {
                    return [
                      {
                        id: 'first_field',
                        hidden: false
                      },
                      {
                        id: 'second_field',
                        hidden: true
                      },
                      {
                        id: 'third_field',
                        hidden: false // change from user input
                      }
                    ]
                  }
                },
                [matchers.call.fn(rest.savePreferences)]
              ])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'Some_list-column-selection',
                    title: 'client.component.list.preferences.columns',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .call.like({fn: channel})
              .put(actions.setColumns({first_field: true, second_field: false, third_field: true}))
              .call(util.getColumnPreferencesToSave, 'Some_list', {third_field: true})
              .call(rest.savePreferences, {'Some_list.third_field.hidden': 'false'})
              .put(listActions.refresh())
              .run()
          })
        })

        describe('displayTableRowsModal ', () => {
          test('should open modal and set num rows', () => {
            return expectSaga(sagas.displayTableRowsModal)
              .provide([
                [
                  select(listSagas.listSelector),
                  {
                    formDefinition: {
                      id: 'Docs_list_item_list'
                    }
                  }
                ],
                [select(sagas.preferencesSelector), {numOfRows: 25}],
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [matchers.call.fn(sagas.getAvailablePageLimitOptions), []],
                [channel, {}],
                {
                  take() {
                    return {numOfRows: 50}
                  }
                },
                [matchers.call.fn(listSagas.reloadData)],
                [matchers.call.fn(rest.savePreferences)]
              ])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'Docs_list_item_list-numOfRows-setting',
                    title: 'client.component.list.preferences.numOfRows',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .call.like({fn: channel})
              .put(actions.setNumberOfTableRows(50))
              .call(rest.savePreferences, {'Docs_list_item_list.numOfRows': 50})
              .call(listSagas.reloadData)
              .run()
          })
        })

        describe('getAvailablePageLimitOptions ', () => {
          test('should load page limits and return ids as limits', () => {
            const pageLimits = [
              {model: 'Page_limit', paths: {unique_id: {value: '25'}}},
              {model: 'Page_limit', paths: {unique_id: {value: '50'}}},
              {model: 'Page_limit', paths: {unique_id: {value: '100'}}},
              {model: 'Page_limit', paths: {unique_id: {value: 'wrong'}}}
            ]

            return expectSaga(sagas.getAvailablePageLimitOptions)
              .provide([[matchers.call.fn(rest.fetchAllEntities), pageLimits]])
              .returns([25, 50, 100])
              .run()
          })
        })

        describe('resetColumn', () => {
          test('should delete preferences', () => {
            return expectSaga(sagas.resetColumns)
              .provide([
                [select(sagas.docsListSelector), {formName: 'Docs_list_item'}],
                [matchers.call.fn(rest.deleteUserPreferences)]
              ])
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.*.position')
              .call(rest.deleteUserPreferences, 'Docs_list_item_list.*.hidden')
              .put(listActions.refresh())
              .run()
          })
        })
      })
    })
  })
})
