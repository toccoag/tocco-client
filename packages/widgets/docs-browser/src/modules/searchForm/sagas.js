import _reduce from 'lodash/reduce'
import {actions as formActions, getFormValues, isDirty} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {channel} from 'redux-saga'
import {all, call, put, select, take, takeLatest} from 'redux-saga/effects'
import {form, notification, rest} from 'tocco-app-extensions'
import {ColumnModal, formsApi, validateSearchFields} from 'tocco-entity-list/src/main'

import searchFormTypes from '../../utils/searchFormTypes'
import {SET_ENTITY_MODEL, SET_FORM_DEFINITION, refresh} from '../list/actions'
import {setSearchMode} from '../path/actions'

import * as actions from './actions'

export const searchFormSelector = state => state.searchForm
export const listSelector = state => state.list
export const docsListSelector = state => state.docsList
export const listFromDefinitionSelector = state => state.list.formDefinition
export const searchValuesSelector = getFormValues('searchForm')
export const isDirtySelector = isDirty('searchForm')

const FORM_ID = 'searchForm'

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE, initialize),
    takeLatest(formActionTypes.CHANGE, submitSearchFrom),
    takeLatest(actions.SUBMIT_SEARCH_FORM, submitSearchFrom),
    takeLatest(actions.RESET_SEARCH, resetSearch),
    takeLatest(actions.DISPLAY_SEARCH_FIELDS_MODAL, displaySearchFieldsModal),
    takeLatest(actions.RESET_SEARCH_FIELDS, resetSearchFields),
    takeLatest(actions.SAVE_PERSISTED_SEARCH_FORM, savePersistedSearchForm),
    takeLatest(actions.LOAD_PERSISTED_SEARCH_FORM, loadPersistedSearchForm),
    takeLatest(actions.CLEAR_PERSISTED_SEARCH_FORM, clearPersistedSearchForm)
  ])
}

export function* initialize() {
  const {searchFormType} = yield select(docsListSelector)
  const searchFormVisible = searchFormType !== searchFormTypes.NONE

  const formDefinition = yield call(initSearchFormType)
  yield call(setInitialFormValues, searchFormVisible, formDefinition)

  yield put(actions.setInitialized())
}

export function* initSearchFormType() {
  const {searchFormType} = yield select(docsListSelector)
  const searchFormVisible = searchFormType !== searchFormTypes.NONE

  const formDefinition = searchFormVisible ? yield call(loadSearchForm) : null
  return formDefinition
}

export function* getListFormDefinition() {
  const formDefinition = yield select(listFromDefinitionSelector)
  if (!formDefinition) {
    const action = yield take(SET_FORM_DEFINITION)
    return action.payload.formDefinition
  }

  return formDefinition
}

export function* setInitialFormValues(searchFormVisible, formDefinition) {
  const {parent} = yield select(docsListSelector)
  let formValues = {}

  if (parent) {
    const listFormDefinition = yield call(getListFormDefinition)
    const endpoint = yield call(formsApi.getEndpoint, listFormDefinition)
    if (!endpoint) {
      const display = yield call(rest.fetchDisplay, parent.model, parent.key)
      formValues[parent.reverseRelationName] = {key: parent.key, display}
    }
  }

  if (searchFormVisible && formDefinition) {
    const fieldDefinitions = yield call(form.getFieldDefinitions, formDefinition)
    const fromDefaultValues = yield call(form.getDefaultValues, fieldDefinitions)
    formValues = {...fromDefaultValues, ...formValues}
  }

  const transformedFromValues = yield call(transformFieldNames, formValues)
  const oldValues = yield select(getFormValues(FORM_ID))

  yield put(formActions.initialize(FORM_ID, transformedFromValues))
  if (oldValues) {
    yield all(Object.keys(oldValues).map(fieldId => put(formActions.change(FORM_ID, fieldId, oldValues[fieldId]))))
  }
  yield put(actions.setValuesInitialized(true))
}

export function transformFieldNames(formValues) {
  return _reduce(formValues, (acc, val, key) => ({...acc, [form.transformFieldName(key)]: val}), {})
}

export function* submitSearchFrom() {
  const values = yield select(getFormValues(FORM_ID))
  const dirty = yield select(isDirtySelector)
  const {formDefinition} = yield select(searchFormSelector)
  const errors = yield call(validateSearchFields, values, formDefinition)

  if (Object.keys(errors).length === 0) {
    yield put(setSearchMode(dirty))
    yield put(actions.executeSearch())
  }
}

export function* loadSearchForm(forceLoad = false) {
  // Don't use same formName as in list. WE always want to use Docs_list_item and never Root_docs_list_item.
  // This simplifies preferences and does not rerender search form after typing on the root.
  const formDefinition = yield call(rest.fetchForm, 'Docs_list_item', 'search', true, forceLoad)

  yield put(actions.setFormDefinition(formDefinition))
  yield put(actions.setFormFieldsFlat(formsApi.getFormFieldFlat(formDefinition)))
  return formDefinition
}

export function* getEntityModel() {
  const docsList = yield select(listSelector)
  if (Object.keys(docsList.entityModel).length === 0) {
    yield take(SET_ENTITY_MODEL)
  }

  const {entityModel} = yield select(listSelector)
  return entityModel
}

export function* resetSearch() {
  yield put(formActions.reset('searchForm'))
  yield call(submitSearchFrom)
}

export function* getSearchFormValues() {
  const {valuesInitialized} = yield select(searchFormSelector)

  if (!valuesInitialized) {
    yield take(actions.SET_VALUES_INITIALIZED)
  }

  const searchValues = yield select(searchValuesSelector)

  return _reduce(
    searchValues,
    (result, value, key) => ({
      ...result,
      ...(!Array.isArray(value) || value.length ? {[form.transformFieldNameBack(key)]: value} : {})
    }),
    {}
  )
}

export function* expandSearchFormValuesForLocalizedFields(searchFormValues) {
  const locales = yield call(rest.fetchLocales)
  if (locales.length > 1) {
    const languageSuffixes = locales.map(locale => locale.substring(0, 2))
    const {
      entityModel: {paths}
    } = yield select(listSelector)
    return Object.keys(searchFormValues)
      .filter(path => languageSuffixes.every(suffix => `${path}_${suffix}` in paths))
      .map(path => ({
        [path]: {
          localizedPaths: languageSuffixes.map(suffix => `${path}_${suffix}`),
          value: searchFormValues[path]
        }
      }))
      .reduce(
        (formValues, localized) => ({
          ...formValues,
          ...localized
        }),
        searchFormValues
      )
  } else {
    return searchFormValues
  }
}

export function* displaySearchFieldsModal() {
  const {formDefinition} = yield select(searchFormSelector)
  const columns = formDefinition.children
    .flatMap(f => f.children)
    .map(f => ({
      id: f.id,
      label: f.label,
      hidden: f.hidden === true
    }))
  const columnsSorted = [...columns.filter(f => !f.hidden), ...columns.filter(f => f.hidden)]

  const answerChannel = yield call(channel)
  yield put(
    notification.modal(
      `${formDefinition.id}-search-fields-selection`,
      'client.component.list.search.settings.searchForm.edit',
      null,
      ({close}) => {
        const onOk = pickedColumns => {
          close()
          answerChannel.put(pickedColumns)
        }

        return <ColumnModal onOk={onOk} initialColumns={columnsSorted} dndEnabled={true} />
      },
      true
    )
  )

  yield call(saveSearchFields, formDefinition.modelName, answerChannel)
  yield call(loadSearchForm, true)
  yield call(resetSearch)
}

function* saveSearchFields(modelName, answerChannel) {
  const columns = yield take(answerChannel)
  const resource = `forms/${modelName}/search-fields`
  const options = {
    method: 'POST',
    body: {
      hiddenFields: columns.filter(c => c.hidden).map(c => c.id),
      displayedFields: columns.filter(c => !c.hidden).map(c => c.id)
    }
  }

  yield call(rest.requestSaga, resource, options)
}

export function* resetSearchFields() {
  const {formDefinition} = yield select(searchFormSelector)
  const resource = `forms/${formDefinition.modelName}/search-fields/reset`
  const options = {
    method: 'POST'
  }

  yield call(rest.requestSaga, resource, options)
  yield call(loadSearchForm, true)
  yield call(resetSearch)
}

export function* refreshData() {
  yield put(refresh())
}

export function* savePersistedSearchForm() {
  const searchValues = yield select(searchValuesSelector)
  yield put(actions.setPersistedSearchForm(searchValues))
  yield put(formActions.reset(FORM_ID))
}

export function* loadPersistedSearchForm() {
  const {persistedSearchForm} = yield select(searchFormSelector)
  yield put(formActions.initialize(FORM_ID, persistedSearchForm))
}

export function* clearPersistedSearchForm() {
  yield call(resetSearch)
  yield put(actions.setPersistedSearchForm({}))
}
