export const SET_INITIALIZED = 'searchForm/SET_INITIALIZED'
export const INITIALIZE = 'searchForm/INITIALIZE'
export const SET_FORM_DEFINITION = 'searchForm/SET_FORM_DEFINITION'
export const EXECUTE_SEARCH = 'searchForm/EXECUTE_SEARCH'
export const RESET_SEARCH = 'searchForm/RESET_SEARCH'
export const SUBMIT_SEARCH_FORM = 'searchForm/SUBMIT_SEARCH_FORM'
export const SET_VALUES_INITIALIZED = 'searchForm/SET_VALUES_INITIALIZED'
export const SET_FORM_FIELDS_FLAT = 'searchForm/SET_FORM_FIELDS_FLAT'
export const DISPLAY_SEARCH_FIELDS_MODAL = 'searchForm/DISPLAY_SEARCH_FIELDS_MODAL'
export const RESET_SEARCH_FIELDS = 'searchForm/RESET_SEARCH_FIELDS'
export const SAVE_PERSISTED_SEARCH_FORM = 'searchForm/SAVE_PERSISTED_SEARCH_FORM'
export const LOAD_PERSISTED_SEARCH_FORM = 'searchForm/LOAD_PERSISTED_SEARCH_FORM'
export const SET_PERSISTED_SEARCH_FORM = 'searchForm/SET_PERSISTED_SEARCH_FORM'
export const CLEAR_PERSISTED_SEARCH_FORM = 'searchForm/CLEAR_PERSISTED_SEARCH_FORM'

export const setInitialized = (initialized = true) => ({
  type: SET_INITIALIZED,
  payload: {
    initialized
  }
})

export const initialize = () => ({
  type: INITIALIZE
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {
    formDefinition
  }
})

export const executeSearch = () => ({
  type: EXECUTE_SEARCH
})

export const resetSearch = () => ({
  type: RESET_SEARCH
})

export const submitSearchForm = () => ({
  type: SUBMIT_SEARCH_FORM
})

export const setValuesInitialized = valuesInitialized => ({
  type: SET_VALUES_INITIALIZED,
  payload: {
    valuesInitialized
  }
})

export const setFormFieldsFlat = formFieldsFlat => ({
  type: SET_FORM_FIELDS_FLAT,
  payload: {
    formFieldsFlat
  }
})

export const displaySearchFieldsModal = () => ({
  type: DISPLAY_SEARCH_FIELDS_MODAL
})

export const resetSearchFields = () => ({
  type: RESET_SEARCH_FIELDS
})

export const savePersistedSearchForm = () => ({
  type: SAVE_PERSISTED_SEARCH_FORM
})

export const loadPersistedSearchForm = () => ({
  type: LOAD_PERSISTED_SEARCH_FORM
})

export const setPersistedSearchForm = persistedSearchForm => ({
  type: SET_PERSISTED_SEARCH_FORM,
  payload: {
    persistedSearchForm
  }
})

export const clearPersistedSearchForm = () => ({
  type: CLEAR_PERSISTED_SEARCH_FORM
})
