import {actions as formActions} from 'redux-form'
import * as formActionTypes from 'redux-form/es/actionTypes'
import {channel} from 'redux-saga'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {form, rest} from 'tocco-app-extensions'
import {formsApi, validateSearchFields} from 'tocco-entity-list/src/main'

import {SET_ENTITY_MODEL, setFormDefinition} from '../list/actions'
import {setSearchMode} from '../path/actions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('docs-list', () => {
  describe('modules', () => {
    describe('searchForm', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.INITIALIZE, sagas.initialize),
                takeLatest(formActionTypes.CHANGE, sagas.submitSearchFrom),
                takeLatest(actions.SUBMIT_SEARCH_FORM, sagas.submitSearchFrom),
                takeLatest(actions.RESET_SEARCH, sagas.resetSearch),
                takeLatest(actions.DISPLAY_SEARCH_FIELDS_MODAL, sagas.displaySearchFieldsModal),
                takeLatest(actions.RESET_SEARCH_FIELDS, sagas.resetSearchFields),
                takeLatest(actions.SAVE_PERSISTED_SEARCH_FORM, sagas.savePersistedSearchForm),
                takeLatest(actions.LOAD_PERSISTED_SEARCH_FORM, sagas.loadPersistedSearchForm),
                takeLatest(actions.CLEAR_PERSISTED_SEARCH_FORM, sagas.clearPersistedSearchForm)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('initializeSearchForm saga', () => {
          test('should load form, initialvalues and set initialized status', () => {
            const formDefinition = []
            const searchFormType = 'admin'

            return expectSaga(sagas.initialize, actions.initialize())
              .provide([
                [select(sagas.docsListSelector), {searchFormType}],
                [matchers.call.fn(sagas.loadSearchForm), formDefinition],
                [matchers.call.fn(sagas.setInitialFormValues)]
              ])
              .put(actions.setInitialized())
              .run()
          })
        })

        describe('setInitialFormValues saga', () => {
          test('should set initial form values', () => {
            const searchFormVisible = true
            const FORM_ID = 'searchForm'
            const entityModel = {paths: [{fieldName: 'firstname', type: 'string'}]}
            const formDefinition = {children: []}
            const fieldDefinitions = [
              {id: 'first.name', path: 'first.name', type: 'string', defaultValue: 'test'},
              {id: 'lastname', path: 'lastname', type: 'string'},
              {id: 'defaultTest', path: 'defaultTest', type: 'string', defaultValue: 'default'}
            ]
            const expectedValues = {'first--name': 'test', defaultTest: 'default'}

            return expectSaga(sagas.setInitialFormValues, searchFormVisible, formDefinition)
              .provide([
                [select(sagas.docsListSelector), {}],
                [matchers.call.fn(sagas.getEntityModel), entityModel],
                [matchers.call.fn(form.getFieldDefinitions), fieldDefinitions]
              ])
              .put(formActions.initialize(FORM_ID, expectedValues))
              .put(actions.setValuesInitialized(true))
              .run()
          })

          test('should set parent relation by default', () => {
            const FORM_ID = 'searchForm'
            const parent = {key: '22', reverseRelationName: 'relWhatever'}
            const expectedValues = {relWhatever: {key: '22', display: 'Test User'}}

            return expectSaga(sagas.setInitialFormValues, false, null)
              .provide([
                [select(sagas.docsListSelector), {parent}],
                [matchers.call.fn(sagas.getListFormDefinition), null],
                [matchers.call.fn(formsApi.getEndpoint), null],
                [matchers.call.fn(rest.fetchDisplay), 'Test User']
              ])
              .put(formActions.initialize(FORM_ID, expectedValues))
              .run()
          })

          test('should set parent relation if custom endpoint is defined', () => {
            const searchFormVisible = true
            const FORM_ID = 'searchForm'
            const parent = {key: '22', reverseRelationName: 'relWhatever'}
            const formDefinition = {children: []}
            const expectedValues = {}

            return expectSaga(sagas.setInitialFormValues, searchFormVisible, formDefinition)
              .provide([
                [select(sagas.docsListSelector), {parent}],
                [matchers.call.fn(sagas.getListFormDefinition), null],
                [matchers.call.fn(formsApi.getEndpoint), '/custom/']
              ])
              .put(formActions.initialize(FORM_ID, expectedValues))
              .run()
          })
        })

        describe('submitSearchFrom saga', () => {
          test('should call executeSearch if no validation error occures', () => {
            const values = {firstname: 'a'}
            const formDefinition = {}
            const gen = sagas.submitSearchFrom()
            gen.next() // Function call not testable: expect(gen.next().value).to.eql(select(getFormValues(FORM_ID)))
            gen.next(values) // Function call not testable: expect(gen.next().value).to.eql(select(isDirtySelector))
            expect(gen.next(false).value).to.eql(select(sagas.searchFormSelector))
            expect(gen.next({formDefinition}).value).to.eql(call(validateSearchFields, values, formDefinition))
            const errors = {}
            expect(gen.next(errors).value).to.eql(put(setSearchMode(false)))
            expect(gen.next().value).to.eql(put(actions.executeSearch()))
            expect(gen.next().done).to.be.true
          })

          test('should not call executeSearch if validation error occures', () => {
            const values = {firstname: 'a'}
            const formDefinition = {}
            const gen = sagas.submitSearchFrom()
            gen.next() // Function call not testable: expect(gen.next().value).to.eql(select(getFormValues(FORM_ID)))
            gen.next(values) // Function call not testable: expect(gen.next().value).to.eql(select(isDirtySelector))
            expect(gen.next(false).value).to.eql(select(sagas.searchFormSelector))
            expect(gen.next({formDefinition}).value).to.eql(call(validateSearchFields, values, formDefinition))
            const errors = {firstname: {minLength: ['to short!']}}
            expect(gen.next(errors).done).to.be.true
          })
        })

        describe('getSearchFormValues saga', () => {
          test('should return values of search form', () => {
            const searchValues = {
              relGender: '1',
              'nameto--transform': 'test',
              emptyArray: []
            }

            const expectedReturn = {
              relGender: '1',
              'nameto.transform': 'test'
            }

            return expectSaga(sagas.getSearchFormValues)
              .provide([
                [select(sagas.searchFormSelector), {valuesInitialized: true}],
                [select(sagas.searchValuesSelector), searchValues]
              ])
              .returns(expectedReturn)
              .run()
          })
        })

        describe('expandSearchFormValuesForLocalizedFields', () => {
          const paths = {
            unique_id: {},
            label: {},
            label_en: {},
            label_de: {}
          }
          const locales = ['en', 'de_CH']
          test('should expand values for localized field', () => {
            const formValues = {
              label: 'value'
            }
            const expectedReturn = {
              label: {
                localizedPaths: ['label_en', 'label_de'],
                value: 'value'
              }
            }
            return expectSaga(sagas.expandSearchFormValuesForLocalizedFields, formValues)
              .provide([
                [matchers.call.fn(rest.fetchLocales), locales],
                [select(sagas.listSelector), {entityModel: {paths}}]
              ])
              .returns(expectedReturn)
              .run()
          })
          test('should ignore unlocalized field', () => {
            const formValues = {
              unique_id: 'value'
            }
            return expectSaga(sagas.expandSearchFormValuesForLocalizedFields, formValues)
              .provide([
                [matchers.call.fn(rest.fetchLocales), locales],
                [select(sagas.listSelector), {entityModel: {paths}}]
              ])
              .returns(formValues)
              .run()
          })
          test('should ignore partially localized field', () => {
            const partialPaths = {
              label: {},
              label_de: {}
            }
            const formValues = {
              label: 'value'
            }
            return expectSaga(sagas.expandSearchFormValuesForLocalizedFields, formValues)
              .provide([
                [matchers.call.fn(rest.fetchLocales), locales],
                [select(sagas.listSelector), {entityModel: {paths: partialPaths}}]
              ])
              .returns(formValues)
              .run()
          })
          test('should return values unchanged when only a single locale is available', () => {
            const partialLocales = ['de_CH']
            const formValues = {
              label: 'value'
            }
            return expectSaga(sagas.expandSearchFormValuesForLocalizedFields, formValues)
              .provide([
                [matchers.call.fn(rest.fetchLocales), partialLocales],
                [select(sagas.listSelector), {entityModel: {paths}}]
              ])
              .returns(formValues)
              .run()
          })
        })

        describe('resetSearch saga', () => {
          test('should trigger submit to reload', () =>
            expectSaga(sagas.resetSearch)
              .provide([[matchers.call.fn(sagas.submitSearchFrom), undefined]])
              .put(formActions.reset('searchForm'))
              .call(sagas.submitSearchFrom)
              .run())
        })

        describe('loadSearchForm saga', () => {
          test('should load the form definition and dispatch setFormDefinition', () => {
            const formDefinition = {children: []}

            return expectSaga(sagas.loadSearchForm)
              .provide([
                [matchers.call.fn(rest.fetchForm), formDefinition],
                [select(sagas.docsListSelector), {searchFormType: 'admin'}]
              ])
              .put(actions.setFormDefinition(formDefinition))
              .returns(formDefinition)
              .run()
          })
        })

        describe('getEntityModel saga', () => {
          test('should return the entity model as soon as initialized', () => {
            const entityModel = {fields: []}
            return expectSaga(sagas.getEntityModel)
              .provide([[select(sagas.listSelector), {entityModel}]])
              .dispatch({type: SET_ENTITY_MODEL})
              .returns(entityModel)
              .run()
          })
        })

        describe('getListFormDefinition saga', () => {
          test('should return the ist form definition as soon as loaded', () => {
            const formDefinition = {table: {}}
            return expectSaga(sagas.getListFormDefinition)
              .provide([[select(sagas.listFromDefinitionSelector), null]])
              .dispatch(setFormDefinition(formDefinition))
              .returns(formDefinition)
              .run()
          })
        })

        describe('displaySearchFieldsModal saga', () => {
          test('should open modal and save search fields', () => {
            const fields = [
              {
                id: 'firstname',
                label: 'firstname',
                hidden: true
              },
              {
                id: 'lastname',
                label: 'lastname',
                hidden: false
              },
              {
                id: 'birthdate',
                label: 'birthdate',
                hidden: false
              }
            ]
            const formDefinition = {
              id: 'User_search',
              modelName: 'User',
              children: [
                {
                  children: fields
                }
              ]
            }

            const body = {
              hiddenFields: ['firstname'],
              displayedFields: ['lastname', 'birthdate']
            }

            return expectSaga(sagas.displaySearchFieldsModal)
              .provide([
                [select(sagas.searchFormSelector), {formDefinition}],
                [channel, {}],
                {
                  take() {
                    return fields
                  }
                },
                [matchers.call.fn(rest.requestSaga), {}],
                [matchers.call.fn(sagas.loadSearchForm), {}]
              ])
              .put.like({
                action: {
                  type: 'notification/MODAL',
                  payload: {
                    id: 'User_search-search-fields-selection',
                    title: 'client.component.list.search.settings.searchForm.edit',
                    message: null,
                    cancelable: true
                  }
                }
              })
              .call.like({fn: channel})
              .call(rest.requestSaga, 'forms/User/search-fields', {method: 'POST', body})
              .call(sagas.loadSearchForm, true)
              .call(sagas.resetSearch)
              .run()
          })
        })

        describe('resetSearchFields saga', () => {
          test('should reset search fields', () => {
            const formDefinition = {
              modelName: 'User'
            }
            return expectSaga(sagas.resetSearchFields)
              .provide([
                [select(sagas.searchFormSelector), {formDefinition}],
                [matchers.call.fn(rest.requestSaga), {}],
                [matchers.call.fn(sagas.loadSearchForm), {}]
              ])
              .call(rest.requestSaga, 'forms/User/search-fields/reset', {method: 'POST'})
              .call(sagas.loadSearchForm, true)
              .call(sagas.resetSearch)
              .run()
          })
        })
      })
    })
  })
})
