export const SET_INITIALIZED = 'docsList/SET_INITIALIZED'
export const INITIALIZE = 'docsList/INITIALIZE'
export const RE_INITIALIZE = 'docsList/RE_INITIALIZE'
export const SET_PARENT = 'docsList/SET_PARENT'
export const SET_LIMIT = 'docsList/SET_LIMIT'
export const SET_FORM_NAME = 'docsList/SET_FORM_NAME'
export const SET_SEARCH_FORM_TYPE = 'docsList/SET_SEARCH_FORM_TYPE'
export const SET_TQL = 'docsList/SET_TQL'
export const SET_KEYS = 'docsList/SET_KEYS'
export const SET_SEARCH_FORM_COLLAPSED = 'docsList/SET_SEARCH_FORM_COLLAPSED'
export const SET_SEARCH_FORM_COLLAPSED_INITIAL_VALUE = 'docsList/SET_SEARCH_FORM_COLLAPSED_INITIAL_VALUE'
export const SHOW_PENDING_CHANGES_MODAL = 'docsList/SHOW_PENDING_CHANGES_MODAL'

export const setInitialized = (initialized = true) => ({
  type: SET_INITIALIZED,
  payload: {
    initialized
  }
})

export const initialize = (formName, searchFormType, limit, parent, tql, keys) => ({
  type: INITIALIZE,
  payload: {
    formName,
    searchFormType,
    limit,
    parent,
    tql,
    keys
  }
})

export const reinitialize = () => ({
  type: RE_INITIALIZE
})

export const setParent = parent => ({
  type: SET_PARENT,
  payload: {
    parent
  }
})

export const setLimit = limit => ({
  type: SET_LIMIT,
  payload: {
    limit
  }
})

export const setFormName = formName => ({
  type: SET_FORM_NAME,
  payload: {
    formName
  }
})

export const setSearchFormType = searchFormType => ({
  type: SET_SEARCH_FORM_TYPE,
  payload: {
    searchFormType
  }
})

export const setSearchFormCollapsed = searchFormCollapsed => ({
  type: SET_SEARCH_FORM_COLLAPSED,
  payload: {
    searchFormCollapsed
  }
})

export const setSearchFormCollapsedInitialValue = searchFormCollapsed => ({
  type: SET_SEARCH_FORM_COLLAPSED_INITIAL_VALUE,
  payload: {
    searchFormCollapsed
  }
})

export const setTql = tql => ({
  type: SET_TQL,
  payload: {
    tql
  }
})

export const setKeys = keys => ({
  type: SET_KEYS,
  payload: {
    keys
  }
})

export const showPendingChangesModal = (message, confirmCallback) => ({
  type: SHOW_PENDING_CHANGES_MODAL,
  payload: {
    message,
    confirmCallback
  }
})
