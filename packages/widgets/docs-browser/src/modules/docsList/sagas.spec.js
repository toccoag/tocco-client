import {takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import {externalEvents} from 'tocco-app-extensions'

import * as listActions from './../list/actions'
import * as preferenceActions from './../preferences/actions'
import * as searchFormActions from './../searchForm/actions'
import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('docs-browser', () => {
  describe('modules', () => {
    describe('docsList', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.INITIALIZE, sagas.initialize),
                takeLatest(actions.RE_INITIALIZE, sagas.reinitialize),
                takeLatest(actions.SET_SEARCH_FORM_COLLAPSED, sagas.setSearchFormCollapsed),
                takeLatest(actions.SHOW_PENDING_CHANGES_MODAL, sagas.showPendingChangesModal)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('initialize', () => {
          test('should coordinate loading of modules and dependencies', () => {
            const formName = 'Docs_list_item_list'
            const searchFormType = 'admin'
            const limit = 25
            const parent = undefined
            const tql = undefined
            const keys = undefined
            return expectSaga(sagas.initialize, {payload: {formName, searchFormType, limit, parent, tql, keys}})
              .put(actions.setFormName(formName))
              .put(actions.setLimit(limit))
              .put(actions.setParent(parent))
              .put(actions.setSearchFormType(searchFormType))
              .put(actions.setTql(tql))
              .put(actions.setKeys(keys))
              .put(listActions.initialize())
              .dispatch({type: listActions.SET_INITIALIZED})
              .put(searchFormActions.initialize())
              .dispatch({type: searchFormActions.SET_INITIALIZED})
              .put(preferenceActions.loadPreferences())
              .dispatch({type: preferenceActions.SET_PREFERENCES_LOADED})
              .put(listActions.defineSorting())
              .dispatch({type: listActions.SET_SORTING})
              .put(searchFormActions.executeSearch())
              .run()
          })
        })

        describe('reinitialize', () => {
          test('should coordinate reinitialize of modules and dependencies', () => {
            return expectSaga(sagas.reinitialize)
              .put(listActions.initialize())
              .dispatch({type: listActions.SET_INITIALIZED})
              .put(searchFormActions.initialize())
              .dispatch({type: searchFormActions.SET_INITIALIZED})
              .put(preferenceActions.loadPreferences())
              .dispatch({type: preferenceActions.SET_PREFERENCES_LOADED})
              .put(listActions.defineSorting())
              .dispatch({type: listActions.SET_SORTING})
              .put(searchFormActions.executeSearch())
              .run()
          })
        })

        describe('setSearchFormCollapsed', () => {
          test('should fire external event', () => {
            return expectSaga(sagas.setSearchFormCollapsed, {payload: {searchFormCollapsed: true}})
              .put(externalEvents.fireExternalEvent('onSearchFormCollapsedChange', {collapsed: true}))
              .run()
          })
        })
      })
    })
  })
})
