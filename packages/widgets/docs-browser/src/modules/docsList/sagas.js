import {channel} from 'redux-saga'
import {put, all, take, takeLatest, call} from 'redux-saga/effects'
import {externalEvents, notification} from 'tocco-app-extensions'
import {PendingChangesModal} from 'tocco-ui'

import * as listActions from '../list/actions'
import * as preferenceActions from '../preferences/actions'
import * as searchFormActions from '../searchForm/actions'

import * as actions from './actions'

export const docsListSelector = state => state.docsList

export default function* sagas() {
  yield all([
    takeLatest(actions.INITIALIZE, initialize),
    takeLatest(actions.RE_INITIALIZE, reinitialize),
    takeLatest(actions.SET_SEARCH_FORM_COLLAPSED, setSearchFormCollapsed),
    takeLatest(actions.SHOW_PENDING_CHANGES_MODAL, showPendingChangesModal)
  ])
}

export function* initialize({payload: {formName, searchFormType, limit, parent, tql, keys}}) {
  yield put(actions.setFormName(formName))
  yield put(actions.setLimit(limit))
  yield put(actions.setParent(parent))
  yield put(actions.setSearchFormType(searchFormType))
  yield put(actions.setTql(tql))
  yield put(actions.setKeys(keys))

  yield put(listActions.initialize())
  yield take(listActions.SET_INITIALIZED)

  yield put(searchFormActions.initialize())
  yield take(searchFormActions.SET_INITIALIZED)

  yield put(preferenceActions.loadPreferences())
  yield take(preferenceActions.SET_PREFERENCES_LOADED)

  yield put(listActions.defineSorting())
  yield take(listActions.SET_SORTING)

  yield put(searchFormActions.executeSearch())
}

export function* reinitialize() {
  yield put(listActions.initialize())
  yield take(listActions.SET_INITIALIZED)

  yield put(searchFormActions.initialize())
  yield take(searchFormActions.SET_INITIALIZED)

  yield put(preferenceActions.loadPreferences())
  yield take(preferenceActions.SET_PREFERENCES_LOADED)

  yield put(listActions.defineSorting())
  yield take(listActions.SET_SORTING)

  yield put(searchFormActions.executeSearch())
}

export function* setSearchFormCollapsed({payload: {searchFormCollapsed}}) {
  yield put(externalEvents.fireExternalEvent('onSearchFormCollapsedChange', {collapsed: searchFormCollapsed}))
}

export function* promptConfirm(message) {
  const answerChannel = yield call(channel)

  yield put(
    notification.modal(
      'navigation-pending-changes',
      'client.docs-browser.detail.confirmTouchedFormTitle',
      message,
      ({close}) => {
        const onYes = () => {
          close()
          answerChannel.put('yes')
        }
        const onNo = () => {
          close()
          answerChannel.put('no')
        }
        return <PendingChangesModal onYes={onYes} onNo={onNo} />
      },
      true,
      () => answerChannel.put('no')
    )
  )
  return yield take(answerChannel)
}

export function* showPendingChangesModal({payload}) {
  const {message, confirmCallback} = payload
  const response = yield call(promptConfirm, message)
  if (response === 'yes') {
    yield call(confirmCallback, true)
  } else {
    yield call(confirmCallback, false)
  }
}
