import searchFormTypes from '../../utils/searchFormTypes'

import * as actions from './actions'
import reducer from './index'

const INITIAL_STATE = {
  initialized: false,
  searchFormType: searchFormTypes.ADMIN,
  searchFormCollapsed: false,
  formName: '',
  limit: 25,
  parent: null,
  tql: null,
  keys: null
}

describe('docs-browser', () => {
  describe('modules', () => {
    describe('docsList', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(INITIAL_STATE)
        })

        test('should handle allow only valid searchFormTypes', () => {
          let state = INITIAL_STATE
          state = reducer(state, actions.setSearchFormType(searchFormTypes.ADMIN))
          expect(state.searchFormType).to.be.eql(searchFormTypes.ADMIN)
          state = reducer(state, actions.setSearchFormType('simple'))
          expect(state.searchFormType).to.be.eql(searchFormTypes.NONE)
          state = reducer(state, actions.setSearchFormType(searchFormTypes.NONE))
          expect(state.searchFormType).to.be.eql(searchFormTypes.NONE)
          state = reducer(state, actions.setSearchFormType('gugus'))
          expect(state.searchFormType).to.be.eql(searchFormTypes.NONE)
          state = reducer(state, actions.setSearchFormType(''))
          expect(state.searchFormType).to.be.eql(searchFormTypes.NONE)
        })
      })
    })
  })
})
