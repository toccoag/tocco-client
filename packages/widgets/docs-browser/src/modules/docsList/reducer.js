import {reducer as reducerUtil} from 'tocco-util'

import searchFormTypes from '../../utils/searchFormTypes'

import * as actions from './actions'

const setSearchFormType = (state, {payload}) => {
  const {searchFormType: newSearchFormType} = payload

  const searchFormType =
    Object.values(searchFormTypes).find(enumV => enumV === newSearchFormType) || searchFormTypes.NONE

  return {...state, searchFormType}
}

const ACTION_HANDLERS = {
  [actions.SET_INITIALIZED]: reducerUtil.singleTransferReducer('initialized'),
  [actions.SET_PARENT]: reducerUtil.singleTransferReducer('parent'),
  [actions.SET_LIMIT]: reducerUtil.singleTransferReducer('limit'),
  [actions.SET_FORM_NAME]: reducerUtil.singleTransferReducer('formName'),
  [actions.SET_SEARCH_FORM_TYPE]: setSearchFormType,
  [actions.SET_TQL]: reducerUtil.singleTransferReducer('tql'),
  [actions.SET_KEYS]: reducerUtil.singleTransferReducer('keys'),
  [actions.SET_SEARCH_FORM_COLLAPSED]: reducerUtil.singleTransferReducer('searchFormCollapsed'),
  [actions.SET_SEARCH_FORM_COLLAPSED_INITIAL_VALUE]: reducerUtil.singleTransferReducer('searchFormCollapsed')
}

const initialState = {
  initialized: false,
  searchFormType: searchFormTypes.ADMIN,
  searchFormCollapsed: false,
  formName: '',
  limit: 25,
  parent: null,
  tql: null,
  keys: null
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
