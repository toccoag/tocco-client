import {combineReducers} from 'redux'
import {reducer as form} from 'redux-form'
import {selectionReducer, selectionSagas} from 'tocco-entity-list/src/main'

import create, {sagas as createSagas} from './create'
import docsListReducer, {sagas as docsListSagas} from './docsList'
import listReducer, {sagas as listSagas} from './list'
import move, {sagas as moveSagas} from './move'
import path, {sagas as pathSagas} from './path'
import preferencesReducer, {sagas as preferencesSaga} from './preferences'
import routing, {sagas as routingSagas} from './routing'
import searchFormReducer, {sagas as searchFormSagas} from './searchForm'

export default {
  docs: combineReducers({path, create, move, routing}),
  docsList: docsListReducer,
  list: listReducer,
  searchForm: searchFormReducer,
  form,
  selection: selectionReducer,
  preferences: preferencesReducer
}

export const sagas = [
  pathSagas,
  createSagas,
  listSagas,
  moveSagas,
  routingSagas,
  docsListSagas,
  searchFormSagas,
  selectionSagas,
  preferencesSaga
]
