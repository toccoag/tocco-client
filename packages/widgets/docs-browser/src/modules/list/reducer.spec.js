import * as actions from './actions'
import reducer from './index'

const EXPECTED_INITIAL_STATE = {
  entityModel: {},
  entities: [],
  currentPage: 1,
  sorting: [],
  formDefinition: null,
  searchListFormDefinition: null,
  entityCount: null,
  entityStore: {},
  inProgress: true,
  createPermission: false
}

describe('docs-browser', () => {
  describe('modules', () => {
    describe('list', () => {
      describe('reducer', () => {
        test('should create a valid initial state', () => {
          expect(reducer(undefined, {})).to.deep.equal(EXPECTED_INITIAL_STATE)
        })

        test('should handle CLEAR_ENTITY_STORE', () => {
          const stateBefore = {
            entityStore: {
              entities: {
                1: [{}]
              }
            }
          }

          const expectedStateAfter = {
            entityStore: {}
          }

          expect(reducer(stateBefore, actions.clearEntityStore())).to.deep.equal(expectedStateAfter)
        })

        test('should handle ADD_ENTITIES_TO_STORE', () => {
          const existingEntities = {
            1: [{field: 'value1'}, {field: 'value2'}]
          }
          const newEntities = [{field: 'value3'}, {field: 'value4'}]

          const mergedEntities = {
            1: [{field: 'value1'}, {field: 'value2'}],
            2: [{field: 'value3'}, {field: 'value4'}]
          }

          const stateBefore = {
            entityStore: existingEntities
          }

          const expectedStateAfter = {
            entityStore: mergedEntities
          }

          expect(reducer(stateBefore, actions.addEntitiesToStore(2, newEntities))).to.deep.equal(expectedStateAfter)
        })

        test('should handle SET_SORTING_INTERACTIVE and switch order on same field', () => {
          const stateBefore = {
            sorting: [{field: 'lastname', order: 'asc'}]
          }

          const expectedStateAfter = {
            sorting: [{field: 'firstname', order: 'asc'}]
          }

          expect(reducer(stateBefore, actions.setSortingInteractive('firstname', false))).to.deep.equal(
            expectedStateAfter
          )

          const expectedStateAfter2 = {
            sorting: [{field: 'firstname', order: 'desc'}]
          }

          expect(reducer(expectedStateAfter, actions.setSortingInteractive('firstname', false))).to.deep.equal(
            expectedStateAfter2
          )
        })

        test('should handle SET_SORTING_INTERACTIVE and add a second sorting', () => {
          const stateBefore = {
            sorting: [{field: 'lastname', order: 'asc'}]
          }

          const expectedStateAfter = {
            sorting: [
              {field: 'lastname', order: 'asc'},
              {field: 'firstname', order: 'asc'}
            ]
          }

          expect(reducer(stateBefore, actions.setSortingInteractive('firstname', true))).to.deep.equal(
            expectedStateAfter
          )

          const expectedStateAfter2 = {
            sorting: [
              {field: 'lastname', order: 'asc'},
              {field: 'firstname', order: 'desc'}
            ]
          }

          expect(reducer(expectedStateAfter, actions.setSortingInteractive('firstname', true))).to.deep.equal(
            expectedStateAfter2
          )
        })
      })
    })
  })
})
