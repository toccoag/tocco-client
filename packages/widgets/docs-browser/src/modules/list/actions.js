export const INITIALIZE = 'list/INITIALIZE'
export const SET_ENTITIES = 'list/SET_ENTITIES'
export const SET_FORM_DEFINITION = 'list/SET_FORM_DEFINITION'
export const SET_SEARCH_LIST_FORM_DEFINITION = 'list/SET_SEARCH_LIST_FORM_DEFINITION'
export const SET_ENTITY_MODEL = 'list/SET_ENTITY_MODEL'
export const SET_SORTING = 'list/SET_SORTING'
export const SET_CURRENT_PAGE = 'list/SET_CURRENT_PAGE'
export const SET_ENTITY_COUNT = 'list/SET_ENTITY_COUNT'
export const ADD_ENTITIES_TO_STORE = 'list/ADD_ENTITIES_TO_STORE'
export const CLEAR_ENTITY_STORE = 'list/CLEAR_ENTITIES_CACHE'
export const SET_IN_PROGRESS = 'list/SET_IN_PROGRESS'
export const CHANGE_PAGE = 'list/CHANGE_PAGE'
export const REFRESH = 'list/REFRESH'
export const ON_ROW_CLICK = 'list/ON_ROW_CLICK'
export const SET_SORTING_INTERACTIVE = 'list/SET_SORTING_INTERACTIVE'
export const DEFINE_SORTING = 'list/DEFINE_SORTING'
export const SET_INITIALIZED = 'list/SET_INITIALIZED'
export const CHANGE_LIST_PARENT = 'docs/list/CHANGE_LIST_PARENT'

export const changeListParent = parent => ({
  type: CHANGE_LIST_PARENT,
  payload: {
    parent
  }
})

export const initialize = () => ({
  type: INITIALIZE
})

export const setEntities = entities => ({
  type: SET_ENTITIES,
  payload: {
    entities
  }
})

export const addEntitiesToStore = (page, entities) => ({
  type: ADD_ENTITIES_TO_STORE,
  payload: {
    page,
    entities
  }
})

export const clearEntityStore = () => ({
  type: CLEAR_ENTITY_STORE
})

export const setEntityCount = entityCount => ({
  type: SET_ENTITY_COUNT,
  payload: {
    entityCount
  }
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {
    formDefinition
  }
})

export const setSearchListFormDefinition = searchListFormDefinition => ({
  type: SET_SEARCH_LIST_FORM_DEFINITION,
  payload: {
    searchListFormDefinition
  }
})

export const setEntityModel = entityModel => ({
  type: SET_ENTITY_MODEL,
  payload: {
    entityModel
  }
})

export const setSorting = sorting => ({
  type: SET_SORTING,
  payload: {
    sorting
  }
})

export const setCurrentPage = currentPage => ({
  type: SET_CURRENT_PAGE,
  payload: {
    currentPage
  }
})

export const setInProgress = inProgress => ({
  type: SET_IN_PROGRESS,
  payload: {
    inProgress
  }
})

export const changePage = page => ({
  type: CHANGE_PAGE,
  payload: {
    page
  }
})

export const refresh = () => ({
  type: REFRESH
})

export const onRowClick = id => ({
  type: ON_ROW_CLICK,
  payload: {
    id
  }
})

export const setSortingInteractive = (field, add) => ({
  type: SET_SORTING_INTERACTIVE,
  payload: {
    field,
    add
  }
})

export const defineSorting = () => ({
  type: DEFINE_SORTING
})

export const setInitialized = () => ({
  type: SET_INITIALIZED
})
