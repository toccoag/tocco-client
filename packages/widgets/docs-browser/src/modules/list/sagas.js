import {all, call, delay, fork, put, select, spawn, take, takeEvery, takeLatest} from 'redux-saga/effects'
import {actionEmitter, externalEvents, remoteEvents, rest} from 'tocco-app-extensions'
import {entitiesListTransformer, formsApi, selectionActions} from 'tocco-entity-list/src/main'

import {entityName, scope, searchListFormName} from '../../utils/constants'
import {getTqlFromSearchForm} from '../../utils/fetchOptions'
import * as preferencesActions from '../preferences/actions'
import * as searchFormActions from '../searchForm/actions'
import {getSearchFormValues, expandSearchFormValuesForLocalizedFields} from '../searchForm/sagas'
import {getActualLimit, getFormDefinition} from '../selectors'

import * as actions from './actions'

export const stateSelector = state => state
export const inputSelector = state => state.input
export const docsListSelector = state => state.docsList
export const listSelector = state => state.list
export const searchFormSelector = state => state.searchForm
export const selectionSelector = state => state.selection
export const preferencesSelector = state => state.preferences

export default function* mainSagas() {
  yield all([
    takeLatest(actions.CHANGE_LIST_PARENT, changeListParent),
    takeLatest(actions.INITIALIZE, initialize),
    takeLatest(actions.CHANGE_PAGE, changePage),
    takeLatest(searchFormActions.EXECUTE_SEARCH, reloadData),
    takeLatest(searchFormActions.EXECUTE_SEARCH, queryChanged),
    takeLatest(actions.SET_SORTING_INTERACTIVE, reloadData),
    takeLatest(actions.REFRESH, refreshData),
    takeLatest(selectionActions.RELOAD_DATA, reloadData),
    takeLatest(actions.ON_ROW_CLICK, onRowClick),
    takeEvery(remoteEvents.REMOTE_EVENT, remoteEvent),
    takeLatest(actions.DEFINE_SORTING, setSorting)
  ])
}

export function* changeListParent({payload}) {
  yield put(externalEvents.fireExternalEvent('onListParentChange', payload.parent))
}

export function* initialize() {
  const {formName} = yield select(docsListSelector)
  yield all([
    call(loadFormDefinition, formName, actions.setFormDefinition),
    call(loadEntityModel),
    searchListFormName ? call(loadFormDefinition, searchListFormName, actions.setSearchListFormDefinition) : undefined
  ])

  yield put(actions.setInitialized())
}

export function* queryChanged() {
  const query = yield call(getBasicQuery)
  yield put(selectionActions.setQuery(query))
}

export function* loadData(page) {
  yield put(actions.setInProgress(true))
  yield fork(countEntities)
  yield put(actions.clearEntityStore())
  yield call(requestEntities, page)
  yield put(actions.setInProgress(false))
}

export function* getBasicQuery(regardSelection = true) {
  const {showSelectedRecords, selection} = yield select(selectionSelector)
  if (regardSelection && showSelectedRecords) {
    return {
      keys: selection
    }
  }

  return yield call(getSearchViewQuery)
}

export function* getSearchViewQuery() {
  const {formFieldsFlat} = yield select(searchFormSelector)
  const {tql, keys} = yield select(docsListSelector)
  const searchFormValues = yield call(getSearchFormValues)
  const localizedSearchFormValues = yield call(expandSearchFormValuesForLocalizedFields, searchFormValues)

  const searchFormTql = yield call(getTqlFromSearchForm, localizedSearchFormValues, formFieldsFlat)
  const where = yield call(getTql, tql, searchFormTql)
  const hasUserChanges = searchFormTql?.length > 0

  return {
    ...(where ? {where} : {}),
    ...(keys ? {keys} : {}),
    hasUserChanges
  }
}

export function* prepareEndpointUrl(endpoint, searchEndpoint, hasUserChanges) {
  const {parent} = yield select(docsListSelector)
  const parentKey = parent ? parent.key : ''
  const selectedEndpoint = hasUserChanges && searchEndpoint ? searchEndpoint : endpoint
  return selectedEndpoint ? selectedEndpoint.replace('{parentKey}', parentKey) : null
}

export function* countEntities() {
  const state = yield select(stateSelector)
  const {showSelectedRecords, selection} = state.selection

  const regardSelection = !showSelectedRecords
  const query = yield call(getBasicQuery, regardSelection)

  const formDefinition = getFormDefinition(state, query)
  const endpoint = formsApi.getEndpoint(formDefinition)
  const searchEndpoint = formsApi.getSearchEndpoint(formDefinition)

  const preparedEndpoint = yield call(prepareEndpointUrl, endpoint, searchEndpoint, query.hasUserChanges)

  const requestOptions = {
    method: endpoint ? 'GET' : 'POST',
    ...(preparedEndpoint ? {endpoint: preparedEndpoint} : {})
  }
  const entityCount = yield call(rest.fetchEntityCount, entityName, query, requestOptions)

  if (showSelectedRecords) {
    yield put(actions.setEntityCount(selection.length))
  } else {
    yield put(actions.setEntityCount(entityCount))
  }

  yield put(selectionActions.setQueryCount(entityCount))
}

export function* changePage({payload}) {
  const {page} = payload
  yield put(actions.setInProgress(true))
  yield put(actions.setCurrentPage(page))
  yield call(requestEntities, page)
  yield put(actions.setInProgress(false))
}

export function* refreshData() {
  const {currentPage} = yield select(listSelector)
  yield call(loadData, currentPage)
}

export function* reloadData() {
  yield put(actions.setCurrentPage(1))
  yield call(loadData, 1)
}

export const getTql = (inputTql, searchTql) =>
  [
    ...(inputTql && inputTql.length > 0 ? [`(${inputTql})`] : []),
    ...(searchTql && searchTql.length > 0 ? [`(${searchTql})`] : [])
  ].join(' and ')

export function* fetchEntitiesAndAddToStore(page) {
  const state = yield select(stateSelector)
  const {columns: columnPreferences} = state.preferences
  const {entityStore, sorting} = state.list
  if (!entityStore[page]) {
    const basicQuery = yield call(getBasicQuery)
    const formDefinition = getFormDefinition(state, basicQuery)

    const {paths} = yield call(formsApi.getFields, formDefinition, columnPreferences)
    const query = {
      ...basicQuery,
      page,
      sorting,
      limit: getActualLimit(state),
      paths
    }

    const endpoint = formsApi.getEndpoint(formDefinition)
    const searchEndpoint = formsApi.getSearchEndpoint(formDefinition)

    const preparedEndpoint = yield call(prepareEndpointUrl, endpoint, searchEndpoint, basicQuery.hasUserChanges)
    const requestOptions = {
      method: endpoint ? 'GET' : 'POST',
      ...(preparedEndpoint ? {endpoint: preparedEndpoint} : {})
    }

    const entities = yield call(rest.fetchEntitiesPage, entityName, query, requestOptions, entitiesListTransformer)

    yield put(actions.addEntitiesToStore(page, entities))
  }
}

export function* requestEntities(page) {
  const {entityStore} = yield select(listSelector)

  if (!entityStore[page]) {
    yield call(fetchEntitiesAndAddToStore, page)
  }

  yield call(displayEntity, page)
  yield spawn(delayedPreloadNextPage, page)
}

export function* delayedPreloadNextPage(page) {
  yield delay(2000)
  yield call(preloadNextPage, page)
}

export function* preloadNextPage(currentPage) {
  const state = yield select(stateSelector)
  const actualLimit = getActualLimit(state)
  const list = yield select(listSelector)
  const {entityStore} = list
  let {entityCount} = list
  const nextPage = currentPage + 1

  if (entityCount === null || entityCount === undefined) {
    const setCountAction = yield take(actions.SET_ENTITY_COUNT)
    entityCount = setCountAction.payload.entityCount
  }

  if (currentPage * actualLimit < entityCount && !entityStore[nextPage]) {
    yield call(fetchEntitiesAndAddToStore, nextPage)
  }
}

export function* displayEntity(page) {
  const list = yield select(listSelector)
  const entities = list.entityStore[page]
  yield put(actions.setEntities(entities))
}

function* getPreferencesSorting() {
  const preferences = yield select(preferencesSelector)
  if (!preferences.sorting) {
    const {payload} = yield take(preferencesActions.SET_SORTING)
    return payload.sorting
  } else {
    return preferences.sorting
  }
}

export const FALLBACK_SORTING_FIELD = 'update_timestamp'
export function* setSorting() {
  const {formDefinition, entityModel} = yield select(listSelector)
  const tableSorting = yield call(formsApi.getSorting, formDefinition)
  const preferencesSorting = yield call(getPreferencesSorting)
  if (preferencesSorting && preferencesSorting.length > 0) {
    yield put(actions.setSorting(preferencesSorting))
  } else if (tableSorting && tableSorting.length > 0) {
    yield put(actions.setSorting(tableSorting))
  } else if (entityModel.paths[FALLBACK_SORTING_FIELD]) {
    yield put(actions.setSorting([{field: FALLBACK_SORTING_FIELD, order: 'desc'}]))
  } else {
    yield put(actions.setSorting([]))
  }
}

export function* loadFormDefinition(formName, actionCreator) {
  const {formDefinition} = yield select(listSelector)
  if (formDefinition?.id !== `${formName}_${scope}`) {
    const fetchedFormDefinition = yield call(rest.fetchForm, formName, scope)
    yield put(actionCreator(fetchedFormDefinition))
  }
}

export function* loadEntityModel() {
  const model = yield call(rest.fetchModel, entityName)
  yield put(actions.setEntityModel(model))
}

export function* onRowClick({payload}) {
  const {selectOnRowClick} = yield select(inputSelector)
  if (selectOnRowClick) {
    const {selection} = yield select(selectionSelector)
    const selected = selection.includes(payload.id)
    yield put(selectionActions.onSelectChange([payload.id], !selected))
  }
}

export const containsEntityOfModel = (event, model) =>
  !!event.payload.entities.find(entity => entity.entityName === model)

export function* remoteEvent(action) {
  const event = action.payload.event
  const {entityModel} = yield select(listSelector)
  if (['entity-create-event', 'entity-delete-event', 'entity-update-event'].includes(event.type)) {
    if (containsEntityOfModel(event, entityModel.name)) {
      yield call(reloadData)
    }
  }

  if (event.type === 'action-trigger-event') {
    yield put(event.payload.func(...event.payload.args))
  }

  if (event.type === 'entity-delete-event') {
    const deletedEntityKeys = event.payload.entities.filter(e => e.entityName === entityModel.name).map(e => e.key)
    yield put(selectionActions.onSelectChange(deletedEntityKeys, false))
  }

  yield put(actionEmitter.emitAction(action))
}
