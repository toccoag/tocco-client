import {call, put, select, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {externalEvents, remoteEvents, rest} from 'tocco-app-extensions'
import {entitiesListTransformer, formsApi, selectionActions} from 'tocco-entity-list/src/main'

import * as searchFormActions from '../searchForm/actions'
import {expandSearchFormValuesForLocalizedFields, getSearchFormValues} from '../searchForm/sagas'

import * as actions from './actions'
import * as sagas from './sagas'

const generateState = (entityStore, page) => ({
  initialized: false,
  sorting: [],
  entityStore,
  formDefinition: {
    id: 'User_list',
    children: [
      {
        componentType: 'table',
        endpoint: '/fetch'
      }
    ]
  },
  page
})

describe('docs-browser', () => {
  describe('modules', () => {
    describe('list', () => {
      describe('sagas', () => {
        describe('main saga', () => {
          test('should fork sagas', () => {
            const saga = testSaga(sagas.default)
            saga
              .next()
              .all([
                takeLatest(actions.CHANGE_LIST_PARENT, sagas.changeListParent),
                takeLatest(actions.INITIALIZE, sagas.initialize),
                takeLatest(actions.CHANGE_PAGE, sagas.changePage),
                takeLatest(searchFormActions.EXECUTE_SEARCH, sagas.reloadData),
                takeLatest(searchFormActions.EXECUTE_SEARCH, sagas.queryChanged),
                takeLatest(actions.SET_SORTING_INTERACTIVE, sagas.reloadData),
                takeLatest(actions.REFRESH, sagas.refreshData),
                takeLatest(selectionActions.RELOAD_DATA, sagas.reloadData),
                takeLatest(actions.ON_ROW_CLICK, sagas.onRowClick),
                takeEvery(remoteEvents.REMOTE_EVENT, sagas.remoteEvent),
                takeLatest(actions.DEFINE_SORTING, sagas.setSorting)
              ])
          })
        })

        describe('listParentChange', () => {
          test('should fire external onListParentChange event', () => {
            const parent = {model: 'User', key: '1'}
            return expectSaga(sagas.changeListParent, {payload: {parent}})
              .put(externalEvents.fireExternalEvent('onListParentChange', parent))
              .run()
          })
        })

        describe('initialize saga', () => {
          test('should load form and model', () => {
            const formName = 'Root_docs_list_item'
            return expectSaga(sagas.initialize)
              .provide([
                [select(sagas.docsListSelector), {formName}],
                [matchers.call.fn(sagas.loadFormDefinition)],
                [matchers.call.fn(sagas.loadEntityModel)]
              ])
              .call(sagas.loadFormDefinition, formName, actions.setFormDefinition)
              .call(sagas.loadEntityModel)
              .put(actions.setInitialized())
              .run()
          })

          test('should also load search list form if requested', () => {
            const formName = 'Root_docs_list_item'
            const searchListFormName = 'Search_docs_list_item'
            return expectSaga(sagas.initialize)
              .provide([
                [select(sagas.docsListSelector), {formName}],
                [matchers.call.fn(sagas.loadFormDefinition)],
                [matchers.call.fn(sagas.loadEntityModel)]
              ])
              .call(sagas.loadFormDefinition, formName, actions.setFormDefinition)
              .call(sagas.loadFormDefinition, searchListFormName, actions.setSearchListFormDefinition)
              .call(sagas.loadEntityModel)
              .put(actions.setInitialized())
              .run()
          })
        })

        describe('changePage saga', () => {
          test('should set currentPage and requestEntities', () => {
            const page = 1
            const gen = sagas.changePage({payload: {page}})
            expect(gen.next().value).to.eql(put(actions.setInProgress(true)))
            expect(gen.next().value).to.eql(put(actions.setCurrentPage(page)))
            expect(gen.next().value).to.eql(call(sagas.requestEntities, page))
            expect(gen.next().value).to.eql(put(actions.setInProgress(false)))
            expect(gen.next().done).to.be.true
          })
        })

        describe('reloadData saga', () => {
          test('should load data with first page', () => {
            return expectSaga(sagas.reloadData)
              .provide([[matchers.call.fn(sagas.loadData)]])
              .put(actions.setCurrentPage(1))
              .call(sagas.loadData, 1)
              .run()
          })
        })

        describe('refresh saga', () => {
          test('should  load data with first page', () => {
            return expectSaga(sagas.refreshData)
              .provide([[select(sagas.listSelector), {currentPage: 3}], [matchers.call.fn(sagas.loadData)]])
              .call(sagas.loadData, 3)
              .run()
          })
        })

        describe('fetchEntitiesAndAddToStore saga', () => {
          test('should not add entities to store if already in it', () => {
            const formName = 'Root_docs_list_item'
            const entityStore = {1: {}}

            const state = {
              entityList: {},
              docsList: {formName},
              preferences: {},
              list: {entityStore}
            }

            const gen = sagas.fetchEntitiesAndAddToStore(1)
            expect(gen.next().value).to.eql(select(sagas.stateSelector))
            expect(gen.next(state).done).to.be.true
          })

          test('should add entities to store', () => {
            const listViewState = generateState({}, 1)
            const entities = []
            const fields = ['firstname', 'lastname']

            const page = 1

            const state = {
              docsList: {formName: 'Root_docs_list_item'},
              list: listViewState,
              preferences: {columns: {}}
            }

            return expectSaga(sagas.fetchEntitiesAndAddToStore, page)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.docsListSelector), state.docsList],
                [select(sagas.listSelector), state.list],
                [matchers.call.fn(formsApi.getFields), fields],
                [matchers.call.fn(rest.fetchEntitiesPage), entities],
                [matchers.call.fn(sagas.getBasicQuery), {}],
                [matchers.spawn.fn(sagas.loadRelationDisplays), {}]
              ])
              .put(actions.addEntitiesToStore(page, entities))
              .run()
          })

          test('should use endpoint from search list form if set and if search mode', () => {
            const listViewState = generateState({}, 1)
            const entities = []
            const fields = ['firstname', 'lastname']

            const page = 1

            const state = {
              docsList: {formName: 'Root_docs_list_item', limit: ''},
              list: {
                ...listViewState,
                searchListFormDefinition: {
                  id: 'UserTestSearch_list',
                  children: [
                    {
                      componentType: 'table',
                      endpoint: '/search-fetch'
                    }
                  ]
                }
              },
              preferences: {columns: {}}
            }

            return expectSaga(sagas.fetchEntitiesAndAddToStore, page)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.docsListSelector), state.docsList],
                [select(sagas.listSelector), state.list],
                [matchers.call.fn(formsApi.getFields), fields],
                [matchers.call.fn(rest.fetchEntitiesPage), entities],
                [
                  matchers.call.fn(sagas.getBasicQuery),
                  {
                    hasUserChanges: true
                  }
                ],
                [matchers.spawn.fn(sagas.loadRelationDisplays), {}]
              ])
              .call(
                rest.fetchEntitiesPage,
                'Docs_list_item',
                {
                  hasUserChanges: true,
                  page: 1,
                  sorting: [],
                  limit: '',
                  paths: undefined
                },
                {method: 'GET', endpoint: '/search-fetch'},
                entitiesListTransformer
              )
              .put(actions.addEntitiesToStore(page, entities))
              .run()
          })
        })

        describe('requestEntities saga', () => {
          test('should request entities', () => {
            const entityStore = {}
            const page = 1
            return expectSaga(sagas.requestEntities, page)
              .provide([
                [select(sagas.listSelector), {entityStore}],
                [matchers.call.fn(sagas.fetchEntitiesAndAddToStore)],
                [matchers.spawn.fn(sagas.delayedPreloadNextPage)]
              ])
              .call.like({fn: sagas.fetchEntitiesAndAddToStore})
              .call(sagas.displayEntity, page)
              .spawn(sagas.delayedPreloadNextPage, page)
              .run()
          })

          test('should not fetch entities if already exists in store', () => {
            const entityStore = {1: []}
            const page = 1
            return expectSaga(sagas.requestEntities, page)
              .provide([[select(sagas.listSelector), {entityStore}], [matchers.spawn.fn(sagas.delayedPreloadNextPage)]])
              .not.call.like({fn: sagas.fetchEntitiesAndAddToStore})
              .call(sagas.displayEntity, page)
              .spawn(sagas.delayedPreloadNextPage, page)
              .run()
          })
        })

        describe('displayEntity saga', () => {
          test('should display entity', () => {
            const page = 1
            const entities = [{}]
            const entityStore = {[page]: entities}
            return expectSaga(sagas.displayEntity, page)
              .provide([[select(sagas.listSelector), {entityStore}]])
              .put(actions.setEntities(entities))
              .run()
          })
        })

        describe('setSorting saga', () => {
          const entityModel = {
            paths: {
              update_timestamp: {}
            }
          }

          const formDefinition = {}
          test('should extract sorting of form', () => {
            const sorting = [{field: 'firstname', order: 'asc'}]
            return expectSaga(sagas.setSorting)
              .provide([
                [matchers.call.fn(formsApi.getSorting), sorting],
                [select(sagas.inputSelector), {}],
                [select(sagas.listSelector), {entityModel, formDefinition}],
                [select(sagas.preferencesSelector), {sorting: []}]
              ])
              .put(actions.setSorting(sorting))
              .run()
          })

          test('should set fallback if not defined in form are not set', () => {
            return expectSaga(sagas.setSorting)
              .provide([
                [matchers.call.fn(formsApi.getSorting), []],
                [select(sagas.inputSelector), {}],
                [select(sagas.listSelector), {entityModel, formDefinition}],
                [select(sagas.preferencesSelector), {sorting: []}]
              ])
              .put(actions.setSorting([{field: sagas.FALLBACK_SORTING_FIELD, order: 'desc'}]))
              .run()
          })

          test('should set fallback', () => {
            return expectSaga(sagas.setSorting)
              .provide([
                [matchers.call.fn(formsApi.getSorting), []],
                [select(sagas.inputSelector), {}],
                [select(sagas.listSelector), {entityModel, formDefinition}],
                [select(sagas.preferencesSelector), {sorting: []}]
              ])
              .put(actions.setSorting([{field: sagas.FALLBACK_SORTING_FIELD, order: 'desc'}]))
              .run()
          })

          test('should set empty array as sorting if fallback sorting field is missing', () => {
            return expectSaga(sagas.setSorting)
              .provide([
                [matchers.call.fn(formsApi.getSorting), []],
                [select(sagas.inputSelector), {}],
                [select(sagas.listSelector), {entityModel: {paths: {}}, formDefinition}],
                [select(sagas.preferencesSelector), {sorting: []}]
              ])
              .put(actions.setSorting([]))
              .run()
          })

          test('should prefer sorting from preferences over table and input', () => {
            const inputSorting = [{field: 'input', order: 'asc'}]
            const formSorting = [{field: 'firstname', order: 'asc'}]
            const preferenceSorting = [{field: 'other', order: 'desc'}]
            return expectSaga(sagas.setSorting)
              .provide([
                [matchers.call.fn(formsApi.getSorting), formSorting],
                [select(sagas.inputSelector), inputSorting],
                [select(sagas.listSelector), {entityModel, formDefinition}],
                [select(sagas.preferencesSelector), {sorting: preferenceSorting}]
              ])
              .put(actions.setSorting(preferenceSorting))
              .run()
          })
        })

        describe('loadData saga', () => {
          test('should load data of provided page', () =>
            expectSaga(sagas.loadData, 2)
              .provide([[matchers.fork.fn(sagas.countEntities)], [matchers.call.fn(sagas.requestEntities)]])
              .put(actions.setInProgress(true))
              .put(actions.clearEntityStore())
              .call(sagas.requestEntities, 2)
              .put(actions.setInProgress(false))
              .fork(sagas.countEntities)
              .run())
        })

        describe('countEntities saga', () => {
          test('should call entity count and set result', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  endpoint: '/fetch'
                }
              ]
            }
            const showSelectedRecords = false

            const entityCount = 100

            const state = {
              selection: {showSelectedRecords},
              docsList: {},
              list: {formDefinition},
              entityList: {}
            }

            return expectSaga(sagas.countEntities)
              .provide([
                [select(sagas.docsListSelector), state.docsList],
                [select(sagas.stateSelector), state],
                [matchers.call.fn(sagas.getBasicQuery), {}],
                [matchers.call.fn(rest.fetchEntityCount), entityCount]
              ])
              .call(rest.fetchEntityCount, 'Docs_list_item', {}, {method: 'GET', endpoint: '/fetch'})
              .put(actions.setEntityCount(entityCount))
              .put(selectionActions.setQueryCount(entityCount))
              .run()
          })

          test('should set entityCount as queryCount if seletion is active', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  endpoint: '/fetch'
                }
              ]
            }
            const showSelectedRecords = true
            const selection = ['1', '3', '99']

            const entityCount = 100

            const state = {
              selection: {showSelectedRecords, selection},
              docsList: {},
              list: {formDefinition},
              entityList: {}
            }

            return expectSaga(sagas.countEntities)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.docsListSelector), state.docsList],
                [matchers.call.fn(sagas.getBasicQuery), {}],
                [matchers.call.fn(rest.fetchEntityCount), entityCount]
              ])
              .call(rest.fetchEntityCount, 'Docs_list_item', {}, {method: 'GET', endpoint: '/fetch'})
              .put(actions.setEntityCount(3))
              .put(selectionActions.setQueryCount(entityCount))
              .run()
          })

          test('should use endpoint from search list form if set and if search mode', () => {
            const formDefinition = {
              children: [
                {
                  componentType: 'table',
                  endpoint: '/fetch'
                }
              ]
            }
            const searchListFormDefinition = {
              children: [
                {
                  componentType: 'table',
                  endpoint: '/search-fetch'
                }
              ]
            }

            const showSelectedRecords = true
            const selection = ['1', '3', '99']

            const entityCount = 100

            const state = {
              selection: {showSelectedRecords, selection},
              docsList: {},
              list: {formDefinition, searchListFormDefinition},
              entityList: {}
            }

            return expectSaga(sagas.countEntities)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.docsListSelector), state.docsList],
                [matchers.call.fn(sagas.getBasicQuery), {hasUserChanges: true}],
                [matchers.call.fn(rest.fetchEntityCount), entityCount]
              ])
              .call(
                rest.fetchEntityCount,
                'Docs_list_item',
                {hasUserChanges: true},
                {method: 'GET', endpoint: '/search-fetch'}
              )
              .run()
          })
        })

        describe('loadFormDefinition saga', () => {
          test('should load form definition', () => {
            const fetchedFormDefinition = {id: 'Docs_list_item_list', children: []}
            const formName = 'Docs_list_item'
            const actionCreator = actions.setFormDefinition
            return expectSaga(sagas.loadFormDefinition, formName, actionCreator)
              .provide([
                [select(sagas.listSelector), {}],
                [call(rest.fetchForm, formName, 'list'), fetchedFormDefinition],
                [select(sagas.docsListSelector), {}]
              ])
              .put(actionCreator(fetchedFormDefinition))
              .run()
          })

          test('should not load form definition if already loaded', () => {
            const selectFormDefinition = {id: 'Docs_list_item_list', children: []}
            const actionCreator = actions.setFormDefinition
            return expectSaga(sagas.loadFormDefinition, 'Docs_list_item', actionCreator)
              .provide([[select(sagas.listSelector), {formDefinition: selectFormDefinition}]])
              .not.put(actions.setFormDefinition(selectFormDefinition))
              .run()
          })

          test('should load form definition if other formName is passed', () => {
            const selectFormDefinition = {id: 'Root_docs_list_item_list', children: []}
            const fetchedFormDefinition = {id: 'Docs_list_item_list', children: []}
            const formName = 'Docs_list_item'
            const actionCreator = actions.setFormDefinition
            return expectSaga(sagas.loadFormDefinition, formName, actionCreator)
              .provide([
                [select(sagas.listSelector), {formDefinition: selectFormDefinition}],
                [select(sagas.docsListSelector), {}],
                [call(rest.fetchForm, formName, 'list'), fetchedFormDefinition]
              ])
              .put(actionCreator(fetchedFormDefinition))
              .run()
          })
        })

        describe('loadEntityModel saga', () => {
          test('should load the entity model', () => {
            const model = {name: 'Docs_list_item'}
            return expectSaga(sagas.loadEntityModel)
              .provide([[matchers.call.fn(rest.fetchModel), model]])
              .put(actions.setEntityModel(model))
              .run()
          })
        })

        describe('onRowClick saga', () => {
          const payload = {id: 'Domain/1'}
          const selection = ['Domain/1', 'Domain/2', 'Domain/3']

          test('should handle row click if selectOnRowClick is true', () => {
            return expectSaga(sagas.onRowClick, {payload})
              .provide([
                [select(sagas.inputSelector), {selectOnRowClick: true}],
                [select(sagas.selectionSelector), {selection}]
              ])
              .put(selectionActions.onSelectChange(['Domain/1'], false))
              .run()
          })

          test('should do nothing if selectOnRowClick is false', () => {
            return expectSaga(sagas.onRowClick, {payload})
              .provide([
                [select(sagas.inputSelector), {selectOnRowClick: false}],
                [select(sagas.selectionSelector), {selection}]
              ])
              .not.put(selectionActions.onSelectChange(['Domain/1'], false))
              .run()
          })
        })

        describe('prepareEndpointUrl', () => {
          test('should replace parentKey if parent exists', () => {
            const docsListState = {parent: {key: 123}}
            const endpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test'
            const expectedResult = 'nice2/rest/entities/2.0/User/123/test'

            return expectSaga(sagas.prepareEndpointUrl, endpoint)
              .provide([[select(sagas.docsListSelector), docsListState]])
              .returns(expectedResult)
              .run()
          })

          test('should remove parentKey placeholder if parent is undefined', () => {
            const docsListState = {parent: null}
            const endpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test'
            const expectedResult = 'nice2/rest/entities/2.0/User//test' // REST API doesn't care about duplicate slash

            return expectSaga(sagas.prepareEndpointUrl, endpoint)
              .provide([[select(sagas.docsListSelector), docsListState]])
              .returns(expectedResult)
              .run()
          })

          test('should use search endpoint if defined and has user search input', () => {
            const docsListState = {parent: {key: 123}}
            const endpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test'
            const searchEndpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test/searchresults'
            const expectedResult = 'nice2/rest/entities/2.0/User/123/test/searchresults'

            return expectSaga(sagas.prepareEndpointUrl, endpoint, searchEndpoint, true)
              .provide([[select(sagas.docsListSelector), docsListState]])
              .returns(expectedResult)
              .run()
          })

          test('should use regular endpoint if search endpoint not defined and has user search input', () => {
            const docsListState = {parent: {key: 123}}
            const endpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test'
            const searchEndpoint = undefined
            const expectedResult = 'nice2/rest/entities/2.0/User/123/test'

            return expectSaga(sagas.prepareEndpointUrl, endpoint, searchEndpoint, true)
              .provide([[select(sagas.docsListSelector), docsListState]])
              .returns(expectedResult)
              .run()
          })

          test('should use regular endpoint if search endpoint defined and does not have user search input', () => {
            const docsListState = {parent: {key: 123}}
            const endpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test'
            const searchEndpoint = 'nice2/rest/entities/2.0/User/{parentKey}/test/searchresults'
            const expectedResult = 'nice2/rest/entities/2.0/User/123/test'

            return expectSaga(sagas.prepareEndpointUrl, endpoint, searchEndpoint, false)
              .provide([[select(sagas.docsListSelector), docsListState]])
              .returns(expectedResult)
              .run()
          })
        })

        describe('getBasicQuery', () => {
          test('should return an object with correct attributes', () => {
            const docsListState = {
              tql: 'foo == "bar"',
              keys: ['235', '18', '120']
            }
            const searchForm = {
              formFieldsFlat: {
                relGender: 'single-remote-field'
              }
            }
            const selection = {
              showSelectedRecords: false
            }
            const searchFormValues = {
              relParent: {key: '1'},
              relGender: {key: '3'},
              txtFulltext: 'full'
            }

            const expectedResult = {
              where: '(foo == "bar") and (relParent.pk == 1 and relGender.pk == 3 and txtFulltext ~= "*full*")',
              keys: ['235', '18', '120'],
              hasUserChanges: true
            }

            return expectSaga(sagas.getBasicQuery)
              .provide([
                [select(sagas.searchFormSelector), searchForm],
                [select(sagas.selectionSelector), selection],
                [select(sagas.docsListSelector), docsListState],
                [select(sagas.listSelector), {}],
                [matchers.call.fn(getSearchFormValues), searchFormValues],
                [matchers.call.fn(expandSearchFormValuesForLocalizedFields), searchFormValues]
              ])
              .returns(expectedResult)
              .run()
          })

          test('should return an object with hasUserChanges `false` if has only input attributes', () => {
            const docsListState = {tql: 'foo == "bar"'}
            const searchForm = {
              formFieldsFlat: {
                relGender: 'single-remote-field'
              }
            }
            const selection = {
              showSelectedRecords: false
            }
            const searchFormValues = {}

            const expectedResult = {
              where: '(foo == "bar")',
              hasUserChanges: false
            }

            return expectSaga(sagas.getBasicQuery)
              .provide([
                [select(sagas.searchFormSelector), searchForm],
                [select(sagas.selectionSelector), selection],
                [select(sagas.docsListSelector), docsListState],
                [select(sagas.listSelector), {}],
                [matchers.call.fn(getSearchFormValues), searchFormValues],
                [matchers.call.fn(expandSearchFormValuesForLocalizedFields), searchFormValues]
              ])
              .returns(expectedResult)
              .run()
          })

          test('should return tql if show selected is true', async () => {
            const selection = {
              showSelectedRecords: true,
              selection: ['1', '22', '99']
            }

            const saga = await expectSaga(sagas.getBasicQuery, true)
              .provide([
                [select(sagas.selectionSelector), selection],
                [select(sagas.listSelector), {}]
              ])
              .run()

            expect(saga.returnValue).to.have.property('keys')
          })
        })

        describe('preloadNextPage saga', () => {
          test('should load next page if not already done and not end', () => {
            const docsListState = {limit: 10}
            const listState = {
              entityStore: {},
              entityCount: 25
            }
            const preferencesState = {numOfRows: 20}

            const state = {
              docsList: docsListState,
              list: listState,
              preferences: preferencesState
            }

            return expectSaga(sagas.preloadNextPage, 1)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.listSelector), listState],
                [matchers.call.fn(sagas.fetchEntitiesAndAddToStore), null]
              ])
              .call(sagas.fetchEntitiesAndAddToStore, 2)
              .run()
          })

          test('should not load next page if at end', () => {
            const docsListState = {limit: 10}
            const listState = {
              entityStore: {},
              entityCount: 20
            }

            const state = {
              docsList: docsListState,
              preferences: {}
            }

            return expectSaga(sagas.preloadNextPage, 2)
              .provide([
                [select(sagas.stateSelector), state],
                [select(sagas.listSelector), listState],
                [matchers.call.fn(sagas.fetchEntitiesAndAddToStore), null]
              ])
              .not.call(sagas.fetchEntitiesAndAddToStore, 3)
              .run()
          })
        })

        describe('remoteEvent saga', () => {
          const payload = {
            entities: [
              {entityName: 'User', key: '1'},
              {entityName: 'Principal', key: '2'}
            ]
          }
          const createEventAction = remoteEvents.remoteEvent({
            type: 'entity-create-event',
            payload
          })
          const deleteEventAction = remoteEvents.remoteEvent({
            type: 'entity-delete-event',
            payload
          })
          const updateEventAction = remoteEvents.remoteEvent({
            type: 'entity-update-event',
            payload
          })

          const userListState = {
            entityModel: {name: 'User'}
          }
          const classroomListState = {
            entityModel: {name: 'Classroom'}
          }

          const expectReload = (listState, remoteEvent) =>
            expectSaga(sagas.remoteEvent, remoteEvent)
              .provide([[select(sagas.listSelector), listState], [call(sagas.reloadData)]])
              .call(sagas.reloadData)
              .run()

          const expectNoReload = (listState, remoteEvent) =>
            expectSaga(sagas.remoteEvent, remoteEvent)
              .provide([[select(sagas.listSelector), listState]])
              .not.call(sagas.reloadData)
              .run()

          test('should reload list if relevant create event', () => {
            return expectReload(userListState, createEventAction)
          })

          test('should not reload list if irrelevant create event', () => {
            return expectNoReload(classroomListState, createEventAction)
          })

          test('should reload list if relevant delete event', () => {
            return expectReload(userListState, deleteEventAction)
          })

          test('should not reload list if irrelevant delete event', () => {
            return expectNoReload(classroomListState, deleteEventAction)
          })

          test('should reload list if relevant update event', () => {
            return expectReload(userListState, updateEventAction)
          })

          test('should not reload list if irrelevant update event', () => {
            return expectNoReload(classroomListState, updateEventAction)
          })

          test('should clean up selection on delete event', () => {
            const deleteEventActionWithMultipleUsers = remoteEvents.remoteEvent({
              type: 'entity-delete-event',
              payload: {
                entities: [
                  {entityName: 'User', key: '1'},
                  {entityName: 'Login', key: '2'},
                  {entityName: 'User', key: '99'}
                ]
              }
            })

            const listState = {
              entityModel: {name: 'User'}
            }

            return expectSaga(sagas.remoteEvent, deleteEventActionWithMultipleUsers)
              .provide([[select(sagas.listSelector), listState], [call(sagas.reloadData)]])
              .put(selectionActions.onSelectChange(['1', '99'], false))
              .run()
          })
        })

        describe('queryChanged saga', () => {
          test('should set query', () => {
            const query = {tql: 'firstname == "Max"'}

            return expectSaga(sagas.queryChanged)
              .provide([[call(sagas.getBasicQuery), query]])
              .put(selectionActions.setQuery(query))
              .run()
          })
        })
      })
    })
  })
})
