import {all, takeEvery, takeLatest, call, put, select} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'
import {consoleLogger} from 'tocco-util'
import {v4 as uuid} from 'uuid'

import getNode from '../../utils/getNode'
import * as listActions from '../list/actions'

import * as actions from './actions'

export const dialogSelector = state => state.docs.create.dialog
export const textResourceSelector = (state, key) => state.intl.messages[key]

const CREATED_STATUS = 201

export default function* mainSagas() {
  yield all([takeEvery(actions.FILES_SELECTED, handleFilesSelected), takeLatest(actions.UPLOAD, upload)])
}

export function* handleFilesSelected({payload: {location, files, isDirectory}}) {
  const blockingInfoId = yield call(uuid)
  let message = 'client.docs-browser.uploadInProgress'
  if (isDirectory) {
    message = 'client.docs-browser.uploadInProgressDirectory'
  } else if (files.length > 1) {
    message = 'client.docs-browser.uploadInProgressMultiple'
  }
  yield put(notification.blockingInfo(blockingInfoId, message, null))

  const {onSuccess, onError} = yield select(dialogSelector)

  try {
    const response = yield call(createDocuments, location, files)

    if (response.status === 403) {
      yield put(notification.removeBlockingInfo(blockingInfoId))
      onError({
        title: yield select(textResourceSelector, 'client.entity-detail.saveAbortedTitle'),
        message: yield select(textResourceSelector, 'client.docs-browser.failedNoPermission')
      })
    } else if (response.status === 413) {
      yield put(notification.removeBlockingInfo(blockingInfoId))
      onError({
        title: yield select(textResourceSelector, 'client.entity-detail.saveAbortedTitle'),
        message: yield select(textResourceSelector, 'client.docs-browser.failedTooLarge')
      })
    } else {
      const remoteEvents = [
        {
          type: 'entity-create-event',
          payload: {
            entities: response.body.items
              .filter(item => item.status === CREATED_STATUS)
              .map(item => ({
                entityName: item.bean.model,
                key: item.bean.key
              }))
          }
        }
      ]

      yield put(notification.removeBlockingInfo(blockingInfoId))

      const msgId = isDirectory
        ? 'client.docs-browser.uploadSuccessfulDirectory'
        : 'client.docs-browser.uploadSuccessful'
      onSuccess({
        title: yield select(textResourceSelector, msgId),
        remoteEvents
      })
    }
  } catch (e) {
    consoleLogger.logError('Failed to upload files', e)
    yield put(notification.removeBlockingInfo(blockingInfoId))
    onError({
      title: yield select(textResourceSelector, 'client.docs-browser.uploadFailed')
    })
  }
}

export function* createDocuments(location, files) {
  const node = getNode(location)

  if (!node) {
    return
  }

  const formData = new FormData()
  for (const file of files) {
    formData.append('files[]', file)
  }

  const resource = `documents/${node.model}/${node.key}`
  const options = {
    method: 'POST',
    body: formData,
    acceptedStatusCodes: [403, 413]
  }

  return yield call(rest.requestSaga, resource, options)
}

export function* upload({payload: {files, location}}) {
  yield call(
    createDocuments,
    location,
    files.map(file => renameFile(file, file.path))
  )
  yield put(listActions.refresh())
}

// dropzone somehow gives us a file that does not follow usual shape?
const renameFile = (originalFile, originalName) => {
  const newName = originalName ? originalName.replace(/^\.\//, '') : originalName
  return new File([originalFile], newName, {
    type: originalFile.type,
    lastModified: originalFile.lastModified
  })
}
