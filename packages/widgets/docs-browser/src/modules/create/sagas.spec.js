import {select, call, takeEvery, takeLatest} from 'redux-saga/effects'
import {expectSaga, testSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, notification} from 'tocco-app-extensions'
import {v4 as uuid} from 'uuid'

import * as actions from './actions'
import * as sagas from './sagas'

describe('docs-browser', () => {
  describe('modules', () => {
    describe('create', () => {
      describe('sagas', () => {
        describe('main saga', () => {
          test('should fork sagas', () => {
            const saga = testSaga(sagas.default)
            saga
              .next()
              .all([
                takeEvery(actions.FILES_SELECTED, sagas.handleFilesSelected),
                takeLatest(actions.UPLOAD, sagas.upload)
              ])
          })
        })

        describe('handleFilesSelected', () => {
          test('should handle selected files', () => {
            const files = [{}, {}]
            const location = '/docs/folders/23523/list'
            const response = {
              body: {
                items: [
                  {
                    status: 201,
                    bean: {
                      model: 'Resource',
                      key: '2352'
                    }
                  },
                  {
                    status: 201,
                    bean: {
                      model: 'Resource',
                      key: '2353'
                    }
                  }
                ]
              }
            }

            const onSuccess = sinon.spy()
            const onError = sinon.spy()

            const expectedRemoteEvents = [
              {
                type: 'entity-create-event',
                payload: {
                  entities: [
                    {entityName: 'Resource', key: '2352'},
                    {entityName: 'Resource', key: '2353'}
                  ]
                }
              }
            ]

            return expectSaga(sagas.handleFilesSelected, actions.filesSelected(location, files))
              .provide([
                [call(uuid), 'my-random-uuid'],
                [
                  select(sagas.dialogSelector),
                  {
                    onSuccess,
                    onError
                  }
                ],
                [call(sagas.createDocuments, location, files), response],
                [select(sagas.textResourceSelector, 'client.docs-browser.uploadSuccessful'), 'upload successful']
              ])
              .put(notification.blockingInfo('my-random-uuid', 'client.docs-browser.uploadInProgressMultiple', null))
              .put(notification.removeBlockingInfo('my-random-uuid'))
              .run()
              .then(() => {
                expect(onSuccess).to.have.been.calledWith({
                  title: 'upload successful',
                  remoteEvents: expectedRemoteEvents
                })
                expect(onError).to.not.have.been.called
              })
          })

          test('no upload permission - upload failed', () => {
            const files = [{}]
            const response = {
              status: 403
            }

            const onSuccess = sinon.spy()
            const onError = sinon.spy()

            return expectSaga(sagas.handleFilesSelected, actions.filesSelected(location, files))
              .provide([
                [call(uuid), 'my-random-uuid'],
                [
                  select(sagas.dialogSelector),
                  {
                    onSuccess,
                    onError
                  }
                ],
                [call(sagas.createDocuments, location, files), response],
                [select(sagas.textResourceSelector, 'client.entity-detail.saveAbortedTitle'), 'save failed'],
                [select(sagas.textResourceSelector, 'client.docs-browser.failedNoPermission'), 'no permission']
              ])
              .put(notification.removeBlockingInfo('my-random-uuid'))
              .run()
              .then(() => {
                expect(onSuccess).to.not.have.been.called
                expect(onError).to.have.been.calledWith({title: 'save failed', message: 'no permission'})
              })
          })

          test('document too large - upload failed', () => {
            const files = [{}]
            const response = {
              status: 413
            }

            const onSuccess = sinon.spy()
            const onError = sinon.spy()

            return expectSaga(sagas.handleFilesSelected, actions.filesSelected(location, files))
              .provide([
                [call(uuid), 'my-random-uuid'],
                [
                  select(sagas.dialogSelector),
                  {
                    onSuccess,
                    onError
                  }
                ],
                [call(sagas.createDocuments, location, files), response],
                [select(sagas.textResourceSelector, 'client.entity-detail.saveAbortedTitle'), 'save failed'],
                [select(sagas.textResourceSelector, 'client.docs-browser.failedTooLarge'), 'too large']
              ])
              .put(notification.removeBlockingInfo('my-random-uuid'))
              .run()
              .then(() => {
                expect(onSuccess).to.not.have.been.called
                expect(onError).to.have.been.calledWith({title: 'save failed', message: 'too large'})
              })
          })
        })

        describe('createDocuments', () => {
          test('should upload files', () => {
            const files = [{}, {}]

            const location = '/docs/folder/23523/list'

            const response = {
              body: {
                items: [
                  {
                    status: 201,
                    bean: {
                      model: 'Resource',
                      key: '2352'
                    }
                  },
                  {
                    status: 201,
                    bean: {
                      model: 'Resource',
                      key: '2353'
                    }
                  }
                ]
              }
            }

            return expectSaga(sagas.createDocuments, location, files)
              .provide([[matchers.call.fn(rest.requestSaga), response]])
              .returns(response)
              .run()
          })
        })

        describe('upload', () => {
          test('should pass files to createDocuments', () => {
            const lastModified = Date.now()
            const location = 'location'
            const originalFile = new File(['test'], 'fileName', {
              type: 'pdf',
              lastModified
            })
            originalFile.path = 'modified.pdf'

            return expectSaga(sagas.upload, {payload: {location, files: [originalFile]}})
              .provide([[matchers.call.fn(sagas.createDocuments)]])
              .run()
              .then(({effects}) => {
                // redux-saga-test-plan can not handle File arguments, so we check them by hand ...
                expect(effects.call).to.have.length(1)
                const actualArguments = effects.call[0].payload.args
                expect(actualArguments[0]).to.eql(location)
                const acutalFiles = actualArguments[1]
                const actualFile = acutalFiles[0]
                expect(actualFile.name).to.eql('modified.pdf')
                expect(actualFile.type).to.eql(originalFile.type)
                expect(actualFile.lastModified).to.eql(originalFile.lastModified)
              })
          })

          test('should rename relative file names properly', () => {
            const lastModified = Date.now()
            const location = 'location'
            const originalFile = new File(['test'], 'fileName', {
              type: 'pdf',
              lastModified
            })
            originalFile.path = './file.png'

            return expectSaga(sagas.upload, {payload: {location, files: [originalFile]}})
              .provide([[matchers.call.fn(sagas.createDocuments)]])
              .run()
              .then(({effects}) => {
                // redux-saga-test-plan can not handle File arguments, so we check them by hand ...
                expect(effects.call).to.have.length(1)
                const actualArguments = effects.call[0].payload.args
                expect(actualArguments[0]).to.eql(location)
                const acutalFiles = actualArguments[1]
                const actualFile = acutalFiles[0]
                expect(actualFile.name).to.eql('file.png')
                expect(actualFile.type).to.eql(originalFile.type)
                expect(actualFile.lastModified).to.eql(originalFile.lastModified)
              })
          })
        })
      })
    })
  })
})
