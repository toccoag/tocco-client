import {entityName} from '../utils/constants'

export const getActualLimit = state => state.preferences.numOfRows || state.docsList.limit

export const getSelectionForAction = state => {
  const {selection, list} = state

  const sortingString = list.sorting.map(({field, order}) => `${field} ${order}`).join(', ')

  // dms action won't work with query selection
  return {
    entityName,
    type: 'ID',
    ids: selection.selection,
    query: {sort: sortingString},
    count: selection.selection.length || 0
  }
}

export const getFormDefinition = (state, query) =>
  (query || state.selection.query).hasUserChanges && state.list.searchListFormDefinition
    ? state.list.searchListFormDefinition
    : state.list.formDefinition
