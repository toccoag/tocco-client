import {put} from 'redux-saga/effects'

import {openDialog} from './modules/create/actions'

export default {
  'upload-document': function* (_definition, _selection, _parent, _params, _config, onSuccess, onError) {
    const directory = false
    yield put(openDialog(directory, onSuccess, onError))
  },
  'upload-directory': function* (_definition, _selection, _parent, _params, _config, onSuccess, onError) {
    const directory = true
    yield put(openDialog(directory, onSuccess, onError))
  }
}
