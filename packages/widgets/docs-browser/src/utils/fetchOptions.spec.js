import {getTqlFromSearchForm} from './fetchOptions'

describe('docs-browser', () => {
  describe('utils', () => {
    describe('fetchOptions', () => {
      describe('getTqlFromSearchForm', () => {
        test('should set basic attributes in tql and connect them with and operator', () => {
          const searchFormValues = {
            firstname: 'Robihoe',
            lastname: 'Griffin',
            cool: true
          }
          const formFields = {
            firstname: 'string',
            lastname: 'string',
            cool: 'boolean'
          }

          const expectedResult = 'firstname ~= "*Robihoe*" and lastname ~= "*Griffin*" and cool == true'
          const result = getTqlFromSearchForm(searchFormValues, formFields)
          expect(result).to.eql(expectedResult)
        })

        test('should ignore condition without', () => {
          const searchFormValues = {
            firstname: 'Robihoe',
            lastname: null
          }

          const expectedResult = 'firstname ~= "*Robihoe*"'

          const result = getTqlFromSearchForm(searchFormValues)
          expect(result).to.eql(expectedResult)
        })
      })
    })
  })
})
