const ROOT_PATH_REGEX = /^\/docs\/?$/
const SEARCH_PATH_REGEX = /^\/docs\/search\/?$/

const isRootLocation = pathname => ROOT_PATH_REGEX.test(pathname)
const isSearchLocation = pathname => SEARCH_PATH_REGEX.test(pathname)

export default isRootLocation
export {isSearchLocation}
