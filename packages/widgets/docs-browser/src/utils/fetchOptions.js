import _reduce from 'lodash/reduce'
import {tqlBuilder} from 'tocco-util'

export const getTqlFromSearchForm = (searchFormValues, formFields = {}) =>
  _reduce(
    searchFormValues,
    (acc, value, path) => {
      const fieldType = formFields[path]
      const tql = tqlBuilder.getTql(path, value, fieldType)

      return [...(acc ? [acc] : []), tql].filter(Boolean).join(' and ')
    },
    ''
  )
