import {useCallback} from 'react'
import {connect} from 'react-redux'
import {useLocation, useParams, useNavigate} from 'react-router-dom'

import {navigate} from '../modules/routing/actions'

const mapActionCreators = {
  navigate,
  navigateReplace: navigate
}

const mapStateToProps = state => ({
  path: state.docs.routing.path,
  params: state.docs.routing.params
})

export const withInternalRouter = connect(mapStateToProps, mapActionCreators)

export const withReactRouter = WrappedComponent => props => {
  const location = useLocation()
  const navigateTo = useNavigate()
  const params = useParams()

  const navigateToReplace = useCallback((to, options) => navigateTo(to, {replace: true, ...options}), [navigateTo])

  return (
    <WrappedComponent
      {...props}
      path={location.pathname}
      navigate={navigateTo}
      navigateReplace={navigateToReplace}
      params={params}
    />
  )
}
