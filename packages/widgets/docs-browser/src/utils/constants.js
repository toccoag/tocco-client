export const entityName = 'Docs_list_item'
export const searchListFormName = 'Search_docs_list_item'
export const scope = 'list'

/**
 * For some dms entity models the default navigation strategy (e.g. from the admin) is not working.
 * - The resource list is not working. An error is shown.
 * - The folder list is extremely slow.
 * - The domain list is normally working so it is not excluded.
 */
export const ignoreDefaultNavigationStrategy = model => model === 'Resource' || model === 'Folder'
