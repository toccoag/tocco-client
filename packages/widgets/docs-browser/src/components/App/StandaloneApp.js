import PropTypes from 'prop-types'
import {Navigate, Routes, Route, createHashRouter, RouterProvider} from 'react-router-dom'
import {notification} from 'tocco-app-extensions'

import DocsBrowser from '../DocsBrowser'

const StandaloneApp = ({rootPath, handleNotifications}) => {
  const router = createHashRouter([
    {
      path: '*',
      element: (
        <>
          {handleNotifications && <notification.Notifications />}

          <Routes>
            <Route exact path="/" element={<Navigate to={rootPath} replace />} />
            {rootPath !== '/docs' && <Route exact path="docs" element={<Navigate to={rootPath} replace />} />}
            <Route exact path="docs/*" element={<DocsBrowser />} />
          </Routes>
        </>
      )
    }
  ])

  return <RouterProvider router={router} />
}

StandaloneApp.propTypes = {
  handleNotifications: PropTypes.bool,
  rootPath: PropTypes.string
}

export default StandaloneApp
