import PropTypes from 'prop-types'
import {Routes, Route, Navigate} from 'react-router-dom'
import {notification} from 'tocco-app-extensions'

import DocsBrowser from '../DocsBrowser'

const InheritedApp = ({rootPath, handleNotifications}) => (
  <>
    {handleNotifications && <notification.Notifications />}

    <Routes>
      {rootPath !== '/docs' && <Route exact path="/docs" element={<Navigate to={rootPath} replace />} />}
      <Route exact path="/*" element={<DocsBrowser />} />
    </Routes>
  </>
)

InheritedApp.propTypes = {
  handleNotifications: PropTypes.bool,
  rootPath: PropTypes.string
}

export default InheritedApp
