import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {selectionActions} from 'tocco-entity-list/src/main'

import {changeListParent, onRowClick} from '../../modules/list/actions'
import {withRouterTypeCompProvider} from '../../utils/withRouterTypeCompProvider'

import DocsView from './DocsView'

const mapStateToProps = state => ({
  domainTypes: state.input.domainTypes,
  rootNodes: state.input.rootNodes,
  limit: state.input.listLimit,
  searchFormType: state.input.searchFormType,
  getCustomLocation: state.input.getCustomLocation,
  getListFormName: state.input.getListFormName,
  showActions: state.input.showActions,
  sortable: state.input.sortable,
  openResource: state.input.openResource,
  searchFormCollapsed: state.input.searchFormCollapsed,
  scrollBehaviour: state.input.scrollBehaviour,
  selection: state.input.selection,
  clearSelectionOnParentChange: state.input.clearSelectionOnParentChange
})

const mapActionCreators = {
  changeListParent,
  clearSelection: selectionActions.clearSelection,
  setSelection: selectionActions.setSelection,
  onRowClick
}

export default withRouterTypeCompProvider(connect(mapStateToProps, mapActionCreators)(injectIntl(DocsView)))
