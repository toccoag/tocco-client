import PropTypes from 'prop-types'
import {useEffect, useMemo, useRef} from 'react'
import {Icon, scrollBehaviourPropType} from 'tocco-ui'

import searchFormTypes, {searchFormTypePropTypes} from '../../utils/searchFormTypes'
import DocsList from '../DocsList'
import FileInput from '../FileInput'

import {StyledContentWrapper, StyledIconWrapper, StyledLabelWrapper} from './StyledComponents'

export const getParent = params => {
  if (params && params.model) {
    const model = params.model.charAt(0).toUpperCase() + params.model.slice(1)
    const key = params.key
    const fullKey = `${model}/${key}`
    return {
      model: 'Docs_list_item',
      key: fullKey // e.g. "Domain/118"
    }
  }
  return null
}

export const getTql = domainTypes => {
  if (Array.isArray(domainTypes) && domainTypes.length > 0) {
    const commaSepartedDomainTypes = domainTypes.map(type => `"${type}"`).join(',')
    return `exists(relDomain_type where IN(unique_id, ${commaSepartedDomainTypes}))`
  }
  return null
}

export const getFormName = (parent, keys) => {
  if (parent) {
    return 'Docs_list_item'
  }

  return keys ? 'Root_docs_list_item_specific' : 'Root_docs_list_item'
}

export const getDefaultLocation = (model, key) => {
  switch (model) {
    case 'Domain':
      return `/docs/domain/${key}/list`
    case 'Folder':
      return `/docs/folder/${key}/list`
    case 'Resource':
      return `/docs/doc/${key}/detail`
    default:
      throw new Error(`Unexpected model: ${model}`)
  }
}

const DocsView = props => {
  const {
    domainTypes,
    rootNodes,
    changeListParent,
    limit,
    searchFormType,
    getCustomLocation,
    getListFormName,
    showActions,
    scrollBehaviour,
    sortable,
    searchMode,
    openResource,
    searchFormCollapsed,
    params,
    navigate,
    clearSelection,
    setSelection,
    selection,
    clearSelectionOnParentChange,
    selectOnRowClick,
    onRowClick,
    path
  } = props

  const parent = useMemo(() => getParent(params), [params])

  const keys = !parent && rootNodes ? rootNodes.map(node => `${node.entityName}/${node.key}`) : null
  const formName = useMemo(
    () => (getListFormName ? getListFormName(parent, keys) : getFormName(parent, keys)),
    [parent, keys, getListFormName]
  )

  const mounted = useRef(false)
  const searchModeRef = useRef(searchMode)
  const pathRef = useRef(path)

  searchModeRef.current = searchMode
  pathRef.current = path

  useEffect(() => {
    mounted.current = true
    return () => (mounted.current = false)
  })

  /**
   * Only call on mount as selection passed as input property should be initially forwarded to selection module
   */
  useEffect(() => {
    if (selection) {
      setSelection(selection)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (clearSelectionOnParentChange) {
      changeListParent(parent)
      /**
       * Reset selection when changing page so that an action is only applied on one page.
       * Do not call `changeSelection` since this is not an active user action.
       * So far we use the `onSelectionChange` for choosing a document and selecting e.g. folders for remote fields.
       * For those use cases we want to keep the selection over multiple pages.
       */
      clearSelection([])
    }
  }, [parent, clearSelection, changeListParent, clearSelectionOnParentChange])

  const handleRowClick = id => {
    const [model, key] = id.split('/')
    const location = getCustomLocation ? getCustomLocation(model, key) : getDefaultLocation(model, key)

    onRowClick(id)

    if (location) {
      if (openResource && model === 'Resource') {
        openResource(location)
      } else {
        navigate(location)
      }
    } else if (selectOnRowClick) {
      setSelection([id])
    }
  }

  const tql = !parent && !keys ? getTql(domainTypes) : null

  return (
    <>
      <DocsList
        id="documents"
        formName={formName}
        limit={limit || 25}
        onRowClick={handleRowClick}
        searchFormType={searchFormType || searchFormTypes.ADMIN}
        parent={parent}
        cellRenderers={{
          'dms-label-with-icon': (rowData, column, cellRenderer) => (
            <StyledContentWrapper>
              <StyledIconWrapper>
                <Icon icon={rowData.icon} />
              </StyledIconWrapper>
              <StyledLabelWrapper publishStatus={rowData.publish_status}>
                {cellRenderer(column.children[0])}
              </StyledLabelWrapper>
            </StyledContentWrapper>
          ),
          'dms-actions': (rowData, column, cellRenderer) => (
            <StyledContentWrapper>
              {column.children
                .filter(c => !c.dmsEntityModel || c.dmsEntityModel === rowData.type)
                .map(c => cellRenderer(c))}
            </StyledContentWrapper>
          )
        }}
        tql={tql}
        keys={keys}
        showActions={showActions}
        sortable={sortable}
        searchFormCollapsed={searchFormCollapsed}
        scrollBehaviour={scrollBehaviour}
      />
      <FileInput />
    </>
  )
}

DocsView.propTypes = {
  domainTypes: PropTypes.arrayOf(PropTypes.string),
  rootNodes: PropTypes.arrayOf(
    PropTypes.shape({
      entityName: PropTypes.string,
      key: PropTypes.string
    })
  ),
  path: PropTypes.string.isRequired,
  params: PropTypes.object.isRequired,
  navigate: PropTypes.func.isRequired,
  changeListParent: PropTypes.func.isRequired,
  clearSelection: PropTypes.func.isRequired,
  setSelection: PropTypes.func.isRequired,
  selection: PropTypes.arrayOf(PropTypes.string),
  clearSelectionOnParentChange: PropTypes.bool.isRequired,
  selectOnRowClick: PropTypes.bool.isRequired,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  searchFormType: searchFormTypePropTypes,
  getCustomLocation: PropTypes.func,
  getListFormName: PropTypes.func,
  domainDetailFormName: PropTypes.string,
  folderDetailFormName: PropTypes.string,
  showActions: PropTypes.bool,
  sortable: PropTypes.bool,
  searchMode: PropTypes.bool,
  openResource: PropTypes.func,
  onRowClick: PropTypes.func,
  searchFormCollapsed: PropTypes.bool,
  scrollBehaviour: scrollBehaviourPropType
}

export default DocsView
