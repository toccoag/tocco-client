import styled, {css} from 'styled-components'
import {scale, themeSelector} from 'tocco-ui'

export const StyledContentWrapper = styled.div`
  display: flex;
  align-items: center;
  color: ${themeSelector.color('text')};
`

export const StyledIconWrapper = styled.span`
  font-size: ${scale.font(1)};
  display: inline-block;
  margin-right: ${scale.space(-2)};
`

export const StyledLabelWrapper = styled.span`
  overflow: hidden;

  && span {
    display: flex;
    ${({publishStatus, theme}) =>
      publishStatus === 'offline' &&
      css`
        color: ${theme.colors.textLighter};
        font-style: italic;
        padding-right: ${scale.space(-2)};
      `}
    ${({publishStatus, theme}) =>
      publishStatus === 'changed' &&
      `
      color: ${theme.colors.secondaryLighter};
      font-weight: ${theme.fontWeights.bold};
    `}
  }
`
