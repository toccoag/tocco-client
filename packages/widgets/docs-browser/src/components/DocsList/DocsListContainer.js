import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {setSearchFormCollapsed, initialize} from '../../modules/docsList/actions'

import DocsList from './DocsList'

const mapActionCreators = {
  initialize,
  setSearchFormCollapsed
}

const mapStateToProps = state => ({
  searchFormCollapsed: state.input.searchFormCollapsed,
  scrollBehaviour: state.input.scrollBehaviour
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(DocsList))
