import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {scrollBehaviourPropType, SidepanelMainContent, Sidepanel, SidepanelContainer} from 'tocco-ui'

import searchFormTypes, {searchFormTypePropTypes} from '../../utils/searchFormTypes'

import ListView from './ListView'
import SearchView from './SearchView'

const DocsList = ({
  initialize,
  formName,
  limit,
  parent,
  searchFormType,
  searchFormCollapsed,
  scrollBehaviour,
  setSearchFormCollapsed,
  onRowClick,
  cellRenderers,
  sortable,
  tql,
  keys,
  showActions
}) => {
  useEffect(() => {
    initialize(formName, searchFormType, limit, parent, tql, keys)
  }, [initialize, formName, searchFormType, limit, parent, tql, keys])

  if (searchFormType === searchFormTypes.NONE) {
    return (
      <ListView onRowClick={onRowClick} cellRenderers={cellRenderers} sortable={sortable} showActions={showActions} />
    )
  }

  return (
    <SidepanelContainer
      sidepanelPosition="left"
      sidepanelCollapsed={searchFormCollapsed}
      setSidepanelCollapsed={setSearchFormCollapsed}
      scrollBehaviour={scrollBehaviour}
    >
      <Sidepanel>
        <SearchView />
      </Sidepanel>
      <SidepanelMainContent>
        <ListView onRowClick={onRowClick} cellRenderers={cellRenderers} sortable={sortable} showActions={showActions} />
      </SidepanelMainContent>
    </SidepanelContainer>
  )
}

DocsList.propTypes = {
  initialize: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
  limit: PropTypes.number,
  searchFormType: searchFormTypePropTypes,
  searchFormCollapsed: PropTypes.bool,
  scrollBehaviour: scrollBehaviourPropType,
  setSearchFormCollapsed: PropTypes.func,
  onRowClick: PropTypes.func,
  parent: PropTypes.object,
  cellRenderers: PropTypes.objectOf(PropTypes.func),
  sortable: PropTypes.bool,
  showActions: PropTypes.bool,
  tql: PropTypes.string,
  keys: PropTypes.array
}

export default DocsList
