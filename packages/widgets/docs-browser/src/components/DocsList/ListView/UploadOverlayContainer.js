import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import UploadOverlay from './UploadOverlay'

const mapStateToProps = state => ({
  breadcrumbs: state.docs.path.breadcrumbs
})

export default connect(mapStateToProps)(injectIntl(UploadOverlay))
