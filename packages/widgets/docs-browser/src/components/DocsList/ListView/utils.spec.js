import {uploadActionExists} from './utils'

describe('docs-browser', () => {
  describe('components', () => {
    describe('DocsList', () => {
      describe('ListView', () => {
        describe('utils', () => {
          describe('uploadActionExists', () => {
            test('should find upload action in action bar', () => {
              const formDefinition = {
                children: [
                  {
                    componentType: 'action-bar',
                    children: [
                      {
                        id: 'upload-document'
                      }
                    ]
                  }
                ]
              }
              expect(uploadActionExists(formDefinition)).to.be.true
            })
            test('should find nested upload action', () => {
              const formDefinition = {
                children: [
                  {
                    componentType: 'action-bar',
                    children: [
                      {
                        id: 'group',
                        children: [
                          {
                            id: 'group',
                            children: [
                              {
                                id: 'upload-directory'
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
              expect(uploadActionExists(formDefinition)).to.be.true
            })
            test('should handle missing action bar', () => {
              const formDefinition = {
                children: []
              }
              expect(uploadActionExists(formDefinition)).to.be.false
            })
            test('should handle missing upload action', () => {
              const formDefinition = {
                children: [
                  {
                    componentType: 'action-bar',
                    children: [
                      {
                        id: 'some-action'
                      }
                    ]
                  }
                ]
              }
              expect(uploadActionExists(formDefinition)).to.be.false
            })
          })
        })
      })
    })
  })
})
