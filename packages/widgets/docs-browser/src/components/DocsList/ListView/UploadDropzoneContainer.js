import {connect} from 'react-redux'

import {upload} from '../../../modules/create/actions'
import {getFormDefinition} from '../../../modules/selectors'
import {withRouterTypeCompProvider} from '../../../utils/withRouterTypeCompProvider'

import UploadDropzone from './UploadDropzone'

const mapActionCreators = {
  upload
}

const mapStateToProps = state => ({
  formDefinition: getFormDefinition(state)
})

export default withRouterTypeCompProvider(connect(mapStateToProps, mapActionCreators)(UploadDropzone))
