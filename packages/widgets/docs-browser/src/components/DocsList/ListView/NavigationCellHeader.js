import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {BallMenu, MenuItem} from 'tocco-ui'

import {StyledNavigationCellHeader} from './StyledComponents'

const NavigationCellHeader = ({
  disablePreferencesMenu,
  displayColumnModal,
  resetColumns,
  sortable,
  resetSorting,
  resetPreferences,
  displayTableRowsModal
}) =>
  !disablePreferencesMenu ? (
    <StyledNavigationCellHeader>
      <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-preferences'}}>
        <MenuItem onClick={displayColumnModal} data-cy="menuitem-columns">
          <FormattedMessage id="client.component.list.preferences.columns" />
        </MenuItem>
        <MenuItem onClick={displayTableRowsModal} data-cy="menuitem-numOfRows">
          <FormattedMessage id="client.component.list.preferences.numOfRows" />
        </MenuItem>
        <MenuItem onClick={resetColumns} data-cy="menuitem-columnsReset">
          <FormattedMessage id="client.component.list.preferences.columns.reset" />
        </MenuItem>
        {sortable && (
          <MenuItem onClick={resetSorting} data-cy="menuitem-sortingReset">
            <FormattedMessage id="client.component.list.preferences.sorting.reset" />
          </MenuItem>
        )}
        <MenuItem onClick={resetPreferences} data-cy="menuitem-preferencesReset">
          <FormattedMessage id="client.component.list.preferences.reset" />
        </MenuItem>
      </BallMenu>
    </StyledNavigationCellHeader>
  ) : null

NavigationCellHeader.propTypes = {
  displayColumnModal: PropTypes.func.isRequired,
  resetSorting: PropTypes.func.isRequired,
  resetPreferences: PropTypes.func.isRequired,
  resetColumns: PropTypes.func.isRequired,
  sortable: PropTypes.bool,
  disablePreferencesMenu: PropTypes.bool,
  displayTableRowsModal: PropTypes.func.isRequired
}

export default NavigationCellHeader
