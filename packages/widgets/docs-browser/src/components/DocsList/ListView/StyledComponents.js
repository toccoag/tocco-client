import styled from 'styled-components'

export const StyledListWrapper = styled.div`
  grid-row-start: table-start;
`

export const StyledDropzone = styled.div`
  width: 100%;
  height: 100%;
  position: relative;
`

export const StyledUploadOverlay = styled.div`
  cursor: pointer;
  position: absolute;
  inset: 0;
  background-color: rgba(0 0 255 / 10%);
  display: flex;
  align-items: center;
  justify-content: center;
`

export const StyledNavigationCellHeader = styled.div`
  position: relative;
  right: 3px;
`
