import {formsApi} from 'tocco-entity-list/src/main'

const uploadActions = ['upload-document', 'upload-directory']

export const uploadActionExists = formDefinition =>
  gatherChildren(formsApi.getActionBar(formDefinition))
    .map(action => action.id)
    .some(id => uploadActions.includes(id))

const gatherChildren = container =>
  container ? [container, ...(container.children || []).flatMap(child => gatherChildren(child))] : []
