import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {formsApi, getTableSelectionStyle, selectionActions} from 'tocco-entity-list/src/main'

import {changePage, refresh, setSortingInteractive} from '../../../modules/list/actions'
import {changePosition, resetSorting, changeWidth} from '../../../modules/preferences/actions'
import {getActualLimit, getFormDefinition} from '../../../modules/selectors'
import {scope} from '../../../utils/constants'

import Table from './Table'

const mapActionCreators = {
  changePage,
  refresh,
  setSortingInteractive,
  onSelectChange: selectionActions.onSelectChange,
  setSelection: selectionActions.setSelection,
  changePosition,
  resetSorting,
  changeWidth
}

const mapStateToProps = state => ({
  currentPage: state.list.currentPage,
  entities: state.list.entities,
  entityCount: state.list.entityCount,
  limit: getActualLimit(state),
  inProgress: state.list.inProgress,
  tableSelectionStyle: getTableSelectionStyle(
    state.input.selectionStyle,
    formsApi.getSelectable(getFormDefinition(state), scope)
  ),
  clickable: formsApi.getClickable(getFormDefinition(state)),
  selection: state.selection.selection,
  selectionFilterFn: state.input.selectionFilterFn,
  parent: state.docsList.parent,
  positions: state.preferences.positions,
  widths: state.preferences.widths,
  scrollBehaviour: state.input.scrollBehaviour
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Table))
