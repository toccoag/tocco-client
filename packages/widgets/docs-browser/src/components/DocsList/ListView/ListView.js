import PropTypes from 'prop-types'
import React, {useMemo} from 'react'
import {errorLogging} from 'tocco-app-extensions'
import {formsApi, getActionBarContent, StyledActionWrapper, StyledListView} from 'tocco-entity-list/src/main'
import {ButtonContextProvider, LoadMask} from 'tocco-ui'
import {js, env} from 'tocco-util'

import {getSelectionForAction} from '../../../modules/selectors'

import {StyledListWrapper} from './StyledComponents'
import TableContainer from './TableContainer'
import UploadDropzone from './UploadDropzoneContainer'

const ListView = ({
  dataLoadingInProgress,
  formDefinition,
  parent,
  showActions,
  showSelectionController,
  sorting,
  columnDisplayPreferences,
  cellRenderers,
  preferencesLoaded,
  intl,
  sortable,
  onRowClick
}) => {
  const msg = (id, values = {}) => intl.formatMessage({id}, values)

  const List = useMemo(() => {
    if (formDefinition && preferencesLoaded) {
      const table = formsApi.getTable(formDefinition)
      const columnsDefinitions = formsApi.getColumnDefinition({
        table,
        sorting,
        sortable,
        parent,
        intl,
        columnDisplayPreferences,
        cellRenderers
      })

      return (
        <StyledListWrapper searchFormPosition="left" key={`tableWrapper-${table.id}`}>
          <UploadDropzone>
            <TableContainer key={`table-${table.id}`} columnDefinitions={columnsDefinitions} onRowClick={onRowClick} />
          </UploadDropzone>
        </StyledListWrapper>
      )
    }
  }, [
    formDefinition,
    sorting,
    columnDisplayPreferences,
    preferencesLoaded,
    sortable,
    parent,
    intl,
    cellRenderers,
    onRowClick
  ])

  const ActionBar = useMemo(() => {
    if (formDefinition) {
      const {content, actionBar} =
        getActionBarContent({
          formDefinition,
          parent,
          dataLoadingInProgress,
          showSelectionController,
          showActions,
          getSelectionForAction
        }) || {}

      if (content.length > 0 && actionBar) {
        return (
          <ButtonContextProvider key={`listActionWrapper-${actionBar.id}`}>
            {ref => (
              <StyledActionWrapper ref={ref} leftAligned={env.isInWidgetEmbedded()}>
                {content}
              </StyledActionWrapper>
            )}
          </ButtonContextProvider>
        )
      }
    }
  }, [dataLoadingInProgress, formDefinition, parent, showSelectionController, showActions])

  return (
    <LoadMask required={[formDefinition]} loadingText={msg('client.component.list.loadingText')}>
      <StyledListView>
        <errorLogging.ErrorBoundary>{ActionBar}</errorLogging.ErrorBoundary>
        <errorLogging.ErrorBoundary>{List}</errorLogging.ErrorBoundary>
      </StyledListView>
    </LoadMask>
  )
}

ListView.propTypes = {
  intl: PropTypes.object.isRequired,
  formDefinition: PropTypes.shape({
    children: PropTypes.array
  }),
  selection: PropTypes.arrayOf(PropTypes.string),
  currentPageIds: PropTypes.arrayOf(PropTypes.string),
  parent: PropTypes.shape({
    key: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    reverseRelationName: PropTypes.string
  }),
  dataLoadingInProgress: PropTypes.bool,
  showSelectionController: PropTypes.bool,
  showActions: PropTypes.bool,
  sorting: PropTypes.arrayOf(
    PropTypes.shape({
      field: PropTypes.string,
      order: PropTypes.string
    })
  ),
  sortable: PropTypes.bool,
  columnDisplayPreferences: PropTypes.objectOf(PropTypes.bool),
  cellRenderers: PropTypes.objectOf(PropTypes.func),
  preferencesLoaded: PropTypes.bool,
  onRowClick: PropTypes.func
}

const areEqual = (prevProps, nextProps) => {
  const diff = Object.keys(js.difference(prevProps, nextProps))
  return diff.length === 0
}

export default React.memo(ListView, areEqual)
