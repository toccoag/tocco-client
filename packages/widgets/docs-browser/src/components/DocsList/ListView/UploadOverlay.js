import PropTypes from 'prop-types'
import {Typography} from 'tocco-ui'

import {StyledUploadOverlay} from './StyledComponents'

const UploadOverlay = ({breadcrumbs, intl}) => {
  const msg = (id, values = {}) => intl.formatMessage({id}, values)

  return (
    <StyledUploadOverlay onDrop={event => event.preventDefault()}>
      <Typography.H1>
        {msg('client.docs-browser.uploadOverlayMessage', {path: breadcrumbs.map(crumb => crumb.display).join('/')})}
      </Typography.H1>
    </StyledUploadOverlay>
  )
}

UploadOverlay.propTypes = {
  breadcrumbs: PropTypes.array,
  intl: PropTypes.object.isRequired
}

export default UploadOverlay
