import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {formsApi, showSelectionComponent} from 'tocco-entity-list/src/main'

import {getFormDefinition} from '../../../modules/selectors'
import {scope} from '../../../utils/constants'

import ListView from './ListView'

const mapStateToProps = (state, props) => ({
  formDefinition: getFormDefinition(state),
  selection: state.selection.selection,
  currentPageIds: state.list.entities.map(e => e.__key),
  parent: state.docsList.parent,
  dataLoadingInProgress: state.list.inProgress,
  showSelectionController: showSelectionComponent(
    state.input.selectionStyle,
    false,
    formsApi.getSelectable(getFormDefinition(state), scope)
  ),
  sorting: state.list.sorting,
  columnDisplayPreferences: state.preferences.columns,
  preferencesLoaded: state.preferences.preferencesLoaded
})

export default connect(mapStateToProps, null)(injectIntl(ListView))
