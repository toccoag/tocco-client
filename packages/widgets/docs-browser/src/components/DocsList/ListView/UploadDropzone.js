import PropTypes from 'prop-types'
import {useMemo} from 'react'
import {useDropzone} from 'react-dropzone'

import {StyledDropzone} from './StyledComponents'
import UploadOverlay from './UploadOverlayContainer'
import {uploadActionExists} from './utils'

const UploadDropzone = ({upload, path, formDefinition, children}) => {
  const onDrop = files => {
    upload(files, path)
  }

  const uploadAvailable = useMemo(() => uploadActionExists(formDefinition), [formDefinition])

  const {getRootProps, isDragActive} = useDropzone({
    onDrop,
    noClick: true,
    noKeyboard: true,
    useFsAccessApi: false,
    disabled: !uploadAvailable
  })

  return (
    <StyledDropzone {...getRootProps()}>
      {children}
      {isDragActive && <UploadOverlay />}
    </StyledDropzone>
  )
}

UploadDropzone.propTypes = {
  upload: PropTypes.func.isRequired,
  path: PropTypes.string,
  formDefinition: PropTypes.shape({
    children: PropTypes.array
  }),
  children: PropTypes.element
}

export default UploadDropzone
