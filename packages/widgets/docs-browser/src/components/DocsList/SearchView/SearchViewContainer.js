import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {displaySearchFieldsModal, resetSearchFields, resetSearch} from '../../../modules/searchForm/actions'

import SearchView from './SearchView'

const mapActionCreators = {
  displaySearchFieldsModal,
  resetSearch,
  resetSearchFields
}

const mapStateToProps = state => ({
  initialized: state.searchForm.initialized
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(SearchView))
