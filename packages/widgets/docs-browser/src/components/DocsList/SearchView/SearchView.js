import PropTypes from 'prop-types'
import {useRef} from 'react'
import {FormattedMessage} from 'react-intl'
import {errorLogging} from 'tocco-app-extensions'
import {Box, StyledSplitWrapper} from 'tocco-entity-list/src/main'
import {Ball, BallMenu, MenuItem, SidepanelHeader} from 'tocco-ui'
import {react as customHooks} from 'tocco-util'

import BasicSearchForm from './BasicSearchFormContainer'

const SearchView = ({intl, displaySearchFieldsModal, resetSearch, resetSearchFields, initialized}) => {
  const searchFormEl = useRef(null)
  const msg = id => intl.formatMessage({id})

  customHooks.useAutofocus(searchFormEl, {selectFulltextFields: true}, [initialized])

  return (
    <errorLogging.ErrorBoundary>
      <SidepanelHeader>
        <Ball data-cy="btn-reset" icon="times" onClick={resetSearch} title={msg('client.component.list.reset')} />
        <BallMenu buttonProps={{icon: 'ellipsis-v', 'data-cy': 'btn-search-menu'}}>
          <MenuItem onClick={displaySearchFieldsModal} data-cy="menuitem-adjust-search-form">
            <FormattedMessage id="client.component.list.search.settings.searchForm.edit" />
          </MenuItem>
          <MenuItem onClick={resetSearchFields} data-cy="menuitem-reset-search-form">
            <FormattedMessage id="client.component.list.search.settings.searchForm.reset" />
          </MenuItem>
        </BallMenu>
      </SidepanelHeader>
      <StyledSplitWrapper>
        <Box ref={searchFormEl}>
          <BasicSearchForm />
        </Box>
      </StyledSplitWrapper>
    </errorLogging.ErrorBoundary>
  )
}

SearchView.propTypes = {
  intl: PropTypes.object.isRequired,
  initialized: PropTypes.bool.isRequired,
  displaySearchFieldsModal: PropTypes.func.isRequired,
  resetSearch: PropTypes.func.isRequired,
  resetSearchFields: PropTypes.func.isRequired
}

export default SearchView
