import PropTypes from 'prop-types'
import {useRef} from 'react'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {react as customHooks} from 'tocco-util'

const REDUX_FORM_NAME = 'searchForm'

const BasicSearchForm = ({entity, form: formName, formValues, searchFormDefinition, submitSearchForm}) => {
  const searchFormEl = useRef(null)

  customHooks.useAutofocus(searchFormEl, {selectFulltextFields: true}, [searchFormDefinition])

  if (!searchFormDefinition.children) {
    return null
  }

  const handleSubmit = e => {
    e.preventDefault()
    e.stopPropagation()
    submitSearchForm()
  }

  return (
    <div ref={searchFormEl}>
      <form onSubmit={handleSubmit}>
        <form.FormBuilder
          entity={entity}
          formName={formName}
          formDefinition={searchFormDefinition}
          formValues={formValues}
          fieldMappingType="search"
          mode="search"
          labelPosition="inside"
        />
      </form>
    </div>
  )
}

BasicSearchForm.propTypes = {
  searchFormDefinition: PropTypes.shape({
    children: PropTypes.array
  }).isRequired,
  submitSearchForm: PropTypes.func.isRequired,
  entity: PropTypes.object,
  form: PropTypes.string,
  formValues: PropTypes.object
}

export default reduxForm({
  form: REDUX_FORM_NAME,
  destroyOnUnmount: false
})(BasicSearchForm)
