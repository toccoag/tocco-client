import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {submitSearchForm} from '../../../modules/searchForm/actions'

import BasicSearchForm from './BasicSearchForm'

const mapActionCreators = {
  submitSearchForm
}

const mapStateToProps = state => ({
  searchFormDefinition: state.searchForm.formDefinition
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(BasicSearchForm))
