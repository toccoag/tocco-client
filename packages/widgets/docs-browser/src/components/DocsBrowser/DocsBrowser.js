import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {errorLogging} from 'tocco-app-extensions'
import {scrollBehaviourPropType} from 'tocco-ui'
import {react} from 'tocco-util'

import isRootLocation, {isSearchLocation} from '../../utils/isRootLocation'

import Breadcrumbs from './Breadcrumbs'
import Content from './Content'
import {StyledWrapper, StyledBreadcrumbs, StyledContent} from './StyledComponents'

const DocsBrowser = ({
  navigate,
  navigateReplace,
  path,
  searchMode,
  navigationStrategy,
  setSearchMode,
  reinitializeList,
  savePersistedSearchForm,
  loadPersistedSearchForm,
  loadBreadcrumbs,
  noLeftPadding,
  scrollBehaviour
}) => {
  const prevPath = react.usePrevious(path)
  const prevSearchMode = react.usePrevious(searchMode)

  useEffect(() => {
    loadBreadcrumbs(path)
  })

  useEffect(() => {
    // only when search mode has changed
    if (searchMode === prevSearchMode) {
      return
    }

    if (searchMode && !isSearchLocation(path)) {
      /**
       * user has started search by input in search form
       *  - navigate to search route
       */
      navigate('/docs/search')
    } else if (!searchMode && isSearchLocation(path)) {
      /**
       * user has reset search by clearing search form
       */
      navigateReplace('/docs')
    }
  }, [searchMode, navigate, navigateReplace, path, prevSearchMode])

  useEffect(() => {
    // only when path has changed
    if (path === prevPath) {
      return
    }

    /**
     * prevPath: previous/old pathname
     * path: next/new pathname
     */
    if (isSearchLocation(prevPath) && searchMode) {
      /**
       * navigate away from search page (e.g. history back, into subfolder, via breadcrumbs)
       *  - disable search mode
       *  - save search form to re-load form when coming back
       *  - re-init list manually when going to root location
       */
      const shouldReinitList = isRootLocation(path)
      savePersistedSearchForm()
      setSearchMode(false)
      if (shouldReinitList) {
        reinitializeList()
      }
    } else if (isSearchLocation(path) && !searchMode) {
      /**
       * navigate to search location (e.g. history back)
       *  - enable search mode
       *  - load search form
       *  - re-init list manually when coming from root location
       */
      const shouldReinitList = isRootLocation(prevPath)
      loadPersistedSearchForm()
      setSearchMode(true)
      if (shouldReinitList) {
        reinitializeList()
      }
    }
  }, [prevPath, path, searchMode, savePersistedSearchForm, setSearchMode, loadPersistedSearchForm, reinitializeList])

  return (
    <StyledWrapper scrollBehaviour={scrollBehaviour}>
      <errorLogging.ErrorBoundary>
        <StyledBreadcrumbs noLeftPadding={noLeftPadding}>
          <Breadcrumbs />
        </StyledBreadcrumbs>
      </errorLogging.ErrorBoundary>
      <StyledContent scrollBehaviour={scrollBehaviour}>
        <Content navigationStrategy={navigationStrategy} searchMode={searchMode} />
      </StyledContent>
    </StyledWrapper>
  )
}

DocsBrowser.propTypes = {
  loadBreadcrumbs: PropTypes.func.isRequired,
  setSearchMode: PropTypes.func.isRequired,
  reinitializeList: PropTypes.func.isRequired,
  savePersistedSearchForm: PropTypes.func.isRequired,
  loadPersistedSearchForm: PropTypes.func.isRequired,
  navigate: PropTypes.func.isRequired,
  navigateReplace: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  searchMode: PropTypes.bool.isRequired,
  navigationStrategy: PropTypes.object,
  noLeftPadding: PropTypes.bool,
  scrollBehaviour: scrollBehaviourPropType
}

export default DocsBrowser
