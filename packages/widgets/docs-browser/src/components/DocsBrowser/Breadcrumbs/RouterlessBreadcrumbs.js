import PropTypes from 'prop-types'
import {withTheme} from 'styled-components'
import {Breadcrumbs, theme as themeUtil} from 'tocco-ui'

const BreadcrumbLink = props => {
  return <span {...props}></span>
}

const DocsBrowserBreadcrumbs = ({embedded, routerType, navigate, theme, pathPrefix, breadcrumbsInfo}) => {
  const handleBreadcrumbsClick = breadcrumbsItem => {
    navigate(`/docs/${breadcrumbsItem.path}`)
  }

  return (
    <Breadcrumbs
      {...(embedded ? {backgroundColor: themeUtil.color('paper')({theme})} : {})}
      updateDocumentTitle={routerType === 'inherit'}
      onClick={handleBreadcrumbsClick}
      linkComp={BreadcrumbLink}
      pathPrefix={pathPrefix}
      breadcrumbsInfo={breadcrumbsInfo}
      showToccoHome={!embedded}
    />
  )
}

DocsBrowserBreadcrumbs.propTypes = {
  embedded: PropTypes.bool,
  routerType: PropTypes.string,
  navigate: PropTypes.func,
  pathPrefix: PropTypes.string,
  breadcrumbsInfo: PropTypes.array,
  theme: PropTypes.object
}

export default withTheme(DocsBrowserBreadcrumbs)
