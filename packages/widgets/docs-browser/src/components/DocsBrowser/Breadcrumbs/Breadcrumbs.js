import PropTypes from 'prop-types'
import {withTheme} from 'styled-components'
import {Breadcrumbs, theme as themeUtil} from 'tocco-ui'

const DocsBrowserBreadcrumbs = ({embedded, routerType, theme, pathPrefix, breadcrumbsInfo}) => (
  <Breadcrumbs
    {...(embedded ? {backgroundColor: themeUtil.color('paper')({theme})} : {})}
    updateDocumentTitle={routerType === 'inherit'}
    pathPrefix={pathPrefix}
    breadcrumbsInfo={breadcrumbsInfo}
    showToccoHome={!embedded}
  />
)

DocsBrowserBreadcrumbs.propTypes = {
  embedded: PropTypes.bool,
  routerType: PropTypes.string,
  pathPrefix: PropTypes.string,
  breadcrumbsInfo: PropTypes.array,
  theme: PropTypes.object
}

export default withTheme(DocsBrowserBreadcrumbs)
