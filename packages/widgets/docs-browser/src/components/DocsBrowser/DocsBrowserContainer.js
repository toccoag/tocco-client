import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {reinitialize} from '../../modules/docsList/actions'
import {loadBreadcrumbs, setSearchMode} from '../../modules/path/actions'
import {savePersistedSearchForm, loadPersistedSearchForm} from '../../modules/searchForm/actions'
import {withRouterTypeCompProvider} from '../../utils/withRouterTypeCompProvider'

import DocsBrowser from './DocsBrowser'

const mapStateToProps = state => ({
  searchMode: state.docs.path.searchMode,
  navigationStrategy: state.input.navigationStrategy,
  noLeftPadding: state.input.noLeftPadding,
  scrollBehaviour: state.input.scrollBehaviour
})

const mapActionCreators = {
  loadBreadcrumbs,
  setSearchMode,
  savePersistedSearchForm,
  loadPersistedSearchForm,
  reinitializeList: reinitialize
}

export default withRouterTypeCompProvider(connect(mapStateToProps, mapActionCreators)(injectIntl(DocsBrowser)))
