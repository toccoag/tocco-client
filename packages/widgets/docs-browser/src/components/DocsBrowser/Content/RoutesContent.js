import PropTypes from 'prop-types'
import {Route, Routes} from 'react-router-dom'

import DocsView from '../../DocsView'
import DocumentView from '../../DocumentView'

const RoutesContent = ({navigationStrategy, searchMode}) => (
  <Routes>
    <Route exact path="doc/:key/detail" element={<DocumentView navigationStrategy={navigationStrategy} />} />
    <Route exact path=":model/:key/list" element={<DocsView searchMode={searchMode} />} />
    <Route exact path="search" element={<DocsView searchMode={searchMode} />} />
    <Route exact path="/" element={<DocsView searchMode={searchMode} />} />
  </Routes>
)

RoutesContent.propTypes = {
  params: PropTypes.object,
  searchMode: PropTypes.bool.isRequired,
  navigationStrategy: PropTypes.object
}

export default RoutesContent
