import PropTypes from 'prop-types'

import DocsView from '../../DocsView'
import DocumentView from '../../DocumentView'

const RouterlessContent = ({params, navigationStrategy, searchMode}) => {
  const {model, view} = params

  if (model === 'doc' && view === 'detail') {
    return <DocumentView navigationStrategy={navigationStrategy} />
  }

  return <DocsView searchMode={searchMode} />
}

RouterlessContent.propTypes = {
  params: PropTypes.object,
  searchMode: PropTypes.bool.isRequired,
  navigationStrategy: PropTypes.object
}

export default RouterlessContent
