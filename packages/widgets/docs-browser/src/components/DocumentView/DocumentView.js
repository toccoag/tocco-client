import PropTypes from 'prop-types'
import React, {Suspense, useState} from 'react'
import {getEntityOverviewUrl} from 'tocco-history/src/main'
import {LoadMask, usePrompt} from 'tocco-ui'

import Action from '../Action/'

import {StyledDocumentViewWrapper} from './StyledComponents'

const LazyDetailApp = React.lazy(() => import('./LazyDetailApp'))

const DocumentView = ({
  breadcrumbs,
  formName,
  navigationStrategy,
  locale,
  emitAction,
  navigate,
  params,
  intl,
  showPendingChangesModal
}) => {
  const msg = id => intl.formatMessage({id})
  const [touched, setTouched] = useState(false)

  usePrompt({
    when: touched,
    message: msg('client.docs-browser.detail.confirmTouchedFormLeave'),
    showPendingChangesModal
  })

  const handleEntityDeleted = () => {
    const lastList = breadcrumbs
      .slice()
      .reverse()
      .find(breadcrumb => breadcrumb.type !== 'record')
    const lastListUrl = `/docs/${lastList.path}`
    navigate(lastListUrl)
  }

  const history = (_definition, selection, _parent, _params, _config, _onSuccess, _onError) => {
    navigationStrategy.navigateToPath({pathname: getEntityOverviewUrl(selection), inNewTab: true})
  }

  return (
    <StyledDocumentViewWrapper>
      <Suspense fallback={<LoadMask />}>
        <LazyDetailApp
          entityName="Resource"
          entityId={params.key}
          formName={formName || 'DmsResource'}
          mode="update"
          actionAppComponent={Action}
          navigationStrategy={navigationStrategy}
          emitAction={emitAction}
          onEntityDeleted={handleEntityDeleted}
          locale={locale}
          customActions={{history}}
          onTouchedChange={setTouched}
        />
      </Suspense>
    </StyledDocumentViewWrapper>
  )
}

DocumentView.propTypes = {
  intl: PropTypes.object,
  breadcrumbs: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired
    })
  ).isRequired,
  formName: PropTypes.string,
  navigationStrategy: PropTypes.object,
  locale: PropTypes.string,
  emitAction: PropTypes.func.isRequired,
  navigate: PropTypes.func,
  params: PropTypes.object,
  showPendingChangesModal: PropTypes.func.isRequired
}

export default DocumentView
