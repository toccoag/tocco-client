import {FormattedMessage} from 'react-intl'
import {Icon} from 'tocco-ui'

import {StyledErrorWrapper, StyledIconWrapper, StyledTitle} from './StyledComponents'

const ErrorPlaceholder = () => (
  <StyledErrorWrapper>
    <StyledIconWrapper>
      <Icon icon="exclamation-circle" hasFixedWidth={false} />
    </StyledIconWrapper>
    <StyledTitle>
      <FormattedMessage id="client.widget-placeholder.error.title" />
    </StyledTitle>
    <FormattedMessage id="client.widget-placeholder.error" />
  </StyledErrorWrapper>
)

export default ErrorPlaceholder
