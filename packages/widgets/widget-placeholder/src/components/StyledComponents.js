import styled from 'styled-components'
import {declareFont, scale, themeSelector} from 'tocco-ui'

export const StyledErrorWrapper = styled.div`
  ${declareFont()}
  padding: ${scale.space(-1)};
  color: ${themeSelector.color('signal.warning')};
  border: 1px solid ${themeSelector.color('signal.warning')};
  display: flex;
  width: fit-content;
  border-radius: ${themeSelector.radii('statusLabel')};
`

export const StyledIconWrapper = styled.span`
  margin-right: ${scale.space(-1)};
`

export const StyledTitle = styled.span`
  font-weight: bold;
  margin-right: ${scale.space(-2)};
`
