import PropTypes from 'prop-types'

import ErrorPlaceholder from './ErrorPlaceholder'

const Placeholder = ({placeholder}) => {
  switch (placeholder) {
    case 'error':
      return <ErrorPlaceholder />
    default:
      return null
  }
}

Placeholder.propTypes = {
  placeholder: PropTypes.string
}

export default Placeholder
