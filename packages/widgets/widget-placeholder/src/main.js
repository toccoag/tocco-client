import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger, env} from 'tocco-util'

import Placeholder from './components/Placeholder'
import reducers, {sagas} from './modules/reducers'

const packageName = 'widget-placeholder'

const initApp = (id, input, events, publicPath) => {
  const content = <Placeholder placeholder={input.placeholder} />

  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const WidgetPlaceholderApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

WidgetPlaceholderApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  placeholder: PropTypes.string
}

export default WidgetPlaceholderApp
export const app = appFactory.createBundleableApp(packageName, initApp, WidgetPlaceholderApp)
