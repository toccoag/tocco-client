import {v4 as uuid} from 'uuid'

import EntityBrowserApp from './main'

export default {
  title: 'Widgets/Entity Browser',
  component: EntityBrowserApp,
  argTypes: {
    searchFormType: {
      options: ['none', 'fulltext', 'simple', 'simple_advanced', 'advanced'],
      control: {type: 'select'}
    },
    scrollBehaviour: {
      options: ['none', 'inline'],
      control: {type: 'select'}
    }
  }
}

const EntityBrowserStory = ({...args}, {globals: {embedType}}) => (
  <EntityBrowserApp key={uuid()} {...args} appContext={{embedType}} />
)

export const Story = EntityBrowserStory.bind({})
Story.args = {
  entityName: 'User',
  searchFormType: 'simple_advanced',
  limit: 20,
  scrollBehaviour: 'none'
}

export const RegistrationPresenceView = EntityBrowserStory.bind({})
RegistrationPresenceView.args = {
  searchFormType: 'simple_advanced',
  businessUnit: '__n-u-l-l__',
  reportIds: [],
  entityName: 'Registration',
  formBase: 'Registration_presence_view',
  limit: 25,
  searchFilters: []
}

export const PresenceCheckPercent = EntityBrowserStory.bind({})
PresenceCheckPercent.args = {
  searchFormType: 'simple_advanced',
  businessUnit: 'test1',
  reportIds: [],
  entityName: 'Event',
  formBase: 'Presence_check_percent',
  limit: 15,
  searchFilters: []
}

export const PresenceCheck = EntityBrowserStory.bind({})
PresenceCheck.args = {
  searchFormType: 'simple_advanced',
  businessUnit: 'test1',
  reportIds: [],
  entityName: 'Event',
  formBase: 'Presence_check',
  limit: 15,
  searchFilters: []
}
