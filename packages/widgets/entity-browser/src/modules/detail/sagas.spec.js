import {put, select, takeLatest, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest} from 'tocco-app-extensions'

import {fetchModel} from '../../util/api/entities'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('entity-browser', () => {
  describe('routes', () => {
    describe('detail', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_DETAIL_PARAMS, sagas.loadEntityDetail),
                takeLatest(actions.CLEAR_DETAIL_PARAMS, sagas.clearDetailParams),
                takeLatest(actions.SHOW_PENDING_CHANGES_MODAL, sagas.showPendingChangesModal)
              ])
            )
            expect(generator.next().done).to.be.true
          })

          describe('clearDetailParams saga', () => {
            test('set undefined detail params', () => {
              const gen = sagas.clearDetailParams()

              expect(gen.next().value).to.eql(put(actions.setDetailParams(undefined)))
              expect(gen.next().done).to.be.true
            })
          })

          describe('loadEntityDetail saga', () => {
            test('should set dispatch DetailParams object', () => {
              const entityName = 'User'
              const formBase = 'UserSearch'

              const modelPaths = ['relDummySubGrid']
              const entityId = '3'
              const parentUrl = `/${entityName}/1`
              const targetEntityName = 'Dummy_Entity'
              const initialKey = '1'
              const mode = 'update'
              const url = `${parentUrl}/${modelPaths[0]}/${entityId}`
              const showBackButton = true
              const expectedFormName = `${formBase}_${targetEntityName}`

              const expectedDetailParams = {
                mode,
                entityName: targetEntityName,
                entityId,
                formName: expectedFormName,
                showBackButton,
                parentUrl
              }

              return expectSaga(sagas.loadEntityDetail, actions.loadDetailParams(url))
                .provide([
                  [
                    select(sagas.entityBrowserSelector),
                    {
                      entityName,
                      formBase
                    }
                  ],
                  [
                    select(sagas.inputSelector),
                    {
                      initialKey
                    }
                  ],
                  [matchers.call.fn(sagas.getEntityForm), [targetEntityName, expectedFormName]]
                ])
                .call(sagas.getEntityForm, entityName, modelPaths, formBase)
                .put(actions.setDetailParams(expectedDetailParams))
                .run()
            })
          })

          describe('getEntityForm saga', () => {
            test('should return getEntityForm', () => {
              const entityName = 'User'
              const formBase = 'UserSearch'
              const dummyEntity = 'Dummy_Entity'
              const modelPaths = ['relDummySubGrid', 'relDummy2']
              const expectedTargetEntity = 'Dummy2'

              const modelUser = {
                relDummySubGrid: {
                  targetEntity: dummyEntity
                }
              }

              const modelDummyEntity = {
                relDummy2: {
                  targetEntity: expectedTargetEntity
                }
              }

              return expectSaga(sagas.getEntityForm, entityName, modelPaths, formBase)
                .provide([
                  [matchers.call(fetchModel, 'User'), modelUser],
                  [matchers.call(fetchModel, 'Dummy_Entity'), modelDummyEntity]
                ])
                .returns([expectedTargetEntity, 'UserSearch_Dummy2'])
                .run()
            })
          })

          test('should handle custom paths', () => {
            const entityName = 'User'
            const formBase = 'UserSearch'
            const dummyEntity = 'Dummy_Entity'
            const modelPaths = ['relDummySubGrid', 'relCustom', 'relCustom2']
            const expectedTargetEntity = 'Dummy2'

            const modelUser = {
              relDummySubGrid: {
                targetEntity: dummyEntity
              }
            }

            const modelDummyEntity = {
              relDummy2: {
                targetEntity: expectedTargetEntity
              }
            }

            const customListForm = {
              modelName: dummyEntity
            }

            const custom2ListForm = {
              modelName: expectedTargetEntity
            }

            return expectSaga(sagas.getEntityForm, entityName, modelPaths, formBase)
              .provide([
                [matchers.call(fetchModel, 'User'), modelUser],
                [matchers.call(fetchModel, 'Dummy_Entity'), modelDummyEntity],
                [matchers.call(rest.fetchForm, 'UserSearch_detail_relCustom', 'list'), customListForm],
                [matchers.call(rest.fetchForm, 'UserSearch_relCustom_detail_relCustom2', 'list'), custom2ListForm]
              ])
              .returns([expectedTargetEntity, `UserSearch_relCustom_relCustom2`])
              .run()
          })
        })
      })
    })
  })
})
