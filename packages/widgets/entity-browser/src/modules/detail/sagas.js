import {channel} from 'redux-saga'
import {call, put, select, takeLatest, all, take} from 'redux-saga/effects'
import {rest, notification} from 'tocco-app-extensions'
import {PendingChangesModal} from 'tocco-ui'

import {fetchModel} from '../../util/api/entities'
import detail from '../../util/detail'
import parseUrl from '../../util/parseUrl'
import doShowBackButton from '../../util/showBackButton'

import * as actions from './actions'

export const entityBrowserSelector = state => state.entityBrowser
export const inputSelector = state => state.input

export default function* sagas() {
  yield all([
    takeLatest(actions.LOAD_DETAIL_PARAMS, loadEntityDetail),
    takeLatest(actions.CLEAR_DETAIL_PARAMS, clearDetailParams),
    takeLatest(actions.SHOW_PENDING_CHANGES_MODAL, showPendingChangesModal)
  ])
}

export function* clearDetailParams() {
  yield put(actions.setDetailParams(undefined))
}

export function* loadEntityDetail({payload}) {
  const {modelPaths, entityId, parentUrl} = yield call(parseUrl, payload.url)
  const {entityName, formBase} = yield select(entityBrowserSelector)

  const mode = yield call(detail.getMode, entityId)

  const [targetEntityName, formName] = yield call(getEntityForm, entityName, modelPaths, formBase)

  const {initialKey} = yield select(inputSelector)
  const showBackButton = yield call(doShowBackButton, initialKey, modelPaths)

  const detailParams = {
    mode,
    entityId,
    entityName: targetEntityName,
    formName,
    parentUrl,
    showBackButton
  }

  yield put(actions.setDetailParams(detailParams))
}

export function* getEntityForm(entityName, modelPaths, formBase, formNameResult = formBase) {
  const [path, ...restPaths] = modelPaths
  if (!path) {
    return [entityName, formNameResult]
  }
  const model = yield call(fetchModel, entityName)
  const relation = model[path]

  if (!relation) {
    const form = yield call(rest.fetchForm, `${formBase}_detail_${path}`, 'list')
    return yield call(getEntityForm, form.modelName, restPaths, `${formBase}_${path}`)
  } else {
    const formName = `${formBase}_${relation.targetEntity}`
    return yield call(getEntityForm, relation.targetEntity, restPaths, formBase, formName)
  }
}

export function* promptConfirm(message) {
  const answerChannel = yield call(channel)

  yield put(
    notification.modal(
      'navigation-pending-changes',
      'client.entity-browser.detail.confirmTouchedFormTitle',
      message,
      ({close}) => {
        const onYes = () => {
          close()
          answerChannel.put('yes')
        }
        const onNo = () => {
          close()
          answerChannel.put('no')
        }
        return <PendingChangesModal onYes={onYes} onNo={onNo} />
      },
      true,
      () => answerChannel.put('no')
    )
  )
  return yield take(answerChannel)
}

export function* showPendingChangesModal({payload}) {
  const {message, confirmCallback} = payload
  const response = yield call(promptConfirm, message)
  if (response === 'yes') {
    yield call(confirmCallback, true)
  } else {
    yield call(confirmCallback, false)
  }
}
