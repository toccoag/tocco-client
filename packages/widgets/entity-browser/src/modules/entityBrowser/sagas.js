import {all, call, put, take, takeLatest} from 'redux-saga/effects'
import {appFactory, login, externalEvents} from 'tocco-app-extensions'

import {ON_ROUTE_CHANGE} from './actions'

export default function* sagas() {
  yield all([call(connectSocket), takeLatest(ON_ROUTE_CHANGE, invokeOnRouteChange)])
}

export function* connectSocket() {
  yield take(appFactory.INPUT_INITIALIZED)
  yield put(login.doSessionCheck())
}

export function* invokeOnRouteChange() {
  yield put(externalEvents.fireExternalEvent('onRouteChange'))
}
