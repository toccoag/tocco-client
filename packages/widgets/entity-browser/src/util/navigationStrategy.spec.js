import navigationStrategy from './navigationStrategy'

describe('entity-browser', () => {
  describe('util', () => {
    describe('navigationStrategy', () => {
      describe('navigateToCreateRelative', () => {
        test('should navigate to detail in create mode on list', () => {
          const navigate = sinon.spy()
          const params = {}

          const strategy = navigationStrategy(navigate, params)
          strategy.navigateToCreateRelative()

          expect(navigate).to.have.been.calledWith('detail')
        })

        test('should navigate to detail in create mode on detail', () => {
          const navigate = sinon.spy()
          const params = {entityId: '123'}

          const strategy = navigationStrategy(navigate, params)
          strategy.navigateToCreateRelative()

          expect(navigate).to.have.been.calledWith('..')
        })

        test('should navigate to relation detail in create mode on detail', () => {
          const navigate = sinon.spy()
          const params = {entityId: '123'}

          const strategy = navigationStrategy(navigate, params)
          strategy.navigateToCreateRelative('relReservation')

          expect(navigate).to.have.been.calledWith('relReservation')
        })

        test('should navigate to relation detail in create mode on relation detail', () => {
          const navigate = sinon.spy()
          const params = {entityId: '123/relReservation/333'}

          const strategy = navigationStrategy(navigate, params)
          strategy.navigateToCreateRelative()

          expect(navigate).to.have.been.calledWith('..')
        })
      })

      describe('navigateToAction', () => {
        test('should navigate to action', () => {
          const definition = {appId: 'input-edit', properties: {a: 'foo'}}
          const selection = {entityName: 'Input'}
          const navigate = sinon.spy()
          const params = {}
          const location = {pathname: '/detail/123'}

          const strategy = navigationStrategy(navigate, params, location)
          strategy.navigateToActionRelative(definition, selection)

          expect(navigate).to.have.been.calledWith(
            {
              pathname: '/action/input-edit',
              search: 'actionProperties=%7B%22a%22%3A%22foo%22%7D&selection=%7B%22entityName%22%3A%22Input%22%7D'
            },
            {
              state: {definition, selection, originUrl: '/detail/123'}
            }
          )
        })
      })
    })
  })
})
