import {queryString as queryStringUtil} from 'tocco-util'

import {DetailLinkRelative} from '../components/EntityDetail/DetailLinkRelative'

/**
 * Routing strategy for entity-browser
 * ListView:
 *  - /
 * DetailView:
 *  - /detail (or /detail/) (create)
 *  - /detail/25
 *  - /detail/25/relReservation/ (create)
 *  - /detail/25/relReservation/533
 * ActionView:
 *  - /action/input-edit?actionProperties=...
 */
export default (navigate, params, location) => {
  const navigateToCreate = (relationName, state) => {
    if (params?.entityId) {
      if (relationName) {
        /**
         * create relation from detail
         *   - /detail/25 =>  /detail/25/relReservation
         */
        navigate(`${relationName}`, {state})
      } else {
        /**
         * create entity from detail
         *   - /detail/25  => /detail
         *   - /detail/25/relReservation/1 =>  /detail/25/relReservation
         *
         * navigate one level up
         *   - but the route setup from the entity browser
         *     does not have a component hierarchy for /detail and /detail/:entityId
         *   - therefore `'..'` or -1 will navigate to list directly
         *   - therefore use `relative: 'path'` to consider path for hierarchy
         */
        navigate('..', {relative: 'path', state})
      }
    } else {
      /**
       * create entity from list
       *   - /  => /detail
       */
      navigate('detail', {state})
    }
  }

  const navigateToAction = (definition, selection) => {
    const search = queryStringUtil.toQueryString({
      selection,
      actionProperties: definition.properties,
      ...(definition.queryParams || {})
    })
    navigate(
      {
        pathname: '/action/' + definition.appId,
        search
      },
      {
        state: {
          definition,
          selection,
          originUrl: location.pathname
        }
      }
    )
  }

  return {
    DetailLinkRelative,
    navigateToCreateRelative: navigateToCreate,
    navigateToActionRelative: navigateToAction
  }
}
