export const VisibilityStatus = {
  list: 'list',
  detail: 'detail',
  fullscreenAction: 'fullscreen-action'
}
