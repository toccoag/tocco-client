import _isEmpty from 'lodash/isEmpty'
import _isEqual from 'lodash/isEqual'
import _pickBy from 'lodash/pickBy'
import PropTypes from 'prop-types'
import React, {Suspense, useEffect} from 'react'
import {RouterProvider, createHashRouter, createMemoryRouter, useLocation, useNavigate} from 'react-router-dom'
import {
  actionEmitter,
  appFactory,
  actions,
  errorLogging,
  externalEvents,
  login,
  notification
} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext, env, queryString as queryStringUtil, react, reducer as reducerUtil, consoleLogger} from 'tocco-util'

import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'entity-browser'

const EXTERNAL_EVENTS = [
  'onVisibilityStatusChange',
  /**
   * fired whenever route change happens
   */
  'onRouteChange'
]

const LazyEntityBrowserComp = React.lazy(() => import('./components/EntityBrowser'))
const LazyEntityBrowser = () => (
  <Suspense fallback="">
    <LazyEntityBrowserComp />
  </Suspense>
)

const InitialNavigation = ({initialKey: initialDetailViewKey, initialAction}) => {
  const navigate = useNavigate()
  const location = useLocation()

  const pathname = location.pathname

  /**
   * run only on mount
   *  - when pathname is in deps-array it's not possible again to navigate away from detail page
   *  (e.g. for creating a new entity)
   */
  useEffect(() => {
    if (initialDetailViewKey && !isNaN(initialDetailViewKey)) {
      const regex = /\/detail\/[0-9]*/
      if (!pathname.match(regex)) {
        const path = `/detail/${initialDetailViewKey}`
        navigate(path)
      }
    }

    if (initialAction && initialAction.name) {
      const path = `/action/${initialAction.name}?${queryStringUtil.toQueryString(initialAction.properties)}`
      navigate(path)
    }
  }, []) // eslint-disable-line
}

const Content = ({memoryRouter, initialKey, initialAction}) => {
  const routerFactory = memoryRouter ? createMemoryRouter : createHashRouter

  const router = routerFactory([
    {
      path: '*',
      element: (
        <>
          <InitialNavigation initialKey={initialKey} initialAction={initialAction} />
          <LazyEntityBrowser />
        </>
      )
    }
  ])
  return <RouterProvider router={router} />
}
Content.propTypes = {
  memoryRouter: PropTypes.bool,
  initialKey: PropTypes.string,
  initialAction: PropTypes.string
}

const initApp = (id, input, events, publicPath) => {
  input = {...input, id}

  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  actionEmitter.addToStore(store)
  actions.dynamicActionsAddToStore(store)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  notification.addToStore(store, true)
  login.addToStore(store)

  const content = (
    <Content memoryRouter={input.memoryHistory} initialKey={input.initialKey} initialAction={input.initialAction} />
  )

  return appFactory.createApp(packageName, content, store, {
    input,
    actions: getDispatchActions(input),
    publicPath,
    textResourceModules: ['component', 'common', 'actions', 'entity-list', 'entity-detail']
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const EntityBrowserApp = props => {
  const {component, store} = appFactory.useApp({initApp, props, packageName})

  const prevProps = react.usePrevious(props)
  react.useDidUpdate(() => {
    const changedProps = _pickBy(props, (value, key) => !_isEqual(value, prevProps[key]))
    if (!_isEmpty(changedProps)) {
      getDispatchActions(changedProps, false).forEach(action => {
        store.dispatch(action)
      })
    }
  }, [props])

  // Fragments-Workaround to support propTypes in Storybook
  return <>{component}</>
}

EntityBrowserApp.propTypes = {
  /**
   * Entity name of records
   */
  entityName: PropTypes.string.isRequired,
  /**
   * Defines what type of search form is shown (default is 'simple_advanced')
   *
   * - none (no search form shown)
   * - fulltext (only one fulltext search field)
   * - simple (simple search only)
   * - simple_advanced (usual (simple) search form with advanced expansion)
   * - advanced (extended advanced search form only)
   */
  searchFormType: searchFormTypePropTypes, // except for `admin` (not allowed because of layouting issues)
  /**
   * Base name of form: `formBase`_list, `formBase`_search and `formBase`_detail will be used.
   */
  formBase: PropTypes.string,
  /**
   * Maximum records per page.
   */
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  /**
   * Array of the search fields with preselected values.
   */
  preselectedSearchFields: PropTypes.array,
  /**
   * Array of search filter ids.
   */
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  /**
   * Comma separated list of search fields which will be displayed by default.
   */
  simpleSearchFields: PropTypes.string,
  /**
   * If set, the detail view is shown direclty with the entity of the speficied key, instead of showing the list view.
   */
  initialKey: PropTypes.string,
  /**
   * If set, the specified fullscreen action is shown.
   * As `name` property the app id must be passed.
   * Optionally use the `properties` property to pass additional information (e.g. selection, actionProperties).
   */
  initialAction: PropTypes.shape({
    name: PropTypes.string,
    properties: PropTypes.objectOf(PropTypes.object)
  }),
  /**
   * The unique id of a business unit.
   * If present, all REST request will use this in their business unit header (X-Business-Unit).
   * Set input parameter to `__n-u-l-l__` if the null business unit should be used.
   */
  businessUnit: PropTypes.string,
  /**
   * Activate in-memory history routing instead of hash history routing.
   * This is useful in testing and non-DOM environments.
   */
  memoryHistory: PropTypes.bool,
  /**
   * Set backend url dynamically to point to nice2 installtion.
   * If not set it fallbacks to the build time environment `__BACKEND_URL__`.
   */
  backendUrl: PropTypes.string,
  /**
   * Defines if it is taking as much space as needed (`'none'`) or if it will fit into its outer container
   * (`'inline'`). When set to `'inline'` the outer container has to have e predefined height (Default: `inline`)
   *
   * - none: Does not handle scroll internally and will take as much space as needed.
   *   The container / page needs to handle the scroll
   * - inline: Does handle scroll internally and takes the space given by the container.
   *   Containers needs to have a predefined height
   */
  scrollBehaviour: PropTypes.oneOf(['none', 'inline']),
  appContext: appContext.propTypes,
  /**
   * Function to modify the form definitions before rendering the forms.
   */
  modifyFormDefinition: PropTypes.func,
  /**
   * Function to modify the entity paths. Useful when new pseudeo form fields has been added.
   */
  modifyEntityPaths: PropTypes.func,
  /**
   * Disable the navigation to the detail view. List view only.
   */
  disableDetailView: PropTypes.bool,
  /**
   * Array of report ids to display in forms.
   */
  reportIds: PropTypes.arrayOf(PropTypes.string),
  customActionEventHandlers: PropTypes.objectOf(PropTypes.objectOf(PropTypes.func)),
  /**
   * Able to hide the entity browser - mostly used for visiblity status pages
   */
  isHidden: PropTypes.bool,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default EntityBrowserApp
export const app = appFactory.createBundleableApp(packageName, initApp, EntityBrowserApp)
