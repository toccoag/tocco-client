import _get from 'lodash/get'
import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useNavigate, useLocation, useParams} from 'react-router-dom'
import {queryString as queryStringUtil} from 'tocco-util'

import navigationStrategy from '../../util/navigationStrategy'
import {VisibilityStatus} from '../../visibilityStatus'
import getActionComponent from '../ActionComponent'

const ActionView = ({fireVisibilityStatusChangeEvent, ...props}) => {
  const navigate = useNavigate()
  const location = useLocation()
  const params = useParams()

  const queryParams = queryStringUtil.fromQueryString(location.search)
  const selection = _get(location, 'state.selection', queryParams.selection)
  const actionProperties = _get(location, 'state.definition.properties', queryParams.actionProperties)

  useEffect(() => {
    fireVisibilityStatusChangeEvent([VisibilityStatus.fullscreenAction])
  }, [fireVisibilityStatusChangeEvent])

  const navigateBack = () => {
    const originUrl = location.state?.originUrl || '/'
    navigate(originUrl, {replace: true})
  }

  const ActionComponent = getActionComponent()

  return (
    <ActionComponent
      appId={params.appId}
      onSuccess={navigateBack}
      onError={navigateBack}
      onCancel={navigateBack}
      navigationStrategy={navigationStrategy(navigate, params, location)}
      {...props}
      {...queryParams}
      selection={selection}
      actionProperties={actionProperties}
    />
  )
}

ActionView.propTypes = {
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  customActionEventHandlers: PropTypes.objectOf(PropTypes.objectOf(PropTypes.func))
}

export default ActionView
