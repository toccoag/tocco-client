import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import ActionView from './ActionView'

const mapActionCreators = {
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  locale: state.intl.locale,
  customActionEventHandlers: state.input.customActionEventHandlers
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(ActionView))
