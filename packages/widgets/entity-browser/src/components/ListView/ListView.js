import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useNavigate, useParams, useLocation} from 'react-router-dom'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {RouterLink, scrollBehaviourPropType} from 'tocco-ui'
import {viewPersistor} from 'tocco-util'

import navigationStrategy from '../../util/navigationStrategy'
import {VisibilityStatus} from '../../visibilityStatus'
import getActionComponent from '../ActionComponent'

const DetailLinkRelative = ({entityKey, children}) => <RouterLink to={`detail/${entityKey}`}>{children}</RouterLink>

DetailLinkRelative.propTypes = {
  entityKey: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
}

const ListView = ({
  id,
  modifyFormDefinition,
  disableDetailView,
  fireVisibilityStatusChangeEvent,
  customActionEventHandlers,
  ...props
}) => {
  const navigate = useNavigate()
  const location = useLocation()
  const params = useParams()
  const storeId = `${id}_${location.pathname}`

  useEffect(() => {
    fireVisibilityStatusChangeEvent([VisibilityStatus.list])
  }, [fireVisibilityStatusChangeEvent])

  const handleRowClick = e => {
    if (!disableDetailView) {
      navigate(`detail/${e.id}`)
    }
  }

  return (
    <EntityListApp
      {...props}
      onRowClick={handleRowClick}
      showLink={!disableDetailView}
      navigationStrategy={{...navigationStrategy(navigate, params, location), DetailLinkRelative}}
      searchFormPosition="top"
      actionAppComponent={getActionComponent()}
      store={viewPersistor.viewInfoSelector(storeId).store}
      onStoreCreate={store => {
        viewPersistor.persistViewInfo(storeId, {store}, 0)
      }}
      modifyFormDefinition={modifyFormDefinition}
      customActionEventHandlers={customActionEventHandlers}
      detailApp={EntityDetailApp}
    />
  )
}

ListView.propTypes = {
  id: PropTypes.string.isRequired,
  locale: PropTypes.string.isRequired,
  entityName: PropTypes.string.isRequired,
  formName: PropTypes.string.isRequired,
  scrollBehaviour: scrollBehaviourPropType,
  searchFormType: PropTypes.string.isRequired,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  searchFitlers: PropTypes.array,
  preselectedSearchFields: PropTypes.array,
  simpleSearchFields: PropTypes.string,
  modifyFormDefinition: PropTypes.func,
  disableDetailView: PropTypes.bool,
  reportIds: PropTypes.arrayOf(PropTypes.string),
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  customActionEventHandlers: PropTypes.objectOf(PropTypes.objectOf(PropTypes.func))
}

export default ListView
