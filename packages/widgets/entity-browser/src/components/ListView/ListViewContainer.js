import {connect} from 'react-redux'
import {actionEmitter, externalEvents} from 'tocco-app-extensions'

import ListView from './ListView'

const mapActionCreators = {
  emitAction: action => actionEmitter.dispatchEmittedAction(action),
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = (state, props) => {
  const id = `${state.entityBrowser.appId}_entity-browser-list`
  return {
    id,
    locale: state.intl.locale,
    entityName: state.entityBrowser.entityName,
    formName: state.entityBrowser.formBase,
    scrollBehaviour: state.entityBrowser.scrollBehaviour,
    searchFormType: state.input.searchFormType || 'simple_advanced',
    limit: state.input.limit,
    searchFilters: state.input.searchFilters,
    preselectedSearchFields: state.input.preselectedSearchFields,
    simpleSearchFields: state.input.simpleSearchFields,
    modifyFormDefinition: state.input.modifyFormDefinition,
    disableDetailView: state.input.disableDetailView,
    reportIds: state.input.reportIds,
    customActionEventHandlers: state.input.customActionEventHandlers
  }
}

export default connect(mapStateToProps, mapActionCreators)(ListView)
