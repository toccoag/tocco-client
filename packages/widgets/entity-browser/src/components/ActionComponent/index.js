import {lazy} from 'react'
import {actions} from 'tocco-app-extensions'

const actionMap = {
  copy: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Copy')),
  delete: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Delete')),
  export: lazy(() => import(/* webpackChunkName: "actions" */ './actions/Export')),
  'input-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/InputEdit')),
  'subscribe-calendar': lazy(() => import(/* webpackChunkName: "actions" */ './actions/SubscribeCalendar')),
  'mailing-list-mail-action': lazy(() => import(/* webpackChunkName: "actions" */ './actions/MailingListMailAction')),
  'evaluation-execution-participation-action': lazy(
    () => import(/* webpackChunkName: "actions" */ './actions/EvaluationExecutionParticipationAction')
  ),
  'event-registration-action': lazy(
    () => import(/* webpackChunkName: "actions" */ './actions/EventRegistrationAction')
  ),
  'job-offer-apply': lazy(() => import(/* webpackChunkName: "actions" */ './actions/JobOfferApplyAction')),
  'exam-edit': lazy(() => import(/* webpackChunkName: "actions" */ './actions/ExamEdit')),
  'payment-provider-action': lazy(() => import(/* webpackChunkName: "actions" */ './actions/PaymentProviderAction'))
}

const getActionComponent = () => {
  return actions.actionFactory(actionMap)
}

export default getActionComponent
