import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {useLocation} from 'react-router-dom'

const Hide = ({isHidden, onRouteChange, children}) => {
  const location = useLocation()
  useEffect(() => {
    onRouteChange()
  }, [location, onRouteChange])

  if (isHidden) {
    return null
  }

  return <>{children}</>
}

Hide.propTypes = {
  isHidden: PropTypes.bool,
  onRouteChange: PropTypes.func,
  children: PropTypes.any
}

export default Hide
