import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {onRouteChange} from '../../modules/entityBrowser/actions'

import Hide from './Hide'

const mapActionCreators = {
  onRouteChange
}

const mapStateToProps = state => ({
  isHidden: state.input.isHidden
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Hide))
