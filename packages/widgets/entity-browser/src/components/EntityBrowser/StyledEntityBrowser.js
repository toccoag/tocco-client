import styled from 'styled-components'
import {theme} from 'tocco-ui'

const StyledEntityBrowser = styled.div`
  && {
    background-color: ${theme.color('paper')};
    display: flex;
    flex-direction: column;
    flex: 1;
    height: 100%;
  }
`

export default StyledEntityBrowser
