import {Route, Routes, ScrollRestoration} from 'react-router-dom'
import {notification} from 'tocco-app-extensions'

import ActionView from '../ActionView'
import EntityDetail from '../EntityDetail'
import ListView from '../ListView'

import Hide from './HideContainer'
import StyledEntityBrowser from './StyledEntityBrowser'

const EntityBrowser = () => (
  <StyledEntityBrowser>
    <notification.Notifications />
    <ScrollRestoration />
    <Hide>
      <Routes>
        <Route path="/" exact element={<ListView />} />
        <Route path="/detail/:entityId?/*" element={<EntityDetail />} />
        <Route path="/action/:appId/*" element={<ActionView />} />
      </Routes>
    </Hide>
  </StyledEntityBrowser>
)

EntityBrowser.propTypes = {}

export default EntityBrowser
