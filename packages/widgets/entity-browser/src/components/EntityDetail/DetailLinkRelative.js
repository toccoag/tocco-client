import PropTypes from 'prop-types'
import {RouterLink} from 'tocco-ui'

export const DetailLinkRelative = ({entityKey, children, relation}) => (
  <RouterLink to={`${relation}/${entityKey}`}>{children}</RouterLink>
)

DetailLinkRelative.propTypes = {
  entityKey: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  relation: PropTypes.string.isRequired
}
