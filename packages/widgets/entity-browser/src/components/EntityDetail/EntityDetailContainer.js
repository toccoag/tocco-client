import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {actionEmitter, externalEvents} from 'tocco-app-extensions'

import {
  loadDetailParams,
  clearDetailParams,
  setFormTouched,
  showPendingChangesModal
} from '../../modules/detail/actions'

import EntityDetail from './EntityDetail'

const mapActionCreators = {
  loadDetailParams,
  clearDetailParams,
  setFormTouched,
  dispatchEmittedAction: actionEmitter.dispatchEmittedAction,
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent,
  showPendingChangesModal
}

const mapStateToProps = (state, props) => {
  return {
    appId: state.entityBrowser.appId,
    detailParams: state.detail.detailParams,
    formTouched: state.detail.formTouched,
    scrollBehaviour: state.entityBrowser.scrollBehaviour,
    locale: state.intl.locale,
    modifyFormDefinition: state.input.modifyFormDefinition,
    modifyEntityPaths: state.input.modifyEntityPaths,
    reportIds: state.input.reportIds,
    customActionEventHandlers: state.input.customActionEventHandlers
  }
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EntityDetail))
