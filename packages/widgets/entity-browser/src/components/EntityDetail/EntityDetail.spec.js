import {screen} from '@testing-library/react'
import {RouterProvider, createMemoryRouter} from 'react-router-dom'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import {VisibilityStatus} from '../../visibilityStatus'

import EntityDetail from './EntityDetail'

const EMPTY_FUNC = () => {}

jest.mock('tocco-entity-detail/src/main', () => () => <div data-testid="entity-detail" />)

describe('entity-browser', () => {
  describe('components', () => {
    describe('EntityDetail', () => {
      const detailParams = {
        entityName: 'User',
        entityId: '1',
        formName: 'User',
        mode: 'update'
      }

      const actionsMock = {
        loadDetailParams: EMPTY_FUNC,
        dispatchEmittedAction: EMPTY_FUNC,
        clearDetailParams: EMPTY_FUNC,
        setFormTouched: EMPTY_FUNC,
        fireVisibilityStatusChangeEvent: EMPTY_FUNC
      }

      test('should render App', () => {
        const router = createMemoryRouter([
          {
            path: '*',
            element: <EntityDetail {...actionsMock} intl={IntlStub} detailParams={detailParams} />
          }
        ])
        testingLibrary.renderWithIntl(<RouterProvider router={router} />)

        expect(screen.queryByTestId('entity-detail')).to.exist
      })

      test('should fire state change event for detail', () => {
        const fireVisibilityStatusChangeEvent = sinon.spy()

        const router = createMemoryRouter([
          {
            path: '*',
            element: (
              <EntityDetail
                {...actionsMock}
                fireVisibilityStatusChangeEvent={fireVisibilityStatusChangeEvent}
                intl={IntlStub}
                detailParams={detailParams}
              />
            )
          }
        ])
        testingLibrary.renderWithIntl(<RouterProvider router={router} />)

        expect(fireVisibilityStatusChangeEvent).to.have.been.calledWith([VisibilityStatus.detail])
      })
    })
  })
})
