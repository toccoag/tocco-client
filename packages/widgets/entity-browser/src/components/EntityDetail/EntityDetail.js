import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {flushSync} from 'react-dom'
import {useLocation, useNavigate, useParams} from 'react-router-dom'
import DocsBrowserApp from 'tocco-docs-browser/src/main'
import EntityDetailApp from 'tocco-entity-detail/src/main'
import EntityListApp from 'tocco-entity-list/src/main'
import {BackButton, scrollBehaviourPropType, usePrompt} from 'tocco-ui'

import navigationStrategy from '../../util/navigationStrategy'
import {VisibilityStatus} from '../../visibilityStatus'
import getActionComponent from '../ActionComponent'

import {modifyFormDefinition} from './formModifier'

const EntityDetail = props => {
  const {
    loadDetailParams,
    setFormTouched,
    clearDetailParams,
    detailParams,
    appId,
    modifyEntityPaths,
    intl,
    dispatchEmittedAction,
    formTouched,
    locale,
    reportIds,
    fireVisibilityStatusChangeEvent,
    scrollBehaviour,
    customActionEventHandlers,
    showPendingChangesModal
  } = props

  const msg = id => intl.formatMessage({id})
  const location = useLocation()
  const navigate = useNavigate()
  const params = useParams()

  const stateDefaultValues = location?.state?.defaultValues || []

  useEffect(() => {
    loadDetailParams(location.pathname)
    setFormTouched(false)

    return () => {
      clearDetailParams()
    }
  }, [clearDetailParams, loadDetailParams, location, setFormTouched])

  useEffect(() => {
    fireVisibilityStatusChangeEvent([VisibilityStatus.detail])
  }, [fireVisibilityStatusChangeEvent])

  usePrompt({
    when: formTouched,
    message: msg('client.entity-browser.detail.confirmTouchedFormLeave'),
    showPendingChangesModal
  })

  const handleSubGridRowClick = ({id, gridName}) => {
    navigate(`${gridName}/${id}`)
  }

  const handleEntityCreated = ({id, followUp}) => {
    /**
     * `formTouched` on the Prompt component has to be already set to false before the
     * navigate happens.
     * Otherwise the prompt will show up evertime after the user has created the new entity.
     */
    flushSync(() => {
      setFormTouched(false)
    })
    const previousPath = params['*']
    const newPath = previousPath ? `${previousPath}/${id}` : id
    navigate(newPath, {state: followUp?.action ? {action: followUp.action} : {}})
  }

  const handleTouchedChange = ({touched}) => {
    setFormTouched(touched)
  }

  const handleGoBack = () => {
    navigate(detailParams.parentUrl)
  }

  const customRenderedActions = {
    back: () => <BackButton onClick={handleGoBack} label={msg('client.entity-browser.back')} />
  }

  const initialAction = location?.state?.action

  const getApp = ({entityName, entityId, formName, mode}) => (
    <EntityDetailApp
      id={`${appId}_detail_${formName}_${entityId}`}
      entityName={entityName}
      entityId={entityId}
      formName={formName}
      mode={mode}
      navigationStrategy={navigationStrategy(navigate, params, location)}
      onSubGridRowClick={handleSubGridRowClick}
      onEntityCreated={handleEntityCreated}
      onEntityDeleted={handleGoBack}
      onTouchedChange={handleTouchedChange}
      emitAction={action => {
        dispatchEmittedAction(action)
      }}
      theme={{}}
      locale={locale}
      modifyFormDefinition={(formDefinition, context) => modifyFormDefinition(formDefinition, context, props)}
      modifyEntityPaths={modifyEntityPaths}
      actionAppComponent={getActionComponent()}
      reportIds={reportIds}
      customRenderedActions={customRenderedActions}
      listApp={EntityListApp}
      docsApp={DocsBrowserApp}
      scrollBehaviour={scrollBehaviour}
      customActionEventHandlers={customActionEventHandlers}
      initialAction={initialAction}
      defaultValues={stateDefaultValues}
    />
  )

  return <>{detailParams && <>{getApp(detailParams)}</>}</>
}

EntityDetail.propTypes = {
  intl: PropTypes.object.isRequired,
  locale: PropTypes.string.isRequired,
  dispatchEmittedAction: PropTypes.func.isRequired,
  loadDetailParams: PropTypes.func.isRequired,
  clearDetailParams: PropTypes.func.isRequired,
  setFormTouched: PropTypes.func.isRequired,
  detailParams: PropTypes.object,
  formTouched: PropTypes.bool,
  showSubGridsCreateButton: PropTypes.bool,
  appId: PropTypes.string,
  modifyFormDefinition: PropTypes.func,
  modifyEntityPaths: PropTypes.func,
  reportIds: PropTypes.arrayOf(PropTypes.string),
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  scrollBehaviour: scrollBehaviourPropType,
  customActionEventHandlers: PropTypes.objectOf(PropTypes.objectOf(PropTypes.func)),
  showPendingChangesModal: PropTypes.func.isRequired
}

export default EntityDetail
