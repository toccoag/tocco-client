import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, env, consoleLogger} from 'tocco-util'

import EntityDms from './components/EntityDms'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'entity-dms'

const initApp = (id, input, events, publicPath) => {
  const content = <EntityDms />

  env.setInputEnvs(input)

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(),
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const EntityDmsApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

EntityDmsApp.propTypes = {
  /**
   * Amount of records per page in list
   */
  listLimit: PropTypes.number,
  businessUnit: PropTypes.string,
  backendUrl: PropTypes.string,
  /**
   * Optional form to use on uppermost level
   */
  formBase: PropTypes.string,
  /**
   * Entity model to display data from
   */
  entityModel: PropTypes.string,
  appContext: appContext.propTypes.isRequired
}

export default EntityDmsApp
export const app = appFactory.createBundleableApp(packageName, initApp, EntityDmsApp)
