import {v4 as uuid} from 'uuid'

import AddressUpdateApp from './main'

export default {
  title: 'Widgets/Address Update',
  component: AddressUpdateApp,
  argTypes: {
    formName: {
      table: {
        disable: true
      }
    },
    backendUrl: {
      table: {
        disable: true
      }
    },
    appContext: {
      table: {
        disable: true
      }
    }
  }
}

const AddressUpdateStory = ({...args}, {globals: {embedType}}) => (
  <AddressUpdateApp key={uuid()} {...args} appContext={{embedType, widgetConfigKey: args.widgetConfigKey}} />
)

export const Story = AddressUpdateStory.bind({})
Story.args = {
  formName: 'Address_update'
}
