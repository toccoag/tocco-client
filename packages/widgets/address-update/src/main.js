import PropTypes from 'prop-types'
import {
  actionEmitter,
  actions,
  appFactory,
  errorLogging,
  externalEvents,
  notification,
  formData,
  form
} from 'tocco-app-extensions'
import EntityListApp from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, env, appContext, consoleLogger} from 'tocco-util'

import AddressUpdate from './components/AddressUpdate'
import reducers, {sagas, formSagaConfig} from './modules/reducers'

const packageName = 'address-update'

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <AddressUpdate />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  externalEvents.addToStore(store, () => ({}))
  actionEmitter.addToStore(store)
  notification.addToStore(store, true)
  errorLogging.addToStore(store, true, ['console', 'remote', 'notification'])
  actions.addToStore(store, () => ({
    appComponent: actions.actionFactory({})
  }))
  form.addToStore(store, formSagaConfig)
  formData.addToStore(store, () => ({
    listApp: EntityListApp
  }))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'docs-browser', 'entity-detail', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const AddressUpdateApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  // Fragments-Workaround to support propTypes in Storybook
  return <>{component}</>
}

AddressUpdateApp.propTypes = {
  formName: PropTypes.string.isRequired,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  backendUrl: PropTypes.string
}

export default AddressUpdateApp
export const app = appFactory.createBundleableApp(packageName, initApp, AddressUpdateApp)
