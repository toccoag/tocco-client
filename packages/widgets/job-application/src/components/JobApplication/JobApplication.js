import PropTypes from 'prop-types'
import {useState, useMemo} from 'react'
import {form} from 'tocco-app-extensions'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {VisibilityStatus} from '../../visibilityStatus'

export function modifyFormDefinition(formDefinition, actionFormBase) {
  return form.adjustAction(formDefinition, 'job-offer-apply', action => ({
    ...action,
    properties: {
      ...action.properties,
      formBase: actionFormBase
    }
  }))
}

const JobApplication = ({
  formBase,
  actionFormBase,
  searchFormType,
  reportIds,
  searchFilters,
  limit,
  backendUrl,
  businessUnit,
  appContext,
  fireVisibilityStatusChangeEvent,
  onVisibilityStatusChange,
  locale
}) => {
  const [hideEntityBrowser, setHideEntityBrowser] = useState(false)

  const customActionEventHandlers = useMemo(
    () => ({
      'job-offer-apply': {
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        },
        onApplicationForm: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.applicationForm])
        }
      }
    }),
    [fireVisibilityStatusChangeEvent]
  )

  return (
    <EntityBrowserApp
      entityName="Job_offer"
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      reportIds={reportIds}
      searchFormType={searchFormType}
      locale={locale}
      modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, actionFormBase)}
      customActionEventHandlers={customActionEventHandlers}
      onVisibilityStatusChange={onVisibilityStatusChange}
      isHidden={hideEntityBrowser}
      onRouteChange={() => setHideEntityBrowser(false)}
    />
  )
}

JobApplication.propTypes = {
  formBase: PropTypes.string.isRequired,
  actionFormBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  onVisibilityStatusChange: PropTypes.func,
  locale: PropTypes.string
}

export default JobApplication
