export const VisibilityStatus = {
  list: 'list',
  detail: 'detail',
  applicationForm: 'application_form',
  success: 'success'
}
