import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {env, appContext, consoleLogger} from 'tocco-util'

import UserGrades from './components/UserGrades'

const packageName = 'user-grades'

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <UserGrades />

  const store = appFactory.createStore({}, null, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {component} = initApp(packageName, input)

      appFactory.renderApp(component)
    }
  }
})()

const UserGradesApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

UserGradesApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  reportIds: PropTypes.arrayOf(PropTypes.string),
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  searchFormType: searchFormTypePropTypes
}

export default UserGradesApp
export const app = appFactory.createBundleableApp(packageName, initApp, UserGradesApp)
