import {v4 as uuid} from 'uuid'

import WageViewApp from './main'

export default {
  title: 'Widgets/Wage View',
  component: WageViewApp,
  argTypes: {
    formBase: {
      table: {
        disable: true
      }
    },
    backendUrl: {
      table: {
        disable: true
      }
    },
    appContext: {
      table: {
        disable: true
      }
    },
    searchFormType: {
      options: ['none', 'simple_advanced', 'advanced'],
      control: {type: 'select'}
    },
    widgetConfigKey: {control: {type: 'text'}}
  }
}

const WageViewStory = ({...args}, {globals: {embedType}}) => (
  <WageViewApp key={uuid()} {...args} appContext={{embedType, widgetConfigKey: args.widgetConfigKey}} />
)

export const Story = WageViewStory.bind({})
Story.args = {
  formBase: 'Wage_view',
  searchFormType: 'simple_advanced',
  limit: 15,
  reportIds: []
}
