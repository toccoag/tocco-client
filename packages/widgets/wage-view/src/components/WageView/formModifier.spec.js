import _get from 'lodash/get'
import {IntlStub} from 'tocco-test-util'

import {modifyFormDefinition} from './formModifier'

describe('wage-view', () => {
  describe('components', () => {
    describe('WageView', () => {
      describe('formModifier', () => {
        const intl = IntlStub
        const formDefinitionWithActionBar = {
          id: 'Wave_view_list',
          label: 'Honorar- und Spesenabrechnung',
          componentType: 'form',
          children: [
            {
              id: 'main-action-bar',
              componentType: 'action-bar',
              children: [
                {
                  id: 'createcopy',
                  label: 'Neu',
                  componentType: 'action-group',
                  children: [
                    {
                      id: 'new',
                      label: 'Neu',
                      componentType: 'action',
                      children: [],
                      actionType: 'custom'
                    },
                    {
                      id: 'copy',
                      label: 'Kopieren',
                      componentType: 'action',
                      children: [],
                      actionType: 'custom'
                    }
                  ],
                  actions: [],
                  defaultAction: {
                    id: 'new',
                    label: 'Neu',
                    componentType: 'action',
                    children: [],
                    actionType: 'custom'
                  }
                }
              ]
            }
          ]
        }

        test('should add create action button', () => {
          const formDefinition = {
            id: 'Wave_view_list',
            label: 'Honorar- und Spesenabrechnung',
            componentType: 'form',
            children: [
              {
                id: 'main-action-bar',
                componentType: 'action-bar',
                children: []
              }
            ]
          }

          const allowCreate = true

          const modifiedFormDefinition = modifyFormDefinition(formDefinition, allowCreate, intl)
          const newAction = _get(modifiedFormDefinition, ['children', '0', 'children', '0'])
          expect(newAction.id).to.eq('new')
        })

        test('should not add create action button twice', () => {
          const allowCreate = true

          const modifiedFormDefinition = modifyFormDefinition(formDefinitionWithActionBar, allowCreate, intl)
          expect(modifiedFormDefinition).to.deep.equal(formDefinitionWithActionBar)
        })

        test('should remove create and copy button', () => {
          const allowCreate = false

          const modifiedFormDefinition = modifyFormDefinition(formDefinitionWithActionBar, allowCreate, intl)
          expect(modifiedFormDefinition.children.length).to.eql(0)
        })
      })
    })
  })
})
