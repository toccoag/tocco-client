import {form} from 'tocco-app-extensions'

export const modifyFormDefinition = (formDefinition, allowCreate, intl) => {
  if (allowCreate) {
    formDefinition = form.addCreate(formDefinition, intl)
  } else {
    formDefinition = form.removeActions(formDefinition, ['new', 'copy'])
  }

  return formDefinition
}
