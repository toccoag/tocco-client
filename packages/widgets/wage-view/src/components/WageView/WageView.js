import PropTypes from 'prop-types'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {modifyFormDefinition} from './formModifier'

const WageView = ({
  formBase,
  allowCreate,
  reportIds,
  searchFilters,
  searchFormType,
  limit,
  backendUrl,
  businessUnit,
  appContext,
  onVisibilityStatusChange,
  intl,
  locale
}) => (
  <EntityBrowserApp
    entityName="Wage"
    formBase={formBase}
    searchFormType={searchFormType}
    searchFilters={searchFilters}
    limit={limit}
    modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, allowCreate, intl)}
    backendUrl={backendUrl}
    businessUnit={businessUnit}
    appContext={appContext}
    reportIds={reportIds}
    onVisibilityStatusChange={onVisibilityStatusChange}
    locale={locale}
  />
)

WageView.propTypes = {
  formBase: PropTypes.string.isRequired,
  allowCreate: PropTypes.bool,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFormType: searchFormTypePropTypes,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  onVisibilityStatusChange: PropTypes.func,
  locale: PropTypes.string
}

export default WageView
