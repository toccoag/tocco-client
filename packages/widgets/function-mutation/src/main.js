import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {reducer as reducerUtil, appContext as appContextPropType, env, consoleLogger} from 'tocco-util'

import FunctionMutation from './components/FunctionMutation'
import reducers, {sagas} from './modules/reducers'

const packageName = 'function-mutation'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)
  const content = <FunctionMutation />

  const store = appFactory.createStore(reducers, sagas, input, packageName)
  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const FunctionMutationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

FunctionMutationApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  allowCreate: PropTypes.bool,
  allowDelete: PropTypes.bool,
  showActiveOnly: PropTypes.bool,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default FunctionMutationApp
export const app = appFactory.createBundleableApp(packageName, initApp, FunctionMutationApp)
