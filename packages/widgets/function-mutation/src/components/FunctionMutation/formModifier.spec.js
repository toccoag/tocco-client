import _get from 'lodash/get'
import {IntlStub} from 'tocco-test-util'

import {getFormModifier} from './formModifier'

describe('function-mutation', () => {
  describe('components', () => {
    describe('FunctionMutation', () => {
      describe('formModifier', () => {
        const formBase = 'Function_mutation'
        const intl = IntlStub

        const createCopyGroup = {
          id: 'createcopy',
          componentType: 'action-group',
          children: [
            {
              id: 'new',
              componentType: 'action'
            },
            {
              id: 'copy',
              componentType: 'action'
            }
          ]
        }
        const deleteAction = {
          id: 'delete',
          componentType: 'action'
        }

        const initialFormDefinition = {
          id: 'Function_mutation_detail',
          componentType: 'form',
          children: [
            {
              id: 'main-action-bar',
              componentType: 'action-bar',
              children: [createCopyGroup, deleteAction]
            }
          ]
        }

        const initialListFormDefinition = {
          id: 'Function_mutation_list',
          componentType: 'form',
          children: [
            {
              id: 'table',
              componentType: 'table',
              children: [],
              constriction: 'constriction'
            }
          ]
        }

        test('should add create when enabled', () => {
          const formModifier = getFormModifier(true, true, false, formBase, intl)
          const result = formModifier(initialListFormDefinition)
          expect(_get(result, ['children', 0, 'children', 0, 'id'])).to.eq('new')
        })

        test('should remove createcopy', () => {
          const formModifier = getFormModifier(false, true, false, formBase, intl)
          const result = formModifier(initialFormDefinition)
          expect(_get(result, ['children', 0, 'children'])).to.have.members([deleteAction])
        })

        test('should remove delete when disabled', () => {
          const formModifier = getFormModifier(false, false, false, formBase, intl)
          const result = formModifier(initialFormDefinition)
          expect(_get(result, ['children'])).to.be.empty
        })

        test('should adjust constriction when only active shown', () => {
          const formModifier = getFormModifier(false, false, true, formBase, intl)
          const result = formModifier(initialListFormDefinition)
          expect(_get(result, ['children', 0, 'constriction'])).to.eq('constriction_active')
        })

        test('should not adjust constriction when all shown', () => {
          const formModifier = getFormModifier(false, false, false, formBase, intl)
          const result = formModifier(initialListFormDefinition)
          expect(_get(result, ['children', 0, 'constriction'])).to.eq('constriction')
        })

        test('should add endpoints', () => {
          const formModifier = getFormModifier(false, false, false, formBase, intl)
          const result = formModifier(initialFormDefinition)
          expect(result.createEndpoint).to.eq('widgets/functionMutation')
          expect(result.updateEndpoint).to.eq('widgets/functionMutation')
        })
      })
    })
  })
})
