import {form} from 'tocco-app-extensions'

const removeActions = allowDelete => formDefinition => {
  const actionsToRemove = ['new', 'copy']
  if (!allowDelete) {
    actionsToRemove.push('delete')
  }
  return form.removeActions(formDefinition, actionsToRemove)
}

const addCreate = (allowCreate, intl) => formDefinition => {
  if (allowCreate) {
    return form.addCreate(formDefinition, intl)
  } else {
    return formDefinition
  }
}

const adjustConstriction = (showActiveOnly, formBase) => formDefinition => {
  if (showActiveOnly && formDefinition.id === `${formBase}_list`) {
    return {
      ...formDefinition,
      children: formDefinition.children.map(child => {
        if (child.componentType === 'table') {
          return {
            ...child,
            constriction: `${child.constriction}_active`
          }
        } else {
          return child
        }
      })
    }
  } else {
    return formDefinition
  }
}

const setEndpoints = formDefinition => {
  return {
    ...formDefinition,
    createEndpoint: `widgets/functionMutation`,
    updateEndpoint: `widgets/functionMutation`
  }
}

export const getFormModifier = (allowCreate, allowDelete, showActiveOnly, formBase, intl) => {
  return formDefinition => {
    return [
      removeActions(allowDelete),
      addCreate(allowCreate, intl),
      adjustConstriction(showActiveOnly, formBase),
      setEndpoints
    ].reduce((definition, modifier) => modifier(definition), formDefinition)
  }
}
