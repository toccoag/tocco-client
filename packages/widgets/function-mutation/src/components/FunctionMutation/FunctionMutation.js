import PropTypes from 'prop-types'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {getFormModifier} from './formModifier'

const FunctionMutation = ({
  formBase,
  searchFormType,
  reportIds,
  searchFilters,
  limit,
  backendUrl,
  businessUnit,
  appContext,
  onVisibilityStatusChange,
  allowCreate,
  allowDelete,
  showActiveOnly,
  intl,
  locale
}) => {
  return (
    <EntityBrowserApp
      entityName="Function"
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      reportIds={reportIds}
      searchFormType={searchFormType}
      onVisibilityStatusChange={onVisibilityStatusChange}
      modifyFormDefinition={getFormModifier(allowCreate, allowDelete, showActiveOnly, formBase, intl)}
      locale={locale}
    />
  )
}

FunctionMutation.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  onVisibilityStatusChange: PropTypes.func,
  allowCreate: PropTypes.bool,
  allowDelete: PropTypes.bool,
  showActiveOnly: PropTypes.bool,
  locale: PropTypes.string
}

export default FunctionMutation
