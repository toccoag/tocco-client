import {v4 as uuid} from 'uuid'

import MailingListApp from './main'

export default {
  title: 'Widgets/Mailing List',
  component: MailingListApp,
  argTypes: {
    backendUrl: {
      table: {
        disable: true
      }
    },
    appContext: {
      table: {
        disable: true
      }
    },
    searchFormType: {
      options: ['none', 'simple_advanced', 'advanced'],
      control: {type: 'select'}
    },
    widgetConfigKey: {control: {type: 'text'}}
  }
}

const EventRegistrationStory = ({...args}, {globals: {embedType}}) => (
  <MailingListApp key={uuid()} {...args} appContext={{embedType, widgetConfigKey: args.widgetConfigKey}} />
)

export const Story = EventRegistrationStory.bind({})
Story.args = {
  formBase: 'Mailing_list',
  entityName: 'Event',
  additionalRecipients: '',
  searchFormType: 'simple_advanced',
  reportIds: ['participant_list'],
  ccToResponsible: false,
  allowEmail: true,
  limit: 15,
  ccToSender: false,
  searchFilters: ['all_open_events']
}
