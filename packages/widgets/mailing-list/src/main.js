import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {env, appContext as appContextPropType, consoleLogger} from 'tocco-util'

import MailingList from './components/MailingList'

const packageName = 'mailing-list'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <MailingList />

  const store = appFactory.createStore({}, null, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {component} = initApp(packageName, input)

      appFactory.renderApp(component)
    }
  }
})()

const MailingListApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

MailingListApp.propTypes = {
  entityName: PropTypes.string.isRequired,
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  reportIds: PropTypes.arrayOf(PropTypes.string),
  /**
   * Mail action gets hidden when false
   */
  allowEmail: PropTypes.bool,
  /**
   * Mail template which gets preloaded when mail action is invoked
   */
  emailTemplate: PropTypes.string,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default MailingListApp
export const app = appFactory.createBundleableApp(packageName, initApp, MailingListApp)
