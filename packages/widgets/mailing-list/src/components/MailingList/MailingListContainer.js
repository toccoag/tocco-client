import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import MailingList from './MailingList'

const mapActionCreators = {}

const mapStateToProps = state => ({
  entityName: state.input.entityName,
  formBase: state.input.formBase,
  searchFormType: state.input.searchFormType,
  searchFilters: state.input.searchFilters,
  limit: state.input.limit,
  backendUrl: state.input.backendUrl,
  businessUnit: state.input.businessUnit,
  appContext: state.input.appContext,
  reportIds: state.input.reportIds,
  showEmailAction: state.input.allowEmail,
  emailTemplate: state.input.emailTemplate,
  onVisibilityStatusChange: state.input.onVisibilityStatusChange,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(MailingList))
