import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/util/searchFormTypes'
import {appContext, consoleLogger, env} from 'tocco-util'

import EvaluationExecutionParticipation from './components/EvaluationExecutionParticipation'

const packageName = 'evaluation-execution-participation'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const store = appFactory.createStore({}, null, input, packageName)
  const content = <EvaluationExecutionParticipation />

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {component} = initApp(packageName, input)

      appFactory.renderApp(component)
    }
  }
})()

const EvaluationExecutionParticipationApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

EvaluationExecutionParticipationApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  searchFormType: searchFormTypePropTypes,
  disableDetailView: PropTypes.bool,
  reportDisplayId: PropTypes.string,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default EvaluationExecutionParticipationApp
export const app = appFactory.createBundleableApp(packageName, initApp, EvaluationExecutionParticipationApp)
