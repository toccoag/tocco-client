import PropTypes from 'prop-types'
import {useMemo, useState} from 'react'
import {form} from 'tocco-app-extensions'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/util/searchFormTypes'
import {appContext as appContextPropType, queryString} from 'tocco-util'

import {VisibilityStatus} from '../../visibilityStatus'

export function modifyFormDefinition(formDefinition, reportDisplayId) {
  return form.adjustAction(formDefinition, 'evaluation-execution-participation-action', action => ({
    ...action,
    properties: {
      ...action.properties,
      reportDisplayId
    }
  }))
}

const EvaluationExecutionParticipation = ({
  formBase,
  searchFilters,
  searchFormType,
  limit,
  disableDetailView,
  reportDisplayId,
  backendUrl,
  businessUnit,
  appContext,
  fireVisibilityStatusChangeEvent,
  locale
}) => {
  const queryParams = queryString.fromQueryString(window.location.search)
  let initialAction = null
  if (queryParams?.uuid) {
    initialAction = {
      name: 'evaluation-execution-participation-action',
      properties: {
        uuid: queryParams.uuid
      }
    }
  }

  const [hideEntityBrowser, setHideEntityBrowser] = useState(false)

  const customActionEventHandlers = useMemo(
    () => ({
      'evaluation-execution-participation-action': {
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        }
      }
    }),
    [fireVisibilityStatusChangeEvent]
  )

  return (
    <EntityBrowserApp
      entityName="Evaluation_party_assignment"
      formBase={formBase}
      searchFilters={searchFilters}
      searchFormType={searchFormType}
      limit={limit}
      disableDetailView={disableDetailView}
      modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, reportDisplayId)}
      initialAction={initialAction}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      locale={locale}
      customActionEventHandlers={customActionEventHandlers}
      isHidden={hideEntityBrowser}
    />
  )
}

EvaluationExecutionParticipation.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  searchFormType: searchFormTypePropTypes,
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  disableDetailView: PropTypes.bool,
  reportDisplayId: PropTypes.String,
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default EvaluationExecutionParticipation
