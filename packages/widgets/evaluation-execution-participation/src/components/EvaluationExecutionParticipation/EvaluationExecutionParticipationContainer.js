import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {externalEvents} from 'tocco-app-extensions'

import EvaluationExecutionParticipation from './EvaluationExecutionParticipation'

const mapActionCreators = {
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  formBase: state.input.formBase,
  searchFilters: state.input.searchFilters,
  searchFormType: state.input.searchFormType,
  limit: state.input.limit,
  disableDetailView: state.input.disableDetailView,
  reportDisplayId: state.input.reportDisplayId,
  backendUrl: state.input.backendUrl,
  businessUnit: state.input.businessUnit,
  appContext: state.input.appContext,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(EvaluationExecutionParticipation))
