import freemarkerCodeReducer, {sagas as freemarkerCodeSagas} from './freemarkerCode'

export default {
  freemarkerCode: freemarkerCodeReducer
}

export const sagas = [freemarkerCodeSagas]
