import {all, call, put, takeLatest} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'
import {consoleLogger} from 'tocco-util'

import * as actions from './actions'

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_FREEMARKER, loadFreemarker)])
}

export function* loadFreemarker() {
  const response = yield call(rest.requestSaga, 'widgets/freemarkerCode', {
    method: 'GET',
    acceptedStatusCodes: [500]
  })

  if (response.ok) {
    const {
      body: {value}
    } = response
    yield put(actions.setFreemarker(value))
  } else {
    consoleLogger.logError(`Could not set default link attributes.`)
  }
}
