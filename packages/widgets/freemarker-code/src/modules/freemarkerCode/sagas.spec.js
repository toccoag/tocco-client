import {all, call, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import {rest} from 'tocco-app-extensions'
import {env} from 'tocco-util'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('freemarker-code', () => {
  describe('modules', () => {
    describe('freemarkerCode', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([takeLatest(actions.LOAD_FREEMARKER, sagas.loadFreemarker)])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadFreemarker saga', () => {
          test('loadFreemarker', () => {
            env.setWidgetConfigKey('widget-key')

            return expectSaga(sagas.loadFreemarker)
              .provide([
                [
                  call(rest.requestSaga, 'widgets/freemarkerCode', {
                    method: 'GET',
                    acceptedStatusCodes: [500]
                  }),
                  {body: {value: 'evaluated freemarker value'}, ok: true}
                ]
              ])
              .put(actions.setFreemarker('evaluated freemarker value'))
              .run()
          })
        })
      })
    })
  })
})
