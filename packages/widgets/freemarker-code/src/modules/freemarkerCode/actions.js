export const LOAD_FREEMARKER = 'freemarkerCode/LOAD_FREEMARKER'
export const SET_FREEMARKER = 'freemarkerCode/SET_FREEMARKER'

export const loadFreemarker = () => ({
  type: LOAD_FREEMARKER
})

export const setFreemarker = freemarker => ({
  type: SET_FREEMARKER,
  payload: {
    freemarker
  }
})
