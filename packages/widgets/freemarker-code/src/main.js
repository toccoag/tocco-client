import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, consoleLogger, env} from 'tocco-util'

import FreemarkerCode from './components/FreemarkerCode'
import reducers, {sagas} from './modules/reducers'

const packageName = 'freemarker-code'

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <FreemarkerCode />

  const store = appFactory.createStore(reducers, sagas, input, packageName)

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const FreemarkerCodeApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

FreemarkerCodeApp.propTypes = {
  appContext: appContext.propTypes.isRequired
}

export default FreemarkerCodeApp
export const app = appFactory.createBundleableApp(packageName, initApp, FreemarkerCodeApp)
