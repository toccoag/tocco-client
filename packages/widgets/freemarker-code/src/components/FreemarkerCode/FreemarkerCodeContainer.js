import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {loadFreemarker} from '../../modules/freemarkerCode/actions'

import FreemarkerCode from './FreemarkerCode'

const mapActionCreators = {
  loadFreemarker
}

const mapStateToProps = state => ({
  loadingSpinner: state.input.loadingSpinner,
  freemarker: state.freemarkerCode.freemarker
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(FreemarkerCode))
