import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {LoadMask} from 'tocco-ui'

const EventRegistration = ({loadingSpinner, loadFreemarker, freemarker}) => {
  useEffect(() => {
    loadFreemarker()
  }, [loadFreemarker])

  if (loadingSpinner) {
    return (
      <LoadMask required={[freemarker]}>
        <div dangerouslySetInnerHTML={{__html: freemarker}} />
      </LoadMask>
    )
  } else if (freemarker) {
    return <div dangerouslySetInnerHTML={{__html: freemarker}} />
  }
}

EventRegistration.propTypes = {
  loadFreemarker: PropTypes.func.isRequired,
  loadingSpinner: PropTypes.bool.isRequired,
  freemarker: PropTypes.string
}

export default EventRegistration
