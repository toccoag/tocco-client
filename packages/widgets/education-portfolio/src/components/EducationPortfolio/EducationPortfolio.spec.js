import _get from 'lodash/get'
import {rest} from 'tocco-app-extensions'

import {detailForm, entitydocsListForm} from '../../dev/testForm'

import {modifyFormDefinition} from './EducationPortfolio'

describe('widgets', () => {
  describe('education-portfolio', () => {
    describe('EducationPortfolio', () => {
      describe('modifyRequirementDetailForm', () => {
        function* getModifiedFormDefinition(formDefinition) {
          return yield modifyFormDefinition(formDefinition, {entityId: '1'}, {formBase: 'Education_portfolio'})
        }

        const buildRequirementResponse = enableUpload => ({
          'relRequirement_rule.enable_upload': enableUpload
        })

        const getBoxIds = formDefinition =>
          _get(formDefinition, ['children', '0', 'children', '0', 'children']).map(child => child.id)

        test('should remove sub-table', function* () {
          jest.spyOn(rest, 'fetchEntity').mockImplementationOnce(() => buildRequirementResponse(false))
          const formDefinition = yield getModifiedFormDefinition(detailForm)
          expect(getBoxIds(formDefinition)).to.have.members(['requirement'])
        })

        test('should not modify form', function* () {
          jest.spyOn(rest, 'fetchEntity').mockImplementationOnce(() => buildRequirementResponse(true))
          const formDefinition = yield getModifiedFormDefinition(detailForm)
          expect(getBoxIds(formDefinition)).to.have.members(['requirement', 'documents'])
        })
      })

      describe('modifyEntitydocsListForm', () => {
        function* getModifiedFormDefinition(formDefinition) {
          return yield modifyFormDefinition(formDefinition, {parent: {key: '1'}}, {formBase: 'Education_portfolio'})
        }

        const buildRequirementResponse = requirementStatus => ({
          'relRequirement_status.unique_id': requirementStatus
        })

        const getActionIds = formDefinition =>
          formDefinition.children
            .filter(c => c.id === 'main-action-bar')
            .flatMap(c => c.children)
            .map(c => c.id)

        test('should not remove upload action for requirement status incomplete', function* () {
          jest.spyOn(rest, 'fetchEntity').mockImplementationOnce(() => buildRequirementResponse('incomplete'))
          const formDefinition = yield getModifiedFormDefinition(entitydocsListForm)
          expect(getActionIds(formDefinition)).to.have.members(['upload_action'])
        })

        test('should remove upload action for requirement status otherStatus', function* () {
          jest.spyOn(rest, 'fetchEntity').mockImplementationOnce(() => buildRequirementResponse('otherStatus'))
          const formDefinition = yield getModifiedFormDefinition(entitydocsListForm)
          expect(getActionIds(formDefinition)).to.have.members([])
        })
      })
    })
  })
})
