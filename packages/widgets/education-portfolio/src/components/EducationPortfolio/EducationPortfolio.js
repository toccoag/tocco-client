import PropTypes from 'prop-types'
import {form, rest} from 'tocco-app-extensions'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType, api} from 'tocco-util'

const uploadActionAllowedCategories = ['incomplete', 'open', 'check']

function* modifyRequirementDetailForm(formDefinition, requirementKey) {
  const requirement = yield rest.fetchEntity(
    'Requirement',
    requirementKey,
    {
      paths: ['relRequirement_rule.enable_upload']
    },
    {},
    api.getFlattenEntity
  )
  const enableUpload = requirement['relRequirement_rule.enable_upload']

  if (!enableUpload) {
    return form.removeBoxes(formDefinition, ['documents'])
  }

  return formDefinition
}

function* modifyEntitydocsListForm(formDefinition, requirementKey) {
  const requirement = yield rest.fetchEntity(
    'Requirement',
    requirementKey,
    {
      paths: ['relRequirement_status.unique_id']
    },
    {},
    api.getFlattenEntity
  )
  const requirementStatus = requirement['relRequirement_status.unique_id']

  if (!uploadActionAllowedCategories.includes(requirementStatus)) {
    return form.removeActions(formDefinition, ['upload_action'])
  }

  return formDefinition
}

export const modifyFormDefinition = (formDefinition, {parent, entityId}, {formBase}) => {
  if (formDefinition.id === `${formBase}_Requirement_detail`) {
    return modifyRequirementDetailForm(formDefinition, entityId)
  } else if (formDefinition.id === `${formBase}_Requirement_detail_relResource_entitydocs_list`) {
    return modifyEntitydocsListForm(formDefinition, parent.key)
  }

  return formDefinition
}

const EducationPortfolio = ({
  formBase,
  searchFormType,
  reportIds,
  searchFilters,
  limit,
  backendUrl,
  businessUnit,
  appContext,
  onVisibilityStatusChange,
  locale
}) => (
  <EntityBrowserApp
    entityName="Schooling"
    formBase={formBase}
    searchFormType={searchFormType}
    searchFilters={searchFilters}
    limit={limit}
    modifyFormDefinition={(formDefinition, context) => modifyFormDefinition(formDefinition, context, {formBase})}
    backendUrl={backendUrl}
    businessUnit={businessUnit}
    appContext={appContext}
    reportIds={reportIds}
    onVisibilityStatusChange={onVisibilityStatusChange}
    locale={locale}
  />
)

EducationPortfolio.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  onVisibilityStatusChange: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default EducationPortfolio
