import {connect} from 'react-redux'

import EducationPortfolio from './EducationPortfolio'

const mapActionCreators = {}

const mapStateToProps = state => ({
  formBase: state.input.formBase,
  searchFormType: state.input.searchFormType,
  reportIds: state.input.reportIds,
  searchFilters: state.input.searchFilters,
  limit: state.input.limit,
  backendUrl: state.input.backendUrl,
  businessUnit: state.input.businessUnit,
  appContext: state.input.appContext,
  onVisibilityStatusChange: state.input.onVisibilityStatusChange,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(EducationPortfolio)
