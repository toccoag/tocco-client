export const detailForm = {
  id: 'Education_portfolio_Requirement_detail',
  componentType: 'form',
  layoutType: 'horizontal-box',
  children: [
    {
      id: 'box1',
      componentType: 'layout',
      children: [
        {
          id: 'box1',
          componentType: 'layout',
          layoutType: 'vertical-box',
          children: [
            {
              id: 'requirement',
              componentType: 'layout',
              layoutType: 'vertical-box',
              children: [
                {
                  id: 'relRequirement_rule.label',
                  componentType: 'field-set',
                  children: [
                    {
                      id: 'relRequirement_rule.label',
                      componentType: 'field',
                      path: 'relRequirement_rule.label',
                      dataType: 'string'
                    }
                  ]
                }
              ]
            },
            {
              id: 'documents',
              componentType: 'layout',
              layoutType: 'vertical-box',
              children: [
                {
                  id: 'relResource_entitydocs',
                  componentType: 'sub-table',
                  path: 'relResource_entitydocs',
                  targetEntity: 'Resource',
                  reverseRelation: 'relRequirement_entitydocs'
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}

export const entitydocsListForm = {
  id: 'Education_portfolio_Requirement_detail_relResource_entitydocs_list',
  label: 'Dokument',
  componentType: 'form',
  children: [
    {
      id: 'main-action-bar',
      label: null,
      componentType: 'action-bar',
      children: [
        {
          id: 'upload_action',
          componentType: 'action'
        }
      ],
      sticky: false
    },
    {
      id: 'table',
      label: null,
      componentType: 'table',
      layoutType: 'table',
      children: [
        {
          id: 'name',
          label: 'Name',
          componentType: 'column',
          children: [
            {
              id: 'name',
              componentType: 'field',
              path: 'name',
              dataType: 'string'
            }
          ]
        }
      ]
    }
  ]
}
