import PropTypes from 'prop-types'
import {useState, useMemo} from 'react'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

import {VisibilityStatus} from '../../visibilityStatus'

import {modifyFormDefinition} from './formModifier'

const StintAuction = props => {
  const {
    formBase,
    searchFilters,
    searchFormType,
    limit,
    backendUrl,
    businessUnit,
    appContext,
    reportIds,
    fireVisibilityStatusChangeEvent,
    onVisibilityStatusChange,
    locale
  } = props

  const [hideEntityBrowser, setHideEntityBrowser] = useState(false)

  const customActionEventHandlers = useMemo(
    () => ({
      stintAuctionRegisterLecturer: {
        onSuccess: () => {
          fireVisibilityStatusChangeEvent([VisibilityStatus.success])
          setHideEntityBrowser(true)
        }
      }
    }),
    [fireVisibilityStatusChangeEvent]
  )

  return (
    <EntityBrowserApp
      entityName="Event"
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      modifyFormDefinition={formDefinition => modifyFormDefinition(formDefinition, appContext)}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      searchFormType={searchFormType}
      reportIds={reportIds}
      onVisibilityStatusChange={onVisibilityStatusChange}
      customActionEventHandlers={customActionEventHandlers}
      locale={locale}
      isHidden={hideEntityBrowser}
      onRouteChange={() => setHideEntityBrowser(false)}
    />
  )
}

StintAuction.propTypes = {
  formBase: PropTypes.string.isRequired,
  searchFormType: searchFormTypePropTypes,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  intl: PropTypes.object.isRequired,
  fireVisibilityStatusChangeEvent: PropTypes.func.isRequired,
  onVisibilityStatusChange: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default StintAuction
