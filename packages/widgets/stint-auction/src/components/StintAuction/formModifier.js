import {form} from 'tocco-app-extensions'

export function modifyFormDefinition(formDefinition, appContext) {
  // allows action to read widget config we're on
  return form.adjustAction(formDefinition, 'stintAuctionRegisterLecturer', action => ({
    ...action,
    properties: {
      ...action.properties,
      widgetKey: appContext.widgetConfigKey
    }
  }))
}
