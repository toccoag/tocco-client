import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import {initPasswordUpdateApp, PasswordUpdateApp} from './PasswordUpdateApp'

const packageName = 'login'
const appName = 'password-update'

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(appName, initPasswordUpdateApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/password_update_input.json')
      consoleLogger.log(`${appName} - input:`, input)

      const app = initPasswordUpdateApp(appName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(app.store, hotReducers)
        })
      }

      appFactory.renderApp(app.component)
    }
  }
})()

export default PasswordUpdateApp
