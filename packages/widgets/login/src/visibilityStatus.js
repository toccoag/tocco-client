export const VisibilityStatus = {
  passwordUpdateSuccess: 'password_update_success',
  passwordUpdateFailed: 'password_update_failed'
}
