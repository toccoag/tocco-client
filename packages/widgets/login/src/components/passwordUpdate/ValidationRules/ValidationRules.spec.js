import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import ValidationRules from './ValidationRules'

describe('login', () => {
  describe('components', () => {
    describe('ValidationRules', () => {
      test('should render empty ul if no rules', () => {
        const {container} = testingLibrary.renderWithIntl(<ValidationRules rules={[]} />)
        jestExpect(container.querySelector('ul')).toBeEmptyDOMElement()
      })

      test('should render rules w/o status if no errors object given', () => {
        const rules = [
          {
            name: 'LENGTH',
            message: 'Must have at least 8 characters'
          },
          {
            name: 'CHARACTERS_DIGITS',
            message: 'Must have at least 1 digit'
          }
        ]

        testingLibrary.renderWithIntl(<ValidationRules rules={rules} />)
        expect(screen.queryByText('Must have at least 8 characters')).to.exist
        expect(screen.queryByText('Must have at least 1 digit')).to.exist
      })

      test('should render rules with success status if errors object empty', async () => {
        const rules = [
          {
            name: 'LENGTH',
            message: 'Must have at least 8 characters'
          },
          {
            name: 'CHARACTERS_DIGITS',
            message: 'Must have at least 1 digit'
          }
        ]
        const errors = {}

        testingLibrary.renderWithIntl(<ValidationRules rules={rules} errors={errors} />)
        expect(screen.queryByText('Must have at least 8 characters')).to.exist
        expect(screen.queryByText('Must have at least 1 digit')).to.exist
        expect(await screen.findAllByTestId('icon-check')).to.have.length(2)
      })

      test('should render rules with error status if errors object not empty', async () => {
        const rules = [
          {
            name: 'LENGTH',
            message: 'Must have at least 8 characters'
          },
          {
            name: 'CHARACTERS_DIGITS',
            message: 'Must have at least 1 digit'
          }
        ]
        const errors = {
          LENGTH: true
        }
        testingLibrary.renderWithIntl(<ValidationRules rules={rules} errors={errors} />)

        expect(screen.queryByText('Must have at least 8 characters')).to.exist
        expect(screen.queryByText('Must have at least 1 digit')).to.exist
        expect(await screen.findAllByTestId('icon-check')).to.have.length(1)
        expect(await screen.findAllByTestId('icon-times')).to.have.length(1)
      })

      test('should render rules with error status and custom message', async () => {
        const rules = [
          {
            name: 'LENGTH',
            message: 'Must have at least 8 characters'
          },
          {
            name: 'CHARACTERS_DIGITS',
            message: 'Must have at least 1 digit'
          }
        ]
        const errors = {
          LENGTH: 'Two more characters required!'
        }
        testingLibrary.renderWithIntl(<ValidationRules rules={rules} errors={errors} />)

        expect(screen.queryByText('Must have at least 8 characters')).to.not.exist
        expect(screen.queryByText('Two more characters required!')).to.exist
        expect(screen.queryByText('Must have at least 1 digit')).to.exist
        expect(await screen.findAllByTestId('icon-check')).to.have.length(1)
        expect(await screen.findAllByTestId('icon-times')).to.have.length(1)
      })
    })
  })
})
