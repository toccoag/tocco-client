import PropTypes from 'prop-types'
import {design, EditableValue, StatedValue} from 'tocco-ui'
import {env} from 'tocco-util'

const PasswordInput = props => {
  const getSignal = () => {
    if (props.value) {
      if (props.valid === undefined) {
        return null
      }

      return props.valid ? design.condition.SUCCESS : design.condition.DANGER
    }
    return null
  }

  const isAdmin = env.isInAdminEmbedded()

  return (
    <StatedValue
      hasValue={!!props.value}
      id={`${props.name}-input`}
      immutable={props.readOnly === true}
      label={props.label}
      signal={getSignal()}
      labelPosition={isAdmin ? 'inside' : 'outside'}
    >
      <EditableValue
        type="password"
        value={props.value}
        id={`${props.name}-input`}
        events={{
          onKeyDown: props.onKeyDown,
          onChange: e => props.onChange(e.target.value)
        }}
        options={{
          'data-cy': 'input-login-form_password',
          autoFocus: props.autoFocus
        }}
        readOnly={props.readOnly === true}
      />
    </StatedValue>
  )
}

PasswordInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  readOnly: PropTypes.bool,
  onKeyDown: PropTypes.func,
  autoFocus: PropTypes.bool,
  valid: PropTypes.bool
}

export default PasswordInput
