import {screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import PasswordUpdateDialog from './PasswordUpdateDialog'

describe('login', () => {
  describe('components', () => {
    describe('PasswordUpdateDialog', () => {
      const emptyPasswordProp = {
        oldPassword: '',
        newPassword: '',
        newPasswordRepeat: '',
        passwordUpdatePending: false
      }

      test('should fetch rules on mount', async () => {
        const fetchValidationRules = sinon.spy()
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={fetchValidationRules}
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )
        await screen.findAllByTestId('icon')

        expect(fetchValidationRules).to.have.property('callCount', 1)
      })

      test('should check current user on mount', async () => {
        const checkCurrentUser = sinon.spy()
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={checkCurrentUser}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )
        await screen.findAllByTestId('icon')

        expect(checkCurrentUser).to.have.been.calledOnce
      })

      test('should call initialized callback once rules rendered', async () => {
        window.requestAnimationFrame = fn => fn()

        const initialized = sinon.spy()
        const props = {
          fetchValidationRules: () => undefined,
          validationRules: null,
          password: emptyPasswordProp,
          intl: IntlStub,
          initialized,
          checkCurrentUser: () => undefined,
          savePassword: () => undefined,
          updateOldPassword: () => undefined,
          updateNewPassword: () => undefined,
          updateNewPasswordRepeat: () => undefined,
          captchaKey: '123'
        }

        const {rerender} = testingLibrary.renderWithIntl(<PasswordUpdateDialog {...props} />)
        await screen.findAllByTestId('icon')

        expect(initialized).to.have.property('callCount', 0)

        rerender(<PasswordUpdateDialog {...props} validationRules={[]} />)

        expect(initialized).to.have.property('callCount', 1)
      })

      test('should display LoadMask until rules loaded', async () => {
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={null}
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
            showTitle={true}
          />
        )
        await screen.findAllByTestId('icon')

        expect(screen.queryAllByText('client.login.passwordUpdate.title')).to.have.length(0)
      })

      test('should hide old password by default', () => {
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={[]}
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )
        expect(screen.queryAllByLabelText('client.login.passwordUpdate.oldPassword')).to.have.length(0)
      })

      test('should handle showOldPasswordField prop', () => {
        const props = {
          fetchValidationRules: () => undefined,
          validationRules: [],
          password: emptyPasswordProp,
          intl: IntlStub,
          initialized: () => undefined,
          checkCurrentUser: () => undefined,
          savePassword: () => undefined,
          updateOldPassword: () => undefined,
          updateNewPassword: () => undefined,
          updateNewPasswordRepeat: () => undefined,
          captchaKey: '123'
        }
        const {rerender} = testingLibrary.renderWithIntl(<PasswordUpdateDialog {...props} showOldPasswordField />)
        expect(screen.queryAllByLabelText('client.login.passwordUpdate.oldPassword')).to.have.length(1)

        rerender(<PasswordUpdateDialog {...props} showOldPasswordField={false} />)
        expect(screen.queryAllByLabelText('client.login.passwordUpdate.oldPassword')).to.have.length(0)
      })

      test('should disable everything except of old password', () => {
        const {container} = testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={[]}
            showOldPasswordField
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(container.querySelector('[id=newPassword-input]')).toBeDisabled()
        jestExpect(container.querySelector('[id=newPasswordRepeat-input]')).toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()
      })

      test('should enable new password as soon as old password is filled', () => {
        const {container} = testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={[]}
            showOldPasswordField
            password={{
              ...emptyPasswordProp,
              oldPassword: 'oldpw'
            }}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(container.querySelector('[id=newPasswordRepeat-input]')).toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()
      })

      test('should enable new password repeat as soon as new password is filled and valid', () => {
        const props = {
          fetchValidationRules: () => undefined,
          validationRules: [],
          showOldPasswordField: true,
          password: {
            ...emptyPasswordProp,
            oldPassword: 'oldpw',
            newPassword: 'invalid',
            newPasswordValidationErrors: {
              LENGTH: true
            }
          },
          intl: IntlStub,
          initialized: () => undefined,
          checkCurrentUser: () => undefined,
          savePassword: () => undefined,
          updateOldPassword: () => undefined,
          updateNewPassword: () => undefined,
          updateNewPasswordRepeat: () => undefined,
          captchaKey: '123'
        }
        const {container, rerender} = testingLibrary.renderWithIntl(<PasswordUpdateDialog {...props} />)

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(container.querySelector('[id=newPasswordRepeat-input]')).toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()

        rerender(
          <PasswordUpdateDialog
            {...props}
            password={{
              ...emptyPasswordProp,
              oldPassword: 'oldpw',
              newPassword: 'validnewpw',
              newPasswordValidationErrors: {}
            }}
          />
        )

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPasswordRepeat')).not.toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()
      })

      test('should enable save Button as soon as new password repeat is filled and matches new password', () => {
        const props = {
          fetchValidationRules: () => undefined,
          validationRules: [],
          showOldPasswordField: true,
          password: {
            ...emptyPasswordProp,
            oldPassword: 'oldpw',
            newPassword: 'newpw',
            newPasswordRepeat: ''
          },
          intl: IntlStub,
          initialized: () => undefined,
          checkCurrentUser: () => undefined,
          savePassword: () => undefined,
          updateOldPassword: () => undefined,
          updateNewPassword: () => undefined,
          updateNewPasswordRepeat: () => undefined,
          captchaKey: '123'
        }

        const {rerender} = testingLibrary.renderWithIntl(<PasswordUpdateDialog {...props} />)

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPasswordRepeat')).not.toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()

        rerender(
          <PasswordUpdateDialog
            {...props}
            password={{
              ...emptyPasswordProp,
              oldPassword: 'oldpw',
              newPassword: 'newpw',
              newPasswordRepeat: 'nomatch'
            }}
          />
        )

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPasswordRepeat')).not.toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).toBeDisabled()

        rerender(
          <PasswordUpdateDialog
            {...props}
            password={{
              ...emptyPasswordProp,
              oldPassword: 'oldpw',
              newPassword: 'newpw',
              newPasswordRepeat: 'newpw'
            }}
          />
        )

        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).not.toBeDisabled()
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPasswordRepeat')).not.toBeDisabled()
        jestExpect(screen.getByRole('button', {name: 'client.login.passwordUpdate.saveButton'})).not.toBeDisabled()
      })

      test('should auto focus old password field if present', () => {
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={[]}
            showOldPasswordField
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.oldPassword')).toHaveFocus()
      })

      test('should auto focus new password field if old password field not present', () => {
        testingLibrary.renderWithIntl(
          <PasswordUpdateDialog
            fetchValidationRules={() => undefined}
            validationRules={[]}
            showOldPasswordField={false}
            password={emptyPasswordProp}
            intl={IntlStub}
            initialized={() => undefined}
            checkCurrentUser={() => undefined}
            savePassword={() => undefined}
            updateOldPassword={() => undefined}
            updateNewPassword={() => undefined}
            updateNewPasswordRepeat={() => undefined}
            captchaKey="123"
          />
        )
        jestExpect(screen.getByLabelText('client.login.passwordUpdate.newPassword')).toHaveFocus()
      })
    })
  })
})
