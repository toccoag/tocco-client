import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initialized} from '../../../modules/passwordUpdate/actions'
import {checkCurrentUser} from '../../../modules/passwordUpdate/dialog/actions'
import {
  updateOldPassword,
  updateNewPassword,
  updateNewPasswordRepeat,
  savePassword
} from '../../../modules/passwordUpdate/password'
import {fetchValidationRules} from '../../../modules/passwordUpdate/validationRules'

import PasswordUpdateDialog from './PasswordUpdateDialog'

const mapActionCreators = {
  updateOldPassword,
  updateNewPassword,
  updateNewPasswordRepeat,
  fetchValidationRules,
  savePassword,
  initialized,
  checkCurrentUser
}

const mapStateToProps = state => ({
  password: state.passwordUpdate.password,
  validationRules: state.passwordUpdate.validationRules,
  showOldPasswordField: state.passwordUpdate.dialog.showOldPasswordField,
  forcedUpdate: state.passwordUpdate.dialog.forcedUpdate,
  captchaKey: state.login.captchaKey,
  needsCaptcha: state.passwordUpdate.dialog.needsCaptcha
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(PasswordUpdateDialog))
