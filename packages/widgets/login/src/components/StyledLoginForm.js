import styled from 'styled-components'
import {StyledInputCss, StyledEditableWrapperCss, Button, StyledButton, StyledButtonGroup, scale} from 'tocco-ui'

export const StyledLoginFormInput = styled.input`
  /* stylelint-disable-next-line block-no-empty */
  @keyframes onAutoFillStart {
    from {
    }
  }

  &&&& {
    ${StyledInputCss}
    transition: background-color 50000s, color 50000s, filter 50000s;

    &:-webkit-autofill {
      animation-duration: 50000s;
      animation-name: onAutoFillStart;
    }
    padding-top: ${scale.space(-0.9)};
    padding-bottom: ${scale.space(-0.9)};
  }
`

export const StyledLoginFormInputWrapper = styled.div`
  &&& {
    ${StyledEditableWrapperCss}
  }
`

export const StyledTransparentButton = styled(Button)`
  &&& {
    margin-right: 0;
    background-color: transparent;
    padding: ${scale.space(-0.1)} 0;
    width: 100%;
    justify-content: center;

    &:hover {
      background-color: transparent;
    }
  }
`

export const StyledLoginFormWrapper = styled.div`
  ${StyledButtonGroup} {
    margin-top: ${scale.space(0.47)};
    width: auto;
    flex-direction: column;

    ${StyledButton} {
      flex-grow: 1;
    }
  }
`
