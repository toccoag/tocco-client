import PropTypes from 'prop-types'
import {useCallback, useEffect, useRef} from 'react'
import ReCAPTCHA from 'react-google-recaptcha'
import {EditableValue, StatedValue} from 'tocco-ui'
import {react} from 'tocco-util'

import {Pages} from '../../types/Pages'
import FormMessage from '../form-components/FormMessage'
import LoginButton from '../form-components/LoginButton'
import Title from '../form-components/Title'
import useAutoFill from '../form-components/useAutoFill'
import {
  StyledLoginFormInput,
  StyledLoginFormInputWrapper,
  StyledLoginFormWrapper,
  StyledTransparentButton
} from '../StyledLoginForm'

const LoginForm = ({
  captchaKey,
  changePage,
  intl,
  login,
  loginPending,
  message,
  password,
  recaptchaActivated,
  setPassword,
  setUsername,
  showTitle,
  username,
  entryPage
}) => {
  const [usernameAutoFill, usernameAutoFillProps] = useAutoFill(setUsername)
  const [passwordAutoFill, passwordAutoFillProps] = useAutoFill(setPassword)
  const recaptchaRef = useRef()

  const hasUsernameForm = entryPage === Pages.USERNAME_FORM

  const msg = id => intl.formatMessage({id})
  const prevRecaptchaActivated = react.usePrevious(recaptchaActivated)

  const handleSubmit = useCallback(
    async e => {
      if (e) {
        e.preventDefault()
      }

      let recaptchaToken = null
      if (recaptchaActivated) {
        recaptchaToken = await recaptchaRef.current.executeAsync()
        recaptchaRef.current.reset()
      }

      login(username, password, recaptchaToken)
    },
    [login, username, password, recaptchaActivated]
  )

  useEffect(() => {
    if (recaptchaActivated && !prevRecaptchaActivated) {
      handleSubmit()
    }
  }, [recaptchaActivated, prevRecaptchaActivated, handleSubmit])

  const passwordFocus = !!username
  const usernameFocus = !passwordFocus

  return (
    <StyledLoginFormWrapper>
      {showTitle && <Title title="client.login.form.title" subtitle="client.login.form.introduction" />}
      <form onSubmit={handleSubmit}>
        <StatedValue
          hasValue={!!username || usernameAutoFill}
          id="login-username"
          label={msg('client.login.form.userPlaceholder')}
          immutable={hasUsernameForm}
          labelPosition="outside"
        >
          <StyledLoginFormInputWrapper>
            <StyledLoginFormInput
              autoFocus={usernameFocus}
              name="user"
              data-cy="input-login-form_user"
              required
              type="text"
              value={username}
              disabled={hasUsernameForm}
              onFocus={e => e.target.select()}
              {...usernameAutoFillProps}
            />
          </StyledLoginFormInputWrapper>
        </StatedValue>

        <StatedValue
          hasValue={!!password || passwordAutoFill}
          id="login-password"
          label={msg('client.login.form.passwordPlaceholder')}
          labelPosition="outside"
        >
          <EditableValue
            type="password"
            value={password}
            events={{
              onFocus: e => {
                if (typeof e.target.select === 'function') {
                  e.target.select()
                }
              },
              ...passwordAutoFillProps
            }}
            options={{
              required: true,
              'data-cy': 'input-login-form_password',
              autoFocus: passwordFocus,
              extraHigh: true,
              name: 'password'
            }}
          />
        </StatedValue>

        <FormMessage message={message} />

        {captchaKey && recaptchaActivated && (
          <ReCAPTCHA ref={recaptchaRef} badge="bottomright" size="invisible" sitekey={captchaKey} hl={intl.locale} />
        )}
        <LoginButton
          disabled={loginPending || (username === '' && !usernameAutoFill) || (password === '' && !passwordAutoFill)}
          label={msg('client.login.form.button')}
          pending={loginPending}
          data-cy="btn-login-form_login"
        />
        <StyledTransparentButton
          label={msg('client.login.form.forgotLink')}
          onClick={() => changePage(Pages.PASSWORD_REQUEST)}
          data-cy="btn-login-form_request"
        />
        {hasUsernameForm && (
          <StyledTransparentButton
            label={msg('client.login.form.backLink')}
            onClick={() => changePage(Pages.USERNAME_FORM)}
          />
        )}
      </form>
    </StyledLoginFormWrapper>
  )
}

LoginForm.propTypes = {
  captchaKey: PropTypes.string,
  changePage: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  loginPending: PropTypes.bool.isRequired,
  message: PropTypes.shape({
    negative: PropTypes.bool,
    text: PropTypes.string
  }),
  password: PropTypes.string,
  recaptchaActivated: PropTypes.bool,
  setPassword: PropTypes.func.isRequired,
  setUsername: PropTypes.func.isRequired,
  showTitle: PropTypes.bool,
  username: PropTypes.string,
  entryPage: PropTypes.string
}

export default LoginForm
