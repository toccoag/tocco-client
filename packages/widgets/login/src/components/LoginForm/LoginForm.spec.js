import {createEvent, fireEvent, screen} from '@testing-library/react'
import {IntlStub, testingLibrary, TestThemeProvider} from 'tocco-test-util'

import {Pages} from '../../types/Pages'

import LoginForm from './LoginForm'

describe('login', () => {
  describe('components', () => {
    describe('LoginForm', () => {
      test('should render some components', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        expect(screen.queryAllByRole('button', {name: 'client.login.form.button'})).to.have.length(1)
        expect(screen.queryAllByRole('button', {name: 'client.login.form.forgotLink'})).to.have.length(1)
      })

      test('should render title', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              showTitle
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        expect(screen.getByText('client.login.form.title')).to.exist
        expect(screen.getByText('client.login.form.introduction')).to.exist
      })

      test('should disable button if username and password are not set', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username=""
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        jestExpect(screen.getByRole('button', {name: 'client.login.form.button'})).toBeDisabled()
      })

      test('should disable button if only username is set', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        jestExpect(screen.getByRole('button', {name: 'client.login.form.button'})).toBeDisabled()
      })

      test('should disable button if only password is set', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username=""
              password="password"
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        jestExpect(screen.getByRole('button', {name: 'client.login.form.button'})).toBeDisabled()
      })

      test('should enable button if username and password are set', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password="password"
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )
        jestExpect(screen.getByRole('button', {name: 'client.login.form.button'})).not.toBeDisabled()
      })

      test('should change page if password is requested', () => {
        const changePage = sinon.spy()

        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={changePage}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password="password"
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const passwordRequestButton = screen.getByRole('button', {name: 'client.login.form.forgotLink'})
        fireEvent.click(passwordRequestButton)
        expect(changePage).to.have.property('callCount', 1)
        expect(changePage.firstCall.args).to.eql([Pages.PASSWORD_REQUEST])
      })

      test('should call setUsername on username change', () => {
        const setUsername = sinon.spy()

        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={setUsername}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password="password"
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const nameInput = screen.getByRole('textbox')
        fireEvent.change(nameInput, {
          target: {
            value: 'newusername'
          }
        })

        expect(setUsername).to.have.property('callCount', 1)
        expect(setUsername.firstCall.args).to.eql(['newusername'])
      })

      test('should call setPassword on password change', () => {
        const setPassword = sinon.spy()

        const {container} = testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={setPassword}
              loginPending={false}
              username="username"
              password="password"
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const passwordInput = container.querySelector('input[type=password]')
        fireEvent.change(passwordInput, {
          target: {
            value: 'newpassword'
          }
        })

        expect(setPassword).to.have.property('callCount', 1)
        expect(setPassword.firstCall.args).to.eql(['newpassword'])
      })

      test('should prevent default and call login on submit', () => {
        const login = sinon.spy()

        const {container} = testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={login}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password="password"
            />
          </TestThemeProvider>
        )

        const formElement = container.querySelector('form')

        const submitEvent = createEvent.submit(formElement)
        fireEvent(formElement, submitEvent)

        expect(submitEvent.defaultPrevented).to.be.true

        expect(login).to.have.property('callCount', 1)
        expect(login.firstCall.args).to.eql(['username', 'password', null])
      })

      test('should focus user input if not username is set', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username=""
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const userInput = screen.getByRole('textbox')
        jestExpect(userInput).toHaveFocus()
      })

      test('should focus password input if username is set', () => {
        const {container} = testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const passwordInput = container.querySelector('input[name="password"]')
        jestExpect(passwordInput).toHaveFocus()
      })

      test('should disable username input when username form is available', () => {
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={() => undefined}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              entryPage={Pages.USERNAME_FORM}
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const userInput = screen.getByRole('textbox')
        jestExpect(userInput).toBeDisabled()
      })

      test('should show back link when username form is available', () => {
        const changePageSpy = sinon.spy()
        testingLibrary.renderWithIntl(
          <TestThemeProvider>
            <LoginForm
              intl={IntlStub}
              login={() => undefined}
              changePage={changePageSpy}
              setUsername={() => undefined}
              setPassword={() => undefined}
              loginPending={false}
              username="username"
              entryPage={Pages.USERNAME_FORM}
              password=""
              googleReCaptchaProps={{
                executeRecaptcha: () => {}
              }}
            />
          </TestThemeProvider>
        )

        const backButton = screen.getByRole('button', {name: 'client.login.form.backLink'})
        expect(backButton).to.exist

        fireEvent.click(backButton)
        expect(changePageSpy).to.have.been.calledWith(Pages.USERNAME_FORM)
      })
    })
  })
})
