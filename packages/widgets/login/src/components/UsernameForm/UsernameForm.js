import PropTypes from 'prop-types'
import {useCallback} from 'react'
import {StatedValue} from 'tocco-ui'

import FormMessage from '../form-components/FormMessage'
import LoginButton from '../form-components/LoginButton'
import Title from '../form-components/Title'
import useAutoFill from '../form-components/useAutoFill'
import {StyledLoginFormInput, StyledLoginFormInputWrapper, StyledLoginFormWrapper} from '../StyledLoginForm'

const UsernameForm = ({intl, checkUsername, loginPending, message, setUsername, showTitle, username}) => {
  const [usernameAutoFill, usernameAutoFillProps] = useAutoFill(setUsername)

  const msg = id => intl.formatMessage({id})

  const handleSubmit = useCallback(
    async e => {
      if (e) {
        e.preventDefault()
      }

      checkUsername(username)
    },
    [checkUsername, username]
  )

  return (
    <StyledLoginFormWrapper>
      {showTitle && <Title title="client.login.form.title" subtitle="client.login.form.introduction" />}
      <form onSubmit={handleSubmit}>
        <StatedValue
          hasValue={!!username || usernameAutoFill}
          id="login-username"
          label={msg('client.login.form.userEmailPlaceholder')}
          labelPosition="outside"
        >
          <StyledLoginFormInputWrapper>
            <StyledLoginFormInput
              autoFocus
              name="user"
              required
              type="text"
              value={username}
              onFocus={e => e.target.select()}
              {...usernameAutoFillProps}
            />
          </StyledLoginFormInputWrapper>
        </StatedValue>

        <FormMessage message={message} />

        <LoginButton
          disabled={!usernameAutoFill && (loginPending || username === '')}
          label={msg('client.login.form.next')}
          pending={loginPending}
        />
      </form>
    </StyledLoginFormWrapper>
  )
}

UsernameForm.propTypes = {
  intl: PropTypes.object.isRequired,
  checkUsername: PropTypes.func.isRequired,
  loginPending: PropTypes.bool.isRequired,
  message: PropTypes.shape({
    negative: PropTypes.bool,
    text: PropTypes.string
  }),
  setUsername: PropTypes.func.isRequired,
  showTitle: PropTypes.bool,
  username: PropTypes.string
}

export default UsernameForm
