import {fireEvent, screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import UsernameForm from './UsernameForm'

describe('login', () => {
  describe('components', () => {
    describe('UsernameForm', () => {
      test('should render', () => {
        testingLibrary.renderWithIntl(
          <UsernameForm
            intl={IntlStub}
            checkUsername={() => undefined}
            setUsername={() => undefined}
            loginPending={false}
          />
        )
        expect(screen.queryAllByRole('button')).to.have.length(1)
      })

      test('should disable next button when username is not set', () => {
        testingLibrary.renderWithIntl(
          <UsernameForm
            intl={IntlStub}
            checkUsername={() => undefined}
            username=""
            setUsername={() => undefined}
            loginPending={false}
          />
        )
        jestExpect(screen.queryByRole('button')).toBeDisabled()
      })

      test('should invoke checkUsername on form submit', () => {
        const checkUsername = sinon.spy()
        testingLibrary.renderWithIntl(
          <UsernameForm
            intl={IntlStub}
            checkUsername={checkUsername}
            username="user1"
            setUsername={() => undefined}
            loginPending={false}
          />
        )

        fireEvent.click(screen.queryByRole('button'))

        expect(checkUsername).to.have.been.calledWith('user1')
      })
    })
  })
})
