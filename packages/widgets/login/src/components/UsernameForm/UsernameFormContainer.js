import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {checkUsername} from '../../modules/actions'
import {setUsername} from '../../modules/login/actions'

import UsernameForm from './UsernameForm'

const mapActionCreators = {
  setUsername,
  checkUsername
}

const mapStateToProps = state => ({
  username: state.login.username,
  message: state.loginForm.message,
  loginPending: state.loginForm.loginPending
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(UsernameForm))
