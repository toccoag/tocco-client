import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {requestPassword} from '../../modules/actions'
import {changePage} from '../../modules/login/actions'

import PasswordRequest from './PasswordRequest'

const mapActionCreators = {
  changePage,
  requestPassword
}

const mapStateToProps = state => ({
  username: state.login.username,
  pending: state.loginForm.loginPending,
  entryPage: state.login.entryPage
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(PasswordRequest))
