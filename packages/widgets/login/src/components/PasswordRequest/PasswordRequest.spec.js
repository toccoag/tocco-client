import {createEvent, fireEvent, screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import {Pages} from '../../types/Pages'

import PasswordRequest from './PasswordRequest'

describe('login', () => {
  describe('components', () => {
    describe('PasswordRequest', () => {
      test('should render components', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )

        expect(screen.queryAllByRole('textbox')).to.have.length(1)
        expect(screen.queryAllByRole('button')).to.have.length(2)
      })

      test('should have an initial password state', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )
        jestExpect(screen.getByRole('textbox')).toHaveValue('')
      })

      test('should update username state on username change', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )

        const userInput = screen.getByRole('textbox')
        fireEvent.change(userInput, {
          target: {
            value: 'user1'
          }
        })

        jestExpect(userInput).toHaveValue('user1')
      })

      test('should disable submit button if username is not set', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )

        const button = screen.getByRole('button', {name: 'client.login.passwordRequest.button'})
        jestExpect(button).toBeDisabled()
      })

      test('should enable submit button if username is set', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )

        const userInput = screen.getByRole('textbox')
        fireEvent.change(userInput, {
          target: {
            value: 'user1'
          }
        })

        const button = screen.getByRole('button', {name: 'client.login.passwordRequest.button'})
        jestExpect(button).not.toBeDisabled()
      })

      test('should hide title by default', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} />
        )

        expect(screen.queryAllByText('client.login.passwordRequest.title')).to.have.length(0)
        expect(screen.queryAllByText('client.login.passwordRequest.introduction')).to.have.length(1)
      })

      test('should display title if showTitle prop is true', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={() => undefined} showTitle />
        )

        expect(screen.queryAllByText('client.login.passwordRequest.title')).to.have.length(1)
        expect(screen.queryAllByText('client.login.passwordRequest.introduction')).to.have.length(1)
      })

      test('should prevent default and call requestPassword on submit', () => {
        const requestPassword = sinon.spy()

        const {container} = testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={() => undefined} requestPassword={requestPassword} />
        )

        const formElement = container.querySelector('form')

        const submitEvent = createEvent.submit(formElement)
        fireEvent(formElement, submitEvent)

        expect(submitEvent.defaultPrevented).to.be.true
        expect(requestPassword).to.have.property('callCount', 1)
      })

      test('should call changePage on abort', () => {
        const changePage = sinon.spy()

        testingLibrary.renderWithIntl(
          <PasswordRequest intl={IntlStub} changePage={changePage} requestPassword={() => undefined} />
        )

        const abortButton = screen.getByRole('button', {name: 'client.login.passwordRequest.abortButton'})
        fireEvent.click(abortButton)

        expect(changePage).to.have.property('callCount', 1)
        expect(changePage.firstCall.args).to.eql([Pages.LOGIN_FORM])
      })

      test('should display provided username', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest
            intl={IntlStub}
            changePage={() => undefined}
            requestPassword={() => undefined}
            username="tocco"
          />
        )
        jestExpect(screen.getByRole('textbox')).toHaveValue('tocco')
      })

      test('should disable username input when username form is available', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest
            intl={IntlStub}
            changePage={() => undefined}
            requestPassword={() => undefined}
            username="tocco"
            entryPage={Pages.USERNAME_FORM}
          />
        )

        const userInput = screen.getByRole('textbox')
        jestExpect(userInput).toBeDisabled()
      })

      test('should hide intro text when username form is available', () => {
        testingLibrary.renderWithIntl(
          <PasswordRequest
            intl={IntlStub}
            changePage={() => undefined}
            requestPassword={() => undefined}
            username="tocco"
            entryPage={Pages.USERNAME_FORM}
          />
        )

        expect(screen.queryAllByText('client.login.passwordRequest.introduction')).to.have.length(0)
      })
    })
  })
})
