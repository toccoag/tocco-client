import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {StatedValue, Typography} from 'tocco-ui'

import {Pages} from '../../types/Pages'
import LoginButton from '../form-components/LoginButton'
import Title from '../form-components/Title'
import {
  StyledLoginFormInput,
  StyledLoginFormInputWrapper,
  StyledLoginFormWrapper,
  StyledTransparentButton
} from '../StyledLoginForm'

const PasswordRequest = ({entryPage, username, requestPassword, intl, showTitle, pending, changePage}) => {
  const [userName, setUserName] = useState(username || '')

  const hasUsernameForm = entryPage === Pages.USERNAME_FORM

  const handleSubmit = e => {
    e.preventDefault()
    requestPassword(userName)
  }

  const handleUsernameChange = e => {
    setUserName(e.target.value)
  }

  const msg = id => intl.formatMessage({id})

  return (
    <StyledLoginFormWrapper>
      {showTitle && <Title title="client.login.passwordRequest.title" />}
      {!hasUsernameForm && (
        <Typography.P>
          <FormattedMessage id="client.login.passwordRequest.introduction" />
        </Typography.P>
      )}
      <form onSubmit={handleSubmit}>
        <StatedValue
          hasValue={Boolean(userName)}
          id="login-username"
          label={msg('client.login.form.userPlaceholder')}
          immutable={hasUsernameForm}
          labelPosition="outside"
        >
          <StyledLoginFormInputWrapper>
            <StyledLoginFormInput
              name="user"
              onChange={handleUsernameChange}
              required
              type="text"
              value={userName}
              disabled={hasUsernameForm}
              data-cy="input-password-request"
            />
          </StyledLoginFormInputWrapper>
        </StatedValue>

        <LoginButton
          disabled={!userName || pending}
          label={msg('client.login.passwordRequest.button')}
          pending={pending}
          data-cy="btn-password-request_submit"
        />
        <StyledTransparentButton
          disabled={pending}
          label={msg('client.login.passwordRequest.abortButton')}
          name="abort"
          onClick={() => changePage(Pages.LOGIN_FORM)}
          data-cy="btn-password-request_abort"
        />
      </form>
    </StyledLoginFormWrapper>
  )
}

PasswordRequest.propTypes = {
  intl: PropTypes.object.isRequired,
  changePage: PropTypes.func.isRequired,
  requestPassword: PropTypes.func.isRequired,
  showTitle: PropTypes.bool,
  pending: PropTypes.bool,
  username: PropTypes.string,
  entryPage: PropTypes.string
}

export default PasswordRequest
