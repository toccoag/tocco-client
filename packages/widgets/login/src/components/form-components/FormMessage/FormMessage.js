import PropTypes from 'prop-types'
import {injectIntl} from 'react-intl'
import {SignalList} from 'tocco-ui'

const FormMessage = ({message, intl}) => {
  const msg = id => intl.formatMessage({id})

  if (message && message.text) {
    return (
      <SignalList.List>
        <SignalList.Item condition={message.negative ? 'danger' : 'base'} label={msg(message.text)} />
      </SignalList.List>
    )
  }
  return null
}

FormMessage.propTypes = {
  intl: PropTypes.object.isRequired,
  message: PropTypes.shape({text: PropTypes.string, negative: PropTypes.bool})
}

export default injectIntl(FormMessage)
