import {useState} from 'react'

const useAutoFill = onChange => {
  const [autoFill, setAutoFill] = useState(false)

  const handleChange = e => {
    onChange(e.target.value)
    setAutoFill(false)
  }

  const handleAutoFill = e => {
    if (e.animationName === 'onAutoFillStart') {
      setAutoFill(true)
    }
  }

  const inputProps = {
    onAnimationStart: handleAutoFill,
    onChange: handleChange
  }

  return [autoFill, inputProps]
}

export default useAutoFill
