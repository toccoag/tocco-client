import PropTypes from 'prop-types'
import {FormattedMessage} from 'react-intl'
import {Typography} from 'tocco-ui'

const Title = ({title, subtitle}) => (
  <>
    <Typography.H5>
      <FormattedMessage id={title} />
    </Typography.H5>
    {subtitle && (
      <Typography.P>
        <FormattedMessage id={subtitle} />
      </Typography.P>
    )}
  </>
)

Title.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string
}

export default Title
