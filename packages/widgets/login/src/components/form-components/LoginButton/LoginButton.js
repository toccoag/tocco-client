import PropTypes from 'prop-types'

import {StyledLoginButton} from './StyledComponents'

const LoginButton = ({disabled, pending, label, ...props}) => (
  <StyledLoginButton
    disabled={disabled}
    pending={pending}
    label={label}
    ink="primary"
    look="raised"
    type="submit"
    data-cy="btn-submit"
    {...props}
  />
)

LoginButton.propTypes = {
  disabled: PropTypes.bool,
  pending: PropTypes.bool,
  label: PropTypes.string
}

export default LoginButton
