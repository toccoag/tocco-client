import styled from 'styled-components'
import {Button, scale, theme} from 'tocco-ui'

export const StyledLoginButton = styled(Button)`
  &&& {
    font-size: ${scale.font(1.3)};
    border-radius: ${theme.radii('buttonLarge')};
    display: flex;
    justify-content: center;
    padding: 1.25rem 0;
    width: 100%;
    margin-top: 20px;
    margin-bottom: 10px;

    &:not(:last-child) {
      margin-right: 0;
    }
  }
`
