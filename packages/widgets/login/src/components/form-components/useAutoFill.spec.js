import {act, renderHook} from '@testing-library/react'

import useAutoFill from './useAutoFill'

describe('login', () => {
  describe('form-components', () => {
    describe('useAutoFill', () => {
      test('should handle initial state', () => {
        const handleChange = sinon.spy()

        const {result} = renderHook(() => useAutoFill(handleChange))

        const [autoFill, inputProps] = result.current
        expect(autoFill).to.be.false
        expect(inputProps.onAnimationStart).to.exist
        expect(inputProps.onChange).to.exist
      })

      test('should handle onChange', () => {
        const handleChange = sinon.spy()

        const {result} = renderHook(() => useAutoFill(handleChange))

        const [_autoFill, inputProps] = result.current // eslint-disable-line no-unused-vars

        act(() => {
          inputProps.onChange({target: {value: 'abc'}})
        })

        const [autoFill] = result.current
        expect(autoFill).to.be.false
        expect(handleChange).to.have.been.calledWith('abc')
      })

      test('should handle auto fill properly', () => {
        const handleChange = sinon.spy()

        const {result} = renderHook(() => useAutoFill(handleChange))

        const [_autoFill, inputProps] = result.current // eslint-disable-line no-unused-vars

        act(() => {
          inputProps.onAnimationStart({animationName: 'onAutoFillStart'})
        })

        const [autoFill1] = result.current
        expect(autoFill1).to.be.true

        act(() => {
          inputProps.onChange({target: {value: 'abc'}})
        })

        const [autoFill2] = result.current
        expect(autoFill2).to.be.false
        expect(handleChange).to.have.been.calledWith('abc')
      })
    })
  })
})
