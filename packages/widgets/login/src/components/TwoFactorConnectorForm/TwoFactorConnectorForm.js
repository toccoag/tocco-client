import PropTypes from 'prop-types'
import TwoFactorConnectorApp from 'tocco-two-factor-connector/src/main'

import {Pages} from '../../types/Pages'
import Title from '../form-components/Title'

const TwoFactorConnectorForm = ({locale, username, password, secret, showTitle, changePage}) => {
  return (
    <div>
      {showTitle && <Title title="client.login.form.title" />}
      <TwoFactorConnectorApp
        locale={locale}
        username={username}
        password={password}
        secret={secret}
        forced={true}
        onSuccess={() => {
          changePage(Pages.LOGIN_FORM)
        }}
      />
    </div>
  )
}

TwoFactorConnectorForm.propTypes = {
  locale: PropTypes.string,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  secret: PropTypes.shape({
    text: PropTypes.string.isRequired,
    uri: PropTypes.string.isRequired
  }).isRequired,
  showTitle: PropTypes.bool,
  changePage: PropTypes.func.isRequired
}

export default TwoFactorConnectorForm
