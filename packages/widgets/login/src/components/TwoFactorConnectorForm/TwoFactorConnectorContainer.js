import {connect} from 'react-redux'

import {changePage} from '../../modules/login/actions'

import TwoFactorConnectorForm from './TwoFactorConnectorForm'

const mapActionCreators = {
  changePage
}

const mapStateToProps = state => ({
  username: state.login.username,
  password: state.login.password,
  secret: state.twoStepLogin.secret,
  locale: state.intl.locale
})

export default connect(mapStateToProps, mapActionCreators)(TwoFactorConnectorForm)
