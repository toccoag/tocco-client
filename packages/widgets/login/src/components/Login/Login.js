import PropTypes from 'prop-types'
import {useEffect} from 'react'
import {createGlobalStyle} from 'styled-components'

import {Pages} from '../../types/Pages'
import LoginForm from '../LoginForm'
import PasswordRequest from '../PasswordRequest'
import PasswordUpdate from '../passwordUpdate/PasswordUpdateDialog'
import TwoFactorConnector from '../TwoFactorConnectorForm'
import TwoStepLogin from '../TwoStepLoginForm'
import UsernameForm from '../UsernameForm'

const GlobalRecaptchaBadgeStyle = createGlobalStyle`
  .grecaptcha-badge {
    z-index: 9999999;
    opacity: .8;
  }
`

const Login = ({initialize, showTitle, currentPage}) => {
  useEffect(() => {
    initialize()
  }, [initialize])

  return (
    <>
      <GlobalRecaptchaBadgeStyle />
      {(() => {
        switch (currentPage) {
          case Pages.PASSWORD_UPDATE:
            return <PasswordUpdate showTitle={showTitle} />
          case Pages.PASSWORD_REQUEST:
            return <PasswordRequest showTitle={showTitle} />
          case Pages.TWOSTEPLOGIN:
            return <TwoStepLogin showTitle={showTitle} />
          case Pages.TWOSTEPLOGIN_ACTIVATION:
            return <TwoFactorConnector />
          case Pages.USERNAME_FORM:
            return <UsernameForm showTitle={showTitle} />
          default:
            return <LoginForm showTitle={showTitle} />
        }
      })()}
    </>
  )
}

Login.propTypes = {
  initialize: PropTypes.func.isRequired,
  currentPage: PropTypes.string,
  showTitle: PropTypes.bool
}

export default Login
