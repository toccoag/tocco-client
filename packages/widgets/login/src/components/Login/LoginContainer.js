import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {initialize} from '../../modules/actions'

import Login from './Login'

const mapActionCreators = {
  initialize
}

const mapStateToProps = (state, props) => ({
  currentPage: props.currentPage || state.login.currentPage,
  showTitle: props.showTitle
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Login))
