import {screen} from '@testing-library/react'
import {testingLibrary} from 'tocco-test-util'

import {Pages} from '../../types/Pages'

import Login from './Login'

jest.mock('../LoginForm', () => () => <div data-testid="login-form" />)
jest.mock('../PasswordRequest', () => () => <div data-testid="password-request" />)
jest.mock('../passwordUpdate/PasswordUpdateDialog', () => () => <div data-testid="password-update" />)
jest.mock('../TwoStepLoginForm', () => () => <div data-testid="two-step-login-form" />)
jest.mock('../UsernameForm', () => () => <div data-testid="username-form" />)

describe('login', () => {
  describe('components', () => {
    describe('Login', () => {
      test('should render LoginFormContainer as default', () => {
        testingLibrary.renderWithIntl(<Login initialize={() => undefined} />)
        expect(screen.queryByTestId('login-form')).to.exist
      })

      test('should render PasswordUpdateContainer', () => {
        testingLibrary.renderWithIntl(<Login currentPage={Pages.PASSWORD_UPDATE} initialize={() => undefined} />)
        expect(screen.queryByTestId('login-form')).to.not.exist
        expect(screen.queryByTestId('password-update')).to.exist
      })

      test('should render PasswordRequestContainer', () => {
        testingLibrary.renderWithIntl(<Login currentPage={Pages.PASSWORD_REQUEST} initialize={() => undefined} />)
        expect(screen.queryByTestId('password-request')).to.exist
      })

      test('should render TwoStepLoginContainer', () => {
        testingLibrary.renderWithIntl(<Login currentPage={Pages.TWOSTEPLOGIN} initialize={() => undefined} />)
        expect(screen.queryByTestId('two-step-login-form')).to.exist
      })

      test('should render UsernameForm', () => {
        testingLibrary.renderWithIntl(<Login currentPage={Pages.USERNAME_FORM} initialize={() => undefined} />)
        expect(screen.queryByTestId('username-form')).to.exist
      })
    })
  })
})
