import {createEvent, fireEvent, screen} from '@testing-library/react'
import {IntlStub, testingLibrary} from 'tocco-test-util'

import TwoStepLoginForm from './TwoStepLoginForm'

describe('login', () => {
  describe('components', () => {
    describe('TwoStepLoginForm', () => {
      test('should render components', () => {
        testingLibrary.renderWithIntl(
          <TwoStepLoginForm
            intl={IntlStub}
            changePage={() => undefined}
            twoStepLogin={() => undefined}
            username=""
            password=""
          />
        )

        expect(screen.queryAllByRole('textbox')).to.have.length(1)
        expect(screen.queryAllByRole('button')).to.have.length(1)
      })

      test('should disable submit button if code is not set', () => {
        testingLibrary.renderWithIntl(
          <TwoStepLoginForm
            intl={IntlStub}
            changePage={() => undefined}
            twoStepLogin={() => undefined}
            username=""
            password=""
          />
        )
        jestExpect(screen.queryByRole('button', {name: 'client.login.form.button'})).toBeDisabled()
      })

      test('should hide title by default', () => {
        testingLibrary.renderWithIntl(
          <TwoStepLoginForm
            intl={IntlStub}
            changePage={() => undefined}
            twoStepLogin={() => undefined}
            username=""
            password=""
          />
        )

        expect(screen.queryAllByText('client.login.form.title')).to.have.length(0)
      })

      test('should display title if showTitle prop is true', () => {
        testingLibrary.renderWithIntl(
          <TwoStepLoginForm
            intl={IntlStub}
            changePage={() => undefined}
            login={() => undefined}
            showTitle
            username=""
            password=""
          />
        )

        expect(screen.queryAllByText('client.login.form.title')).to.have.length(1)
      })

      test('should prevent default and call login on submit', () => {
        const login = sinon.spy()

        const {container} = testingLibrary.renderWithIntl(
          <TwoStepLoginForm intl={IntlStub} changePage={() => undefined} login={login} username="" password="" />
        )

        const formElement = container.querySelector('form')

        const submitEvent = createEvent.submit(formElement)
        fireEvent(formElement, submitEvent)

        expect(submitEvent.defaultPrevented).to.be.true

        expect(login).to.have.property('callCount', 1)
      })
    })
  })
})
