import PropTypes from 'prop-types'
import {useState, useLayoutEffect, useRef} from 'react'
import {FormattedMessage} from 'react-intl'
import {StatedValue, Typography, EditableValue} from 'tocco-ui'

import LoginButton from '../form-components/LoginButton'
import Title from '../form-components/Title'
import {StyledLoginFormInputWrapper} from '../StyledLoginForm'

import {StyledTwoStepLogin} from './StyledTwoStepLoginForm'

const TwoStepLoginForm = ({username, password, showTitle, loginPending, login, intl}) => {
  const [userCode, setUserCode] = useState(null)
  const formEl = useRef()

  useLayoutEffect(() => {
    const firstInput = formEl.current.querySelector('input')
    firstInput.focus()
  }, [])

  const handleSubmit = e => {
    e.preventDefault()
    login(username, password, null, userCode)
  }

  const msg = id => intl.formatMessage({id})
  return (
    <StyledTwoStepLogin>
      {showTitle && <Title title="client.login.form.title" />}
      <form onSubmit={handleSubmit} ref={formEl}>
        <Typography.P>
          <FormattedMessage id="client.login.twoStepLogin.introduction" />
        </Typography.P>
        <StatedValue
          hasValue={!!userCode}
          id="twoFactorCode-label"
          label={msg('client.login.twoStepLogin.codePlaceholder')}
          labelPosition="outside"
        >
          <StyledLoginFormInputWrapper>
            <EditableValue
              type="integer"
              value={userCode}
              events={{onChange: setUserCode}}
              options={{format: '### ###', allowLeadingZeros: true}}
            />
          </StyledLoginFormInputWrapper>
        </StatedValue>
        <LoginButton
          disabled={!userCode || userCode.toString().length < 6 || loginPending}
          label={msg('client.login.form.button')}
          pending={loginPending}
        />
      </form>
    </StyledTwoStepLogin>
  )
}

TwoStepLoginForm.propTypes = {
  intl: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  showTitle: PropTypes.bool,
  loginPending: PropTypes.bool
}

export default TwoStepLoginForm
