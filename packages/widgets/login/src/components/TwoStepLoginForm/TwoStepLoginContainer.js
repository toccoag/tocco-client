import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'

import {login} from '../../modules/actions'

import TwoStepLoginForm from './TwoStepLoginForm'

const mapActionCreators = {
  login
}

const mapStateToProps = state => ({
  username: state.login.username,
  password: state.login.password,
  loginPending: state.loginForm.loginPending
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(TwoStepLoginForm))
