import PropTypes from 'prop-types'
import {LoadMask} from 'tocco-ui'

import Login from '../../components/Login'

const LoadUsernameMask = ({usernameOrPk, showTitle}) => (
  <LoadMask required={[usernameOrPk]}>
    <Login currentPage="PASSWORD_UPDATE" showTitle={showTitle} />
  </LoadMask>
)

LoadUsernameMask.propTypes = {
  usernameOrPk: PropTypes.any,
  showTitle: PropTypes.bool
}

export default LoadUsernameMask
