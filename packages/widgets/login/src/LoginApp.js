import PropTypes from 'prop-types'
import {appFactory, errorLogging, externalEvents} from 'tocco-app-extensions'
import {env, appContext} from 'tocco-util'

import Login from './components/Login'
import * as login from './modules/login/actions'
import * as passwordUpdate from './modules/passwordUpdate/dialog/actions'
import loginReducers, {sagas} from './modules/reducers'
import {Pages} from './types/Pages'

const packageName = 'login'

const EXTERNAL_EVENTS = [
  /**
   * This event is fired after successful login attempt
   */
  'loginSuccess',
  /**
   * This event is fired when a new username as a valid e-mail address is submitted (only via `username` page)
   */
  'register',
  'resize'
]
const ENTRY_PAGE_MAP = {
  username: Pages.USERNAME_FORM,
  'password-request': Pages.PASSWORD_REQUEST,
  login: Pages.LOGIN_FORM
}

export const initLoginApp = (id, input, events, publicPath, customTheme) => {
  const entryPage = ENTRY_PAGE_MAP[input.entryPage] || Pages.LOGIN_FORM

  const actions = [
    passwordUpdate.setShowOldPasswordField(false),
    passwordUpdate.setStandalone(false),
    login.setUsername(input.username || ''),
    login.setEntryPage(entryPage),
    login.changePage(entryPage)
  ]

  const showTitle = !!input.showTitle
  const content = <Login showTitle={showTitle} />

  const store = appFactory.createStore(loginReducers, sagas, input, packageName)

  env.setInputEnvs(input)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))
  errorLogging.addToStore(store, true, ['console', 'remote'])

  return appFactory.createApp(packageName, content, store, {
    input,
    actions,
    publicPath,
    textResourceModules: ['login'],
    customTheme
  })
}

export const LoginApp = props => {
  const {component} = appFactory.useApp({
    initApp: initLoginApp,
    props,
    packageName,
    externalEvents: EXTERNAL_EVENTS
  })
  return component
}

LoginApp.propTypes = {
  /**
   * Display a title above the dialog
   */
  showTitle: PropTypes.bool,
  /**
   * ISO Language Code
   */
  locale: PropTypes.string,
  /**
   * Defines first page for login component (default: `'login`)
   */
  entryPage: PropTypes.oneOf(['username', 'login', 'password-request']),
  /**
   * Open login app with pre-filled username
   */
  username: PropTypes.string,
  setLocale: PropTypes.func,
  backendUrl: PropTypes.string,
  /**
   * A URL that the user gets redirected to after they're logged in
   * If empty, the `loginSuccess` event will probably need to be used
   */
  redirectUrl: PropTypes.string,
  appContext: appContext.propTypes,
  ...EXTERNAL_EVENTS.reduce((propTypes, event) => {
    propTypes[event] = PropTypes.func
    return propTypes
  }, {})
}
