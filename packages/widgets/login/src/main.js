import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, consoleLogger} from 'tocco-util'

import {initLoginApp, LoginApp} from './LoginApp'
import {initPasswordUpdateApp, PasswordUpdateApp} from './PasswordUpdateApp'

const packageName = 'login'

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initLoginApp)
    appFactory.registerAppInRegistry('password-update', initPasswordUpdateApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/login_input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initLoginApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

export {PasswordUpdateApp}
export default LoginApp
export const app = appFactory.createBundleableApp(packageName, initLoginApp, LoginApp)
export const appPasswordUpdate = appFactory.createBundleableApp(
  'password-update',
  initPasswordUpdateApp,
  PasswordUpdateApp
)
