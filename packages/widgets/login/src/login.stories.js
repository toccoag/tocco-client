import {linkTo} from '@storybook/addon-links'
import {useState} from 'react'
import {v4 as uuid} from 'uuid'

import LoginApp from './main'

export default {
  title: 'Widgets/Login',
  component: LoginApp,
  argTypes: {
    username: {type: 'string', defaultValue: 'tocco'},
    showTitle: {type: 'boolean', defaultValue: true},
    entryPage: {
      type: 'string',
      control: 'select',
      options: ['login', 'username', 'password-request'],
      defaultValue: 'login'
    }
  }
}

export const LoginStory = ({...args}) => {
  const [confirmed, setConfirmed] = useState(false)
  return (
    <LoginApp
      key={uuid()}
      {...args}
      loginSuccess={() => {
        if (!confirmed) {
          const redirect = confirm('Login successful. Redirect to Entity-Browser?')
          if (redirect) {
            linkTo('Apps/Entity Browser', 'Entity Browser Story')()
          } else {
            setConfirmed(true)
          }
        }
      }}
    />
  )
}
