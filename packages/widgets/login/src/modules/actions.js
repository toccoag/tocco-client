export const INITIALIZE = 'login/INITIALIZE'
export const INITIALIZED = 'login/INITIALIZED'
export const LOGIN = 'login/LOGIN'
export const REQUEST_PASSWORD = 'login/REQUEST_PASSWORD'
export const CHECK_USERNAME = 'login/CHECK_USERNAME'

export const initialize = () => ({
  type: INITIALIZE
})

export const initialized = () => ({
  type: INITIALIZED
})

export const login = (username, password, captchaToken, userCode) => ({
  type: LOGIN,
  payload: {
    username,
    password,
    captchaToken,
    userCode
  }
})

export const requestPassword = username => ({
  type: REQUEST_PASSWORD,
  payload: {
    username
  }
})

export const checkUsername = username => ({
  type: CHECK_USERNAME,
  payload: {
    username
  }
})
