export const SET_MESSAGE = 'loginForm/SET_MESSAGE'
export const CLEAR_MESSAGE = 'loginForm/CLEAR_MESSAGE'
export const SET_PENDING = 'loginForm/SET_PENDING'
export const ACTIVATE_RECAPTCHA = 'loginForm/ACTIVATE_RECAPTCHA'

export const setMessage = (text, negative = false) => ({
  type: SET_MESSAGE,
  payload: {
    text,
    negative
  }
})

export const clearMessage = () => ({
  type: CLEAR_MESSAGE
})

export const setPending = (pending = false) => ({
  type: SET_PENDING,
  payload: {
    pending
  }
})

export const activateRecaptcha = () => ({
  type: ACTIVATE_RECAPTCHA
})
