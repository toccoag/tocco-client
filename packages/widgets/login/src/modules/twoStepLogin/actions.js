export const SET_SECRET = 'login/SET_SECRET'

export const setSecret = secret => ({
  type: SET_SECRET,
  payload: {
    secret
  }
})
