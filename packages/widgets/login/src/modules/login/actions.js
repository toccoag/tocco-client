export const CHANGE_PAGE = 'login/CHANGE_PAGE'
export const SET_ENTRY_PAGE = 'login/SET_ENTRY_PAGE'
export const SET_USERNAME = 'login/SET_USERNAME'
export const SET_PASSWORD = 'login/SET_PASSWORD'
export const SET_CAPTCHA_KEY = 'login/SET_CAPTCHA_KEY'

export const setEntryPage = entryPage => ({
  type: SET_ENTRY_PAGE,
  payload: {
    entryPage
  }
})
export const changePage = currentPage => ({
  type: CHANGE_PAGE,
  payload: {
    currentPage
  }
})

export const setUsername = username => ({
  type: SET_USERNAME,
  payload: {
    username
  }
})

export const setPassword = password => ({
  type: SET_PASSWORD,
  payload: {
    password
  }
})

export const setCaptchaKey = captchaKey => ({
  type: SET_CAPTCHA_KEY,
  payload: {
    captchaKey
  }
})
