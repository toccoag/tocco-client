import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const ACTION_HANDLERS = {
  [actions.SET_ENTRY_PAGE]: reducerUtil.singleTransferReducer('entryPage'),
  [actions.CHANGE_PAGE]: reducerUtil.singleTransferReducer('currentPage'),
  [actions.SET_USERNAME]: reducerUtil.singleTransferReducer('username'),
  [actions.SET_PASSWORD]: reducerUtil.singleTransferReducer('password'),
  [actions.SET_CAPTCHA_KEY]: reducerUtil.singleTransferReducer('captchaKey')
}

const initialState = {
  entryPage: undefined,
  currentPage: undefined,
  username: '',
  password: '',
  captchaKey: null
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
