import {takeLatest, select, all} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('login', () => {
  describe('modules', () => {
    describe('passwordUpdate', () => {
      describe('validationRules', () => {
        describe('sagas', () => {
          describe('root saga', () => {
            test('should fork child sagas', () => {
              const generator = rootSaga()
              expect(generator.next().value).to.deep.equal(
                all([takeLatest(actions.FETCH_VALIDATION_RULES, sagas.fetchValidationRules)])
              )
              expect(generator.next().done).to.equal(true)
            })
          })

          describe('fetchValidationRules', () => {
            const locale = 'de-CH'
            const username = 'user1'

            const oldPasswordRules = [
              {
                name: 'PASSWORD_NOT_CHANGED',
                params: {},
                message: 'Das neue Passwort muss sich vom alten Passwort unterscheiden.'
              }
            ]
            const generalRules = [
              {
                name: 'RULE_LENGTH',
                params: {},
                message: 'Passwort hat die falsche Länge.'
              }
            ]
            const allRules = [...oldPasswordRules, ...generalRules]

            test('should load validation rules', () => {
              return expectSaga(sagas.fetchValidationRules)
                .provide([
                  [select(sagas.dialogSelector), {usernameOrPk: username, showOldPasswordField: true}],
                  [select(sagas.intlSelector), locale],
                  [matchers.call.fn(sagas.loadValidationRules), {rules: allRules}]
                ])
                .put(actions.setValidationRules(allRules))
                .run()
            })

            test('should ignore password changed rule without old password', () => {
              return expectSaga(sagas.fetchValidationRules)
                .provide([
                  [select(sagas.dialogSelector), {usernameOrPk: username, showOldPasswordField: false}],
                  [select(sagas.intlSelector), locale],
                  [matchers.call.fn(sagas.loadValidationRules), {rules: allRules}]
                ])
                .put(actions.setValidationRules(generalRules))
                .run()
            })
          })
        })
      })
    })
  })
})
