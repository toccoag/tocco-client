import {takeLatest, call, put, select, all} from 'redux-saga/effects'
import {rest} from 'tocco-app-extensions'

import {RULE_PASSWORD_NOT_CHANGED} from '../password'

import * as actions from './actions'

export const dialogSelector = state => state.passwordUpdate.dialog
export const intlSelector = state => state.intl

export function* loadValidationRules(usernameOrPk, locale = '') {
  const resource = `principals/${usernameOrPk}/password-rules`

  const validationResponse = yield call(rest.requestSaga, resource, {queryParams: {locale}})
  return validationResponse.body
}

const isRelevant = (rule, showOldPasswordField) => {
  return showOldPasswordField || rule.name !== RULE_PASSWORD_NOT_CHANGED
}

export function* fetchValidationRules() {
  const {usernameOrPk, showOldPasswordField} = yield select(dialogSelector)
  const {locale} = yield select(intlSelector)

  const response = yield call(loadValidationRules, usernameOrPk, locale)
  const relevantRules = response.rules.filter(rule => isRelevant(rule, showOldPasswordField))
  yield put(actions.setValidationRules(relevantRules))
}

export default function* sagas() {
  yield all([takeLatest(actions.FETCH_VALIDATION_RULES, fetchValidationRules)])
}
