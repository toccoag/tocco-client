import {takeLatest, all, select} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, login} from 'tocco-app-extensions'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('login', () => {
  describe('modules', () => {
    describe('passwordUpdate', () => {
      describe('dialog', () => {
        describe('sagas', () => {
          describe('root saga', () => {
            test('should fork child sagas', () => {
              const generator = rootSaga()
              expect(generator.next().value).to.deep.equal(
                all([
                  takeLatest(actions.SET_CURRENT_USERNAME, sagas.setCurrentUsername),
                  takeLatest(actions.CHECK_CURRENT_USER, sagas.checkCurrentUser)
                ])
              )
              expect(generator.next().done).to.equal(true)
            })
          })

          describe('setCurrentUsername', () => {
            test('load current user and set username', () => {
              return expectSaga(sagas.setCurrentUsername)
                .provide([
                  [
                    matchers.call.fn(rest.requestSaga, 'principals', {acceptedStatusCodes: [403]}),
                    {body: {username: 'tocco'}}
                  ]
                ])
                .put(actions.setUsernameOrPk('tocco'))
                .run()
            })
          })

          describe('checkCurrentUser', () => {
            /**
             * welcome to the wonderful world of JavaScript,
             * where you have to type out the indentation of multiline strings by hand
             * because otherwise they'll copy the surrounding indentation as well
             */
            const getUserCheckCondition = usernameOrKeyCondition => {
              return (
                'username == :currentUsername\n' +
                '        and (\n' +
                `          ${usernameOrKeyCondition}\n` +
                '          or not exists(relLogin_role.relRole where unique_id == "usermanager")\n' +
                '        )'
              )
            }

            test('should set needsCaptcha true if principal was found', () => {
              return expectSaga(sagas.checkCurrentUser)
                .provide([
                  [select(sagas.usernameOrPkSelector), 'principal name'],
                  [matchers.call.fn(rest.fetchEntityCount), 1],
                  [matchers.call.fn(login.doRequest), {username: 'logged in'}]
                ])
                .call(rest.fetchEntityCount, 'Principal', {
                  where: getUserCheckCondition('username == "principal name"')
                })
                .put(actions.setNeedsCaptcha(true))
                .run()
            })
            test('should set needsCaptcha false if principal was not found', () => {
              return expectSaga(sagas.checkCurrentUser)
                .provide([
                  [select(sagas.usernameOrPkSelector), 'principal name'],
                  [matchers.call.fn(rest.fetchEntityCount), 0],
                  [matchers.call.fn(login.doRequest), {username: 'logged in'}]
                ])
                .call(rest.fetchEntityCount, 'Principal', {
                  where: getUserCheckCondition('username == "principal name"')
                })
                .put(actions.setNeedsCaptcha(false))
                .run()
            })
            test('should check key if one was given', () => {
              return expectSaga(sagas.checkCurrentUser)
                .provide([
                  [select(sagas.usernameOrPkSelector), '123'],
                  [matchers.call.fn(rest.fetchEntityCount), 0],
                  [matchers.call.fn(login.doRequest), {username: 'logged in'}]
                ])
                .call(rest.fetchEntityCount, 'Principal', {
                  where: getUserCheckCondition('pk == 123')
                })
                .run()
            })
            test('should require captcha for anonymous', () => {
              return expectSaga(sagas.checkCurrentUser)
                .provide([[matchers.call.fn(login.doRequest), {username: 'anonymous'}]])
                .not.call.fn(rest.fetchEntityCount)
                .put(actions.setNeedsCaptcha(true))
                .run()
            })
          })
        })
      })
    })
  })
})
