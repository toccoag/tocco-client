import {combineReducers} from 'redux'

import login from './login'
import loginForm from './loginForm'
import passwordUpdateReducers, {sagas as passwordUpdateSagas} from './passwordUpdate/reducers'
import mainSagas from './sagas'
import twoStepLogin from './twoStepLogin'

export default {
  login,
  loginForm,
  passwordUpdate: combineReducers(passwordUpdateReducers),
  twoStepLogin
}

export const sagas = [mainSagas, ...passwordUpdateSagas]
