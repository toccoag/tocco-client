import PropTypes from 'prop-types'
import {appFactory, externalEvents} from 'tocco-app-extensions'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {env, appContext, consoleLogger} from 'tocco-util'

import UserAvailability from './components/UserAvailability'

const packageName = 'user-availability'

const EXTERNAL_EVENTS = ['onVisibilityStatusChange']

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <UserAvailability />

  const store = appFactory.createStore({}, null, input, packageName)

  externalEvents.addToStore(store, state => appFactory.getEvents(EXTERNAL_EVENTS, state.input))

  return appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: [],
    publicPath,
    textResourceModules: ['component', 'common', 'actions', packageName]
  })
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {component} = initApp(packageName, input)

      appFactory.renderApp(component)
    }
  }
})()

const UserAvailabilityApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

UserAvailabilityApp.propTypes = {
  formBase: PropTypes.string.isRequired,
  allowCreate: PropTypes.bool,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  searchFormType: searchFormTypePropTypes,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContext.propTypes.isRequired,
  ...externalEvents.createPropTypes(EXTERNAL_EVENTS)
}

export default UserAvailabilityApp
export const app = appFactory.createBundleableApp(packageName, initApp, UserAvailabilityApp)
