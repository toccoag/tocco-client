import PropTypes from 'prop-types'
import {form} from 'tocco-app-extensions'
import EntityBrowserApp from 'tocco-entity-browser/src/main'
import {searchFormTypePropTypes} from 'tocco-entity-list/src/main'
import {appContext as appContextPropType} from 'tocco-util'

const UserAvailability = ({
  formBase,
  allowCreate,
  reportIds,
  searchFilters,
  limit,
  searchFormType,
  backendUrl,
  businessUnit,
  appContext,
  intl,
  onVisibilityStatusChange,
  locale
}) => {
  const modifyFormDefinition = formDefinition => {
    if (allowCreate) {
      formDefinition = form.addCreate(formDefinition, intl)
    } else {
      formDefinition = form.removeActions(formDefinition, ['new', 'copy'])
    }

    return formDefinition
  }

  return (
    <EntityBrowserApp
      entityName="Availability"
      formBase={formBase}
      searchFilters={searchFilters}
      limit={limit}
      searchFormType={searchFormType}
      modifyFormDefinition={modifyFormDefinition}
      backendUrl={backendUrl}
      businessUnit={businessUnit}
      appContext={appContext}
      reportIds={reportIds}
      onVisibilityStatusChange={onVisibilityStatusChange}
      locale={locale}
    />
  )
}

UserAvailability.propTypes = {
  formBase: PropTypes.string.isRequired,
  allowCreate: PropTypes.bool,
  reportIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  searchFilters: PropTypes.arrayOf(PropTypes.string),
  limit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  searchFormType: searchFormTypePropTypes,
  intl: PropTypes.object.isRequired,
  backendUrl: PropTypes.string,
  businessUnit: PropTypes.string,
  appContext: appContextPropType.propTypes.isRequired,
  onVisibilityStatusChange: PropTypes.func.isRequired,
  locale: PropTypes.string
}

export default UserAvailability
