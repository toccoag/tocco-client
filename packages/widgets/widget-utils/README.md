# Widget Utils

Contains tools that are needed to let our apps run in external websites.

## bootstrap

The `bootstrap` script finds all the widget containers in the website and renders the corresponding application.

### Import

```html
<script src="https://${customer}.tocco.ch/js/tocco-widget-utils/dist/bootstrap.js"/>
```

### Embed widgets

To embed a widget in the website, just add a `<div>` container and decorate it with the necessary attributes.

#### Attributes

- `data-tocco-widget-key` (required): The key of the widget config

#### Example

```html
<div data-tocco-widget-key="12"></div>
<script src="https://${customer.tocco.ch}/js/tocco-widget-utils/dist/bootstrap.js"></script>
```

### Events
A widget can provide events which will be invoked. When a widget gets rendered the event handlers which are defined on the `window.toccoEventHandlers` will be attached to the widget events.

Use an flat object of functions which will be used for each widget on that page. Preferably only used if only one widget is emebedded.
```js
window.toccoEventHandlers = {
  loginSuccess: () => {}
}
```

The widget key is used as property name on the `toccoEventHandlers` obj to attach all event handlers for each widget. Preferable used if multiple widgets are embedded.
```html
<div data-tocco-widget-key="12"></div>
```

```js
window.toccoEventHandlers = {
  '12': {
    loginSuccess: () => {}
  }
}
```

### Methods
A widget can provide methods which can be invoked to communicate with the widget itself.
When a widget gets rendered these methods will be attached to `window.toccoMethods` to provide them to outer scripts.

The widget key is used as a property name on the `window.toccoMethods` object.

#### Example
```html
<div data-tocco-widget-key="12"></div>
```
```js
// use methods via `window.toccoMethods` object
window.toccoMethods['12'].setLocale('fr-CH');
```

### Theme

A theme can be configured by defining a global theme object on `window.toccoTheme`.
```js
window.toccoTheme = {
  colors: {
    primary: '#BB8800'
  },
  fontSize: {
    base: 1.3
  }
}
```

### Visibitlity Status
A widget can be in different visibility status. Dependend on the status other html elements can be shown.

In order to show an element dependend on a visibility status it has to have two attributes:
- `data-tocco-widget-ref` to define which widget it should listen to 
- `data-tocco-visibility-status` to define the status of the widget to show the element as whitespace-separated list

```html
<div data-tocco-widget-key="12"></div>

<div data-tocco-widget-ref="12" data-tocco-visibility-status="success">Erfolgreich.</div>

<div data-tocco-widget-ref="12" data-tocco-visibility-status="list detail">...</div>
```
