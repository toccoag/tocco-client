import _isEqual from 'lodash/isEqual'
import {consoleLogger} from 'tocco-util'

import {TQL_FUNC_NAME, TQL_META_OBJ_NAME} from '../utils/constants'
import {enhanceExtractedBody, executeRequest} from '../utils/requests'
import {getVersion} from '../utils/utils'

const makeTqlFunc = params => {
  const {backendUrl} = params

  /**
   * Executes a tql request to the corresponding tocco instance with users auth cookie.
   *
   * @param {string} entityModel - `entityModel` which the search request should be executed on
   * @param {Object} options - available tql options
   *
   * @example
   * ```
   * tql('User', {
   *  'limit': 25,
   *  'offset': 0,
   *  'sort': 'update_timestamp desc',
   *  'paths': ['user_nr', 'firstname','lastname'],
   *  'where': 'relUser_status.unique_id != "archive"'
   * })
   * ```
   */
  return async (entityModel, options) => {
    const requestOptions = {
      method: 'POST',
      body: JSON.stringify(options)
    }
    const response = await executeRequest(
      `${backendUrl}/nice2/rest/entities/2.0/${entityModel}/search?_omitLinks=true`,
      requestOptions
    )
      .then(enhanceExtractedBody)
      .then(resp => resp?.body)
      .catch(error => {
        consoleLogger.logError(`Could not execute tql.`, error)
      })
    return response
  }
}

export const setupTql = ({backendUrl, assetUrl}) => {
  const version = getVersion()
  const tqlMetaObj = {version, backendUrl, assetUrl}
  const tqlFunc = makeTqlFunc(tqlMetaObj)

  const existingTqlMetaObj = window[TQL_META_OBJ_NAME]
  if (existingTqlMetaObj) {
    consoleLogger.log('tocco-widget-utils - tql function is already initialized')
    const mismatch = !_isEqual(existingTqlMetaObj, tqlMetaObj)
    if (mismatch) {
      consoleLogger.logError(
        'More than one tql function with different settings has been found. Add only one tql function per page.',
        '\nInitialized tql function:',
        existingTqlMetaObj,
        '\nIgnored tql function:',
        tqlMetaObj
      )
    }
    return
  }

  window[TQL_FUNC_NAME] = tqlFunc
  window[TQL_META_OBJ_NAME] = tqlMetaObj
  consoleLogger.log(`tocco-widget-utils - v${version} - tql`)
}
