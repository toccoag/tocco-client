import {extractScriptParams} from '../utils/utils'

import {setupTql} from './tql'
;(() => {
  const params = extractScriptParams()
  setupTql(params)
})()
