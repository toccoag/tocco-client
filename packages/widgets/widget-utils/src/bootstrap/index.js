import {consoleLogger} from 'tocco-util'
import {utils} from 'tocco-util/bundle'

import {setupTql} from '../tql/tql'
import {extractScriptParams} from '../utils/utils'

import bootstrapVisibilityStatus from './bootstrapVisibilityStatus'
import bootstrapWidgets from './bootstrapWidgets'
;(() => {
  const params = extractScriptParams()

  bootstrapVisibilityStatus(params)
  setupTql(params)

  // guarantee to initalize all widgets on the page
  document.addEventListener('DOMContentLoaded', async () => {
    if (!window.React || !window.reactRegistry) {
      await utils.loadScriptAsync(`${params.assetUrl}/nice2/javascript/nice2-react.release.js`)
    } else {
      consoleLogger.log(`tocco-widget-utils - React and react-registry already loaded - v${window.React.version}`)
    }
    await bootstrapWidgets(params)
  })
})()
