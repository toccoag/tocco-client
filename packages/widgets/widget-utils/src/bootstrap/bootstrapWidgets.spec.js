import fetchMock from 'fetch-mock'
import {testingLibrary} from 'tocco-test-util'
import {utils} from 'tocco-util/bundle'

import {BOOTSTRAP_SCRIPT_OBJ_NAME, EVENT_HANDLERS_OBJ_NAME, THEME_OBJ_NAME} from '../utils/constants'

import bootstrapWidgets from './bootstrapWidgets'

let stub

const srcPath = 'http://localhost:8080/js/tocco-login/dist/'
const srcPathWidgetUtils = 'http://localhost:8080/js/tocco-widget-utils/dist/'
const applyFetchMockForLoginWidget = (config = {}) => {
  fetchMock
    .get('http://localhost:8080/nice2/rest/widget/configs/1', {
      key: '1',
      appName: 'login',
      packageName: 'login',
      locale: 'de',
      config
    })
    .spy()
}

const backendUrl = 'http://localhost:8080'
const assetUrl = 'http://localhost:8080'
const expectedWidgetPlaceholderInput = {
  backendUrl,
  placeholder: 'error',
  appContext: {embedType: 'widget'}
}

const expectWidgetPlaceholderIsRendered = (renderSpy, container) => {
  expect(renderSpy).to.have.been.calledWithMatch(
    'widget-placeholder',
    container,
    '1',
    expectedWidgetPlaceholderInput,
    {},
    srcPathWidgetUtils
  )
}

describe('widget-utils', () => {
  describe('boostrap', () => {
    describe('bootstrapWidgets', () => {
      beforeEach(() => {
        stub = sinon.stub(utils, 'loadScriptAsync').returns({})
        fetchMock.hardReset()
        fetchMock.mockGlobal()
      })

      afterEach(() => {
        if (stub) {
          stub.restore()
        }

        delete window[THEME_OBJ_NAME]
        delete window[EVENT_HANDLERS_OBJ_NAME]
        delete window[BOOTSTRAP_SCRIPT_OBJ_NAME]
        delete window.reactRegistry
      })

      test('should initialize and render widget', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        const config = {
          showTitle: true,
          passwordRequest: false,
          username: 'test-username'
        }
        applyFetchMockForLoginWidget(config)

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        const expectedInput = {
          backendUrl,
          locale: 'de',
          showTitle: true,
          passwordRequest: false,
          username: 'test-username',
          appContext: {embedType: 'widget', widgetConfigKey: '1'}
        }
        expect(renderSpy).to.have.been.calledWithMatch('login', container, '', expectedInput, {}, srcPath)
        expect(window[BOOTSTRAP_SCRIPT_OBJ_NAME].backendUrl).to.eql(backendUrl)
        expect(window[BOOTSTRAP_SCRIPT_OBJ_NAME].assetUrl).to.eql(assetUrl)
      })

      test('should not execute when bootstrap script has run before', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }
        window[BOOTSTRAP_SCRIPT_OBJ_NAME] = {version: '1.0'}

        fetchMock.spy()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(0)

        expect(renderSpy).to.not.have.been.called
      })

      test('should ignore bootstrap script when bootstrap script has been initialized', async () => {
        window[BOOTSTRAP_SCRIPT_OBJ_NAME] = {version: '1.0', backendUrl: 'foo.ch'}

        await bootstrapWidgets({backendUrl})

        expect(window[BOOTSTRAP_SCRIPT_OBJ_NAME].backendUrl).to.eql('foo.ch')
      })

      test('should initialize bootstrap script', async () => {
        testingLibrary.renderWithTheme(<div></div>)

        await bootstrapWidgets({backendUrl})

        expect(window[BOOTSTRAP_SCRIPT_OBJ_NAME]).to.not.be.undefined
        expect(window[BOOTSTRAP_SCRIPT_OBJ_NAME].version).to.not.be.undefined
      })

      test('should apply tocco theme as input parameter', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }
        window[THEME_OBJ_NAME] = {fontSize: 30}

        applyFetchMockForLoginWidget()

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        const expectedInput = {
          backendUrl,
          customTheme: window[THEME_OBJ_NAME],
          locale: 'de',
          appContext: {embedType: 'widget', widgetConfigKey: '1'}
        }
        expect(renderSpy).to.have.been.calledWithMatch('login', container, '', expectedInput, {}, srcPath)
      })

      test('should apply event handlers', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }
        window[EVENT_HANDLERS_OBJ_NAME] = {someEvent: () => {}}

        applyFetchMockForLoginWidget()

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        const expectedInput = {
          backendUrl,
          locale: 'de',
          appContext: {embedType: 'widget', widgetConfigKey: '1'}
        }
        expect(renderSpy).to.have.been.calledWithMatch(
          'login',
          container,
          '',
          expectedInput,
          window[EVENT_HANDLERS_OBJ_NAME],
          srcPath
        )
      })

      test('should invoke onVisibilityStatusChange event handlers', async () => {
        const renderMock = (_apName, _container, _id, input, eventHandlers, _srcPath) => {
          input.onVisibilityStatusChange({status: ['list']})
          eventHandlers.onVisibilityStatusChange({status: ['detail']})
        }
        const onVisibilityStatusChangeSpy = sinon.spy()
        window.reactRegistry = {
          render: renderMock
        }
        window[EVENT_HANDLERS_OBJ_NAME] = {onVisibilityStatusChange: onVisibilityStatusChangeSpy}

        applyFetchMockForLoginWidget()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        expect(onVisibilityStatusChangeSpy).to.have.been.calledWith({status: ['list']})
        expect(onVisibilityStatusChangeSpy).to.have.been.calledWith({status: ['detail']})
      })

      test('should consider package name', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            key: '1',
            appName: 'password-update',
            packageName: 'login',
            locale: 'de',
            config: {}
          })
          .spy()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        expect(renderSpy).to.have.been.calledWith(
          'password-update',
          sinon.match.any,
          sinon.match.any,
          sinon.match.any,
          sinon.match.any,
          srcPath
        )
      })

      test('should add locale as input parameter', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            key: '1',
            appName: 'password-update',
            packageName: 'login',
            locale: 'de',
            config: {}
          })
          .spy()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(1)

        const expectedInput = {
          backendUrl,
          locale: 'de',
          appContext: {embedType: 'widget', widgetConfigKey: '1'}
        }
        expect(renderSpy).to.have.been.calledWith(
          sinon.match.any,
          sinon.match.any,
          sinon.match.any,
          sinon.match(expectedInput),
          sinon.match.any,
          sinon.match.any
        )
      })

      test('should handle 403 errors', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            status: 403,
            body: {
              status: 403,
              errorCode: 'INVALID_DOMAIN',
              message: 'widget embedded on invalid domain'
            }
          })
          .spy()

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(2)

        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=widget%20embedded%20on%20invalid%20domain'
          )
        ).to.equal(true)

        expectWidgetPlaceholderIsRendered(renderSpy, container)
      })

      test('should handle 404 errors', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            status: 404,
            body: {}
          })
          .spy()

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(2)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Widget%20config%20%271%27%20not%20found.'
          )
        ).to.equal(true)

        expectWidgetPlaceholderIsRendered(renderSpy, container)
      })

      test('should handle network errors', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            throws: new Error('Failed to fetch')
          })
          .spy()

        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(2)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Could%20not%20fetch%20widget%20config%20%271%27'
          )
        ).to.equal(true)

        expectWidgetPlaceholderIsRendered(renderSpy, container)
      })

      test('should ignore remote log errors', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        fetchMock
          .get('http://localhost:8080/nice2/rest/widget/configs/1', {
            throws: new Error('Failed to fetch')
          })
          .post('begin:http://localhost:8080/nice2/log', {
            throws: new Error('Failed to fetch')
          })
          .spy()
        const {container: wrapper} = testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)
        const container = wrapper.firstChild

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(2)

        expectWidgetPlaceholderIsRendered(renderSpy, container)
        // should not fail with UnhandledPromiseRejection
      })

      test('should handle errors while render app', async () => {
        const renderFailure = sinon.stub().throws()
        window.reactRegistry = {
          render: renderFailure
        }

        applyFetchMockForLoginWidget()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(3)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Could%20not%20render%20app%20%27login%27.'
          )
        ).to.equal(true)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Could%20not%20render%20app%20%27widget-placeholder%27.'
          )
        ).to.equal(true)

        expect(renderFailure).to.have.been.called
        // should not fail with unhandled error
      })
    })

    describe('bootstrapWidgets', () => {
      beforeEach(() => {
        stub = sinon.stub(utils, 'loadScriptAsync').rejects()
        fetchMock.hardReset()
        fetchMock.mockGlobal()
      })

      afterEach(() => {
        if (stub) {
          stub.restore()
        }
      })

      test('should handle errors while fetching the package', async () => {
        const renderSpy = sinon.spy()
        window.reactRegistry = {
          render: renderSpy
        }

        applyFetchMockForLoginWidget()

        testingLibrary.renderWithTheme(<div data-tocco-widget-key="1"></div>)

        await bootstrapWidgets({backendUrl, assetUrl})

        await fetchMock.callHistory.flush()
        expect(fetchMock.callHistory.callLogs.length).to.equal(3)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Could%20not%20fetch%20package%20%27tocco-login%27.'
          )
        ).to.equal(true)
        expect(
          fetchMock.callHistory.called(
            'begin:http://localhost:8080/nice2/log?level=error&message=Could%20not%20fetch%20package%20%27tocco-widget-utils%27.'
          )
        ).to.equal(true)

        expect(renderSpy).to.not.have.been.called
        // should not fail with unhandled error
      })
    })
  })
})
