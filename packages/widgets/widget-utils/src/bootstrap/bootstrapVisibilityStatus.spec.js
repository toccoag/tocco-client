import bootstrapVisibilityStatus, {setGlobalStyles, setVisibilityStatusStyles} from './bootstrapVisibilityStatus'

describe('widget-utils', () => {
  describe('boostrap', () => {
    describe('bootstrapVisibilityStatus', () => {
      const backendUrl = 'http://localhost:8080'
      const widgetKey = '23'
      const params = {backendUrl}

      afterEach(() => {
        document.getElementsByTagName('html')[0].innerHTML = ''
      })

      describe('bootstrapVisibilityStatus', () => {
        test('should set global initial styles for visibility status', () => {
          bootstrapVisibilityStatus(params)

          expect(document.head.innerHTML).to.eql(
            '<style id="tocco-initial-visibility-status-styles">[data-tocco-visibility-status] {display: none;}</style>'
          )
        })
      })

      describe('setVisibilityStatusStyles', () => {
        test('should set visibility status styles for multiple status', () => {
          const widgetStates = ['list', 'advanced-search']

          setVisibilityStatusStyles(backendUrl, widgetKey, widgetStates)

          // eslint-disable-next-line max-len
          const expectedStyles = `<style id="tocco-visibility-status-styles-23">[data-tocco-widget-ref="23"][data-tocco-visibility-status~="list"],
[data-tocco-widget-ref="23"][data-tocco-visibility-status~="advanced-search"] {
  display: inherit;
}</style>`
          expect(document.head.innerHTML).to.eql(expectedStyles)
        })

        test('should set visibility status styles for single status', () => {
          const widgetStates = ['list']

          setVisibilityStatusStyles(backendUrl, widgetKey, widgetStates)

          // eslint-disable-next-line max-len
          const expectedStyles = `<style id="tocco-visibility-status-styles-23">[data-tocco-widget-ref="23"][data-tocco-visibility-status~="list"] {
  display: inherit;
}</style>`
          expect(document.head.innerHTML).to.eql(expectedStyles)
        })

        test('should overwrite existing visibility status style', () => {
          const widgetStates = ['list']
          const initialStyle = 'body {color: red}'
          setGlobalStyles(backendUrl, `tocco-visibility-status-styles-${widgetKey}`, initialStyle)
          expect(document.head.innerHTML).to.eql(
            `<style id="tocco-visibility-status-styles-23">${initialStyle}</style>`
          )

          setVisibilityStatusStyles(backendUrl, widgetKey, widgetStates)

          // eslint-disable-next-line max-len
          const expectedStyles = `<style id="tocco-visibility-status-styles-23">[data-tocco-widget-ref="23"][data-tocco-visibility-status~="list"] {
  display: inherit;
}</style>`
          expect(document.head.innerHTML).to.eql(expectedStyles)
        })

        test('should keep existing visibility status style from other widgets', () => {
          const widgetStates = ['list']
          const initialStyle = 'body {color: red}'
          setGlobalStyles(backendUrl, `tocco-visibility-status-styles-44`, initialStyle)
          expect(document.head.innerHTML).to.eql(
            `<style id="tocco-visibility-status-styles-44">${initialStyle}</style>`
          )

          setVisibilityStatusStyles(backendUrl, widgetKey, widgetStates)

          // eslint-disable-next-line max-len
          const expectedStyles = `<style id="tocco-visibility-status-styles-44">${initialStyle}</style><style id="tocco-visibility-status-styles-23">[data-tocco-widget-ref="23"][data-tocco-visibility-status~="list"] {
  display: inherit;
}</style>`
          expect(document.head.innerHTML).to.eql(expectedStyles)
        })

        test('should initial visibility status style', () => {
          const widgetStates = ['list']
          bootstrapVisibilityStatus(params)

          setVisibilityStatusStyles(backendUrl, widgetKey, widgetStates)

          // eslint-disable-next-line max-len
          const expectedStyles = `<style id="tocco-initial-visibility-status-styles">[data-tocco-visibility-status] {display: none;}</style><style id="tocco-visibility-status-styles-23">[data-tocco-widget-ref="23"][data-tocco-visibility-status~="list"] {
  display: inherit;
}</style>`
          expect(document.head.innerHTML).to.eql(expectedStyles)
        })
      })
    })
  })
})
