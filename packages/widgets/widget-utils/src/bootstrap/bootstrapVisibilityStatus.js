import {
  ATTRIBUTE_WIDGET_REF,
  ATTRIBUTE_VISIBILITY_STATUS,
  ID_INITIAL_VISIBILITY_STATUS_STYLES,
  ID_VISIBILITY_STATUS_STYLES
} from '../utils/constants'
import * as remoteLogger from '../utils/remoteLogger'

const createStyleElement = id => {
  const style = document.createElement('style')
  style.id = id
  document.head.appendChild(style)
  return style
}

export const setGlobalStyles = (backendUrl, id, css) => {
  try {
    const style = document.getElementById(id) || createStyleElement(id)
    style.innerHTML = css
  } catch (error) {
    remoteLogger.logException(backendUrl, `Could not apply global styles.`, error)
  }
}

export const setVisibilityStatusStyles = (backendUrl, widgetKey, visibilityStatus) => {
  const selector = visibilityStatus
    .map(status => `[${ATTRIBUTE_WIDGET_REF}="${widgetKey}"][${ATTRIBUTE_VISIBILITY_STATUS}~="${status}"]`)
    .join(',\n')
  const css = `${selector} {
  display: inherit;
}`
  setGlobalStyles(backendUrl, `${ID_VISIBILITY_STATUS_STYLES}-${widgetKey}`, css)
}

const bootstrapVisibilityStatus = params => {
  const {backendUrl} = params

  const css = `[${ATTRIBUTE_VISIBILITY_STATUS}] {display: none;}`
  const id = ID_INITIAL_VISIBILITY_STATUS_STYLES
  setGlobalStyles(backendUrl, id, css)
}

export default bootstrapVisibilityStatus
