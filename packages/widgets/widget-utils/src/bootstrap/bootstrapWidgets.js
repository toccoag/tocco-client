import _isEqual from 'lodash/isEqual'
import {consoleLogger} from 'tocco-util'
import {utils} from 'tocco-util/bundle'

import {
  ATTRIBUTE_WIDGET_KEY,
  BOOTSTRAP_SCRIPT_OBJ_NAME,
  ERROR_CODE_INACTIVE,
  ERROR_CODE_INVALID_DOMAIN
} from '../utils/constants'
import * as remoteLogger from '../utils/remoteLogger'
import {executeRequest, enhanceExtractedBody} from '../utils/requests'
import {attachMethods, getEventHandlers, getTheme, getVersion} from '../utils/utils'
import {renderErrorWidgetPlaceholder} from '../utils/widgetPlaceholder'

import {setVisibilityStatusStyles} from './bootstrapVisibilityStatus'

const getWidgetKey = container => container.getAttribute(ATTRIBUTE_WIDGET_KEY)

const getDocsUrlForError = () => {
  const version = getVersion()
  const subdomain = version
    .split('.')
    .slice(0, 2)
    // always use two digits (01 vs 1) except for the first number
    .map((d, index) => (index === 0 ? d : d.padStart(2, '0')))
    .join('')
  return `https://${subdomain}.docs.tocco.ch/Web/widgets/Web_Einbindung_Tocco_Widgets_Fehlermeldungen.html`
}

const handleRequestError = (backendUrl, response, key) => {
  const {ok, status, body} = response
  if (!ok) {
    if (status === 403 && body?.errorCode === ERROR_CODE_INVALID_DOMAIN) {
      remoteLogger.logError(backendUrl, `${body.message}. Visit ${getDocsUrlForError()} for further information.`)
    }

    if (status === 403 && body?.errorCode === ERROR_CODE_INACTIVE) {
      remoteLogger.logError(backendUrl, `${body.message}. Visit ${getDocsUrlForError()} for further information.`)
    }

    if (status === 404) {
      remoteLogger.logError(
        backendUrl,
        `Widget config '${key}' not found. Visit ${getDocsUrlForError()} for further information.`
      )
    }

    return undefined
  }
  return response
}

const makeHandleVisibilityStatusChange =
  (backendUrl, widgetKey, eventHandler) =>
  (...args) => {
    const [{status}] = args
    setVisibilityStatusStyles(backendUrl, widgetKey, status)
    if (typeof eventHandler === 'function') {
      eventHandler(...args)
    }
  }

const initializeWidget = async (backendUrl, assetUrl, container) => {
  const key = getWidgetKey(container)
  const widgetConfig = await executeRequest(`${backendUrl}/nice2/rest/widget/configs/${key}`)
    .then(enhanceExtractedBody)
    .then(response => handleRequestError(backendUrl, response, key))
    .then(response => response?.body)
    .catch(error => {
      remoteLogger.logException(
        backendUrl,
        `Could not fetch widget config '${key}'. Visit ${getDocsUrlForError()} for further information.`,
        error
      )
    })

  if (widgetConfig) {
    const {appName, packageName, locale, config} = widgetConfig

    try {
      await utils.loadScriptAsync(`${assetUrl}${utils.getEntryFilePath(packageName, appName)}`)
    } catch (error) {
      remoteLogger.logException(
        backendUrl,
        `Could not fetch package 'tocco-${packageName}'. Visit ${getDocsUrlForError()} for further information.`,
        error
      )
      await renderErrorWidgetPlaceholder(backendUrl, assetUrl, container, key, locale)
      return
    }

    const externalEventHandlers = getEventHandlers(key)
    const eventHandlers = {
      ...externalEventHandlers,
      onVisibilityStatusChange: makeHandleVisibilityStatusChange(
        backendUrl,
        key,
        externalEventHandlers.onVisibilityStatusChange
      )
    }
    const customTheme = getTheme()
    const input = {
      backendUrl,
      ...(customTheme ? {customTheme} : {}),
      locale,
      ...config,
      appContext: {
        embedType: 'widget',
        widgetConfigKey: key
      },
      ...eventHandlers
    }
    const srcPath = `${assetUrl}/js/tocco-${packageName}/dist/`

    try {
      const methods = window.reactRegistry.render(appName, container, key, input, eventHandlers, srcPath)
      attachMethods(key, methods)
    } catch (error) {
      remoteLogger.logException(
        backendUrl,
        `Could not render app '${appName}'. Visit ${getDocsUrlForError()} for further information.`,
        error
      )
      await renderErrorWidgetPlaceholder(backendUrl, assetUrl, container, key, locale)
    }
  } else {
    const htmlLocale = document.documentElement.lang
    await renderErrorWidgetPlaceholder(backendUrl, assetUrl, container, key, htmlLocale)
  }
}

/**
 * Widgets should get initialized only once.
 * Sets global variable to block other embedded bootstrap scripts from execution.
 * Only the first bootstrap script gets executed.
 *
 * @returns boolean to indicate whether script can be executed
 */
const initializeBootstrap = params => {
  const version = getVersion()
  const bootstrapScriptObj = {version, ...params}

  const existingBootstrapScriptObj = window[BOOTSTRAP_SCRIPT_OBJ_NAME]
  if (existingBootstrapScriptObj) {
    consoleLogger.log('tocco-widget-utils - bootstrap script is already initialized')

    const mismatch = !_isEqual(existingBootstrapScriptObj, bootstrapScriptObj)
    if (mismatch) {
      /* eslint-disable max-len */
      consoleLogger.logError(
        'More than one bootstrap script with different settings has been found. Add only one bootstrap script per page.',
        '\nInitialized bootstrap script:',
        existingBootstrapScriptObj,
        '\nIgnored bootstrap script:',
        bootstrapScriptObj
      )
      /* eslint-enable max-len */
    }

    return false
  }

  window[BOOTSTRAP_SCRIPT_OBJ_NAME] = bootstrapScriptObj
  consoleLogger.log(`tocco-widget-utils - v${version} - bootstrap`)

  return true
}

const bootstrapWidgets = async params => {
  const canExecute = initializeBootstrap(params)
  if (!canExecute) {
    return
  }

  const {backendUrl, assetUrl} = params

  const widgetContainerNodeList = document.querySelectorAll(`[${ATTRIBUTE_WIDGET_KEY}]`)
  const widgetContainers = Array.prototype.slice.call(widgetContainerNodeList)

  await Promise.all(widgetContainers.map(container => initializeWidget(backendUrl, assetUrl, container)))
}

export default bootstrapWidgets
