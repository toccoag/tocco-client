import {BACKEND_URL, EVENT_HANDLERS_OBJ_NAME, METHODS_OBJ_NAME, THEME_OBJ_NAME} from './constants'

export const snakeToCamel = s => s.replace(/(-\w)/g, m => m[1].toUpperCase())

export const transformObjectValue = val => {
  try {
    return JSON.parse(val)
  } catch (e) {
    return val
  }
}

const getLocation = jsFileSource => (jsFileSource.startsWith('http') ? new URL(jsFileSource) : window.location)

export const getAssetUrl = document => {
  const jsFileSource = document.currentScript.getAttribute('src')
  const {protocol, host} = getLocation(jsFileSource)
  return protocol + '//' + host
}

export const extractScriptParams = () => {
  const assetUrl = getAssetUrl(document)
  let backendUrl
  if (window[BACKEND_URL]) {
    backendUrl = window[BACKEND_URL]
  } else {
    backendUrl = assetUrl
  }

  const params = {
    backendUrl,
    assetUrl
  }
  return params
}

export const buildInputFromDom = widgetContainer => {
  const attributes = Array.prototype.slice.call(widgetContainer.attributes)
  return attributes.reduce(
    (acc, val) => ({
      ...acc,
      [snakeToCamel(val.name.replace('data-', ''))]: transformObjectValue(val.value)
    }),
    {}
  )
}

export const getTheme = () => {
  const theme = window[THEME_OBJ_NAME]
  if (theme && typeof theme === 'object') {
    return theme
  }

  return undefined
}

export const isMapOfFunctions = obj =>
  obj && Object.keys(obj).length > 0 && typeof obj[Object.keys(obj)[0]] === 'function'

export const getEventHandlers = key => {
  const handlers = window[EVENT_HANDLERS_OBJ_NAME]
  if (isMapOfFunctions(handlers)) {
    return handlers
  }

  if (handlers) {
    const handlersOfId = handlers[key]
    if (isMapOfFunctions(handlersOfId)) {
      return handlersOfId
    }
  }

  return {}
}

export const attachMethods = (key, methods) => {
  window[METHODS_OBJ_NAME] = {
    ...window[METHODS_OBJ_NAME],
    [key]: methods
  }
}

export const getVersion = () => {
  const json = require('../../package.json')
  return json.version
}
