import {EVENT_HANDLERS_OBJ_NAME, METHODS_OBJ_NAME, THEME_OBJ_NAME} from './constants'
import {attachMethods, buildInputFromDom, getAssetUrl, getEventHandlers, getTheme} from './utils'

describe('widget-utils', () => {
  describe('bootstrap', () => {
    describe('getAssetUrl', () => {
      test('should derive backend url from script source url', () => {
        const document = {
          currentScript: {
            getAttribute: name =>
              name === 'src' ? 'https://customer.tocco.ch/js/tocco-widget-utils/dist/bootstrap.js' : undefined
          }
        }
        expect(getAssetUrl(document)).to.eql('https://customer.tocco.ch')
      })

      test('should derive absolute backend url from relative embedded script', () => {
        const document = {
          currentScript: {
            getAttribute: name => (name === 'src' ? '/js/tocco-widget-utils/dist/bootstrap.js' : undefined)
          }
        }
        expect(getAssetUrl(document)).to.eql('http://localhost')
      })
    })

    describe('buildInputFromDom', () => {
      test('should build input object from DOM attributes', () => {
        const container = {
          attributes: [
            {name: 'data-tocco-widget', value: 'entity-browser'},
            {name: 'data-entity-name', value: 'User'},
            {name: 'style', value: 'color: red;'},
            {name: 'data-form-base', value: 'User'},
            {name: 'data-memory-history', value: 'true'}
          ]
        }
        expect(buildInputFromDom(container)).to.eql({
          entityName: 'User',
          formBase: 'User',
          memoryHistory: true,

          // the following two aren't actually needed as input
          // but they also shouldn't do any harm either
          toccoWidget: 'entity-browser',
          style: 'color: red;'
        })
      })
    })

    describe('getTheme', () => {
      test('should return undefined if global theme is not defined', () => {
        delete window[THEME_OBJ_NAME]

        expect(getTheme()).to.be.undefined
      })

      test('should return undefined if global theme is not an object', () => {
        window[THEME_OBJ_NAME] = 'abc'

        expect(getTheme()).to.be.undefined
      })

      test('should return theme object', () => {
        window[THEME_OBJ_NAME] = {fontSize: 30}

        expect(getTheme()).to.eql(window[THEME_OBJ_NAME])
      })
    })

    describe('getEventHandlers', () => {
      test('should return empty object if global map not defined', () => {
        delete window[EVENT_HANDLERS_OBJ_NAME]
        const widgetKey = '23'

        expect(getEventHandlers(widgetKey)).to.eql({})
      })

      test('should return event handlers if global map is defined (one-dimensional)', () => {
        window[EVENT_HANDLERS_OBJ_NAME] = {
          loginSuccess: () => {},
          resize: () => {}
        }
        const widgetKey = '23'

        expect(getEventHandlers(widgetKey)).to.eql(window[EVENT_HANDLERS_OBJ_NAME])
      })

      test('should return event handlers for key (two-dimensional)', () => {
        window[EVENT_HANDLERS_OBJ_NAME] = {
          23: {
            loginSuccess: () => {},
            resize: () => {}
          }
        }
        const widgetKey = '23'

        expect(getEventHandlers(widgetKey)).to.eql(window[EVENT_HANDLERS_OBJ_NAME]['23'])
      })

      test('should return empty object if no handlers defined for widget key', () => {
        window[EVENT_HANDLERS_OBJ_NAME] = {
          45: {
            loginSuccess: () => {},
            resize: () => {}
          }
        }
        const widgetKey = '23'

        expect(getEventHandlers(widgetKey)).to.eql({})
      })
    })

    describe('attachMethods', () => {
      beforeEach(() => {
        delete window[METHODS_OBJ_NAME]
      })

      test('should create obj on window', () => {
        const widgetKey = '23'
        const methods = {setLocale: () => {}}

        const expectedMethodsObj = {
          23: methods
        }

        attachMethods(widgetKey, methods)

        expect(window[METHODS_OBJ_NAME]).to.deep.equal(expectedMethodsObj)
      })

      test('should attach methods to obj', () => {
        const widgetKey = '23'
        const methods = {setLocale: () => {}}

        window[METHODS_OBJ_NAME] = {
          foo: 'something'
        }

        const expectedMethodsObj = {
          foo: 'something',
          23: methods
        }

        attachMethods(widgetKey, methods)

        expect(window[METHODS_OBJ_NAME]).to.deep.equal(expectedMethodsObj)
      })
    })
  })
})
