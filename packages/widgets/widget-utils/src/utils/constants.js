export const BOOTSTRAP_SCRIPT_OBJ_NAME = 'toccoBootstrap'
export const EVENT_HANDLERS_OBJ_NAME = 'toccoEventHandlers'
export const THEME_OBJ_NAME = 'toccoTheme'
export const METHODS_OBJ_NAME = 'toccoMethods'
export const TQL_FUNC_NAME = 'tql'
export const TQL_META_OBJ_NAME = 'tqlMeta'
export const BACKEND_URL = 'toccoBackendUrl'

export const ATTRIBUTE_WIDGET_KEY = 'data-tocco-widget-key'
export const ATTRIBUTE_WIDGET_REF = 'data-tocco-widget-ref'
export const ATTRIBUTE_VISIBILITY_STATUS = 'data-tocco-visibility-status'

export const ID_INITIAL_VISIBILITY_STATUS_STYLES = 'tocco-initial-visibility-status-styles'
export const ID_VISIBILITY_STATUS_STYLES = 'tocco-visibility-status-styles'

export const WIDGET_UTILS_PACKAGE_NAME = 'widget-utils'
export const WIDGET_PLACEHOLDER_APP_NAME = 'widget-placeholder'

export const ERROR_CODE_INVALID_DOMAIN = 'INVALID_DOMAIN'
export const ERROR_CODE_INACTIVE = 'INACTIVE'
