import {utils} from 'tocco-util/bundle'

import {WIDGET_PLACEHOLDER_APP_NAME, WIDGET_UTILS_PACKAGE_NAME} from './constants'
import * as remoteLogger from './remoteLogger'
import {getTheme} from './utils'

const renderWidgetPlaceholder = async (backendUrl, assetUrl, container, key, input) => {
  try {
    await utils.loadScriptAsync(
      `${assetUrl}${utils.getEntryFilePath(WIDGET_UTILS_PACKAGE_NAME, WIDGET_PLACEHOLDER_APP_NAME)}`
    )
  } catch (error) {
    remoteLogger.logException(backendUrl, `Could not fetch package 'tocco-${WIDGET_UTILS_PACKAGE_NAME}'.`, error)
    return
  }

  try {
    const srcPath = `${assetUrl}/js/tocco-${WIDGET_UTILS_PACKAGE_NAME}/dist/`
    window.reactRegistry.render(WIDGET_PLACEHOLDER_APP_NAME, container, key, input, {}, srcPath)
  } catch (error) {
    remoteLogger.logException(backendUrl, `Could not render app '${WIDGET_PLACEHOLDER_APP_NAME}'.`, error)
  }
}

export const renderErrorWidgetPlaceholder = async (backendUrl, assetUrl, container, key, locale) => {
  const customTheme = getTheme()

  await renderWidgetPlaceholder(backendUrl, assetUrl, container, key, {
    backendUrl,
    ...(locale ? {locale} : {}),
    ...(customTheme ? {customTheme} : {}),
    placeholder: 'error',
    appContext: {embedType: 'widget'}
  })
}
