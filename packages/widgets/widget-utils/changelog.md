3.13.41
- make widget date picker more compact

3.13.40
- add simple image fiel component
- use image field for read only images
- render bigger thumbnails
- use smaller image dimension
- fix all upload components
- align ranges horizontally for widgets

3.13.39
- filter columns based on current layout
- change signal box color to light, harmonize padding and add border radius

3.13.38
- add isTransparent prop to Ball to enable transparent backgrounds

3.13.37
- fix document component

3.13.36
- fix modal in routerless widgets

3.13.35
- show icon on tiles
- add icons

3.13.34
- revert ckeditor4 update to 4.25.1
- show title columns in tile layout
- add week numbers to date picker

3.13.33
- add labelVisibility prop to Button to enable omitting the prop in specific cases

3.13.32
- fix sidepanel button overflow
- able to set justify alingment on tile column
- handle use label properly

3.13.31
- harmonize react select spacing

3.13.30
- fix focus styling

3.13.29
- close all modals on route change
- adapt component list text resources

3.13.28
- fix adress / location browser autofill
- implement error border inside cell inputs and remove cell focus on bool edit
- fix focus styling

3.13.27
- force rerender of ckeditor with auto-complete
- export idSelector
- fix ckeditor and initial auto complete

3.13.26
- improve label / form field component accessibility
- improve tile responsiveness on mobile devices

3.13.25
- fix stylelint error by removing obsolete styling

3.13.24
- remove useSelectorsByDefault property
- show links to docs for errors
- show proper message for inactive widgets

3.13.23
- allow setting title for button
- pass inWrongBusinessUnit

3.13.22
- add data-cy to table refresh button

3.13.21
- add background color for table tiles
- improve tile styling

3.13.20
- no error message on column resize

3.13.19
- add adjustFieldSets form modifier
- apply correct theme to dynamic actions
- show tile on narrow phones
- hide empty cells on tile

3.13.18
- pass allowed files type to Upload input
- pass allowed file types to upload components

3.13.17
- fix react autosuggest input width
- activate tree shaking and prevent bundle conflicts

3.13.16
- fix stated value labels spacing
- fix color of label-neutral

3.13.15
- harmonize google maps icon positioning in LocationEdit

3.13.14
- add timezone header

3.13.13
- show tooltips on multi-select values
- add tocco home breadcrumb item

3.13.12
- make display form fields always immutable
- move DescriptionFormatter icon in line with other fields

3.13.11
- first version of tile table layout
- add table header for tile layout
- adapt styling for tile layout
- change dark theme borderTable color for more contrast
- harmonize tile spacing and contrast

3.13.10
- show full content of immutable textarea
- hide terms component if all fields are readonly
- harmonize cell spacing of table form

3.13.9
- ignore line breaks in label texts for the sidepanel since they cover the input content

3.13.8
- pass condition to list in AdvancedSearch
- show scrollbar on large menus
- add data-cy attribute

3.13.7
- cleanup marking code
- add native date(time)picker for touch devices

3.13.6
- fix table header drop indicator
- improve draggable element's visibility
- show drop indicator only on valid drop areas

3.13.5
- add _widget_key query param to all requests

3.13.4
- apply touch improvements only for primary touch devices
- fix wrong hover color of add/remove button
- escape html based on options
- pass escapeHtml option in display field

3.13.3
- fix growing textarea
- handle StyledLayoutContainer styling globally
- able to set link state on breadcrumbs

3.13.2
- fix cut off text in singleValue Selects
- add fetchModules helper

3.13.1
- use common error list for fields
- add null check for reference
- set correct height for non searchable selects

3.13.0
- initial release for version 3.13

3.12.6
- revert focus fix

3.12.5
- do not auto focus anything on touch devices

3.12.4
- fix missing focus styles in some elements
- fix double scrollbars in some modals
- improve UX for simple selects on touch devices
- render error message instead nothing

3.12.3
- prevent infinite resize
- consider vertical drop position for column sorting

3.12.2
- add string table navigation handling
- add datepicker table navigation handling
- add text table navigation handling

3.12.1
- dependency update
- dev dependency update
- update storybook 8
- update path-to-regexp

3.12.0
- initial release for version 3.12

3.11.8
- support search fields in useAutofocus

3.11.7
- cleanup transient props

3.11.6
- add navigateToPath to navigationStrategy

3.11.5
- update to yarn 4

3.11.4
- handle rule provider in widgets

3.11.3
- add react-router compatibility package

3.11.2
- fix range mapping after upgrade to date-fns 3.x

3.11.1
- allow auto focus to be disabled

3.11.0
- initial release for version 3.11

3.10.7
- map postcode fields to string tql

3.10.6
- return horizontal drop position in useDnD

3.10.5
- add method to extract env as input props

3.10.4
- map phone fields to string tql

3.10.3
- add isDefined helper

3.10.2
- handle float imprecision in duration calculation

3.10.1
- allow not whitelisted style properties for svg element

3.10.0
- initial release for version 3.10

3.9.2
- map email fields to string tql

3.9.1
- fix column order resetting when moving multiple columns after another

3.9.0
- initial release for version 3.9

3.8.4
- escape front-slash in fulltext tql query

3.8.3
- disable sideEffects optimization in webpack

3.8.2
- add useColumnPosition
- add sorting reducer helper
- add splitDataForm
- add additional units

3.8.1
- fix broken prop types
- do not use array index in keys

3.8.0
- initial release for version 3.8

0.1.52
- load tql helper synchron

0.1.51
- handle validation response properly

0.1.50
- handle de-DE locale for datepicker

0.1.49
- add pickAllProperties to getFlattenEntity

0.1.48
- move question code to tocco-util

0.1.47
- use key as app id

0.1.46
- fix build for customer bundles

0.1.45
- add tql request

0.1.44
- extend embed type
- treat admin embedType logic as well for legacy-admin

0.1.43
- remove x-backend-url header

0.1.42
- fix fulltext search

0.1.41
- restructure cache for widget use cases
- remove revision from object cache

0.1.40
- add X-Backend-Url to all requests
- adjust X-Backend-Url to all requests

0.1.39
- load React only once
- re-init cache instead of clear cache

0.1.38
- tqlBuilder now builds multiple conditions connected by 'or' when passed localized paths

0.1.37
- be able to embed bootstrap script relative

0.1.36
- remove data-id attribute

0.1.35
- rename onStateChange to onVisibilityStatusChange

0.1.34
- fix autofocus by wrapping selector in timeout

0.1.33
- add possibility to show reload prompt

0.1.32
- add widget states

0.1.31
- fix object cache

0.1.30
- add object cache

0.1.29
- support identifier type in tql builder

0.1.28
- allow target attribute in html

0.1.27
- introduce legacy-admin embed type

0.1.26
- warn about mismatching bootstrap scripts

0.1.25
- fix performance of fulltext search

0.1.24
- autofocus datepickers

0.1.23
- fix escape handling in tql fulltext search

0.1.22
- add widget config key to env

0.1.21
- ignore any text and textarea fields of a datepicker when autofocusing

0.1.20
- fix datetime search field with default value

0.1.19
- fulltext search fields are now prioritized to be autofocused

0.1.18
- fix datetime in searchforms after using date-fns

0.1.17
- do not import react components in widget-utils

0.1.16
- remove momentjs in favor of date-fns

0.1.15
- use boolean search component for marking

0.1.14
- use different entry files per app in package

0.1.13
- add legacy-widget embedType

0.1.12
- support selectors in forms

0.1.11
- debouncer accepts value changes from outside

0.1.10
- add widgetConfigKey to appContext input params

0.1.9
- filter out null values on nested paths
- handle nested 'to many' relations on list forms

0.1.8
- set embedType for widgets

0.1.7
- split backend url in asset and backend url

0.1.6


0.1.5
- fix cache clearance to keep notifications session
- Tiny millisecond values (1 - 3) are now displayed correctly.

0.1.4


0.1.3
- handle errors and log to remote
- allow whitelisted inline css for nice tooltips
- provide `methods` via `window.toccoMethods`
- run bootstrap script only once
- run bootstrap script after dom loaded

0.1.2
- initialize widget with widget config

0.1.1
- add support for ext. event handlers for widgets
- prefix global objects with `tocco`

0.1.0
- initial release

