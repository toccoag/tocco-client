import {getPackageDirectory} from '../../../../build/lib/packages'
import {removeCKEditor} from '../../../../build/lib/webpack'

export const adjustConfig = (webpackConfig, config, paths) => {
  const {__PACKAGE__} = config.globals
  const packageDir = getPackageDirectory(__PACKAGE__)

  webpackConfig.entry = {
    bootstrap: paths.client(`${packageDir}/src/bootstrap/index.js`),
    'widget-placeholder': paths.client(`${packageDir}/src/apps/widget-placeholder.js`),
    tql: paths.client(`${packageDir}/src/tql/index.js`)
  }
  webpackConfig.output.filename = '[name].js'
  webpackConfig.output.library = `tocco-${__PACKAGE__}-[name]`

  webpackConfig = removeCKEditor(webpackConfig)

  return webpackConfig
}
