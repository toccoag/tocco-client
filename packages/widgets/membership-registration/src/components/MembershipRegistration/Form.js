import PropTypes from 'prop-types'
import {reduxForm} from 'redux-form'
import {form} from 'tocco-app-extensions'
import {Button} from 'tocco-ui'

export const REDUX_FORM_NAME = 'membership-registration'

const Form = ({questionFieldMapping, formDefinition, formValues, submitting, valid, register, intl}) => {
  const formEventProps = form.hooks.useFormEvents({submitForm: register})

  const shouldRenderField = fieldName => {
    if (form.isQuestionField(fieldName)) {
      const membershipTypeKey = formValues?.relMembership_type?.key
      return form.shouldRenderQuestionField(
        fieldName,
        membershipTypeKey ? [membershipTypeKey] : [],
        questionFieldMapping
      )
    }
    return true
  }

  return (
    <form {...formEventProps}>
      <form.FormBuilder
        entity={{model: 'Membership'}}
        formName={REDUX_FORM_NAME}
        formDefinition={formDefinition}
        formValues={formValues}
        fieldMappingType="editable"
        mode={'create'}
        beforeRenderField={shouldRenderField}
      />
      <Button
        disabled={submitting || !valid}
        label={intl.formatMessage({id: 'client.membership-registration.register'})}
        type="submit"
        look="raised"
        ink="primary"
      />
    </form>
  )
}

Form.propTypes = {
  questionFieldMapping: PropTypes.object,
  formDefinition: PropTypes.object,
  formValues: PropTypes.object,
  submitting: PropTypes.bool,
  valid: PropTypes.bool,
  register: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired
}

export default reduxForm({form: REDUX_FORM_NAME, destroyOnUnmount: false})(Form)
