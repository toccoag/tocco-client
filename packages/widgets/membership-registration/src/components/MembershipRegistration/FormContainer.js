import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormValues} from 'redux-form'
import {form} from 'tocco-app-extensions'

import {register} from '../../modules/membershipRegistration/actions'

import Form, {REDUX_FORM_NAME} from './Form'

const mapActionCreators = {
  register
}

const mapStateToProps = state => ({
  questionFieldMapping: state.membershipRegistration.questionFieldMapping,
  formDefinition: state.membershipRegistration.formDefinition,
  formValues: getFormValues(REDUX_FORM_NAME)(state),
  formErrors: form.selectors.getFormErrors(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Form))
