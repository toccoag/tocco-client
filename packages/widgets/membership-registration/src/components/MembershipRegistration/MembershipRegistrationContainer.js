import {injectIntl} from 'react-intl'
import {connect} from 'react-redux'
import {getFormInitialValues} from 'redux-form'
import {externalEvents} from 'tocco-app-extensions'

import {loadFormDefinition} from '../../modules/membershipRegistration/actions'

import {REDUX_FORM_NAME} from './Form'
import MembershipRegistration from './MembershipRegistration'

const mapActionCreators = {
  loadFormDefinition,
  fireVisibilityStatusChangeEvent: externalEvents.fireVisibilityStatusChangeEvent
}

const mapStateToProps = state => ({
  hideForm: state.membershipRegistration.hideForm,
  formDefinition: state.membershipRegistration.formDefinition,
  fieldDefinitions: state.membershipRegistration.fieldDefinitions,
  formInitialValues: getFormInitialValues(REDUX_FORM_NAME)(state)
})

export default connect(mapStateToProps, mapActionCreators)(injectIntl(MembershipRegistration))
