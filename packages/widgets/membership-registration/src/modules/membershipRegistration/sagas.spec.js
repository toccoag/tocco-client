import {actions as formActions} from 'redux-form'
import {all, select, takeLatest} from 'redux-saga/effects'
import {expectSaga} from 'redux-saga-test-plan'
import * as matchers from 'redux-saga-test-plan/matchers'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, env, intl, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/MembershipRegistration'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'
import rootSaga, * as sagas from './sagas'

describe('membership-registration', () => {
  describe('modules', () => {
    describe('membershipRegistration', () => {
      describe('sagas', () => {
        describe('rootSaga', () => {
          test('should fork child sagas', () => {
            const generator = rootSaga()
            expect(generator.next().value).to.deep.equal(
              all([
                takeLatest(actions.LOAD_FORM_DEFINITION, sagas.loadFormDefinition),
                takeLatest(actions.REGISTER, sagas.register)
              ])
            )
            expect(generator.next().done).to.be.true
          })
        })

        describe('loadFormDefinition', () => {
          test('should load form and field definitions and initialize form', () => {
            const formDefinition = {children: []}
            const questionsPseudoFormFields = []
            const expectedFormDefinition = {createEndpoint: 'widgets/membershipRegistration/validate', children: []}
            const fieldDefinitions = [{id: 'field_1'}, {id: 'field_2'}]
            const defaultValues = {field_1: 'value 1'}
            const entityValues = {field_2: 'value 2'}
            const additionalDefaultValues = {field_3: 'value 3'}
            const formValues = {
              __key: undefined,
              __model: 'Membership',
              __version: undefined,
              field_1: 'value 1'
            }
            return expectSaga(sagas.loadFormDefinition)
              .provide([
                [
                  select(sagas.inputSelector),
                  {formBase: 'Single_membership_registration', defaultValues: additionalDefaultValues}
                ],
                [matchers.call(rest.fetchForm, 'Single_membership_registration', 'create'), formDefinition],
                [matchers.call(sagas.getQuestionsPseudoFormFields), questionsPseudoFormFields],
                [matchers.call(form.getFieldDefinitions, expectedFormDefinition), fieldDefinitions],
                [matchers.call(form.getDefaultValues, fieldDefinitions), defaultValues],
                [matchers.call.fn(api.getFlattenEntity), entityValues],
                [matchers.call.fn(form.entityToFormValues), formValues]
              ])
              .call(
                form.entityToFormValues,
                {...entityValues, ...defaultValues, ...additionalDefaultValues},
                fieldDefinitions
              )
              .put(formActions.initialize(REDUX_FORM_NAME, formValues))
              .put(actions.setFormDefinition(expectedFormDefinition))
              .put(actions.setFieldDefinitions(fieldDefinitions))
              .run()
          })
        })

        describe('getQuestionsPseudoFormFields', () => {
          test('create pseudo membership question fields', () => {
            const response = {
              body: {
                questions: []
              }
            }
            const questionFieldMapping = {}
            const questionsPseudoFields = []
            return expectSaga(sagas.getQuestionsPseudoFormFields)
              .provide([[matchers.call(rest.requestSaga, '/widgets/membershipRegistration/questions'), response]])
              .put(actions.setQuestionFieldMapping(questionFieldMapping))
              .returns(questionsPseudoFields)
              .run()
          })
        })

        describe('register', () => {
          const bean = {
            membership: {
              field: 'value'
            },
            questions: []
          }
          const widgetConfigKey = '456'
          env.setWidgetConfigKey(widgetConfigKey)

          test('should register', () => {
            const response = {status: 201}
            return expectSaga(sagas.register)
              .provide([
                [matchers.call(sagas.loadRegisterBean), bean],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'widgets/membershipRegistration/register', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: bean,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(actions.hideForm())
              .put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
              .run()
          })

          test('should handle validation', () => {
            const response = {status: 400, body: {errorCode: 'VALIDATION_FAILED', errors: []}}
            return expectSaga(sagas.register)
              .provide([
                [matchers.call(sagas.loadRegisterBean), bean],
                [select(intl.localeSelector), 'de'],
                [matchers.call.fn(rest.requestSaga), response]
              ])
              .put(formActions.startSubmit(REDUX_FORM_NAME))
              .call(rest.requestSaga, 'widgets/membershipRegistration/register', {
                method: 'POST',
                queryParams: {
                  locale: 'de'
                },
                body: bean,
                acceptedErrorCodes: ['VALIDATION_FAILED']
              })
              .put(formActions.stopSubmit(REDUX_FORM_NAME))
              .put(
                notification.toaster({
                  type: 'error',
                  title: 'client.component.actions.validationError',
                  body: validation.getErrorCompact(response.body.errors)
                })
              )
              .run()
          })
        })

        describe('loadRegisterBean', () => {
          test('should return register bean', () => {
            const formValues = {
              relMembership_type: {
                key: '123'
              }
            }
            const questionFieldMapping = {
              1: {type: 'text_single_line', relatedEntityKey: '123'},
              2: {type: 'text_single_line', relatedEntityKey: '678'}
            }
            const flatEntity = {
              comment: 'value',
              pseudoField_question_1: 'value1',
              pseudoField_question_2: 'value2'
            }
            const flatMembership = {
              comment: 'value'
            }
            const membership = {
              key: '321'
            }
            const returnValue = {
              membership,
              questions: [
                {
                  key: '1',
                  type: 'text_single_line',
                  value: 'value1'
                }
              ]
            }
            const fieldDefinitions = [
              {
                id: 'field-id'
              },
              {
                id: 'pseudoField_question_1',
                pseudoField: true
              },
              {
                id: 'pseudoField_question_2',
                pseudoField: true
              }
            ]
            return expectSaga(sagas.loadRegisterBean)
              .provide([
                [matchers.call.fn(sagas.loadFormValues), formValues],
                [select(sagas.membershipRegistrationSelector), {fieldDefinitions, questionFieldMapping}],
                // do not pass arguments here because getFormValues doesn't work with redux-saga-test-plan
                [matchers.call.fn(form.formValuesToFlattenEntity), flatEntity],
                [matchers.call(api.toEntity, flatMembership), membership]
              ])
              .returns(returnValue)
              .run()
          })
        })
      })
    })
  })
})
