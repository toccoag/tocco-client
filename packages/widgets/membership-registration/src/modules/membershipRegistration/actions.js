export const LOAD_FORM_DEFINITION = 'membershipRegistration/LOAD_FORM_DEFINITION'
export const SET_FORM_DEFINITION = 'membershipRegistration/SET_FORM_DEFINITION'
export const SET_FIELD_DEFINITIONS = 'membershipRegistration/SET_FIELD_DEFINITIONS'
export const REGISTER = 'membershipRegistration/REGISTER'
export const SET_HIDE_FORM = 'membershipRegistration/SET_HIDE_FORM'
export const SET_QUESTION_FIELD_MAPPING = 'eventRegistrationAction/SET_QUESTION_FIELD_MAPPING'

export const loadFormDefinition = () => ({
  type: LOAD_FORM_DEFINITION
})

export const setFormDefinition = formDefinition => ({
  type: SET_FORM_DEFINITION,
  payload: {formDefinition}
})

export const setFieldDefinitions = fieldDefinitions => ({
  type: SET_FIELD_DEFINITIONS,
  payload: {fieldDefinitions}
})

export const register = () => ({
  type: REGISTER
})

export const hideForm = () => ({
  type: SET_HIDE_FORM,
  payload: {
    hideForm: true
  }
})

export const setQuestionFieldMapping = questionFieldMapping => ({
  type: SET_QUESTION_FIELD_MAPPING,
  payload: {
    questionFieldMapping
  }
})
