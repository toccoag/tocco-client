import {reducer as reducerUtil} from 'tocco-util'

import * as actions from './actions'

const initialState = {
  formDefinition: null,
  fieldDefinitions: null,
  hideForm: false,
  questionFieldMapping: undefined
}

const ACTION_HANDLERS = {
  [actions.SET_FORM_DEFINITION]: reducerUtil.singleTransferReducer('formDefinition'),
  [actions.SET_FIELD_DEFINITIONS]: reducerUtil.singleTransferReducer('fieldDefinitions'),
  [actions.SET_HIDE_FORM]: reducerUtil.singleTransferReducer('hideForm'),
  [actions.SET_QUESTION_FIELD_MAPPING]: reducerUtil.singleTransferReducer('questionFieldMapping')
}

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
