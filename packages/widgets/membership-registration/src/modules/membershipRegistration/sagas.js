import {actions as formActions, getFormValues} from 'redux-form'
import {all, call, put, select, takeLatest} from 'redux-saga/effects'
import {rest, form, externalEvents, notification} from 'tocco-app-extensions'
import {api, intl, remoteLogger, validation} from 'tocco-util'

import {REDUX_FORM_NAME} from '../../components/MembershipRegistration'
import {VisibilityStatus} from '../../visibilityStatus'

import * as actions from './actions'

export const inputSelector = state => state.input
export const membershipRegistrationSelector = state => state.membershipRegistration

export default function* sagas() {
  yield all([takeLatest(actions.LOAD_FORM_DEFINITION, loadFormDefinition), takeLatest(actions.REGISTER, register)])
}

export function* loadFormDefinition() {
  const {formBase, defaultValues: additionalDefaultValues} = yield select(inputSelector)
  // add createEndpoint for async validation
  const formDefinition = {
    ...(yield call(rest.fetchForm, formBase, 'create')),
    createEndpoint: 'widgets/membershipRegistration/validate'
  }

  const questionsPseudoFormFields = yield call(getQuestionsPseudoFormFields)

  const newFormDefinition = form.replaceBoxChildren(
    formDefinition,
    'membership_question_box',
    questionsPseudoFormFields.map(f => f.formField)
  )

  const fieldDefinitions = yield call(form.getFieldDefinitions, newFormDefinition)

  const defaultValues = yield call(form.getDefaultValues, fieldDefinitions)
  const entityValues = yield call(api.getFlattenEntity, {
    model: 'Membership',
    paths: form.getMergedEntityPaths(questionsPseudoFormFields)
  })
  const formValues = yield call(
    form.entityToFormValues,
    {...entityValues, ...defaultValues, ...additionalDefaultValues},
    fieldDefinitions
  )
  yield put(formActions.initialize(REDUX_FORM_NAME, formValues))

  yield all([put(actions.setFormDefinition(newFormDefinition)), put(actions.setFieldDefinitions(fieldDefinitions))])
}

export function* getQuestionsPseudoFormFields() {
  const response = yield call(rest.requestSaga, '/widgets/membershipRegistration/questions')

  const questions = response.body.questions
  yield put(actions.setQuestionFieldMapping(form.getQuestionFieldMapping(questions)))
  const questionsPseudoFields = questions.map(q => form.mapQuestionToPseudoField(q))
  return questionsPseudoFields
}

export function* register() {
  yield put(formActions.startSubmit(REDUX_FORM_NAME))

  const bean = yield call(loadRegisterBean)
  const locale = yield select(intl.localeSelector)

  const response = yield call(rest.requestSaga, 'widgets/membershipRegistration/register', {
    method: 'POST',
    queryParams: {
      locale
    },
    body: bean,
    acceptedErrorCodes: ['VALIDATION_FAILED']
  })
  if (response.status === 201) {
    yield put(actions.hideForm())
    yield put(externalEvents.fireVisibilityStatusChangeEvent([VisibilityStatus.success]))
  } else {
    yield put(formActions.stopSubmit(REDUX_FORM_NAME))
    if (response.body.errorCode === 'VALIDATION_FAILED') {
      yield put(
        notification.toaster({
          type: 'error',
          title: 'client.component.actions.validationError',
          body: validation.getErrorCompact(response.body.errors)
        })
      )
    } else {
      remoteLogger.logError('unexpected error during registration', response)
    }
  }
}

export function* loadRegisterBean() {
  const formValues = yield call(loadFormValues, REDUX_FORM_NAME)
  const {fieldDefinitions, questionFieldMapping} = yield select(membershipRegistrationSelector)
  const flatEntity = yield call(form.formValuesToFlattenEntity, formValues, fieldDefinitions)
  const splittedFlatEntity = form.splitFlattenEntity(flatEntity, fieldDefinitions)

  const membershipTypeKey = formValues?.relMembership_type?.key

  const membership = yield call(api.toEntity, splittedFlatEntity.entity)
  const questions = form.mapQuestionsToBean(questionFieldMapping, [membershipTypeKey], splittedFlatEntity.pseudoFields)

  return {
    membership,
    questions
  }
}

// helper function to allow easy mocking in test
export function* loadFormValues(formId) {
  return yield select(getFormValues(formId))
}
