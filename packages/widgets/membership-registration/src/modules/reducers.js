import {reducer as form} from 'redux-form'

import membershipRegistrationReducer from './membershipRegistration/reducer'
import membershipRegistrationSagas from './membershipRegistration/sagas'

export default {
  membershipRegistration: membershipRegistrationReducer,
  form
}

export const sagas = [membershipRegistrationSagas]
