import {useFloating, useInteractions, offset, useClick, useDismiss, autoUpdate, flip} from '@floating-ui/react'
import PropTypes from 'prop-types'
import {useState} from 'react'
import {FormattedMessage} from 'react-intl'
import {MenuItem, Menu, Icon} from 'tocco-ui'

import {StyledButton, StyledHeader, StyledIconWrapper, StyledLabelWrapper} from './StyledComponents'

const UserMenu = ({loggedIn, username, logout, buttonProps, onOpen}) => {
  const [menuOpen, setMenuOpen] = useState(false)
  const [onOpenCalled, setOnOpenCalled] = useState(false)

  const handleClick = e => {
    setMenuOpen(!menuOpen)
    e.stopPropagation()

    if (onOpen && !onOpenCalled) {
      onOpen()
      setOnOpenCalled(true)
    }
  }

  const handleClose = () => {
    setMenuOpen(false)
  }

  const {refs, floatingStyles, context} = useFloating({
    open: menuOpen,
    onOpenChange: setMenuOpen,
    placement: 'bottom-start',
    middleware: [offset(7), flip()],
    whileElementsMounted: autoUpdate
  })

  const chevronIcon = menuOpen ? 'chevron-up' : 'chevron-down'
  const click = useClick(context)
  const dismiss = useDismiss(context)

  const {getReferenceProps, getFloatingProps} = useInteractions([click, dismiss])

  return (
    <StyledHeader>
      {loggedIn && (
        <StyledButton
          {...buttonProps}
          type="button"
          ref={refs.setReference}
          {...getReferenceProps()}
          title={username}
          onClick={handleClick}
          data-cy="btn-user-menu"
        >
          <StyledLabelWrapper>{username}</StyledLabelWrapper>
          <StyledIconWrapper>
            <Icon icon={chevronIcon} />
          </StyledIconWrapper>
          {menuOpen && (
            <Menu
              floatingStyles={floatingStyles}
              getFloatingProps={getFloatingProps}
              refs={refs}
              isOpen={menuOpen}
              onClose={handleClose}
            >
              <MenuItem disabled>{username}</MenuItem>
              <MenuItem onClick={logout} data-cy="menuitem-logout">
                <FormattedMessage id="client.user-menu.logout" />
              </MenuItem>
            </Menu>
          )}
        </StyledButton>
      )}
    </StyledHeader>
  )
}

UserMenu.propTypes = {
  loggedIn: PropTypes.bool,
  username: PropTypes.string,
  logout: PropTypes.func.isRequired,
  /**
   * Object of properties as described in Ball component
   */
  buttonProps: PropTypes.object,
  /**
   * Callback when the menu is opened (only called once)
   */
  onOpen: PropTypes.func
}

export default UserMenu
