import PropTypes from 'prop-types'
import {appFactory} from 'tocco-app-extensions'
import {reducer as reducerUtil, appContext, env, consoleLogger} from 'tocco-util'

import FixedWrapper from './components/FixedWrapper'
import UserMenu from './components/UserMenu'
import {getDispatchActions} from './input'
import reducers, {sagas} from './modules/reducers'

const packageName = 'user-menu'

const initApp = (id, input, events, publicPath) => {
  env.setInputEnvs(input)

  const content = <UserMenu />

  const defaultInputs = {
    width: '120px',
    height: '40px'
  }
  const actualInputs = {...defaultInputs, ...input}
  const store = appFactory.createStore(reducers, sagas, actualInputs, packageName)

  const userMenuApp = appFactory.createApp(packageName, content, store, {
    input,
    events,
    actions: getDispatchActions(),
    publicPath,
    textResourceModules: ['component', 'common', packageName]
  })

  return {
    ...userMenuApp,
    component: (
      <FixedWrapper width={actualInputs.width} height={actualInputs.height}>
        {userMenuApp.component}
      </FixedWrapper>
    )
  }
}

;(() => {
  if (__PACKAGE_NAME__ === packageName) {
    appFactory.registerAppInRegistry(packageName, initApp)

    if (__DEV__) {
      const input = __DEV_PACKAGE_INPUT__ || require('./dev/input.json')
      consoleLogger.log(`${packageName} - input:`, input)

      const {store, component} = initApp(packageName, input)

      if (module.hot) {
        module.hot.accept('./modules/reducers', () => {
          const hotReducers = require('./modules/reducers').default
          reducerUtil.hotReloadReducers(store, hotReducers)
        })
      }

      appFactory.renderApp(component)
    }
  }
})()

const UserMenuApp = props => {
  const {component} = appFactory.useApp({initApp, props, packageName})
  return component
}

UserMenuApp.propTypes = {
  appContext: appContext.propTypes.isRequired,
  /**
   * Redirect url if user clicks on the logout button.
   * If no redirect url is set the user is redirected the the home page
   */
  logoutRedirectUrl: PropTypes.string
}

export default UserMenuApp
export const app = appFactory.createBundleableApp(packageName, initApp, UserMenuApp)
