import {addons} from '@storybook/manager-api'

import toccoTheme from './toccoTheme'

addons.setConfig({
  theme: toccoTheme
})
