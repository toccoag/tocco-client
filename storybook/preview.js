import {useGlobals} from '@storybook/preview-api'
import {StyleSheetManager, ThemeProvider, createGlobalStyle} from 'styled-components'

import darkTheme from '../packages/core/tocco-theme/src/ToccoTheme/darkTheme'
import defaultTheme from '../packages/core/tocco-theme/src/ToccoTheme/defaultTheme'
import shouldForwardProp from '../packages/core/tocco-theme/src/utils/shouldForwardProp'
import widgetTheme from '../packages/core/tocco-theme/src/WidgetTheme/widgetTheme'
import GlobalStyles from '../packages/core/tocco-ui/src/GlobalStyles'
import env from '../packages/core/tocco-util/src/env'

const intlMessageProxy = new Proxy(
  {},
  {
    get: (target, prop, receiver) => prop,
    getOwnPropertyDescriptor: (target, prop) => ({configurable: true, enumerable: true, value: prop})
  }
)

const withGlobalStyles = Story => (
  <>
    <GlobalStyles />
    <Story />
  </>
)

const GlobalCmsStyles = createGlobalStyle`
  html,
  body {
    font-size: 14px;
    font-family: Roboto, serif;
  }
`

const getTheme = themeName => {
  switch (themeName) {
    case 'dark':
      return darkTheme
    case 'light':
    default:
      return defaultTheme
  }
}

const syncTheme = (themeName, embedType, updateTheme) => {
  if (['admin'].includes(embedType)) {
    if (!['light', 'dark'].includes(themeName)) {
      updateTheme('light')
    }
  }

  if (['legacy-widget', 'legacy-admin'].includes(embedType)) {
    if (themeName !== 'light') {
      // only light theme possible
      updateTheme('light')
    }
  }

  if (embedType === 'widget') {
    if (themeName !== 'widget') {
      // only widget theme possible
      updateTheme('widget')
    }
  }
}

const withEmbedType = (Story, context) => {
  const embedType = context.globals.embedType
  const themeName = context.globals.theme
  const theme = getTheme(themeName)
  env.setEmbedType(embedType)

  const [_globals, updateGlobals] = useGlobals() // eslint-disable-line
  const isWidgetStory = context.componentId.startsWith('widgets-')
  const isAdminStory = context.componentId.startsWith('admin-')

  if (embedType !== 'widget' && isWidgetStory) {
    updateGlobals({
      embedType: 'widget'
    })
  }

  if (embedType !== 'admin' && isAdminStory) {
    updateGlobals({
      embedType: 'admin'
    })
  }

  syncTheme(themeName, embedType, t => updateGlobals({theme: t}))

  const actualTheme = env.isInWidgetEmbedded() ? widgetTheme : theme

  return (
    <ThemeProvider theme={actualTheme}>
      <StyleSheetManager shouldForwardProp={shouldForwardProp}>
        {env.isInWidgetEmbedded() && <GlobalCmsStyles />}
        <Story />
      </StyleSheetManager>
    </ThemeProvider>
  )
}

export const decorators = [withGlobalStyles, withEmbedType]
export const parameters = {
  intl: {
    locales: ['de-CH', 'de', 'fr', 'it', 'en'],
    defaultLocale: 'de-CH',
    getMessages: () => intlMessageProxy
  }
}

export const argTypes = {
  intl: {table: {disable: true}}
}

export const globalTypes = {
  embedType: {
    defaultValue: 'admin',
    toolbar: {
      title: 'EmbedType',
      icon: 'eye',
      items: ['admin', 'widget', 'legacy-admin', 'legacy-widget'],
      dynamicTitle: true
    }
  },
  theme: {
    defaultValue: 'light',
    toolbar: {
      title: 'Theme',
      icon: 'paintbrush',
      items: ['light', 'dark', 'widget'],
      dynamicTitle: true
    }
  }
}

export const tags = ['autodocs']
