module.exports = {
  stories: [
    '../packages/core/tocco-theme/src/**/*.stories.@(js)',
    '../packages/core/tocco-ui/src/**/*.stories.@(js)',
    '../packages/core/app-extensions/src/**/*.stories.@(js)',
    '../packages/apps/admin/src/*.stories.@(js)',
    '../packages/widgets/**/*.stories.@(js)',
    '../packages/core/entity-detail/src/*.stories.@(js)',
    '../packages/core/entity-list/src/*.stories.@(js)',
    '../packages/core/simple-table-form/src/*.stories.@(js)',
    '../packages/actions/input-edit/src/*.stories.@(js)',
    '../packages/actions/resource-scheduler/src/*.stories.@(js)'
  ],
  addons: [
    '@storybook/addon-essentials',
    'storybook-addon-intl',
    '@storybook/addon-a11y',
    '@storybook/addon-webpack5-compiler-babel'
  ],
  framework: {
    name: '@storybook/react-webpack5',
    options: {}
  },
  staticDirs: ['assets'],
  docs: {}
}
