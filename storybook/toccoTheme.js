import {create} from '@storybook/theming'

export default create({
  base: 'light',
  brandTitle: 'Tocco Storybook',
  brandImage: 'tocco.png',
  brandUrl: 'https://gitlab.com/toccoag/tocco-client'
})
