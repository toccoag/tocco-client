import {removeIgnoredFiles} from './helper.mjs'

export default {
  '*': files => [`prettier --write --ignore-unknown ${files.join(' ')}`],
  '*.js': async files => {
    const filesToLint = await removeIgnoredFiles(files)
    return [`eslint --fix --cache ${filesToLint}`, `stylelint --cache --allow-empty-input ${files}`]
  }
}
