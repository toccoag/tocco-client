import {removeIgnoredFiles} from './helper.mjs'

export default {
  '*': files => [`prettier --check --ignore-unknown ${files.join(' ')}`],
  '*.js': async files => {
    const filesToLint = await removeIgnoredFiles(files)
    return [`eslint --cache ${filesToLint}`, `stylelint --fix --cache --allow-empty-input ${files.join(' ')}`]
  }
}
